<?php
require('./connect.php');

$tno=$_SESSION['diary'];

$type = escapeString($conn,strtoupper($_POST['type']));

$get_last_trip = Qry($conn,"SELECT to_station,to_id FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1");
		
if(!$get_last_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error.","./");
	exit();
}

if(numRows($get_last_trip)==0)
{
	$get_last_trip_final = Qry($conn,"SELECT to_station,to_id FROM dairy.trip_final WHERE tno='$tno' ORDER BY id DESC LIMIT 1");
		
	if(!$get_last_trip_final){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error.","./");
		exit();
	}
	
	if(numRows($get_last_trip_final)>0)
	{
		$row_last_trip_final = fetchArray($get_last_trip_final);
		$from_loc = $row_last_trip_final['to_station'];
		$from_id_set = $row_last_trip_final['to_id'];
	}
	else
	{
		$from_loc = "";
		$from_id_set = "";
	}
}
else
{
	$row_last_trip = fetchArray($get_last_trip);
	$from_loc = $row_last_trip['to_station'];
	$from_id_set = $row_last_trip['to_id'];
}
	
if($type=='OWN')
{
	$frno = escapeString($conn,strtoupper($_POST['frno']));
	
	$array_1 = explode('_', $frno);

	$frno = $array_1[0];
	$frno_id = $array_1[1];
	
	if($frno=="ATLOADING")
	{
		$at_loading = "1";
	}
	else
	{
		$fetch=Qry($conn,"SELECT l.frno,l.fstation,l.tstation,l.from_id,l.to_id,l.con1_id,l.con2_id,s1.pincode as from_pincode,
		s1._lat as from_lat,s1._long as from_long,s2.pincode as to_pincode,s2._lat as to_lat,s2._long as to_long 
		FROM freight_form_lr as l 
		LEFT OUTER JOIN station as s1 ON s1.id=l.from_id 
		LEFT OUTER JOIN station as s2 ON s2.id=l.to_id 
		WHERE l.id='$frno_id'");
		
		if(!$fetch){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error.","./");
			exit();
		}
	
		$fetch2=Qry($conn,"SELECT GROUP_CONCAT(lrno SEPARATOR ',') as lrno,SUM(wt12) as act,SUM(weight) as charge FROM freight_form_lr 
		WHERE frno='$frno' GROUP by frno");
		
		if(!$fetch2){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error.","./");
			exit();
		}
	
		$row = fetchArray($fetch);
		
		if($row['frno'] != $frno)
		{
			echo "<script>
				alert('Warning : Voucher not verified !');
				$('#lr_type_own').val('');
				$('#loadicon').fadeOut('slow');
			</script>";	
			exit();
		}
		
		$row2 = fetchArray($fetch2);
		
		$lrnos = $row2['lrno'];
		$act = sprintf('%0.3f',$row2['act']);
		$charge = sprintf('%0.3f',$row2['charge']);
		$from=$row['fstation'];
		$from_pincode=$row['from_pincode'];
		$from_lat=$row['from_lat'];
		$from_long=$row['from_long'];
		$from_id=$row['from_id'];
		$to=$row['tstation'];
		$to_pincode=$row['to_pincode'];
		$to_lat=$row['to_lat'];
		$to_long=$row['to_long'];
		$to_id=$row['to_id'];
		$billing_type='';
		$freight=0;
		
		$at_loading="0";
	}
	
	$lr_type_id="lr_type_own";
}
else if($type=='MKT')
{
	$mbno = escapeString($conn,strtoupper($_POST['frno']));
	
	$array_1 = explode('_', $mbno);

	$mbno = $array_1[0];
	$mbno_id = $array_1[1];
		
	if($mbno=="ATLOADING")
	{
		$at_loading="2";
	}
	else
	{
		$qq=Qry($conn,"SELECT l.bilty_no,l.billing_type,l.frmstn,l.tostn,l.awt,l.cwt,l.tamt,l.from_id,l.to_id,s1.pincode as from_pincode,
		s1._lat as from_lat,s1._long as from_long,s2.pincode as to_pincode,s2._lat as to_lat,s2._long as to_long 
		FROM mkt_bilty AS l 
		LEFT OUTER JOIN station as s1 ON s1.id=l.from_id 
		LEFT OUTER JOIN station as s2 ON s2.id=l.to_id 
		WHERE l.id='$mbno_id'");
		
		if(!$qq){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error.","./");
			exit();
		}
		
		$row = fetchArray($qq);
		
		if($row['bilty_no'] != $mbno)
		{
			echo "<script>
				alert('Warning : Voucher not verified !');
				$('#lr_type_mkt').val('');
				$('#loadicon').fadeOut('slow');
			</script>";	
			exit();
		}
		
		$from=$row['frmstn'];
		$from_pincode=$row['from_pincode'];
		$from_lat=$row['from_lat'];
		$from_long=$row['from_long'];
		$from_id=$row['from_id'];
		$to=$row['tostn'];
		$to_pincode=$row['to_pincode'];
		$to_lat=$row['to_lat'];
		$to_long=$row['to_long'];
		$to_id=$row['to_id'];
		$billing_type=$row['billing_type'];
		$freight=$row['tamt'];
		$act=sprintf('%0.3f',$row['awt']);
		$charge=sprintf('%0.3f',$row['cwt']);
		$lrnos=$mbno;
		$at_loading="0";
	}
	
	$lr_type_id="lr_type_mkt";
}
else
{
	Redirect("Invalid bilty type.","./");
	exit();
}

if($from_loc!='' AND $at_loading=="0")
{
	if($from_loc != $from)
	{
		echo "<script>
			alert('Warning : Last trip\'s destination: $from_loc not matching with LR\'s from location: $from.');
			$('#$lr_type_id').val('');
			$('#loadicon').fadeOut('slow');
		</script>";	
		exit();
	}
	
	if($from_id_set != $from_id)
	{
		errorLog("Location Ids does not match. FromLocSet: $from_id_set. From_Id_LR: $from_id.",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Warning : Location Ids not verified.');
			$('#$lr_type_id').val('');
			$('#loadicon').fadeOut('slow');
		</script>";	
		exit();
	}
}

if($at_loading>0)
{
	echo "<script>
		$('.from_station').val('$from_loc');
		$('.from_loc_id_1').val('$from_id_set');
		$('#to_stn').val('');
		$('#to_stn_id_create_trip').val('');
		$('#actual').val('0');
		$('#charge').val('0');
		$('#lrno').val('ATLOADING');
		$('#billing_type_text').val('');
		$('#freight_bilty_text').val('0');
		
		$('#to_stn').attr('readonly',false);
		$('#actual').attr('readonly',true);
		$('#charge').attr('readonly',true);
		$('#lrno').attr('readonly',true);
		$('#at_loading_set').val('$at_loading');
		$('#loadicon').hide();
	</script>";
	
	if($from_loc==''){
		echo "<script>$('.from_station').attr('readonly',false);</script>";	
	}
	else{
		echo "<script>$('.from_station').attr('readonly',true);</script>";	
	}
}
else
{
	echo "<script>
		$('.from_station').val('$from');
		$('.from_loc_id_1').val('$from_id');
		
		$('#to_stn').val('$to');
		$('#actual').val('$act');
		$('#charge').val('$charge');
		$('#lrno').val('$lrnos');
		$('#billing_type_text').val('$billing_type');
		$('#freight_bilty_text').val('$freight');
		
		$('#to_stn_id_create_trip').val('$to_id');
		
		$('.from_station').attr('readonly',true);
		$('#to_stn').attr('readonly',true);
		$('#actual').attr('readonly',true);
		$('#charge').attr('readonly',true);
		$('#lrno').attr('readonly',true);
		$('#at_loading_set').val('0');
		$('#loadicon').hide();
		
		$('#from_stn').attr('readonly',true);
	</script>";
}	
?>