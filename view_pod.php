<?php
require("connect.php");

$trip_id=escapeString($conn,$_POST['trip_id']);

$sel_files=Qry($conn,"SELECT tno,lrno,trip_no,date,copy FROM dairy.rcv_pod WHERE trip_id='$trip_id'");
if(!$sel_files){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($sel_files)==0)
{
	echo "<script>
		alert('NO RESULT FOUND..');
		window.close();
	</script>";
	exit();
}

echo '
	<title>View POD : E-diary || RAMAN ROADWAYS PVT LTD</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>';

echo "
<br>
<br>
<div class='container'>
		<div class='row'>

<table class='table table-bordered' style='font-family:Verdana;font-size:14px'>";

while($rowPod=fetchArray($sel_files))
{
	echo "
	<tr>
		<th>Truck No: $rowPod[tno], Trip No: $rowPod[trip_no], LR No: $rowPod[lrno]</th>
	</tr>
	<tr>
	<td>
	";
	$sn=1;
	foreach(explode(",",$rowPod['copy']) as $pod_files)
	{
		echo "&nbsp; &nbsp; <a class='btn btn-sm btn-primary' target='_blank' href='close_trip/$pod_files'>File $sn</a>";
		$sn++;	
	}	
	echo "</td>
	</tr>";
}

echo "</table>";
?>
</div>
</div>