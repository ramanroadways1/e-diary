<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 
$tno = escapeString($conn,strtoupper($_POST['tno']));
$adv_trans_type = escapeString($conn,strtoupper($_POST['adv_trans_type']));

$trip_id = escapeString($conn,strtoupper($_POST['trip_id']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertError("Trip not found..");
	errorLog("Trip id not found. id: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
	exit();
}

// echo "<script>alert('$adv_trans_type');</script>";

require_once("./_check_fix_lane.php");
require_once("./check_cache.php");

if($adv_trans_type=='HAPPAY')
{
	$get_Card_using = Qry($conn,"SELECT card_using,card_assigned FROM dairy.happay_card WHERE tno='$tno'");
	
	if(!$get_Card_using){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}

	if(numRows($get_Card_using)==0)
	{
		AlertError("Happay-card not found..");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	 
	$row_card_using = fetchArray($get_Card_using);

	if($row_card_using['card_assigned']=="0")
	{
		AlertError("Happay-card not assigned !");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	$card_kit_no = $row_card_using['card_using'];

	if($tno!=$card_kit_no){
		$other_Card_narration="Vehicle_No : $tno. Card Using : $card_kit_no.";
	}else{
		$other_Card_narration="";
	}
}
	
$comp=Qry($conn,"SELECT comp,happay_active FROM dairy.own_truck WHERE tno='$tno'");

if(!$comp){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
	exit();
}
	
if(numRows($comp)>0)
{
	$row_comp=fetchArray($comp);
	$company=$row_comp['comp'];
	$happay_active=$row_comp['happay_active'];
}
else
{
	AlertError("Company not found..");
	echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
	exit();
}

$amount = escapeString($conn,$_POST['amount']);
$date = date("Y-m-d");
$narration = escapeString($conn,($_POST['narration']));
$happay_card_no=escapeString($conn,strtoupper($_POST['happay_card_no']));

//// HAPPAY DUPLICATE RECHARGE ////

if($adv_trans_type=='HAPPAY')
{
	$chk_duplicate_txn = Qry($conn,"SELECT id FROM dairy.happay_card_transactions WHERE date(done_time)='".date("Y-m-d")."' AND card_using='$tno' 
	AND credit='$amount'");
		
	if(!$chk_duplicate_txn){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	$chk_duplicate_txn2 = Qry($conn,"SELECT id FROM dairy.happay_card_transactions WHERE date(timestamp)='".date("Y-m-d")."' AND card_using='$tno' 
	AND credit='$amount'");
		
	if(!$chk_duplicate_txn2){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	if(numRows($chk_duplicate_txn)>0 || numRows($chk_duplicate_txn2)>0)
	{
		AlertError("Error: Duplicate recharge request !");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
}

//// HAPPAY DUPLICATE RECHARGE ////

$check_trip = Qry($conn,"SELECT t.id,t.tno,t.branch,t.cash,t.driver_code,t.trip_no,d.name as driver_name,d.mobile,d.mobile2 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
WHERE t.id='$trip_id'");

if(!$check_trip){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertError("Trip not found..");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$driver_name = $row_trip['driver_name'];
$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];
$mobile1 = $row_trip['mobile'];
$mobile2 = $row_trip['mobile2'];

$nrr = $tno."/".$driver_name." : ".$narration;

if($adv_trans_type=='CASH')
{
	$Get_Cash_With_Happay_Branches = Qry($conn,"SELECT id FROM dairy.happay_cash_and_happay_branches WHERE branch='$branch'");
	
	if(!$Get_Cash_With_Happay_Branches){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}

	if($happay_active=="1" AND numRows($Get_Cash_With_Happay_Branches)==0)
	{
		AlertError("Happay-card is active. Cash not allowed !!");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
}
else if($adv_trans_type=='HAPPAY')
{
	if($happay_active!="1" || $happay_card_no=='NA')
	{
		AlertError("Happay-card is not active.");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
}
else
{
	AlertError("Invalid txn type..");
	echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
	errorLog("Invalid Transaction Type.",$conn,$page_name,__LINE__);
	exit();
}

if($adv_trans_type=="HAPPAY")
{
	$GetHappay = Qry($conn,"SELECT card_no2,card_assigned,card_status as status FROM dairy.happay_card_inventory WHERE 
	veh_no='$card_kit_no'");

	if(!$GetHappay){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}

	if(numRows($GetHappay)==0)
	{
		AlertError("Happay-card not found..");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		errorLog("Card not found. VehNo: $card_kit_no.",$conn,$page_name,__LINE__);
		exit();
	}
	else if(numRows($GetHappay)>1)
	{
		AlertError("Multiple card found..");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		errorLog("Multiple Card found. VehNo: $card_kit_no.",$conn,$page_name,__LINE__);
		exit();
	}
	
	$rowHappay = fetchArray($GetHappay);
	
	if($rowHappay['card_no2']!=substr($happay_card_no,-4))
	{
		AlertError("Happay-card not mapped with vehicle..");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		errorLog("Card not mapped with vehicle. Branch: $branch, VehNo: $card_kit_no, CardNo: $happay_card_no",$conn,$page_name,__LINE__);
		exit();
	}
	
	if($rowHappay['card_assigned']!=1)
	{
		AlertError("Happay-card not assigned..");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		errorLog("Card not Assigned. Branch: $branch, VehNo: $card_kit_no, CardNo: $happay_card_no",$conn,$page_name,__LINE__);
		exit();
	}
	
	if($rowHappay['status']!=1)
	{
		AlertError("Happay-card is not active..");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		errorLog("Card is In-Active. Branch: $branch, VehNo: $card_kit_no, CardNo: $happay_card_no",$conn,$page_name,__LINE__);
		exit();
	}
	
	// $happay_card_balance = $rowBal['balance']+$amount;
	
	if($row_trip['cash']>0)
	{
		AlertError("Advance entry allowed one time only ! Contact supervisor.");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	else
	{
		if($row_trip['branch']!=$branch)
		{
			AlertError("Only trip branch: $row_trip[branch] can give advance..");
			echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
			exit();
		}
	}

	$Chk_happay_main_bal = Qry($conn,"SELECT balance,api_bal FROM dairy.happay_main_balance WHERE company='$company'");
	
	if(!$Chk_happay_main_bal){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	if(numRows($Chk_happay_main_bal)==0)
	{
		AlertError("Balance not found..");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		errorLog("Happay Main Balance not found.",$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_main_bal = fetchArray($Chk_happay_main_bal);
	
	$happay_main_balance = $row_main_bal['api_bal'];
	$happay_main_balance_sys = $row_main_bal['balance'];
	
	if($happay_main_balance<$amount) 
	{
		AlertError("Insufficient fund in happay ac: $company !!");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	$trans_type="HAPPAY";
	
	$updated_main_balance = $happay_main_balance_sys - $amount;
}	
else
{
	$check_bal=Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");

	if(!$check_bal){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}

	if(numRows($check_bal)==0)
	{
		AlertError("Cash balance not found..");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}

	$row_bal = fetchArray($check_bal);

	if($company=='RRPL' && $row_bal['balance']>=$amount)
	{
	}
	else if($company=='RAMAN_ROADWAYS' && $row_bal['balance2']>=$amount)
	{
	}
	else
	{
		AlertError("Insufficient fund in company: $company !!");
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	$trans_type="CASH";
}
	
	$get_vou_id = GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
	
	if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id==""){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	$tids = $get_vou_id;
	
	$trans_id_Qry = GetTxnId_eDiary($conn,$trans_type);
	
	if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	$trans_id = $trans_id_Qry;
	
StartCommit($conn);
$flag = true;
		
$insert_cash_diary=Qry($conn,"INSERT INTO dairy.cash (trip_id,trans_id,trans_type,tno,vou_no,amount,date,narration,branch,branch_user,
timestamp) VALUES ('$trip_id','$trans_id','$trans_type','$tno','$tids','$amount','$date','$narration','$branch','$_SESSION[user_code]',
'$timestamp')");

if(!$insert_cash_diary){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($trans_type=="CASH")
{
	$check_bal=Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");

	if(!$check_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($check_bal)==0){
		$flag = false;
		errorLog("Branch not found.",$conn,$page_name,__LINE__);
	}

	$row_bal=fetchArray($check_bal);

	if($company=='RRPL' && $row_bal['balance']>=$amount)
	{
		$newbal=$row_bal['balance'] - $amount;
		$debit='debit';			
		$balance='balance';
		
	}
	else if($company=='RAMAN_ROADWAYS' && $row_bal['balance2']>=$amount)
	{
		$newbal=$row_bal['balance2'] - $amount;
		$debit='debit2';			
		$balance='balance2';
	}
	else
	{
		$flag = false;
		errorLog("INSUFFICIENT Balance in Company : $company.",$conn,$page_name,__LINE__);
	}
	
	$query_update = Qry($conn,"update user set `$balance`='$newbal' where username='$branch'");
	
	if(!$query_update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_cashbook=Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$debit`,`$balance`,timestamp) 
	VALUES ('$branch','$date','$date','$company','$tids','Truck_Voucher','$nrr','$amount','$newbal','$timestamp')");

	if(!$update_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
}
else if($trans_type=="HAPPAY") 
{
	$insert_happay_trans=Qry($conn,"INSERT INTO dairy.happay_card_transactions(card_no,card_using,driver_code,narration,trans_id,trans_type,credit,
	branch,date,timestamp) VALUES ('$tno','$card_kit_no','$driver_code','$other_Card_narration','$trans_id','Wallet Credit','$amount',
	'$branch','$date','$timestamp')");
	
	if(!$insert_happay_trans){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	// $update_happay_bal=Qry($conn,"UPDATE happay_live_balance SET balance='$happay_card_balance' WHERE tno='$tno'");
	// if(!$update_happay_bal)
	// {
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// }
	
	$update_happay_main_bal=Qry($conn,"UPDATE dairy.happay_main_balance SET balance=balance-'$amount' WHERE company='$company'");
	
	if(!$update_happay_main_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$flag = false;
	errorLog("Error : Invalid Transaction Type.",$conn,$page_name,__LINE__);
}

$insert_vou=Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,dest,timestamp) VALUES 
('$branch','$_SESSION[user_code]','$company','$tids','$date','$date','$tno','$driver_name','$amount','$trans_type','$narration','$timestamp')");	
		
if(!$insert_vou){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_trip=Qry($conn,"UPDATE dairy.trip SET cash=cash+'$amount' WHERE id='$trip_id'");

if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$select_amount=Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_amount = fetchArray($select_amount);
$hold_amt = $row_amount['amount_hold']+$amount;
$driver_balance_id = $row_amount['id'];

$insert_book=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,credit,balance,date,branch,timestamp) 
VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','CASH_ADV','$amount','$hold_amt','$date','$branch','$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+$amount WHERE id='$driver_balance_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	if($trans_type=='HAPPAY'){
		$msg_text = "आपके एटीएम कार्ड में $amount रुपए का रिचार्ज $branch ब्रांच द्वारा  दिनांक  $date को किया गया हैं\nकिसी प्रकार की सहायता के लिए  9016962404 पर संपर्क करे।";
	}
	else{
		$msg_text = "आपको $branch  ब्रांच ने दिनांक  $date  को  $amount  रुपए नकद दिए हैं\nकिसी प्रकार की सहायता के लिए  9016962404 पर संपर्क करे।";
	}
	
	if(strlen($mobile2)==10)
	{
		$adv_msg_mobile = $mobile2;
	}
	else if(strlen($mobile1)==10)
	{
		$adv_msg_mobile = $mobile1;
	} 
	
	// SendAdvMsg($adv_msg_mobile,$tno.": ".$msg_text);
	
	$adv_msg_date = date("d/m/y",strtotime($date));
	
	if(strlen($adv_msg_mobile)==10 AND $trans_type!='HAPPAY')
	{
		MsgAdvDiary($adv_msg_mobile,$tno,$trans_type,$amount,$branch,$adv_msg_date);
	}
	
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error..");
	echo "<script>$('#cash_adv_button').attr('disabled',false);</script>";
	exit();
}

	
	
echo "<script>
		$('#cash_adv_button').attr('disabled',false);
		$('#CashAdvForm')[0].reset();
		document.getElementById('hide_cash').click();
		TripAdv('#$trip_id');
		LoadDriverBalance('$driver_code');
		$('#loadicon').fadeOut('slow');	
		
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: 'Success ! Advance entry.',
			showConfirmButton: false,
			timer: 2000
		})
	</script>";
exit();
?>