<?php
require("./connect.php");

$id = escapeString($conn,$_POST['id']);
$type = escapeString($conn,$_POST['type']);

if($type=='Collect_Freight')
{
	$get_lr = Qry($conn,"SELECT lr_type,lr_type2,freight FROM dairy.trip WHERE id='$id'");

	if(!$get_lr){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error..');window.location.href='./logout.php';</script>";
		exit();
	}
			
	$row_lr = fetchArray($get_lr);	
			
	$lrno1 = $row_lr['lr_type'];	
	$bhada = $row_lr['freight'];	
	$lr_type22 = $row_lr['lr_type2'];	
		
	$get_freight_data = Qry($conn,"SELECT DISTINCT bilty_no FROM dairy.bilty_book 
	WHERE FIND_IN_SET(bilty_no,(SELECT lr_type FROM dairy.trip WHERE id='$id' AND lr_type2>0)) AND trans_id LIKE 'FRTADV%'");

	if(!$get_freight_data){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error..');window.location.href='./logout.php';</script>";
		exit();
	}

	if(numRows($get_freight_data)>0)
	{
		echo "<label>Rcv Against LR <font color='red'>*</font></label>
			<select name='lr_no' class='form-control' required='required'>
				<option value=''>--SELECT LR--</option>";
			while($row=fetchArray($get_freight_data))
			{
				echo "<option value='$row[bilty_no]'>$row[bilty_no]</option>";  
			}
		echo "</select>";	
			
		echo "<script>
			$('#freight2_button').attr('disabled',false);
		</script>";	
	}
	else if((strpos($lrno1,"OLR")!==false) AND $bhada>0)
	{
		echo "<label>Rcv Against LR <font color='red'>*</font></label>
			<select style='font-size:12px' name='lr_no' class='form-control' required='required'>
				<option style='font-size:12px' value=''>--SELECT LR--</option>";
			$comma_sep = explode(',',$lrno1);
			foreach($comma_sep as $comma_sep1)
			{
				echo "<option style='font-size:12px' value='$comma_sep1'>$comma_sep1</option>";
			}
		echo "</select>";
		
		echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	}
	else
	{
		echo "<label>Rcv Against LR <font color='red'>*</font></label>
			<select style='font-size:12px' name='lr_no' class='form-control' required='required'>
				<option style='font-size:12px' value=''>--No LR Found--</option>
			</select>";	
			
		echo "<script>$('#freight2_button').attr('disabled',true);</script>";	
	}
}
else
{
	$get_lr = Qry($conn,"SELECT lr_type FROM dairy.trip WHERE id='$id' AND lr_type2>0");
	
	if(!$get_lr){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error..');window.location.href='./logout.php';</script>";
		exit();
	}

	if(numRows($get_lr)>0)
	{
		$row_lr=fetchArray($get_lr);
		
		$myArray = explode(',', $row_lr['lr_type']);
		
		echo "<label>Rcv Against LR <font color='red'>*</font></label>
			<select style='font-size:12px' name='lr_no' class='form-control' required='required'>
				<option style='font-size:12px' value=''>--SELECT LR--</option>";
				foreach($myArray as $my_Array)
				{
					echo "<option style='font-size:12px' value='$my_Array'>$my_Array</option>";  
				}
			
		echo "</select>";	
		echo "<script>$('#freight_button').attr('disabled',false);</script>";	
	}
	else
	{
		echo "<label>Rcv Against LR <font color='red'>*</font></label>
			<select style='font-size:12px' name='lr_no' class='form-control' required='required'>
				<option style='font-size:12px' value=''>--No LR Found--</option>
			</select>";	
		echo "<script>$('#freight_button').attr('disabled',true);</script>";	
	}
}

echo "<script>
	$('#loadicon').fadeOut('slow');
</script>";
?>