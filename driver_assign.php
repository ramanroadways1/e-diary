<?php
require('./connect.php');

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$tno = escapeString($conn,($_SESSION['diary']));

echo "<script>$('#add_driver_button').attr('disabled',true);</script>";
	
$driver_id = $_SESSION['driver_verify_driver_id'];

if(empty($driver_id) || $driver_id==0)
{
	AlertError("Driver not found !");
	exit();
}

$fetch_driver_data = Qry($conn,"SELECT d.name,d.active,d.mobile,d.code,d.dummy_driver
FROM dairy.driver AS d 
WHERE d.id='$driver_id'");

if(!$fetch_driver_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	exit();
}

if(numRows($fetch_driver_data)==0)
{
	AlertError("Driver details not found !");
	exit();
}

$row_driver = fetchArray($fetch_driver_data); 

if($row_driver['active']!='0')
{
	AlertError("Driver active on another vehicle !");
	exit();
}

$driver_code = $row_driver['code'];
$driver_name = $row_driver['name'];
$mobile = $row_driver['mobile'];
$is_dummy_driver = $row_driver['dummy_driver'];

if(strlen($mobile)!=10){
	AlertError("Invalid mobile number found !");
	exit();
}

$chk_veh_data = Qry($conn,"SELECT driver_code,diesel_tank_cap,comp FROM dairy.own_truck WHERE tno='$tno'");

if(!$chk_veh_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	exit();
}

if(numRows($chk_veh_data)==0)
{
	AlertError("Vehicle not found !");
	exit();
}

$row_vehicle = fetchArray($chk_veh_data);

$company = $row_vehicle['comp'];

if($row_vehicle['driver_code']!="0" AND $row_vehicle['driver_code']!='')
{
	AlertError("Another driver active on: $tno !");
	exit();
}

$down_check=Qry($conn,"SELECT tno,down,status,amount_hold FROM dairy.driver_up WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$down_check){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	exit();
}
			
if(numRows($down_check)>0)
{
	$row_down = fetchArray($down_check);
		
	if($row_down['down']==0)
	{
		errorLog("DRIVER NOT DOWN PROPERLY. Code : $driver_code. Active On: $row_down[tno].",$conn,$page_name,__LINE__);
		AlertError("Driver not down completely !");
		exit();
	}
	
	$down_type = $row_down['status'];
	$opening_balance = $row_down['amount_hold'];
}
else
{
	$down_type="NEW";
	$opening_balance="0";
}

$salary_master = escapeString($conn,($_POST['salary_master']));
$salary_date_pending = escapeString($conn,($_POST['salary_date_pending']));
$left_reason = escapeString($conn,($_POST['left_reason']));
$on_duty_on = escapeString($conn,($_POST['on_duty_on']));
// $diesel_left = escapeString($conn,($_POST['diesel_left']));

$on_duty_on = "TODAY";
// old da salary code temp. on hold

$old_da_sal="0";

// if($diesel_left<=0)
// {
	// AlertError("Please check diesel Qty !");
	// exit();
// }

// if($diesel_left>$row_vehicle['diesel_tank_cap'])
// {
	// AlertError("Invalid diesel Qty entered !");
	// exit();
// }

/*
if($on_duty_on=='BACK')
{
	$old_da_sal="1";
	
	$on_duty_date=escapeString($conn,$_POST['driver_on_duty_date']);
	
	if(escapeString($conn,$_POST['da_diff'])==1)
	{
		$old_da_from = escapeString($conn,$_POST['diff_da_from']);
		$old_da_to = escapeString($conn,$_POST['diff_da_to']);
		
		if($old_da_to>=date("Y-m-d") || $old_da_from<$on_duty_date || $old_da_to<=$old_da_from)
		{
			echo "<script>
				alert('Error : Please check DA dates.');
				$('#add_driver_button').attr('disabled',false);
			</script>";
			exit();
		}
		
		$pay_back_dates_da="1";
	}
	else
	{
		$old_da_from = "";
		$old_da_to = "";
		$pay_back_dates_da="0";
	}
	
	if(escapeString($conn,$_POST['salary_diff'])==1)
	{
		$old_salary_from = escapeString($conn,$_POST['diff_sal_from']);
		$old_salary_to = escapeString($conn,$_POST['diff_sal_to']);
		
		if($salary_date_pending!=0 || $salary_date_pending!='')
		{
			if($old_salary_to>=$salary_date_pending || $old_salary_from<=$salary_date_pending)
			{
				echo "<script>
					alert('Error : Please check salary dates. Less than SALARY Pending From.');
					$('#add_driver_button').attr('disabled',false);
				</script>";
				exit();
			}
		}
			
		if($old_salary_to<=$old_salary_from || $old_salary_from<$on_duty_date || $old_salary_to>=date("Y-m-d"))
		{
			echo "<script>
				alert('Error : Please check salary dates.');
				$('#add_driver_button').attr('disabled',false);
			</script>";
			exit();
		}
		$pay_back_dates_salary="1";
	}
	else
	{
		$old_salary_from = "";
		$old_salary_to = "";
		$pay_back_dates_salary="0";
	}
	
	if($pay_back_dates_salary==0 AND $pay_back_dates_da==0)
	{
		echo "<script>
				alert('Error : Please check DA and SALARY option both selected as NO.');
				$('#add_driver_button').attr('disabled',false);
			</script>";
		exit();
	}
}
else
{
	$old_da_sal="0";
}

*/

if($down_type!='NEW' AND $down_type!='0' AND $down_type!='1' AND $down_type!='2')
{
	errorLog("Invalid down type: $down_type. Driver_Code : $driver_code. Truck_No : $tno.",$conn,$page_name,__LINE__);
	AlertError("Invalid data poseted !");
	exit();
}

if(($opening_balance=='' || empty($opening_balance)) AND $opening_balance!=0)
{
	errorLog("Opening Balance not found. Code : $driver_code. Truck No : $tno.",$conn,$page_name,__LINE__);
	AlertError("Opening Balance not found !");		
	echo "<script>$('#add_driver_button').attr('disabled',false);</script>";
	exit();
}

if($salary_master=='' || empty($salary_master))
{
	errorLog("SALARY not found. Code : $driver_code. Truck No : $tno.",$conn,$page_name,__LINE__);
	AlertError("Salary not found !");					
	exit();
}

$salary_amount = strtok($salary_master,'_');
$salary_type = substr($salary_master, strpos($salary_master, "_") + 1);  

if($salary_amount=='' || $salary_type=='')
{
	errorLog("SALARY not found. Code : $driver_code. Truck No : $tno.",$conn,$page_name,__LINE__);
	AlertError("Salary not found !");		
	exit();
}

$fetch_ext_up_driver_from_truck=Qry($conn,"SELECT code FROM dairy.driver_up WHERE tno='$tno' AND down=0 ORDER BY id DESC LIMIT 1");

if(!$fetch_ext_up_driver_from_truck){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");		
	exit();
}

if(numRows($fetch_ext_up_driver_from_truck)>0)
{
	$row_ext_up_code=fetchArray($fetch_ext_up_driver_from_truck);
	
	errorLog("Driver not down Properly. Code : $row_ext_up_code[code]. Truck No : $tno.",$conn,$page_name,__LINE__);
	AlertError("Previous driver not down properly !");	
	exit();	
}

StartCommit($conn);
$flag = true;
	
/* On duty back dated code

if($on_duty_on=='BACK' AND $is_dummy_driver==0)
{
	if($pay_back_dates_da==1)
	{
				$insert_old_da=Qry($conn,"INSERT INTO dairy.da_book(driver_code,tno,from_date,to_date,branch,date,on_duty_da,narration,timestamp) 
				VALUES ('$driver_code','$tno','$old_da_from','$old_da_to','$branch','$date','1','ON_DUTY_DA_DIFF','$timestamp')");
				
				if(!$insert_old_da){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$da_insert_id = getInsertID($conn);
			}
			else
			{
				$da_insert_id = "0";
			}
			
			if($pay_back_dates_salary==1)
			{
				$insert_old_salary=Qry($conn,"INSERT INTO dairy.salary(driver_code,from_date,to_date,tno,branch,date,on_duty_sal,narration,
				timestamp) VALUES ('$driver_code','$old_salary_from','$old_salary_to','$tno','$branch','$date','1','ON_DUTY_SAL_DIFF','$timestamp')");
				
				if(!$insert_old_salary){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$salary_insert_id = getInsertID($conn);
			}
			else
			{
				$salary_insert_id="0";
			}
			
	$insert_old_sal_and_da=Qry($conn,"INSERT INTO dairy.driver_on_duty(tno,driver_code,up_on,up_branch,on_duty,old_da,da_table_id,da_from,
		da_to,old_salary,salary_table_id,salary_from,salary_to,timestamp) VALUES ('$tno','$driver_code','$date','$branch','$on_duty_date',
		'$pay_back_dates_da','$da_insert_id','$old_da_from','$old_da_to','$pay_back_dates_salary','$salary_insert_id','$old_salary_from',
		'$old_salary_to','$timestamp')");	
		
			if(!$insert_old_sal_and_da){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
}
*/
		
if($down_type=='1' || $down_type=='2')
{
	if($down_type=='2')
	{
		$log_left_up_driver=Qry($conn,"INSERT INTO dairy.left_up_log(code,reason,salary_date,opening,branch,timestamp) VALUES 
		('$driver_code','$left_reason','$salary_date_pending','$opening_balance','$branch','$timestamp')");	
				
		if(!$log_left_up_driver){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$sal_desc="LEFT_COMP_UP";
		$da_desc="LEFT_COMP_UP";
	}
	else
	{
		$sal_desc="ON_LEAVE_UP";
		$da_desc="ON_LEAVE_UP";
	}
			
	if($is_dummy_driver==0)
	{
		$insert_salary_date = Qry($conn,"INSERT INTO dairy.salary (driver_code,to_date,branch,date,narration,timestamp) VALUES 
		('$driver_code','$date','$branch','$date','$sal_desc','$timestamp')");
			
		if(!$insert_salary_date){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$insert_da_date = Qry($conn,"INSERT INTO dairy.da_book (driver_code,to_date,branch,date,narration,timestamp) VALUES 
		('$driver_code','$date','$branch','$date','$da_desc','$timestamp')");
			
		if(!$insert_da_date){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
		
$update_own_truck_db=Qry($conn,"UPDATE dairy.own_truck SET driver_code='$driver_code' WHERE tno='$tno'");

if(!$update_own_truck_db){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($down_type=='NEW')
{
	$update_driver_active=Qry($conn,"UPDATE dairy.driver SET active='1',last_salary='$salary_date_pending' WHERE 
	code='$driver_code' AND mobile='$mobile'");
		
	if(!$update_driver_active){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$log_new_driver=Qry($conn,"INSERT INTO dairy.new_driver_log(code,salary_date,opening,branch,timestamp) VALUES 
	('$driver_code','$salary_date_pending','$opening_balance','$branch','$timestamp')");
			
	if(!$log_new_driver){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$update_driver_active=Qry($conn,"UPDATE dairy.driver SET active='1' WHERE code='$driver_code' AND mobile='$mobile'");

	if(!$update_driver_active){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}			
}
	
if($is_dummy_driver==0)
{	
	$update_driver_up_data=Qry($conn,"INSERT INTO dairy.driver_up (code,tno,up,salary_amount,salary_type,branch,amount_hold,old_da_sal,
	timestamp) VALUES ('$driver_code','$tno','$date','$salary_amount','$salary_type','$branch','$opening_balance','$old_da_sal',
	'$timestamp')");
}
else
{
	$update_driver_up_data=Qry($conn,"INSERT INTO dairy.driver_up (code,tno,up,salary_amount,salary_type,salary_approval,branch,
	amount_hold,old_da_sal,timestamp) VALUES ('$driver_code','$tno','$date','$salary_amount','$salary_type','1','$branch',
	'$opening_balance','0','$timestamp')");	
}	

if(!$update_driver_up_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
$driver_book=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,desct,credit,balance,date,branch,timestamp) VALUES 
('$driver_code','$tno','DRIVER_UP','$opening_balance','$opening_balance','$date','$branch','$timestamp')");

if(!$driver_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_up_id = getInsertID($conn);
		
$chk_driver_active = Qry($conn,"SELECT driver_code FROM dairy.trip WHERE tno='$tno'");

if(!$chk_driver_active){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
$row_driver_active = fetchArray($chk_driver_active);

$active_driver_in_trip = $row_driver_active['driver_code'];
		
if($active_driver_in_trip==0 || $active_driver_in_trip=="")
{
	$update_driver1 = Qry($conn,"UPDATE dairy.trip SET driver_code='$driver_code' WHERE tno='$tno'");
	
	if(!$update_driver1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_new_trip_no = Qry($conn,"UPDATE dairy.driver_book SET trip_no=(SELECT DISTINCT trip_no FROM dairy.trip WHERE tno='$tno') 
	WHERE id='$insert_up_id'");
	
	if(!$update_new_trip_no){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
		
$delete_active_truck=Qry($conn,"DELETE FROM own_truck WHERE tno='$tno'");

if(!$delete_active_truck){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{
	unset($_SESSION['driver_verify_otp']);
	unset($_SESSION['driver_verify_otp_by_pass']);
	unset($_SESSION['driver_verify_driver_id']);
	
	MySQLCommit($conn);
	closeConnection($conn);
	
	// ----------------------------- TO change mobile number in happay card ---------------- //

	$requestId = mt_rand().$tno;

	$json_update = array(
	"requestId"=>$requestId,
	"userId"=>$tno,
	"mobileNo"=>$mobile
	);

	$json_update = json_encode($json_update);

	$result_update = HappayAPI("auth/v1/updateuser/",$json_update,$company);

	$result_decode = json_decode($result_update, true);

	$update_happay_user = Qry($conn,"UPDATE dairy.happay_users SET mobile='$mobile' WHERE veh_no='$tno'");	
		
	if(!$update_happay_user){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	// ----------------------------- TO change mobile number in happay card ---------------- //

	Redirect("Driver assigned successfully.","./");
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");	
	exit();
}
?>