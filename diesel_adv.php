<?php
require_once("./connect.php");
$timestamp = date("Y-m-d H:i:s"); 

$trip_id=escapeString($conn,strtoupper($_POST['trip_id']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertError("Trip not found..");
	errorLog("Trip id not found. id: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

// require_once("./_check_fix_lane.php");
require_once("./check_cache.php");

$check_trip = Qry($conn,"SELECT t.id,t.tno,t.driver_code,t.fix_lane,t.trip_no,d.name as driver_name,d.mobile,d.mobile2 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
WHERE t.id='$trip_id'");

if(!$check_trip){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertError("Trip not found..");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$chk_diesel_rights = Qry($conn,"SELECT login_type FROM emp_attendance WHERE code='$_SESSION[user_code]'");

if(!$chk_diesel_rights){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_diesel_rights)==0)
{
	AlertError("Employee not found..");
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

$row_emp = fetchArray($chk_diesel_rights);

if($row_emp['login_type']=='6')
{
	AlertError("You do not have right to give fuel !");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

$get_diesel_rate_max = Qry($conn,"SELECT qty,rate,amount FROM _diesel_max_rate WHERE market_own='OWN'");

if(!$get_diesel_rate_max){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}
				
$row_dsl_max_rate = fetchArray($get_diesel_rate_max);

$dsl_max_rate = $row_dsl_max_rate['rate'];
$dsl_max_amount = $row_dsl_max_rate['amount'];
$dsl_max_qty = $row_dsl_max_rate['qty'];

$tno = escapeString($conn,strtoupper($_POST['tno']));

// if($row_trip['fix_lane']!='0')
// {
	// $get_fix_qty = Qry($conn,"SELECT diesel_left_fix FROM dairy.own_truck WHERE tno='$tno'");

	// if(!$get_fix_qty){
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// exit();
	// }

	// if(numRows($get_fix_qty)==0)
	// {
		// echo "<script>
			// alert('Vehicle not found..');
			// window.location.href='./';
		// </script>";
		// exit();
	// }
	
	// $row_fix_qty = fetchArray($get_fix_qty);
	// $dsl_max_qty = $row_fix_qty['diesel_left_fix'];
// }

$diesel_key = $tno."_".mt_rand().date("His");

if($tno!=$_SESSION['diary'])
{
	Redirect("Please log in again..","./logout.php");
	exit();
}

$chk_fix_txns = Qry($conn,"SELECT id FROM dairy.fix_txn_pending WHERE tno='$tno'");

if(!$chk_fix_txns){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_fix_txns)>0)
{
	AlertError('Fix Txns are in process !');
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

$amount = escapeString($conn,$_POST['amount']);
$rate = escapeString($conn,$_POST['rate']);
$qty = escapeString($conn,$_POST['qty']);

$get_dsl_left_db = Qry($conn,"SELECT diesel_tank_cap,diesel_left,diesel_trip_id FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_dsl_left_db){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_dsl_left_db)==0)
{
	errorLog("Vehicle record not found. VehicleNo: $tno.",$conn,$page_name,__LINE__);
	echo "<script>window.location.href='/';</script>";
	exit();
}
				
$row_dsl_left_db = fetchArray($get_dsl_left_db);
	
if($row_dsl_left_db['diesel_trip_id']!='-1')
{
	$total_new_diesel = $qty + $row_dsl_left_db['diesel_left'] - 20;
	
	// if($total_new_diesel > $row_dsl_left_db['diesel_tank_cap'])
	// {
		// AlertError("Error : entered qty exceeds tank capacity !<br>Fuel available: $row_dsl_left_db[diesel_left].<br>Tank capacity is: $row_dsl_left_db[diesel_tank_cap].");
		// echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		// exit();
	// }
}

if($rate!=0 AND $rate>$dsl_max_rate)
{
	AlertError("Error : Rate is out of Limit.");
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

if($qty<=0)
{
	AlertError("Error : Invalid Qty !");
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

if($qty>$dsl_max_qty)
{
	AlertError("Error : Qty is out of Limit !<br>Max value is: $dsl_max_qty.");
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

if($amount!=0 AND $amount>$dsl_max_amount)
{
	AlertError("Error : Amount is out of Limit.");
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

$rilpre = escapeString($conn,strtoupper($_POST['rilpre']));
$pump_card = escapeString($conn,strtoupper($_POST['pump_card']));

if($pump_card=='CARD')
{
	
$card=escapeString($conn,strtoupper($_POST['card_no']));
$type='CARD';
$phy_card_no=escapeString($conn,strtoupper($_POST['phy_card_no']));

if(empty($card)){
	AlertError("Error : card not found.");
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

$fuel_company = escapeString($conn,strtoupper($_POST['fuel_company']));
$mobile_no="";

$diesel_given_by="कार्ड";
$consumer_pump = "0";
$total_qty = $qty;
$total_amount = $amount;

}
else if($pump_card=='OTP')
{
$card_id="0";
$card=escapeString($conn,strtoupper($_POST['mobile_no']));
$mobile_no=escapeString($conn,strtoupper($_POST['mobile_no']));
$phy_card_no=$mobile_no;
$type="OTP";
$fuel_company=escapeString($conn,strtoupper($_POST['fuel_company']));
$diesel_given_by="OTP";
$consumer_pump = "0";
$total_qty = $qty;
$total_amount = $amount;
}
else
{
$consumer_pump = "0";	
$mobile_no="";	
$card_id="0";
$card=escapeString($conn,strtoupper($_POST['pump_name']));	
$card=strtok($card,'-');
$phy_card_no=$card;
$type='PUMP';
$fuel_company=escapeString($conn,strtoupper($_POST['pump_company']));

$total_qty = $qty;
$total_amount = $amount;

if($qty<=0)
{
	AlertError("Error : invalid Qty.");
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

$check_consumer = Qry($conn,"SELECT id FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no'");

if(!$check_consumer){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_consumer)>0)
{
	$check_stock = Qry($conn,"SELECT SUM(balance) as balance FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no'");
	
	if(!$check_stock){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	$row_balance = fetchArray($check_stock);
	
	if($row_balance['balance']<$qty)
	{
		AlertError("Error : Diesel not in Stock.");
		echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		exit();
	}
}

$get_qty = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no' AND balance>0");

if(!$get_qty){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

$numRowsQty = numRows($get_qty);

if($numRowsQty==1)
{
	$row_Qty = fetchArray($get_qty);
	
	if($row_Qty['balance']<$qty)
	{
		AlertError("Error : Diesel not in Stock. Code: 01");
		echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	$remainQty = sprintf("%.2f",$row_Qty['balance']-$qty);
	$StockId = $row_Qty['id'];
	$PurchaseId = $row_Qty['purchaseid'];
	
	$rate = $row_Qty['rate'];
	$amount = round($qty*$rate);
	$consumer_pump = "1";
	
	$total_qty = $qty;
	$total_amount = $amount;
}
else if($numRowsQty==2)
{
	$consumer_pump = "1";
	
	$get_first_pump = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no' AND balance>0 
	ORDER BY id ASC LIMIT 1");
	
	if(!$get_first_pump){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		exit();
	}

	$get_second_pump = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no' AND balance>0 
	ORDER BY id DESC LIMIT 1");
	
	if(!$get_second_pump){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		exit();
	}

	$rowFirstPump = fetchArray($get_first_pump);
	$rowSecondPump = fetchArray($get_second_pump);
	
	if(($rowFirstPump['balance']+$rowSecondPump['balance'])<$qty)
	{
		AlertError("Error : Diesel not in Stock. Code: 02");
		echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	if($rowFirstPump['balance']>=$qty)
	{
		$split_entry="0";
		$rate = $rowFirstPump['rate'];
		$amount = round($qty*$rate);
		$remainQty = sprintf("%.2f",$rowFirstPump['balance']-$qty);
		$StockId = $rowFirstPump['id'];
		$PurchaseId = $rowFirstPump['purchaseid'];
		
		$total_qty = $qty;
		$total_amount = $amount;
	}
	else
	{
		$split_entry="1";
		
		$rate = $rowFirstPump['rate'];
		$rate2 = $rowSecondPump['rate'];
		$qty1 = $rowFirstPump['balance'];
		$qty2 = sprintf("%.2f",$qty-$qty1);
		$amount = round($rate * $qty1);
		$amount2 = round($rate2 * $qty2);
		$StockId = $rowFirstPump['id'];
		$StockId2 = $rowSecondPump['id'];
		$remainQty = 0;
		$remainQty2 = sprintf("%.2f",$rowSecondPump['balance']-$qty2);
		$PurchaseId = $rowFirstPump['purchaseid'];
		$PurchaseId2 = $rowSecondPump['purchaseid'];
		
		$total_qty = $qty1+$qty2;
		$total_amount = $amount+$amount2;
	}
	
}
else if($numRowsQty>2)
{
	AlertError("Error : Multiple diesel stock found.");
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

$GetPumpName = Qry($conn,"SELECT name FROM dairy.diesel_pump_own WHERE code='$phy_card_no'");
	
if(!$GetPumpName){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($GetPumpName)==0){
	errorLog("PUMP not found. Code: $phy_card_no.",$conn,$page_name,__LINE__);
	Redirect("PUMP not found.","./");
	exit();
}
	
$rowPumpName = fetchArray($GetPumpName);

$diesel_given_by=$rowPumpName['name'];	
}

if($rilpre=="1" AND $pump_card=='CARD')
{
	$check_stock = Qry($conn,"SELECT stockid,balance FROM diesel_api.dsl_ril_stock WHERE cardno='$phy_card_no' ORDER BY id DESC LIMIT 1");
	
	if(!$check_stock){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	if(numRows($check_stock)==0){
		errorLog("Ril Stock not found. Card_no : $phy_card_no",$conn,$page_name,__LINE__);
		AlertError("Error : RIL stock not found.");
		echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		exit();
	}
	
	$row_ril_stock = fetchArray($check_stock);
	
	$ril_stockid = $row_ril_stock['stockid'];
	
	if($row_ril_stock['balance']<$amount)
	{
		AlertError("Error : Card not in Stock. Available balance is: $row_ril_stock[balance].");
		echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
		exit();
	}		
}
else
{
	$ril_stockid="";
}

$date=date("Y-m-d");
$bill_no=escapeString($conn,strtoupper($_POST['bill_no']));

$driver_name = $row_trip['driver_name'];
$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];
$mobile1 = $row_trip['mobile'];
$mobile2 = $row_trip['mobile2'];

$trans_id_Qry = GetTxnId_eDiary($conn,"DSL");
	
if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
{
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}
	
$trans_id = $trans_id_Qry;
	
StartCommit($conn);
$flag = true;
	
if($consumer_pump=="1")
{
	if($numRowsQty==1)
	{
		$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel (unq_id,trip_id,trip_no,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
		timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trip_no','$trans_id','$tno','$rate','$qty','$amount','$date',
		'$fuel_company-$phy_card_no','$branch','$_SESSION[user_code]','$timestamp','$PurchaseId')");

		if(!$insert_diesel_diary){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}	

		$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
		narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','$bill_no','$tno','$amount','$type','$card','$phy_card_no',
		'$fuel_company','$fuel_company-$phy_card_no','$branch','$date','$timestamp','$mobile_no')");

		if(!$insert_diesel_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='$remainQty' WHERE id='$StockId'");

		if(!$updateStock1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		if($split_entry=="0")
		{
			$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trip_no,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
			timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trip_no','$trans_id','$tno','$rate','$qty','$amount','$date','$fuel_company-$phy_card_no',
			'$branch','$_SESSION[user_code]','$timestamp','$PurchaseId')");

			if(!$insert_diesel_diary)
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
			narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','$bill_no','$tno','$amount','$type','$card','$phy_card_no',
			'$fuel_company','$fuel_company-$phy_card_no','$branch','$date','$timestamp','$mobile_no')");

			if(!$insert_diesel_entry){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='$remainQty' WHERE id='$StockId'");

			if(!$updateStock1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	
		}
		else
		{
			$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trip_no,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
			timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trip_no','$trans_id','$tno','$rate','$qty1','$amount','$date','$fuel_company-$phy_card_no',
			'$branch','$_SESSION[user_code]','$timestamp','$PurchaseId')");

			if(!$insert_diesel_diary)
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry(unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
			narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','$bill_no','$tno','$amount','$type','$card','$phy_card_no',
			'$fuel_company','$fuel_company-$phy_card_no','$branch','$date','$timestamp','$mobile_no')");

			if(!$insert_diesel_entry){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='0' WHERE id='$StockId'");

			if(!$updateStock1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_diary2=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trip_no,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
			timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trip_no','$trans_id','$tno','$rate2','$qty2','$amount2','$date',
			'$fuel_company-$phy_card_no','$branch','$_SESSION[user_code]','$timestamp','$PurchaseId2')");

			if(!$insert_diesel_diary2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_entry2=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
			narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','$bill_no','$tno','$amount2','$type','$card','$phy_card_no',
			'$fuel_company','$fuel_company-$phy_card_no','$branch','$date','$timestamp','$mobile_no')");

			if(!$insert_diesel_entry2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$updateStock2 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='$remainQty2' WHERE id='$StockId2'");

			if(!$updateStock2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}			
		}
	}	
}
else
{	
	$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel (trip_id,trip_no,trans_id,unq_id,tno,rate,qty,amount,date,narration,branch,branch_user,
	timestamp,ril_card,stockid) VALUES ('$trip_id','$trip_no','$trans_id','$diesel_key','$tno','$rate','$qty','$amount','$date','$fuel_company-$phy_card_no',
	'$branch','$_SESSION[user_code]','$timestamp','$rilpre','$ril_stockid')");

	if(!$insert_diesel_diary)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,narration,branch,
	date,dsl_mobileno,timestamp) VALUES ('$diesel_key','$bill_no','$tno','$amount','$type','$card','$phy_card_no','$fuel_company',
	'$fuel_company-$phy_card_no','$branch','$date','$mobile_no','$timestamp')");

	if(!$insert_diesel_entry)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$insert_id=getInsertID($conn);

	if($type=='CARD')
	{
		$card_live_bal=Qry($conn,"select balance from diesel_api.dsl_cards where cardno='$card'");
		if(!$card_live_bal){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($card_live_bal)==0){
			$flag = false;
			errorLog("Card not found.",$conn,$page_name,__LINE__);
		}
		
		$row_card_bal=fetchArray($card_live_bal); 
		$live_bal = $row_card_bal['balance']; 

		$qry_log=Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,
		dslcomp,dsltype,cardno,vehno,cardbal,amount,mobileno,reqid) VALUES 
		('$branch','SUCCESS','New Request Added','$timestamp','OWN','$tno','$fuel_company',
		'CARD','$card','$phy_card_no','$live_bal','$amount','$mobile_no','$insert_id')");
		
		if(!$qry_log){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
	}
}
				
$dateMsg = date("d/m/y",strtotime($date));

if($rilpre=="1" AND $pump_card=='CARD')
{
	$update_ril_stock = Qry($conn,"UPDATE diesel_api.dsl_ril_stock SET balance=balance-'$total_amount' WHERE 
	cardno='$phy_card_no' ORDER BY id DESC LIMIT 1");
			
	if(!$update_ril_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($row_trip['fix_lane']!='0')
{
	$minus_qty_left_fix = Qry($conn,"UPDATE dairy.own_truck SET diesel_left_fix = diesel_left_fix-'$total_qty' WHERE tno='$tno'");

	if(!$minus_qty_left_fix){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($row_dsl_left_db['diesel_trip_id']!='-1')
	{
		$minus_qty_left = Qry($conn,"UPDATE dairy.own_truck SET diesel_left = diesel_left+'$total_qty' WHERE tno='$tno'");

		if(!$minus_qty_left){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	$update_trip = Qry($conn,"UPDATE dairy.trip SET diesel=diesel+'$total_amount',fix_dsl_left=fix_dsl_left-'$total_qty',
	diesel_qty=ROUND(diesel_qty+'$total_qty',2) WHERE id='$trip_id'");

	if(!$update_trip){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$update_trip = Qry($conn,"UPDATE dairy.trip SET diesel=diesel+'$total_amount',diesel_qty=ROUND(diesel_qty+'$total_qty',2) WHERE id='$trip_id'");

	if(!$update_trip){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($row_dsl_left_db['diesel_trip_id']!='-1')
	{
		$minus_qty_left = Qry($conn,"UPDATE dairy.own_truck SET diesel_left = diesel_left+'$total_qty' WHERE tno='$tno'");

		if(!$minus_qty_left){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

if($flag)
{
	if(strlen($mobile2)==10)
	{
		$adv_msg_mobile = $mobile2;
	}
	else if(strlen($mobile1)==10)
	{
		$adv_msg_mobile = $mobile1;
	}
	
	if(strlen($adv_msg_mobile)==10)
	{
		MsgDieselDiary($adv_msg_mobile,$tno,$diesel_given_by,$total_qty,$branch,$dateMsg);
	}
	
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertError("Error while processing request..");
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}

echo "<script>
		$('#diesel_adv_button').attr('disabled',false);
		$('#DieselAdvForm')[0].reset();
		document.getElementById('hide_diesel').click();
		TripAdv('#$trip_id');
		$('#loadicon').fadeOut('slow');	
		
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: 'Success ! Diesel entry.',
			showConfirmButton: false,
			timer: 2000
		})
	</script>";
exit();
?>