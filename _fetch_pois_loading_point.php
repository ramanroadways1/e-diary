<?php
require_once './connect.php'; 

$timestamp = date("Y-m-d H:i:s");
$date = date("d-m-Y",strtotime(escapeString($conn,($_POST['date']))));
$veh_no = escapeString($conn,$_SESSION['diary']);
$start_date = $date."T00:00:01";
$end_date = $date."T23:59:59";
$Duration1= "2"; // in minutes
$lr_type = escapeString($conn,$_POST['lr_type']);
$vou_no_olr = escapeString($conn,$_POST['vou_no_olr']);
$from_id = escapeString($conn,$_POST['from_id']);

echo "<option style='font-size:12px !important' value=''>--start/loading point--</option>";
		
if($lr_type=='OWN' AND $vou_no_olr=='')
{
	AlertError("Voucher not found !");
	echo "<script>$('#loading_start_date').val('');$('#trip_loading_point').val('').selectpicker('refresh');</script>";
	exit();
}
		
if($lr_type=='OWN' AND explode("_",$vou_no_olr)[0]!='ATLOADING')
{
	$vou_id = explode("_",$vou_no_olr)[1];

	$chk_crossing = Qry($conn,"SELECT crossing FROM freight_form_lr WHERE id<'$vou_id' AND lrno = (SELECT lrno from freight_form_lr WHERE id='$vou_id')");
	
	if(!$chk_crossing){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error while processing request !");
		echo "<script>$('#loading_start_date').val('');$('#trip_loading_point').val('').selectpicker('refresh');</script>";
		exit();
	}
	
	if(numRows($chk_crossing) == 0)
	{
		$get_my_loading_points = Qry($conn,"SELECT label,_lat,_long,pincode FROM address_book_consignor WHERE from_id='$from_id' 
		AND consignor=(SELECT con1_id FROM freight_form_lr WHERE id='$vou_id')");

		if(!$get_my_loading_points){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			AlertError("Error while processing request !");
			echo "<script>$('#loading_start_date').val('');$('#trip_loading_point').val('').selectpicker('refresh');</script>";
			exit();
		}
				
		if(numRows($get_my_loading_points)>0)
		{
			$row_loading_p = fetchArray($get_my_loading_points);
			echo "<option style='font-size:12px !important' value='$row_loading_p[_lat]_$row_loading_p[_long]_$row_loading_p[pincode]'>Loading Point - $row_loading_p[label]</option>";
		}
				
		echo "<script> 
			$('#trip_loading_point').selectpicker('refresh');
			$('#loadicon').fadeOut('slow')
		</script>";
		exit();
	}
}

$gps_veh_by_pass = Qry($conn,"SELECT id FROM _functions WHERE func_type='GPS_VEHICLE_BY_PASS' AND func_value='$veh_no' AND is_active='1'");

if(!$gps_veh_by_pass){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	AlertError("Error while processing request !");
	exit();
}
	
$chk_location_based_poi = Qry($conn,"SELECT id FROM _functions WHERE func_type='LOCATION_BASED_POI' AND is_active='1'");

if(!$chk_location_based_poi){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	AlertError("Error while processing request !");
	exit();
}
	
$chk_gps_active_or_not = Qry($conn,"SELECT gps_timestamp FROM dairy._gps_position WHERE tno='$veh_no' AND gps_timestamp!=0");

if(!$chk_gps_active_or_not){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	AlertError("Error while processing request !");
	exit();
}
	
$row_gps = fetchArray($chk_gps_active_or_not);
	
$time_now = strtotime(date("Y-m-d H:i:s"));
$time_gps = strtotime($row_gps['gps_timestamp']);
$gps_hours = (($time_now-$time_gps) / ( 60 * 60 ));
	
$get_from_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$from_id'");

if(!$get_from_loc_poi){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	AlertError("Error while processing request !");
	exit();
}

if(numRows($get_from_loc_poi)==0)
{
	AlertError("From location not found !");
	echo "<script>$('#loading_start_date').val('');$('#trip_loading_point').val('').selectpicker('refresh');</script>";
	exit();
}

$row_from_poi = fetchArray($get_from_loc_poi);
			
$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => "https://apps2.locanix.net/RuningHourReport/api/Stoppages?VehicleName=$veh_no&FromDateTime=$start_date&ToDateTime=$end_date&Duration=$Duration1",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 900,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "GET",
CURLOPT_HTTPHEADER => array(
'Content-Type: text/plain'
),
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
		
if($err)
{
	AlertError("Connection failed with Locanix !");
	echo "<script>$('#loading_start_date').val('');$('#trip_loading_point').val('').selectpicker('refresh');</script>";
	exit();
}
		
$response2 = json_decode($response, true);

if($response=='null' || $response=='')
{
}
else
{
	if($response2['Stoppages']!='')
	{
		foreach($response2['Stoppages'] as $Data1)
		{
			$from_time = str_replace("+05:30","",str_replace("T"," ",$Data1['From']));
			$to_time = str_replace("+05:30","",str_replace("T"," ",$Data1['To']));
			$duration = $Data1['Duration'];
			$address = $Data1['Address']; 
			
			$zipcode = preg_match("/\b\d{6}\b/", $address, $matches);
			
			if(empty($matches[0])){
				$pincode = "";
				$pincode1 = "";
			}else {
				$pincode = ": ".$matches[0];
				$pincode1 = $matches[0];
			}
			
			$pincode1 = str_replace(" ","",$pincode1);
			$pincode1 = str_replace(".","",$pincode1);
			
			$lat = $Data1['Latitude'];
			$long = $Data1['Longitude'];
			$lat_long_pincode = $lat."_".$long."_".$pincode1; 
			$from_time_new = date("h:i A",strtotime($from_time));
			$to_time_new = date("h:i A",strtotime($to_time));
			// echo $from_time." to ".$to_time."<br>".$duration."<br>".$address."<br>".$lat."<br>".$long."<br>";
			echo "<option style='font-size:12px !important' value='$lat_long_pincode'>$address $pincode- $from_time_new to $to_time_new</option>";
		}
	}
}

$lat_long = $row_from_poi['_lat'].",".$row_from_poi['_long'];
echo "<option style='font-size:12px !important' value='$row_from_poi[_lat]_$row_from_poi[_long]_$row_from_poi[pincode]'>Location - $row_from_poi[name]</option>";

echo "<script>$('#trip_loading_point').selectpicker('refresh');$('#loadicon').fadeOut('slow');</script>";
?>