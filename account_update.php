<?php
require_once("./connect.php");

// BANK AC DETIALS
$ac_holder=trim(escapeString($conn,strtoupper($_POST['update_acname'])));
$acno=trim(escapeString($conn,strtoupper($_POST['update_acno'])));
$bank_name=trim(escapeString($conn,strtoupper($_POST['update_bank'])));
$ifsc=trim(escapeString($conn,strtoupper($_POST['update_ifsc'])));
$code=trim(escapeString($conn,strtoupper($_POST['account_id'])));

if($code=='')
{
	errorLog("Unable to fetch Driver",$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		swal({
			title: 'Unable to fetch Driver !',
			type: 'error',
			closeOnConfirm: true
			});
		$('#loadicon').hide();
		</script>";
	exit();
}

if($ac_holder=='' || $acno=='' || $bank_name=='' || $ifsc=='')
{
	echo "<script type='text/javascript'>
		swal({
			title: 'All input fields are required !',
			type: 'error',
			closeOnConfirm: true
			});
		$('#loadicon').hide();
		</script>";
	exit();
}

	$update_ac=Qry($conn,"UPDATE dairy.driver_ac SET acname='$ac_holder',acno='$acno',bank='$bank_name',ifsc='$ifsc' WHERE code='$code'");
	if(!$update_ac){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		echo "<script>
			window.location.href='./';
		</script>";
		exit();
	}
	
	echo "
	<script>
		swal({
		 title: 'UPDATE SUCCESS',
		  type: 'success',
		  closeOnConfirm: true
			},
		function(){
			 window.location.href='./';
		});
	</script>
	";
	exit();
?>