<?php
require('./connect.php');

// exit();

echo "<script type='text/javascript'>
			alert('Function in-active !!');
			window.location.href='./logout.php';
		</script>";
	exit();
	
$tno=escapeString($conn,strtoupper($_SESSION['diary']));
$tno_set=escapeString($conn,strtoupper($_POST['tno']));
$timestamp = date("Y-m-d H:i:s"); 
$date=date("Y-m-d");

if($tno!=$tno_set)
{
	errorLog("Truck Number not macthed.",$conn,$page_name,__LINE__);
	
	echo "<script type='text/javascript'>
			window.location.href='./logout.php';
		</script>";
	exit();
}

$from=escapeString($conn,strtoupper($_POST['from']));
$to=escapeString($conn,strtoupper($_POST['to']));
$lr_type=escapeString($conn,strtoupper($_POST['lr_type']));
$next_edit_branch=escapeString($conn,strtoupper($_POST['next_edit_branch']));

$billing_type_text=escapeString($conn,strtoupper($_POST['billing_type_text']));
$freight_bilty_text=escapeString($conn,strtoupper($_POST['freight_bilty_text']));
$pincode=escapeString($conn,strtoupper($_POST['pincode']));
$set_km=escapeString($conn,($_POST['set_km']));
$trip_km=escapeString($conn,($_POST['trip_km']));
$from_loc_id=escapeString($conn,($_POST['from_loc_id']));
$to_loc_id=escapeString($conn,($_POST['to_loc_id']));
$at_loading_set=escapeString($conn,($_POST['at_loading_set']));
$trip_id=escapeString($conn,($_POST['trip_id']));

if($trip_id=='' || $from=='' || $to=='' || $from_loc_id=='' || $to_loc_id=='' || $lr_type=='')
{
	echo "<script type='text/javascript'>
			alert('Error while fetching data.')
			window.location.href='./';
		</script>";
	exit();
}

if($at_loading_set>0)
{
	$at_loading="1";
}
else
{
	$at_loading="0";
}

if($from_loc_id=='' || $to_loc_id=='')
{
	errorLog("Unable to fetch Location Id.",$conn,$page_name,__LINE__);
	
	echo "<script type='text/javascript'>
			swal({
			  title: 'Unable to fetch Location Id.',
			  type: 'error',
			  closeOnConfirm: true
			});
		</script>";
	exit();
}

if(strlen($pincode)!=6)
{
	echo "<script type='text/javascript'>
			swal({
			  title: 'Invalid PINCODE.',
			  type: 'error',
			  closeOnConfirm: true
			});
		</script>";
	exit();
}

if($freight_bilty_text>0 and $billing_type_text=='TO_PAY')
{
	$mb_to_pay=1;
	$mb_amount=$freight_bilty_text;
}
else
{
	$mb_to_pay=0;
	$mb_amount=0;
}

if($lr_type=='EMPTY')
{
	$actual="0";
	$charge="0";
	$lrno="EMPTY";
	$lr_type2="0";
}
else
{
	$actual=escapeString($conn,strtoupper($_POST['actual']));
	$charge=escapeString($conn,strtoupper($_POST['charge']));
	$lrno=escapeString($conn,strtoupper($_POST['lrno']));
	// errorLog("Unable to fetch Location Id.",$conn,$page_name,__LINE__);
	
	if(substr($lr_type,3,1)=='M')
	{
		$lr_type2=1;
	}
	else if(substr($lr_type,3,3)=='OLR')
	{
		$lr_type2=1;
	}
	else
	{
		$lr_type2="0";
	}
}

$fetch_old_trip_data = Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE tno='$tno' AND id<'$trip_id' ORDER BY id DESC LIMIT 1");
if(!$fetch_old_trip_data)	
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($fetch_old_trip_data)>0)
{
$row_old_trip = fetchArray($fetch_old_trip_data);

if($row_old_trip['from_station']=='' || $row_old_trip['from_station']=='NA' || $row_old_trip['to_station']=='' || $row_old_trip['to_station']=='NA')
{
	Redirect("Warning : ADD LR in Previous Trip First !","./");
	exit();
}

	$to_set = "1";
	$to_set_loc = $row_old_trip['to_station'];
}
else
{
	$to_set = "0";
	$to_set_loc = "";
}

$fetch_next_trip_data = Qry($conn,"SELECT from_station FROM dairy.trip WHERE tno='$tno' AND id>'$trip_id' ORDER BY id ASC LIMIT 1");
if(!$fetch_next_trip_data)	
{
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($fetch_next_trip_data)>0)
{
	
$row_next_trip = fetchArray($fetch_next_trip_data);

if($row_next_trip['from_station']!='' AND $row_next_trip['from_station']!='NA')
{
	$from_set = "1";
	$from_set_loc = $row_next_trip['from_station'];
}
else
{
	$from_set = "0";
	$from_set_loc = "";
}
}
else
{
	$from_set = "0";
	$from_set_loc = "";
}

$fetch_trip_data = 	Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE id='$trip_id'");	
if(!$fetch_trip_data)	
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

$row_this_trip = fetchArray($fetch_trip_data);

if($row_this_trip['from_station']!='NA' AND $row_this_trip['from_station']!='')
{
	if($row_this_trip['from_station']!=$from)
	{
		echo "<script>
			alert('Warning : Location not matching. Current trip\'s from location is : $row_this_trip[from_station] and LR\'s From Location is : $from.');
			window.location.href='./';
		</script>";
		exit();
	}
}

if($to_set=='1')
{
	if($to_set_loc!=$from)
	{
		echo "<script>
			alert('Warning : Location not matching. Last trip\'s to location is : $to_set_loc. and LR\'s From Location is : $from. Trip id : $trip_id.');
			window.location.href='./';
		</script>";
		exit();
	}
}

if($from_set=='1')
{
	if($from_set_loc!=$to)
	{
		echo "<script>
			alert('Warning : Location not matching. Next trip\'s From location is : $from_set_loc and LR\'s To Location is : $to.');
			window.location.href='./';
		</script>";
		exit();
	}
}

StartCommit($conn);
$flag = true;

if($set_km==1)
{
	$insert_lane=Qry($conn,"INSERT INTO dairy.master_km (from_loc,from_loc_id,to_loc,to_loc_id,km,branch,timestamp) 
	VALUES ('$from','$from_loc_id','$to','$to_loc_id','$trip_km','$branch','$timestamp')");
	
	if(!$insert_lane)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$qry=Qry($conn,"UPDATE dairy.trip SET branch='$branch',edit_branch='$next_edit_branch',from_station='$from',to_station='$to',
from_id='$from_loc_id',to_id='$to_loc_id',km='$trip_km',pincode='$pincode',act_wt='$actual',charge_wt='$charge',lr_type='$lr_type',
lr_type2='$lr_type2',mb_to_pay='$mb_to_pay',mb_amount='$mb_amount',lrno='$lrno' WHERE id='$trip_id'");

if(!$qry)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
	if($flag)
	{
		MySQLCommit($conn);
	}
	else
	{
		MySQLRollBack($conn);
		errorLog("COMMIT NOT COMPLETED. Timestamp : $timestamp. Truck No: $tno, TripNo: $trip_no.",$conn,$page_name,__LINE__);
		closeConnection($conn);
		Redirect("Error While Processing Request.","./");
		exit();
	}
	
	echo "<script type='text/javascript'>
			alert('Trip updated Successfully !');
			 window.location.href='./';
		</script>";
	closeConnection($conn);	
	exit();

?>