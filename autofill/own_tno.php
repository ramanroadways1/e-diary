<?php
include($_SERVER['DOCUMENT_ROOT']."/_connect.php");

$db = new mysqli($host,$username,$password,$db_name);
$searchTerm = $_REQUEST['term'];
$query = $db->query("SELECT tno,comp,superv_id,model,wheeler FROM dairy.own_truck WHERE is_sold='0' AND tno LIKE '%".$searchTerm."%' ORDER BY tno ASC LIMIT 5");
while ($row = $query->fetch_assoc()) 
{
	$data[] = array("value"=>$row['tno'],"label"=>$row['tno']." ($row[comp]) - $row[model], $row[wheeler]W","supervisor"=>$row['superv_id']);
}
echo json_encode($data);
?>