<?php
require_once("../../_connect.php");

if(isset($_POST['search'])){
 $search = $_POST['search'];
 $sql = Qry($conn,"SELECT id,name,pan FROM dairy.broker WHERE name LIKE '%".$search."%' ORDER BY name ASC LIMIT 10");
 $response = array();
 while($row = fetchArray($sql) ){
   $response[] = array("value"=>$row['name'],"label"=>$row['name']." ($row[pan])","id"=>$row['id'],"pan"=>$row['pan']);
 }
 echo json_encode($response);
}
?>