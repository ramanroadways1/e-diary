<?php
require("../connect.php");

if(isset($_POST['search'])){
 $search = $_POST['search'];
 $sql = Qry($conn,"SELECT id,acno,branch,bank FROM dairy.cheque_ac WHERE acno LIKE '%".$search."%' ORDER BY name ASC LIMIT 10");
 $response = array();
 while($row = fetchArray($sql) ){
   $response[] = array("value"=>$row['acno'],"label"=>$row['acno'],"id"=>$row['id'],"bank_branch"=>$row['bank']."-".$row['branch']);
 }
 echo json_encode($response);
}
?>