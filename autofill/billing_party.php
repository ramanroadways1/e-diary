<?php
require_once("../../_connect.php");

if(isset($_POST['search'])){
 $search = $_POST['search'];
 $sql = Qry($conn,"SELECT id,name,pan,addr FROM dairy.billing_party WHERE name LIKE '%".$search."%' ORDER BY name ASC LIMIT 10");
 $response = array();
 while($row = fetchArray($sql) ){
   $response[] = array("value"=>$row['name'],"label"=>$row['name']." ($row[pan])","id"=>$row['id'],"pan"=>$row['pan'],"addr"=>$row['addr']);
 }
 echo json_encode($response);
}
?>