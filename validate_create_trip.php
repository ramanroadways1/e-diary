<?php
require("./connect.php");

$from_id = escapeString($conn,($_POST['from_id']));
$to_id = escapeString($conn,($_POST['to_id'])); 
$start_point_array = escapeString($conn,($_POST['start_point']));

$start_point= @explode("_",$start_point_array)[0].",".@explode("_",$start_point_array)[1];
$start_point_pincode = @explode("_",$start_point_array)[2];

$end_point_array = escapeString($conn,($_POST['end_point'])); // stoppage point to loc

$end_point = @explode("_",$end_point_array)[0].",".@explode("_",$end_point_array)[1];
$end_point_pincode = @explode("_",$end_point_array)[2];

$from_loc = escapeString($conn,($_POST['from_loc'])); // from loc name ex. VISALPUR
$to_loc = escapeString($conn,($_POST['to_loc']));  // to loc name ex. BARODA
$running_trip = escapeString($conn,($_POST['running_trip']));  // true // false
$lr_type = escapeString($conn,($_POST['lr_type']));  // MKT // OWN // Others
$vou_no = escapeString($conn,($_POST['vou_no']));  // OLR or Mkt bilty number // empty if other 
$start_date = escapeString($conn,($_POST['start_date']));  // trip start date
$end_date = escapeString($conn,($_POST['end_date']));  // trip end_date
$actual_wt = escapeString($conn,($_POST['actual_wt'])); 
$charge_wt = escapeString($conn,($_POST['charge_wt'])); 

$tno = escapeString($conn,strtoupper($_SESSION['diary']));

$get_max_distance_from_poi = Qry($conn,"SELECT func_value FROM _functions WHERE func_type='MAX_DISTANCE_FROM_POI'");
	
if(!$get_max_distance_from_poi){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if(numRows($get_max_distance_from_poi)==0)
{
	$max_distance_from_poi=20;
}
else
{
	$row_max_distance = fetchArray($get_max_distance_from_poi);
	$max_distance_from_poi = $row_max_distance['func_value'];
}

if($lr_type=='')
{
	AlertError("LR type not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if(($lr_type=='MKT' || $lr_type=='OWN') AND $vou_no=='')
{
	AlertError("Voucher not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if($from_id=='' || $to_id=='')
{
	AlertError("Locations not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if(strlen($start_point)<5)
{
	AlertError("Trip start point not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if($start_date=='' || $start_date==0)
{
	AlertError("Trip start date not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if($running_trip=='false' AND strlen($end_point)<5)
{
	AlertError("Trip end point not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if($running_trip=='false' AND ($end_date==0 || $end_date==''))
{
	AlertError("Trip end date not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if($running_trip=='true')
{
	$end_point="";
	$end_date = 0;
}

$get_avg = GetAvgValue($conn,$from_id,$to_id,$from_loc,$to_loc,$tno,$lr_type,$actual_wt);
		
if($get_avg[0]==false)
{
	AlertError($get_avg[1]);
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}
		
$avg_value = $get_avg[2];
$by_pass_route = $get_avg[3];

$get_from_loc = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$from_id'");
	
if(!$get_from_loc){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if(numRows($get_from_loc)==0)
{
	AlertError("From location not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

$get_to_loc = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$to_id'");
	
if(!$get_to_loc){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if(numRows($get_to_loc)==0)
{
	AlertError("To location not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

$row_from = fetchArray($get_from_loc);
$row_to = fetchArray($get_to_loc);

if($row_from['_lat']=='' || $row_from['_long']=='' || $row_from['pincode']=='')
{
	AlertError("From location POI not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if($row_to['_lat']=='' || $row_to['_long']=='' || $row_to['pincode']=='')
{
	AlertError("To location POI not found..");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if($row_from['name']!=$from_loc)
{
	AlertError("Location error: Check From Location.");
	echo "<script>editBranchReset();$('.from_station').attr('readonly',false);$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

if($row_to['name']!=$to_loc)
{
	// errorLog("To location from db is: $row_to[name] and LR to Location is : $to_loc. Location Id is: $to_id.",$conn,$page_name,__LINE__);
	AlertError("Location error: Check To Location. $row_to[name] and $to_loc");
	echo "<script>editBranchReset();$('#to_stn').attr('readonly',false);$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
	exit();
}

$insert_from_poi = "NO";
$insert_to_poi = "NO";
$from_addr_book_id = "0";
$to_addr_book_id = "0";
$con1_id = 0;
$con2_id = 0;
	
if($lr_type=='OWN')
{
	$vou_no_Array = explode('_', $vou_no);
	$vou_no = $vou_no_Array[0];
	$vou_id = $vou_no_Array[1];
	
	if($vou_no!='ATLOADING')
	{
		$chk_voucher = Qry($conn,"SELECT f.frno,f.consignor,f.consignee,f.con1_id,f.con2_id,f.lrno,f.done,f.crossing,l.shipment_party_id 
		FROM freight_form_lr AS f 
		LEFT OUTER JOIN lr_sample AS l ON l.id = f.mother_lr_id	
		WHERE f.id='$vou_id'");
			
		if(!$chk_voucher){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>alert('Error..');editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}

		if(numRows($chk_voucher)==0 AND $vou_no!='ATLOADING')
		{
			AlertError("Voucher not found..");
			echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}
		
		$row_voucher = fetchArray($chk_voucher);
		
		if($row_voucher['done']!=0)
		{
			AlertError("Voucher already attached to another trip..");
			echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}
		
		if($row_voucher['frno']!=$vou_no)
		{
			AlertError("Voucher not verified..");
			echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}
		
		$con1_id = $row_voucher['con1_id'];
		
		if($row_voucher['shipment_party_id']!=0 AND $row_voucher['shipment_party_id']!=$row_voucher['con2_id'])
		{
			$con2_id = $row_voucher['shipment_party_id'];
		}
		else
		{
			$con2_id = $row_voucher['con2_id'];
		}
		
		$chk_from_poi=Qry($conn,"SELECT id,pincode,_lat,_long FROM address_book_consignor WHERE consignor='$con1_id' AND from_id='$from_id'");

		if(!$chk_from_poi){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			echo "<script>alert('Error..');editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}
		
		if(numRows($chk_from_poi)>0)
		{
			$row_from_poi = fetchArray($chk_from_poi);
			$trip_start_point = $row_from_poi['_lat'].",".$row_from_poi['_long'];
			$trip_start_pincode = $row_from_poi['pincode'];
			$from_addr_book_id = $row_from_poi['id'];
		}
		else
		{
			//  check for crossing lrno
			
			$chk_crossing_lr = Qry($conn,"SELECT crossing FROM freight_form_lr WHERE lrno='$row_voucher[lrno]' AND frno!='$row_voucher[frno]' 
			ORDER BY id DESC LIMIT 1");
				
			if(!$chk_crossing_lr){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				echo "<script>alert('Error..');editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
				exit();
			}
			
			if(numRows($chk_crossing_lr)==0)
			{
				AlertError("Please update loading point first !<br>Consignor: $row_voucher[consignor]<br>Loading location: $from_loc.");
				echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
				exit();
			}
			else
			{
				$row_chk_cross = fetchArray($chk_crossing_lr);
				
				if($row_chk_cross['crossing']!='YES')
				{
					AlertError("Please update loading point first !<br>Consignor: $row_voucher[consignor]<br>Loading location: $from_loc.");
					echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
					exit();
				}
				else
				{
					// $trip_start_point = $row_from['_lat'].",".$row_from['_long'];
					// $trip_start_pincode = $row_from['pincode'];
					$trip_start_point = $start_point;
					$trip_start_pincode = $start_point_pincode;
				}
			}
		}
		
		$chk_to_poi = Qry($conn,"SELECT id,pincode,_lat,_long FROM address_book_consignee WHERE consignee='$con2_id' AND to_id='$to_id'");

		if(!$chk_to_poi){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			echo "<script>alert('Error..');editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}
		
		if(numRows($chk_to_poi)>0)
		{
			$row_to_poi = fetchArray($chk_to_poi);
			$trip_end_point = $row_to_poi['_lat'].",".$row_to_poi['_long'];
			$trip_end_pincode = $row_to_poi['pincode'];
			$to_addr_book_id = $row_to_poi['id'];
		}
		else
		{ 
			if($row_voucher['crossing']!='YES')  
			{
				AlertError("Please update unloading point first !<br>Consignee: $row_voucher[consignee]<br>Destination: $to_loc.");
				echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
				exit();
			}
			else
			{
				// $trip_end_point = $row_to['_lat'].",".$row_to['_long'];
				// $trip_end_pincode = $row_to['pincode'];
				
				if($running_trip=='false')
				{
					$trip_end_point = $end_point;
					$trip_end_pincode = $end_point_pincode;
					
					$distance_unloading_point_and_loc = calculate_distance($row_to['_lat'],$row_to['_long'],explode(",",$trip_end_point)[0],explode(",",$trip_end_pincode)[1]); 
			
					if($distance_unloading_point_and_loc>$max_distance_from_poi)
					{
						AlertError('Check destination OR trip end point !<br><span style=\"color:maroon\">Distance is: '.$distance_unloading_point_and_loc.' KMs</span>');
						echo "<script>editBranchReset();$('#route_type_1').val('');$('#to_stn').attr('readonly',false);$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
						exit();
					}
				}
				else
				{
					$trip_end_point = $row_to['_lat'].",".$row_to['_long'];
					$trip_end_pincode = $row_to['pincode'];
				}
			}
		}
	}
	else
	{
		$trip_start_point = $start_point;
		$trip_start_pincode = $start_point_pincode;
		
		if($running_trip=='true')
		{
			$trip_end_point = $row_to['_lat'].",".$row_to['_long'];
			$trip_end_pincode = $row_to['pincode'];
			
			if(strlen($trip_end_pincode)!=6)
			{
				AlertError("Destination location pincode not found !");
				echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
				exit();
			}
		}
		else
		{
			$str_end_location = $row_to['_lat'].",".$row_to['_long'];
			$trip_end_point = $end_point;
			$trip_end_pincode = $end_point_pincode;
			
			// distance b/w to location and unloading point
			
			$distance_unloading_point_and_loc = calculate_distance(explode(",",$str_end_location)[0],explode(",",$str_end_location)[1],explode(",",$trip_end_point)[0],explode(",",$trip_end_point)[1]); 
			
			if($distance_unloading_point_and_loc>$max_distance_from_poi)
			{
				AlertError('Check destination OR trip end point !<br><span style=\"color:maroon\">Distance is: '.$distance_unloading_point_and_loc.' KMs</span>');
				echo "<script>editBranchReset();$('#route_type_1').val('');$('#to_stn').attr('readonly',false);$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
				exit();
			}
			
			if(strlen($trip_end_pincode)!=6)
			{
				$trip_end_pincode = getZipcode(explode(",",$trip_end_point)[0].",".explode(",",$trip_end_point)[1]);
				
				if(strlen($trip_end_pincode)!=6)
				{
					AlertError("Unloading point pincode not found !");
					echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
					exit();
				}
			}
		}
	}
}
else
{
	$str_start_location = $row_from['_lat'].",".$row_from['_long'];
	$trip_start_point = $start_point;
	$trip_start_pincode = $start_point_pincode;
	
	// distance b/w loading point and from location
	
	$distance_loading_loc_and_point = calculate_distance(explode(",",$trip_start_point)[0],explode(",",$trip_start_point)[1],explode(",",$str_start_location)[0],explode(",",$str_start_location)[1]); 

	if($distance_loading_loc_and_point>$max_distance_from_poi)
	{
		AlertError('Check From location OR trip start point !<br><span style=\"color:maroon\">Distance is: '.$distance_loading_loc_and_point.' KMs</span>');
		echo "<script>editBranchReset();$('#route_type_1').val('');$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
		exit();
	}
	
	if(strlen($trip_start_pincode)!=6)
	{
		$trip_start_pincode = getZipcode(explode(",",$trip_start_point)[0].",".explode(",",$trip_start_point)[1]);
		
		if(strlen($trip_start_pincode)!=6)
		{
			AlertError("Loading point pincode not found !");
			echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
			exit();
		}
	}
	
	if($running_trip=='true')
	{
		$trip_end_point = $row_to['_lat'].",".$row_to['_long'];
		$trip_end_pincode = $row_to['pincode'];
		
		if(strlen($trip_end_pincode)!=6)
		{
			AlertError("Destination location pincode not found !");
			echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
			exit();
		}
	}
	else
	{
		$str_end_location = $row_to['_lat'].",".$row_to['_long'];
		$trip_end_point = $end_point;
		$trip_end_pincode = $end_point_pincode;
		
		// distance b/w to location and unloading point
		
		$distance_unloading_point_and_loc = calculate_distance(explode(",",$str_end_location)[0],explode(",",$str_end_location)[1],explode(",",$trip_end_point)[0],explode(",",$trip_end_point)[1]); 
		
		if($distance_unloading_point_and_loc>$max_distance_from_poi)
		{
			AlertError('Check destination OR trip end point !<br><span style=\"color:maroon\">Distance is: '.$distance_unloading_point_and_loc.' KMs</span>');
			echo "<script>editBranchReset();$('#route_type_1').val('');$('#to_stn').attr('readonly',false);$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}
		
		if(strlen($trip_end_pincode)!=6)
		{
			$slice_1 = explode(",",$trip_end_point)[0];
			$slice_2 = explode(",",$trip_end_point)[1];
			
			$trip_end_pincode = getZipcode($slice_1.",".$slice_2);
			
			if(strlen($trip_end_pincode)!=6)
			{ 
				$trip_end_pincode = $row_to['pincode'];
				// AlertError("Unloading point pincode not found !");
				// echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
				// exit();
			}
		}
	}
}

$_SESSION['create_trip_insert_from_poi'] = $insert_from_poi;	
$_SESSION['create_trip_insert_to_poi'] = $insert_to_poi;	

$_SESSION['create_trip_from_addr_id'] = $from_addr_book_id;	
$_SESSION['create_trip_to_addr_id'] = $to_addr_book_id;	
$_SESSION['create_trip_vou_no'] = escapeString($conn,$_POST['vou_no']); // vou_no_vou_id_con1_id_con2_id
$_SESSION['create_trip_running_trip']=$running_trip;	 // true / false
$_SESSION['create_trip_start_date'] = $start_date;	 // trip end date / unload date
$_SESSION['create_trip_end_date'] = $end_date;	 // trip end date / unload date
$_SESSION['create_trip_start_point'] = $trip_start_point; // lat,long 20.152456,45.45465
$_SESSION['create_trip_end_point'] = $trip_end_point; // lat,long 20.152456,45.45465
$_SESSION['create_trip_km']="0"; // trip km Google
$_SESSION['create_trip_dest_pincode'] = $trip_end_pincode; // trip km Google
$_SESSION['create_trip_start_pincode'] = $trip_start_pincode;
$_SESSION['create_trip_from_id'] = $from_id;
$_SESSION['create_trip_to_id'] = $to_id;
$_SESSION['create_trip_from_loc'] = $from_loc;
$_SESSION['create_trip_to_loc'] = $to_loc;
$_SESSION['create_trip_lr_type'] = $lr_type;
$_SESSION['create_trip_con1_id'] = $con1_id;
$_SESSION['create_trip_con2_id'] = $con2_id;
?>
<div id="km_session_html_create_new_trip"></div>

<script type="text/javascript">
    function myfunction(){
        var map;
        var start = new google.maps.LatLng(<?php echo $trip_start_point; ?>);
        var end = new google.maps.LatLng(<?php echo $trip_end_point; ?>);
        var option ={
            zoom : 15,
            center : start,
			disableDefaultUI: true,
        };
        map = new google.maps.Map(document.getElementById('map'),option);
        var display = new google.maps.DirectionsRenderer();
        var services = new google.maps.DirectionsService();
        display.setMap(map);
            var request ={
                origin : start,
                destination:end,
                travelMode: 'DRIVING'
            };
            services.route(request,function(result,status){
                if(status =='OK'){
                    display.setDirections(result);
					 var directionsData = result.routes[0].legs[0]; // Get data about the mapped route
					if (!directionsData) {
					  alert('Request failed');
						editBranchReset();
						$('#loadicon').fadeOut('slow');
						$('#create_trip_dsl_qty_required').val('');
						$('#trip_sub').attr('disabled',true);
						$('#trip_sub').hide();
					}
					else {
					  var distance1 = directionsData.distance.text;
					// alert(distance1);
					var distance_no = distance1.replace(',','');	
					// alert(distance_no);
					var distance_no = parseFloat(distance_no.match(/[\d\.]+/));
					var duration1 =  directionsData.duration.text;
					  // alert(distance_no);
					  
					  var encodedPolyLine = result.routes[0].overview_polyline;
					  
					  $('#trip_km').val(distance_no);
					  $('#create_trip_encodedPolyLine').val(encodedPolyLine);
					  $('#km_google1').html(distance_no+" KM");
					  $('#duration_google1').html(duration1);
					  $('#loadicon').fadeOut('slow');
					  $('.from_station').attr('readonly',true);
					  $('#to_stn').attr('readonly',true);
					  // $('#from_loc_google').attr('readonly',true);
					  // $('#to_loc_google').attr('readonly',true);
					  $('#trip_sub').attr('disabled',false);
					  $('#trip_sub').show();
					   
					  jQuery.ajax({url: "./_create_session_var_for_km_create_trip.php",
					  data:'distance='+distance_no + '&avg_value=' + '<?php echo $avg_value; ?>',
					  type: "POST",
					  success: function(data){
						$("#km_session_html_create_new_trip").html(data);
					 },
						error: function() {
					}});
				}
                }
				else{
					alert('Error: '+status);
					editBranchReset();
					$('#loadicon').fadeOut('slow');
					$('#create_trip_dsl_qty_required').val('');
					$('#trip_sub').attr('disabled',true);
					$('#trip_sub').hide();
				}
            });
}
myfunction();
</script>

<?php
echo "<script>
	$('#by_pass_route').val('$by_pass_route');
	$('#create_trip_pincode').val('$trip_end_pincode');
</script>";
?>