<?php
require("./connect.php");

exit();
// file on hold bcz other logic is developed

$trip_id = escapeString($conn,($_POST['trip_id']));
$elem = escapeString($conn,($_POST['elem']));
$txn_id = escapeString($conn,($_POST['txn_id']));
$timestamp = date("Y-m-d H:i:s");

if($txn_id=='')
{
	echo "<script>
		alert('Error : Txn id not found !');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($trip_id=='')
{
	echo "<script>
		alert('Error : Trip-ID not found !');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$chk_gps = Qry($conn,"SELECT tno,trip_no,driver_code,gps_naame_txn_id,driver_gps_naame FROM dairy.trip WHERE id='$trip_id'");

if(!$chk_gps){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_gps)==0)
{
	echo "<script>alert('Trip not found !');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row = fetchArray($chk_gps);

if($row['gps_naame_txn_id'] != $txn_id)
{
	echo "<script>alert('Txn id not verified !');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row['driver_gps_naame']<=0)
{
	echo "<script>alert('GPS Charges not verified !');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_txn_record = Qry($conn,"SELECT id,driver_code,date FROM dairy.driver_book WHERE trans_id='$row[gps_naame_txn_id]'");

if(!$get_txn_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_txn_record)==0)
{
	echo "<script>alert('Txn not found !');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_driver = fetchArray($get_txn_record);

if($row_driver['driver_code'] != $row['driver_code'])
{
	echo "<script>alert('Driver not verified !');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$tno = $row['tno'];
$driver_code = $row_driver['driver_code'];
$trip_no = $row['trip_no'];
$txn_id_db = $row['gps_naame_txn_id'];
$txn_date_db = $row_driver['date'];

StartCommit($conn);
$flag = true;

$chk_record = Qry($conn,"SELECT id FROM dairy.driver_book WHERE id>'$row_driver[id]' AND tno='$row[tno]'");

if(!$chk_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$total_rows = numRows($chk_record);

$update_balance = Qry($conn,"UPDATE dairy.driver_book SET balance = balance - '3000' WHERE id>'$row_driver[id]' AND driver_code='$row_driver[driver_code]'");

if(!$update_balance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn) != $total_rows)
{
	$flag = false;
	errorLog("No all rows balance updated. Tno : $row[tno]. Driver: $row_driver[driver_code].",$conn,$page_name,__LINE__);
}

$update_trip = Qry($conn,"UPDATE dairy.trip SET driver_naame = driver_naame - '3000' WHERE id='$trip_id'");

if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_balance2 = Qry($conn,"UPDATE dairy.driver_up SET amount_hold = amount_hold - '3000' WHERE down=0 AND code='$driver_code' 
ORDER BY id DESC LIMIT 1");
	
if(!$update_balance2){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_db_record = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$row_driver[id]'");

if(!$delete_db_record){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$delete_naame = Qry($conn,"DELETE FROM dairy.driver_naame WHERE trans_id='$row[gps_naame_txn_id]'");

if(!$delete_naame){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,tno,action,desct,branch,username,timestamp) VALUES ('$driver_code','$tno',
'Vedl_GPS_Naame_Deleted_On_Pod_Rcv','Trip_No: $trip_no. Txn_Id: $txn_id_db. Txn_Date: $txn_date_db.','$branch','$_SESSION[user_code]','$timestamp')");

if(!$insert_log){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{ 
	MySQLCommit($conn);
	closeConnection($conn);	
	echo "<script>
		alert('Updated Successfully.');
		$('#btn_gps_sel1').attr('disabled',true);
		$('#vedl_gps_sel').attr('disabled',true);
		$('.gps_sel_opt1').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request..");
	exit();
}
?>

