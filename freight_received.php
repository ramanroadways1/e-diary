<?php
require('connect.php');

$timestamp = date("Y-m-d H:i:s"); 
$tno = escapeString($conn,strtoupper($_POST['tno']));

if($tno!=$_SESSION['diary'])
{
	Redirect("Please log in again..","./logout.php");
	exit();
}

require_once("./check_cache.php");

$amount = escapeString($conn,$_POST['amount']);
$date = date("Y-m-d");
$trip_id=escapeString($conn,strtoupper($_POST['trip_id']));
$narration=escapeString($conn,strtoupper($_POST['narration']));
$lrno=escapeString($conn,strtoupper($_POST['lr_no']));
$adv_bal=escapeString($conn,strtoupper($_POST['adv_bal']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertError("Trip not found..");
	errorLog("Trip id not found. id: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#freight_button').attr('disabled',false);</script>";
	exit();
}

$check_trip = Qry($conn,"SELECT t.id,t.tno,t.driver_code,t.trip_no,d.name as driver_name 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
WHERE t.id='$trip_id'");

if(!$check_trip){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#freight_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertError("Trip not found..");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#freight_button').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$driver_name = $row_trip['driver_name'];
$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];

if(substr($lrno,3,1)=='M')
{
	$chk_book=Qry($conn,"SELECT balance FROM dairy.bilty_balance WHERE bilty_no='$lrno'");
	
	if(numRows($chk_book)==0)
	{
		AlertError("Market bilty not found..");
		echo "<script>$('#freight_button').attr('disabled',false);</script>";
		errorLog("Market Bilty not found.",$conn,$page_name,__LINE__);
		exit();	
	}
	
	$row_bilty_current_bal = fetchArray($chk_book);
	$current_bilty_bal = $row_bilty_current_bal['balance'];
}
else if(substr($lrno,3,3)=='OLR')
{
}
else
{
	AlertError("LR not found..");
	echo "<script>$('#freight_button').attr('disabled',false);</script>";
	exit();	
}
		
$trans_id_Qry = GetTxnId_eDiary($conn,"FRTADV");
	
if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error..");
	echo "<script>$('#freight_button').attr('disabled',false);</script>";
	exit();
}
	
$trans_id = $trans_id_Qry;	

StartCommit($conn);
$flag = true;

$insert_freight_diary=Qry($conn,"INSERT INTO dairy.freight_adv (trip_id,adv_bal,trans_id,vou_id,tno,amount,date,narration,branch,branch_user,
timestamp) VALUES ('$trip_id','$adv_bal','$trans_id','$lrno','$tno','$amount','$date','$lrno : $narration','$branch','$_SESSION[user_code]','$timestamp')");

if(!$insert_freight_diary){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_trip=Qry($conn,"UPDATE dairy.trip SET freight=freight+$amount WHERE id='$trip_id'");

if(!$update_trip){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$nrr = $tno."/".$driver_name." : ".$narration;

$select_amount=Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($select_amount)==0){
	$flag = false;
	errorLog("Error While Fetching Driver Balance. Code: $driver_code.",$conn,$page_name,__LINE__);
}

$row_amount=fetchArray($select_amount);

$hold_amt = $row_amount['amount_hold']+$amount;
$driver_book_id = $row_amount['id'];

$insert_book=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,credit,balance,date,branch,
timestamp) VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','FREIGHT_ADVANCE','$amount','$hold_amt','$date','$branch',
'$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+$amount WHERE id='$driver_book_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(substr($lrno,3,1)=='M')
{
$new_balance = $current_bilty_bal-$amount;

$insert_into_bilty_book=Qry($conn,"INSERT INTO dairy.bilty_book (bilty_no,trans_id,branch,branch_user,date,advbal,debit,balance,timestamp) VALUES 
('$lrno','$trans_id','$branch','$_SESSION[user_code]','$date','$adv_bal','$amount','$new_balance','$timestamp')");

if(!$insert_into_bilty_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_balance=Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=rcvd+'$amount',balance='$new_balance' WHERE bilty_no='$lrno'");

if(!$update_balance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Unable to update bilty new balance. Bilty No : $lrno. CurrentBal : $current_bilty_bal,CrAmt : $amount,NewBal:$new_balance.",$conn,$page_name,__LINE__);
}

}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error..");
	echo "<script>$('#freight_button').attr('disabled',false);</script>";
	exit();
}

echo "<script>
		$('#freight_button').attr('disabled',false);
		$('#FreightForm')[0].reset();
		document.getElementById('hide_freight').click();
		TripAdv('#$trip_id');
		LoadDriverBalance('$driver_code');
		$('#loadicon').fadeOut('slow');	
		
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: 'Success ! Freight credited.',
			showConfirmButton: false,
			timer: 2000
		})
	</script>";
exit();
?>