<?php
require("./connect.php");
require("./_header.php");
$date=date("Y-m-d");
include("code_disabled_right_click.php");
?>
<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<br/>
<br/>

<style>
body{font-family:'Open Sans Condensed', 'sans-serif'}
label{font-size:13px;}
.form-control{
	text-transform:uppercase;
}
#bg {
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: url(truck_bg.jpg) no-repeat center center fixed;
  background-size: cover;
  -webkit-filter: blur(4px);    
}
</style>

<div id="bg"></div>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormBroker").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#broker_submit").attr("disabled", true);
	$.ajax({
        	url: "./save_broker.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_div").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<form action="" id="FormBroker" autocomplete="off" method="POST">
<div class="container" style="">
	<div class="row">
		<div class="col-md-5 col-md-offset-1" style="border:0px solid gray;border-radius:px;box-shadow:15px 15px 15px 15px #DDD inset">
			
			<div class="row bg-primary" style="padding:3px;">
				<center><span style="font-size:16px;color:#FFF;">ADD NEW BROKER</span></center>
			</div>	
			
			<div class="form-group col-md-12"></div>
			
			<div class="form-group col-md-12">
				<label>Broker Name <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,]/,'')" name="broker_name" class="form-control" required />
			</div>
			
			<div class="form-group col-md-12">
				<label>PAN No <font color="red"><sup>(optional)</sup></font></label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePAN2()" maxlength="10" type="text" id="broker_pan" name="broker_pan" class="form-control" />
			</div>
			
			<div class="form-group col-md-12">
				<label>Mobile No <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="broker_mobile" minlength="10" maxlength="10" class="form-control" required />
			</div>
			
			<div class="form-group col-md-12">
				<input type="submit" id="broker_submit" name="broker_submit" class="btn btn-sm btn-danger" value="Save Broker" />
				<!--<button type="button" onclick="var customWindow = window.open('', '_blank', '');customWindow.close();" class="btn btn-sm pull-right btn-default">Close window</button>-->
			</div>
			
			
			<br />
		</div>
	</div>
</div>
</form>

<script>
function ValidatePAN2() { 
  var Obj = document.getElementById("broker_pan");
  if(Obj.value!=""){
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                document.getElementById("broker_pan").setAttribute("style","background-color:#ff8080;color:#FFF");
				$("#broker_submit").attr("disabled", true);
                Obj.focus();
                return false;
            }
			else
            {
				document.getElementById("broker_pan").setAttribute("style","background-color:#66ff66;color:#000");
				$("#broker_submit").attr("disabled", false);
            }
        }
		else
		{
			document.getElementById("broker_pan").setAttribute("style","background-color:white;");
			$("#broker_submit").attr("disabled", false);
		}
  }

$('#loadicon').fadeOut('slow');
</script>


<div id="result_div"></div>