<?php
require('./connect.php');

$timestamp=date("Y-m-d H:i:s");
$type=escapeString($conn,strtoupper($_POST['type']));
$act_wt=escapeString($conn,($_POST['act_wt']));
$chrg_wt=escapeString($conn,($_POST['chrg_wt']));
$trip_id=escapeString($conn,($_POST['trip_id']));
$tno=escapeString($conn,strtoupper($_SESSION['diary']));

echo '<option value="">--select location--</option>';

if(empty($trip_id))
{
	echo "<script>
		alert('Trip not found.');
		window.location.href='./';
	</script>";
	exit();
}

if(empty($act_wt) || empty($chrg_wt))
{
	echo "<script>
		alert('Actual or Charge weight not found.');
		window.location.href='./';
	</script>";
	exit();
}

$get_trip_data = Qry($conn,"SELECT t.tno,t.from_station,t.from_id,t.to_station,t.to_id,t.from_poi,t.consignor_id,t.start_poi_date,
t.addr_book_id_consignor,s.pincode,s._lat,s._long 
FROM dairy.trip as t 
LEFT JOIN station AS s ON s.id=t.from_id 
WHERE t.id='$trip_id'");
		
if(!$get_trip_data){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($get_trip_data)==0)
{
	errorLog("Trip not found--> $trip_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		alert('Trip not found.');
		window.location.href='./';
	</script>";
	exit();
}

$row_loc_data = fetchArray($get_trip_data);

$to_loc_db = $row_loc_data['to_station'];
$to_loc_db_id = $row_loc_data['to_id'];
$from_loc_db = $row_loc_data['from_station'];

if($row_loc_data['tno']!=$tno)
{
	errorLog("Vehicle number not verified-->Trip tno is $row_loc_data[tno] and Client is $tno.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		alert('Vehicle not verified.');
		window.location.href='./';
	</script>";
	exit();
}
		
if(empty($_POST['frno']))
{
	echo "<script>
		$('#actual_for_add_more').val('0');
		$('#charge_for_add_more').val('0');
		$('#lrno_for_add_new').val('');
		$('#total_awt_add_more').html('$act_wt');
		$('#total_cwt_add_more').html('$chrg_wt');
		$('#add_more_lr_submit').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strpos($_POST['frno'],'OLR') !== false)
{
    $type="OWN";
}
else
{
	$type="MARKET";
}

if($type=='OWN')
{
	$frno = escapeString($conn,strtoupper($_POST['frno']));
	
	$array_1 = explode('_', $frno);

	$frno = $array_1[0];
	$frno_id = $array_1[1];
	
	$fetch=Qry($conn,"SELECT GROUP_CONCAT(lrno SEPARATOR',') as lrno,SUM(wt12) as act,SUM(weight) as charge FROM freight_form_lr WHERE frno='$frno'");
		
	if(!$fetch){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		echo "<script>alert('Error..');$('#loadicon').hide();</script>";
		exit();
	}
		
	$fetch_loc=Qry($conn,"SELECT DISTINCT tstation,to_id,con2_id FROM freight_form_lr WHERE frno='$frno' AND tstation!='$row_loc_data[to_station]'");
		
	if(!$fetch_loc){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		echo "<script>alert('Error..');$('#loadicon').hide();</script>";
		exit();
	}
		
	$row=fetchArray($fetch);
		
	$lrnos = $row['lrno'];
	$act = sprintf('%0.3f',$row['act']);
	$charge = sprintf('%0.3f',$row['charge']);
		
	$loc_value_default = "$to_loc_db#$to_loc_db_id#0";
	
	echo "<option style='background-color:yellow' value='$loc_value_default'>$to_loc_db</option>";
		
	while($row_fm_loc = fetchArray($fetch_loc))
	{
		$loc_value = "$row_fm_loc[tstation]#$row_fm_loc[to_id]#$row_fm_loc[con2_id]";
		echo "<option value='$loc_value'>$row_fm_loc[tstation]</option>";
	}
		
	$billing_type='';
	$freight = 0;
}
else if($type=='MARKET')
{
	$mbno = escapeString($conn,strtoupper($_POST['frno']));
	
	$array_1 = explode('_', $mbno);

	$mbno = $array_1[0];
	$mbno_id = $array_1[1];
	
	$qq = Qry($conn,"SELECT billing_type,frmstn,tostn,to_id,awt,cwt,tamt FROM mkt_bilty WHERE id='$mbno_id'");
	
	if(!$qq){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		echo "<script>alert('Error..');$('#loadicon').hide();</script>";
		exit();
	}
		
	$row=fetchArray($qq);
		
	$loc_value_default = "$to_loc_db#$to_loc_db_id#0";
		
	echo "<option style='background-color:yellow' value='$loc_value_default'>$to_loc_db</option>";
		
	$loc_value = "$row[tostn]#$row[to_id]#0";
		
	if($row['to_id']!=$to_loc_db_id)
	{
		echo "<option value='$loc_value'>$row[tostn]</option>";
	}
		
	/*?><script>$('#to_loc_for_add_more').html('<option value="">--select location--</option><option style="background-color:yellow" value="<?php echo $to_loc_db."#".$to_loc_db_id; ?>"><?php echo $to_loc_db; ?>  - current destination</option><?php foreach(array_unique(explode(",",$row["to_loc"])) as $toloc) { echo "<option value=$toloc>$toloc</option>"; } ?>');</script><?php*/
		
	$billing_type=$row['billing_type'];
	$freight=$row['tamt'];
	$act=sprintf('%0.3f',$row['awt']);
	$charge=sprintf('%0.3f',$row['cwt']);
	$lrnos=$mbno;
}

$total_act = sprintf('%0.3f',$act_wt+$act);
$total_chrg = sprintf('%0.3f',$chrg_wt+$charge);

	echo "<script>
		$('#actual_for_add_more').val('$act');
		$('#charge_for_add_more').val('$charge');
		$('#lrno_for_add_new').val('$lrnos');
		$('#total_awt_add_more').html('$total_act');
		$('#total_cwt_add_more').html('$total_chrg');
		$('#loadicon').hide();
	</script>";
?>