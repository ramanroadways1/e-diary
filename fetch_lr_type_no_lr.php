<?php
require("./connect.php");

$tno=escapeString($conn,strtoupper($_SESSION['diary']));

$sel_trip_dates=Qry($conn,"SELECT date(date) as date1 FROM dairy.trip WHERE tno='$tno' ORDER BY id ASC LIMIT 1");
if(numRows($sel_trip_dates)==0)
{
	Redirect("Date Rule not updated for LR Selection.","./");
	exit();
}

$row_date_rule=fetchArray($sel_trip_dates);

$date2=date("Y-m-d");
$days_ago = $row_date_rule['date1'];
$days_ago = date('Y-m-d', strtotime('-2 days', strtotime($days_ago)));

$type=escapeString($conn,strtoupper($_POST['type']));

if($type=='CON20')
{
			echo "<script>
					$('#actual_no_lr').val('2');
					$('#charge_no_lr').val('2');
					$('#lrno_no_lr').val('');
					$('#billing_type_text_no_lr').val('0');
					$('#freight_bilty_text_no_lr').val('0');
					$('#actual_no_lr').attr('readonly',false);
					$('#charge_no_lr').attr('readonly',false);
					$('#lrno_no_lr').attr('readonly',false);
					$('#at_loading_set_no_lr').val('0');
					$('#lr_type_div2').html('');
			</script>";
}
else if($type=='CON40')
{
			echo "<script>
					$('#actual_no_lr').val('4');
					$('#charge_no_lr').val('4');
					$('#lrno_no_lr').val('');
					$('#billing_type_text_no_lr').val('0');
					$('#freight_bilty_text_no_lr').val('0');
					$('#actual_no_lr').attr('readonly',false);
					$('#charge_no_lr').attr('readonly',false);
					$('#lrno_no_lr').attr('readonly',false);
					$('#at_loading_set_no_lr').val('0');
					$('#lr_type_div2').html('');
			</script>";
}
else if($type=='EMPTY')
{
			echo "<script>
					$('#actual_no_lr').val('0');
					$('#charge_no_lr').val('0');
					$('#billing_type_text_no_lr').val('0');
					$('#freight_bilty_text_no_lr').val('0');
					$('#lrno_no_lr').val('EMPTY');
					$('#actual_no_lr').attr('readonly',true);
					$('#charge_no_lr').attr('readonly',true);
					$('#lrno_no_lr').attr('readonly',true);
					$('#at_loading_set_no_lr').val('0');
					$('#lr_type_div2').html('');
			</script>";
}
else if($type=='OWN')
{
	$fetch_own=Qry($conn,"SELECT frno,date,fstation,tstation FROM freight_form_lr WHERE create_date 
	between '$days_ago' AND '$date2' AND truck_no='$tno' AND branch='$branch' ORDER BY id desc");
	echo "
	<div class='form-group col-md-4'>
		<label>OWN LR No <sup><font color='red'>*</font></sup></label>
		<select name='lr_type' id='lr_type_own_no_lr' class='form-control' required onchange=FetchLRDataFromLR_no_lr(this.value,'OWN')>
	";
	if(numRows($fetch_own)>0)
	{
		echo "<option value=''>--SELECT--</option>";
		
		while($row_own=fetchArray($fetch_own))
		{
			echo "<option value='$row_own[frno]'>$row_own[frno]: $row_own[date] $row_own[fstation] to $row_own[tstation]</option>";
		}
	}
	else
	{
		echo "<option value=''>--SELECT--</option>";
	}
	echo "</select>
	</div>
	";
}
else if($type=='MKT')
{
	$fetch_mkt=Qry($conn,"SELECT lrdate,bilty_no,frmstn,tostn FROM mkt_bilty WHERE date between '$days_ago' AND '$date2' AND 
	tno='$tno' AND branch='$branch' ORDER BY id desc");
	
	echo "
	<div class='form-group col-md-4'>
		<label>Select Bilty No <sup><font color='red'>*</font></sup></label>
	<select name='lr_type' id='lr_type_mkt_no_lr' class='form-control' required onchange=FetchLRDataFromLR_no_lr(this.value,'MKT')>
	";
	if(numRows($fetch_mkt)>0)
	{
		echo "<option value=''>--SELECT--</option>";
		
		while($row_mkt=fetchArray($fetch_mkt))
		{
			echo "<option value='$row_mkt[bilty_no]'>$row_mkt[bilty_no]: $row_mkt[lrdate], $row_mkt[frmstn] to $row_mkt[tostn]</option>";
		}
	}
	else
	{
		echo "<option value=''>--SELECT--</option>";
	}
	echo "</select>
	</div>
	";
}
?>