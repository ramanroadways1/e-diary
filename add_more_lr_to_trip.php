<?php
require('./connect.php');

$date=date("Y-m-d");
$timestamp = date("Y-m-d H:i:s"); 

$tno = escapeString($conn,strtoupper($_SESSION['diary']));

if(!isset($_SESSION['add_more_lr_diesel_required']))
{
	errorLog("Diesel consumption not found.",$conn,$page_name,__LINE__);
	echo "<script>alert('Diesel value not found.');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

require_once("./check_cache.php");

if(empty($_SESSION['add_more_bilty_type']) || empty($_SESSION['add_more_vou_no']) || empty($_SESSION['add_more_trip_id'])){
	echo "<script>window.location.href='./';</script>";exit();
}

$vou_no = $_SESSION['add_more_vou_no'];

$chk_oxygen_lr=Qry($conn,"SELECT id FROM oxygen_olr WHERE frno = '$vou_no'");

if(!$chk_oxygen_lr){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($chk_oxygen_lr)>0)
{
	AlertError("Add more LR not allowed in Oxygen vehicles !");
	echo "<script>
		$('#add_more_lr_submit').attr('disabled',false);
	</script>";
	exit();
}

$bilty_type = $_SESSION['add_more_bilty_type'];
$vou_id = $_SESSION['add_more_vou_id'];
$id = $_SESSION['add_more_trip_id'];

$check_trip = Qry($conn,"SELECT tno,from_station,from_id,to_id,consignor_id,addr_book_id_consignor,consignee_id,addr_book_id_consignee,
lr_type2,act_wt,charge_wt,dsl_consumption,status FROM dairy.trip WHERE id='$id'");

if(!$check_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Trip not Found.","./");
	exit();
}

if(numRows($check_trip)==0){
	AlertError("Trip not found !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

$row_trip = fetchArray($check_trip);

if($row_trip['status'] != "0")
{
	AlertError("This trip closed already !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

if($row_trip['lr_type2']<=0)
{
	AlertError("Invalid LR !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

if($row_trip['lr_type2']!=1)
{
	AlertError("Max limit is: 2 vouchers per trip !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

if($row_trip['tno']!=$tno){
	AlertError("Vehicle number not verified !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

$get_destination = Qry($conn,"SELECT name,pincode FROM station WHERE id='$_SESSION[add_more_destination_id]'");
if(!$get_destination){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Trip not Found.","./");
	exit();
}

if(numRows($get_destination)==0){
	AlertError("Destination not found !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

$row_destination = fetchArray($get_destination);

if($bilty_type=='OWN')
{
	$get_lr_data = Qry($conn,"SELECT GROUP_CONCAT(lrno SEPARATOR',') as lrno,SUM(wt12) as act_wt,SUM(weight) as charge_wt,con2_id,
	'' as billing_type,'0' as freight_amount FROM freight_form_lr WHERE frno='$vou_no'");
}
else
{
	$get_lr_data = Qry($conn,"SELECT bilty_no as lrno,billing_type,awt as act_wt,cwt as charge_wt,tamt as freight_amount,'' as con2_id 
	FROM mkt_bilty WHERE id='$vou_id'");
}

if(!$get_lr_data){errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);Redirect("Something went wrong..","./");exit();}

if(numRows($get_lr_data)==0){
	AlertError("LR not found !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
	
$row_lr = fetchArray($get_lr_data);

$freight_amount=$row_lr['freight_amount'];
$billing_type=$row_lr['billing_type'];
$lrnos = $row_lr['lrno'];
$lr_act_wt = $row_lr['act_wt'];
$lr_charge_wt = $row_lr['charge_wt'];
$con2_id = $row_lr['con2_id'];

$from_loc = $row_trip['from_station'];
$from_id = $row_trip['from_id'];
$consignor_id = $row_trip['consignor_id'];
$to_loc = $row_destination['name'];
$to_id = $_SESSION['add_more_destination_id'];
$end_poi = $_SESSION['add_more_end_point'];
$end_pincode = $_SESSION['add_more_lr_to_pincode'];

$actual_weight = $row_trip['act_wt']+$lr_act_wt;
$charge_weight = $row_trip['charge_wt']+$lr_charge_wt;

$diesel_consumption_db = $row_trip['dsl_consumption'];
$to_id_ext = $row_trip['to_id'];
$diesel_consumption = $_SESSION['add_more_lr_diesel_required'];

$chk_next_trip=Qry($conn,"SELECT from_id FROM dairy.trip WHERE id>$id AND tno='$tno' ORDER BY id ASC LIMIT 1");

if(!$chk_next_trip){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	AlertError("Error !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_next_trip)>0)
{
	$RowNextTrip=fetchArray($chk_next_trip);
	
	if($to_id!=$RowNextTrip['from_id']){
		AlertError("Destination is different from next trip\'s from location !");
		echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
}

if($freight_amount>0 and $billing_type=='TO_PAY'){
	$mb_to_pay="1";
	$mb_amount=$freight_amount;
}
else{
	$mb_to_pay=0;
	$mb_amount=0;
}

$insert_from_addr_book="NO";
$insert_to_addr_book="NO";
$update_addr_from_id="NO";
$update_addr_to_id="NO";

$by_pass_route = $_POST['by_pass_route'];
$distance=$_SESSION['add_more_trip_km'];

// echo $pincode_from." - ".$pincode_to;
// echo "<script>alert('OK');$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();

$diff_dsl_consumption=0;

if($diesel_consumption>$diesel_consumption_db)
{
	$diff_dsl_consumption = sprintf("%.2f",($diesel_consumption - $diesel_consumption_db));
}
else if($diesel_consumption<$diesel_consumption_db)
{
	errorLog("Diesel consumption is negative ! $diesel_consumption and existing value is: $diesel_consumption_db.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	AlertError("Error !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

StartCommit($conn);
$flag = true;

if($to_id_ext != $to_id)
{
	$encodedPolyLine = $_POST['encodedPolyLine'];
	
	$qry=Qry($conn,"UPDATE dairy.trip SET to_station='$to_loc',to_id='$to_id',to_poi='$end_poi',to_pincode='$end_pincode',km='$distance',
	dsl_consumption='$diesel_consumption',pincode='$end_pincode',act_wt='$actual_weight',charge_wt='$charge_weight',
	lr_type=CONCAT(lr_type,',','$vou_no'),lr_type2=lr_type2+1,mb_to_pay='$billing_type',mb_amount=mb_amount+'$freight_amount',
	lrno=CONCAT(lrno,',','$lrnos'),polyline='$encodedPolyLine' WHERE id='$id'");
}
else
{
	$qry=Qry($conn,"UPDATE dairy.trip SET act_wt='$actual_weight',charge_wt='$charge_weight',lr_type=CONCAT(lr_type,',','$vou_no'),lr_type2=lr_type2+1,
	mb_to_pay='$billing_type',mb_amount=mb_amount+'$freight_amount',lrno=CONCAT(lrno,',','$lrnos') WHERE id='$id'");
}

if(!$qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($by_pass_route=='YES')
{
	$chk_route = Qry($conn,"SELECT id FROM dairy.pending_master_lane WHERE from_id='$from_id' AND to_id='$to_id'");
	
	if(!$chk_route){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	if(numRows($chk_route)==0)
	{
		$insert_pending_route = Qry($conn,"INSERT INTO dairy.pending_master_lane(from_id,to_id,timestamp,tno,trip_id,branch,branch_user) VALUES 
		('$from_id','$to_id','$timestamp','$tno','$id','$branch','$_SESSION[user_code]')");
		
		if(!$insert_pending_route){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
	}
}

$update_dsl_qty = Qry($conn,"UPDATE dairy.own_truck SET diesel_left=diesel_left-'$diff_dsl_consumption',diesel_trip_id='$id' 
WHERE tno='$tno' AND diesel_trip_id!='-1'");

if(!$update_dsl_qty){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($bilty_type=='MARKET')
{
	$update=Qry($conn,"UPDATE mkt_bilty SET done='1',trip_id='$id' WHERE id='$vou_id'");
	if(!$update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
}
else if($bilty_type=='OWN')
{
	$update=Qry($conn,"UPDATE freight_form_lr SET done='1',trip_id='$id' WHERE frno='$vou_no'");
	if(!$update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
}
else
{
	$flag = false;
	errorLog("Invalid bilty type: $bilty_type.",$conn,$page_name,__LINE__);
}
	
if($flag)
{
	unset($_SESSION['add_more_lr_diesel_required']);
	MySQLCommit($conn);
	closeConnection($conn);
	SessionDestroyAddMoreLR();
	Redirect("LR ADDED SUCCESSFULLY.","./");
	// echo "<script>alert('LR ADDED SUCCESSFULLY');$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	echo "<script>$('#add_more_lr_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>