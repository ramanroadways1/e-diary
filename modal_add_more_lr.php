<button style="display:none" data-toggle="modal" data-target="#AddMoreLRModal" id="add_more_modal_button" type="button"></button>

<form style="font-size:13px" id="FormADDLRMORE" autocomplete="off">
<div id="AddMoreLRModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
	<div class="modal-content">
		<div class="modal-header bg-primary">
			<span style="font-size:15px">Add LR in Trip : ( रनिंग ट्रिप में और LR जोड़े )</span>
		</div>
    <div class="modal-body">
	  <div class="row">
	  
	  <div class="form-group col-md-8">
		
	 <div class="row">	
		<div class="form-group col-md-6">	
			<label>Select LR Type. <font color="red"><sup>*</sup></font></label>
			<select style="font-size:13px" onchange="BiltyType22(this.value)" name="bilty_type" id="bilty_type_add_more_lrs" class="form-control" required>
				<option style="font-size:13px" value="MARKET">Market Bilty</option>
				<option style="font-size:13px" value="OWN">Own Bilty</option>
			</select>
		</div>
		
		<script>
		function BiltyType22(elem)
		{
			$('#to_loc_for_add_more').html('<option value="">--select location--</option>');
			$('#add_more_lr_submit').attr('disabled',true);
			$('#market_div_input').val('');
			$('#own_div_input').val('');
			
			if(elem=='MARKET')
			{
				$('#mb_div2').show();
				$('#market_div_input').attr('required',true);
				$('#own_div2').hide();
				$('#own_div_input').attr('required',false);
			}
			else
			{
				$('#mb_div2').hide();
				$('#market_div_input').attr('required',false);
				$('#own_div2').show();
				$('#own_div_input').attr('required',true);
			}
		}
		</script>
		
<div id="mb_div2" class="form-group col-md-6">	
	<label>Select Market Bilty <font color="red"><sup>*</sup></font></label>
	<select style="font-size:13px" onchange="FetchLRDataAddMoreLR(this.value,'MARKET')" id="market_div_input" name="lrno_market" class="form-control" required>
				<option style="font-size:13px" value="">--select LR--</option>
				<?php
$fetch_market_bilty_to_add_more=Qry($conn,"SELECT id,lrdate,bilty_no,frmstn,tostn FROM mkt_bilty WHERE date between 
				'$bilty_days_ago' AND '$date' AND tno='$tno' AND branch='$branch' AND done=0 ORDER BY id ASC");
				
				if(!$fetch_market_bilty_to_add_more){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				
				if(numRows($fetch_market_bilty_to_add_more)>0)
				{
					while($RowMb_add_more=fetchArray($fetch_market_bilty_to_add_more))
					{
						echo "<option style='font-size:13px' value='$RowMb_add_more[bilty_no]_$RowMb_add_more[id]_0_0'>
						$RowMb_add_more[bilty_no]: ".convertDate("d/m/y",$RowMb_add_more['lrdate']).", 
						$RowMb_add_more[frmstn] to $RowMb_add_more[tostn]</option>";
					}
				}
				?>
			</select>
		</div>
		
		<div id="own_div2" style="display:none" class="form-group col-md-6">	
			<label>Select Own Bilty <font color="red"><sup>*</sup></font></label>
			<select style="font-size:13px" onchange="FetchLRDataAddMoreLR(this.value,'OWN')" name="lrno_own" id="own_div_input" class="form-control">
				<option style="font-size:13px" value="">--select LR--</option>
				<?php
$fetch_own_bilty_to_add_more=Qry($conn,"SELECT id,frno,date,fstation,tstation,consignor,con1_id,con2_id FROM freight_form_lr WHERE create_date between 
				'$bilty_days_ago' AND '$date' AND truck_no='$tno' AND branch='$branch' AND done=0 ORDER BY id ASC");
				
				if(!$fetch_own_bilty_to_add_more){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				
				if(numRows($fetch_own_bilty_to_add_more)>0)
				{
					while($RowOwn2_add_more=fetchArray($fetch_own_bilty_to_add_more))
					{
						echo "<option style='font-size:13px' value='$RowOwn2_add_more[frno]_$RowOwn2_add_more[id]_$RowOwn2_add_more[con1_id]_$RowOwn2_add_more[con2_id]'>
						$RowOwn2_add_more[frno]: ".convertDate("d/m/y",$RowOwn2_add_more['date']).", 
						$RowOwn2_add_more[fstation] to $RowOwn2_add_more[tstation]. Consignor: $RowOwn2_add_more[consignor]</option>";
					}
				}
				?>
			</select>
		</div>
		
		<script>
		function FetchLRDataAddMoreLR(elem,type)
		{
			var trip_id = $('#trip_id_for_add_more').val();
			var act_wt = Number($('#act_wt_db_add_more').val());
			var chrg_wt = Number($('#chrg_wt_db_add_more').val());
			
			$("#loadicon").show();
			jQuery.ajax({
			url: "./fetch_lr_data_for_add_more.php",
			data: 'frno=' + elem + '&trip_id=' + trip_id + '&type=' + type + '&act_wt=' + act_wt + '&chrg_wt=' + chrg_wt,
			type: "POST",
			success: function(data) {
				$("#to_loc_for_add_more").html(data);
			},
			error: function() {}
			});
		}
		</script>
		
	<script>	
		function LastLocForAddMore(loc)
		{
			var trip_id = $('#trip_id_for_add_more').val();
			
			if(loc!='')
			{
				$("#loadicon").show();
				jQuery.ajax({
				url: "./match_location_for_add_more.php",
				data: 'last_loc=' + loc + '&trip_id=' + trip_id,
				type: "POST",
				success: function(data) {
					$("#lr_result_add_more").html(data);
				},
				error: function() {}
				});
			}	
		}
	</script>	
		
		<div class="form-group col-md-6">	
			<label>Trip start from <font color="red">*</font></label>
			<input type="text" readonly id="from_loc_db_txt_for_add_more" class="form-control" required>
		</div>
		
		<div class="form-group col-md-6">	
			<label>Last or Trip end location <font color="red">*</font></label>
			<select style='font-size:13px' onchange="LastLocForAddMore(this.value)" id="to_loc_for_add_more" name="to_location" required class="form-control">
				<option style='font-size:13px' value="">--select location--</option>
			</select>
		</div>	
		
		<input type="hidden" name="trip_id" id="trip_id_for_add_more">	 <!-- trip id -->
		
		<input type="hidden" name="to_id" id="to_loc_id_for_add_new">
		<input type="hidden" name="get_stoppage_point_to_loc" id="get_stoppage_point_to_loc">
		
		<!-- Weight of this trip --->		
		<input type="hidden" id="act_wt_db_add_more">
		<input type="hidden" id="chrg_wt_db_add_more">
		<input type="hidden" id="by_pass_route_add_more_lr" name="by_pass_route">
		<!-- Weight of this trip --->
		
		
		<div class="form-group col-md-3">	
			<label>Act. wt <font color="red">* <span style="font-size:11px">Total:</span> <span style="font-size:11px" id="total_awt_add_more"></span></font></label>
			<input type="number" step="any" readonly name="actual" id="actual_for_add_more" class="form-control" required>
		</div>	
		
		<div class="form-group col-md-3">	
			<label>Chrg. wt <font color="red">* <span style="font-size:11px">Total:</span> <span style="font-size:11px" id="total_cwt_add_more"></span></font></label>
			<input style="font-size:13px" type="number" step="any" readonly name="charge" id="charge_for_add_more" class="form-control" required>
		</div>	
		
		<div class="form-group col-md-6">	
			<label>Trip KM <font color="red">*</font> <sup><span style="color:maroon;font-size:12px" id="from_loc_text"></span> <span id="to_loc_text" style="color:maroon;font-size:12px"></span></sup></label>
			<input style="font-size:13px" oninput="this.value=this.value.replace(/[^0-9,]/,'')" type="text" name="trip_km" id="trip_km_for_add_new" class="form-control" required readonly>
		</div>
		
		<div class="form-group col-md-4">	
			<label>Dest. Pincode <font color="red">*</font> <sup><span style="color:maroon;font-size:12px" id="pincode_text"></span></sup></label>
			<input oninput="this.value=this.value.replace(/[^0-9,]/,'')" maxlength="6" type="text" name="pincode" id="pincode_for_add_new" class="form-control" required readonly>
		</div>
		
		<div class="form-group col-md-8">	
			<label>LR No <font color="red">*</font></label>
			<input style="font-size:13px" type="text" readonly name="lrnos" id="lrno_for_add_new" class="form-control" required>
		</div>	
		
		<div class="form-group col-md-4">
			<label>Fuel Consumption <sup><font color="red">* (Qty in LTR)</font></sup></label>
			<input type="text" readonly id="add_more_lr_dsl_qty_required" class="form-control">
		</div>
		
		<div class="form-group col-md-4">	
			<label>&nbsp;</label>
			<?php
			if(!isMobile()){
			echo "<br />";
			}
			?>
			<button onclick="ValidateAddMoreLR()" disabled id="val_btn_add_more" type="button" class="btn btn-sm btn-primary"><i class="fa fa-check-circle-o" aria-hidden="true"></i> &nbsp; Validate Data</button>
		</div>	
		
		<script>
		function ValidateAddMoreLR()
		{
			var bilty_type = $('#bilty_type_add_more_lrs').val();
			if(bilty_type=='MARKET'){
				var vou_no = $('#market_div_input').val();
			}
			else{
				var vou_no = $('#own_div_input').val();
			}
			
			var trip_id = $('#trip_id_for_add_more').val();
			var to_loc = $('#to_loc_for_add_more').val();
			var to_loc_id = $('#to_loc_id_for_add_new').val();
			var act_wt = $('#actual_for_add_more').val();
			var chrg_wt = $('#charge_for_add_more').val();
			var total_act_wt = Number($('#total_awt_add_more').html());
			
			$("#loadicon").show();
			$('#val_btn_add_more').attr('disabled',true);
			$('#add_more_lr_submit').attr('disabled',true);
			$('#add_more_lr_submit').hide();
			jQuery.ajax({
			url: "./validate_add_more_lr.php",
			data: 'trip_id=' + trip_id + '&act_wt=' + act_wt + '&chrg_wt=' + chrg_wt + '&bilty_type=' + bilty_type + '&to_loc=' + to_loc + '&to_loc_id=' + to_loc_id + '&vou_no=' + vou_no + '&total_act_wt=' + total_act_wt,
			type: "POST",
			success: function(data) {
				$("#map_result_add_more").html(data);
			},
			error: function() {}
			});
		}
		</script>
		
	</div>	
</div>	
		
		<div class="form-group col-md-4">
			<div class="row">
				<div class="col-md-12">
					<div class="map" id="map_add_more_lrs" style="border:.5px solid #666;margin-top:5px;width:100%;height:330px">
						<img src="map_def.jpg" style="width:100%;max-height:330px !important" />
					</div>
				</div>
				
				<div class="col-md-12">
					<div style="font-size:13px;padding:5px;border-bottom:.5px solid #666;border-left:.5px solid #666;border-right:.5px solid #666" class="bg-success col-md-12">
						<div class="row">
							<div class="col-md-5">
								<font color="maroon">Trip distance:</font> <span id="km_google_add_more" style="font-size:12px">00 km</span> 
							</div>	
							<div class="col-md-7">
								<font color="maroon">Est. time:</font> <span id="duration_google_add_more" style="font-size:12px">NA</span>
							</div>	
						</div>	
					</div>
				</div>
			</div>
		</div>
		
		</div>	
		
		<input type="hidden" id="add_more_lr_trip_encodedPolyLine" name="encodedPolyLine">
		
		<div id="lr_result_add_more"></div>
		<div id="map_result_add_more"></div>

 	 </div>
		
     <div class="modal-footer">
		<input id="add_more_lr_submit" style="display:none" disabled type="submit" value="ADD LR" class="btn btn-sm btn-danger" />
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
	
    </div>
</form>			
  </div>
</div>