<?php
require_once("../../_connect.php");

if(empty($_POST['trip_no']))
{
	echo "<script>alert('Trip Number not found !');window.close();</script>";
	exit();
}

if(empty($_POST['secret_key']))
{
	echo "<script>alert('Client secret not found !');window.close();</script>";
	exit();
}

$trip_no = escapeString($conn,strtoupper($_POST['trip_no']));
$secret_key = escapeString($conn,($_POST['secret_key']));

$days = Qry($conn,"SELECT 
(SELECT tno FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'tno',
(SELECT date(date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from',
(SELECT date(end_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to',
(SELECT from_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from_station',
(SELECT date(hisab_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_date',
(SELECT hisab_branch FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_branch',
(SELECT driver_code FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'driver_code',
(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_desc',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_amount',
(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_desc',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_amount',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-TELEPHONE') as 'tel_exp_amount',
(SELECT credit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='TRISHUL_CARD_CREDIT') as 'trishul_credit',
(SELECT credit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='ASSET_FORM_CREDIT') as 'asset_form_credit',
(SELECT sum(debit) FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='CASH_DIESEL') as 'cash_diesel',
(SELECT to_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to_station'");

if(!$days){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error while processing request !');window.close();</script>";
	exit();
}

$row_days = fetchArray($days);

$tno = $row_days['tno'];

if($secret_key!=md5($trip_no.$tno.date("ymd")))
{
	echo "<script>alert('Client secret is not valid !');window.close();</script>";
	exit();
}

$fetch_comp = Qry($conn,"SELECT comp FROM dairy.own_truck WHERE tno='$tno'");

if(!$fetch_comp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error while processing request !');window.close();</script>";
	exit();
}

$row11=fetchArray($fetch_comp);
$company=$row11['comp'];

$start = $row_days['from'];
$end = $row_days['to'];
$from_station = $row_days['from_station'];
$to_station = $row_days['to_station'];
$hisab_branch = $row_days['hisab_branch'];
$driver_code = $row_days['driver_code'];
$hisab_date = date('d/m/Y', strtotime($row_days['hisab_date']));
$hisab_date2=$row_days['hisab_date'];

if($from_station=='')
{
	echo "<script type='text/javascript'>
			alert('Invalid Trip Number Entered.');
			window.close();
		</script>";
	exit();
}

$hisab_type=Qry($conn,"SELECT desct,timestamp FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'HISAB%'");

if(!$hisab_type){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error while processing request !');window.close();</script>";
	exit();
}

$hisab_rows=numRows($hisab_type);

if($hisab_rows==0)
{
	$hisab_type_cr_ho = Qry($conn,"SELECT desct,debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='CREDIT-HO'");
	
	if(!$hisab_type_cr_ho){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error while processing request !');window.close();</script>";
		exit();
	}

	if(numRows($hisab_type_cr_ho)==0)
	{
		echo "<script type='text/javascript'>
				alert('NO HISAB FOUND.');
				window.close();
			</script>";
		exit();
	}
	else
	{
		$hisab_type_db = "CREDIT_HO";
	}
}
else
{
	$hisab_type_db = "HISAB";
}

if($hisab_type_db=='HISAB')
{
	
$hisab_types=array();

while($row_hisab=fetchArray($hisab_type))
{
	$hisab_types[]=$row_hisab['desct'];
	$hisab_time=$row_hisab['timestamp'];
}

$hisab_time=$hisab_time;

$hisab_types=implode(" + ",$hisab_types);

if(strpos($hisab_types,'HISAB-PAID')!== false)
{
    $vouchers=Qry($conn,"SELECT f.tdvid,f.amt,f.mode,r.utr_date 
	FROM mk_tdv AS f 
	LEFT OUTER JOIN rtgs_fm as r ON r.fno = f.tdvid 
	WHERE f.newdate='$hisab_date2' AND f.timestamp='$hisab_time' AND f.truckno='$tno'");
	
	if(!$vouchers){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error while processing request !');window.close();</script>";
		exit();
	}
	
	if(numRows($vouchers)==0)
	{
		errorLog("HISAB Voucher not found. Truck No : $tno. Trip No : $trip_no.",$conn,$page_name,__LINE__);
		echo "<script>alert('Hisab Voucher not found !');window.close();</script>";
		exit();
	}
	
	$voucher_types=array();
	
	while($row_vouchers=fetchArray($vouchers))
	{
		if($row_vouchers['mode']=='NEFT')
		{
			if($row_vouchers['utr_date']!='' AND $row_vouchers['utr_date']!=0)
			{	
				$utr_date = date("d-m-y",strtotime($row_vouchers['utr_date']));
			}
			else
			{
				$utr_date="NA";
			}
			
			$voucher_types[]=$row_vouchers['tdvid'].": ".$row_vouchers['amt']."(".$row_vouchers['mode']."), Date: $utr_date.";
		}
		else
		{
			$voucher_types[]=$row_vouchers['tdvid'].": ".$row_vouchers['amt']."(".$row_vouchers['mode'].")";
		}
	}
	
	$voucher_types=implode(", ",$voucher_types);
}
else
{
	$voucher_types="";
}

}
else
{
	$fetch_record_ho_cr = fetchArray($hisab_type_cr_ho);
	
	$Cr_amount = $fetch_record_ho_cr['debit'];
	
	$hisab_types="CREDIT-HO";
	$voucher_types="Amount : $Cr_amount.";
}

$diff = (strtotime($end)- strtotime($start))/24/3600; 
$diff_days=$diff+1;

$fetch_driver=Qry($conn,"SELECT name FROM dairy.driver WHERE code='$driver_code'");

if(!$fetch_driver){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error while processing request !');window.close();</script>";
	exit();
}
	
$row_fetch_driver = fetchArray($fetch_driver);
$driver_name = $row_fetch_driver['name'];

$opening = Qry($conn,"SELECT o.id,o.opening,o.closing,o.gps_km_of_trip,o.avg,u.name as hisab_person 
FROM dairy.opening_closing AS o 
LEFT OUTER JOIN emp_attendance AS u ON u.code=o.closing_user 
WHERE o.trip_no='$trip_no'");

if(!$opening){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error while processing request !');window.close();</script>";
	exit();
}

$row_bal = fetchArray($opening);
$opening_bal = $row_bal['opening'];
$closing_bal = $row_bal['closing'];
$gps_km = $row_bal['gps_km_of_trip'];
$avg = $row_bal['avg'];
$hisab_person = $row_bal['hisab_person'];
$opening_closing_id = $row_bal['id'];

$last_trip_records=Qry($conn,"SELECT o.trip_no,o.tno,o.closing,o.closing_branch,o.closing_date,e.name 
FROM dairy.opening_closing AS o 
LEFT JOIN emp_attendance AS e ON e.code=o.closing_user 
WHERE o.id<'$opening_closing_id' AND o.driver='$driver_code' ORDER BY o.id DESC LIMIT 1");

if(!$last_trip_records){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error while processing request !');window.close();</script>";
	exit();
}

$row_last_trip_records = fetchArray($last_trip_records);

$last_trip_no = $row_last_trip_records['trip_no'];
$last_tno = $row_last_trip_records['tno'];
$last_closing_balance = $row_last_trip_records['closing'];
$last_closing_branch = $row_last_trip_records['closing_branch'];
$last_closing_date = $row_last_trip_records['closing_date'];
$last_closing_emp_name = $row_last_trip_records['name'];

$credit_debit=Qry($conn,"SELECT SUM(credit) as credit,SUM(debit) as debit FROM dairy.driver_book WHERE trip_no='$trip_no' 
AND desct NOT IN('DRIVER_UP','DRIVER_DOWN','CREDIT-HO','HISAB-PAID','HISAB-CARRY-FWD')");

if(!$credit_debit){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error while processing request !');window.close();</script>";
	exit();
}

$row_cr_dr=fetchArray($credit_debit);
$credit=$row_cr_dr['credit'];
$debit=$row_cr_dr['debit'];
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="robots" content="noindex,nofollow"/>
<link rel="icon" type="image/png" href="../logo_rrpl.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Hisab Print : <?php echo $tno; ?> || E-Diary || RAMAN ROADWAYS PVT. LTD.</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="../../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<?php include("../load_icon.php"); ?>

<style>
	.form-control{
		border:1px solid #000;
		text-transform:uppercase;
	}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}

@media print {
  .btn{
    display: none;
  }
  #links{
	 display: none; 
  }
  body {
   zoom:85%;
 }
  
}
</style>

<a href="../" id="links"><button style="margin:10px;color:#000" type="button" class="btn btn-sm btn-warning"><i class="fa fa-backward" aria-hidden="true"></i> &nbsp; Go back</button></a>
<button id="btn" onclick="print();" class="btn btn-sm btn-danger"><i class="fa fa-print" aria-hidden="true"></i> &nbsp; Print Hisab</button>
<button style="margin:10px;" type="button" onclick="window.open('', '_self').window.close();" class="btn btn-sm btn-primary"><i class="fa fa-window-close-o" aria-hidden="true"></i> &nbsp; Close window</button>
	
<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
	
<div class="container-fluid">
	<div class="row">
		<div class="form-group col-md-12">
			<table border="0" style="width:100%;font-size:7pt">
				<tr>
				<td>
				<?php
				if($company=='RRPL'){
					echo "<b>RAMAN ROADWAYS PRIVATE LIMITED</b>";
				}
				else{
					echo "<b>RAMAN ROADWAYS</b>";	
				}
				?>
				<br>
				805, Samedh complex, Nr. Associate petrol pump, C.G. Road AHMEDABAD.
				<br>
				Ph: 079-26460631.	
				</td>
				<td>
				<b>Accounting Year:</b> <?php echo GetFinYear(); ?>
				<br>
				<b>Generated On: </b><?php echo date("d-m-y h:i A"); ?>
				
			</td>
			</tr>
			</table>
		</div>
		<div class="form-group col-md-12">
		<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
			<center><h5 style="padding:5px;font-size:12px;">
			Trip No : <?php echo $trip_no; ?>, 
			<?php echo $from_station." to ".$to_station." (".$diff_days." days)"; ?>
			</h5></center>
		</div>
			<table class="table table-bordered" style="font-size:7pt;">
				
				<?php
				if($last_trip_no!='')
				{
				?>
				<tr>
					<td colspan="8">Last Vehicle: <?php echo $last_tno; ?>, 
					Trip_no: <?php echo $last_trip_no; ?>, Closing: <?php echo $last_closing_balance; ?>, 
					Hisab branch: <?php echo $last_closing_branch; ?>, Closing date: <?php echo date("d-m-y",strtotime($last_closing_date)); ?>, 
					Hisab user: <?php echo $last_closing_emp_name; ?></td>
				</tr>	
				<?php				
				}
				?>
				<tr>
					<th>Truck No (ट्रक): </th><td><?php echo $tno; ?></td>
					<th>Trip No (ट्रिप न.): </th><td><?php echo $trip_no; ?></td>
					<th>DriverName (ड्राइवर): </th><td colspan="3"><?php echo $driver_name; ?></td>
				</tr>	
				
				<tr>
					<th>Hisab By <br>(हिसाब ब्रांच/डेट ): </th><td><?php echo $hisab_date." (".$hisab_branch.")<br>By: $hisab_person"; ?></td>
					<th>Trip Start <br>(ट्रिप शुरू): </th><td><?php echo date('d/m/y', strtotime($start)); ?></td>
					<th>Trip END <br>(ट्रिप पूर्ण): </th><td><?php echo date('d/m/y', strtotime($end)); ?></td>
					<th>Trip Duration<br> (ट्रिप अवधि): </th><td><?php echo $diff_days." days"; ?></td>
				</tr>
				
				<tr>
					<th>Opening Bal <br> (ओपनिंग): </th><td><?php echo $opening_bal; ?></td>
					<th>Credit<br> (जमा): </th><td><?php echo $credit; ?></td>
					<th>Debit<br> (खर्च): </th><td><?php echo $debit; ?></td>
					<th>Closing Bal <br>(क्लोजिंग): </th><td><?php echo $closing_bal; ?></td>
				</tr>
				
				<tr>
					<th>DA </th><td colspan="2"><?php echo $row_days['da_amount']." : ".$row_days['da_desc']; ?></td>
					<th>SALARY </th><td colspan="2"><?php echo $row_days['sal_amount']." : ".$row_days['sal_desc']; ?></td>
					<th>Telephone & Air-Grease </th><td colspan="2"><?php echo $row_days['tel_exp_amount']*2; ?></td>
				</tr>
				
				<tr>
					<th>GPS KM <br>(किलोमीटर):</th><td><?php echo "<font size='3'>".$gps_km." KM </font>"; ?></td>
					<th>Average<br> (एवरेज): </th><td><?php echo "<font size='3'>".$avg." kmpl </font>"; ?></td>
					<th>Hisab Description :</th><td colspan="3"><?php echo $hisab_types."<br>".$voucher_types; ?></td>
				</tr>
				
				<tr>
					<th>Other Credits :</th> 
					<td colspan="3">
					<?php if($row_days['trishul_credit']>0){ echo "Trishul-Card credit : $row_days[trishul_credit]"; }; ?>
					<?php if($row_days['asset_form_credit']>0){ echo "&nbsp; Asset-Form credit : $row_days[asset_form_credit]"; }; ?>
					</td>
					<th>Other Debits :</th> 
					<td colspan="3">
					<?php if($row_days['cash_diesel']>0){ echo "Cash Diesel : $row_days[cash_diesel]"; }; ?>
					</td>
				</tr>
				
			</table>
			
			<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
			<center><h5 style="padding:5px;font-size:12px;">
			Trip Summary (ट्रिप ब्यौरा) :</h5></center>
		</div>
			<table class="table table-bordered" style="font-size:7pt;">
				<?php
				$qry_trips = Qry($conn,"SELECT id,trip_id,branch,from_station,to_station,act_wt,charge_wt,lr_type,lrno,fix_lane,
				date(date) as start_date,date(end_date) as end_date,freight,cash,cheque,rtgs,diesel,diesel_qty,dsl_consumption,driver_naame,
				expense,toll_tax
				FROM dairy.trip_final WHERE trip_no='$trip_no'");
				
				if(!$qry_trips){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				
				$i=1;
				echo "<tr>
					<th>Trip (ट्रिप) </th>
					<th>From (कहा से)</th>
					<th>To (कहा तक)</th>
					<th>Wt (वजन) </th>
					<th>LR(एल आर )</th>
					<th>Start(ट्रिप शुरू)</th>
					<th>End (ट्रिप पूर्ण)</th>
					<th>Credit (जमा)</th>
					<th>Diesel_Consmp. (डीजल खपत)</th>
					<th>Diesel (डीजल)</th>
					<th>Toll (टोल टैक्स)</th>
					<th>Fix_Lane (फिक्स लेन)</th>
					</tr>";
				
				while($row_trips=fetchArray($qry_trips))
				{
					$end_date = date('d/m/y', strtotime($row_trips['end_date']));
					$start_date = date('d/m/y', strtotime($row_trips['start_date']));
					
					$sum_credit_trip = $row_trips['freight']+$row_trips['cash']+$row_trips['cheque']+$row_trips['rtgs']+$row_trips['driver_naame'];

					if($row_trips['dsl_consumption']==0){
						$dsl_consumption = "NA";
					}else{
						$dsl_consumption = $row_trips['dsl_consumption']." LTR";
					}
					
					if($row_trips['fix_lane']!=0){
						$is_fix_lane = "<font color='maroon'>Fix_Lane</font>";
					}else{
						$is_fix_lane = "";
					}
					
					echo "<tr>
						<td>$i</td>
						<td>$row_trips[from_station]</td>
						<td>$row_trips[to_station]</td>
						<td>$row_trips[charge_wt]</td>
						<td>$row_trips[lrno]</td>
						<td>$start_date</td>
						<td>$end_date</td>
						<td>$sum_credit_trip</td>
						<td>$dsl_consumption</td>
						<td>$row_trips[diesel] - ($row_trips[diesel_qty] LTR)</td>
						<td>$row_trips[toll_tax]</td>
						<td>$is_fix_lane</td>
					</tr>";
				$i++;	
				}
				?>
			</table>
		</div>
		
		<div class="col-md-12">
		<table class="table" style="width:100%">
			<tr>
			<td>
			<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
				<center><h5 style="padding:5px;font-size:12px;">
				Advance/NAAME to Driver (ड्राइवर को एडवांस / नामे) :</h5></center>
			</div>
			<table border="1" style="width:100%;font-size:7pt;">
				<?php
				$qry_adv_cash = Qry($conn,"SELECT amount,date,trans_type,branch FROM dairy.cash WHERE trip_id IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0) ORDER BY date ASC");
				$qry_adv_cheque = Qry($conn,"SELECT amount,cheq_no,date,branch FROM dairy.cheque WHERE trip_id IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0) ORDER BY date ASC");
				$qry_adv_rtgs = Qry($conn,"SELECT amount,date,branch FROM dairy.rtgs WHERE trip_id IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0) ORDER BY date ASC");
				$qry_adv_freight = Qry($conn,"SELECT amount,vou_id,date,branch FROM dairy.freight_adv WHERE trip_id IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0) ORDER BY date ASC");
				$qry_adv_diesel = Qry($conn,"SELECT rate,qty,amount,date,narration,branch FROM dairy.diesel WHERE trip_id IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0) ORDER BY date ASC");
				$qry_exp = Qry($conn,"SELECT exp_name,amount,date,branch FROM dairy.trip_exp WHERE trip_id IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0) ORDER BY date ASC");
				$qry_freight_collected = Qry($conn,"SELECT amount,date,vou_id,branch FROM dairy.freight_rcvd WHERE trip_id IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0) ORDER BY date ASC");
				$qry_naame = Qry($conn,"SELECT amount,date,lrno,naame_type,branch,narration FROM dairy.driver_naame WHERE trip_id 
				IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0) ORDER BY date ASC");
				
				$qry_naame_sum = Qry($conn,"SELECT SUM(amount) as total_naame FROM dairy.driver_naame WHERE trip_id 
				IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0) ORDER BY date ASC");
						
				$qry_happay_wdl = Qry($conn,"SELECT ROUND(amount) as amount,date(timestamp) as date FROM dairy.happay_card_withdrawal 
				WHERE trip_no='$trip_no' ORDER BY timestamp ASC");
				
				if(!$qry_happay_wdl){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				
				if(!$qry_adv_cash)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				if(!$qry_adv_cheque)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				if(!$qry_adv_rtgs)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				if(!$qry_adv_freight)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				if(!$qry_adv_diesel)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				if(!$qry_exp)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				if(!$qry_freight_collected)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				if(!$qry_naame)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				if(!$qry_naame_sum)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				
				$row_total_naame = fetchArray($qry_naame_sum);
				$total_naame = $row_total_naame['total_naame'];
				
				$sum_adv = Qry($conn,"SELECT SUM(freight+cash+cheque+rtgs) as total_adv FROM dairy.trip_final WHERE trip_no='$trip_no'");
				
				if(!$sum_adv)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				
				$row_adv_sum = fetchArray($sum_adv);
				
				echo "<tr>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Amount</th>
					<th style='padding:3px;'>Type</th>
					</tr>
					";
				if(numRows($qry_adv_cash)>0)
				{
					while($row_adv_cash = fetchArray($qry_adv_cash))
					{
						$cash_date = date('d/m/y', strtotime($row_adv_cash['date']));
						
						if($row_adv_cash['trans_type']=='CASH'){
							$adv_trans_type="CASH";
						}else{
							$adv_trans_type="ATM";
						}
						
						echo "<tr>
								<td style='padding:3px;'>$cash_date</td>
								<td style='padding:3px;'>$row_adv_cash[branch]</td>
								<td style='padding:3px;'>$row_adv_cash[amount]</td>
								<td style='padding:3px;'>$adv_trans_type</td>
							</tr>
							";
					}
				}	

				if(numRows($qry_adv_cheque)>0)
				{
					while($row_adv_cheque=fetchArray($qry_adv_cheque))
					{
						$cheque_date=date('d/m/y', strtotime($row_adv_cheque['date']));
						
						echo "<tr>
								<td style='padding:3px;'>$cheque_date</td>
								<td style='padding:3px;'>$row_adv_cheque[branch]</td>
								<td style='padding:3px;'>$row_adv_cheque[amount]</td>
								<td style='padding:3px;'>CHQ ($row_adv_cheque[cheq_no])</td>
							</tr>
							";
					}
				}	

				if(numRows($qry_adv_rtgs)>0)
				{
					while($row_adv_rtgs=fetchArray($qry_adv_rtgs))
					{
						$rtgs_date=date('d/m/y', strtotime($row_adv_rtgs['date']));
						
						echo "<tr>
								<td style='padding:3px;'>$rtgs_date</td>
								<td style='padding:3px;'>$row_adv_rtgs[branch]</td>
								<td style='padding:3px;'>$row_adv_rtgs[amount]</td>
								<td style='padding:3px;'>RTGS</td>
								
							</tr>
							";
					}
				}	

				if(numRows($qry_adv_freight)>0)
				{
					while($row_adv_freight=fetchArray($qry_adv_freight))
					{
						$freight_date=date('d/m/y', strtotime($row_adv_freight['date']));
						
						echo "<tr>
								<td style='padding:3px;'>$freight_date</td>
								<td style='padding:3px;'>$row_adv_freight[branch]</td>
								<td style='padding:3px;'>$row_adv_freight[amount]</td>
								<td style='padding:3px;'>FT (भाड़ा) : $row_adv_freight[vou_id]</td>
								
							</tr>
							";
					}
				}
				
				if(numRows($qry_naame)>0)
				{
					while($row_naame2=fetchArray($qry_naame))
					{
						$naame_date=date('d/m/y', strtotime($row_naame2['date']));
						
						if($row_naame2['lrno']!=''){
							$naame_lrnos = " : $row_naame2[lrno]";
						}else{
							$naame_lrnos = "";
						}
						echo "<tr>
								<td style='padding:3px;'>$naame_date</td>
								<td style='padding:3px;'>$row_naame2[branch]</td>
								<td style='padding:3px;'>$row_naame2[amount]</td>
								<td style='padding:3px;'>$row_naame2[naame_type]-NAAME (नामे)$naame_lrnos</td>
							</tr>
							";
					}
				}
			
			echo "<tr>
					<td style='padding:3px;' colspan='2'><b>Total Calculation :</b></td>
					<td style='padding:3px;'><b>Rs: ";echo $row_adv_sum['total_adv']+$total_naame; echo "/-</b></td>
					<td></td>
				</tr>
				";
			
				?>
			</table>
		</td>
		
		<!-- FREIGHT COLLECTED AT HO DIV -->
		<td>
			<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
				<center><h5 style="padding:5px;font-size:12px;">
				Freight Collected (भाड़ा जमा) :</h5></center>
			</div>
			<table border="1" style="width:100%;font-size:7pt;">
				<?php
				
				if(numRows($qry_freight_collected)>0)
				{
					$sum_ft_coll=Qry($conn,"SELECT SUM(freight_collected) as total_ft_coll FROM dairy.trip_final WHERE trip_no='$trip_no'");
					
					if(!$sum_ft_coll){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						echo "<script>alert('Error while processing request !');window.close();</script>";
						exit();
					}
					
				$row_coll_sum=fetchArray($sum_ft_coll);
				
					echo "<tr>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Amount</th>
					<th style='padding:3px;'>LR</th>
					</tr>
					";
					
					while($row_freight_collected=fetchArray($qry_freight_collected))
					{
					$freight_coll_date=date('d/m/y', strtotime($row_freight_collected['date']));
					
					echo "<tr>
						<td style='padding:3px;'>$freight_coll_date</td>
						<td style='padding:3px;'>$row_freight_collected[branch]</td>
						<td style='padding:3px;'>$row_freight_collected[amount]</td>
						<td style='padding:3px;'>$row_freight_collected[vou_id]</td>
						</tr>
						";
					}
					
					echo "<tr>
					<td style='padding:3px;' colspan='2'><b>Total Calculation :</b></td>
					<td style='padding:3px;'><b>Rs: $row_coll_sum[total_ft_coll] /-</b></td>
					</tr>
				";
				}
				?>
			</table>
		</td>
		</tr>
		
		<tr>
			<!-- DIESEL DIV -->
			<td>
			<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
				<center><h5 style="padding:5px;font-size:12px;">
				Diesel Summary (डीजल) :</h5></center>
			</div>
			<table border="1" style="width:100%;font-size:7pt;">
				<?php
				if(numRows($qry_adv_diesel)>0)
				{
				$sum_diesel=Qry($conn,"SELECT SUM(qty) as dsl_qty,SUM(amount) as dsl_amt FROM dairy.diesel WHERE trip_id IN
				(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' AND trip_id!=0)");
				if(!$sum_diesel)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>alert('Error while processing request !');window.close();</script>";
					exit();
				}
				$row_sum_diesel=fetchArray($sum_diesel);
				
					echo "<tr>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Qty (लीटर)</th>
					<th style='padding:3px;'>Rate (रेट)</th>
					<th style='padding:3px;'>Amt</th>
					<th style='padding:3px;'>Narr (ब्यौरा)</th>
					</tr>
					";
					
					while($row_diesel=fetchArray($qry_adv_diesel))
					{
					$diesel_date=date('d/m/y', strtotime($row_diesel['date']));
					
					echo "<tr>
						<td style='padding:3px;'>$diesel_date</td>
						<td style='padding:3px;'>$row_diesel[branch]</td>
						<td style='padding:3px;'>$row_diesel[qty]</td>
						<td style='padding:3px;'>$row_diesel[rate]</td>
						<td style='padding:3px;'>$row_diesel[amount]</td>
						<td style='padding:3px;'>$row_diesel[narration]</td>
						</tr>
						";
					}
					
					echo "<tr>
					<td style='padding:3px;' colspan='2'><b>Total Calculation :</b></td>
					<td style='padding:3px;'><b>".sprintf("%.2f",$row_sum_diesel['dsl_qty'])." ltr</b></td>
					<td></td>
					<td style='padding:3px;'><b>Rs: $row_sum_diesel[dsl_amt] /-</b></td>
					<td></td>
					</tr>";
				}
				?>
			</table>
			</td>
			
			<!-- EXPENSE DIV -->
			
			<td>
			<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
				<center><h5 style="padding:5px;font-size:12px;">
				Trip Expenses (ट्रिप खर्चा) :</h5></center>
			</div>
			<table border="1" style="width:100%;font-size:7pt;">
				<?php
				if(numRows($qry_exp)>0 || numRows($qry_happay_wdl)>0)
				{
					echo "<tr>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Exp (खर्चा)</th>
					<th style='padding:3px;'>Amount</th>
					</tr>";
					
					$total_exp_amount = 0;
					while($row_exp=fetchArray($qry_exp))
					{
					$exp_date=date('d/m/y', strtotime($row_exp['date']));
					$total_exp_amount = $total_exp_amount + $row_exp['amount'];
					echo "<tr>
						<td style='padding:3px;'>$exp_date</td>
						<td style='padding:3px;'>$row_exp[branch]</td>
						<td style='padding:3px;'>$row_exp[exp_name]</td>
						<td style='padding:3px;'>$row_exp[amount]</td>
						</tr>
						";
					}
					
					while($row_happay_wdl=fetchArray($qry_happay_wdl))
					{
					$total_exp_amount = $total_exp_amount + $row_happay_wdl['amount'];
						
					$wdl_date=date('d/m/y', strtotime($row_happay_wdl['date']));
					
					echo "<tr>
						<td style='padding:3px;'>$wdl_date</td>
						<td style='padding:3px;'>ADMIN</td>
						<td style='padding:3px;'>HAPPAY_WDL</td>
						<td style='padding:3px;'>$row_happay_wdl[amount]</td></tr>";
					}
					
					echo "<tr>
					<td colspan='3' style='padding:3px;'><b>Total Calculation :</b></td>
					<td style='padding:3px;'><b>Rs: $total_exp_amount /-</b></td>
					</tr>";
				}
				
			closeConnection($conn);	
			?>
			</table>
			</td>
		</tr>
		</table>
		</div>
		
	</div>
</div>