<?php
require_once '../connect.php';

$date2=date("Y-m-d");

$tno=escapeString($conn,strtoupper($_POST['truck_no']));

$from_date=date('Y-m-d', strtotime($_POST['duration'], strtotime($date2)));
$to_date=date("Y-m-d");

$result2 = Qry($conn,"SELECT trip_no FROM dairy.log_hisab WHERE date BETWEEN '$from_date' AND '$to_date' AND tno='$tno' ORDER BY date ASC");

if(!$result2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","../");
	exit();
}

if(numRows($result2)==0)
{
	echo "<script type='text/javascript'>
			alert('No result found...');
			window.close();
		</script>";
	exit();
}
?>
<html>
<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="robots" content="noindex,nofollow"/>
<link rel="icon" type="image/png" href="../logo_rrpl.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Hisab Summary : <?php echo $tno; ?> || E-Diary || RAMAN ROADWAYS PVT. LTD.</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="../../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
<meta name="google" content="notranslate">
	
<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
 
 <style>
td {
  white-space: normal !important; 
  word-wrap: break-word;  
}
</style>

</head>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<a href="../"><button class="btn btn-primary btn-sm" style="color:#FFF;margin-top:10px;margin-left:10px;letter-spacing:1px;">Dashboard</button></a>

    <div class="container-fluid">	
        <div class="row">
          <div class="col-md-12">
               <center>
                       <h4 style="color:maroon;font-size:20px;letter-spacing:1px">Hisab Summary : <?php echo $tno; ?></h4>
					   
               </center>
<div class="table-responsive" style="overflow:auto">                         
<br>
<?php
echo "<table class='table table-bordered' style='font-family:Arial !important;font-size:12px;'>";
echo "<tr>
	<th>#</th>
	<th>Trip No</th>
	<th>Driver Name</th>
	<th>DA</th>
	<th>SALARY</th>
	<th>From</th>
	<th>To</th>
	<th>Trip Start</th>
	<th>Trip End</th>
	<th>Hisab Date</th>
	<th>Hisab Branch</th>
</tr>";
$sn=1;
while($row2 = fetchArray($result2))
  {
$trip_no=$row2['trip_no'];

$days=Qry($conn,"SELECT 
(SELECT date(date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from',
(SELECT date(end_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to',
(SELECT from_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from_station',
(SELECT date(hisab_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_date',
(SELECT hisab_branch FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_branch',
(SELECT driver_code FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'driver_code',
(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_desc',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_amount',
(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_desc',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_amount',
(SELECT to_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to_station'");

if(!$days){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row = fetchArray($days);

$start=date('d-m-y', strtotime($row['from']));
$end=date('d-m-y', strtotime($row['to']));
$from_station = $row['from_station'];
$to_station = $row['to_station'];
$hisab_branch = $row['hisab_branch'];
$driver_code = $row['driver_code'];
$hisab_date = date('d-m-y', strtotime($row['hisab_date']));	 
 
$d_qry=Qry($conn,"SELECT name FROM dairy.driver WHERE code='$driver_code'");
if(!$d_qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row_d=fetchArray($d_qry);

echo "<tr style='font-family: Open Sans, sans-serif !important;font-size:11px;'>
	<td>$sn</td>
	<td>
<form action='hisab_view.php' target='_blank' method='POST'>
<input name='trip_no' value='$trip_no' type='hidden' />
<input name='secret_key' value='".md5($trip_no.$tno.date("ymd"))."' type='hidden' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$trip_no</a>
</form>
</td>
	<td>$row_d[name]</td>
	<td><b>$row[da_amount]</b> : $row[da_desc]</td>
	<td><b>$row[sal_amount]</b> : $row[sal_desc]</td>
	<td>$from_station</td>
	<td>$to_station</td>
	<td>$start</td>
	<td>$end</td>
	<td>$hisab_date</td>
	<td>$hisab_branch</td>
</tr>"; 
$sn++;
}
echo "</table>";

closeConnection($conn);
?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>