<?php
require_once("../connect.php");

// if($branch!='HEAD')
// {
	// echo "<script type='text/javascript'>
			// alert('Hisab is under maintenance till 5 PM Today !');
			// window.location.href='./';
		// </script>";
	// exit();
// }

$today_date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

if(!isset($_POST['KEY']))
{
	echo "<script type='text/javascript'>
			window.location.href='../';
		</script>";
	exit();
}

if($_POST['KEY']!='myfile')
{
	echo "<script type='text/javascript'>
			window.location.href='../';
		</script>";
	exit();
}

$tno=escapeString($conn,strtoupper($_POST['tno']));
$trip_no=escapeString($conn,strtoupper($_POST['trip_no']));

if($tno!=$_SESSION['diary'])
{
	echo "<script type='text/javascript'>
			window.location.href='../';
		</script>";
	exit();
}

$chk_lock_air_grease = Qry($conn,"SELECT trishul_card,lock_ag,lock_tel FROM dairy.own_truck WHERE tno='$tno'");
if(!$chk_lock_air_grease){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

$row_air_grease = fetchArray($chk_lock_air_grease);

$lock_ag = $row_air_grease['lock_ag'];
$lock_tel = $row_air_grease['lock_tel'];
$trishul_card = $row_air_grease['trishul_card'];

// if($tno=="GJ18BT1140" || $tno=="GJ18AX4545")
// {
	// echo "<script type='text/javascript'>
			// window.location.href='../';
		// </script>";
	// exit();
// }

// if($tno=="RJ38GA0436" || $tno=="GJ18AX9396")
// {
	// echo "<script type='text/javascript'>
			// alert('This vehicle : $tno is not enabled for loaded vehicle hisab !');
			// window.location.href='../';
		// </script>";
	// exit();
// }

$chk_running_script = Qry($conn,"SELECT id FROM dairy.running_script");
if(!$chk_running_script){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_running_script)>0)
{
	echo "<script type='text/javascript'>
			alert('Please wait.. Try after some time !!');
			window.close();
		</script>";
	exit();
}

$chk_standby_approval = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no' AND stand_by_approval='1'");
if(!$chk_standby_approval){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_standby_approval)>0)
{
	$stand_by_approval="1";
}
else
{
	$stand_by_approval="0";
}

$chk_for_hisab_cache = Qry($conn,"SELECT id,final_running FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$chk_for_hisab_cache)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($chk_for_hisab_cache)>0)
{
	$row_cache_type = fetchArray($chk_for_hisab_cache);
	
	if($row_cache_type['final_running']=='1')
	{
		$delete_cache1 = Qry($conn,"DELETE FROM dairy.hisab_cache WHERE tno='$tno'");
		if(!$delete_cache1){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error While Processing Request.","./");
			exit();
		}

		echo "<script>
			alert('Final Hisab\'s Cache found and you\'ve clicked on Loaded Vehicle Hisab. Take hisab again !!');
			window.location.href='../';
		</script>";
		exit();	
	}
	
	echo "<form action='./final_hisab_proceed_running.php' id='RedirectCache' method='POST'>
			<input type='hidden' value='hisab_cache' name='hisab_cache'>		
			<input type='hidden' value='OK' name='hisab_verify'>		
		</form>";
		
	echo "<script>
		document.getElementById('RedirectCache').submit();
	</script>";
	exit();	
}

if($branch=='')
{
	echo "<script type='text/javascript'>
			window.location.href='../';
		</script>";
	exit();
}

if($tno=='' || $trip_no==0 || $trip_no=='')
{
	errorLog("Unable to fetch Required Data",$conn,$page_name,__LINE__);
			
	echo "<script type='text/javascript'>
		swal({
			title: 'Unable to fetch inputs !',
			type: 'error',
			closeOnConfirm: true
			});
		$('#loadicon').hide();
		</script>";
	exit();
}

$fetch_driver_details=Qry($conn,"SELECT t.driver_code,d.name,d.last_salary,d.dummy_driver FROM dairy.trip as t 
	LEFT OUTER JOIN dairy.driver as d ON d.code=t.driver_code
	WHERE t.tno='$tno'");
	
if(!$fetch_driver_details)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($fetch_driver_details)==0)
{
	errorLog("Driver not Found.",$conn,$page_name,__LINE__);
	Redirect("Driver not Found.","./");
	exit();
}

$row_driver_data=fetchArray($fetch_driver_details);
$driver_code_value=$row_driver_data['driver_code'];
$driver_name=$row_driver_data['name'];
$driver_last_salary=$row_driver_data['last_salary'];
$is_dummy_driver=$row_driver_data['dummy_driver'];
				
if($driver_code_value=='')
{
	errorLog("Driver not Found. Truck No : $tno.",$conn,$page_name,__LINE__);
	Redirect("Driver not Found.","./");
	exit();
}	
								
$days=Qry($conn,"SELECT 
(SELECT date(date) FROM dairy.trip WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from',
(SELECT date(end_date) FROM dairy.trip WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to',
(SELECT from_station FROM dairy.trip WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from_station',
(SELECT SUM(km) FROM dairy.trip WHERE trip_no='$trip_no' AND narration!='TRAIN' GROUP BY trip_no) as 'total_km',
(SELECT to_station FROM dairy.trip WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to_station'");

if(!$days)
{ 
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_days=fetchArray($days);

$start = $row_days['from'];
$end = $row_days['to'];

if($end==0 || $end=='')
{
	$end = date("Y-m-d");
}

$from_station = $row_days['from_station'];
$to_station = $row_days['to_station'];
$total_km = $row_days['total_km'];

$diff = (strtotime($end)- strtotime($start))/24/3600; 
$diff_days=$diff+1;

// FETCH DATA FROM DRIVER _ UP

$fetch_balance=Qry($conn,"SELECT id,up,asset_form_down,salary_amount,salary_type,salary_approval,amount_hold FROM dairy.driver_up WHERE 
down=0 AND code='$driver_code_value' ORDER BY id DESC LIMIT 1");

if(!$fetch_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($fetch_balance)==0)
{
	errorLog("Unable to fetch driver. Driver Code : $driver_code_value",$conn,$page_name,__LINE__);
	Redirect("Driver not found.","./");
	exit();
}

if(numRows($fetch_balance)>1)
{
	errorLog("Multiple Driver found. Driver Code : $driver_code_value",$conn,$page_name,__LINE__);
	Redirect("Multiple Driver found.","./");
	exit();
}

$row_balance=fetchArray($fetch_balance);

$driver_up_date=$row_balance['up'];

$asset_form_naame=Qry($conn,"SELECT trip_no,amount_naame FROM dairy.asset_form WHERE tno='$tno'");

if(!$asset_form_naame){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($asset_form_naame)==0)
{
	errorLog("Vehicle No : $tno. Asset form not found.",$conn,$page_name,__LINE__);
	Redirect("Asset form not found.","./");
	exit();
}

$asset_naame_amount = 0;

while($row_asset_naame = fetchArray($asset_form_naame))
{
	$trip_no_asset_form = $row_asset_naame['trip_no'];
	$asset_naame_amount = $asset_naame_amount+$row_asset_naame['amount_naame'];
}

if($trip_no!=$trip_no_asset_form)
{
	errorLog("Vehicle No : $tno. Asset form trip no : $trip_no_asset_form and system trip no: $trip_no not verified.",$conn,$page_name,__LINE__);
	Redirect("Trip number not verified.","./");
	exit();
}

// FETCH DATA FROM DRIVER _ UP ENDS HEREEEEEE


// Da Dates Code Start

$da_fetch_last=Qry($conn,"SELECT to_date,narration FROM dairy.da_book WHERE driver_code='$driver_code_value' ORDER BY id DESC LIMIT 1");
	
	if(!$da_fetch_last)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
	
if(numRows($da_fetch_last)>0)
{	
	$row_da_fetch=fetchArray($da_fetch_last);
	
	$da_date_Db = $row_da_fetch['to_date'];

	if($row_da_fetch['narration']=='ON_LEAVE_UP' || $row_da_fetch['narration']=='LEFT_COMP_UP' || $row_da_fetch['narration']=='DA_START_ADMIN')
	{
		$new_da_min_date = $da_date_Db;
	}
	else
	{
		if($driver_up_date<=$row_da_fetch['to_date'])
		{
			$new_da_min_date = date("Y-m-d",strtotime($row_da_fetch['to_date']. ' + 1 days'));
		}
		else
		{
			$new_da_min_date = $driver_up_date;
		}	
	}
}
else
{
	$new_da_min_date=$driver_up_date;
}

// Da Dates Code Ends

$diff_da_days="0";

if($diff_da_days>0)
{
	$old_da_from=date("d-m-y",strtotime($row_da_fetch['to_date']. ' + 1 days'));
	$old_da_to=date("d-m-y",strtotime($last_trip_date));
}
else
{
	$old_da_from="";
	$old_da_to="";
}
// echo "MIN".$new_da_min_date;

$getHappayBal = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no=(SELECT card_using FROM dairy.happay_card 
WHERE tno='$tno')");

if(!$getHappayBal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($getHappayBal)==0)
{
	$happay_balance=0;
}
else
{
	$rowHappay = fetchArray($getHappayBal);
	$happay_balance = $rowHappay['balance'];
}
?>
<!doctype html>
<html>
<head>
	<title>Final HISAB || E-Diary || RAMAN ROADWAYS PVT. LTD.</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<!-- SWEET ALERT-->
	<script src="../sweetalert.js"></script>
    <link rel="stylesheet" href="../sweetalert.css">
	<!-- END SWEET ALERT-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:600&display=swap" rel="stylesheet">
	<meta name="google" content="notranslate">
	<link rel="stylesheet" href="../../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
	
</head>

<?php include("../code_disabled_right_click.php"); ?>

<?php include("../load_icon.php"); ?>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<form action="./final_hisab_index_running_trip.php" id="ReloadForm" method="POST">
	<input type="hidden" value="myfile" name="KEY">		
	<input type="hidden" value="<?php echo $tno; ?>" name="tno">		
	<input type="hidden" value="<?php echo $trip_no; ?>" name="trip_no">		
</form>

<script type='text/javascript'>
	function ReLoad()
	{
		document.getElementById('ReloadForm').submit();
	}
</script>

<?php

$asset_form_down=$row_balance['asset_form_down'];
$salary_amount_db=$row_balance['salary_amount'];
$salary_type=$row_balance['salary_type'];
$salary_approval=$row_balance['salary_approval'];

if($salary_approval==0 || $salary_amount_db==0)
{
	errorLog("SALARY DATA NOT UPDATED. Driver Code : $driver_code_value, Truck No $tno.",$conn,$page_name,__LINE__);
	Redirect("SALARY DATA NOT UPDATED YET.","./");
	exit();
}

if($salary_type==1)
{
	$trip_days_per_trip_salary = Qry($conn,"SELECT id FROM dairy.trip WHERE driver_code='$driver_code_value' AND lr_type!='EMPTY'");
	
	if(!$trip_days_per_trip_salary)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Unable to process Request.","./");
		exit();
	}
	
	$trip_days_for_salary=numRows($trip_days_per_trip_salary);
}

$qry_salary_date=Qry($conn,"SELECT to_date,narration FROM dairy.salary WHERE driver_code='$driver_code_value' ORDER BY id DESC LIMIT 1");

if(!$qry_salary_date)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($qry_salary_date)>0)
{ 
	$row_date=fetchArray($qry_salary_date);
	 
	if($row_date['narration']=='ON_LEAVE_UP' || $row_date['narration']=='LEFT_COMP_UP' || $row_date['narration']=='SALARY_START_ADMIN')
	{
		$salary_from=$row_date['to_date'];
	}
	else
	{
		$salary_from=date('Y-m-d', strtotime($row_date['to_date']. ' + 1 days'));
	}	
}
else 
{
	if($driver_last_salary==0)
	{
		$salary_from=$driver_up_date;
	}
	else
	{
		$salary_from=$driver_last_salary;
	}	
}

$salary_to=$end; // TILL TRIP END DATE

$salary_days=(strtotime($salary_to)- strtotime($salary_from))/24/3600+1; 

$balance_on_trip = $row_balance['amount_hold'];

echo "<input type='hidden' id='opening_balance' value='$balance_on_trip'>";
echo "<input type='hidden' id='salary_days_value' value='$salary_days'>";
?>
<div class="container-fluid" style="">

<div class="row">
	<div class="form-group col-md-12">
	<br>
		&nbsp; <b><span class="glyphicon glyphicon-home"></span> Logged in as : <font color="red"><?php echo $tno; ?></font></b>
		&nbsp; <b><span class="glyphicon glyphicon-user"></span> Driver : <?php echo $driver_name; ?>.</b>
		&nbsp; <b><span class="glyphicon glyphicon-list-alt"></span> Balance  : <span style="letter-spacing:.5px;font-size:16px;"><?php echo $row_balance['amount_hold']; ?></span></b>
		&nbsp; <b><span class="fa fa-credit-card"></span> Happay Balance  : <span style="letter-spacing:.5px;font-size:16px;"><?php echo $happay_balance; ?></span></b>
		
		<?php 
		if($asset_form_down==0)
		{
			?>
			&nbsp; <button id="AssetFormViewButton" data-toggle="modal" data-target="#AssetFormViewModal" 
			class="btn btn-sm btn-primary pull-right">VIEW ASSET FORM</button>
			<?php
		}
		?>
	</div>
</div>

<?php include("modal_asset_form_view.php"); ?>

<div class="row">
	<div class="form-group col-md-12" style="box-shadow:0 0 50px #ddd inset;color:#000;padding:8px;">
		
		<a href="../"><button id="ClickBack" class="pull-left btn btn-sm btn-primary" style="border-radius:8px;">
			<span class="glyphicon glyphicon-arrow-left"></span> GoBack</button>
		</a> 
			<center><span style="font-size:23px;">Loaded Vehicle - Hisab ( TRIP No: <?php echo $trip_no; ?> )</span> 
			<br/>
			<?php echo $from_station." to ".$to_station." (".$diff_days." days)"; ?>
			</center>
	</div>	
</div>	

<form action='../print/single_trip.php' target="_blank" id='PrintForm1' method='POST'>
	<input type='hidden' id="trip_id_print" name='trip_id_print'>
	<input type='hidden' value="<?php echo $driver_name; ?>" name='driver_name'>
</form>

<script>	
function PrintTrip(id)
{
	$("#trip_id_print").val(id);
	
	function submitform22()
		{
			document.getElementById('PrintForm1').submit();
		}
	submitform22();	
}
</script>	

<div class="row">
	
	<div class="form-group col-md-12" class="table-responsive" style="overflow:auto">
			
		<h5 class="bg-primary" style="color:#FFF;letter-spacing:1px;padding:5px;">Trip Summary:</h5> 
			
			<table class="table table-bordered" style="font-size:12px">
				<tr>
					<th>#</th>
					<th>Route</th>
					<th>Date</th>
					<!--<th>LR_No</th>-->
					<th>Advance</th>
					<!--<th>Cheque</th>-->
					<th>Rtgs</th>
					<th>FrtAdv.</th>
					<th>Diesel</th>
					<th>Naame</th>
					<th>Exp</th>
					<th>TOLL TAX</th>
					<th>FrtColl.</th>
					<th><center><span style="font-size:15px;" class='glyphicon glyphicon-print'></span></center></th>
				</tr>
				
				<script>
				function MyFunc(trip)
				{
					//alert(trip);
				}
				</script>
				
<?php
$qry=Qry($conn,"SELECT t.id,t.from_station,t.to_station,t.km,t.lrno,date(t.date) as trip_date,t.freight,t.cash,t.cheque,
t.rtgs,t.driver_naame,t.diesel,t.diesel_qty,t.freight_collected,t.expense,t.toll_tax FROM dairy.trip AS t 
WHERE t.tno='$tno'");
				
if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}
				
$qry_sum_diesel=Qry($conn,"SELECT SUM(amount) as sum_diesel,SUM(qty) as sum_qty FROM dairy.diesel WHERE trip_id 
IN(SELECT id FROM dairy.trip WHERE tno='$tno')");
				
if(!$qry_sum_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}	

if(numRows($qry_sum_diesel)>0)
{
	$row_sum_diesel=fetchArray($qry_sum_diesel);
	$total_diesel_db = $row_sum_diesel['sum_diesel'];
	$total_diesel_qty_db = $row_sum_diesel['sum_qty'];
}
else
{
	$total_diesel_db = 0;
	$total_diesel_qty_db = 0;
}
				
if(numRows($qry)>0)
{
$sn=1;

$total_cash_sum = 0;
$total_cheque_sum = 0;
$total_rtgs_sum = 0;
$total_diesel_sum = 0;
$total_diesel_qty_sum = 0;
$total_naame_sum = 0;
$total_freight_adv_sum = 0;
$total_freight_collected_sum = 0;
$total_exp_sum = 0;
$total_toll_tax_sum = 0;

while($row=fetchArray($qry))
{
	$total_cash_sum = $total_cash_sum+$row['cash'];
	$total_cheque_sum = $total_cheque_sum+$row['cheque'];
	$total_rtgs_sum = $total_rtgs_sum+$row['rtgs'];
	$total_diesel_sum = $total_diesel_sum+$row['diesel'];
	$total_diesel_qty_sum = $total_diesel_qty_sum+$row['diesel_qty'];
	$total_naame_sum = $total_naame_sum+$row['driver_naame'];
	$total_freight_adv_sum = $total_freight_adv_sum+$row['freight'];
	$total_freight_collected_sum = $total_freight_collected_sum+$row['freight_collected'];
	$total_exp_sum = $total_exp_sum+$row['expense'];
	$total_toll_tax_sum = $total_toll_tax_sum+$row['toll_tax'];
	
	echo "<tr>
		<td>$sn</td>
		<td>
		<b><a onclick=MyFunc('$row[id]')>$row[from_station] to $row[to_station]</a> &nbsp; ($row[km] km)</b></td>
		<td>".date('d-m-y',strtotime($row['trip_date']))."</td>
		<td>$row[cash]</td>
		<td>$row[rtgs]</td>
		<td>$row[freight]</td>
		<td>$row[diesel] (".sprintf('%0.2f',$row['diesel_qty'])." ltr)</td>
		<td>$row[driver_naame]</td>
		<td>$row[expense]</td>
		<td>$row[toll_tax]</td>
		<td>$row[freight_collected]</td>
		<td><center><button onclick='PrintTrip($row[id])' style='color:#000'>
		<span class='glyphicon glyphicon-print'></span></button></center></td>
	</tr>
	";
$sn++;	
}

echo "
	<tr>
		<td colspan='3'><b>Total Calculation : </b></td>
		<td><b>$total_cash_sum</b></td>
		<td><b>$total_rtgs_sum</b></td>
		<td><b>$total_freight_adv_sum</b></td>
		<td><b>".sprintf('%0.2f',$total_diesel_sum)." <br> 
		(Qty: ".sprintf('%0.2f',$total_diesel_qty_sum)." ltr)</b></td>
		<td><b>$total_naame_sum</b></td>
		<td><b>$total_exp_sum</b></td>
		<td><b>$total_toll_tax_sum</b></td>
		<td><b>$total_freight_collected_sum</b></td>
		<td></td>
	</tr>";

	if($total_diesel_db!=$total_diesel_sum)
	{
		errorLog("Diesel Amount not verified. Diesel Table is: $total_diesel_db and trip is: $total_diesel_sum.",$conn,$page_name,__LINE__);
		Redirect("Diesel Amount not verified.","../");
		exit();
	}
	
	if(sprintf('%0.2f',$total_diesel_qty_db)!=sprintf('%0.2f',$total_diesel_qty_sum))
	{
		errorLog("Diesel Qty not verified. Diesel Table Qty is: $total_diesel_qty_db and trip Qty is: $total_diesel_qty_sum.",$conn,$page_name,__LINE__);
		Redirect("Diesel Qty not verified.","../");
		exit();
	}
	
	$diesel_qty=sprintf('%0.2f',$row_sum_diesel['sum_qty']);	
}
else
{
	echo "<td colspan='11'><b><font color='red'>No records found.</font></b></td>";
	$diesel_qty=0;		
}
?>
				
			</table>
			
			</div>

<div id="result"></div>	

		<script>
			function sum(amount)
			{
				if($('#da_from').val()=='' || $('#da_to').val()=='')
				{
					alert('SELECT DA DATE FIRST.');
					ReLoad();
				}
				else
				{
					if($('#da_from').val()=='2021-03-31' && $('#da_to').val()=='2021-03-31')
					{
						
					}
					else
					{
						if($('#da_from').val()<'<?php echo $new_da_min_date ?>')
						{
							$('#proceed_hisab').attr('disabled',true);
							alert('Invalid Date Selected.');
							ReLoad();
						}
					}
					
					$('#da_from').attr('readonly',true);
					$('#da_to').attr('readonly',true);
					
					$("#da").val((Number($("#da_date_duration").val()) * Number($("#da_amt").val())).toFixed(2));
				}	
			}
			
			function DaDateChk()
			{
				if($('#da_from').val()<'<?php echo $new_da_min_date ?>')
				{
					$('#da_from').val('');
				}
			}
		</script>
		
<?php include("modal_asset_form_down.php"); ?>

<form action="./final_hisab_proceed_running.php" method="POST">
		
<input type="hidden" name="hisab_verify" value="OK">
		
<div class="form-group col-md-7">
			
<script type="text/javascript">
    function CheckAssetForm(elem)
	{
		var asset_form = '<?php echo $asset_form_down; ?>';
		var happay_balance = '<?php echo $happay_balance; ?>';
		
		if(elem==1)
		{
			if(happay_balance>0)
			{
				alert('Please withdraw Happay Card\'s Balance First !');
				window.location.href='../';
			}
			else
			{
				if(asset_form==0)
				{
					function AssetFuncDown()
					{
						document.getElementById('AssetModalDown').click(); 
						$('#down_type_div').hide();
						$('#down_alert').show();
						$('#down_driver_type').attr('required',false);
					}	
				}
				else
				{
					function AssetFuncDown()
					{			
						$('#down_alert').show();
						$('#down_type_div').show();
						$('#down_driver_type').attr('required',true);
					}	
				}
			
				AssetFuncDown();
			}
		}
		else
		{
			$('#down_alert').hide();
			$('#down_type_div').hide();
			$('#down_driver_type').attr('required',false);
		}
	}
</script>
			
			<div class="form-group col-md-6">
				<label>Down DRIVER <span style="font-size:13px;"> ( ड्राइवर डाउन करे ) </span> <sup><font color="red">*</font></sup> </label>
				<select onchange="CheckAssetForm(this.value)" id="down_driver_selection" name="down_driver_selection" class="form-control" required>
					<option value="">Select an option</option>
					<option value="0">NO</option>
					<option value="1">YES</option>
				</select>
			</div>
			
					<script>
					function DownDriverCheck(elem)
					{
						if(elem=='LEFT' || elem=='LEFT_ROUTE')
						{
							$('#reason_div').show();
							$('#reason_left').attr('required',true);
							$('#standby_div').hide();
							$('#standby_reason').attr('required',false);
							$('#reason_other_div').hide();
							$('#reason_other').attr('required',false);
							
						}
						else if(elem=='STAND_BY')
						{
							$('#reason_div').hide();
							$('#reason_left').attr('required',false);
							$('#standby_div').show();
							$('#reason_other_div').hide();
							$('#standby_reason').attr('required',true);
							$('#reason_other').attr('required',false);
						}
						else
						{
							$('#reason_div').hide();
							$('#reason_left').attr('required',false);
							$('#standby_div').hide();
							$('#reason_other_div').hide();
							$('#standby_reason').attr('required',false);
							$('#reason_other').attr('required',false);
						}
					}
					</script>
					
					<div id="driver_down_result"></div>
					
					<div style="display:none" id="down_type_div" class="form-group col-md-6">
						<label>Down Reason : <sup><font color="red">*</font></sup></label>
						<select onchange="DownDriverCheck(this.value)" id="down_driver_type" name="down_driver_type" class="form-control" required>
							<option value="">Select an option</option>
							<option <?php if($stand_by_approval=="0"){ echo "disabled"; }?> value="STAND_BY">STANDBY - स्टैंडबाय  ( सैलरी चालू )</option>
							<option value="ON_LEAVE">ON LEAVE - छुट्टी पर</option>
							<option value="LEFT">LEFT COMPANY - कंपनी छोड़ कर गया</option>
							<option value="LEFT_ROUTE">LEFT ON-ROUTE - बीच में छोड़ कर गया</option>
						</select>
					</div>
					
					<script>
					function MyFuncStandBy(elem)
					{
						if(elem=='OTHER')
						{
							$('#reason_other_div').show();
							$('#reason_other').attr('required',true);
						}
						else
						{
							$('#reason_other_div').hide();
							$('#reason_other').attr('required',false);
						}
					}
					</script>
					
					<div id="standby_div" style="display:none" class="form-group col-md-6">
						<label>Reason of Stand By : </label>
						<select onclick="MyFuncStandBy(this.value)" id="standby_reason" name="standby_reason" class="form-control">
							<option value="">Select an option</option>
							<option value="MAINTENANCE">MAINTENANCE</option>
							<option value="STRIKE">STRIKE ( हड़ताल )</option>
							<option value="ACCIDENT">ACCIDENT</option>
							<option value="OTHER">OTHER</option>
						</select>
					</div>
					
					<div id="reason_other_div" style="display:none" class="form-group col-md-6">
						<label>Write other Reason of Stand By : <sup><font color="red">*</font></sup></label>
						<textarea name="reason_other" id="reason_other" class="form-control"></textarea>
					</div>
					
					<div id="reason_div" style="display:none" class="form-group col-md-12">
						<label>Reason of LEFT : <sup><font color="red">*</font></sup></label>
						<textarea name="reason_left" id="reason_left" class="form-control"></textarea>
					</div>
					
				<div class="col-md-12">
					<div id="down_alert" class="alert alert-danger" style="font-weight:bold;display:none">
						आपने ड्राइवर डाउन करने का चयन किया हुआ है , ड्राइवर हिसाब के बाद स्वतः डाउन हो जाएगा ! <br>
						Your have selected driver down option, driver will be down automatically after hisab.
					</div>	
				</div>
					
				<?php
				if($diff_da_days>0)
				{
				?>
				<div class="form-group col-md-12">
					<div class="alert alert-danger" style="font-weight:bold;">
						<label>DA Difference Found of <?php echo $diff_da_days; ?> Days 
						(<b><?php echo $old_da_from; ?> to <?php echo $old_da_to; ?></b>) . <br> Do You want to Pay ..? </label>
						<br />
						<select style="width:50%" onchange="OldDaCall(this.value)" name="diff_da_pay" class="form-control" required>
							<option value="">--SELECT--</option>
							<option value="1">YES</option>
							<option value="0">NO</option>
						</select>
					</div>	
				</div>
				<?php		
				}
				else
				{
					echo '<input type="hidden" name="diff_da_pay" value="0" required />';
				}
				?>		

<input type="hidden" id="old_da_diff_input" value="0" />

<script type="text/javascript">
function OldDaCall(elem)
{
		$('#da_from').val('');
		 $('#da_to').val('');
		 $('#da').val('');
		 $('#da_date_duration').val('');
		 $('#da_amt').val('');
		 
		 $('#da_from').attr('readonly',false);
		 $('#da_to').attr('readonly',false);
		 
	if(elem==1)
	{
		 $('#old_da_diff_input').val('<?php echo $diff_da_days; ?>');
	}
	else
	{
		 $('#old_da_diff_input').val('0');
	}
}
</script>				
				
				<div class="form-group col-md-12">
					<font size="3" color="red">DA Duration : From 
					<span id="from_date_value_da">0000-00-00</span> to <span id="to_date_value_da">0000-00-00</span> (<span id="da_date_diff"></span> days)</font>
				</div>
				
				<div id="da_dates" style="display:none1">
				
				<div class="form-group col-md-6">
                   <label>DA From Date <font color="red">*</font> <?php if(date("Y-m-d")=='2021-03-31') { ?>&nbsp; <input style="width:13px" id="hisab_31_march" type="checkbox" /> <span style="color:blue;font-size:12px">Hisab 31<sup>st</sup> March</span> <?php } ?></label>
                   <input name="da_from" onchange="FromDateDA(this.value);DaDateChk();" id="da_from" type="date" class="form-control" min="<?php echo $new_da_min_date; ?>" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
                 </div>
				 
				 <script>
				 $(document).on('click','#hisab_31_march',function(){
				
					$('#da_from').val('');	
					$('#da_to').val('');
					$("#from_date_value_da").html("0000-00-00");
					$("#to_date_value_da").html("0000-00-00");
					$('#da_date_diff').html('');
					$('#da_date_duration').val('');
					$('#da_from_date').val('');
					$('#da_to_date').val('');
				
					if($(this).is(':checked')){
						$('#da_from').attr('readonly',true);
						$('#da_to').attr('readonly',true);
						$('#da_from').val('2021-03-31');	
						$('#da_to').val('2021-03-31');	
						
						$("#from_date_value_da").html("2021-03-31");
						$("#to_date_value_da").html("2021-03-31");
						$('#da_date_diff').html('0');
						$('#da_date_duration').val('0');
						$('#da_from_date').val('2021-03-31');
						$('#da_to_date').val('2021-03-31');
					 }
					 else{
						$('#da_from').attr('readonly',false);
						$('#da_to').attr('readonly',false);
					 }
				});
		
				 function FromDateDA(fromdate)
				 {
					var nextDay = new Date(fromdate);
					nextDay.setDate(nextDay.getDate() + 0);
					
					var next_day=nextDay.toISOString().slice(0,10);
					
					var date_today = '<?php echo $today_date; ?>';
					
					if(fromdate!=date_today)
					{
						$("#da_to").attr("min",nextDay.toISOString().slice(0,10));
					}
					
					$("#da_to").val("");
					$("#from_date_value_da").html("0000-00-00");
					$("#to_date_value_da").html("0000-00-00");
					$("#from_date_value_da").html(fromdate);
					$('#da_date_diff').html('0');
					$('#da_date_duration').val('0');
				}
				 </script>
				 
				  <script>
				 function ToDateDA(todate)
				 {
					if($('#da_from').val()=='')
					{
						alert('SELECT DA FROM DATE FIRST !');
						$("#to_date_value_da").html("0000-00-00");
						$('#da_to').val('');
						
						$('#da_from_date').val('');
						$('#da_to_date').val('');
						$('#da_date_diff').html('0');
						$('#da_date_duration').val('0');
						
						$('#tel').val('0');
						$('#ag').val('0');
					}
					else
					{
						$("#to_date_value_da").html(todate);
						
						var dafrom = $('#da_from').val();
						var dato = todate;
						var diff =  Math.floor(( Date.parse(dato) - Date.parse(dafrom) ) / 86400000 +1); 
						var old_da_diff = Number($('#old_da_diff_input').val());
						
						if(old_da_diff=='')
						{
							var old_da_diff_days = 0;
						}
						else
						{
							var old_da_diff_days = old_da_diff;
						}
						
						$('#da_from_date').val(dafrom);
						$('#da_to_date').val(dato);
						$('#da_date_diff').html(diff);
						$('#da_date_duration').val(diff+old_da_diff_days);
						
						<?php
						if($lock_ag=="1")
						{
						?>
							$('#ag').val('0');
						<?php						
						}
						else
						{
						?>
							$('#ag').val(diff*10);
						<?php
						}
						
						if($lock_tel=="1")
						{
						?>
						$('#tel').val('0');
						<?php	
						}
						else
						{
						?>
						$('#tel').val(diff*10);
						<?php	
						}
						?>
					}	
				}
				 </script>
				 
				 <div id="da_result"></div>
				
				<div class="form-group col-md-6">
                   <label>DA Till Date <font color="red">*</font></label>
				   <input name="da_to" onchange="ToDateDA(this.value)" id="da_to" type="date" class="form-control" min="<?php echo date("Y-m-d"); ?>" max="<?php echo date('Y-m-d',(strtotime ('-0 day' ,strtotime(date("Y-m-d"))))); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					<!-- <input name="da_to" onchange="ToDateDA(this.value)" id="da_to" type="date" class="form-control" min="<?php// echo date("Y-m-d"); ?>" max="<?// echo $end; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" /> -->
                </div>
				
				</div>
				
				<div class="form-group col-md-4">
					<label>Total DA</label>
					<input type="number" step="any" class="form-control" value="0" id="da" name="da" readonly required>
				</div>
				
				<div class="form-group col-md-4">
					<label>DA Duration (days)</label>
					<input type="number" step="any" class="form-control" id="da_date_duration" value="<?php echo $diff_days; ?>" name="da_duration" readonly required>
				</div>
				
				<div class="form-group col-md-4">
					<label>DA/PerDay <font color="red"><sup>*</font></sup></label>
					<select id="da_amt" name="da_amt" onchange="sum(this.value);sum_balance()" class="form-control" required>
						<option value="">Select an option</option>
						<option value="0">0</option>
						<option value="250">250</option>
						<option value="275">275</option>
						<option value="300">300</option>
					</select>
				</div>
				
				
				<div class="form-group col-md-2">
					<label>TEL.</label>
					<input type="number" step="any" class="form-control" value="<?php if($lock_tel=="1") { echo "0"; } else { echo $diff_days*10; } ?>" id="tel" name="tel" readonly required>
				</div>
				
				<div class="form-group col-md-3">
					<label>AIR & GREASE</label>
					<input type="number" step="any" class="form-control" value="<?php if($lock_ag=="1") { echo "0"; } else { echo $diff_days*10; }?>" id="ag" name="ag" readonly required>
				</div>
				
				<div class="form-group col-md-3">
					<label>TRIP Days</label>
					<input type="number" step="any" class="form-control" id="duration" value="<?php echo $diff_days; ?>" name="duration" readonly required>
				</div>
				
				<div id="give_salary_div" class="form-group col-md-4">
					<label>GIVE SALARY <font color="red"><sup>*</font></sup></label>
					<select id="give_salary" name="give_salary" onchange="FuncSal(this.value);sum_balance()" class="form-control" required>
						<option value="">Select an option</option>
						<option value="0">NO</option>
						<option value="1">YES</option>
					</select>
				</div>
				
				<script>
				function FuncSal(elem)
				{
					$('#sal_from_date').val('');
					$('#sal_to_date').val('');
					
					if(elem==1)
					{
						$('#salary').attr('min','1');
						
						<?php
						if($salary_type==1)
						{
							?>
								$('#salary').val('<?php echo $trip_days_for_salary*$salary_amount_db; ?>');
								$('#salary_amount_cache').val('<?php echo $trip_days_for_salary*$salary_amount_db; ?>');
								$('#sal_dates').hide();
								$('#sal_from_date').attr('required',false);
								$('#sal_to_date').attr('required',false);
								$('#sal_to_date').attr('min',false);
								$('#sal_to_date').attr('max',false);
								$('#sal_from_date').val('<?php echo $start; ?>');
								$('#sal_to_date').val('<?php echo $end; ?>');
								$("#from_date_value").html('<?php echo $start; ?>');
								$("#to_date_value").html('<?php echo $end; ?>');
							<?php
						}
						else
						{
						?>
							$('#salary').val('');
							$('#salary_amount_cache').val('');
							$('#sal_dates').show();
							$('#sal_from_date').attr('required',true);
							$('#sal_to_date').attr('required',true);
							$("#from_date_value").html("0000-00-00");
							$("#to_date_value").html("0000-00-00");
						<?php
						}
						?>
						$('#sal_div').show();
						$('#salary_duration').show();
					}
					else
					{
						$('#salary').attr('min','0');
						$('#salary').val('0');
						$('#salary_amount_cache').val('0');
						$('#sal_div').hide();
						$('#salary_duration').hide();
						$('#sal_dates').hide();
						$('#sal_from_date').attr('required',false);
						$('#sal_to_date').attr('required',false);
						$("#from_date_value").html("0000-00-00");
						$("#to_date_value").html("0000-00-00");
					}
				}
				</script>
			
			<div id="sal_dates" style="display:none">
				
				<div class="form-group col-md-4">
                   <label>SALARY From Date <font color="red">*</font></label>
                   <input name="sal_from_date" onchange="FromDate(this.value)" id="sal_from_date" type="date" class="form-control" min="<?php echo $salary_from; ?>" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
                 </div>
				 
				 <script>
				 function FromDate(fromdate)
				 {
					var nextDay = new Date(fromdate);
					nextDay.setDate(nextDay.getDate() + 0);
					$("#sal_to_date").attr("min",nextDay.toISOString().slice(0,10));
					$("#sal_to_date").val("");
					$("#from_date_value").html("0000-00-00");
					$("#to_date_value").html("0000-00-00");
					$("#from_date_value").html(fromdate);
				}
				 </script>
				 
				  <script>
				 function ToDate(todate)
				 {
					if($('#sal_from_date').val()=='')
					{
						alert('SELECT SALARY FROM DATE FIRST !');
						$("#to_date_value").html("0000-00-00");
						$('#sal_to_date').val('');
					}
					else
					{
						$("#to_date_value").html(todate);
						
							$("#loadicon").show();
                           jQuery.ajax({
                           url: "./date_for_salary.php",
						   data: 'from=' + $('#sal_from_date').val() + '&to=' + todate + '&sal_amount=' + '<?php echo $salary_amount_db; ?>' + '&page_name=' + 'running',
                           type: "POST",
                           success: function(data) {
                           $("#salary_result").html(data);
                           $("#loadicon").hide();
                           },
                           error: function() {}
                         });
					}	
				}
				 </script>
				 
				 <div id="salary_result"></div>
				 
				<div class="form-group col-md-4">
                   <label>SALARY To Date <font color="red">*</font></label>
                   <input name="sal_to_date" onchange="ToDate(this.value)" id="sal_to_date" type="date" class="form-control" min="<?php echo date("Y-m-d"); ?>" max="<?php echo date('Y-m-d',(strtotime ('-0 day' ,strtotime(date("Y-m-d"))))); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
				</div>
				</div>
				<div id="salary_duration" style="display:none" class="form-group col-md-12">
					<font size="4" color="red">SALARY Duration : From 
					<span id="from_date_value">0000-00-00</span> to <span id="to_date_value">0000-00-00</span></font>
				</div>
			
			<div id="sal_div" style="display:none" class="form-group col-md-6">
				<label>SALARY AMOUNT <font color="red"><sup>*</font></sup></label>
				<input min="0" class="form-control" oninput="sum_balance()" onblur="sum_balance()" value="0" type="number" id="salary" name="salary" readonly required>
			</div>
			
		<script>
			function sum_balance()
			{
				$("#closing").val((Number($("#opening_balance").val()) - (Number($("#da").val()) + Number($("#tel").val()) + Number($("#ag").val()) + Number($("#salary").val())) + Number($("#asset_naame_amount").val())).toFixed(2)); 
			}
			//
		</script>
			
			<div class="form-group col-md-6">
				<label>Closing Balance <font color="red"><sup>*</font></sup></label>
				<input class="form-control" value="<?php echo $row_balance['amount_hold']; ?>" readonly step="any" type="number" id="closing" name="closing" required>
			</div>
			
			<div class="col-md-12">
				<div class="col-md-12 alert alert-danger" style="font-weight:bold">
					Enter GPS KM Between : <?php echo date("d-m-y",strtotime($start))." to ".date("d-m-y",strtotime($end)); ?>
					&nbsp; , System KM is : <?php echo $total_km." km"; ?> , &nbsp; Average : <span id="truck_avg"></span>
				</div>	
			</div>	
			
			<?php
				$five_perc=round($total_km*5/100);
				$ten_perc=round($total_km*10/100);
				$max_km = $total_km+$five_perc;
				$min_km = $total_km-$five_perc;
				
				$max_km_new = $total_km+$ten_perc;
				$min_km_new = $total_km-$ten_perc;
			?>

			<input type="hidden" id="salary_amount_cache" name="salary_amount_cache">
			
			<div class="col-md-12" id="gps_km_error_div" style="display:none;color:red">
				<label id="gps_km_error">GPS km Difference can't be grater than 5%.</label>
			</div>
			
			<div class="col-md-12">
				<div class="row">
					<div class="form-group col-md-5">
						<label>Enter GPS KM <font color="red"><sup>*</font></sup></label>
						<input class="form-control" min="0" oninput="GPSKmEntered(this.value)" type="number" id="gps_km1" name="gps_km" required>
					</div>	
					
					<div class="form-group col-md-3">
						<label>&nbsp;</label>
						<br>
						<button type="submit" id="proceed_hisab" class="btn btn-primary btn-block">
							Next Step &nbsp; <span class="glyphicon glyphicon-arrow-right"></span>
						</button>
					</div>	
				</div>	
			</div>
			
			<script>
			function GPSKmEntered(km)
			{
				var km = Number(km);
				var max_km = '<?php echo $max_km; ?>';
				var min_km = '<?php echo $min_km; ?>';
				var diesel_qty = '<?php echo $diesel_qty; ?>';
				var truck_avg = Number(km/diesel_qty).toFixed(2);
				
				if(diesel_qty>0)
				{
					$('#truck_avg').html(truck_avg+" kmpl");
					
					if(km>max_km || km<min_km)
					{
						$('#gps_km_error_div').show();
						// $('#proceed_hisab').attr('disabled',true);
					}
					else
					{
						$('#gps_km_error_div').hide();
						// $('#proceed_hisab').attr('disabled',false);
					}
				}
				else
				{
					$('#truck_avg').html("NO DIESEL");
					
					if(km>max_km || km<min_km)
					{
						$('#gps_km_error_div').show();
						// $('#proceed_hisab').attr('disabled',true);
					}
					else
					{
						$('#gps_km_error_div').hide();
						// $('#proceed_hisab').attr('disabled',false);
					}
				}
			}
			</script>
			
			<input type="hidden" name="tno" value="<?php echo $tno; ?>">
			<input type="hidden" name="trip_no" value="<?php echo $trip_no; ?>">
			<input type="hidden" name="driver_code" value="<?php echo $driver_code_value; ?>">
			<input type="hidden" name="salary_from_date" value="<?php echo $salary_from; ?>">
			<input type="hidden" name="salary_to_date" value="<?php echo $salary_to; ?>">
			<input type="hidden" name="da_from_date" id="da_from_date" value="<?php echo $new_da_min_date; ?>">
			<input type="hidden" name="da_to_date" id="da_to_date" value="<?php echo $end; ?>">
			<input type="hidden" name="balance_on_trip" value="<?php echo $balance_on_trip; ?>">
			<input type="hidden" name="from_station" value="<?php echo $from_station; ?>">
			<input type="hidden" name="to_station" value="<?php echo $to_station; ?>">
			<input type="hidden" name="diff_days" value="<?php echo $diff_days; ?>">
			<input type="hidden" name="driver_name" value="<?php echo $driver_name; ?>">
			
			<input type="hidden" name="old_da_from" value="<?php echo $old_da_from; ?>">
			<input type="hidden" name="old_da_to" value="<?php echo $old_da_to; ?>">
			<input type="hidden" name="diesel_qty" value="<?php echo $diesel_qty; ?>">
			<input type="hidden" name="driver_up_date" value="<?php echo $driver_up_date; ?>">
			<input type="hidden" name="system_km" value="<?php echo $total_km; ?>">
			<input type="hidden" id="asset_naame_amount" name="asset_naame_amount" value="<?php echo $asset_naame_amount; ?>">
		
		<div class="form-group col-md-4">
			<label>&nbsp;</label>
			
		</div>
			
		</form>
		
	</div>
</div>