<?php
require_once("../connect.php");
$tno=escapeString($conn,strtoupper($_SESSION['diary']));

$get_cache_data = Qry($conn,"SELECT trip_no,lock_ag,lock_tel,trishul_card,driver_name,driver_code,down_driver,down_type,sal_cut_off,
from_station,to_station,balance_on_hisab,da_amount,da_from,da_to,salary_amount,salary_pattern,sal_from,sal_to,trip_start,trip_end,
trip_days,tel_amount,ag_amount,closing,asset_form_naame,diesel_qty,sys_km,gps_km FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$get_cache_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_get_cache = fetchArray($get_cache_data);

$trishul_card_deduction = $row_get_cache['trishul_card'];
$trip_no = $row_get_cache['trip_no'];
$driver_name = $row_get_cache['driver_name'];
$driver_code = $row_get_cache['driver_code'];
$down_driver = $row_get_cache['down_driver'];
$down_type = $row_get_cache['down_type'];
$from_station = $row_get_cache['from_station'];
$to_station = $row_get_cache['to_station'];
$balance_on_hisab = $row_get_cache['balance_on_hisab']; // opening balance
$da_amount = $row_get_cache['da_amount'];
$da_from = $row_get_cache['da_from'];
$da_to = $row_get_cache['da_to'];
$salary_amount = $row_get_cache['salary_amount'];
$salary_pattern = $row_get_cache['salary_pattern'];
$sal_from = $row_get_cache['sal_from'];
$sal_to = $row_get_cache['sal_to'];
$trip_start = $row_get_cache['trip_start'];
$trip_end = $row_get_cache['trip_end'];
$trip_days = $row_get_cache['trip_days'];
$tel_amount = $row_get_cache['tel_amount'];
$ag_amount = $row_get_cache['ag_amount'];
$asset_form_naame = $row_get_cache['asset_form_naame'];
$closing_db = $row_get_cache['closing'];
$diesel_qty = $row_get_cache['diesel_qty'];
$sys_km = $row_get_cache['sys_km'];
$gps_km = $row_get_cache['gps_km'];
$sal_cut_off_db = $row_get_cache['sal_cut_off']; // salary deduction in bulkers

$dr_var = $da_amount+$tel_amount+$ag_amount+$salary_amount;
$closing_amount = ($balance_on_hisab-$dr_var)+$asset_form_naame+$trishul_card_deduction;

if($closing_amount!=$closing_db)
{
	errorLog("Closing amount not verified. Sys : $closing_amount. Db : $closing_db.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$ac_updated="1";

$chk_driver_ac_details = Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$driver_code'");

if(!$chk_driver_ac_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($chk_driver_ac_details)>0)
{
	$row_d_ac = fetchArray($chk_driver_ac_details);
		
	if($row_d_ac['acname']=='' || $row_d_ac['acno']=='' || $row_d_ac['bank']=='' || $row_d_ac['ifsc']=='')
	{	
		$ac_updated="0";
	}
}
else
{
	$ac_updated="0";
}
?>

<input type="hidden" value="<?php echo $closing_amount; ?>" id="closing_amount">
	
<script>
function PayHisabClick()
{
	$('.all_payment_div').hide();
	$('.hisab_btns').hide();
	
	$('#pay_hisab_btn').attr('disabled',true);
	$("#pay_hisab_div").show();
	// $("#amount_to_pay").val(Math.abs($('#closing_amount').val()));
}
	
function CarryFwDandPay()
{
	$('.all_payment_div').hide();
	$('.hisab_btns').hide();
	
	$('#pay_cfwd_btn').attr('disabled',true);
	$("#pay_and_cfwd_div").show();
	// $("#amount_to_pay").val(Math.abs($('#closing_amount').val()));
}
	
function CreditCash()
{
	$('.all_payment_div').hide();
	$('.hisab_btns').hide();
	
	$('#credit_amount_btn').attr('disabled',true);
	// $("#amount_to_pay").val(Math.abs($('#closing_amount').val()));
	$("#credit_to_cash_div").show();
}
</script>
	
<input type="hidden" value="<?php echo abs($closing_amount); ?>" id="amount_to_pay">	
	

	
<form autocomplete="off" id="PayHisabFormId">	
	<div id="pay_hisab_div" style="display:none" class="all_payment_div form-group col-md-12">
<?php
if($closing_amount>=0)
{
?>
	<div class="row">	
		<div class="col-md-12">
		<br />
			<div class="alert alert-danger" style="">
				चालक का बकाया है, भुगतान निषेध.
				DRIVER's outstanding is -ve. You Can't PAY.
			</div>	
		</div>	
<?php
}
else
{
?>
	<div class="col-md-12">
		<h5 class="bg-primary" style="padding:6px;font-size:14px;color:#FFF">Pay Hisab. &nbsp; <?php echo "Amount : ".abs($closing_amount)." /-"; ?></h5>
	</div>
		
<script>
function PaymentBy(elem)
{
	var payable_amount = Number($("#amount_to_pay").val());
	var ac_updated = '<?php echo $ac_updated; ?>';
	
	$('#pay_hisab_submit').attr('disabled',true);
	$("#rtgs_pay").val('');
	$("#cash_pay").val('');
	$("#cheque_no").val('');
	
	$("#rtgs_pay").attr('required',false);
	$("#cash_pay").attr('required',false);
	$("#cheque_no").attr('required',false);
	
	$("#pay_rtgs_div").hide();
	$("#pay_cash_div").hide();
	$("#pay_cheque_div").hide();
	
	if(elem=='CASH')
	{
		if(payable_amount>3000)
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Cash max limit is: 3000/-</font>',});
			$('#payment_by').val('');
		}
		else
		{
			$('#pay_hisab_submit').attr('disabled',false);
		}
	}
	else if(elem=='RTGS')
	{
		if(ac_updated=="0")
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Driver account details not updated !</font>',});
			$('#payment_by').val('');
			$('#pay_hisab_submit').attr('disabled',true);
		}
		else
		{
			$('#pay_hisab_submit').attr('disabled',false);
		}
	}
	else if(elem=='CHEQUE')
	{
		$("#pay_cheque_div").show();
		$("#cheque_no").attr('required',true);
		$('#pay_hisab_submit').attr('disabled',false);
	}
	else if(elem=='BOTH')
	{
		if(ac_updated=="0")
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Driver account details not updated !</font>',});
			$('#payment_by').val('');
			$('#pay_hisab_submit').attr('disabled',true);
		}
		else
		{
			$("#pay_cash_div").show();
			$("#pay_rtgs_div").show();
			
			$("#cash_pay").attr('required',true);
			$("#rtgs_pay").attr('required',true);
			
			$("#rtgs_pay").attr('min','1');
			$("#cash_pay").attr('min','1');
			$("#cash_pay").attr('max','3000');
			$('#pay_hisab_submit').attr('disabled',false);
		}
	}
	else
	{
		$("#cash_pay").attr('required',true);
		$("#rtgs_pay").attr('required',true);
		$("#cheque_no").attr('required',true);
	}
}
</script>

<script>
function Calc_pay_cash()
{
	if($('#payment_by').val()=='BOTH')
	{
		$("#rtgs_pay").val((Number($("#amount_to_pay").val()) - Number($("#cash_pay").val())).toFixed(2));
	}	
}
		
function Calc_pay_rtgs()
{
	if($('#payment_by').val()=='BOTH')
	{
		$("#cash_pay").val((Number($("#amount_to_pay").val()) - Number($("#rtgs_pay").val())).toFixed(2));
	}	
}
</script>
		
	<div class="form-group col-md-6">
		<label>Payment By <font color="red"><sup>*</sup></font></label>
		<select style="font-size:12px" id="payment_by" name="payment_by" onchange="PaymentBy(this.value)" class="form-control" required>
			<option style="font-size:12px" value="">Select an option</option>
			<option style="font-size:12px" value="CASH">CASH</option>
			<option style="font-size:12px" value="CHEQUE">CHEQUE</option>
			<option style="font-size:12px" value="RTGS">RTGS</option>
			<option style="font-size:12px" value="BOTH">CASH + RTGS</option>
		</select>
	</div>
		
	<div id="pay_cheque_div" style="display:none" class="form-group col-md-6">
		<label>Cheque number <font color="red"><sup>*</sup></font></label>
		<input name="cheque_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9,-]/,'')" id="cheque_no" type="text" class="form-control" required />
	</div>
			
	<div id="pay_cash_div" style="display:none" class="form-group col-md-6">
		<label>Cash Amount to PAY <font color="red"><sup>*</sup></font></label>
		<input name="cash_pay" oninput="Calc_pay_cash();" id="cash_pay" type="number" min="1" max="3000" class="form-control" required />
	</div>
		
	<div id="pay_rtgs_div" style="display:none" class="form-group col-md-6">
		<label>RTGS Amount to PAY <font color="red"><sup>*</sup></font></label>
		<input id="rtgs_pay" oninput="Calc_pay_rtgs();" name="rtgs_pay" type="number" min="1" class="form-control" required />
	</div>
		
	<input type="hidden" name="total_amount" value="<?php echo abs($closing_amount); ?>">
		
	<div class="form-group col-md-12">
		<button disabled="disabled" type="submit" id="pay_hisab_submit" class="btn btn-sm btn-primary">Submit</button>
		&nbsp; &nbsp;
		<button type="button" onclick="RefreshProceed();" class="btn btn-sm btn-danger">Cancel</button>
	</div>
<?php
}
?>
	</div>
</div>
</form>


<div id="credit_to_cash_div" style="display:none" class="all_payment_div form-group col-md-12">
	<br />
	<div class="col-md-12" style="font-size:13px">
		<div class="alert alert-danger" style="">
			चालक का बकाया है, रिकवर करे. <br>
			DRIVER's outstanding is -ve, Recover it.
		</div>	
	</div>
		
	<div class="col-md-6">
		<button type="button" onclick="CreditToCashbook()" name="credit_to_cash" id="credit_to_cash" class="btn btn-primary btn-sm">
		Credit <?php echo abs($closing_amount); ?> /- to your CashBook</button>
		&nbsp; &nbsp;
		<button type="button" onclick="RefreshProceed();" class="btn btn-sm btn-danger">Cancel</button>
	</div>
</div>
	
	
<form autocomplete="off" id="PayhisabBothFunc">	
<div id="pay_and_cfwd_div" style="display:none" class="all_payment_div form-group col-md-12">
<?php
if($closing_amount>=0)
{
?>
	<br />
	<div class="col-md-12" style="font-size:13px">
		<div class="alert alert-danger" style="">
			चालक का बकाया है, भुगतान निषेध.
			DRIVER's outstanding is -ve. You Can't PAY.
		</div>	
	</div>	
<?php
}
else
{
?>
<script>
function PaymentByBoth(elem)
{
	var cfwd_amt = Number($('#carry_fwd_amount').val());
	var pay_amount = Number($('#payable_amount').val());
	var ac_updated = '<?php echo $ac_updated; ?>';

	if(cfwd_amt=='' || pay_amount==''|| cfwd_amt<=0 || pay_amount<=0)
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Invalid carry-fwd or pay amount !</font>',});
		$('#payment_by_both').val('');
	}
	else
	{
		$('#carry_fwd_amount').attr('readonly',true);
		$('#payable_amount').attr('readonly',true);
		
		$('#pay_hisab_submit_both').attr('disabled',true);
		$("#rtgs_pay_both").val('');
		$("#cash_pay_both").val('');
		$("#cheque_no_both").val('');
		
		$("#rtgs_pay_both").attr('required',false);
		$("#cash_pay_both").attr('required',false);
		$("#cheque_no_both").attr('required',false);
		
		$("#rtgs_div_both").hide();
		$("#cash_div_both").hide();
		$("#cheque_div_both").hide();
		
		if(elem=='CASH')
		{
			if(pay_amount>3000)
			{
				Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Cash max limit is: 3000/-</font>',});
				$('#payment_by_both').val('');
				
				$('#carry_fwd_amount').attr('readonly',false);
				$('#payable_amount').attr('readonly',false);
			}
			else
			{
				$('#pay_hisab_submit_both').attr('disabled',false);
			}
		}
		else if(elem=='RTGS')
		{
			if(ac_updated=="0")
			{
				Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Driver account details not updated !</font>',});
				$('#payment_by_both').val('');
				$('#pay_hisab_submit_both').attr('disabled',true);
			}
			else
			{
				$('#pay_hisab_submit_both').attr('disabled',false);
			}
		}
		else if(elem=='CHEQUE')
		{
			$("#cheque_div_both").show();
			$("#cheque_no_both").attr('required',true);
			$('#pay_hisab_submit_both').attr('disabled',false);
		}
		else if(elem=='BOTH')
		{
			if(ac_updated=="0")
			{
				Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Driver account details not updated !</font>',});
				$('#payment_by_both').val('');
				$('#pay_hisab_submit_both').attr('disabled',true);
			}
			else
			{
				$("#cash_div_both").show();
				$("#rtgs_div_both").show();
				
				$("#cash_pay_both").attr('required',true);
				$("#rtgs_pay_both").attr('required',true);
				
				$("#rtgs_pay_both").attr('min','1');
				$("#cash_pay_both").attr('min','1');
				$("#cash_pay_both").attr('max','3000');
				$('#pay_hisab_submit_both').attr('disabled',false);
			}
		}
		else
		{
			$("#cash_pay_both").attr('required',true);
			$("#rtgs_pay_both").attr('required',true);
			$("#cheque_no_both").attr('required',true);
		}
	}
}

function CarryFwdBoth()
{
	$("#payable_amount").val((Math.abs(Number($("#amount_to_pay").val())) - Number($("#carry_fwd_amount").val())).toFixed(2));
}
		
function PayAmtBoth()
{
	$("#carry_fwd_amount").val((Math.abs(Number($("#amount_to_pay").val())) - Number($("#payable_amount").val())).toFixed(2));
}

function pay_cash_both()
{
	if(Number($("#payable_amount").val())=='' || Number($("#payable_amount").val())<=0)
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Enter valid amount !</font>',});
		$("#payable_amount").focus();
		$("#cash_pay_both").val('');
		$("#rtgs_pay_both").val('');
	}
	else
	{
		if($('#payment_by_both').val()=='BOTH')
		{
			$("#rtgs_pay_both").val((Number($("#payable_amount").val()) - Number($("#cash_pay_both").val())).toFixed(2));
		}	
	}
}
		
function pay_rtgs_both()
{
	if(Number($("#payable_amount").val())=='' || Number($("#payable_amount").val())<=0)
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Enter valid amount !</font>',});
		$("#payable_amount").focus();
		$("#cash_pay_both").val('');
		$("#rtgs_pay_both").val('');
	}
	else
	{
		if($('#payment_by_both').val()=='BOTH')
		{
			$("#cash_pay_both").val((Number($("#payable_amount").val()) - Number($("#rtgs_pay_both").val())).toFixed(2));
		}	
	}	
}
</script>

	<div class="col-md-12">
		<h5 class="bg-primary" style="padding:6px;color:#FFF">Carry Forward & Pay Hisab.</h5>
	</div>
		
		<div class="form-group col-md-4">
			<label>Amount Carry Forward <font color="red"><sup>*</sup></font></label>
			<input name="carry_fwd_amount" oninput="CarryFwdBoth();" value="0" id="carry_fwd_amount" type="number" min="1" class="form-control" required />
		</div>
		
		<div class="form-group col-md-4">
			<label>Amount PAY <font color="red"><sup>*</sup></font></label>
			<input name="payable_amount" oninput="PayAmtBoth();" value="0" id="payable_amount" type="number" min="1" class="form-control" required />
		</div>
		
		<div class="form-group col-md-4">
			<label>Payment By <font color="red"><sup>*</sup></font></label>
			<select style="font-size:12px" id="payment_by_both" name="payment_by_both" onchange="PaymentByBoth(this.value)" class="form-control" required>
				<option style="font-size:12px" value="">Select an option</option>
				<option style="font-size:12px" value="CASH">CASH</option>
				<option style="font-size:12px" value="RTGS">RTGS</option>
				<option style="font-size:12px" value="CHEQUE">CHEQUE</option>
				<option style="font-size:12px" value="BOTH">CASH + RTGS</option>
			</select>
		</div>
		
		<div id="cash_div_both" style="display:none" class="form-group col-md-6">
			<label>Cash Amount to PAY <font color="red"><sup>*</sup></font></label>
			<input name="cash_pay" oninput="pay_cash_both();" id="cash_pay_both" type="number" min="1" max="3000" class="form-control" required />
		</div>
		
		<div id="cheque_div_both" style="display:none" class="form-group col-md-6">
			<label>Cheque Number <font color="red"><sup>*</sup></font></label>
			<input name="cheque_no_both" oninput="this.value=this.value.replace(/[^a-zA-Z0-9,-]/,'')" id="cheque_no_both" type="text" class="form-control" required />
		</div>
			
		<div id="rtgs_div_both" style="display:none" class="form-group col-md-6">
			<label>RTGS Amount to PAY <font color="red"><sup>*</sup></font></label>
			<input id="rtgs_pay_both" oninput="pay_rtgs_both();" name="rtgs_pay" type="number" min="1" class="form-control" required />
		</div>
		
		<div class="form-group col-md-12">
			<button disabled type="submit" id="pay_hisab_submit_both" class="btn btn-sm btn-primary">Submit</button>
			&nbsp; &nbsp;
			<button type="button" onclick="RefreshProceed();" class="btn btn-sm btn-danger">Cancel</button>
		</div>
		
<?php
}
?>
	</div>
</form>

<script>
		
</script>	

<script>
$('.hisab_btns').hide();

<?php
if($closing_amount>=0)  // if closing amount is zero or +ve
{
	if($closing_amount==0)
	{
	?>
			$('#carry_forward_btn').show();
		
			$('#carry_forward_btn').attr('disabled',false);
			$('#pay_hisab_btn').attr('disabled',true);
			$('#pay_cfwd_btn').attr('disabled',true);
			$('#credit_amount_btn').attr('disabled',true);
	<?php	
	}
	else
	{
	?>
			$('#carry_forward_btn').show();
			$('#credit_amount_btn').show();
			
			$('#carry_forward_btn').attr('disabled',false);
			$('#pay_hisab_btn').attr('disabled',true);
			$('#pay_cfwd_btn').attr('disabled',true);
			$('#credit_amount_btn').attr('disabled',false);
	<?php	
	}
}
else // if closing amount is -ve
{
?>
	$('#carry_forward_btn').show();
	$('#pay_hisab_btn').show();
	$('#pay_cfwd_btn').show();
			
	$('#carry_forward_btn').attr('disabled',false);
	$('#pay_hisab_btn').attr('disabled',false);
	$('#pay_cfwd_btn').attr('disabled',false);
	$('#credit_amount_btn').attr('disabled',true);
<?php
}
	
if($down_driver=="0" AND $closing_amount>0)	
{	
?>
	$('#credit_amount_btn').hide();
	$('#credit_amount_btn').attr('disabled',true);
<?php
}
?>
	
$('#loadicon').fadeOut('slow');	
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#PayHisabFormId").on('submit',(function(e) {
$("#loadicon").show(); 
$('#pay_hisab_submit').attr('disabled',true);
e.preventDefault();
	$.ajax({
	url: "./final_pay_hisab.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_data").html(data);
	},
	error: function() 
	{} });}));});
</script>


<script type="text/javascript">
$(document).ready(function (e) {
$("#PayhisabBothFunc").on('submit',(function(e) {
$("#loadicon").show();
$('#pay_hisab_submit_both').attr('disabled',true);
e.preventDefault();
	$.ajax({
	url: "./final_pay_hisab_both.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_main").html(data);
	},
	error: function() 
	{} });}));});
</script>