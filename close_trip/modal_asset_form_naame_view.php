<div class="modal fade print_div" id="Asset_form" style="background:#DDD" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
		<h4 class="modal-title"><b>ASSET - FORM</h4></b>
			<h5> Truck No: <?php echo $tno; ?><br><br> Driver Name: <?php echo $driver_name; ?></h5>
		</div>
		
<div class="modal-body">

 <table class="table table-bordered" style="font-size:15px;">
		<tr>
			<th>सामान दिया </th>
			<th>मात्रा सामान (देते समय )</th>
			<th>मात्रा सामान (जमा करते समय )</th>
			<th>रकम नामे </th>
		</tr>
	
	<?php
	mysqli_set_charset($conn, 'utf8');
	
	$qry_asset_item_view = Qry($conn,"SELECT a.item_qty,a.quantity,a.amount_naame,i.item_name 
	FROM dairy.asset_form as a 
	LEFT OUTER JOIN dairy.asset_form_items AS i ON i.code=a.item_code 
	WHERE a.tno='$tno' AND a.item_value_up='1' AND a.amount_naame>0");
	
	if(!$qry_asset_item_view){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$naame_total_sum = 0;
	if(numRows($qry_asset_item_view)>0)
		{
			while($row_asset_view = fetchArray($qry_asset_item_view))
			{
				$naame_total_sum = $naame_total_sum+$row_asset_view['amount_naame'];
				echo "
				<tr>
					<td> 
						$row_asset_view[item_name]
					</td>
					
					<td> 
						$row_asset_view[item_qty]
					</td>
					
					<td> 
						$row_asset_view[quantity]
					</td>
				
					<td> 
						₹ $row_asset_view[amount_naame]
					</td>
					
 				</tr>";
			}
		}

?>
		<tr>
			<td colspan="3"> 
				<b>Total Amount : </b>
			</td>
			<td> ₹ <?php echo $naame_total_sum; ?>
			</td>
		</tr>
<?php

	?>
</table>

</div>

<div class="modal-footer">
       <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	</div>
	
    </div>
  </div>
</div>