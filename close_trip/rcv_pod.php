<?php
require("../connect.php");

if(!isset($_SESSION['diary']))
{
	Redirect("","./logout.php");
	exit();
}

// if($_SESSION['user_code']!='032')
// {
	// Redirect("Pod upload on hold till 11.00 PM.","../");
	// exit();
// }

$tno=$_SESSION['diary'];

if($branch=='')
{
	Redirect("Unable to Fetch Branch","../");
	exit();
}

$date2=date("Y-m-d");
$time_1=date('H:i:s');
$timestamp=date("Y-m-d H:i:s");

$trip_id=escapeString($conn,$_POST['trip_id']);
$trip_no=escapeString($conn,$_POST['trip_no']);
$lrno=escapeString($conn,$_POST['lr_type']);
$lrnos=escapeString($conn,$_POST['lrnos']);

if($trip_id=='' || $trip_no=='' || $lrno=='' || $lrnos=='')
{
	Redirect("Unable to fetch Trip Id or Trip No or LR No.","../");
	exit();
}

if(substr($lrnos,3,5)=='OLRSP')
{
	echo "<script>
		alert('Warning : LR Belongs to COAL.');
		$('#loadicon').hide();
	</script>";
	exit();
}
	
$lr_count=substr_count($lrno, ',');

if($lr_count>2)
{
	errorLog("More than 2 Bilty Found.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	
	echo "<script>
				swal({
				  title: 'More than 2 Bilty Found.',
				  type: 'error',
				  closeOnConfirm: true
				});
		$('#loadicon').hide();
		$('#pod_button').attr('disabled', false);
	</script>";
	exit();
}

if(count($_FILES['pod_copy']['name']) > 0)
{
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	foreach($_FILES['pod_copy']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			echo "<script>
				swal({
				  title: 'Only Image Upload Allowed',
				  type: 'error',
				  closeOnConfirm: true
				});
			$('#loadicon').hide();
			$('#pod_button').attr('disabled', false);
			</script>";
			exit();
		}
    }
        
		for($i=0; $i<count($_FILES['pod_copy']['name']); $i++)
		{
          
		  $sourcePath = $_FILES['pod_copy']['tmp_name'][$i];

            if($sourcePath!="")
			{
			$fix_name=$trip_id."-".$trip_no.$tno.date('dmYHis').$i;
            $shortname = "pod/".$fix_name.".".pathinfo($_FILES['pod_copy']['name'][$i],PATHINFO_EXTENSION);
			$targetPath = "pod/".$fix_name.".".pathinfo($_FILES['pod_copy']['name'][$i],PATHINFO_EXTENSION);

			if($_FILES['pod_copy']['type'][$i]!='application/pdf')
			{		
				ImageUpload(1000,1000,$_FILES['pod_copy']['tmp_name'][$i]);
			}

				if(move_uploaded_file($sourcePath, $targetPath))
			   {
						$files[] = $shortname;
               }
			   else
			   {
				   echo "<script type='text/javascript'>
						swal({
						  title: 'Unable to Upload Files',
						  type: 'error',
						  closeOnConfirm: true
						});
						$('#loadicon').hide();
						$('#pod_button').attr('disabled', false);
					</script>";
					exit();
			   }
			}
			else
			{
				echo "<script type='text/javascript'>
					swal({
					  title: 'No Upload Found',
					  type: 'error',
					  closeOnConfirm: true
					});
					$('#loadicon').hide();
					$('#pod_button').attr('disabled', false);
				</script>";
				exit();
			}

        }
		$file_name=implode(',',$files);
    }
	else
	{
		echo "<script type='text/javascript'>
			swal({
				  title: 'No Upload Found',
				  type: 'error',
				  closeOnConfirm: true
				});
			$('#loadicon').hide();
			$('#pod_button').attr('disabled', false);
		</script>";
		exit();
	}

StartCommit($conn);
$flag = true;

$qry=Qry($conn,"INSERT INTO dairy.rcv_pod(trip_id,tno,lrno,trip_no,date,copy,branch,branch_user,timestamp) VALUES 
('$trip_id','$tno','$lrnos','$trip_no','$date2','$file_name','$branch','$_SESSION[user_code]','$timestamp')");

if(!$qry)
{
	foreach(explode(",",$file_name) as $uploaded_files)
	{
		unlink($uploaded_files);
	}
	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(substr($lrnos,3,1)=='M')
{
	$updateMktBilty=Qry($conn,"UPDATE dairy.bilty_balance SET pod='1',pod_copy='$file_name' WHERE bilty_no='$lrnos'");
	if(!$updateMktBilty)
	{
		foreach(explode(",",$file_name) as $uploaded_files)
		{
			unlink($uploaded_files);
		}
	
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("NO MARKET BILTY FOUND IN BILTY BALANCE. LRNO : $lrnos",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	}
}

$update=Qry($conn,"UPDATE dairy.trip SET pod=pod+1,pod_date='$timestamp' WHERE id='$trip_id'");
if(!$update)
{
	foreach(explode(",",$file_name) as $uploaded_files)
	{
		unlink($uploaded_files);
	}
	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
}
else
{
	MySQLRollBack($conn);
	errorLog("COMMIT NOT COMPLETED. Timestamp : $timestamp. Truck No: $tno, TripNo: $trip_no.",$conn,$page_name,__LINE__);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}

	echo "<script type='text/javascript'>
				$('#loadicon').hide();
				$('#pod_button').attr('disabled', true);
				document.getElementById('PODForm').reset();
			swal({
				  title: 'Files Uploaded Succesfully',
				  type: 'success',
				  closeOnConfirm: true
				},
				function(){
					window.location.href='./';
				});
		</script>";
	closeConnection($conn);	
	exit();
?>