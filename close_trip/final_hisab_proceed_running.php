<?php
require_once("../connect.php");

// if($branch!='HEAD')
// {
	// echo "<script type='text/javascript'>
			// alert('Hisab is under maintenance till 5 PM Today !');
			// window.location.href='./';
		// </script>";
	// exit();
// }
	
$timestamp = date("Y-m-d H:i:s");

$tno=escapeString($conn,strtoupper($_SESSION['diary']));

if($branch=='')
{
	errorLog("Unable to fetch Branch.",$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","./");
	exit();
}

if(!isset($_POST['hisab_verify']))
{
	Redirect("Error while processing request.","./");
	exit();
}

if(isset($_POST['hisab_cache']))
{
	$hisab_cache_found="YES";
}
else
{
	$hisab_cache_found="NO";
}

$chk_running_script = Qry($conn,"SELECT id FROM dairy.running_script");
if(!$chk_running_script){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_running_script)>0)
{
	echo "<script type='text/javascript'>
			alert('Please wait.. Try after some time !!');
			window.close();
		</script>";
	exit();
}

$chk_for_hisab_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$chk_for_hisab_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if($hisab_cache_found=='YES' AND numRows($chk_for_hisab_cache)==0)
{
	errorLog("HISAB Cache set but not found in Database.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if($hisab_cache_found=='NO')
{
	
$chk_for_bulker = Qry($conn,"SELECT lock_ag,lock_tel FROM dairy.own_truck WHERE tno='$tno'");
if(!$chk_for_bulker){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_bulker = fetchArray($chk_for_bulker);	
	
$down_driver = $_POST['down_driver_selection']; // Driver down YES/NO
$down_type = $_POST['down_driver_type']; // Driver Down Type. on leave etc..
$left_reason = escapeString($conn,strtoupper($_POST['reason_left']));
$standby_reason = escapeString($conn,strtoupper($_POST['standby_reason']));
$standby_reason_other = escapeString($conn,strtoupper($_POST['reason_other']));
$gps_km = $_POST['gps_km'];

$da = $_POST['da'];
$tel = $_POST['tel'];
$ag = $_POST['ag'];
$give_salary = $_POST['give_salary'];
$salary_amount = $_POST['salary'];
$driver_code = escapeString($conn,strtoupper($_POST['driver_code']));
$salary_from = escapeString($conn,strtoupper($_POST['sal_from_date']));
$salary_to = escapeString($conn,strtoupper($_POST['sal_to_date']));

if($salary_amount!=$_POST['salary_amount_cache'])
{
	errorLog("Problem with salary amount. Cache is : $_POST[salary_amount_cache]. And Client us : $salary_amount.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$da_from = escapeString($conn,strtoupper($_POST['da_from_date']));
$da_to = escapeString($conn,strtoupper($_POST['da_to_date']));
$da_amt_per_day = escapeString($conn,strtoupper($_POST['da_amt']));

$diff_da_days = (strtotime($da_to)- strtotime($da_from))/24/3600; 
$diff_da_days=$diff_da_days+1;

$diff_da_salary = (strtotime($salary_to)- strtotime($salary_from))/24/3600; 
$diff_da_salary=$diff_da_salary+1;

if($da!=($diff_da_days*$da_amt_per_day))
{
	$system_da = $diff_da_days*$da_amt_per_day;
	errorLog("DA amount not verified. DA amount : $da. System Amount : $system_da.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$from_station = escapeString($conn,strtoupper($_POST['from_station']));
$to_station = escapeString($conn,strtoupper($_POST['to_station']));
$diff_days = escapeString($conn,strtoupper($_POST['diff_days'])); // Trip Days Total

$system_km = escapeString($conn,strtoupper($_POST['system_km']));
$closing_amount_system = escapeString($conn,strtoupper($_POST['closing']));

$diff_da_pay=escapeString($conn,$_POST['diff_da_pay']);

if($down_driver==1)
{		
	$qry_asset = Qry($conn,"SELECT SUM(amount_naame) as asset_amount FROM dairy.asset_form WHERE driver_code='$driver_code' AND 
	amount_naame>0");
	
	if(!$qry_asset)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
	
	$row_asset = fetchArray($qry_asset);
	$asset_amount = $row_asset['asset_amount'];
}
else
{
	$asset_amount=0;	
}

if($down_type=='LEFT' || $down_type=='LEFT_ROUTE')
{
	if($left_reason=='')
	{
		errorLog("Unable to fetch Driver Left Reason.",$conn,$page_name,__LINE__);
		Redirect("Unable to fetch Driver Left Reason.","./");
		exit();
	}	
}
else if($down_type=='STAND_BY')
{
	if($standby_reason=='')
	{
		errorLog("Unable to fetch Driver STAND_BY Reason.",$conn,$page_name,__LINE__);
		Redirect("Unable to fetch Driver STAND_BY Reason.","./");
		exit();
	}
	else if($standby_reason=='OTHER')
	{
		if($standby_reason_other=='')
		{
			errorLog("Unable to fetch Driver STAND_BY Other Reason.",$conn,$page_name,__LINE__);
			Redirect("Unable to fetch Driver STAND_BY Other Reason.","./");
			exit();
		}	
	}
}

$chk_driver_balance = Qry($conn,"SELECT code,up,amount_hold,salary_type,salary_amount 
FROM dairy.driver_up WHERE down=0 AND code IN(SELECT driver_code FROM dairy.own_truck WHERE tno='$tno') 
ORDER BY id DESC LIMIT 1");

if(!$chk_driver_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_driver_bal = fetchArray($chk_driver_balance);

$driver_balance_db = $row_driver_bal['amount_hold'];
$driver_code_db = $row_driver_bal['code'];
$salary_amount_master = $row_driver_bal['salary_amount'];
$salary_type_db = $row_driver_bal['salary_type'];
$driver_up_date = $row_driver_bal['up'];

if($driver_code_db!=$driver_code)
{
	errorLog("Driver Code not verified. DB is : $driver_code_db. Client is : $driver_code.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if($driver_balance_db!=$_POST['balance_on_trip'])
{
	errorLog("Driver Balance not verified. DB is : $driver_balance_db. Client is : $_POST[balance_on_trip].",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$chk_da_dates = Qry($conn,"SELECT to_date,narration FROM dairy.da_book WHERE driver_code='$driver_code' ORDER BY id DESC LIMIT 1");
if(!$chk_da_dates)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($chk_da_dates)>0)
{
	$row_min_da = fetchArray($chk_da_dates);
	
	if($row_min_da['narration']=='ON_LEAVE_UP' || $row_min_da['narration']=='LEFT_COMP_UP' || $row_min_da['narration']=='DA_START_ADMIN')
	{
		if(($da_from<$row_min_da['to_date']) || ($da_to>date("Y-m-d")))
		{
			errorLog("DA Dates not verified. DB is : $row_min_da[to_date]. Client is : $da_from. Da To Date : $da_to.",$conn,$page_name,__LINE__);
			Redirect("Error While Processing Request.","./");
			exit();
		}
	}
	else
	{
		if($da_from!='2021-03-31' AND $da_to!='2021-03-31')
		{
			if(($da_from<$driver_up_date) || ($da_from<=$row_min_da['to_date']) || ($da_to>date("Y-m-d")))
			{
				errorLog("Last Da Date $row_min_da[to_date] and Driver Up Date is : $driver_up_date. User Selected : $da_from.",$conn,$page_name,__LINE__);
				$next_da_dates = date("d-m-y",strtotime($row_min_da['to_date']. ' + 1 days'));
				Redirect("DA From Date Value Must be greater than or equal to : $next_da_dates. Please Try Again !!!","./");
				exit();
			}
		}
	}
}
else
{
	if(($da_from<$driver_up_date) || ($da_to>date("Y-m-d")))
	{
		errorLog("DA Dates not verified. DB is : $driver_up_date. Client is : $da_from. Da To Date : $da_to.",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}

$chk_diesel_qty = Qry($conn,"SELECT SUM(qty) as sum_qty FROM dairy.diesel WHERE trip_id IN(SELECT id FROM dairy.trip WHERE tno='$tno')");
if(!$chk_diesel_qty){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_diesel_db = fetchArray($chk_diesel_qty);

$diesel_qty_db=sprintf('%0.2f',$row_diesel_db['sum_qty']);

if($diesel_qty_db!=$_POST['diesel_qty'])
{
	errorLog("Diesel QTY not verified. DB is : $diesel_qty_db. Client is : $_POST[diesel_qty].",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$chk_trip = Qry($conn,"SELECT 
(SELECT date(date) FROM dairy.trip WHERE tno='$tno' ORDER BY id ASC LIMIT 1) as 'from',
(SELECT date(end_date) FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1) as 'to',
(SELECT trip_no FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1) as 'trip_no',
(SELECT SUM(km) FROM dairy.trip WHERE tno='$tno' AND narration!='TRAIN') as 'total_km'");

if(!$chk_trip)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_trip_chk = fetchArray($chk_trip);

$start_date_db = $row_trip_chk['from'];
$end_date_db = $row_trip_chk['to'];

if($end_date_db==0 || $end_date_db=="")
{
	$end_date_db=date("Y-m-d");
}

$total_km_db = $row_trip_chk['total_km'];
$trip_no = $row_trip_chk['trip_no'];

$diff_trip_days_db = (strtotime($end_date_db)- strtotime($start_date_db))/24/3600; 
$diff_trip_days_db=$diff_trip_days_db+1;

$chk_standby_approval = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no' AND stand_by_approval='1'");

if(!$chk_standby_approval){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($chk_standby_approval)==0)
{
	if($down_driver=="1" AND $down_type=='STAND_BY')
	{
		Redirect("Stand-by approval pending.","./");
		exit();
	}
}

if($diff_days!=$diff_trip_days_db)
{
	errorLog("Trip days not verified. DB Is : $diff_trip_days_db. Client is : $diff_days.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if($row_bulker['lock_ag']=='1')
{
	if($ag!=0)
	{
		errorLog("AG amount not verified.. AG amount : $ag. Must Be : 0.",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}
else
{
	if($ag!=$diff_da_days*10)
	{
		$system_tel_ag = $diff_da_days*10;
		errorLog("AG amount not verified.. amount : $ag. System Amount : $system_tel_ag.",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}

if($row_bulker['lock_tel']=='1')
{
	if($tel!=0)
	{
		errorLog("tel amount not verified.. tel amount : $tel. Must Be : 0.",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}
else
{
	if($tel!=$diff_da_days*10)
	{
		$system_tel_ag = $diff_da_days*10;
		errorLog("tel amount not verified.. amount : $tel. System Amount : $system_tel_ag.",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}

if($total_km_db!=$system_km)
{
	errorLog("System KM not verified. DB is : $total_km_db. Client is : $system_km.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$fetch_driver_name = Qry($conn,"SELECT name,last_salary FROM dairy.driver WHERE code='$driver_code'");

if(!$fetch_driver_name)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_driver_name = fetchArray($fetch_driver_name);

$driver_name=$row_driver_name['name'];
$driver_last_salary=$row_driver_name['last_salary'];
		
if($give_salary=='1')
{
	if($salary_type_db=='1')
	{
		$chk_trip_days_without_empty = Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$tno' AND lr_type!='EMPTY'");
		if(!$chk_trip_days_without_empty)
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error While Processing Request.","./");
			exit();
		}
		
		$trip_num_rows = numRows($chk_trip_days_without_empty);
		
		if($salary_amount!=($trip_num_rows*$salary_amount_master))
		{
			$sal_amount_db = $trip_num_rows*$salary_amount_master;
			
			errorLog("Salary Amount not verified. DB is : $sal_amount_db. Client is : $salary_amount.",$conn,$page_name,__LINE__);
			Redirect("Error While Processing Request.","./");
			exit();
		}
	}
	else
	{
		$chk_salary = Qry($conn,"SELECT to_date,narration FROM dairy.salary WHERE driver_code='$driver_code' ORDER BY id DESC LIMIT 1");
		if(!$chk_salary)
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error While Processing Request.","./");
			exit();
		}

		if(numRows($chk_salary)==0)
		{
			if($driver_last_salary==0)
			{
				$ChkSalDate=$driver_up_date;
			}
			else
			{
				$ChkSalDate=$driver_last_salary;
			}	
			
			if(($salary_from<$ChkSalDate) || ($salary_to>date("Y-m-d")))
			{
				errorLog("Salary Dates not verified. DB is : $ChkSalDate. Client is : $salary_from. To date : $salary_to.",$conn,$page_name,__LINE__);
				Redirect("Error While Processing Request.","./");
				exit();
			}
		}
		else
		{
			$row_chk_sal = fetchArray($chk_salary);
		
			$narr_sal = $row_chk_sal['narration'];
			 
			if($narr_sal=='ON_LEAVE_UP' || $narr_sal=='LEFT_COMP_UP' || $narr_sal=='SALARY_START_ADMIN')
			{
				if(($salary_from<$row_chk_sal['to_date']) || ($salary_to>date("Y-m-d")))
				{
					errorLog("Salary Dates not verified. DB is : $row_chk_sal[to_date]. Client is : $salary_from. To date : $salary_to.",$conn,$page_name,__LINE__);
					Redirect("Error While Processing Request.","./");
					exit();
				}
			}
			else
			{
				if(($salary_from<=$row_chk_sal['to_date']) || ($salary_to>date("Y-m-d")))
				{
					errorLog("Salary Dates not verified. DB is : $row_chk_sal[to_date]. Client is : $salary_from. To date : $salary_to.",$conn,$page_name,__LINE__);
					Redirect("Error While Processing Request.","./");
					exit();
				}
			}
		}
	}
}
else
{
	$salary_amount="0";
	$salary_from="";
	$salary_to="";
}

if($closing_amount_system!=($driver_balance_db-($da+$salary_amount+$tel+$ag)+$asset_amount))
{
	$sys_cal_closing = $driver_balance_db-($da+$salary_amount+$tel+$ag)+$asset_amount;
	errorLog("Vehicle : $tno. Closing not verified. Server Side is : $sys_cal_closing. Client side is : $closing_amount_system. (Opening : $driver_balance_db, DA : $da, SALARY : $salary_amount, Tel: $tel, Ag: $ag,, Asset Naame: $asset_amount).",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if($row_bulker['lock_ag']=="1" AND $ag!=0)
{
	errorLog("AG amount not verified.. AG amount : $ag. Must Be : 0.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if($row_bulker['lock_tel']=="1" AND $tel!=0)
{
	errorLog("Tel amount not verified.. Tel amount : $tel. Must Be : 0.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$hisab_cache = Qry($conn,"INSERT INTO dairy.hisab_cache(final_running,branch,tno,trip_no,lock_ag,lock_tel,driver_name,driver_code,down_driver,down_type,left_reason,
standby_reason,standby_reason_other,give_salary,da_amt_per_day,from_station,to_station,balance_on_hisab,da_amount,da_days,
da_from,da_to,salary_amount,sal_from,sal_to,trip_start,trip_end,trip_days,tel_amount,ag_amount,closing,asset_form_naame,diesel_qty,sys_km,
gps_km,timestamp) VALUES ('0','$branch','$tno','$trip_no','$row_bulker[lock_ag]','$row_bulker[lock_tel]','$driver_name','$driver_code','$down_driver','$down_type','$left_reason','$standby_reason',
'$standby_reason_other','$give_salary','$da_amt_per_day','$from_station','$to_station','$driver_balance_db','$da','$diff_da_days',
'$da_from','$da_to','$salary_amount','$salary_from','$salary_to','$start_date_db','$end_date_db','$diff_days','$tel','$ag',
'$closing_amount_system','$asset_amount','$diesel_qty_db','$system_km','$gps_km','$timestamp')");

if(!$hisab_cache)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

}

$get_cache_data = Qry($conn,"SELECT trip_no,lock_ag,lock_tel,driver_name,driver_code,down_driver,down_type,from_station,to_station,balance_on_hisab,
da_amount,da_from,da_to,salary_amount,sal_from,sal_to,trip_start,trip_end,trip_days,tel_amount,ag_amount,closing,asset_form_naame,diesel_qty,
sys_km,gps_km FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$get_cache_data)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_get_cache = fetchArray($get_cache_data);

if($row_get_cache['lock_ag']=='1' AND $row_get_cache['ag_amount']!=0)
{
	errorLog("Air grease amount must be zero.",$conn,$page_name,__LINE__);
	echo "<script>
			alert('Error While Processing Request !!!');
			window.close();
		</script>";
	exit();
}

if($row_get_cache['lock_tel']=='1' AND $row_get_cache['tel_amount']!=0)
{
	errorLog("Tel grease amount must be zero.",$conn,$page_name,__LINE__);
	echo "<script>
			alert('Error While Processing Request !!!');
			window.close();
		</script>";
	exit();
}

$trip_no = $row_get_cache['trip_no'];
$driver_name = $row_get_cache['driver_name'];
$driver_code = $row_get_cache['driver_code'];
$down_driver = $row_get_cache['down_driver'];
$down_type = $row_get_cache['down_type'];
$from_station = $row_get_cache['from_station'];
$to_station = $row_get_cache['to_station'];
$balance_on_hisab = $row_get_cache['balance_on_hisab'];
$da_amount = $row_get_cache['da_amount'];
$da_from = $row_get_cache['da_from'];
$da_to = $row_get_cache['da_to'];
$salary_amount = $row_get_cache['salary_amount'];
$sal_from = $row_get_cache['sal_from'];
$sal_to = $row_get_cache['sal_to'];
$trip_start = $row_get_cache['trip_start'];
$trip_end = $row_get_cache['trip_end'];
$trip_days = $row_get_cache['trip_days'];
$tel_amount = $row_get_cache['tel_amount'];
$ag_amount = $row_get_cache['ag_amount'];
$asset_form_naame = $row_get_cache['asset_form_naame'];
$closing_db = $row_get_cache['closing'];
$diesel_qty = $row_get_cache['diesel_qty'];
$sys_km = $row_get_cache['sys_km'];
$gps_km = $row_get_cache['gps_km'];

	echo "<form action='./final_hisab_proceed_running.php' id='myForm3' method='POST'>
			<input type='hidden' name='hisab_verify' value='OK'>
			<input type='hidden' name='hisab_cache' value='OK'>
		</form>";
		
		echo "<script type='text/javascript'>
			function RefreshProceed()
			{
				document.getElementById('myForm3').submit();
			}
		</script>";

$dr_var = $da_amount+$tel_amount+$ag_amount+$salary_amount;
$closing_amount = ($balance_on_hisab-$dr_var)+$asset_form_naame;

if($closing_amount!=$closing_db)
{
	errorLog("Closing amount not verified. Sys : $closing_amount. Db : $closing_db.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$verify_opening_bal = Qry($conn,"SELECT amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");
if(!$verify_opening_bal)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
			alert('Error While Processing Request !!!');
			window.close();
		</script>";
	exit();
}

if(numRows($verify_opening_bal)==0)
{
	errorLog("Driver balance not found. Driver Code: $driver_code.",$conn,$page_name,__LINE__);
	echo "<script>
			alert('Driver not found !!!');
			window.close();
		</script>";
	exit();
}

$row_verify_opening = fetchArray($verify_opening_bal);

if($row_verify_opening['amount_hold']!=$balance_on_hisab)
{
	$delete_cache = Qry($conn,"DELETE FROM dairy.hisab_cache WHERE tno='$tno'");
	
	if(!$delete_cache)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>
			alert('Error While Processing Request !!!');
			window.close();
		</script>";
		exit();
	}

	echo "<script>
		alert('Error While Processing Request. Try to take hisab Again !!!');
		window.close();
	</script>";
	exit();
}
?>
<!doctype html>
<html>
<head>
	<meta name="google" content="notranslate">
	<title>Final HISAB || E-Diary || RAMAN ROADWAYS PVT. LTD.</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<!-- SWEET ALERT-->
	<script src="../sweetalert.js"></script>
    <link rel="stylesheet" href="../sweetalert.css">
	<!-- END SWEET ALERT-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:600&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="../../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
</head>

<style>
body{font-family:'Open Sans Condensed', 'sans-serif !important'}
</style>

<?php include("../code_disabled_right_click.php"); ?>

<?php include("../load_icon.php"); ?>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div id="result_main"></div>

<script type="text/javascript">
$(document).ready(function (e) {
$("#CaryyForwadHisab").on('submit',(function(e) {
$("#loadicon").show();
$('#carry_forward').attr('disabled',true);
e.preventDefault();
	$.ajax({
	url: "./final_carry_fwd_hisab_running.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_main").html(data);
	$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script>

		
<script type="text/javascript">
$(document).ready(function (e) {
$("#PayHisabForm").on('submit',(function(e) {
$("#loadicon").show();
$('#pay_hisab_submit').attr('disabled',true);
e.preventDefault();
	$.ajax({
	url: "./final_pay_hisab_running.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_main").html(data);
	$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script>


<script type="text/javascript">
$(document).ready(function (e) {
$("#PayHisabFormBoth").on('submit',(function(e) {
$("#loadicon").show();
$('#pay_hisab_submit_both').attr('disabled',true);
e.preventDefault();
	$.ajax({
	url: "./final_pay_hisab_both_running.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_main").html(data);
	$("#loadicon").hide();
	$('#pay_hisab_submit_both').attr('disabled',false);
	},
	error: function() 
	{} });}));});
</script>

<script>
function CreditToCashBookForm()
{
	var amount = $("#credit_amount_ho_value").val();
	
	if(amount<=0)
	{
		alert('invalid amount !');
	}
	else
	{
		$("#credit_to_cash_btn").attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
		url: "final_credit_to_cash_running.php",
		data: 'amount=' + amount,
		type: "POST",
		success: function(data) {
			$("#result_main").html(data);
		},
		error: function() {}
		});
	}
}
</script>

<style>
	.form-control{
		border:1px solid #000;
		text-transform:uppercase;
	}
</style>

<div class="container-fluid">

<div class="row">
	<div class="form-group col-md-12" style="box-shadow:0 0 50px #ddd inset;color:#000;padding:8px;">
		<div class="col-md-4">
			<b><span class="fa fa-truck"></span> &nbsp; <span style="letter-spacing:.5px;font-size:16px;"><?php echo $tno; ?></span></b>
			<br>
			<b><span class="fa fa-user-o"></span> &nbsp;  <?php echo $driver_name; ?>.</b>
		</div>	
		
		<div class="col-md-4">
			<center><span style="font-size:23px;">Loaded Vehicle - Hisab ( TripNo: <?php echo $trip_no; ?> )</span> 
			<br/>
			<?php echo "<font size='3'>".$from_station." to ".$to_station." (".$trip_days." days)</font>"; ?>
			</center>
		</div>
			
		<div class="col-md-4" style="text-align:right;color:maroon">
			<span style="text-align:right;font-size:18px;">System km : <?php echo $sys_km; ?></span> 
			,&nbsp; <span style="text-align:right;font-size:18px;">GPS km : <?php echo $gps_km; ?></span> 
			<br />
			<span style="text-align:right;font-size:18px;">AVERAGE : <?php if($diesel_qty>0) { echo "<b>".sprintf("%.2f",$gps_km/$diesel_qty)." kmpl</b>"; } else { echo "<b>NO_DIESEL</b>"; } ?></span> 
		</div>
	</div>	
</div>

<script>
function ClearCache(tno)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "clear_hisab_cache.php",
	data: 'tno=' + tno,
	type: "POST",
	success: function(data) {
		$("#result_data").html(data);
	},
	error: function() {}
	});
}
</script>

<div id="result_data"></div>

<div class="row">
	<div class="form-group col-md-12">
		<button onclick="ClearCache('<?php echo $tno; ?>')" class="btn btn-danger" type="button">Clear Hisab Cache</button>
	</div>	
</div>

<div class="row">

	<div class="form-group col-md-6">
<?php
include("modal_asset_form_naame_view.php");

$qry_fetch_ac = Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$driver_code'");
if(!$qry_fetch_ac){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

			if(numRows($qry_fetch_ac)>0)
			{
				$row_fetch_ac = fetchArray($qry_fetch_ac);
				$ac_holder=$row_fetch_ac['acname'];
				$ac_no=$row_fetch_ac['acno'];
				$ac_bank=$row_fetch_ac['bank'];
				$ac_ifsc=$row_fetch_ac['ifsc'];
			}
			?>
		
		<h5 style="background-color:#666;color:#FFF;padding:6px;">Driver Account Details :</h5>
		<table class="table table-bordered table-striped">
			<tr>
				<th>
					A/c Holder : <?php echo $ac_holder; ?>
				</th>
				<th>				
					A/c No : <?php echo $ac_no; ?>
				</th>
			</tr>	
			<tr>	
				<th>				
					Bank : <?php echo $ac_bank; ?>
				</th>
				<th>	
					IFSC : <?php echo $ac_ifsc; ?>
				</th>
			</tr>
		</table>

	<h5 style="background-color:#666;color:#FFF;padding:6px;">Driver's Outstanding : &nbsp; <span style="font-size:16px;letter-spacing:.5px;">
	<?php echo $balance_on_hisab+$asset_form_naame."/-"; ?></span></h5>
		<table class="table table-bordered table-striped" style="font-size:14px;">
			<tr>
				<th>DA : <?php echo "<span style='font-size:12px'>(".date("d-m-y", strtotime($da_from))." to ".date("d-m-y", strtotime($da_to)).")</span>"; ?></th>
				<th>₹ <?php echo $da_amount."/-"; ?></th>
			</tr>
			<tr> 
				<th>ASSETS Short : <?php if($asset_form_naame>0) { ?><a style="font-size:13px;" data-toggle='modal' data-target='#Asset_form'>View Asset Form</a> <?php } ?></th>
				<th><?php echo "₹ $asset_form_naame /-"; ?></th>
			</tr>
			<tr>
				<th>TELEPHONE :</th>
				<th>₹ <?php echo $tel_amount."/-"; ?></th>
			</tr>				
			<tr>
				<th>AIR & GREASE :</th>
				<th>₹ <?php echo $ag_amount."/-"; ?></th>
			</tr>	
			<tr>
				<th>SALARY : <?php if($salary_amount>0){echo "<font size='2'>(".date("d-m-y", strtotime($sal_from))." to ".date("d-m-y", strtotime($sal_to)).")</font>"; }?></th>
				<th>₹ <?php echo $salary_amount."/-"; ?></th>
			</tr>
			<tr class="success">
				<th>CLOSING AMOUNT :</th>
				<th>₹ <?php echo $closing_db."/-"; ?></th>
			</tr>
		</table>		
		
		<div class="form-group col-md-3" id="carry_forward_button_div" style="display:none1">
			<form id="CaryyForwadHisab" autocomplete="off">
				<button type="submit" id="carry_forward" name="carry_forward" class="btn btn-danger">CARRY FWD</button>
			</form>	
		</div>
		
		<div class="form-group col-md-3" id="pay_button_div" style="display:none1">
			<button type="button" onclick="PayHisabClick();" id="pay_hisab" name="pay_hisab" class="btn btn-primary">PAY HISAB</button>
		</div>
		
		<div class="form-group col-md-3" id="both_button_div" style="display:none">
			<button type="button" disabled style="color:#000" onclick="Both();" id="both_button" name="both_button" class="btn btn-warning">CARRY FWD + PAY</button>
		</div>
		
		<div class="form-group col-md-3" id="credit_amount_button_div" style="display:none1">
			<button type="button" disabled style="color:#FFF" onclick="CreditCash();" id="credit_amount" class="btn btn-success">Credit to CashBook</button>
		</div>
		
	</div>
	
	<div class="form-group col-md-6"> <!-- PAY STUFF DIV STARTS -->
	
		<input type="hidden" value="<?php echo $closing_db; ?>" id="closing_amount">
	
	<script>
	function DownData()
	{
		$('#loadicon').show();
		<?php
		if($down_driver==1)	
		{	
				if($closing_db>=0)
				{
					if($closing_db==0)
					{
					?>
					$('#carry_forward').attr('disabled',false);
					$('#pay_hisab').attr('disabled',true);
					$('#both_button').attr('disabled',true);
					$('#credit_amount').attr('disabled',true);
					<?php	
					}
					else
					{
					?>
					$('#carry_forward').attr('disabled',false);
					$('#pay_hisab').attr('disabled',true);
					$('#both_button').attr('disabled',true);
					$('#credit_amount').attr('disabled',false);
					<?php	
					}
				}
				else
				{
				?>
					$('#carry_forward').attr('disabled',false);
					$('#pay_hisab').attr('disabled',false);
					$('#both_button').attr('disabled',false);
					$('#credit_amount').attr('disabled',true);
				<?php
				}
		}
		else
		{
				if($closing_db>=0)
				{
					if($closing_db==0)
					{
					?>
					$('#carry_forward').attr('disabled',false);
					$('#pay_hisab').attr('disabled',true);
					$('#both_button').attr('disabled',true);
					$('#credit_amount').attr('disabled',true);
					<?php	
					}
					else
					{
					?>
					$('#carry_forward').attr('disabled',false);
					$('#pay_hisab').attr('disabled',true);
					$('#both_button').attr('disabled',true);
					$('#credit_amount').attr('disabled',true);
					<?php	
					}
				}
				else
				{
				?>
					$('#carry_forward').attr('disabled',false);
					$('#pay_hisab').attr('disabled',false);
					$('#both_button').attr('disabled',false);
					$('#credit_amount').attr('disabled',true);
				<?php
				}
		}		
		?>
	$('#loadicon').hide();	
	}
	</script>
	
	<script>
	function PayHisabClick()
	{
		$('#carry_forward').attr('disabled',true);
		$('#pay_hisab').attr('disabled',true);
		$('#both_button').attr('disabled',true);
		$('#credit_amount').attr('disabled',true);
		
		$("#pay_div").show();
		$("#amount_to_pay").val(Math.abs($('#closing_amount').val()));
	}
	
	function Both()
	{
		$('#carry_forward').attr('disabled',true);
		$('#pay_hisab').attr('disabled',true);
		$('#both_button').attr('disabled',true);
		$('#credit_amount').attr('disabled',true);
		
		$("#both_div").show();
		$("#amount_to_pay").val(Math.abs($('#closing_amount').val()));
	}
	
	function CreditCash()
	{
		$('#carry_forward').attr('disabled',true);
		$('#pay_hisab').attr('disabled',true);
		$('#both_button').attr('disabled',true);
		$('#credit_amount').attr('disabled',true);
		
		$("#amount_to_pay").val(Math.abs($('#closing_amount').val()));
		
		$("#credit_to_cash_div").show();
	}
	</script>
	
	<input type="hidden" id="amount_to_pay">	
	
<!------------------------------  PAY HISAB STARTS  --------------------------------->	
	
	<div id="pay_div" style="display:none" class="form-group col-md-12">
	
		<form autocomplete="off" id="PayHisabForm">
		<?php
			if($closing_db>=0)
			{
				?>
			<div class="row">	
			
				<div class="col-md-12">
					<div class="alert alert-danger" style="font-weight:bold;">
						चालक का बकाया है, भुगतान निषेध.
						DRIVER's outstanding is -ve. You Can't PAY.
					</div>	
				</div>	
				<?php
			}
			else
			{
		?>
		<div class="col-md-12">
			<h5 class="bg-primary" style="padding:6px;color:#FFF">PAY HISAB (<?php echo "Amount : ".abs($closing_db)." /-"; ?>)</h5>
		</div>
		
		<script>
		function PaymentBy(elem)
		{
			$("#rtgs_pay").val('');
			$("#cash_pay").val('');
			$("#cheque_no").val('');
			$("#cheque_no").val('');
				
			if(elem=='CASH')
			{
				if($("#amount_to_pay").val()>3000)
				{
					$('#pay_hisab_submit').attr('disabled',true);
					alert('Max Cash Pay Allowed is Rs : 3000.');
					RefreshProceed();
				}
				else
				{
					$("#pay_cash_div").hide();
					$("#pay_rtgs_div").hide();
					$("#pay_cheque_div").hide();
					
					$("#cash_pay").val($("#amount_to_pay").val());
					$("#cash_pay").attr('required',true);
					$("#cash_pay").attr('min','1');
					
					$("#rtgs_pay").val('0');
					$("#rtgs_pay").attr('required',false);
					$("#rtgs_pay").attr('min','0');
					
					$("#cheque_pay").val('0');
					$("#cheque_pay").attr('required',false);
					$("#cheque_pay").attr('min','0');
					
					$("#cheque_no").val('');
					$("#cheque_no").attr('required',false);
				}
			}
			else if(elem=='RTGS')
			{
				$("#pay_cash_div").hide();
				$("#pay_rtgs_div").hide();
				$("#pay_cheque_div").hide();
				
				$("#cash_pay").attr('required',false);
				$("#cash_pay").attr('min','0');
				$("#cash_pay").val('0');
				
				$("#rtgs_pay").attr('required',true);
				$("#rtgs_pay").attr('min','1');
				$("#rtgs_pay").val($("#amount_to_pay").val());
				
				$("#cheque_pay").val('0');
				$("#cheque_pay").attr('required',false);
				$("#cheque_pay").attr('min','0');
				
				$("#cheque_no").val('');
				$("#cheque_no").attr('required',false);
				
			}
			else if(elem=='CHEQUE')
			{
				$("#pay_cash_div").hide();
				$("#pay_rtgs_div").hide();
				$("#pay_cheque_div").show();
				
				$("#cash_pay").attr('required',false);
				$("#cash_pay").attr('min','0');
				$("#cash_pay").val('0');
				
				$("#rtgs_pay").attr('required',true);
				$("#rtgs_pay").attr('min','0');
				$("#rtgs_pay").val('0');
				
				$("#cheque_pay").val($("#amount_to_pay").val());
				$("#cheque_pay").attr('required',true);
				$("#cheque_pay").attr('min','1');
				
				$("#cheque_no").val('');
				$("#cheque_no").attr('required',true);
			}
			else if(elem=='BOTH')
			{
				$("#pay_cash_div").show();
				$("#pay_rtgs_div").show();
				$("#pay_cheque_div").hide();
				
				$("#cash_pay").attr('required',true);
				$("#rtgs_pay").attr('required',true);
				$("#cash_pay").val('');
				$("#rtgs_pay").val('');
				$("#rtgs_pay").attr('min','1');
				$("#cash_pay").attr('min','1');
				
				$("#cheque_pay").val('0');
				$("#cheque_pay").attr('required',false);
				$("#cheque_pay").attr('min','0');
				
				$("#cheque_no").val('');
				$("#cheque_no").attr('required',false);
			}
			else
			{
				$("#pay_cash_div").hide();
				$("#pay_rtgs_div").hide();
				$("#pay_cheque_div").hide();
				
				$("#cash_pay").attr('required',true);
				$("#rtgs_pay").attr('required',true);
				$("#cash_pay").val('');
				$("#rtgs_pay").val('');
				$("#rtgs_pay").attr('min','1');
				$("#cash_pay").attr('min','1');
				
				$("#cheque_pay").val('');
				$("#cheque_pay").attr('required',true);
				$("#cheque_pay").attr('min','1');
				
				$("#cheque_no").val('');
				$("#cheque_no").attr('required',true);
			}
		}
		</script>
		
		<div class="form-group col-md-6">
			<label>Payment By <font color="red"><sup>*</sup></font></label>
			<select id="payment_by" name="payment_by" onchange="PaymentBy(this.value)" class="form-control" required>
				<option value="">Select an option</option>
				<option value="CASH">CASH</option>
				<option value="CHEQUE">CHEQUE</option>
				<option value="RTGS">RTGS</option>
				<option value="BOTH">CASH + RTGS</option>
			</select>
		</div>
		
		<script>
		function pay_cash()
		{
			if($('#payment_by').val()=='BOTH')
			{
				$("#rtgs_pay").val((Number($("#amount_to_pay").val()) - Number($("#cash_pay").val())).toFixed(2));
			}	
		}
		
		function pay_rtgs()
		{
			if($('#payment_by').val()=='BOTH')
			{
				$("#cash_pay").val((Number($("#amount_to_pay").val()) - Number($("#rtgs_pay").val())).toFixed(2));
			}	
		}
		</script>
		
		<div id="pay_cheque_div" style="display:none">
			
			<input name="cheque_pay" id="cheque_pay" type="number" style="display:none" min="1" class="form-control" required />
			
			<div class="form-group col-md-6">
				<label>Cheque Number <font color="red"><sup>*</sup></font></label>
				<input name="cheque_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9,-]/,'')" id="cheque_no" type="text" class="form-control" required />
			</div>
			
		</div>
		
		<div id="pay_cash_div" style="display:none" class="form-group col-md-6">
			<label>Cash Amount to PAY <font color="red"><sup>*</sup></font></label>
			<input name="cash_pay" oninput="pay_cash();" id="cash_pay" type="number" min="1" max="3000" class="form-control" required />
		</div>
		
		<div id="pay_rtgs_div" style="display:none" class="form-group col-md-6">
			<label>RTGS Amount to PAY <font color="red"><sup>*</sup></font></label>
			<input id="rtgs_pay" oninput="pay_rtgs();" name="rtgs_pay" type="number" min="1" class="form-control" required />
		</div>
		
		<input type="hidden" name="total_amount" value="<?php echo abs($closing_amount); ?>">
		
		<div class="form-group col-md-12">
			<button <?php if($closing_db>=0) {echo "disabled";} ?> type="submit" id="pay_hisab_submit" class="btn btn-primary">SUBMIT</button>
			&nbsp; &nbsp;
			<button type="button" onclick="RefreshProceed();" class="btn btn-danger">Cancel</button>
		</div>
		</form>
			<?php } ?>
	</div>
</div>

<!----------------------------     PAY DIV ENDS ------------------------------------>

<!----------------------------         CREDIT TO CASH DIV STARTSSS ------------------------------------>

	<div id="credit_to_cash_div" style="display:none" class="form-group col-md-12">
		
		<div class="form-group col-md-12">
			<div class="alert alert-danger" style="font-weight:bold;">
				चालक का बकाया है, रिकवर करे. <br>
				DRIVER's outstanding is -ve, Recover it.
			</div>	
		</div>
	
		<input type="hidden" id="credit_amount_ho_value" name="credit_amount_ho_value" value="<?php echo abs($closing_db); ?>">
		<div class="form-group col-md-6">
			<button type="button" onclick="CreditToCashBookForm()" name="credit_to_cash" id="credit_to_cash_btn" class="btn btn-primary btn-lg">
			Credit <?php echo abs($closing_db); ?> /- to your CashBook</button>
		</div>
		
	</div>
	
<!----------------------------         CREDIT TO CASH DIV ENDSS ------------------------------------>	
	
<!-------------------------  CARRY FWD + PAY HISAB STARTS ------------------------------------>	
	
	<div id="both_div" style="display:none" class="form-group col-md-6">
		
		<form autocomplete="off" id="PayHisabFormBoth">
		<?php
			if($closing_db>=0)
			{
				?>
				<div class="col-md-12">
					<div class="alert alert-danger" style="font-weight:bold;">
						चालक का बकाया है, भुगतान निषेध.
						DRIVER's outstanding is -ve. You Can't PAY.
					</div>	
				</div>	
				<?php
			}
			else
			{
		?>
		
		<div class="col-md-12">
			<h5 class="bg-primary" style="padding:6px;color:#FFF">CARRY FORWARD AND PAY HISAB</h5>
		</div>
		
		<div class="form-group col-md-4">
			<label>Amount Carry Forward <font color="red"><sup>*</sup></font></label>
			<input name="carry_fwd_amount" oninput="CaryyFwd();" value="0" id="carry_fwd_amount" type="number" min="1" class="form-control" required />
		</div>
		
		<div class="form-group col-md-4">
			<label>Amount PAY <font color="red"><sup>*</sup></font></label>
			<input name="payable_amount" oninput="PayAmt();" value="0" id="payable_amount" type="number" min="1" class="form-control" required />
		</div>
		
		<div class="form-group col-md-6">
			<label>Payment By <font color="red"><sup>*</sup></font></label>
			<select id="payment_by_both" name="payment_by_both" onchange="PaymentByBoth(this.value)" class="form-control" required>
				<option value="">Select an option</option>
				<option value="CASH">CASH</option>
				<option value="RTGS">RTGS</option>
				<option value="CHEQUE">CHEQUE</option>
				<option value="BOTH">CASH + RTGS</option>
			</select>
		</div>
		
		<div id="cash_div_both" style="display:none" class="form-group col-md-6">
			<label>Cash Amount to PAY <font color="red"><sup>*</sup></font></label>
			<input name="cash_pay" oninput="pay_cash_both();" id="cash_pay_both" type="number" min="1" max="3000" class="form-control" required />
		</div>
		
		<div id="cheque_div_both" style="display:none" class="form-group col-md-6">
			<label>Cheque Number <font color="red"><sup>*</sup></font></label>
			<input name="cheque_no_both" id="cheque_no_both" type="text" class="form-control" required />
		</div>
			
		<input name="cheque_pay_both" id="cheque_pay_both" type="number" style="display:none" min="1" class="form-control" required />
		
		<div id="rtgs_div_both" style="display:none" class="form-group col-md-6">
			<label>RTGS Amount to PAY <font color="red"><sup>*</sup></font></label>
			<input id="rtgs_pay_both" oninput="pay_rtgs_both();" name="rtgs_pay" type="number" min="1" class="form-control" required />
		</div>
		
		<div class="form-group col-md-12">
			<button <?php if($closing_db>=0) {echo "disabled";} ?> type="submit" id="pay_hisab_submit_both" class="btn btn-primary">SUBMIT</button>
			&nbsp; &nbsp;
			<button type="button" onclick="RefreshProceed();" class="btn btn-danger">Cancel</button>
		</div>
		</form>
		<?php } ?>
	</div>
	
<!----------------------------    PAY BOTH DIV ENDSS ------------------------------------>	
	</div> <!-- PAY STUFF DIV END-->
	
	</div>
</div>
	
<script>
		function PaymentByBoth(elem)
		{
			if($("#payable_amount").val()<1 || $("#carry_fwd_amount").val()<1)
			{
				swal({
					  title: 'Enter Amount First !',
					  type: 'error',
					  closeOnConfirm: true
					});
				$("#payable_amount").val('');
				$("#carry_fwd_amount").val('');
				$("#payment_by_both").val('');
			}
			else
			{
				
			$("#payable_amount").attr('readonly',true);
			$("#carry_fwd_amount").attr('readonly',true);
			
			$("#rtgs_pay_both").val('');
			$("#cash_pay_both").val('');
			$("#cheque_pay_both").val('');
			$("#cheque_no_both").val('');
				
			if(elem=='CASH')
			{
				if($("#amount_to_pay").val()>3000)
				{
					$('#pay_hisab_submit_both').attr('disabled',true);
					alert('Max Cash Pay Allowed is Rs : 3000.');
					RefreshProceed();
				}
				else
				{
					$("#cash_div_both").hide();
					$("#cheque_div_both").hide();
					$("#rtgs_div_both").hide();
					
					$("#cash_pay_both").val($("#payable_amount").val());
					$("#cash_pay_both").attr('required',true);
					$("#cash_pay_both").attr('min','1');
					
					$("#rtgs_pay_both").val('0');
					$("#rtgs_pay_both").attr('required',false);
					$("#rtgs_pay_both").attr('min','0');
					
					$("#cheque_pay_both").val('0');
					$("#cheque_pay_both").attr('required',false);
					$("#cheque_pay_both").attr('min','0');
					
					$("#cheque_no_both").attr('required',false);
				}
			}
			else if(elem=='CHEQUE')
			{
				$("#cash_div_both").hide();
				$("#cheque_div_both").show();
				$("#rtgs_div_both").hide();
				
				$("#cash_pay_both").val('0');
				$("#cash_pay_both").attr('required',false);
				$("#cash_pay_both").attr('min','0');
				
				$("#rtgs_pay_both").val('0');
				$("#rtgs_pay_both").attr('required',false);
				$("#rtgs_pay_both").attr('min','0');
				
				$("#cheque_pay_both").val($("#payable_amount").val());
				$("#cheque_pay_both").attr('required',true);
				$("#cheque_pay_both").attr('min','1');
				
				$("#cheque_no_both").attr('required',true);
			}
			else if(elem=='RTGS')
			{
				$("#cash_div_both").hide();
				$("#cheque_div_both").hide();
				$("#rtgs_div_both").hide();
				
				$("#cash_pay_both").val('0');
				$("#cash_pay_both").attr('required',false);
				$("#cash_pay_both").attr('min','0');
				
				$("#rtgs_pay_both").val($("#payable_amount").val());
				$("#rtgs_pay_both").attr('required',true);
				$("#rtgs_pay_both").attr('min','1');
				
				$("#cheque_pay_both").val('0');
				$("#cheque_pay_both").attr('required',false);
				$("#cheque_pay_both").attr('min','0');
				
				$("#cheque_no_both").attr('required',false);
			}
			else if(elem=='BOTH')
			{
				$("#cash_div_both").show();
				$("#cheque_div_both").hide();
				$("#rtgs_div_both").show();
				
				$("#cash_pay_both").val('');
				$("#cash_pay_both").attr('required',true);
				$("#cash_pay_both").attr('min','1');
				
				$("#rtgs_pay_both").val('');
				$("#rtgs_pay_both").attr('required',true);
				$("#rtgs_pay_both").attr('min','1');
				
				$("#cheque_pay_both").val('0');
				$("#cheque_pay_both").attr('required',false);
				$("#cheque_pay_both").attr('min','0');
				
				$("#cheque_no_both").attr('required',false);
			}
			else
			{
				$("#cash_div_both").hide();
				$("#cheque_div_both").hide();
				$("#rtgs_div_both").hide();
				
				$("#cash_pay_both").val('0');
				$("#cash_pay_both").attr('required',true);
				$("#cash_pay_both").attr('min','1');
				
				$("#rtgs_pay_both").val('0');
				$("#rtgs_pay_both").attr('required',true);
				$("#rtgs_pay_both").attr('min','1');
				
				$("#cheque_pay_both").val('0');
				$("#cheque_pay_both").attr('required',true);
				$("#cheque_pay_both").attr('min','1');
				
				$("#cheque_no_both").attr('required',true);
			}
			}
		}
		</script>
		
		<script>
		function CaryyFwd()
		{
			$("#payable_amount").val((Math.abs(Number($("#closing_amount").val())) - Number($("#carry_fwd_amount").val())).toFixed(2));
		}
		
		function PayAmt()
		{
			$("#carry_fwd_amount").val((Math.abs(Number($("#closing_amount").val())) - Number($("#payable_amount").val())).toFixed(2));
		}
		
		
		
		function pay_cash_both()
		{
			if($("#payable_amount").val()=='')
			{
				swal({
					  title: 'Enter Amount to PAY First !',
					  type: 'error',
					  closeOnConfirm: true
					});
				
				$("#cash_pay_both").val('');
				$("#rtgs_pay_both").val('');
			}
			else
			{
				if($('#payment_by_both').val()=='BOTH')
				{
					$("#rtgs_pay_both").val((Number($("#payable_amount").val()) - Number($("#cash_pay_both").val())).toFixed(2));
				}	
			}
		}
		
		function pay_rtgs_both()
		{
			if($("#payable_amount").val()=='')
			{
				swal({
					  title: 'Enter Amount to PAY First !',
					  type: 'error',
					  closeOnConfirm: true
					});
					
				$("#cash_pay_both").val('');
				$("#rtgs_pay_both").val('');
			}
			else
			{
				if($('#payment_by_both').val()=='BOTH')
				{
					$("#cash_pay_both").val((Number($("#payable_amount").val()) - Number($("#rtgs_pay_both").val())).toFixed(2));
				}	
			}	
		}		
</script>	

<script>
	DownData();
</script>