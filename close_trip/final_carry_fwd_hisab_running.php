<?php
require_once("../connect.php");

// if($branch!='HEAD')
// {
	// Redirect("Error While Processing Request.","./");
	// exit();
// }

$timestamp = date("Y-m-d H:i:s"); 

$date=date("Y-m-d");

$tno=escapeString($conn,$_SESSION['diary']);

include("./_hisab_func.php");

$get_cache_data = Qry($conn,"SELECT trip_no,driver_name,driver_code,down_driver,down_type,left_reason,standby_reason,
standby_reason_other,balance_on_hisab,da_amount,da_from,da_to,salary_amount,sal_from,sal_to,tel_amount,ag_amount,closing,asset_form_naame,
diesel_qty,sys_km,gps_km FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$get_cache_data)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_cache_data)==0)
{
	errorLog("HISAB NOT FOUND. in Cache. Truck No: $tno.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_get_cache = fetchArray($get_cache_data);

$trip_no = $row_get_cache['trip_no'];
$driver_name = $row_get_cache['driver_name'];
$driver_code = $row_get_cache['driver_code'];
$down_driver = $row_get_cache['down_driver'];
$down_type = $row_get_cache['down_type'];
$balance_on_hisab = $row_get_cache['balance_on_hisab'];
$da_amount = $row_get_cache['da_amount'];
$da_from = $row_get_cache['da_from'];
$da_to = $row_get_cache['da_to'];
$salary_amount = $row_get_cache['salary_amount'];
$sal_from = $row_get_cache['sal_from'];
$sal_to = $row_get_cache['sal_to'];
$tel_amount = $row_get_cache['tel_amount'];
$ag_amount = $row_get_cache['ag_amount'];
$asset_form_naame = $row_get_cache['asset_form_naame'];
$closing_db = $row_get_cache['closing'];
$diesel_qty = $row_get_cache['diesel_qty'];
$sys_km = $row_get_cache['sys_km'];
$gps_km = $row_get_cache['gps_km'];

if($diesel_qty>0)
{
	$avg=sprintf("%.2f",$gps_km/$diesel_qty);
}
else
{
	$avg=0;
}

$da = $da_amount;
$tel = $tel_amount;
$ag = $ag_amount;

$balance = $balance_on_hisab;
$asset_amount = $asset_form_naame;

$salary_from = $sal_from;
$salary_to = $sal_to;

$closing=$closing_db;

if($closing!=($balance-($da+$tel+$ag+$salary_amount)+$asset_amount))
{
	errorLog("Unable to Verify Amount. Closing : $closing, DA : $da, SAL : $salary_amount, Tel & AG : $tel+$ag, Asset AmountName :$asset_amount.",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","");	
	exit();
}

if($down_driver==1)
{
	if($down_type=='STAND_BY'){
		$down_type_db=0;
		$down_reason = escapeString($conn,strtoupper($row_get_cache['standby_reason']));
		
		if($row_get_cache['standby_reason']=='OTHER'){
			$down_reason=escapeString($conn,strtoupper($row_get_cache['standby_reason_other']));
		}
		else{
			$down_reason=$row_get_cache['standby_reason'];
		}
	}
	else if($down_type=='ON_LEAVE'){
		$down_type_db=1;
		$down_reason="ON_LEAVE";
	}
	else if($down_type=='LEFT_ROUTE' || $down_type=='LEFT'){
		$down_type_db=2;
		$down_reason=escapeString($conn,strtoupper($row_get_cache['left_reason']));
	}
	else{
		errorLog("Driver down status not found. Driver Code : $driver_code.",$conn,$page_name,__LINE__);
		Redirect("Error while processing your request. Down Status","./");	
		exit();
	}
	$d_code_new="";
	$next_trip_opening = "0";
}
else
{
	$d_code_new=$driver_code;
	$next_trip_opening = $closing;
	$down_type_db = 0;
	$down_reason = "";
}

StartCommit($conn);
$flag = true;

$RunningTripBreakup = RunningTripBreakup($conn,$page_name,$tno,$trip_no,$branch,$_SESSION['user_code'],$gps_km,$avg,$diesel_qty,$next_trip_opening,$closing,$d_code_new,$date,$timestamp);

if($RunningTripBreakup==false){
	$flag = false;
}

$balance=$balance+$asset_amount;

if($asset_amount>0)
{
	$insert_driver_book_asset = AssetFormCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$balance,$driver_code,$asset_amount,$timestamp);
	
	if($insert_driver_book_asset==false){
		$flag = false;
	}

	$asset_row_id = mysqli_insert_id($conn);
}
else
{
	$asset_row_id = "0";
}

if($salary_amount>0)
{
	$insert_sal = SalaryCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$timestamp);
	
	if($insert_sal==false){
		$flag = false;
	}
	
	$balance=$balance-$salary_amount;
	
	$insert_driver_book_sal = SalaryCreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$balance,$timestamp);
	
	if($insert_driver_book_sal==false){
		$flag = false;
	}

	$salary_row_id = mysqli_insert_id($conn);
}
else
{
	$salary_row_id = "0";
}

if($da>0)
{
	$insert_da = DAcredit($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$timestamp);
	
	if($insert_da==false){
		$flag = false;
	}
	
	$balance=$balance-$da;
 
	$insert_da_1= DAcreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$balance,$timestamp);
	
	if($insert_da_1==false){
		$flag = false;
	}

	$da_row_id = mysqli_insert_id($conn);
}
else
{
	$da_row_id = "0";
}

	$balance=$balance-$tel;
	
	$insert_exp_tel = InsertTelephone($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$tel,$balance,$timestamp);
	
	if($insert_exp_tel==false){
		$flag = false;
	}
	
	$telephone_row_id = mysqli_insert_id($conn);
	
	$balance = $balance - $ag;

	$insert_exp_ag = InsertAirGrease($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$ag,$balance,$timestamp);

	if($insert_exp_ag==false){
		$flag = false;
	}
	
	$air_grease_row_id = mysqli_insert_id($conn);
	
	if($balance!=$closing)
	{
		$flag = false;
		errorLog("Balance amount not equal to closing amount. Balance: $balance,Closing: $closing.",$conn,$page_name,__LINE__);
	}
	
	$insert_carry_fwd= Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,balance,date,branch,timestamp) 
	VALUES ('$driver_code','$tno','$trip_no','HISAB-CARRY-FWD','$balance','$date','$branch','$timestamp')");

	if(!$insert_carry_fwd){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_pay_row_id = "0";
	$hisab_cfwd_row_id = mysqli_insert_id($conn);
	$hisab_credit_ho_row_id = "0";
	
$insert_hisab_log = InsertHisabLog($conn,$page_name,$tno,$trip_no,"0","1",$down_driver,$down_type,0,0,0,0,$driver_code,$branch,$_SESSION['user_code'],$date,$timestamp,$asset_row_id,$salary_row_id,$da_row_id,$telephone_row_id,$air_grease_row_id,$hisab_pay_row_id,$hisab_cfwd_row_id,$hisab_credit_ho_row_id);

if($insert_hisab_log==false){
	$flag = false;
}

$driver_down_qry = DriverDownSet("RUNNING",$conn,$page_name,$tno,$trip_no,$down_driver,$date,$down_type_db,$balance,$driver_code,$down_type,$down_reason,$branch,$timestamp);

if($driver_down_qry==false){
	$flag = false;
}

$delete_cache = Qry($conn,"DELETE FROM dairy.hisab_cache WHERE tno='$tno'");
if(!$delete_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	errorLog("COMMIT NOT COMPLETED. Timestamp : $timestamp. Truck No: $tno, TripNo: $trip_no.",$conn,$page_name,__LINE__);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}

echo "<form action='../hisab/hisab_view.php' id='myFormHisab' method='POST'>
			<input type='hidden' value='$trip_no' name='trip_no'>
			<input type='hidden' value='$tno' name='tno'>					
	</form>";
	
	echo "<script type='text/javascript'>
			function submitform2()
			{
			alert('Hisab Success.'); 
			document.getElementById('myFormHisab').submit();
			}
		submitform2();	
		</script>";
		// closeConnection($conn);
	exit();
?>