<?php
require_once("../connect.php");

$driver_code = escapeString($conn,strtoupper($_POST['driver_code']));
$page_url = escapeString($conn,($_POST['page_url']));
$tno = escapeString($conn,strtoupper($_SESSION['diary']));
$trip_no = escapeString($conn,strtoupper($_POST['trip_no']));
$timestamp = date("Y-m-d H:i:s");

if($driver_code=='' || $tno=='' || $page_url=='')
{
	Redirect("Unable to fetch Driver or Truck Number.","./logout.php");
	exit();
}

StartCommit($conn);
$flag = true;

foreach($_POST['asset_code'] as $key=>$value)
{
    $item_code = escapeString($conn,strtoupper($_POST['asset_code'][$key]));
    $item_sel = escapeString($conn,strtoupper($_POST['item_sel'][$key]));
    $item_rate = escapeString($conn,strtoupper($_POST['asset_rate'][$key]));
    $item_qty = escapeString($conn,strtoupper($_POST['item_qty'][$key]));
    $asset_qty_ext = escapeString($conn,strtoupper($_POST['asset_qty_ext'][$key]));
    $asset_rate_ext = escapeString($conn,strtoupper($_POST['asset_rate_ext'][$key]));
    $asset_qty_db = escapeString($conn,strtoupper($_POST['asset_qty_db'][$key]));
    $rate_naame = escapeString($conn,strtoupper($_POST['rate_naame'][$key]));
	
	if($item_qty==$asset_qty_ext)
	{
		$amount_naame=0;
	}
	else
	{
		$amount_naame=$asset_rate_ext-$item_rate;
		
		if($amount_naame=='' || $amount_naame==0 || $amount_naame<0)
		{
			$flag = false;
			errorLog("ASSET FORM AMOUNT NAAME ERROR. Driver Code : $driver_code, Truck No $tno.",$conn,$page_name,__LINE__);
		}
		
		if($rate_naame!=$amount_naame)
		{
			$flag = false;
			errorLog("ASSET FORM AMOUNT NAAME ERROR. Driver Code : $driver_code, Truck No $tno.",$conn,$page_name,__LINE__);
		}
	}
	
	$update = Qry($conn,"UPDATE dairy.asset_form SET item_value_down='$item_sel',quantity='$item_qty',amount='$item_rate',
	amount_naame='$amount_naame' WHERE driver_code='$driver_code' AND item_code='$item_code'");
	
	if(!$update)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_form_status = Qry($conn,"UPDATE dairy.driver_up SET asset_form_down='1' WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");
if(!$update_form_status)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
}
else
{
	MySQLRollBack($conn);
	errorLog("COMMIT NOT COMPLETED. Timestamp : $timestamp. Truck No: $tno, TripNo: $trip_no.",$conn,$page_name,__LINE__);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}

echo "<form action='./$page_url' id='myForm33' method='POST'>
		<input type='hidden' value='".sha1($trip_no.md5($tno))."' name='KEY'>	
		<input type='hidden' value='$tno' name='tno'>		
		<input type='hidden' value='$trip_no' name='trip_no'>		
	</form>";
		
	echo "<script type='text/javascript'>
		alert('ASSET FORM SUCCESSFULLY UPDATED.');		
		document.getElementById('myForm33').submit();
	</script>";
	closeConnection($conn);
	exit();

?>