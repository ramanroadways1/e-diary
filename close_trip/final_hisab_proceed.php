<?php
require_once("../connect.php");
include ("./_hisab_func.php");

if(!isset($_SESSION['final_hisab_type']))
{
	Redirect("Hisab type not found.","./");
	exit();
}

if($_SESSION['final_hisab_type']=="0")
{
	$final_hisab_type="Final";
	$hisab_type="0";
}
else if($_SESSION['final_hisab_type']=="1")
{
	$final_hisab_type="Loaded vehicle";
	$hisab_type="1";
}
else
{
	Redirect("Hisab type is not valid.","./");
	exit();
}

$check_running_trip = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name!='LOAD_API_TRANS'");

if(!$check_running_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($check_running_trip)>0)
{
	Redirect("Please wait ! Try again after some time..","./");
	exit();
}

$timestamp = date("Y-m-d H:i:s");
$tno=escapeString($conn,strtoupper($_SESSION['diary']));

$hisab_verify_salt_key = sha1(sha1($_SESSION['hisab_trip_no'].md5($_SESSION['diary']))."hisab_verify");

if(!isset($_POST['hisab_verify']))
{
	Redirect("Error while processing request.","./");
	exit();
}

if($_POST['hisab_verify']!=$hisab_verify_salt_key)
{
	Redirect("Hisab not verified.","./");
	exit();
}

$chk_for_hisab_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$chk_for_hisab_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($chk_for_hisab_cache)>0)
{
	$hisab_cache_found="YES";
}
else
{
	$hisab_cache_found="NO";
}

if(!isset($_SESSION['hisab_driver_code']))
{
	$dlt_cache = Qry($conn,"DELETE FROM dairy.hisab_cache WHERE tno='$tno'");

	if(!$dlt_cache){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
	
	echo "<script>
		alert('Session expired !');
		window.location.href='./';
	</script>";
	exit();
}

$driver_code = $_SESSION['hisab_driver_code'];
$driver_up_id = $_SESSION['hisab_driver_up_id'];

$get_driver_up_data = Qry($conn,"SELECT code,up,salary_amount,salary_type,amount_hold FROM dairy.driver_up WHERE id='$driver_up_id'");

if(!$get_driver_up_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_driver_up_data)==0)
{
	errorLog("Driver up data not found. Code: $driver_code",$conn,$page_name,__LINE__);
	echo "<script>
		alert('Driver-salary record not found !');
		window.location.href='./';
	</script>";
	exit();
}

$row_driver_up = fetchArray($get_driver_up_data);

$driver_salary_db = $row_driver_up['salary_amount'];
$driver_salary_type_db = $row_driver_up['salary_type'];
$opening_balance_db = $row_driver_up['amount_hold'];
$driver_up_date = $row_driver_up['up'];

if($hisab_cache_found=='NO')
{

$trishul_card_deduction = escapeString($conn,($_POST['trishul_card_deduction']));

$chk_for_bulker = Qry($conn,"SELECT is_crain,is_bulker,trishul_card,lock_ag,lock_tel,diesel_left_fix FROM dairy.own_truck WHERE tno='$tno'");
if(!$chk_for_bulker){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_bulker = fetchArray($chk_for_bulker);

$diesel_left_fix = $row_bulker['diesel_left_fix'];

if($row_bulker['trishul_card']=="1")
{
	if(!isset($_POST['is_trishul_rcvd']))
	{
		errorLog("Trishul card selection not found. Truck_no: $tno",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Error while processing request!!');
			window.location.href='./';
		</script>";
		exit();
	}
	
	if($_POST['is_trishul_rcvd']=="YES")
	{
		if($trishul_card_deduction!=0)
		{
			errorLog("Trishul card amount is not valid. Truck_no: $tno",$conn,$page_name,__LINE__);
			echo "<script>
				alert('Error while processing request!!');
				window.location.href='./';
			</script>";
			exit();
		}
	}
	else 
	{
		if($trishul_card_deduction!=1000)
		{
			errorLog("Trishul card amount is not valid. Truck_no: $tno",$conn,$page_name,__LINE__);
			echo "<script>
				alert('Error while processing request!!');
				window.location.href='./';
			</script>";
			exit();
		}
	}
}

if($_SESSION['amount_hold_before_hisab'] != $opening_balance_db)
{
	errorLog("Driver balance not verified.",$conn,$page_name,__LINE__);
	Redirect("Driver balance not verified.","./");
	exit();
}

$get_driver_data = Qry($conn,"SELECT id,name,dummy_driver,last_salary FROM dairy.driver WHERE code='$driver_code'");

if(!$get_driver_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_driver_data)==0)
{
	errorLog("Driver not found. Code: $driver_code",$conn,$page_name,__LINE__);
	echo "<script>
		alert('Driver not found !');
		window.location.href='./';
	</script>";
	exit();
}

$row_driver = fetchArray($get_driver_data);

$is_dummy_driver = $row_driver['dummy_driver'];
$driver_name = $row_driver['name'];
$driver_id = $row_driver['id'];

$down_driver = escapeString($conn,$_POST['down_driver_selection']); // Driver down YES/NO
$down_type = escapeString($conn,$_POST['down_driver_type']); // Driver Down Type. on leave etc..
$left_reason = escapeString($conn,($_POST['reason_left']));
$standby_reason = escapeString($conn,($_POST['standby_reason']));
$standby_reason_other = escapeString($conn,($_POST['reason_other']));
$gps_km = escapeString($conn,$_POST['gps_km']);
$hisab_date_1 = escapeString($conn,$_POST['hisab_date_1']);
$hisab_location_1 = escapeString($conn,$_POST['hisab_location_1']);
$hisab_loc_pincode = escapeString($conn,$_POST['hisab_location_pincode_new']);

$hisab_location_1 = explode("_",$hisab_location_1)[0]."_".explode("_",$hisab_location_1)[1]."_".$hisab_loc_pincode;

if($is_dummy_driver=="1")
{
	if($_POST['da']!=0)
	{
		errorLog("DA amount should be zero. Driver_code: $driver_code",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
	
	if($_POST['tel']!=0)
	{
		errorLog("Telephone amount should be zero. Driver_code: $driver_code",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
	
	if($_POST['ag']!=0)
	{
		errorLog("Air-grease amount should be zero. Driver_code: $driver_code",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
	
	if($_POST['salary']!=0)
	{
		errorLog("Salary amount should be zero. Driver_code: $driver_code",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
	
	if($_POST['give_salary']!="0")
	{
		errorLog("Salary selection should be NO. Driver_code: $driver_code",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}

$da = escapeString($conn,$_POST['da']);
$tel = escapeString($conn,$_POST['tel']);
$ag = escapeString($conn,$_POST['ag']);
$give_salary = escapeString($conn,$_POST['give_salary']);
$salary_amount = escapeString($conn,$_POST['salary']);
$salary_from = escapeString($conn,strtoupper($_POST['sal_from_date']));
$salary_to = escapeString($conn,strtoupper($_POST['sal_to_date']));
$salary_pattern = escapeString($conn,strtoupper($_POST['salary_pattern']));

if($driver_salary_type_db=="1") // trip wise salary 
{
	if($give_salary=="1")
	{
		if($salary_amount != round($driver_salary_db*$_SESSION['hisab_non_empty_trip_count']))
		{
			errorLog("Salary amount not verified. Driver_code: $driver_code. Salary amt posted: $salary_amount. Per-trip salary is: $driver_salary_db and No of trip days is: $_SESSION[hisab_non_empty_trip_count].",$conn,$page_name,__LINE__);
			Redirect("Salary amount not verified.","./");
			exit();
		}
		
		if($salary_from != $_SESSION['hisab_trip_start_date'] || $salary_to != $_SESSION['hisab_trip_end_date'])
		{
			errorLog("Salary dates not verified. Driver_code: $driver_code. Salary from and to date posted: $salary_from to $salary_to. Session set is: $_SESSION[hisab_trip_start_date] to $_SESSION[hisab_trip_end_date].",$conn,$page_name,__LINE__);
			Redirect("Salary dates not verified.","./");
			exit();
		}
	}
	else
	{
		if($salary_amount != 0)
		{
			errorLog("Salary amount not verified. Driver_code: $driver_code. Salary amount must be zero.",$conn,$page_name,__LINE__);
			Redirect("Salary amount not verified.","./");
			exit();
		}
		
		$salary_from = "";
		$salary_to = "";
	}
}
else  // month wise salary 
{
	if($give_salary=="1")
	{
		if($salary_amount != $_SESSION['hisab_salary_amount'])
		{
			errorLog("Salary amount not verified. Driver_code: $driver_code. Salary amt posted: $salary_amount. Session set is: $_SESSION[hisab_salary_amount].",$conn,$page_name,__LINE__);
			Redirect("Salary amount not verified.","./");
			exit();
		}
		
		if($salary_from != $_SESSION['hisab_salary_from_date'] || $salary_to != $_SESSION['hisab_salary_to_date'])
		{
			errorLog("Salary dates not verified. Driver_code: $driver_code. Salary from and to date posted: $salary_from to $salary_to. Session set is: $_SESSION[hisab_salary_from_date] to $_SESSION[hisab_salary_to_date].",$conn,$page_name,__LINE__);
			Redirect("Salary dates not verified.","./");
			exit();
		}
	}
	else
	{
		if($salary_amount != 0)
		{
			errorLog("Salary amount not verified. Driver_code: $driver_code. Salary amount must be zero.",$conn,$page_name,__LINE__);
			Redirect("Salary amount not verified.","./");
			exit();
		}
		
		$salary_from = "";
		$salary_to = "";
	}
}

$salary_amt_hidden_ip = escapeString($conn,$_POST['salary_amount_cache']);

if($salary_amount != $salary_amt_hidden_ip)
{
	errorLog("Problem with salary amount. Hidden input value is : $salary_amt_hidden_ip and posted is : $salary_amount.",$conn,$page_name,__LINE__);
	Redirect("Salary amount not verified.","./");
	exit();
}

$da_from = escapeString($conn,($_POST['da_from_date']));
$da_to = escapeString($conn,($_POST['da_to_date']));
$da_amt_per_day = escapeString($conn,($_POST['da_amt']));

$diff_da_days = (strtotime($da_to)- strtotime($da_from))/24/3600; 
$diff_da_days = $diff_da_days+1;

$diff_days_salary = (strtotime($salary_to)- strtotime($salary_from))/24/3600; 
$diff_days_salary = $diff_days_salary+1;
$driver_balance_db = $opening_balance_db;

if($da>0 AND $da != ($diff_da_days*$da_amt_per_day))
{
	$system_da = $diff_da_days*$da_amt_per_day;
	errorLog("DA amount not verified. DA amount : $da. System Amount : $system_da.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$min_da_date = getLastDaDates($conn,$driver_code,$driver_up_date);

if(empty($min_da_date)){
	errorLog("DA min date not found: $min_da_date.",$conn,$page_name,__LINE__);
	Redirect("DA date not found.","./");
	exit();
}

if($da_from < $min_da_date){
	errorLog("Invalid DA from-date: $da_from. Min-date is: $min_da_date.",$conn,$page_name,__LINE__);
	Redirect("DA from date is not valid.","./");
	exit();
}

if($da_to > date("Y-m-d")){
	errorLog("Invalid DA to-date: $da_to. Max-date is: '".date("Y-m-d")."'.",$conn,$page_name,__LINE__);
	Redirect("DA to date is not valid.","./");
	exit();
}

$from_station = escapeString($conn,strtoupper($_POST['from_station']));
$to_station = escapeString($conn,strtoupper($_POST['to_station']));
$diff_days = $_SESSION['hisab_trip_days']; // Trip Days Total

$trip_km = $_SESSION['hisab_trip_km'];
$closing_amount_system = escapeString($conn,($_POST['closing']));

$diff_da_pay = 0;
// $diff_da_pay = escapeString($conn,$_POST['diff_da_pay']);

if($down_driver==1)
{		
	$qry_asset_naame = Qry($conn,"SELECT SUM(amount_naame) as asset_amount FROM dairy.asset_form 
	WHERE driver_code='$driver_code'");
	
	if(!$qry_asset_naame){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
	
	$row_asset = fetchArray($qry_asset_naame);
	$asset_amount = $row_asset['asset_amount'];
}
else
{
	$asset_amount=0;	
}

if($down_type=='LEFT' || $down_type=='LEFT_ROUTE')
{
	if(empty($left_reason)){
		errorLog("Unable to fetch Driver Left Reason.",$conn,$page_name,__LINE__);
		Redirect("Driver left-reason not found.","./");
		exit();
	}	
}
else if($down_type=='STAND_BY')
{
	if(empty($standby_reason))
	{
		errorLog("Unable to fetch Driver STAND_BY Reason.",$conn,$page_name,__LINE__);
		Redirect("Driver stand-by reason not found.","./");
		exit();
	}
	else if($standby_reason=='OTHER')
	{
		if(empty($standby_reason_other))
		{
			errorLog("Unable to fetch Driver STAND_BY Other Reason.",$conn,$page_name,__LINE__);
			Redirect("Driver stand-by-others reason not found.","./");
			exit();
		}	
	}
}

$chk_diesel_qty = Qry($conn,"SELECT SUM(qty) as sum_qty FROM dairy.diesel WHERE trip_id IN(SELECT id FROM dairy.trip WHERE tno='$tno')");

if(!$chk_diesel_qty){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_diesel_db = fetchArray($chk_diesel_qty);

$diesel_qty_db = sprintf('%0.2f',$row_diesel_db['sum_qty']);
$hisab_diesel_qty = $_SESSION['hisab_diesel_qty'];

if($diesel_qty_db != $hisab_diesel_qty)
{
	errorLog("Diesel QTY not verified. DB is: $diesel_qty_db. By session is: $hisab_diesel_qty.",$conn,$page_name,__LINE__);
	Redirect("Diesel Qty. not verified.","./");
	exit();
}

$start_date_db = $_SESSION['hisab_trip_start_date'];
$end_date_db = $_SESSION['hisab_trip_end_date'];

if($end_date_db==0 || empty($end_date_db))
{
	$end_date_db = date("Y-m-d");
}

$trip_no = $_SESSION['hisab_trip_no'];

if($trip_no==0 || $trip_no=='' || empty($trip_no))
{
	errorLog("trip_no not found: $trip_no.",$conn,$page_name,__LINE__);
	Redirect("Trip not found.","./");
	exit();
}

$diff_trip_days_db = (strtotime($end_date_db)- strtotime($start_date_db))/24/3600; 
$diff_trip_days_db=$diff_trip_days_db+1;

$chk_standby_approval = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no' AND stand_by_approval='1'");

if(!$chk_standby_approval){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($chk_standby_approval)==0)
{
	if($down_driver=="1" AND $down_type=='STAND_BY')
	{
		Redirect("Driver stand-by approval pending.","./");
		exit();
	}
}

$driver_last_salary=$row_driver['last_salary'];
		
if($is_dummy_driver=="1")
{
	$give_salary="0";
	$da=0;
	$tel=0;
	$ag=0;
	$da_amt_per_day=0;
	$diff_da_days=0;
	$da_from="";
	$da_to="";
}
	
if($row_bulker['lock_ag']=='1')
{
	if($ag!=0)
	{
		errorLog("Air-grease amount not verified. Amount is: $ag. Should Be : 0.",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}
else
{
	if(($ag != $diff_da_days*10) AND $is_dummy_driver!="1" AND $row_bulker['is_crain']!='1')
	{
		$system_tel_ag = $diff_da_days*10;
		errorLog("Air-grease amount not verified. Amount is: $ag. Should be: $system_tel_ag.",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}
	
if($row_bulker['lock_tel']=='1')
{
	if($tel!=0)
	{
		errorLog("telephone amount not verified. Amount is: $tel. Should Be: 0.",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}
else
{
	if(($tel!=$diff_da_days*10) AND $is_dummy_driver!="1" AND $row_bulker['is_crain']!='1')
	{
		$system_tel_ag = $diff_da_days*10;
		errorLog("Telephone exp amount not verified. Amount is: $tel. Should be: $system_tel_ag.",$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
}
	
if($give_salary=='1')
{
	if($driver_salary_type_db!='1') // if salary is not trip wise
	{
		$salary_min_date = getLastSalaryDates($conn,$driver_code,$driver_up_date,$driver_last_salary);
			
		if(empty($salary_min_date)){
			errorLog("SALARY min date not found: $salary_min_date.",$conn,$page_name,__LINE__);
			Redirect("SALARY date not found.","./");
			exit();
		}

		if($salary_from < $salary_min_date){
			errorLog("Invalid SALARY from-date: $salary_from. Min-date is: $salary_min_date.",$conn,$page_name,__LINE__);
			Redirect("SALARY from date is not valid.","./");
			exit();
		}

		if($salary_to > date("Y-m-d")){
			errorLog("Invalid SALARY to-date: $salary_to. Max-date is: '".date("Y-m-d")."'.",$conn,$page_name,__LINE__);
			Redirect("SALARY to date is not valid.","./");
			exit();
		}
	}
}
else
{
	$salary_amount="0";
	$salary_from="";
	$salary_to="";
}

if($closing_amount_system != ($driver_balance_db+$trishul_card_deduction+$asset_amount-($da+$salary_amount+$tel+$ag)))
{
	$sys_cal_closing = ($driver_balance_db+$trishul_card_deduction+$asset_amount)-($da+$salary_amount+$tel+$ag);
	errorLog("Closing amount not verified. Html input is: $closing_amount_system and calucated by sys is: $sys_cal_closing.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if($row_bulker['is_bulker']=="1" AND $give_salary=='1')
{
	$sal_cut_off = escapeString($conn,$_POST['salary_cuttoff']);
	
	if(empty($sal_cut_off)){
		errorLog("Salary deduction is not valid. Truck_no : $tno.",$conn,$page_name,__LINE__);
		Redirect("Salary deduction is not valid.","./");
		exit();
	}
	else if($sal_cut_off=='NO')
	{
		$sal_cut_off = "0";
	}
	else
	{
		$sal_cut_off = $sal_cut_off;
	}
}
else
{
	$sal_cut_off = "0";
}

$hisab_cache = Qry($conn,"INSERT INTO dairy.hisab_cache(final_running,branch,tno,trip_no,lock_ag,lock_tel,trishul_card,driver_name,
driver_code,down_driver,down_type,left_reason,standby_reason,standby_reason_other,give_salary,sal_per_month,sal_cut_off,da_amt_per_day,
from_station,to_station,balance_on_hisab,da_amount,da_days,da_from,da_to,salary_amount,salary_pattern,sal_from,sal_to,trip_start,
trip_end,trip_days,tel_amount,ag_amount,closing,asset_form_naame,diesel_qty,fix_diesel_value,diesel_balance,sys_km,gps_km,hisab_date,hisab_location,
hisab_loc_pincode,timestamp) VALUES ('$hisab_type','$branch','$tno','$trip_no','$row_bulker[lock_ag]','$row_bulker[lock_tel]','$trishul_card_deduction',
'$driver_name','$driver_code','$down_driver','$down_type','$left_reason','$standby_reason','$standby_reason_other','$give_salary','',
'$sal_cut_off','$da_amt_per_day','$from_station','$to_station','$driver_balance_db','$da','$diff_da_days','$da_from','$da_to','$salary_amount',
'$salary_pattern','$salary_from','$salary_to','$start_date_db','$end_date_db','$diff_days','$tel','$ag','$closing_amount_system','$asset_amount',
'$diesel_qty_db','$_SESSION[hisab_fix_diesel_value]','$diesel_left_fix','$trip_km','$gps_km','$hisab_date_1','$hisab_location_1','$hisab_loc_pincode',
'$timestamp')");

if(!$hisab_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

}

$get_cache_data = Qry($conn,"SELECT trip_no,lock_ag,lock_tel,trishul_card,driver_name,driver_code,down_driver,down_type,sal_cut_off,
from_station,to_station,balance_on_hisab,da_amount,da_from,da_to,salary_amount,salary_pattern,sal_from,sal_to,trip_start,trip_end,
trip_days,tel_amount,ag_amount,closing,asset_form_naame,diesel_qty,fix_diesel_value,diesel_balance,sys_km,gps_km,hisab_date,hisab_location,hisab_loc_pincode 
FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$get_cache_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_get_cache = fetchArray($get_cache_data);

if($row_get_cache['lock_ag']=='1' AND $row_get_cache['ag_amount']!=0)
{
	errorLog("Air grease amount must be zero.",$conn,$page_name,__LINE__);
	echo "<script>
			alert('Error While Processing Request !!!');
			window.location.href='./';
		</script>";
	exit();
}

if($row_get_cache['lock_tel']=='1' AND $row_get_cache['tel_amount']!=0)
{
	errorLog("Tel grease amount must be zero.",$conn,$page_name,__LINE__);
	echo "<script>
			alert('Error While Processing Request !!!');
			window.location.href='./';
		</script>";
	exit();
}

$trishul_card_deduction = $row_get_cache['trishul_card'];
$trip_no = $row_get_cache['trip_no'];
$driver_name = $row_get_cache['driver_name'];
$driver_code = $row_get_cache['driver_code'];
$down_driver = $row_get_cache['down_driver'];
$down_type = $row_get_cache['down_type'];
$from_station = $row_get_cache['from_station'];
$to_station = $row_get_cache['to_station'];
$balance_on_hisab = $row_get_cache['balance_on_hisab']; // opening balance
$da_amount = $row_get_cache['da_amount'];
$da_from = $row_get_cache['da_from'];
$da_to = $row_get_cache['da_to'];
$salary_amount = $row_get_cache['salary_amount'];
$salary_pattern = $row_get_cache['salary_pattern'];
$sal_from = $row_get_cache['sal_from'];
$sal_to = $row_get_cache['sal_to'];
$trip_start = $row_get_cache['trip_start'];
$trip_end = $row_get_cache['trip_end'];
$trip_days = $row_get_cache['trip_days'];
$tel_amount = $row_get_cache['tel_amount'];
$ag_amount = $row_get_cache['ag_amount'];
$asset_form_naame = $row_get_cache['asset_form_naame'];
$closing_db = $row_get_cache['closing'];
$diesel_qty = $row_get_cache['diesel_qty'];
$fix_diesel_value = $row_get_cache['fix_diesel_value'];
$diesel_balance = $row_get_cache['diesel_balance'];
$sys_km = $row_get_cache['sys_km'];
$gps_km = $row_get_cache['gps_km'];
$hisab_date_1 = $row_get_cache['hisab_date'];
$hisab_location_1 = $row_get_cache['hisab_location'];
$sal_cut_off_db = $row_get_cache['sal_cut_off']; // salary deduction in bulkers

$dr_var = $da_amount+$tel_amount+$ag_amount+$salary_amount;
$closing_amount = ($balance_on_hisab-$dr_var)+$asset_form_naame+$trishul_card_deduction;

if($closing_amount!=$closing_db)
{
	errorLog("Closing amount not verified. Trishul: $trishul_card_deduction. Sys : $closing_amount. Db : $closing_db.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$get_opening = Qry($conn,"SELECT amount_hold FROM dairy.driver_up WHERE id='$driver_up_id'");

if(!$get_opening){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_opening)==0)
{
	errorLog("Driver balance not found. Driver Code: $driver_code.",$conn,$page_name,__LINE__);
	echo "<script>
			alert('Driver not found !!!');
			window.location.href='./';
		</script>";
	exit();
}

$row_opening = fetchArray($get_opening);

if($row_opening['amount_hold']!=$balance_on_hisab)
{
	// $delete_cache = Qry($conn,"DELETE FROM dairy.hisab_cache WHERE tno='$tno'");
	
	// if(!$delete_cache){
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// echo "<script>
			// alert('Error While Processing Request !!!');
			// window.close();
		// </script>";
		// exit();
	// }
// echo $row_opening['amount_hold']." & ".$balance_on_hisab;
	
	echo "<script>
		alert('Driver balance not verified !');
		window.location.href='./';
	</script>";
	exit();
}

include("../_header.php");
include("../code_disabled_right_click.php");
?>
<style>
label{font-size:12.5px;}
</style>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div id="result_main"></div>

<script type="text/javascript">
function CreditToCashbook()
{
	Swal.fire({
	  title: "Are you sure ?",
	  html: "<span style='font-size:13px;color:maroon'>आप बकाया आपके केशबूक में जमा करना चाहते है ?</span>",
	  icon: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes Confirm",
	  closeOnConfirm: false
	}).then((result) => {
		if(result.isConfirmed) {
			$('#credit_to_cash').attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
			url: "final_credit_to_cash.php",
			data: 'ok=' + 'ok',
			type: "POST",
			success: function(data) {
				$("#result_data").html(data);
			},
			error: function() {}
			});
		}
	})
}
</script>

<style>
	.form-control{
		border:1px solid #000;
		text-transform:uppercase;
	}
</style>

<div class="container-fluid">

<div class="row">
	<div class="form-group col-md-12" style="box-shadow:0 0 50px #ddd inset;color:#000;padding:8px;">
		<div class="col-md-4">
			<b><span class="fa fa-truck"></span> &nbsp; <span style="letter-spacing:.5px;<?php if(isMobile()){ echo "font-size:13px"; } else { echo "font-size:16px"; } ?>"><?php echo $tno; ?></span></b>
			<br>
			<b><span style="<?php if(isMobile()){ echo "font-size:12px"; }?>"><span class="fa fa-user-o"></span> &nbsp;  <?php echo $driver_name; ?>.</b></span>
		</div>	
		
		<div class="col-md-4">
			<center><span style="<?php if(isMobile()){ echo "font-size:16px"; } else { echo "font-size:18px"; } ?>"><?php echo $final_hisab_type; ?> - Hisab (TripNo: <?php echo $trip_no; ?>)</span> 
			<br/>
			<span style="<?php if(isMobile()){ echo "font-size:13px"; } else { echo "font-size:14px"; } ?>"><?php echo $from_station." to ".$to_station." (".$trip_days." days)"; ?></span>
			</center>
		</div>
			
		<div class="col-md-4" style="<?php if(!isMobile()){ echo "text-align:right"; } ?>;color:maroon">
			<span style="text-align:right;<?php if(isMobile()){ echo "font-size:12px"; } else { echo "font-size:16px"; } ?>">Sys km : <?php echo $sys_km; ?></span> 
			,&nbsp;<span style="<?php if(!isMobile()){ echo "text-align:right;font-size:16px;"; } else { echo "font-size:12px"; } ?>">GPS km : <?php echo $gps_km; ?></span> 
			<?php if(!isMobile()){ echo "<br />"; } ?>
			<span style="<?php if(!isMobile()){ echo "text-align:right;font-size:16px;"; } else { echo "font-size:12px"; } ?>"><?php if(!isMobile()){ echo "Average"; } else { echo ", Avg"; } ?>: <?php if($diesel_qty>0) { echo "<b>".sprintf("%.2f",$gps_km/$diesel_qty)." kmpl</b>"; } else { echo "<b>NO_DIESEL</b>"; } ?></span> 
		</div>
	</div>	
</div>

<script>
function ClearCache(tno)
{
	Swal.fire({
	  title: "Are you sure ?",
	  html: "<font color='maroon'>Do you really want clear hisab data !</font>",
	  icon: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes Confirm",
	  closeOnConfirm: false
	}).then((result) => {
		if(result.isConfirmed) {
			$("#loadicon").show();
			jQuery.ajax({
			url: "clear_hisab_cache.php",
			data: 'tno=' + tno,
			type: "POST",
			success: function(data) {
				$("#result_data").html(data);
			},
			error: function() {}
			});
		}
	})
}

function HisabCarryFwd()
{
	Swal.fire({
	  title: "Are you sure ?",
	  html: "<span style='font-size:13px;color:maroon'>आप ड्राइवर का बकाया आगे बढ़ाना चाहते है ?</span>",
	  icon: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes Confirm",
	  closeOnConfirm: false
	}).then((result) => {
		if(result.isConfirmed) {
			$('#carry_forward_btn').attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
			url: "final_carry_fwd_hisab.php",
			data: 'ok=' + 'ok',
			type: "POST",
			success: function(data) {
				$("#result_data").html(data);
			},
			error: function() {}
			});
		}
	})
}
</script>

<div id="result_data"></div>

<div class="row">
	<div class="form-group col-md-12">
		<a href="../"><button class="btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-primary" type="button"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>&nbsp; Go back</button></a>
		<button style="margin-left:10px;" onclick="ClearCache('<?php echo $tno; ?>')" class="btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-danger" type="button"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp; Clear hisab cache</button>
	</div>	
</div>

<div class="row">
	<div class="form-group col-md-6">
	
<?php
include("modal_asset_form_view.php");

$qry_fetch_ac = Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$driver_code'");
			
if(!$qry_fetch_ac){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($qry_fetch_ac)>0)
{
	$row_fetch_ac = fetchArray($qry_fetch_ac);
	
	$ac_holder=$row_fetch_ac['acname'];
	$ac_no=$row_fetch_ac['acno'];
	$ac_bank=$row_fetch_ac['bank'];
	$ac_ifsc=$row_fetch_ac['ifsc'];
}
?>
	<?php
	if($fix_diesel_value>0)
	{
	?>
	<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success" style="font-size:13px;font-weight:bold;">
			Trip Fix Diesel: <?php echo $fix_diesel_value; ?> LTR, Diesel Given: <?php echo $diesel_qty; ?> LTR, Diesel Balance: <?php echo $diesel_balance; ?> LTR
		</div>	
	</div>
	</div>
	<?php	
	}
	?>
	
	<h5 style="background-color:#666;color:#FFF;padding:6px;<?php if(isMobile()){ echo "font-size:12px"; } ?>">Driver Account Details :</h5>
	<table class="table table-bordered table-striped" <?php if(isMobile()){ echo "style='font-size:10px'"; }?>>
			<tr>
				<th>
					A/c Holder : <?php echo $ac_holder; ?>
				</th>
				<th>				
					A/c No : <?php echo $ac_no; ?>
				</th>
			</tr>	
			<tr>	
				<th>				
					Bank : <?php echo $ac_bank; ?>
				</th>
				<th>	
					IFSC : <?php echo $ac_ifsc; ?>
				</th>
			</tr>
		</table>

<h5 style="background-color:#666;color:#FFF;padding:6px;<?php if(isMobile()){ echo "font-size:12px"; } ?>">Driver's Outstanding : &nbsp; <span style="<?php if(isMobile()){ echo "font-size:13px"; } else { echo "font-size:16px"; } ?>;letter-spacing:.5px;">
<?php echo $balance_on_hisab."/-"; ?></span></h5>
	<table class="table table-bordered table-striped" <?php if(isMobile()){ echo "style='font-size:10px'"; } else { echo "style='font-size:14px'"; }?>>
			<tr>
				<th>DA : <?php if($da_amount>0){ echo "<span>".date("d-m-y", strtotime($da_from))." to ".date("d-m-y", strtotime($da_to))."</span>"; } else { echo "<font color='maroon'>NOT GIVEN</font>"; } ?></th>
				<th>₹ <?php echo $da_amount."/-"; ?></th>
			</tr>
			<tr>
				<th>ASSET SHORTAGE NAAME : <a style="cursor:pointer;font-size:13px;" data-toggle='modal' data-target='#AssetFormViewModal'>View Asset Form</a></th>
				<th><?php echo "₹ $asset_form_naame /-"; ?></th>
			</tr>
			<?php
			if($trishul_card_deduction>0)
			{
			?>
			<tr>
				<th>TRISHUL CARD NAAME :</th>
				<th>₹ <?php echo $trishul_card_deduction."/-"; ?></th>
			</tr>				
			<?php
			}
			?>
			<tr>
				<th>TELEPHONE :</th>
				<th>₹ <?php echo $tel_amount."/-"; ?></th>
			</tr>				
			<tr>
				<th>AIR & GREASE :</th>
				<th>₹ <?php echo $ag_amount."/-"; ?></th>
			</tr>	
			<tr>
				<th>SALARY : <?php if($salary_amount>0){echo date("d-m-y", strtotime($sal_from))." to ".date("d-m-y", strtotime($sal_to)); if($sal_cut_off_db>0) { echo " : <font color='red'> $sal_cut_off_db% Deduction</font>"; } } else { echo "<font color='maroon'>NOT GIVEN</font>"; }?></th>
				<th>₹ <?php echo $salary_amount."/-"; ?></th>
			</tr>
			<tr class="success">
				<th>CLOSING AMOUNT :</th>
				<th>₹ <?php echo $closing_db."/-"; ?></th>
			</tr>
		</table>		
		
	<div class="row">
		<div class="form-group col-xs-3 col-md-3" id="carry_forward_button_div" style="display:none1">
			<button type="button" onclick="HisabCarryFwd()" id="carry_forward_btn" name="carry_forward" class="hisab_btns btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-danger">Carry Forward</button>
		</div>
		
		<div class="form-group col-xs-3 col-md-3" id="pay_button_div" style="display:none1">
			<button type="button" onclick="PayHisabClick();" id="pay_hisab_btn" name="pay_hisab" class="hisab_btns btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-primary">Pay Hisab</button>
		</div>
		
		<div class="form-group col-xs-3 col-md-3" id="both_button_div" style="display:none1">
			<button type="button" onclick="CarryFwDandPay();" id="pay_cfwd_btn" name="both_button" class="hisab_btns btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-warning"><?php if(isMobile()){ echo "Cfwd + Pay"; } else { echo "Carry Fwd + Pay"; } ?></button>
		</div>
		
		<div class="form-group col-xs-3 col-md-3" id="credit_amount_button_div" style="display:none1">
			<button type="button" onclick="CreditCash();" id="credit_amount_btn" class="hisab_btns btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-success">Credit to CashBook</button>
		</div>
		
	</div>

</div>
	
		<div class="form-group col-md-6" id="payment_methods"></div>
	<br />
	<br />
	</div>
</div> 
	
<script>
function LoadHisab()
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "hisab_payment_methods.php",
	data: 'okkk=' + 'okkk',
	type: "POST",
	success: function(data) {
		$("#payment_methods").html(data);
	},
		error: function() {}
	});
}
LoadHisab();
</script>

<form action="./final_hisab_proceed.php" id="myForm3" method="POST">
	<input type="hidden" name="hisab_verify" value="<?php echo $hisab_verify_salt_key; ?>">
	<input type='hidden' name='hisab_cache' value='OK'>
</form>
		
<script>
function RefreshProceed()
{
	$('#myForm3')[0].submit();
}
</script>

<?php include("../footer.php"); ?>