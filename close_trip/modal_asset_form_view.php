<div class="modal fade print_div" id="AssetFormViewModal" role="dialog" data-backdrop="static" data-keyboard="false" 
	style="left: 0;top: 0;background-image:url(truck_bg.jpg); background-repeat: no-repeat;  position: fixed;width: 100%;height: 100%; 
	background-size: cover;">
	
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
	  
		<div class="modal-header bg-primary">
			<span style="font-size:13px">Asset Form : <?php echo $tno; ?></span>
			<button type="button" data-dismiss="modal" class="btn btn-default pull-right btn-sm"><span class="fa fa-window-close"></span>&nbsp; Close</button>
		</div>
		
		<div class="modal-header">
			<span style="font-size:13px">Driver Name: <?php echo $driver_name; ?></span>
		</div>
		
<div class="modal-body table-responsive">

<table <?php if(isMobile()){ echo "style='font-size:11px'"; } else { echo "style='font-size:13px'"; } ?> class="table table-striped table-bordered">
		<tr>
			<th>सामान</th>
			<th>मात्रा सामान (देते समय )</th>
			<th>मात्रा सामान (जमा करते समय )</th>
			<th>रकम नामे </th>
		</tr>
	
	<?php
	mysqli_set_charset($conn, 'utf8');
	
	$qry_asset_item_view = Qry($conn,"SELECT a.item_qty,a.quantity,a.amount_naame,i.item_name 
	FROM dairy.asset_form as a 
	LEFT OUTER JOIN dairy.asset_form_items AS i ON i.code=a.item_code 
	WHERE a.tno='$tno' AND a.item_value_up='1'");
	
	if(!$qry_asset_item_view){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>window.location.href='../';</script>";
		exit();
	}

	if(numRows($qry_asset_item_view)>0)
		{
			while($row_asset_view = fetchArray($qry_asset_item_view))
			{
				echo "
				<tr>
					<td> 
						$row_asset_view[item_name]
					</td>
					
					<td> 
						$row_asset_view[item_qty]
					</td>
					
					<td> 
						$row_asset_view[quantity]
					</td>
				
					<td> 
						₹ $row_asset_view[amount_naame]
					</td>
					
 				</tr>";
			}
		}

		$qry_sum_asset = Qry($conn,"SELECT sum(amount_naame) as total_naame FROM dairy.asset_form WHERE tno='$tno'");
		
		if(!$qry_sum_asset){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>window.location.href='../';</script>";
			exit();
		}
	
		$row_amount_naame = fetchArray($qry_sum_asset);
?>
		<tr>
			<td colspan="3"> 
				<b>Total Amount : </b>
			</td>
			<td> ₹ <?php echo $row_amount_naame['total_naame']; ?>
			</td>
		</tr>
<?php

	?>
</table>

<!--
<center>
	<form action='../print/asset_form.php' target="_blank" id='PrintAssetForm1' method='POST'>
		<input type="hidden" name="tno" value="<?php echo $tno; ?>">
		<button type="submit" disabled class="btn btn-danger btn-lg">Print Asset Form</button>
	</form>
</center>
-->
</div>
    </div>
  </div>
</div>