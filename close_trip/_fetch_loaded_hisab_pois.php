<?php
require_once '../connect.php'; 

$timestamp = date("Y-m-d H:i:s");
$date = date("d-m-Y",strtotime(escapeString($conn,($_POST['date']))));
$veh_no = escapeString($conn,$_SESSION['diary']);
$start_date = $date."T00:00:01";
$end_date = $date."T23:59:59";
$Duration1= "5"; // in minutes

echo "<option style='font-size:12px !important' value=''>--hisab location/time--</option>";
echo "<option style='font-size:12px !important' value='22.908195584885878_72.48545306877904_382210'>Visalpur Branch</option>";

$curl = curl_init();
curl_setopt_array($curl, array(
	CURLOPT_URL => "https://apps2.locanix.net/RuningHourReport/api/Stoppages?VehicleName=$veh_no&FromDateTime=$start_date&ToDateTime=$end_date&Duration=$Duration1",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 900,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		'Content-Type: text/plain'
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
		
if($err)
{
	AlertError("Connection failed with Locanix !");
	echo "<script>$('#hisab_location_1').val('');</script>";
	exit();
}
		
$response2 = json_decode($response, true);

if($response=='null' || $response=='')
{
}
else
{
	// $stop_array=array();
	if($response2['Stoppages']!='')
	{
		foreach($response2['Stoppages'] as $Data1)
		{
			$from_time = str_replace("+05:30","",str_replace("T"," ",$Data1['From']));
			$to_time = str_replace("+05:30","",str_replace("T"," ",$Data1['To']));
			$duration = $Data1['Duration'];
			$address = $Data1['Address']; 
			
			$zipcode = preg_match("/\b\d{6}\b/", $address, $matches);
			
			if(empty($matches[0])){
				$pincode = "";
				$pincode1 = ""; 
			}else {
				$pincode = ": ".$matches[0];
				$pincode1 = $matches[0];
			}
			
			$pincode1 = str_replace(" ","",$pincode1);
			$pincode1 = str_replace(".","",$pincode1);
			
			$lat = $Data1['Latitude'];
			$long = $Data1['Longitude'];
			$lat_long_pincode = $lat."_".$long."_".$pincode1; 
			$from_time_new = date("h:i A",strtotime($from_time));
			$to_time_new = date("h:i A",strtotime($to_time));
			// echo $from_time." to ".$to_time."<br>".$duration."<br>".$address."<br>".$lat."<br>".$long."<br>";
			echo "<option style='font-size:12px !important' value='$lat_long_pincode'>$address $pincode- $from_time_new to $to_time_new</option>";
		}
	}
}

echo "<script>$('#loadicon').fadeOut('slow');</script>";
?>