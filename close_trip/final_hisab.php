<?php
require_once("../connect.php");

$timestamp = date("Y-m-d H:i:s");

// $_SESSION['hisab_trip_no']="";

$tno = escapeString($conn,$_POST['tno']);
$trip_no = escapeString($conn,$_POST['trip_no']);
$type = escapeString($conn,$_POST['type']);
	 
$chk_if_coal = Qry($conn,"SELECT id FROM user WHERE username='$_SESSION[user]' AND z='1'");

if(!$chk_if_coal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_if_coal)>0)
{
	AlertRightCornerErrorFinalHisab("Coal branch can\'t take hisab.");
	exit();
}

$chk_trishul_card = Qry($conn,"SELECT trishul_card,trishul_mapped FROM dairy.own_truck WHERE tno='$tno'");

if(!$chk_trishul_card){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_trishul_card = fetchArray($chk_trishul_card);

if($row_trishul_card['trishul_card']=="1" AND $row_trishul_card['trishul_mapped']!="1")
{
	AlertRightCornerErrorFinalHisab("Active trishul card first !");
	exit();
}

$chk_location_verification = Qry($conn,"SELECT id FROM dairy._verify_location WHERE tno='$tno'");
if(!$chk_location_verification){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_location_verification)>0)
{
	AlertRightCornerErrorFinalHisab("Locations verfication pending !");
	exit();
}

$chk_hisab_approval = Qry($conn,"SELECT t.trip_clear,u.title 
FROM dairy.opening_closing AS t 
LEFT OUTER JOIN dairy.own_truck AS o ON o.tno = t.tno 
LEFT OUTER JOIN dairy.user AS u ON u.id = o.superv_id 
WHERE t.trip_no='$trip_no'");

if(!$chk_hisab_approval)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_hisab_approval) == 0)
{
	Redirect("Trip not found..","./");
	exit();	
}

$row_hisab_approval = fetchArray($chk_hisab_approval);

if($row_hisab_approval['trip_clear'] != '1')
{
	AlertRightCornerErrorFinalHisab("Hisab approval pending !<br>Supervisor: $row_hisab_approval[title].");
	exit();	
}

if($type=='FINAL')
{
	$chk_vedl_gps_status = Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE tno='$tno' AND gps_naame_txn_id!='' AND vedl_gps_status='0' 
	ORDER BY id ASC LIMIT 1");

	if(!$chk_vedl_gps_status){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request.","./");
		exit();
	}

	if(numRows($chk_vedl_gps_status)>0)
	{
		$row_gps_status = fetchArray($chk_vedl_gps_status);
		
		AlertRightCornerErrorFinalHisab("Vedanta GPS status pending !<br>Trip: $row_gps_status[from_station] to $row_gps_status[to_station]");
		exit();
	}
}

$chk_supervisor_approval = Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE tno='$tno' AND supervisor_approval='0' 
ORDER BY id ASC LIMIT 1");

if(!$chk_supervisor_approval){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_supervisor_approval)>0 AND $_SESSION['user_code']!='032')
{
	$row_approval = fetchArray($chk_supervisor_approval);
	
	AlertRightCornerErrorFinalHisab("Supervisor approval pending !<br>Trip: $row_approval[from_station] to $row_approval[to_station]");
	exit();
}

if($tno!=$_SESSION['diary'])
{
	Redirect("Invalid Session.","../logout.php");
	exit();
}

if($tno=='' || $trip_no=='' || $trip_no==0 || $type=='')
{
	AlertRightCornerErrorFinalHisab("Data verification failed !");
	exit();
}

// $chk_total_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$tno'");
// if(!$chk_total_trip){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request.","./");
	// exit();
// }

// $total_trips = numRows($chk_total_trip);

$chk_date_verify = Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$tno' AND dates_pending=1");

if(!$chk_date_verify){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_date_verify)>0)
{
	AlertRightCornerErrorFinalHisab("Trip dates verification pending !");
	exit();
}

$chk_pending_lr=Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE lr_pending='1' AND tno='$tno'");

if(!$chk_pending_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_pending_lr)>0)
{
	$row_lr_pending=fetchArray($chk_pending_lr);
	
	AlertRightCornerErrorFinalHisab("LR pending !<br>Trip: $row_lr_pending[from_station] to $row_lr_pending[to_station]");
	exit();
}

$last_trip_id = Qry($conn,"SELECT id,driver_code FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1");

if(!$last_trip_id){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

$row_last_trip = fetchArray($last_trip_id);
$last_trip_id = $row_last_trip['id'];
$driver_code = $row_last_trip['driver_code'];

$get_driver = Qry($conn,"SELECT dummy_driver FROM dairy.driver WHERE code='$driver_code'");
if(!$get_driver){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($get_driver)==0)
{
	AlertRightCornerErrorFinalHisab("Driver not found !");
	exit();
}

$row_driver = fetchArray($get_driver);

if($row_driver['dummy_driver']=="1" AND $type=='LOADED')
{
	AlertRightCornerErrorFinalHisab("Dummy driver active on vehicle !<br>Can\'t take loaded vehicle hisab..");
	exit();
}

if($type=='FINAL')
{
	$qry_pod_check = Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE tno='$tno' AND pod='0' ORDER BY id ASC");
	if(!$qry_pod_check){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request.","./");
		exit();
	}

	$qry_chk_pod_count = Qry($conn,"SELECT from_station,to_station,lrno,pod FROM dairy.trip WHERE tno='$tno' AND lrno not 
	in('EMPTY','CON20','CON40') ORDER BY id ASC");
	if(!$qry_chk_pod_count){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request.","./");
		exit();
	}
	
	$qry_check_trip_end = Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE tno='$tno' AND status=0 ORDER BY id ASC");
	if(!$qry_check_trip_end){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request.","./");
		exit();
	}
	
	$qry_date_chk_zero=Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$tno' AND (date=0 || end_date=0 || pod_date=0 || pod_date is NULL)");
	if(!$qry_date_chk_zero){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request.","./");
		exit();
	}
}
else
{
	$qry_pod_check = Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE tno='$tno' AND pod='0' AND id!='$last_trip_id' 
	ORDER BY id ASC");
	if(!$qry_pod_check){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request.","./");
		exit();
	}
	
	$qry_chk_pod_count = Qry($conn,"SELECT from_station,to_station,lrno,pod FROM dairy.trip WHERE tno='$tno' AND lrno not 
	in('EMPTY','CON20','CON40') AND id!='$last_trip_id' ORDER BY id ASC");
	if(!$qry_chk_pod_count){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request.","./");
		exit();
	}
	
	$qry_check_trip_end = Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE tno='$tno' AND status=0 AND 
	id!='$last_trip_id' ORDER BY id ASC");
	if(!$qry_check_trip_end){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request.","./");
		exit();
	}
	
	$qry_date_chk_zero=Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$tno' AND (date=0 || end_date=0 || pod_date=0 || pod_date is NULL) 
	AND id!='$last_trip_id'");
	if(!$qry_date_chk_zero){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request.","./");
		exit();
	}
}

if(numRows($qry_pod_check)>0)
{
	$row_pod_check = fetchArray($qry_pod_check);
	
	AlertRightCornerErrorFinalHisab("POD not received !<br>Trip: $row_pod_check[from_station] to $row_pod_check[to_station]");
	exit();
}

if(numRows($qry_chk_pod_count)>0)
{
	$row_pod_count = fetchArray($qry_chk_pod_count);

	$lr_count1 = count(explode(',',$row_pod_count['lrno']));

	if($lr_count1!=$row_pod_count['pod'])
	{
		AlertRightCornerErrorFinalHisab("Receive all PODs first !<br>Trip: $row_pod_count[from_station] to $row_pod_count[to_station]");
		exit();
	}
}
	
if(numRows($qry_check_trip_end)>0)
{
	$row_chk_trip_end = fetchArray($qry_check_trip_end);
	
	AlertRightCornerErrorFinalHisab("END trip first !<br>Trip: $row_chk_trip_end[from_station] to $row_chk_trip_end[to_station]");
	exit();
}

if(numRows($qry_date_chk_zero)>0)
{
	$row_zero_date=fetchArray($qry_date_chk_zero);
	$trip_id=$row_zero_date['id'];
	
	errorLog("ZERO DATE FOUND. Truck No : $tno. Trip_id: $trip_id.",$conn,$page_name,__LINE__);
	AlertRightCornerErrorFinalHisab("System error found..");
	exit();
}

if($type=='FINAL')
{
	$_SESSION['final_hisab_type']="0";
}
else if($type=='LOADED')
{
	$_SESSION['final_hisab_type']="1";
}
else
{
	AlertRightCornerErrorFinalHisab("Data verification failed !!");
	exit();
}

$_SESSION['hisab_trip_no'] = $trip_no;

	echo "<form action='./close_trip/final_hisab_index.php' id='myForm2' method='POST'>
		<input type='hidden' value='".sha1($trip_no.md5($tno))."' name='KEY'>		
		<input type='hidden' value='$tno' name='tno'>		
		<input type='hidden' value='$trip_no' name='trip_no'>		
	</form>";
	
AlertRightCornerSuccess("Please wait !<br>Redirecting to hisab page..");
echo "<script>window.setTimeout(function(){document.getElementById('myForm2').submit();},2000);</script>";
exit();
?>