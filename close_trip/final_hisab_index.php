<?php
require_once("../connect.php");
include("../_header.php");

$tno_session = $_SESSION['diary'];
$today_date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$tno = escapeString($conn,strtoupper($_POST['tno']));
$trip_no = escapeString($conn,strtoupper($_POST['trip_no']));

$hisab_trip_no = $_SESSION['hisab_trip_no'];

$check_running_trip = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name!='LOAD_API_TRANS'");

if(!$check_running_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($check_running_trip)>0)
{
	Redirect("Please wait ! Try again after some time..","./");
	exit();
}

if(!isset($_SESSION['final_hisab_type']))
{
	Redirect("Hisab type not found.","./");
	exit();
}

if($_SESSION['final_hisab_type']=="0")
{
	$final_hisab_type="Final";
	$hisab_type="0";
}
else if($_SESSION['final_hisab_type']=="1")
{
	$final_hisab_type="Loaded vehicle";
	$hisab_type="1";
}
else
{
	Redirect("Hisab type is not valid.","./");
	exit();
}

if(empty($branch))
{
	echo "<script type='text/javascript'>
		window.location.href='../';
	</script>";
	exit();
}

if(!isset($_SESSION['diary']))
{
	echo "<script type='text/javascript'>
		window.location.href='../';
	</script>";
	exit();
}

if($tno != $_SESSION['diary'])
{
	echo "<script type='text/javascript'>
		window.location.href='../';
	</script>";
	exit();
}

if(!isset($_POST['KEY']))
{
	echo "<script type='text/javascript'>
			window.location.href='../';
		</script>";
	exit();
}

if(!isset($_SESSION['hisab_trip_no']))
{
	echo "<script type='text/javascript'>
		window.location.href='../';
	</script>";
	exit();
}

$salt_key = sha1($hisab_trip_no.md5($_SESSION['diary']));
$hisab_verify_salt_key = sha1(sha1($hisab_trip_no.md5($_SESSION['diary']))."hisab_verify");

if($trip_no != $hisab_trip_no)
{
	AlertRightCornerErrorFinalHisab("Trip number not verified !");
	echo "<script>window.setTimeout(function(){window.location.href='../'},2000);</script>";
	exit();
}

if($_POST['KEY']!=$salt_key)
{
	AlertRightCornerErrorFinalHisab("Validation error !");
	echo "<script>window.setTimeout(function(){window.location.href='../'},2000);</script>";
	exit();
}

$chk_lock_air_grease = Qry($conn,"SELECT trishul_card,lock_ag,lock_tel,diesel_left_fix,is_crain FROM dairy.own_truck WHERE tno='$tno'");

if(!$chk_lock_air_grease){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

$row_air_grease = fetchArray($chk_lock_air_grease);

$lock_ag = $row_air_grease['lock_ag'];
$lock_tel = $row_air_grease['lock_tel'];
$trishul_card = $row_air_grease['trishul_card'];
$diesel_left_fix_db = $row_air_grease['diesel_left_fix'];
$is_crain = $row_air_grease['is_crain'];

$chk_standby_approval = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no' AND stand_by_approval='1'");
if(!$chk_standby_approval){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($chk_standby_approval)>0)
{
	$stand_by_approval="1"; // to put driver on stand-by // salary start until up on nxt vehicle
}
else
{
	$stand_by_approval="0";
}


$chk_for_hisab_cache = Qry($conn,"SELECT id,trip_no,final_running FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$chk_for_hisab_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($chk_for_hisab_cache)>0)
{
	$row_cache_type = fetchArray($chk_for_hisab_cache);
	
	if($row_cache_type['final_running']!=$hisab_type)
	{
		if($hisab_type=="0"){
			$alert_msg_hisab="Loaded vehicle hisab is in process and you\'ve clicked on Final-Hisab !";
		} else {
			$alert_msg_hisab="Final hisab is in process and you\'ve clicked on Loaded-Vehicle Hisab !";
		}
		
		echo "<script>
			alert('$alert_msg_hisab');
			window.location.href='../';
		</script>";
		exit();
	}
	
	echo "<form action='./final_hisab_proceed.php' id='RedirectCache' method='POST'>
			<input type='hidden' value='hisab_cache' name='hisab_cache'>		
			<input type='hidden' value='$hisab_verify_salt_key' name='hisab_verify'>		
		</form>";
		
	echo "<script>$('#RedirectCache')[0].submit();</script>";
	exit();	
}

$fetch_driver_details=Qry($conn,"SELECT t.driver_code,d.id,d.name,d.last_salary,d.dummy_driver 
FROM dairy.trip as t 
LEFT OUTER JOIN dairy.driver as d ON d.code=t.driver_code
WHERE t.tno='$tno'");
	
if(!$fetch_driver_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($fetch_driver_details)==0)
{
	Redirect("Driver not Found.","./");
	exit();
}

$row_driver_data = fetchArray($fetch_driver_details);

if($row_driver_data['driver_code']=='' || $row_driver_data['driver_code']==0)
{
	errorLog("Driver not Found. Truck No : $tno.",$conn,$page_name,__LINE__);
	Redirect("Driver not Found.","./");
	exit();
}

$driver_code_value = $row_driver_data['driver_code'];
$driver_name = $row_driver_data['name'];
$driver_id = $row_driver_data['id'];
$driver_last_salary = $row_driver_data['last_salary'];
$is_dummy_driver = $row_driver_data['dummy_driver'];

$days = Qry($conn,"SELECT 
(SELECT date(date) FROM dairy.trip WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from',
(SELECT date(end_date) FROM dairy.trip WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to',
(SELECT from_station FROM dairy.trip WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from_station',
(SELECT SUM(km) FROM dairy.trip WHERE trip_no='$trip_no' AND narration!='TRAIN' GROUP BY trip_no) as 'total_km',
(SELECT COUNT(id) FROM dairy.trip WHERE trip_no='$trip_no' AND lr_type!='EMPTY' GROUP BY trip_no) as 'non_empty_trip_count',
(SELECT to_station FROM dairy.trip WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to_station'");

if(!$days){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_days = fetchArray($days);

$start = $row_days['from'];
$end = $row_days['to'];
$from_station = $row_days['from_station'];
$to_station = $row_days['to_station'];
$total_km = $row_days['total_km'];

if($end==0)
{
	$end = date("Y-m-d");
}

$diff = (strtotime($end)- strtotime($start))/24/3600;

$diff_days = $diff+1; // trip running duration in days

// FETCH DATA FROM DRIVER _ UP

$fetch_balance = Qry($conn,"SELECT id,up,asset_form_down,salary_amount,salary_type,salary_approval,amount_hold FROM dairy.driver_up WHERE 
down=0 AND code='$driver_code_value' ORDER BY id DESC LIMIT 1");

if(!$fetch_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($fetch_balance)==0)
{
	errorLog("Unable to fetch driver. Driver Code : $driver_code_value",$conn,$page_name,__LINE__);
	Redirect("Driver not found.","./");
	exit();
}

if(numRows($fetch_balance)>1)
{
	errorLog("Multiple Driver found. Driver Code : $driver_code_value",$conn,$page_name,__LINE__);
	Redirect("Multiple Driver found.","./");
	exit();
}

$row_balance=  fetchArray($fetch_balance);

$driver_up_date = $row_balance['up'];

$da_from = getLastDaDates($conn,$driver_code_value,$driver_up_date);

if(empty($da_from))
{
	AlertRightCornerErrorFinalHisab("DA record not found !");
	echo "<script>window.setTimeout(function(){window.location.href='../'},2000);</script>";
	exit();
}

$salary_from = getLastSalaryDates($conn,$driver_code_value,$driver_up_date,$driver_last_salary);

if(empty($salary_from))
{
	AlertRightCornerErrorFinalHisab("Salary record not found !");
	echo "<script>window.setTimeout(function(){window.location.href='../'},2000);</script>";
	exit();
}

$salary_to = $end; // TILL TRIP END DATE

$salary_days = ((strtotime($salary_to)- strtotime($salary_from))/24/3600+1); 

$diff_da_days="0";

if($diff_da_days>0)
{
	$old_da_from=date("d-m-y",strtotime($row_da_fetch['to_date']. ' + 1 days'));
	$old_da_to=date("d-m-y",strtotime($last_trip_date));
}
else
{
	$old_da_from="";
	$old_da_to="";
}
// echo "MIN".$da_from;

$getHappayBal = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no=(SELECT card_using FROM dairy.happay_card 
WHERE tno='$tno')");

if(!$getHappayBal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($getHappayBal)==0)
{
	$happay_balance=0;
}
else
{
	$rowHappay = fetchArray($getHappayBal);
	$happay_balance = $rowHappay['balance'];
}
include("../code_disabled_right_click.php");
?>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<form action="./final_hisab_index.php" id="ReloadForm" method="POST">
	<input type="hidden" value="<?php echo sha1($trip_no.md5($tno)); ?>" name="KEY">		
	<input type="hidden" value="<?php echo $tno; ?>" name="tno">		
	<input type="hidden" value="<?php echo $trip_no; ?>" name="trip_no">		
</form>

<script>
function ReLoad(){
	$('#ReloadForm')[0].submit();
}
</script>

<style>
label{
	font-size:12px !important;
}
</style>

<?php
$asset_form_down = $row_balance['asset_form_down'];
$salary_amount_db = $row_balance['salary_amount'];
$salary_type = $row_balance['salary_type'];
$salary_approval = $row_balance['salary_approval'];

$qry_asset_naameQry = Qry($conn,"SELECT SUM(amount_naame) as asset_amount FROM dairy.asset_form WHERE driver_code='$driver_code_value'");
	
if(!$qry_asset_naameQry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}
	
$row_asset_naame11 = fetchArray($qry_asset_naameQry);
$asset_amount_naame_rs = $row_asset_naame11['asset_amount'];

if($salary_approval==0 || $salary_amount_db==0)
{
	errorLog("SALARY DATA NOT UPDATED. Driver Code : $driver_code_value, Truck No $tno.",$conn,$page_name,__LINE__);
	AlertRightCornerErrorFinalHisab("SALARY record not updated !");
	echo "<script>window.setTimeout(function(){window.location.href='../'},2000);</script>";
	exit();
}

if($salary_type=="1")
{
	$trip_days_for_salary = $row_days['non_empty_trip_count'];
	$_SESSION['hisab_non_empty_trip_count'] = $trip_days_for_salary; // no of loaded trip
}

$balance_on_trip = $row_balance['amount_hold'];
$driver_up_id = $row_balance['id'];

$_SESSION['amount_hold_before_hisab'] = $balance_on_trip;
$_SESSION['hisab_trip_days'] = $diff_days;
$_SESSION['hisab_trip_start_date'] = $start;
$_SESSION['hisab_trip_end_date'] = $end;
$_SESSION['hisab_trip_km'] = $total_km;
$_SESSION['hisab_driver_code'] = $driver_code_value;
$_SESSION['hisab_driver_up_id'] = $driver_up_id;

echo "<input type='hidden' id='opening_balance' value='$balance_on_trip'>";
echo "<input type='hidden' id='salary_days_value' value='$salary_days'>";
?>
<div class="container-fluid">

<div class="row">
	<div class="form-group col-md-12" style="<?php if(!isMobile()){ echo "font-size:13px"; } else { echo "font-size:11px"; } ?>">
	<br>
		<b><span class="glyphicon glyphicon-home"></span> Logged in as : <font color="red"><?php echo $tno; ?></font></b>
		<?php if(!isMobile()){ echo "&nbsp;"; } else { echo "<br>"; } ?><b><span class="glyphicon glyphicon-user"></span> Driver : <span style="<?php if(isMobile()){ echo "font-size:10.5px"; } ?>"><?php echo $driver_name; ?>.</span></b>
		<?php if(!isMobile()){ echo "&nbsp;"; } else { echo "<br>"; } ?><b><span style="font-size:14px;">₹</span> Driver Balance  : <span style="letter-spacing:.5px;<?php if(!isMobile()){ echo "font-size:16px"; } else { echo "font-size:12px"; } ?>"><?php echo $row_balance['amount_hold']; ?></span></b>
		<?php if(!isMobile()){ echo "&nbsp;"; } else { echo "<br>"; } ?><b><span class="fa fa-credit-card"></span> Happay Balance  : <span style="letter-spacing:.5px;<?php if(!isMobile()){ echo "font-size:16px"; } else { echo "font-size:12px"; } ?>"><?php echo $happay_balance; ?></span></b>
		&nbsp; <button id="AssetFormViewButton" data-toggle="modal" data-target="#AssetFormViewModal" 
		class="btn <?php if(!isMobile()){ echo "btn-sm"; } else { echo "btn-xs"; } ?> btn-primary pull-right"><span class="glyphicon glyphicon-book" style="<?php if(isMobile()){ echo "font-size:10px"; } else { echo "font-size:12px;"; } ?>"></span> &nbsp; View Asset Form</button>
	</div>
</div>

<?php include("modal_asset_form_view.php"); ?>

<div class="row">
	<div class="form-group col-md-12" style="box-shadow:0 0 50px #ddd inset;color:#000;padding:8px;">
		<a href="../"><button id="ClickBack" class="pull-left btn <?php if(!isMobile()){ echo "btn-sm"; } else { echo "btn-xs"; } ?> btn-primary" style="border-radius:8px;">
			<span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Go Back</button>
		</a> 
			<center><span style="<?php if(!isMobile()){ echo "font-size:20px"; } else { echo "font-size:13px"; } ?>">
			<?php echo $final_hisab_type; ?> - Hisab (Trip No: <?php echo $trip_no; ?>)</span> 
			<br/>
			<span style="<?php if(isMobile()){ echo "font-size:12px"; }?>"><?php echo $from_station." to ".$to_station." (".$diff_days." days)"; ?></span>
			</center>
	</div>	
</div>	

<script>	
function PrintTrip(id){
	$("#trip_id_print").val(id);
	$('#PrintForm1')[0].submit();
}
</script>	

<div class="row">
	<div class="form-group col-md-12" class="table-responsive" style="overflow:auto">
		<table class="table table-bordered" style="font-family: 'Open Sans', sans-serif !important;font-size:11px">
				<tr>
					<td class="bg-warning" style="font-size:13px" colspan="12">Trip Summary :</td>
				</tr>
				<tr>
					<th>#</th>
					<th>Route</th>
					<th>Date</th>
					<!--<th>LR_No</th>-->
					<th>Advance</th>
					<!--<th>Cheque</th>-->
					<th>Rtgs</th>
					<th>FrtAdv.</th>
					<th>Diesel</th>
					<th>Naame</th>
					<th>Exp</th>
					<th>TOLL TAX</th>
					<th>FrtColl.</th>
					<th><center><span style="font-size:15px;" class='glyphicon glyphicon-print'></span></center></th>
				</tr>
				
				<script>
				function MyFunc(trip)
				{
					//alert(trip);
				}
				</script>
				
<?php
$qry = Qry($conn,"SELECT t.id,t.from_station,t.to_station,t.km,t.lrno,date(t.date) as trip_date,t.freight,t.cash,t.cheque,
t.rtgs,t.driver_naame,t.diesel,t.fix_dsl_value,t.fix_dsl_left,t.diesel_qty,t.freight_collected,t.expense,t.toll_tax FROM dairy.trip AS t 
WHERE t.tno='$tno'");
				
if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$qry_sum_diesel = Qry($conn,"SELECT SUM(amount) as sum_diesel,SUM(qty) as sum_qty FROM dairy.diesel WHERE trip_id 
IN(SELECT id FROM dairy.trip WHERE tno='$tno')");
				
if(!$qry_sum_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}				

if(numRows($qry_sum_diesel)>0)
{
	$row_sum_diesel=fetchArray($qry_sum_diesel);
	$total_diesel_db = $row_sum_diesel['sum_diesel'];
	$total_diesel_qty_db = $row_sum_diesel['sum_qty'];
}
else
{
	$total_diesel_db = 0;
	$total_diesel_qty_db = 0;
}
				
if(numRows($qry)>0)
{
$sn=1;

$total_cash_sum = 0;
$total_cheque_sum = 0;
$total_rtgs_sum = 0;
$total_diesel_sum = 0;
$total_diesel_qty_sum = 0;
$total_naame_sum = 0;
$total_freight_adv_sum = 0;
$total_freight_collected_sum = 0;
$total_exp_sum = 0;
$total_toll_tax_sum = 0;

$fix_dsl_left = 0;
$fix_dsl_value = 0;
		
while($row=fetchArray($qry))
{
	$total_cash_sum = $total_cash_sum+$row['cash'];
	$total_cheque_sum = $total_cheque_sum+$row['cheque'];
	$total_rtgs_sum = $total_rtgs_sum+$row['rtgs'];
	$total_diesel_sum = $total_diesel_sum+$row['diesel'];
	$total_diesel_qty_sum = $total_diesel_qty_sum+$row['diesel_qty'];
	$total_naame_sum = $total_naame_sum+$row['driver_naame'];
	$total_freight_adv_sum = $total_freight_adv_sum+$row['freight'];
	$total_freight_collected_sum = $total_freight_collected_sum+$row['freight_collected'];
	$total_exp_sum = $total_exp_sum+$row['expense'];
	$total_toll_tax_sum = $total_toll_tax_sum+$row['toll_tax'];
	
	$fix_dsl_left = $fix_dsl_left+$row['fix_dsl_left'];
	$fix_dsl_value = $fix_dsl_value+$row['fix_dsl_value'];

	echo "<tr>
		<td>$sn</td>
		<td>
		<b><a onclick=MyFunc('$row[id]')>$row[from_station] to $row[to_station]</a> &nbsp; ($row[km] km)</b></td>
		<td>".date('d-m-y',strtotime($row['trip_date']))."</td>
		<td>$row[cash]</td>
		<td>$row[rtgs]</td>
		<td>$row[freight]</td>
		<td>$row[diesel] (".sprintf('%0.2f',$row['diesel_qty'])." ltr)</td>
		<td>$row[driver_naame]</td>
		<td>$row[expense]</td>
		<td>$row[toll_tax]</td>
		<td>$row[freight_collected]</td>
		<td><center><button class='btn btn-xs btn-default' onclick='PrintTrip($row[id])' style='color:#000'>
		<span class='glyphicon glyphicon-print'></span></button></center></td>
	</tr>";
	$sn++;
}

	echo "<tr>
		<td colspan='3'><b>Total Calculation : </b></td>
		<td><b>$total_cash_sum</b></td>
		<td><b>$total_rtgs_sum</b></td>
		<td><b>$total_freight_adv_sum</b></td>
		<td><b>".sprintf('%0.2f',$total_diesel_sum)." <br> 
		(Qty: ".sprintf('%0.2f',$total_diesel_qty_sum)." ltr)</b></td>
		<td><b>$total_naame_sum</b></td>
		<td><b>$total_exp_sum</b></td>
		<td><b>$total_toll_tax_sum</b></td>
		<td><b>$total_freight_collected_sum</b></td>
		<td></td>
	</tr>";

	// if($fix_dsl_left!=$diesel_left_fix_db)
	// {
		// errorLog("Diesel balance not verified. Own truck table value is: $diesel_left_fix_db and trip sum is: $fix_dsl_left.",$conn,$page_name,__LINE__);
		// AlertRightCornerErrorFinalHisab("Fix Diesel balance not verified !");
		// echo "<script>window.setTimeout(function(){window.location.href='../'},2000);</script>";
		// exit();
	// }
	
	if($total_diesel_db!=$total_diesel_sum)
	{
		errorLog("Diesel Amount not verified. Diesel Table is: $total_diesel_db and trip is: $total_diesel_sum.",$conn,$page_name,__LINE__);
		AlertRightCornerErrorFinalHisab("Diesel amount not verified !");
		echo "<script>window.setTimeout(function(){window.location.href='../'},2000);</script>";
		exit();
	}
	
	if(sprintf('%0.2f',$total_diesel_qty_db)!=sprintf('%0.2f',$total_diesel_qty_sum))
	{
		errorLog("Diesel Qty not verified. Diesel Table Qty is: $total_diesel_qty_db and trip Qty is: $total_diesel_qty_sum.",$conn,$page_name,__LINE__);
		AlertRightCornerErrorFinalHisab("Diesel Qty not verified !");
		echo "<script>window.setTimeout(function(){window.location.href='../'},2000);</script>";
		exit();
	}
	
	$diesel_qty = sprintf('%0.2f',$total_diesel_qty_sum);	
					
}
else
{
	echo "<td colspan='11'><b><font color='red'>No records found.</font></b></td>";
	$diesel_qty=0;		
}

$_SESSION['hisab_diesel_qty'] = $diesel_qty;
$_SESSION['hisab_diesel_amount'] = $total_diesel_db;
$_SESSION['hisab_fix_diesel_value'] = $fix_dsl_value;
?>
		</table>
	</div>

<div id="result"></div>	

<script>
function CalculateDA(amount)
{
	if($('#da_from').val()=='' || $('#da_to').val()=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select DA dates first !</font>',});
		$('#da_from').val('');
		$('#da_to').val('');
		$('#da').val('');
		$('#da_amt').val('');
	}
	else
	{
		if($('#da_from').val()<'<?php echo $da_from ?>')
		{
			$('#proceed_hisab').attr('disabled',true);
			$('#da_from').val('');
			$('#da_to').val('');
			$('#da').val('');
			$('#da_amt').val('');
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>DA from-date is invalid !</font>',});
		}
		else
		{
			$('#da_from').attr('readonly',true);
			$('#da_to').attr('readonly',true);
			$("#da").val((Number($("#da_date_duration").val()) * Number($("#da_amt").val())).toFixed(2));
		}	
	}	
}

function DaDateChk()
{
	if($('#da_from').val()<'<?php echo $da_from ?>')
	{
		$('#da_from').val('');
	}
}
</script>
				
<?php include("modal_asset_form_down.php"); ?>

<form action="./final_hisab_proceed.php" method="POST">
		
<input type="hidden" name="hisab_verify" value="<?php echo $hisab_verify_salt_key; ?>">
		
<div class="form-group col-md-7" style="font-size:12px">
			
<script type="text/javascript">
function CheckAssetForm(elem)
{
var asset_form = '<?php echo $asset_form_down; ?>';
var happay_balance = '<?php echo $happay_balance; ?>';
var asset_naame = '<?php echo $asset_amount_naame_rs; ?>';

	if(elem==1)
	{
		if(happay_balance>0)
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Withdraw happay balance first !</font>',});
			$('#down_driver_selection').val('');
		}
		else
		{
			if(asset_form==0)
			{
				function AssetFuncDown()
				{
					$('#AssetModalDown')[0].click(); 
					$('#down_type_div').hide();
					$('#down_alert').show();
					$('#down_driver_type').attr('required',false);
				}
			}
			else
			{
				function AssetFuncDown()
				{
					$('#down_alert').show();
					$('#down_type_div').show();
					$('#down_driver_type').attr('required',true);
				}
			}

			AssetFuncDown();
			
			$('#asset_amount_naame_rs').val(asset_naame);
			$('#asset_amount_naame_rs_html').val(asset_naame);
		}
	}
	else
	{
		$('#down_alert').hide();
		$('#down_type_div').hide();
		$('#down_driver_type').attr('required',false);
		
		$('#asset_amount_naame_rs').val("0");
		$('#asset_amount_naame_rs_html').val("0");
	}
}
</script>
			
	<?php
	if($fix_dsl_value>0)
	{
	?>
	<div class="col-md-12">
		<div class="alert alert-success" style="font-size:13px;font-weight:bold;">
			Trip Fix Diesel: <?php echo $fix_dsl_value; ?> LTR, Diesel Given: <?php echo $diesel_qty; ?> LTR, Diesel Balance: <?php echo $fix_dsl_left; ?> LTR
		</div>	
	</div>
	<?php	
	}
	?>	
	
	<div class="form-group col-md-6">
		<label>Down DRIVER <span style="font-size:12px;"> ( ड्राइवर डाउन करे ) </span> <sup><font color="red">*</font></sup> </label>
		<select style="font-size:12px" onchange="CheckAssetForm(this.value)" id="down_driver_selection" name="down_driver_selection" class="form-control" required>
			<option style="font-size:12px" value="">--select an option--</option>
			<option style="font-size:12px" value="0">No continue - जारी रखे</option>
			<option style="font-size:12px" value="1">Yes down - हां डाउन करे</option>
		</select>
	</div>
			
	<div id="driver_down_result"></div>
					
	<div style="display:none" id="down_type_div" class="form-group col-md-6">
		<label>Down Reason : <sup><font color="red">*</font></sup></label>
		<select style="font-size:12px" onchange="DownDriverCheck(this.value)" id="down_driver_type" name="down_driver_type" class="form-control" required>
			<option style="font-size:12px" value="">Select an option</option>
			<option <?php if($stand_by_approval=="0"){ echo "style='background:yellow;font-size:12px'; disabled='disabled'"; } else { echo "style='font-size:12px'"; }?> value="STAND_BY">STANDBY - स्टैंडबाय  ( सैलरी चालू )  <?php if($stand_by_approval=="0"){ echo "- admin approval required."; }?></option>
			<option style="font-size:12px" value="ON_LEAVE">ON LEAVE - छुट्टी पर</option>
			<option style="font-size:12px" value="LEFT">LEFT COMPANY - कंपनी छोड़ कर गया</option>
			<option style="font-size:12px" value="LEFT_ROUTE">LEFT ON-ROUTE - बिना सुचना छोड़ कर गया</option>
		</select>
	</div>

<script>
function DownDriverCheck(elem)
{
	if(elem=='LEFT' || elem=='LEFT_ROUTE')
	{
		$('#reason_div').show();
		$('#standby_div').hide();
		$('#reason_other_div').hide();
		
		$('#reason_left').attr('required',true);
		$('#standby_reason').attr('required',false);
		$('#reason_other').attr('required',false);
		
		$('#loadicon').show();
		jQuery.ajax({
		url: "./chk_guarantor_ref.php",
		data: 'ok=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#result_guarantor_ref").html(data);
		},
		error: function() {}
		});
	}
	else if(elem=='STAND_BY')
	{
		$('#reason_div').hide();
		$('#standby_div').show();
		$('#reason_other_div').hide();
		
		$('#reason_left').attr('required',false);
		$('#standby_reason').attr('required',true);
		$('#reason_other').attr('required',false);
	}
	else
	{
		if(elem!='')
		{
			$('#loadicon').show();
			jQuery.ajax({
			url: "./chk_guarantor_ref.php",
			data: 'ok=' + 'ok',
			type: "POST",
			success: function(data) {
				$("#result_guarantor_ref").html(data);
			},
			error: function() {}
			});
		}
		
		$('#reason_div').hide();
		$('#standby_div').hide();
		$('#reason_other_div').hide();
		
		$('#reason_left').attr('required',false);
		$('#standby_reason').attr('required',false);
		$('#reason_other').attr('required',false);
	}
}
</script>

<script>
function MyFuncStandBy(elem)
{
	if(elem=='OTHER')
	{
		$('#reason_other_div').show();
		$('#reason_other').attr('required',true);
	}
	else
	{
		$('#reason_other_div').hide();
		$('#reason_other').attr('required',false);
	}
}
</script>
					
	<div id="standby_div" style="display:none" class="form-group col-md-6">
		<label>Reason of Stand By : </label>
		<select style="font-size:12px" onclick="MyFuncStandBy(this.value)" id="standby_reason" name="standby_reason" class="form-control">
			<option style="font-size:12px" value="">Select an option</option>
			<option style="font-size:12px" value="MAINTENANCE">Maintenance - गाड़ी रख -रखाव में है</option>
			<option style="font-size:12px" value="STRIKE">Strike - हड़ताल</option>
			<option style="font-size:12px" value="ACCIDENT">Accident - एक्सीडेंट</option>
			<option style="font-size:12px" disabled value="OTHER">Other Reason</option>
		</select>
	</div>
					
	<div id="reason_other_div" style="display:none" class="form-group col-md-6">
		<label>Write other Reason of Stand By : <sup><font color="red">*</font></sup></label>
		<textarea style="font-size:12px" name="reason_other" id="reason_other" class="form-control"></textarea>
	</div>
					
	<div id="reason_div" style="display:none" class="form-group col-md-12">
		<label>Reason of LEFT : <sup><font color="red">*</font></sup></label>
		<textarea style="font-size:12px" name="reason_left" id="reason_left" class="form-control"></textarea>
	</div>
					
	<div class="col-md-12">
		<div id="down_alert" class="alert alert-warning" style="font-weight:bold;display:none">
			आपने ड्राइवर डाउन करने का चयन किया हुआ है , ड्राइवर हिसाब के बाद स्वतः डाउन हो जाएगा ! <br>
			Your have selected driver down option, driver will be down automatically after hisab.
		</div>	
	</div>
					
<?php
if($diff_da_days>0)
{
?>
	<div class="form-group col-md-12">
		<div class="alert alert-danger" style="font-weight:bold;">
			<label>DA Difference Found of <?php echo $diff_da_days; ?> Days 
			(<b><?php echo $old_da_from; ?> to <?php echo $old_da_to; ?></b>) . <br> Do You want to Pay ..? </label>
			<br />
			<select style="width:50%" onchange="OldDaCall(this.value)" name="diff_da_pay" class="form-control" required>
				<option value="">--SELECT--</option>
				<option value="1">YES</option>
				<option value="0">NO</option>
			</select>
		</div>	
	</div>
<?php		
}
else
{
	echo '<input type="hidden" name="diff_da_pay" value="0" required />';
}
?>		

<input type="hidden" id="old_da_diff_input" value="0" />
<input type="hidden" id="asset_amount_naame_rs" value="<?php echo $asset_amount_naame_rs; ?>" />

<script type="text/javascript">
function OldDaCall(elem)
{
		$('#da_from').val('');
		 $('#da_to').val('');
		 $('#da').val('');
		 $('#da_date_duration').val('');
		 $('#da_amt').val('');
		 
		 $('#da_from').attr('readonly',false);
		 $('#da_to').attr('readonly',false);
		 
	if(elem==1)
	{
		 $('#old_da_diff_input').val('<?php echo $diff_da_days; ?>');
	}
	else
	{
		 $('#old_da_diff_input').val('0');
	}
}
</script>				
				
	<div class="form-group col-md-12">
		<span style="font-size:13px;color:maroon" color="red">DA Duration : 
		<span id="from_date_value_da">0000-00-00</span> to <span id="to_date_value_da">0000-00-00</span> <span style="display:none">(<span id="da_date_diff"></span> days)</span></span>
	</div>
				
	<div id="da_dates" style="display:none1">
		
		<div class="form-group col-md-6">
			<label>DA From Date <font color="red">*</font></label>
            <input <?php if($is_dummy_driver==1 || $is_crain=='1') { echo "readonly"; } ?> name="da_from" onchange="FromDateDA(this.value);DaDateChk();" id="da_from" type="date" class="form-control" min="<?php echo $da_from; ?>" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
        </div>
				 
<script>
function FromDateDA(fromdate)
{
	var nextDay = new Date(fromdate);
	nextDay.setDate(nextDay.getDate() + 0);
	
	var next_day=nextDay.toISOString().slice(0,10);
	
	var date_today = '<?php echo $today_date; ?>';
	
	if(fromdate!=date_today)
	{
		$("#da_to").attr("min",nextDay.toISOString().slice(0,10));
	}
	
	$("#da_to").val("");
	$("#from_date_value_da").html("0000-00-00");
	$("#to_date_value_da").html("0000-00-00");
	$("#from_date_value_da").html(fromdate);
	$('#da_date_diff').html('0');
	$('#da_date_duration').val('0');
}

function ToDateDA(todate)
{
	if($('#da_from').val()=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select DA from-date first !</font>',});
		$("#to_date_value_da").html("0000-00-00");
		$('#da_to').val('');
		
		$('#da_from_date').val('');
		$('#da_to_date').val('');
		$('#da_date_diff').html('0');
		$('#da_date_duration').val('0');
		
		$('#tel').val('0');
		$('#ag').val('0');
	}
	else
	{
		$("#to_date_value_da").html(todate);
		
		var dafrom = $('#da_from').val();
		var dato = todate;
		var diff =  Math.floor(( Date.parse(dato) - Date.parse(dafrom) ) / 86400000 +1); 
		var old_da_diff = Number($('#old_da_diff_input').val());
		
		if(old_da_diff=='')
		{
			var old_da_diff_days = 0;
		}
		else
		{
			var old_da_diff_days = 0;
		}
		
		$('#da_from_date').val(dafrom);
		$('#da_to_date').val(dato);
		$('#da_date_diff').html(diff);
		$('#da_date_duration').val(diff+old_da_diff_days);
						
	<?php
	if($lock_ag=="1" || $is_crain=='1'){
	?>
		$('#ag').val('0');
	<?php						
	} else {
	?>
		$('#ag').val(diff*10);
	<?php
	}
	?>

	<?php
	if($lock_tel=="1" || $is_crain=='1'){
	?>
		$('#tel').val('0');
	<?php						
	} else {
	?>
		$('#tel').val(diff*10);
	<?php
	}
	?>
	}	
}
</script>
				 
<div id="da_result"></div>
				
	<div class="form-group col-md-6">
        <label>DA Till Date <font color="red">*</font></label>
		<input <?php if($is_dummy_driver==1 || $is_crain=='1') { echo "readonly"; } ?> name="da_to" onchange="ToDateDA(this.value)" id="da_to" type="date" class="form-control" min="<?php echo date("Y-m-d"); ?>" max="<?php echo date('Y-m-d',(strtotime ('-0 day' ,strtotime(date("Y-m-d"))))); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
		<!-- <input name="da_to" onchange="ToDateDA(this.value)" id="da_to" type="date" class="form-control" min="<?php// echo date("Y-m-d"); ?>" max="<?// echo $end; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" /> -->
    </div>
				
</div>
				
	<div class="form-group col-md-4">
		<label>Total DA</label>
		<input type="number" step="any" class="form-control" value="0" id="da" name="da" readonly required>
	</div>
				
	<div class="form-group col-md-4">
		<label>DA Duration (in days)</label>
		<input type="number" step="any" class="form-control" id="da_date_duration" value="<?php if($is_dummy_driver==1 || $is_crain=='1') { echo "0"; } else { echo $diff_days; } ?>" name="da_duration" readonly required>
	</div>
				
	<div class="form-group col-md-4">
		<label>DA/PerDay <font color="red"><sup>*</font></sup></label>
		<select style="font-size:12px" <?php if($is_dummy_driver==1 || $is_crain=='1') { echo "readonly"; } ?> id="da_amt" name="da_amt" onchange="CalculateDA(this.value);sum_balance()" class="form-control" required>
			<option style="font-size:12px" <?php if($is_dummy_driver==1 || $is_crain=='1') { echo "disabled"; } ?> value="">--select amount/day--</option>
			<option style="font-size:12px" <?php if($is_dummy_driver==1 || $is_crain=='1') { echo "readonly selected='selected'"; } ?> value="0">0</option>
			<option style="font-size:12px" <?php if($is_dummy_driver==1 || $is_crain=='1') { echo "disabled"; } ?> value="200">200/- per day</option>
			<option style="font-size:12px" <?php if($is_dummy_driver==1 || $is_crain=='1') { echo "disabled"; } ?> value="250">250/- per day</option>
			<option style="font-size:12px" <?php if($is_dummy_driver==1 || $is_crain=='1') { echo "disabled"; } ?> value="275">275/- per day</option>
			<option style="font-size:12px" <?php if($is_dummy_driver==1 || $is_crain=='1') { echo "disabled"; } ?> value="300">300/- per day</option>
		</select>
	</div>
				
				
	<div class="form-group col-md-3">
		<label>Telephone</label>
		<input type="number" step="any" class="form-control" value="<?php if($is_dummy_driver==1 || $is_crain=='1') { echo "0"; } else if($lock_tel=="1") { echo "0"; } else { echo $diff_days*10; }?>" id="tel" name="tel" readonly required>
	</div>
				
	<div class="form-group col-md-3">
		<label>Air & Grease</label>
		<input type="number" step="any" class="form-control" value="<?php if($is_dummy_driver==1 || $is_crain=='1') { echo "0"; } else if($lock_ag=="1") { echo "0"; } else { echo $diff_days*10; }?>" id="ag" name="ag" readonly required>
	</div>
				
	<div class="form-group col-md-3">
		<label>Trip Days</label>
		<input type="number" step="any" class="form-control" id="duration" value="<?php echo $diff_days; ?>" name="duration" readonly required>
	</div>
				
	<div id="give_salary_div" class="form-group col-md-3">
		<label>Give Salary <font color="red"><sup>*</font></sup></label>
		<select style="font-size:12px" <?php if($is_dummy_driver==1) { echo "readonly"; } ?> id="give_salary" name="give_salary" onchange="FuncSal(this.value);sum_balance()" class="form-control" required>
			<option style="font-size:12px" <?php if($is_dummy_driver==1) { echo "disabled"; } ?> value="">Select an option</option>
			<option style="font-size:12px" <?php if($is_dummy_driver==1) { echo "readpnly selected='selected'"; } ?> value="0">NO</option>
			<option style="font-size:12px" <?php if($is_dummy_driver==1) { echo "disabled"; } ?> value="1">YES</option>
		</select>
	</div>
				
<script>
function FuncSal(elem)
{
	$('#sal_from_date').val('');
	$('#sal_to_date').val('');
	
	if(elem==1)
	{
		$('#salary').attr('min','1');
		$("#salary_cuttoff").attr('required',true);
		<?php
		if($salary_type=="1")
		{
			?>
			$('#salary').val('<?php echo $trip_days_for_salary*$salary_amount_db; ?>');
			$('#salary_amount_cache').val('<?php echo $trip_days_for_salary*$salary_amount_db; ?>');
			$('#sal_dates').hide();
			$('#sal_from_date').attr('required',false);
			$('#sal_to_date').attr('required',false);
			$('#sal_to_date').attr('min',false);
			$('#sal_to_date').attr('max',false);
			$('#sal_from_date').val('<?php echo $start; ?>');
			$('#sal_to_date').val('<?php echo $end; ?>');
			$("#from_date_value").html('<?php echo $start; ?>');
			$("#to_date_value").html('<?php echo $end; ?>');
			$("#salary_pattern").attr('required',false);
			<?php
		}
		else
		{
		?>
			$('#salary').val('');
			$('#salary_amount_cache').val('');
			$('#sal_dates').show();
			$('#sal_from_date').attr('required',true);
			$('#sal_to_date').attr('required',true);
			$("#from_date_value").html("0000-00-00");
			$("#to_date_value").html("0000-00-00");
			$("#salary_pattern").attr('required',true);
		<?php
		}
		?>
		$('#sal_div').show();
		$('#salary_duration').show();
	}
	else
	{
		$("#salary_cuttoff").attr('required',false);
		$("#salary_pattern").attr('required',false);
		$('#salary').attr('min','0');
		$('#salary').val('0');
		$('#salary_amount_cache').val('0');
		$('#sal_div').hide();
		$('#salary_duration').hide();
		$('#sal_dates').hide();
		$('#sal_from_date').attr('required',false);
		$('#sal_to_date').attr('required',false);
		$("#from_date_value").html("0000-00-00");
		$("#to_date_value").html("0000-00-00");
	}
}
</script>
			 
	<div id="sal_dates" style="display:none">
				
<?php
	$chk_bulker = Qry($conn,"SELECT is_bulker FROM dairy.own_truck WHERE tno='$tno'");
	
	if(!$chk_bulker){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}
				
	$row_bulker = fetchArray($chk_bulker);
				
	if($row_bulker['is_bulker']=="1")
	{
	?>
	<div class="form-group col-md-4">
        <label>SALARY Deduction <font color="red">*</font></label>
		<select style="font-size:12px" onchange="$('#sal_from_date').val('');$('#sal_to_date').val('');$('#salary_pattern').val('')" id="salary_cuttoff" name="salary_cuttoff" class="form-control" required>
			<option style="font-size:12px" value="">--select option--</option>
			<option style="font-size:12px" value="NO">No deduction</option>
			<option style="font-size:12px" value="30">Yes 30% deduction</option>
		</select>
	</div>
	<?php
	}
	else
	{
		echo "<input type='hidden' value='0' id='salary_cuttoff' name='salary_cuttoff'>";
	}
?>
				
	<div class="form-group col-md-4">
		<label>Salary per Month <font color="red">*</font></label>
		<select style="font-size:12px" onchange="$('#sal_from_date').val('');$('#sal_to_date').val('');" id="salary_pattern" name="salary_pattern" class="form-control" required>
			<option style="font-size:12px" value="">--select option--</option>
			<option style="font-size:12px" value="<?php echo $salary_amount_db; ?>"><?php echo $salary_amount_db; ?></option>
		</select>
	</div>
				
	<div class="form-group col-md-4">
		<label>SALARY From Date <font color="red">*</font></label>
        <input name="sal_from_date" onchange="FromDate(this.value)" id="sal_from_date" type="date" class="form-control" min="<?php echo $salary_from; ?>" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
				 
<script>
function FromDate(fromdate)
{
	var sal_cutt_off = $("#salary_cuttoff").val();
	var salary_pattern = $("#salary_pattern").val();
	
	if(sal_cutt_off=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select salary deduction first !</font>',});
		$('#sal_from_date').val('');
		$('#salary_cuttoff').focus();
	}
	else	
	{
		if(salary_pattern=='')
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select salary pattern first !</font>',});
			$('#sal_from_date').val('');
			$('#salary_pattern').focus();
		}
		else
		{
			var nextDay = new Date(fromdate);
			nextDay.setDate(nextDay.getDate() + 0);
			$("#sal_to_date").attr("min",nextDay.toISOString().slice(0,10));
			$("#sal_to_date").val("");
			$("#from_date_value").html("0000-00-00");
			$("#to_date_value").html("0000-00-00");
			$("#from_date_value").html(fromdate);
		}
	}
}
</script>
				 
<div id="salary_result"></div>
				
		<div class="form-group col-md-4">
            <label>SALARY Till Date <font color="red">*</font></label>
            <input name="sal_to_date" onchange="ToDate(this.value)" id="sal_to_date" type="date" class="form-control" min="<?php echo date("Y-m-d"); ?>" max="<?php echo date('Y-m-d',(strtotime ('-0 day' ,strtotime(date("Y-m-d"))))); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
		</div>
				
	</div>
	
<script>
function ToDate(todate)
{
	var sal_cutt_off = $("#salary_cuttoff").val();
	var salary_pattern = $("#salary_pattern").val();
	
	if(sal_cutt_off=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select salary deduction first !</font>',});
		$('#sal_from_date').val('');
		$('#sal_to_date').val('');
		$('#salary_cuttoff').focus();
	}
	else					
	{
		if(salary_pattern=='')
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select salary pattern first !</font>',});
			$('#sal_from_date').val('');
			$('#sal_to_date').val('');
			$('#salary_pattern').focus();
		}
		else
		{
			if($('#sal_from_date').val()=='')
			{
				Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select salary from-date first !</font>',});
				$("#to_date_value").html("0000-00-00");
				$('#sal_to_date').val('');
			}
			else
			{
				$("#to_date_value").html(todate);
				$("#loadicon").show();
					jQuery.ajax({
					url: "./date_for_salary.php",
					data: 'from=' + $('#sal_from_date').val() + '&to=' + todate + '&sal_amount=' + '<?php echo $salary_amount_db; ?>' + '&sal_cut_off=' + sal_cutt_off + '&page_name=' + 'final' + '&salary_pattern=' + salary_pattern,
					type: "POST",
					success: function(data) {
						$("#salary_result").html(data);
					},
					error: function() {}
				});
			}	
		}	
	}	
} 
</script>	
			
		<div id="salary_duration" style="display:none" class="form-group col-md-12">
			<span style="font-size:13px;color:maroon">SALARY Duration : 
			<span id="from_date_value">0000-00-00</span> to <span id="to_date_value">0000-00-00</span> - <span id="salary_days_html">NA</span></span>
		</div>
			
		<div id="sal_div" style="display:none" class="form-group col-md-6">
			<label>Salary Amount <font color="red"><sup>*</font></sup></label>
			<input min="0" class="form-control" oninput="sum_balance()" onblur="sum_balance()" value="0" type="number" id="salary" name="salary" readonly required>
		</div>
			
		<script>
			function sum_balance()
			{
				$("#closing").val((Number($("#opening_balance").val()) - Number($("#da").val()) - Number($("#tel").val()) - Number($("#ag").val()) - Number($("#salary").val()) + Number($("#trishul_card_deduction").val()) + Number($("#asset_amount_naame_rs").val())).toFixed(2));
			}
			//
		</script>
			
	<div class="form-group col-md-6">
		<label>Closing Balance <font color="red"><sup>*</font></sup></label>
		<input class="form-control" value="<?php echo $row_balance['amount_hold']; ?>" readonly step="any" type="number" id="closing" name="closing" required>
	</div>
	
	<div id="asset_naame_div" class="form-group col-md-6">
		<label style="color:red">Asset Naame ( एसेट नामे )</label>
		<input class="form-control" value="<?php echo $asset_amount_naame_rs; ?>" readonly step="any" type="number" id="asset_amount_naame_rs_html" name="asset_amount_naame_rs" required>
	</div>
			
<?php
if($trishul_card=="1")
{
?>
	<div class="form-group col-md-6">
		<label>Trishul Card Received <font color="red"><sup>*</font></sup></label>
		<select style="font-size:12px" onchange="TrishulCardSel(this.value)" id="is_trishul_rcvd" name="is_trishul_rcvd" class="form-control" required>
			<option style="font-size:12px" value="">--select option--</option>
			<option style="font-size:12px" value="YES">YES</option>
			<option style="font-size:12px" value="NO">NO</option>
		</select>
	</div>
			
	<div id="trishul_deduction" style="display:none" class="form-group col-md-12">
		<font color="red">System will recover Rs: 1000/- from driver's for trishul card.<br>
		त्रिशूल कार्ड के लिए 1000 रुपये ड्राइवर से वसूले जायेगे</font>
	</div>
			
<script>
function TrishulCardSel(elem)
{
	if(elem=='NO'){
		$('#trishul_deduction').show();
		$('#trishul_card_deduction').val('1000'); 
		sum_balance();
	
	} else {
		$('#trishul_deduction').hide();
		$('#trishul_card_deduction').val('0');
		sum_balance();
	}
}
</script>

<?php
}
?>
			
	<input type="hidden" value="0" id="trishul_card_deduction" name="trishul_card_deduction">
			
	<div class="col-md-12">
		<div class="col-md-12 alert alert-danger" style="font-weight:bold">
			Enter GPS KM Between : <?php echo date("d-m-y",strtotime($start))." to ".date("d-m-y",strtotime($end)); ?>
			&nbsp;, System KM is : <?php echo $total_km." km"; ?> , &nbsp; Average : <span id="truck_avg"></span>
		</div>	
	</div>	
			
<?php
	$five_perc=round($total_km*5/100);
	$ten_perc=round($total_km*10/100);
	$max_km = $total_km+$five_perc;
	$min_km = $total_km-$five_perc;
				
	$max_km_new = $total_km+$ten_perc;
	$min_km_new = $total_km-$ten_perc;
?>

	<input type="hidden" id="salary_amount_cache" name="salary_amount_cache">
			
	<div class="col-md-12" id="gps_km_error_div" style="display:none;color:red">
		<label id="gps_km_error">GPS km Difference can't be greater than 5%.</label>
	</div>
			
	<div class="col-md-12">
		<div class="row">
			<div class="form-group col-md-<?php if($_SESSION['final_hisab_type']=="1"){ echo "3"; } else { echo "4"; } ?>">
				<label>GPS KM <font color="red"><sup>*</font></sup></label>
				<input class="form-control" min="0" oninput="GPSKmEntered(this.value)" type="number" id="gps_km1" name="gps_km" required>
			</div>

	<input type="hidden" name="da_from_date" id="da_from_date" value="<?php echo $da_from; ?>">
	<input type="hidden" name="da_to_date" id="da_to_date" value="<?php echo $end; ?>">
	<input type="hidden" name="from_station" value="<?php echo $from_station; ?>">
	<input type="hidden" name="to_station" value="<?php echo $to_station; ?>">			
	
<script>	
function LoadHisabTimestamp(hisab_date)
{ 
	$('#loadicon').show();
	jQuery.ajax({
		url: "./_fetch_loaded_hisab_pois.php",
		data: 'date=' + hisab_date,
		type: "POST",
		success: function(data) {
			$("#hisab_location_1").html(data);
		},
		error: function() {}
		});
}

function CheckHisabLocPincode(elem)
{
	if(elem!='')
	{
		$('#loadicon').show();
		jQuery.ajax({
		url: "./check_pincode_hisab_location.php",
		data: 'elem=' + elem,
		type: "POST",
		success: function(data) {
			$("#pincode_chk_div").html(data);
		},
		error: function() {} 
		});
	}
}
</script>	
			<?php
			if($_SESSION['final_hisab_type']=="1")
			{
			?>
			
			<div class="form-group col-md-3">
				<label>Hisab Date <font color="red"><sup>*</font></sup></label>
				<input onchange="LoadHisabTimestamp(this.value);" id="hisab_date_1" name="hisab_date_1" type="date" 
				min="<?php echo date("Y-m-d", strtotime("-3 day")); ?>" max="<?php echo date("Y-m-d"); ?>" class="form-control" 
				required="required" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<div class="form-group col-md-3">
				<label>Hisab Location/Time <font color="red"><sup>*</font></sup></label>
				<select style="font-size:12px" onchange="CheckHisabLocPincode(this.value)" name="hisab_location_1" id="hisab_location_1" class="form-control" required>
					<option style="font-size:12px !important" value="">--hisab location/time--</option>
				</select>
			</div>
			
			<div id="pincode_chk_div"></div>
			
			<div class="form-group col-md-3">
				<label>Pincode <font color="red"><sup>*</font></sup></label>
				<input id="hisab_location_pincode_new" name="hisab_location_pincode_new" readonly type="text" class="form-control" required="required" />
			</div>
			
			<?php			
			}
			else
			{
				echo "
				<input type='hidden' name='hisab_date_1'>
				<input type='hidden' name='hisab_location_1'>
				";
			}
			?>
			
			<div class="form-group col-md-3">
				<label>&nbsp;</label>
				<br>
				<button type="submit" id="proceed_hisab" class="btn btn-primary btn-block">
					Next Step &nbsp; <span class="glyphicon glyphicon-arrow-right"></span>
				</button>
			</div>	
		</div>	
	</div>
			
<script>
function GPSKmEntered(km)
{
	var km = Number(km);
	var max_km = '<?php echo $max_km; ?>';
	var min_km = '<?php echo $min_km; ?>';
	var diesel_qty = '<?php echo $diesel_qty; ?>';
	var truck_avg = Number(km/diesel_qty).toFixed(2);
	
	if(diesel_qty>0)
	{
		$('#truck_avg').html(truck_avg+" kmpl");
		
		if(km>max_km || km<min_km)
		{
			$('#gps_km_error_div').show();
			$('#proceed_hisab').attr('disabled',true);
		}
		else
		{
			$('#gps_km_error_div').hide();
			$('#proceed_hisab').attr('disabled',false);
		}
	}
	else
	{
		$('#truck_avg').html("NO DIESEL");
		
		if(km>max_km || km<min_km)
		{
			$('#gps_km_error_div').show();
			$('#proceed_hisab').attr('disabled',true);
		}
		else
		{
			$('#gps_km_error_div').hide();
			$('#proceed_hisab').attr('disabled',false);
		}
	}
}
</script>
			
	<div class="form-group col-md-4">
		<label>&nbsp;</label>
	</div>
	</div>
</div>
</div>
</form>

</body>

<?php
if($is_dummy_driver==1)
{
	echo "<script>FuncSal('0');</script>";
}
?>

<form action='../print/single_trip.php' target="_blank" id='PrintForm1' method='POST'>
	<input type='hidden' id="trip_id_print" name='trip_id_print'>
	<input type='hidden' value="<?php echo $driver_name; ?>" name='driver_name'>
</form>

<script>$("#loadicon").fadeOut('slow');</script>

<div id="result_guarantor_ref"></div>	

<?php include("../footer.php"); ?>