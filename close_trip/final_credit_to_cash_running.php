<?php
require_once("../connect.php");

// if($branch!='HEAD')
// {
	// Redirect("Error While Processing Request.","./");
	// exit();
// }

$timestamp = date("Y-m-d H:i:s"); 

$date=date("Y-m-d");

$tno=escapeString($conn,$_SESSION['diary']);
$cr_amount=escapeString($conn,$_POST['amount']);

include("./_hisab_func.php");

$get_cache_data = Qry($conn,"SELECT trip_no,driver_name,driver_code,down_driver,down_type,left_reason,standby_reason,
standby_reason_other,balance_on_hisab,da_amount,da_from,da_to,salary_amount,sal_from,sal_to,tel_amount,ag_amount,closing,asset_form_naame,
diesel_qty,sys_km,gps_km FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$get_cache_data)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_cache_data)==0)
{
	errorLog("HISAB NOT FOUND. in Cache. Truck No: $tno.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_get_cache = fetchArray($get_cache_data);

$trip_no = $row_get_cache['trip_no'];
$driver_name = $row_get_cache['driver_name'];
$driver_code = $row_get_cache['driver_code'];
$down_driver = $row_get_cache['down_driver'];
$down_type = $row_get_cache['down_type'];
$balance_on_hisab = $row_get_cache['balance_on_hisab'];
$da_amount = $row_get_cache['da_amount'];
$da_from = $row_get_cache['da_from'];
$da_to = $row_get_cache['da_to'];
$salary_amount = $row_get_cache['salary_amount'];
$sal_from = $row_get_cache['sal_from'];
$sal_to = $row_get_cache['sal_to'];
$tel_amount = $row_get_cache['tel_amount'];
$ag_amount = $row_get_cache['ag_amount'];
$asset_form_naame = $row_get_cache['asset_form_naame'];
$closing_db = $row_get_cache['closing'];
$diesel_qty = $row_get_cache['diesel_qty'];
$sys_km = $row_get_cache['sys_km'];
$gps_km = $row_get_cache['gps_km'];

if($diesel_qty>0)
{
	$avg=sprintf("%.2f",$gps_km/$diesel_qty);
}
else
{
	$avg=0;
}

$da = $da_amount;
$tel = $tel_amount;
$ag = $ag_amount;
// $give_salary = $_POST['give_salary'];

$balance = $balance_on_hisab;
$asset_amount = $asset_form_naame;

$salary_from = $sal_from;
$salary_to = $sal_to;

$closing=$closing_db;

if($closing<=0)
{
	errorLog("Closing must be postive value for credit to cashbook.",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

if($closing!=($balance-($da+$tel+$ag+$salary_amount)+$asset_amount))
{
	errorLog("Unable to Verify Amount. Closing : $closing, DA : $da, SAL : $salary_amount, Tel & AG : $tel+$ag, Asset AmountName :$asset_amount.",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

if($cr_amount!=$closing)
{
	errorLog("Credit amount is not valid. Client is : $cr_amount and system is $closing.",$conn,$page_name,__LINE__);
	Redirect("Credit amount is not valid.","./");	
	exit();
}

if($down_driver==1)
{
	if($down_type=='STAND_BY'){
		$down_type_db=0;
		$down_reason = escapeString($conn,strtoupper($row_get_cache['standby_reason']));
		
		if($row_get_cache['standby_reason']=='OTHER'){
			$down_reason=escapeString($conn,strtoupper($row_get_cache['standby_reason_other']));
		}
		else{
			$down_reason=$row_get_cache['standby_reason'];
		}
	}
	else if($down_type=='ON_LEAVE'){
		$down_type_db=1;
		$down_reason="ON_LEAVE";
	}
	else if($down_type=='LEFT_ROUTE' || $down_type=='LEFT'){
		$down_type_db=2;
		$down_reason=escapeString($conn,strtoupper($row_get_cache['left_reason']));
	}
	else{
		errorLog("Driver down status not found. Driver Code : $driver_code.",$conn,$page_name,__LINE__);
		Redirect("Error while processing your request. Down Status","./");	
		exit();
	}
	
	$d_code_new="";
	$next_trip_opening = "0";
}
else
{
	$d_code_new=$driver_code;
	$next_trip_opening = "0";
}

$comp_sel = Qry($conn,"SELECT comp FROM dairy.own_truck WHERE tno='$tno'");
if(!$comp_sel)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

$row_comp = fetchArray($comp_sel);

$company = $row_comp['comp'];

if($company=='RRPL')
{
	$col = "balance";
	$credit = "credit";
}
else if($company=='RAMAN_ROADWAYS')
{
	$col = "balance2";
	$credit = "credit2";
}
else
{
	errorLog("Company name is EMPTY. TruckNo $tno",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

StartCommit($conn);
$flag = true;

$RunningTripBreakup = RunningTripBreakup($conn,$page_name,$tno,$trip_no,$branch,$_SESSION['user_code'],$gps_km,$avg,$diesel_qty,0,0,$d_code_new,$date,$timestamp);

if($RunningTripBreakup==false){
	$flag = false;
}

$select_branch_bal = Qry($conn,"SELECT `$col` as balance FROM user WHERE username='$branch'");

if(!$select_branch_bal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_balance = fetchArray($select_branch_bal);

$new_balance = $closing+$row_balance['balance'];

$update_cashbook = Qry($conn,"INSERT INTO cashbook (user,date,vou_date,comp,vou_type,desct,`$credit`,`$col`,timestamp) 
VALUES ('$branch','$date','$date','$company','CREDIT-HO','Driver Down Credit, Trip_No: $trip_no, TruckNo: $tno, Driver: $driver_name',
'$closing','$new_balance','$timestamp')");

$cashbook_id=getInsertID($conn);

if(!$update_cashbook){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_balance = Qry($conn,"UPDATE user SET `$col`='$new_balance' WHERE username='$branch'");

if(!$update_balance)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insret_credit_entry = Qry($conn,"INSERT INTO credit(credit_by,cash_id,section,tno,branch,company,amount,narr,date,timestamp) 
VALUES ('CASH','$cashbook_id','CREDIT-HO','$tno','$branch','$company','$closing','Driver Down Credit, Trip_No: $trip_no, TruckNo: $tno, 
Driver: $driver_name','$date','$timestamp')");

if(!$insret_credit_entry)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// $qry_update_closing = UpdateOpeningClosing($trip_no,$conn,$page_name,$date,$branch,$_SESSION['user_code'],"0",$gps_km,$avg,$timestamp);

// if($qry_update_closing==false){
	// $flag = false;
// }

$balance=$balance+$asset_amount;

if($asset_amount>0)
{
	$qry_asset_form_cr = AssetFormCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$balance,$driver_code,$asset_amount,$timestamp);

	if($qry_asset_form_cr==false){
		$flag = false;
	}

	$asset_row_id = mysqli_insert_id($conn);
}
else
{
	$asset_row_id = "0";
}

if($salary_amount>0)
{
	$insert_sal = SalaryCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$timestamp);

	if($insert_sal==false){
		$flag = false;
	}
	
	$balance=$balance-$salary_amount;

	$insert_driver_book_sal = SalaryCreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$balance,$timestamp);

	if($insert_driver_book_sal==false){
		$flag = false;
	}

	$salary_row_id = mysqli_insert_id($conn);
}
else
{
	$salary_row_id = "0";
}

if($da>0)
{ 
	$insert_da = DAcredit($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$timestamp);

	if($insert_da==false){
		$flag = false;
	}
	
	$balance=$balance-$da;

	$insert_da_1 = DAcreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$balance,$timestamp);

	if($insert_da_1==false){
		$flag = false;
	}

	$da_row_id = mysqli_insert_id($conn);
}
else
{
	$da_row_id = "0";
}

	$balance=$balance-$tel;
	
	$insert_exp_tel = InsertTelephone($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$tel,$balance,$timestamp);

	if($insert_exp_tel==false){
		$flag = false;
	}
	
	$telephone_row_id = mysqli_insert_id($conn);
	
	$balance = $balance - $ag;

	$insert_exp_ag = InsertAirGrease($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$ag,$balance,$timestamp);

	if($insert_exp_ag==false){
		$flag = false;
	}
	
	$air_grease_row_id = mysqli_insert_id($conn);
	
	if($balance!=$closing)
	{
		$flag = false;
		errorLog("Balance amount not equal to closing amount. Balance is : $balance. Closing Amount is : $closing.",$conn,$page_name,__LINE__);
	}
	
	// if($balance!=0)
	// {
		// $flag = false;
		// errorLog("Balance amount not equal to Zero. Balance is : $balance. Closing Amount is : $closing.",$conn,$page_name,__LINE__);
	// }

	$insert_driver_2= Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,debit,balance,date,branch,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','CREDIT-HO','$closing','0','$date','$branch','$timestamp')");

	if(!$insert_driver_2)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_pay_row_id = "0";
	$hisab_cfwd_row_id = "0";
	$hisab_credit_ho_row_id = mysqli_insert_id($conn);
	
$insert_hisab_log = InsertHisabLog($conn,$page_name,$tno,$trip_no,"1","4",$down_driver,$down_type,0,0,0,$cashbook_id,$driver_code,$branch,$_SESSION['user_code'],$date,$timestamp,$asset_row_id,$salary_row_id,$da_row_id,$telephone_row_id,$air_grease_row_id,$hisab_pay_row_id,$hisab_cfwd_row_id,$hisab_credit_ho_row_id);

if($insert_hisab_log==false){
	$flag = false;
}

$driver_down_qry = DriverDownSet("RUNNING",$conn,$page_name,$tno,$trip_no,$down_driver,$date,$down_type_db,$balance,$driver_code,$down_type,$down_reason,$branch,$timestamp);

if($driver_down_qry==false){
	$flag = false;
}

$delete_cache = Qry($conn,"DELETE FROM dairy.hisab_cache WHERE tno='$tno'");
if(!$delete_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}

echo "<form action='../hisab/hisab_view.php' id='myFormHisab' method='POST'>
			<input type='hidden' value='$trip_no' name='trip_no'>		
			<input type='hidden' value='$tno' name='tno'>		
		</form>";
	
	echo "<script type='text/javascript'>
			function submitform2()
			{
			alert('Hisab Success.'); 
			document.getElementById('myFormHisab').submit();
			}
		submitform2();	
		</script>";
	exit();
?>