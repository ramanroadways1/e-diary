<?php
require("../connect.php");

if(!isset($_SESSION['diary'])){
	Redirect("","./logout.php");
	exit();
}

if($branch==''){
	Redirect("Unable to Fetch Branch","../");
	exit();
}

$tno = escapeString($conn,$_SESSION['diary']);

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$trip_id=escapeString($conn,$_POST['trip_id']);

$get_trip = Qry($conn,"SELECT tno,trip_no,lr_type,lrno FROM dairy.trip WHERE id='$trip_id'");

if(!$get_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_trip)==0){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Trip not found..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_trip = fetchArray($get_trip);

if($row_trip['tno']!=$tno)
{
	echo "<script>alert('Vehicle number not verified..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$trip_no=escapeString($conn,$row_trip['trip_no']);
$lrno=escapeString($conn,$row_trip['lr_type']);
$lrnos=escapeString($conn,$row_trip['lrno']);

if(strpos($lrno,'OLRSP')!==false)
{
	echo "<script>alert('Error: LR Belongs to COAL.');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(substr_count($lrno,',')>0)
{
	echo "<script>alert('Error: Invalid LR found..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(count($_FILES['pod_copy']['name'])==0)
{
	echo "<script>alert('Error: POD not found..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");
	
foreach($_FILES['pod_copy']['type'] as $file_types)
{
	if(!in_array($file_types, $valid_types))
	{
		echo "<script>alert('Error: Only pdf/image upload allowed..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
}
        
for($i=0; $i<count($_FILES['pod_copy']['name']); $i++)
{
   $sourcePath = $_FILES['pod_copy']['tmp_name'][$i];

   if(empty($sourcePath))
   {
	   echo "<script>alert('Error: POD not found..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	   exit();
   }
   
	$fix_name=$trip_id."-".$trip_no.$tno.date('dmYHis').$i;
	$shortname = "pod/".$fix_name.".".pathinfo($_FILES['pod_copy']['name'][$i],PATHINFO_EXTENSION);
	$targetPath = "pod/".$fix_name.".".pathinfo($_FILES['pod_copy']['name'][$i],PATHINFO_EXTENSION);

	  if($_FILES['pod_copy']['type'][$i]!='application/pdf'){		
		ImageUpload(800,800,$_FILES['pod_copy']['tmp_name'][$i]);
	  }

	if(move_uploaded_file($sourcePath, $targetPath))
	{
		$files[] = $shortname;
    }
	else
	{
	   echo "<script>alert('Error while uploading files..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	   exit();
	}
}

$file_name=implode(',',$files);
   
StartCommit($conn);
$flag = true;

$insert_pod = Qry($conn,"INSERT INTO dairy.rcv_pod(trip_id,tno,lrno,trip_no,date,copy,branch,branch_user,timestamp) VALUES 
('$trip_id','$tno','$lrnos','$trip_no','$date','$file_name','$branch','$_SESSION[user_code]','$timestamp')");

if(!$insert_pod){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_pod_count = Qry($conn,"UPDATE dairy.trip SET pod=pod+1,pod_date='$timestamp' WHERE id='$trip_id'");

if(!$update_pod_count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>alert('POD Received !!');window.location.href='./';</script>";
	exit();
}
else
{
	foreach(explode(",",$file_name) as $uploaded_files){
		unlink($uploaded_files);
	}
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>