<?php

// start --> for change trip order e-diary admin

function CopyTripToTempTable($conn,$tno,$page_name,$trip_id,$after_trip_id){
	
	$flag_check = true;
	
	$dlt_cache_data = Qry($conn,"DELETE FROM dairy.trip_cache WHERE tno='$tno'");

	if(!$dlt_cache_data){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_trip_bkp_id_old = Qry($conn,"UPDATE dairy.trip SET trip_id_backup='' WHERE tno='$tno'");

	if(!$update_trip_bkp_id_old){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	} 

	$copy = Qry($conn,"INSERT INTO dairy.trip_cache(trip_id_ext,trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,
	to_station,from_id,to_id,from_poi,from_pincode,to_poi,to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,
	addr_book_id_consignee,
	fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,fix_adv_amount,loaded_hisab,km,dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,
	charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,mb_amount,lrno,start_poi_date,end_poi_date,date,end_date,trip_end_user,status,dates_pending,
	active_trip,freight,cash,cheque,rtgs,diesel,diesel_qty,freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,
	final_exp_tel,final_exp_ag,pod,pod_date,narration,timestamp,roll_back_timestamp,check_empty,supervisor_approval,approval_timestamp,polyline) 
	SELECT id,trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,to_station,from_id,to_id,from_poi,from_pincode,to_poi,
	to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,
	fix_adv_amount,loaded_hisab,km,dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,
	mb_amount,lrno,start_poi_date,end_poi_date,date,end_date,trip_end_user,status,dates_pending,active_trip,freight,cash,cheque,rtgs,diesel,
	diesel_qty,freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,final_exp_tel,final_exp_ag,pod,pod_date,
	narration,timestamp,roll_back_timestamp,check_empty,supervisor_approval,approval_timestamp,polyline FROM dairy.trip WHERE id='$trip_id'");

	if(!$copy)
	{
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$copy_next_trips = Qry($conn,"INSERT INTO dairy.trip_cache(trip_id_ext,trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,
	to_station,from_id,to_id,from_poi,from_pincode,to_poi,to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,
	fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,fix_adv_amount,loaded_hisab,km,dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,
	charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,mb_amount,lrno,start_poi_date,end_poi_date,date,end_date,trip_end_user,status,dates_pending,
	active_trip,freight,cash,cheque,rtgs,diesel,diesel_qty,freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,
	final_exp_tel,final_exp_ag,pod,pod_date,narration,timestamp,roll_back_timestamp,check_empty,supervisor_approval,approval_timestamp,polyline) 
	SELECT id,trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,to_station,from_id,to_id,from_poi,from_pincode,to_poi,
	to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,
	fix_adv_amount,loaded_hisab,km,dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,
	mb_amount,lrno,start_poi_date,end_poi_date,date,end_date,trip_end_user,status,dates_pending,active_trip,freight,cash,cheque,rtgs,diesel,
	diesel_qty,freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,final_exp_tel,final_exp_ag,pod,pod_date,
	narration,timestamp,roll_back_timestamp,check_empty,supervisor_approval,approval_timestamp,polyline FROM dairy.trip WHERE tno='$tno' 
	AND id>'$after_trip_id' AND id!='$trip_id'");

	if(!$copy_next_trips){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_og_trip_main = Qry($conn,"DELETE FROM dairy.trip WHERE id='$trip_id'");

	if(!$delete_og_trip_main){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$delete_og_next_trips = Qry($conn,"DELETE FROM dairy.trip WHERE tno='$tno' AND id>'$after_trip_id' AND id!='$trip_id'");

	if(!$delete_og_next_trips){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_trip_order = Qry($conn,"INSERT INTO dairy.trip(trip_id_backup,trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,
	to_station,from_id,to_id,from_poi,from_pincode,to_poi,to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,
	fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,fix_adv_amount,loaded_hisab,km,dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,
	charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,mb_amount,lrno,start_poi_date,end_poi_date,date,end_date,trip_end_user,status,dates_pending,
	active_trip,freight,cash,cheque,rtgs,diesel,diesel_qty,freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,
	final_exp_tel,final_exp_ag,pod,pod_date,narration,timestamp,roll_back_timestamp,check_empty,supervisor_approval,approval_timestamp,polyline) 
	SELECT trip_id_ext,trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,to_station,from_id,to_id,from_poi,from_pincode,
	to_poi,to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,
	fix_adv_amount,loaded_hisab,km,dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,
	mb_amount,lrno,start_poi_date,end_poi_date,date,end_date,trip_end_user,status,'1',active_trip,freight,cash,cheque,rtgs,diesel,diesel_qty,
	freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,final_exp_tel,final_exp_ag,pod,pod_date,narration,
	timestamp,roll_back_timestamp,check_empty,supervisor_approval,approval_timestamp,polyline FROM dairy.trip_cache WHERE tno='$tno'");

	if(!$insert_trip_order){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($flag_check){
		return true;
	}else{
		return false;
	}
}

// end --> for change trip order e-diary admin
 
function TripCopy($trip_no,$conn,$timestamp,$branch,$page_name){
	
$copy = Qry($conn,"INSERT INTO dairy.trip_final(trip_id,trip_no,tno,driver_code,branch,edit_branch,from_station,to_station,from_id,to_id,
from_poi,from_pincode,to_poi,to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,fix_lane,fix_dsl_type,
fix_dsl_value,fix_dsl_left,fix_adv_amount,loaded_hisab,km,dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,charge_wt,lr_type,
lr_type2,oxygen_lr,mb_to_pay,mb_amount,lrno,start_poi_date,end_poi_date,date,end_date,status,freight,cash,cheque,rtgs,diesel,diesel_qty,
freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,final_exp_tel,final_exp_ag,pod,pod_date,hisab_date,hisab_branch,narration,timestamp,supervisor_approval,
approval_timestamp,polyline) 
SELECT id,trip_no,tno,driver_code,branch,edit_branch,from_station,to_station,from_id,to_id,from_poi,from_pincode,to_poi,to_pincode,consignor_id,
addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,fix_adv_amount,loaded_hisab,km,
dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,mb_amount,lrno,start_poi_date,
end_poi_date,date,end_date,status,freight,cash,cheque,rtgs,diesel,diesel_qty,freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,final_exp_tel,
final_exp_ag,pod,pod_date,'$timestamp','$branch',narration,timestamp,supervisor_approval,approval_timestamp,polyline FROM dairy.trip WHERE trip_no='$trip_no'");

if(!$copy)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	return false;
}
else
{
	return true;
	
	if(AffectedRows($conn)==0)
	{
		errorLog("Zero trip copied. Trip No : $trip_no. Truck No : $_SESSION[diary].",$conn,$page_name,__LINE__);
		return false;
	}
}
}

function DeleteTrips($trip_no,$conn,$page_name){
	
	$delete_trip=Qry($conn,"DELETE FROM dairy.trip WHERE trip_no='$trip_no'");
	if(!$delete_trip)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true;
		
		if(AffectedRows($conn)==0)
		{
			errorLog("Zero trip Deleted. Trip No : $trip_no. Truck No : $_SESSION[diary].",$conn,$page_name,__LINE__);
			return false;
		}
	}
}

function UpdateOpeningClosing($trip_no,$conn,$page_name,$closing_date,$closing_branch,$branch_user,$closing_amount,$gps_km,$avg,$diesel_qty,$timestamp){
	
	$diesel_balance = $_SESSION['hisab_fix_diesel_value'] - $diesel_qty;
	
	$qry_update_closing=Qry($conn,"UPDATE dairy.opening_closing SET closing='$closing_amount',closing_branch='$closing_branch',
	closing_user='$branch_user',closing_date='$closing_date',closing_timestamp='$timestamp',gps_km_of_trip='$gps_km',
	fix_diesel='$_SESSION[hisab_fix_diesel_value]',diesel_qty='$diesel_qty',diesel_balance='$diesel_balance',avg='$avg',fromstation='$closing_branch',
	tostation='CGROAD',duedate='$closing_date' WHERE trip_no='$trip_no'");

	if(!$qry_update_closing)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true;
		
		// if(AffectedRows($conn)==0)
		// {
			// errorLog("Zero trip Deleted. Trip No : $trip_no. Truck No : $_SESSION[diary].",$conn,$page_name,__LINE__);
			// return false;
		// }
	}
}

function AssetFormCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$balance,$driver_code,$asset_amount,$timestamp){
	
	$insert_driver_book_asset=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,credit,balance,date,branch,
	timestamp) VALUES ('$driver_code','$tno','$trip_no','ASSET_FORM_CREDIT','$asset_amount','$balance','$date','$branch','$timestamp')");

	if(!$insert_driver_book_asset)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true;
	}
}
 
function TrishulCardCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$balance,$driver_code,$trishul_card,$timestamp){
	
	$flag_check = true;
		
	$insert_driver_book_record=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,credit,balance,date,branch,
	timestamp) VALUES ('$driver_code','$tno','$trip_no','TRISHUL_CARD_CREDIT','$trishul_card','$balance','$date','$branch','$timestamp')");

	if(!$insert_driver_book_record){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_card_status = Qry($conn,"INSERT INTO dairy._trishul_card_log (tno,status,trip_no,timestamp) VALUES 
	('$tno','Card_Lost','$trip_no','$timestamp')");
	
	if(!$update_card_status){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_card_status2 = Qry($conn,"UPDATE dairy.own_truck SET trishul_card='0',trishul_mapped='0' WHERE tno='$tno'");
	
	if(!$update_card_status2){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($flag_check){
		return true;
	}else{
		return false;
	}
}

function SalaryCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$timestamp){
	
	$insert_sal=Qry($conn,"INSERT INTO dairy.salary (driver_code,trip_no,from_date,to_date,amount,tno,branch,date,timestamp) VALUES 
	('$driver_code','$trip_no','$salary_from','$salary_to','$salary_amount','$tno','$branch','$date','$timestamp')");

	if(!$insert_sal)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true;
	}
}

function SalaryCreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$balance,$timestamp){
	
	$insert_driver_book_sal=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,debit,balance,date,branch,timestamp) 
	VALUES ('$driver_code','$tno','$trip_no','SALARY- $salary_from to $salary_to','$salary_amount','$balance','$date','$branch',
	'$timestamp')");

	if(!$insert_driver_book_sal)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true;
	}
}

function DAcredit($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$timestamp){
	
	$insert_da=Qry($conn,"INSERT INTO dairy.da_book (trip_no,driver_code,tno,from_date,to_date,amount,branch,date,narration,timestamp) 
	VALUES ('$trip_no','$driver_code','$tno','$da_from','$da_to','$da','$branch','$date','','$timestamp')");

	if(!$insert_da)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true;
	}
}

function DAcreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$balance,$timestamp){
	
	$insert_da_1= Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,debit,balance,date,branch,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','DA- $da_from to $da_to','$da','$balance','$date','$branch','$timestamp')");

	if(!$insert_da_1)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true;
	}
}

function InsertTelephone($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$tel,$balance,$timestamp){
	
	$insert_exp_tel= Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,debit,balance,date,branch,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','EXP-TELEPHONE','$tel','$balance','$date','$branch','$timestamp')");

	if(!$insert_exp_tel)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true; 
	}
}

function InsertAirGrease($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$ag,$balance,$timestamp){
	
	$insert_exp_ag= Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,debit,balance,date,branch,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','EXP-AIR_AND_GREASE','$ag','$balance','$date','$branch','$timestamp')");

	if(!$insert_exp_ag)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true;
	}
} 

function InsertHisabLog($closing_balance,$conn,$page_name,$tno,$trip_no,$loaded_final,$hisab_type,$down_driver,$down_type,$cash_vou_id,$chq_vou_id,
	$rtgs_vou_id,$cashbook_id,$driver_code,$branch,$user_code,$date,$timestamp,$row_asset,$row_sal,$row_da,$row_tel,$row_ag,$row_pay,$row_cfwd,$row_cr_ho){
	
	$insert_hisab_log=Qry($conn,"INSERT INTO dairy.log_hisab(tno,trip_no,closing_balance,loaded_final,hisab_type,row_cfwd,row_pay,row_credit_ho,
	row_da,row_salary,row_tel_exp,row_ag_exp,row_asset_naame,driver_down,down_type,cash_vou,chq_vou,rtgs_vou,credit_cash,
	driver,branch,branch_user,date,timestamp) VALUES ('$tno','$trip_no','$closing_balance','$loaded_final','$hisab_type','$row_cfwd','$row_pay',
	'$row_cr_ho','$row_da','$row_sal','$row_tel','$row_ag','$row_asset','$down_driver','$down_type','$cash_vou_id',
	'$chq_vou_id','$rtgs_vou_id','$cashbook_id','$driver_code','$branch','$user_code','$date','$timestamp')");

	if(!$insert_hisab_log)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		return false;
	}
	else
	{
		return true;
	}
}

function DriverDownSet($hisab_type,$conn,$page_name,$tno,$trip_no,$down_driver,$date,$down_type_db,$balance,$driver_code,$down_type,$down_reason,$branch,$timestamp){
if($down_driver==1)
{
	$flag_check = true;
		
	$update_balance = Qry($conn,"UPDATE dairy.driver_up SET down='$date',status='$down_type_db',amount_hold='$balance' WHERE down=0 
	AND code='$driver_code' ORDER BY id DESC LIMIT 1");
	if(!$update_balance){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(AffectedRows($conn)==0){
		$flag_check = false;
		errorLog("Unable to update driver balance. Driver Code : $driver_code.",$conn,$page_name,__LINE__);
	}

	$update_down_truck = Qry($conn,"UPDATE dairy.own_truck SET driver_code='0' WHERE driver_code='$driver_code'");
	if(!$update_down_truck){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_down_driver = Qry($conn,"UPDATE dairy.driver SET active='0' WHERE code='$driver_code'");
	if(!$update_down_driver){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$insert_down_driver = Qry($conn,"INSERT INTO dairy.driver_down_reasons(driver_code,down_type,reason,branch,tno,trip_no,timestamp) VALUES 
	('$driver_code','$down_type','$down_reason','$branch','$tno','$trip_no','$timestamp')");
	if(!$insert_down_driver){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$insert_down_data=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,date,branch,timestamp) VALUES 
		('$driver_code','$tno','$trip_no','DRIVER_DOWN','$date','$branch','$timestamp')");
	if(!$insert_down_data){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$move_asset_form = Qry($conn,"INSERT INTO dairy.asset_form_done(driver_code,item_code,item_qty,item_rate,item_value_up,item_value_down,
	quantity,amount,amount_naame,branch,trip_no,tno,timestamp) SELECT driver_code,item_code,item_qty,item_rate,item_value_up,item_value_down,
	quantity,amount,amount_naame,branch,trip_no,tno,timestamp FROM dairy.asset_form WHERE driver_code='$driver_code'");
	if(!$move_asset_form){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$dlt_asset_form = Qry($conn,"DELETE FROM dairy.asset_form WHERE driver_code='$driver_code'");
	if(!$dlt_asset_form){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$chk_if_guarantor = Qry($conn,"SELECT id FROM dairy.driver WHERE guarantor='$driver_code'");
	
	if(!$chk_if_guarantor){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($chk_if_guarantor) > 0)
	{
		$date_today_1 = date("Y-m-d");
		
		$update_down_date = Qry($conn,"UPDATE dairy.driver SET guarantor_last_active_date='$date_today_1' WHERE guarantor='$driver_code'");
	
		if(!$update_down_date){
			$flag_check = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	$chk_if_vedl_gps = Qry($conn,"SELECT id,loaded_hisab,gps_naame_txn_id FROM dairy.trip WHERE tno='$tno'");	
	
	if(!$chk_if_vedl_gps){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($chk_if_vedl_gps) > 0)
	{
		$row_vedl_gps_chk = fetchArray($chk_if_vedl_gps);
		
		if($row_vedl_gps_chk['loaded_hisab']=="1" AND $row_vedl_gps_chk['gps_naame_txn_id']!='')
		{
			$flag_check = false;
			errorLog("Driver down not allowed in loaded hisab for VEDL GPS Case.",$conn,$page_name,__LINE__);
		}
	}

	if($flag_check){
		return true;
	}else{
		return false;
	}
}
else
{
	$flag_check = true;

	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold='$balance' WHERE down=0 AND code='$driver_code' 
	ORDER BY id DESC LIMIT 1");

	if(!$update_driver_balance){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	// if($balance!=0 AND AffectedRows($conn)==0)
	// {
		// $flag_check = false;
		// errorLog("Unable to update driver balance: $balance. Driver Code : $driver_code.",$conn,$page_name,__LINE__);
	// }
	
	if($hisab_type=='RUNNING')
	{
		$update_asset_form = Qry($conn,"UPDATE dairy.asset_form SET trip_no=(SELECT trip_no FROM dairy.trip WHERE tno='$tno') 
		WHERE driver_code='$driver_code'");
		
		if(!$update_asset_form){
			$flag_check = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	$chk_if_vedl_gps = Qry($conn,"SELECT id,branch,trip_no,loaded_hisab,gps_naame_txn_id FROM dairy.trip WHERE tno='$tno'");	
	
	if(!$chk_if_vedl_gps){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($chk_if_vedl_gps) > 0)
	{
		$row_vedl_gps_chk = fetchArray($chk_if_vedl_gps);
		
		if($row_vedl_gps_chk['loaded_hisab']=="1" AND $row_vedl_gps_chk['gps_naame_txn_id']!='')
		{
			$gps_naame_txn_id_last = $row_vedl_gps_chk['gps_naame_txn_id'];
			$new_trip_id = $row_vedl_gps_chk['id'];
			
			if($hisab_type!='RUNNING')
			{
				$flag_check = false;
				errorLog("Something went wrong. Hisab must be take loaded in VEDL GPS case. Vehicle No : $tno.",$conn,$page_name,__LINE__);
			}
			
			SendWAMsg($conn,"9024281599","*Take loaded hisab of VEDL gps vehicle.*\nVehicle No: $tno.\nTimestamp: $timestamp");
			
			$get_gps_naame_data = Qry($conn,"SELECT id FROM dairy.driver_book WHERE trans_id='$gps_naame_txn_id_last'");
		
			if(!$get_gps_naame_data){
				$flag_check = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($get_gps_naame_data)==0)
			{
				$flag_check = false;
				errorLog("VEDL GPS Naame txn not found. Truck No : $tno. Txnid: $gps_naame_txn_id_last.",$conn,$page_name,__LINE__);
			}
			
			if(numRows($get_gps_naame_data)>1)
			{
				$flag_check = false;
				errorLog("VEDL GPS Naame multiple txn found. Truck No : $tno. Txnid: $gps_naame_txn_id_last.",$conn,$page_name,__LINE__);
			}
			
			$row_data_gps_naame = fetchArray($get_gps_naame_data);
			
			$naame_db_id = $row_data_gps_naame['id'];
			
			$update_d_new_bal = Qry($conn,"UPDATE dairy.driver_book SET balance = balance - 3000 WHERE id>'$naame_db_id' AND 
			driver_code='$driver_code'");
			
			if(!$update_d_new_bal){
				$flag_check = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			// Get previous and current trip then update opening closing
			
			$get_last_trip_no = Qry($conn,"SELECT trip_no FROM dairy.trip_final WHERE tno='$tno' ORDER BY id DESC LIMIT 1");
			
			if(!$get_last_trip_no){
				$flag_check = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$row_prev_trip = fetchArray($get_last_trip_no);
			
			$update_closing = Qry($conn,"UPDATE dairy.opening_closing SET closing = closing - '3000' WHERE trip_no='$row_prev_trip[trip_no]'");
			
			if(!$update_closing){
				$flag_check = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$update_opening = Qry($conn,"UPDATE dairy.opening_closing SET opening = opening - '3000' WHERE trip_no='$row_vedl_gps_chk[trip_no]'");
			
			if(!$update_opening){
				$flag_check = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			// Get previous and current trip then update opening closing
			
			$delete_current_record = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$naame_db_id'");
			
			if(!$delete_current_record){
				$flag_check = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		
			$get_driver_balance = Qry($conn,"SELECT amount_hold FROM dairy.driver_up WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");
			
			if(!$get_driver_balance){
				$flag_check = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($get_driver_balance)==0)
			{
				$flag_check = false;
				errorLog("Driver balance not found. Driver_code: $driver_code.",$conn,$page_name,__LINE__);
			}
			
			$row_d_bal = fetchArray($get_driver_balance);
			
			$driver_balance = $row_d_bal['amount_hold'];
			
			$insert_driver_book = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_id,trip_no,trans_id,desct,credit,balance,
			date,branch,narration,timestamp) VALUES ('$driver_code','$tno','$row_vedl_gps_chk[id]','$row_vedl_gps_chk[trip_no]',
			'$row_vedl_gps_chk[gps_naame_txn_id]','DRIVER_NAAME','3000','$driver_balance','".date("Y-m-d")."','$row_vedl_gps_chk[branch]',
			'VEDL_GPS_CONTINUE','$timestamp')");
			
			if(!$insert_driver_book){
				$flag_check = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}

	if($flag_check){
		return true;
	}else{
		return false;
	}
}
}

function RunningTripBreakup($down_driver,$conn,$page_name,$tno,$trip_no,$branch,$branch_user,$gps_km,$avg,$diesel_qty,$next_trip_opening,$closing,$d_code_new,$date,$timestamp){
	
	$flag_check = true;
	
	$get_hisab_corrd = Qry($conn,"SELECT hisab_location FROM dairy.hisab_cache WHERE trip_no='$trip_no'");

	if(!$get_hisab_corrd){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_hisab_corrd) == 0)
	{
		$flag_check = false;
		errorLog("Hisab cache not found. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}
	
	$row_cache = fetchArray($get_hisab_corrd);
	
	$hisab_location_db = $row_cache['hisab_location'];
	
	$get_last_trip_from = Qry($conn,"SELECT driver_code,from_station,from_poi,to_station,to_poi,km,dsl_consumption FROM dairy.trip 
	WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1");

	if(!$get_last_trip_from){
		$flag_check = false; 
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_last_trip_from) == 0)
	{
		$flag_check = false;
		errorLog("Last Trip not found. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}
	
	$row_last_trip = fetchArray($get_last_trip_from);
	
	$driver_code_current = $row_last_trip['driver_code'];
	
	//////////////////// Distance - 1 Endss
	
	$origin = $row_last_trip['from_poi'];
	$destination = explode("_",$hisab_location_db)[0].",".explode("_",$hisab_location_db)[1];

	$url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw";

	$api = file_get_contents($url);
	$data = json_decode($api);
				
	$api_status = $data->rows[0]->elements[0]->status;
		
	if($api_status=='NOT_FOUND')
	{
		$flag_check = false;
		errorLog("Google distance not found. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}

	if($api_status!='OK')
	{
		$flag_check = false;
		errorLog("API Error: $api_status. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}
				
	$distance_first = sprintf('%.2f',((int)$data->rows[0]->elements[0]->distance->value / 1000));
	
	$avg_1 = sprintf('%.2f',($row_last_trip['km']/$row_last_trip['dsl_consumption']));
	
	$new_dsl_consumption_1 =  sprintf('%.2f',($distance_first/$avg_1));
	
	//////////////////// Distance - 1 Endss
	
	
	//////////////////// Distance - 2
	
	$origin1 = explode("_",$hisab_location_db)[0].",".explode("_",$hisab_location_db)[1];
	$destination1 = $row_last_trip['to_poi'];

	$url1 = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin1)."&destinations=".urlencode($destination1)."&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw";

	$api1 = file_get_contents($url1);
	$data1 = json_decode($api1);
				
	$api_status1 = $data1->rows[0]->elements[0]->status;
		
	if($api_status1=='NOT_FOUND')
	{
		$flag_check = false;
		errorLog("Google distance not found. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}

	if($api_status1!='OK')
	{
		$flag_check = false;
		errorLog("API Error: $api_status1. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}
				
	$distance_second = sprintf('%.2f',((int)$data1->rows[0]->elements[0]->distance->value / 1000));
	
	$new_dsl_consumption_2 =  sprintf('%.2f',($distance_second/$avg_1));
	
	//////////////////// Distance - 2 Ends
	

	$hisab_loc_pincode = explode("_",$hisab_location_db)[2];
	
	if(strlen($hisab_loc_pincode)!=6)
	{
		$get_pincode = getZipcode($destination);

		if(strlen($get_pincode)!=6)
		{
			$flag_check = false;
			errorLog("Unable to find pincode. Trip_no: $trip_no. Pincode: $get_pincode. Geo cord.: $destination",$conn,$page_name,__LINE__);
		}
		else
		{
			$hisab_loc_pincode = $get_pincode;
		}
	}
	
	$select_last_trip = Qry($conn,"SELECT id,tno,edit_branch,from_station,to_station,from_id,to_id,km,pincode,act_wt,charge_wt,
	lr_type,lr_type2,oxygen_lr,mb_to_pay,mb_amount,lrno,date(end_date) as end_date,from_poi,from_pincode,to_poi,to_pincode,consignor_id,
	addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,fix_adv_amount,dsl_consumption,
	start_poi_date,end_poi_date,polyline,driver_naame,driver_gps_naame,gps_naame_txn_id 
	FROM dairy.trip WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1");

	if(!$select_last_trip){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($select_last_trip)==0)
	{
		$flag_check = false;
		errorLog("Last trip not found for running hisab. Trip No : $trip_no. Truck No : $tno.",$conn,$page_name,__LINE__);
	}

	$row_last = fetchArray($select_last_trip);

	$last_trip_id = $row_last['id'];
	
	$edit_branch1=$row_last['edit_branch'];
	$last_location1=$row_last['to_station'];
	$from_location1=$row_last['from_station']."(".$branch.")";
	$from_id1=$row_last['from_id'];
	$to_id1=$row_last['to_id'];
	$km1=$row_last['km'];
	$pincode1=$row_last['pincode'];
	$act_wt1=$row_last['act_wt'];
	$charge_wt1=$row_last['charge_wt'];
	$lr_type1=$row_last['lr_type'];
	$lr_type21=$row_last['lr_type2'];
	$oxygen_lr1=$row_last['oxygen_lr'];
	$polyline = escapeString($conn,$row_last['polyline']);
	$mb_to_pay1=$row_last['mb_to_pay'];
	$mb_amount1=$row_last['mb_amount'];
	$lrno1=$row_last['lrno'];
	
	$driver_naame_last = $row_last['driver_naame'];
	$driver_gps_naame_last = $row_last['driver_gps_naame'];
	$gps_naame_txn_id_last = $row_last['gps_naame_txn_id'];
	
	$end_date1= $timestamp;
	
	if($lr_type21>0)
	{
		foreach(explode(",",$lr_type1) as $lr_list)
		{
			if(substr($lr_list,3,1)=='M')
			{
				$update_pod_type=Qry($conn,"UPDATE mkt_bilty SET pod_date_crossing=pod_date,pod_date=0 WHERE bilty_no='$lr_list'");

				if(!$update_pod_type){
					$flag_check = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
			else
			{
				$update_pod_type=Qry($conn,"UPDATE freight_form_lr SET pod_rcv_date=pod_rcv_date_own,pod_rcv_date_own=0 WHERE frno='$lr_list'");

				if(!$update_pod_type){
					$flag_check = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
		}
	}
					
	$update_last_loc = $last_location1."(".$branch.")";

	if($gps_naame_txn_id_last!='')
	{
		$update_last_trip = Qry($conn,"UPDATE dairy.trip SET to_station='$update_last_loc',to_poi='$destination',to_pincode='$hisab_loc_pincode',
		pincode='$hisab_loc_pincode',end_date='$timestamp',km='$distance_first',dsl_consumption='$new_dsl_consumption_1',
		driver_naame = driver_naame - 3000,driver_gps_naame='',gps_naame_txn_id='' WHERE id='$row_last[id]'");
	}
	else
	{
		$update_last_trip = Qry($conn,"UPDATE dairy.trip SET to_station='$update_last_loc',to_poi='$destination',to_pincode='$hisab_loc_pincode',
		pincode='$hisab_loc_pincode',end_date='$timestamp',km='$distance_first',dsl_consumption='$new_dsl_consumption_1' WHERE id='$row_last[id]'");
	}
	
	if(!$update_last_trip){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(AffectedRows($conn)==0)
	{
		$flag_check = false;
		errorLog("Unable to update last trip location. Trip No : $trip_no. Truck No : $tno.",$conn,$page_name,__LINE__);
	}

	$copy = Qry($conn,"INSERT INTO dairy.trip_final(trip_id,trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,
	to_station,from_id,to_id,from_poi,from_pincode,to_poi,to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,
	fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,fix_adv_amount,loaded_hisab,km,dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,
	charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,mb_amount,lrno,start_poi_date,end_poi_date,date,end_date,status,dates_pending,freight,cash,
	cheque,rtgs,diesel,diesel_qty,freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,final_exp_tel,final_exp_ag,pod,pod_date,hisab_date,hisab_branch,
	narration,timestamp,supervisor_approval,approval_timestamp,polyline) SELECT id,trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,to_station,from_id,to_id,from_poi,
	from_pincode,to_poi,to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,fix_lane,fix_dsl_type,fix_dsl_value,
	fix_dsl_left,fix_adv_amount,loaded_hisab,km,dsl_consumption,lr_pending,lr_pending_type,pincode,act_wt,charge_wt,lr_type,lr_type2,oxygen_lr,
	mb_to_pay,mb_amount,lrno,start_poi_date,end_poi_date,date,end_date,status,dates_pending,freight,cash,cheque,rtgs,diesel,diesel_qty,
	freight_collected,driver_naame,driver_gps_naame,gps_naame_txn_id,expense,toll_tax,final_exp_tel,final_exp_ag,pod,pod_date,'$timestamp','$branch',narration,timestamp,
	supervisor_approval,approval_timestamp,polyline	
	FROM dairy.trip WHERE trip_no='$trip_no'");

	if(!$copy){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(AffectedRows($conn)==0)
	{
		$flag_check = false;
		errorLog("Zero trip copied. Trip No : $trip_no. Truck No : $tno.",$conn,$page_name,__LINE__);
	}
	
	$delete_trip=Qry($conn,"DELETE FROM dairy.trip WHERE trip_no='$trip_no'");

	if(!$delete_trip){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(AffectedRows($conn)==0) 
	{
		$flag_check = false;
		errorLog("Zero trip Deleted. Trip No : $trip_no. Truck No : $tno.",$conn,$page_name,__LINE__);
	}

	$get_trip_no = GetTripNo($conn);

	if(!$get_trip_no || $get_trip_no=="0" || $get_trip_no=="")
	{
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$trip_no_new = $get_trip_no;
	
	if($d_code_new=="" || $d_code_new==0)
	{
		$driver_last_veh="";
		$driver_code_new="";
	}
	else
	{
		$driver_last_veh=$tno;
		$driver_code_new=$d_code_new;
	} 
	
	$create_new_trip_no = Qry($conn,"INSERT INTO dairy.opening_closing(trip_no,tno,driver,last_active_on,opening,opening_branch,branch_user,
	opening_date,timestamp) VALUES ('$trip_no_new','$tno','$driver_code_new','$driver_last_veh','$next_trip_opening','$branch','$branch_user',
	'$date','$timestamp')");

	if(!$create_new_trip_no){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	// $get_opening_closing_id = getInsertID($conn); 

	$copy_new_trip = Qry($conn,"INSERT INTO dairy.trip(trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,
	to_station,from_id,to_id,from_poi,from_pincode,to_poi,to_pincode,consignor_id,addr_book_id_consignor,consignee_id,consignee_multiple,addr_book_id_consignee,
	fix_lane,fix_dsl_type,fix_dsl_value,fix_dsl_left,fix_adv_amount,dsl_consumption,start_poi_date,end_poi_date,loaded_hisab,km,pincode,act_wt,
	charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,mb_amount,lrno,date,timestamp,polyline) VALUES ('$trip_no_new','$tno','$d_code_new','$branch',
	'$branch_user','$edit_branch1','$from_location1','$last_location1','$from_id1','$to_id1','$destination','$hisab_loc_pincode',
	'$row_last[to_poi]','$row_last[to_pincode]','$row_last[consignor_id]','$row_last[addr_book_id_consignor]','$row_last[consignee_id]',
	'$row_last[consignee_multiple]','$row_last[addr_book_id_consignee]','$row_last[fix_lane]','$row_last[fix_dsl_type]','$row_last[fix_dsl_value]','$row_last[fix_dsl_left]',
	'$row_last[fix_adv_amount]','$new_dsl_consumption_2','$row_last[start_poi_date]','$row_last[end_poi_date]','1','$distance_second','$pincode1',
	'$act_wt1','$charge_wt1','$lr_type1','$lr_type21','$oxygen_lr1','$mb_to_pay1','$mb_amount1','$lrno1','$timestamp','$timestamp','$polyline')");

	if(!$copy_new_trip){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$new_trip_id = getInsertID($conn); 
	
	if($down_driver=="1" AND $gps_naame_txn_id_last!='')
	{
		$flag_check = false;
		errorLog("Driver down not allowed in loaded vehicle. VEDL GPS Case. Trip_No: $trip_no",$conn,$page_name,__LINE__);
	}
	
	if($gps_naame_txn_id_last!='')
	{
		$update_new_trip = Qry($conn,"UPDATE dairy.trip SET driver_naame='$driver_naame_last',driver_gps_naame='$driver_gps_naame_last',
		gps_naame_txn_id='$gps_naame_txn_id_last' WHERE id='$new_trip_id'");
		
		if(!$update_new_trip){
			$flag_check = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_driver_naame_trip_id = Qry($conn,"UPDATE dairy.driver_naame SET trip_id='$new_trip_id' WHERE trans_id='$gps_naame_txn_id_last' 
		AND trip_id='$last_trip_id'");
		
		if(!$update_driver_naame_trip_id){
			$flag_check = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0)
		{
			$flag_check = false;
			errorLog("Unable to update new trip id in trip_exp GPS naame txn record. Trip No : $trip_no. Truck No : $tno. Txnid: $gps_naame_txn_id_last.",$conn,$page_name,__LINE__);
		}
	}
	
	$diesel_balance = $_SESSION['hisab_fix_diesel_value'] - $diesel_qty;
	
	$qry_update_closing=Qry($conn,"UPDATE dairy.opening_closing SET closing='$closing',closing_branch='$branch',
	closing_user='$branch_user',closing_date='$date',closing_timestamp='$timestamp',gps_km_of_trip='$gps_km',
	fix_diesel='$_SESSION[hisab_fix_diesel_value]',diesel_qty='$diesel_qty',diesel_balance='$diesel_balance',avg='$avg',fromstation='$branch',
	tostation='CGROAD',duedate='$date' WHERE trip_no='$trip_no'");

	if(!$qry_update_closing){
		$flag_check = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if($flag_check){
		return true;
	}else{
		return false;
	}
}
?>