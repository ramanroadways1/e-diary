<?php
require("../connect.php");

$timestamp = date("Y-m-d H:i:s");
$ends_on = date("Y-m-d");

if(!isset($_SESSION['diary'])){
	Redirect("Vehicle number not found !","login.php");
	exit();
}	

if($branch=='' || empty($branch)){
	Redirect("Branch not found !","login.php");
	exit();
}

if(empty($_POST['rating']))
{
	echo "<script>alert('Please rate driver..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$end_point_array = escapeString($conn,($_POST['trip_end_point'])); // stoppage point to loc

$trip_end_point = explode("_",$end_point_array)[0].",".explode("_",$end_point_array)[1];
$end_point_pincode = explode("_",$end_point_array)[2];

$tno = escapeString($conn,strtoupper($_SESSION['diary']));
$id = escapeString($conn,$_POST['id']);
$rating = escapeString($conn,$_POST['rating']);
$feedback = escapeString($conn,$_POST['comment']);

$chk_trip = Qry($conn,"SELECT t.tno,t.branch,t.edit_branch,t.driver_code,t.to_station,t.from_id,t.to_id,t.lr_type,t.lr_type2,t.consignee_id,t.to_poi,t.to_pincode,
t.addr_book_id_consignee,t.lr_pending,date(t.date) as start_date,date(t.end_date) as end_date,t.end_date as end_date_timestamp,
t.start_poi_date,t.end_poi_date,l._lat,l._long,l.pincode 
FROM dairy.trip AS t 
LEFT OUTER JOIN station AS l ON l.id=t.to_id 
WHERE t.id='$id'");

if(!$chk_trip){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#end_button_submit').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_trip)==0)
{
	AlertError("Trip not found !");
	echo "<script>$('#end_button_submit').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($chk_trip);

if($branch!=$row_trip['branch'] AND $branch!=$row_trip['edit_branch'])
{
	AlertError("Trip neither created by you nor you are \"edit branch\" of the Trip !");
	echo "<script>alert('Trip not found..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$lr_type = escapeString($conn,$row_trip['lr_type']);
$lr_type2 = escapeString($conn,$row_trip['lr_type2']);
$driver_code = escapeString($conn,$row_trip['driver_code']);
$con2_id = escapeString($conn,$row_trip['consignee_id']);

if($row_trip['tno']!=$tno)
{
	echo "<script>alert('Vehicle not verified..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_trip['lr_pending']==1)
{
	echo "<script>alert('LR Pending for this trip..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_trip['end_date_timestamp']!=0)
{
	echo "<script>alert('Trip is not active or already closed..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_trip['start_poi_date']==0 || $row_trip['start_poi_date']=='')
{
	echo "<script>alert('Trip start date not found.');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_trip['driver_code']==0 || $row_trip['driver_code']=='')
{
	echo "<script>alert('Driver not found..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$lr_count = substr_count($row_trip['lr_type2'],',');

if($lr_count>1)
{
	echo "<script>alert('More than 2 LRs attached in trip..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$trip_end_date = escapeString($conn,$_POST['trip_end_date']);

$update_to_addr_id = "NO";

if($row_trip['end_poi_date']==0 || $row_trip['end_poi_date']=='')
{
	if($trip_end_date==0 || $trip_end_date=='')
	{
		echo "<script>alert('Trip end date not found..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	if(strlen($trip_end_point)<4)
	{
		AlertError("Trip end point not found !");
		echo "<script>
			$('#end_button_submit').attr('disabled',false);
		</script>";
		exit();
	}

	$lat_1 = explode(",",$trip_end_point)[0];
	$long_1 = explode(",",$trip_end_point)[1];
			
	$distance = calculate_distance($row_trip['_lat'],$row_trip['_long'],$lat_1,$long_1);
			
	if($distance>80)
	{
		AlertError('Check to location OR trip end point.<br>Distance is: '.$distance.' KMs');
		echo "<script>$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	if($con2_id!=0)
	{
		if($lr_type2>1)
		{
			foreach(explode(",",$lr_type) as $lr_type1)
			{
				if(substr($lr_type1,3,1)=='M'){
					
				}
				else{
					$vou_no1 = $lr_type1;
				}
			}
		}
		else
		{
			$vou_no1 = $lr_type;
		}
		
		$chk_crossing = Qry($conn,"SELECT crossing,consignee FROM freight_form_lr WHERE frno='$vou_no1' AND con2_id='$con2_id'");
			
		if(!$chk_crossing){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			echo "<script>alert('Error..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
			exit();
		}
		
		if(numRows($chk_crossing)==0)
		{
			$chk_crossing2 = Qry($conn,"SELECT crossing,consignee FROM freight_form_lr WHERE frno='$vou_no1'");
			
			if(!$chk_crossing2){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				echo "<script>alert('Error..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
				exit();
			}
		
			if(numRows($chk_crossing2)==0)
			{
				// HAJOLR230720215 
				errorLog("Voucher not found. Vou_no: $vou_no1. Con2_id: $con2_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
				AlertError("Voucher not found ! Vou_no: $vou_no1. Consignee: $con2_id.");
				echo "<script>$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
				exit();
			}
			
			$row_voucher = fetchArray($chk_crossing2);
		}
		else
		{
			$row_voucher = fetchArray($chk_crossing);
		}
		
		if($row_voucher['crossing']!='YES')
		{
			$chk_addr_book = Qry($conn,"SELECT id,pincode,_lat,_long FROM address_book_consignee WHERE to_id='$row_trip[to_id]' AND consignee='$con2_id'");
			
			if(!$chk_addr_book){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				echo "<script>alert('Error..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
				exit();
			}
			
			if(numRows($chk_addr_book)>0)
			{
				$row_to_poi = fetchArray($chk_addr_book);
			
				$trip_end_point = $row_to_poi['_lat'].",".$row_to_poi['_long'];
				$end_point_pincode = $row_to_poi['pincode'];
				$to_addr_book_id = $row_to_poi['id'];
				
				// AlertError($to_addr_book_id);
			}
			else
			{
				AlertError("Please update unloading point first !<br>Consignee: $row_voucher[consignee]<br>Destination: $row_trip[to_station].");
				echo "<script>$('#end_button_submit').attr('disabled',false);</script>";
				exit();
			}
			
			$update_to_addr_id = "YES";
		}
		else
		{
			if(strlen($end_point_pincode)!=6)
			{
				$end_point_pincode = getZipcode(explode(",",$trip_end_point)[0].",".explode(",",$trip_end_point)[1]);
				
				if(strlen($end_point_pincode)!=6)
				{
					AlertError("Unloading point pincode not found !");
					echo "<script>$('#end_button_submit').attr('disabled',false);</script>";
					exit();
				}
			}
		}
	}
	else
	{
		if(strlen($end_point_pincode)!=6)
		{
			$end_point_pincode = getZipcode(explode(",",$trip_end_point)[0].",".explode(",",$trip_end_point)[1]);
			
			if(strlen($end_point_pincode)!=6)
			{
				AlertError("Unloading point pincode not found !");
				echo "<script>$('#end_button_submit').attr('disabled',false);</script>";
				exit();
			}
		}
	}
}
else
{
	$end_point_pincode = $row_trip['to_pincode'];
	$trip_end_point = $row_trip['to_poi'];
}

if(strlen($end_point_pincode)!=6)
{
	// AlertError("Unloading point pincode not found !");
	echo "<script>$('#end_button_submit').attr('disabled',false);</script>";
	exit();
}

if(strlen($trip_end_point)<4)
{
	AlertError("Trip end point not found !");
	echo "<script>$('#end_button_submit').attr('disabled',false);</script>";
	exit();
}

$get_trip_days_diff = Qry($conn,"SELECT date(end_date) as end_date FROM dairy.trip WHERE tno='$tno' AND id<'$id' ORDER BY id DESC LIMIT 1");

if(!$get_trip_days_diff){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#end_button_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_trip_days_diff)>0)
{
	$this_trip_start = $row_trip['start_date'];	
	$day_diff_1 = fetchArray($get_trip_days_diff);
	
	if(strtotime($ends_on)==strtotime($day_diff_1['end_date']))
	{
		$diff=0; // trip ending same days as previous trip
	}
	else if(strtotime($day_diff_1['end_date'])==strtotime($this_trip_start))
	{
		$diff = ((strtotime($ends_on)- strtotime($this_trip_start))/24/3600);
	}
	else
	{
		$diff = ((strtotime($ends_on)- strtotime($day_diff_1['end_date']))/24/3600); 
	}
}
else
{
	$diff = ((strtotime($ends_on)- strtotime($row_trip['start_date']))/24/3600+1);
}

$tel_ag_amt = $diff*10; // 10 Rs. per day

StartCommit($conn);
$flag = true;

$insert_rating=Qry($conn,"INSERT INTO dairy.driver_rating(userId,trip_id,branch,branch_user,ratingNumber,comments,created,modified) VALUES 
('$driver_code','$id','$branch','$_SESSION[user_code]','$rating','$feedback','$timestamp','$timestamp')");
	
if(!$insert_rating){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(isset($_POST['bilty_narration']))
{
	if($_POST['bilty_narration']=='OTHER'){
		$narr1 = escapeString($conn,($_POST['other_narrtion']));
	}
	else{
		$narr1 = escapeString($conn,($_POST['bilty_narration']));
	}
	
	$recovery_amount = escapeString($conn,($_POST['recovery_amount']));
	$recovery_date = escapeString($conn,($_POST['recovery_date']));
	
	$insert_bilty_nrr = Qry($conn,"INSERT INTO dairy.bilty_narration(bilty_no,bilty_type,trip_no,narration,branch,date,amount,
	recovery_date,timestamp) VALUES ('$lr_type','TO_PAY','$id','$narr1','$branch','$ends_on','$recovery_amount','$recovery_date',
	'$timestamp')");

	if(!$insert_bilty_nrr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
}

if($update_to_addr_id=='YES')
{
	if($lr_type=='EMPTY')
	{
		$update_trip_end = Qry($conn,"UPDATE dairy.trip SET end_poi_date='$trip_end_date',addr_book_id_consignee='$to_addr_book_id',to_poi='$trip_end_point',to_pincode='$end_point_pincode',pincode='$end_point_pincode',
		end_date='$timestamp',status='1',final_exp_tel='$tel_ag_amt',final_exp_ag='$tel_ag_amt',pod='1',pod_date='$timestamp',
		trip_end_user='$_SESSION[user_code]' WHERE id='$id'");

		if(!$update_trip_end){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$update_trip_end=Qry($conn,"UPDATE dairy.trip SET end_poi_date='$trip_end_date',addr_book_id_consignee='$to_addr_book_id',to_poi='$trip_end_point',to_pincode='$end_point_pincode',pincode='$end_point_pincode',
		end_date='$timestamp',status='1',final_exp_tel='$tel_ag_amt',final_exp_ag='$tel_ag_amt',trip_end_user='$_SESSION[user_code]' WHERE id='$id'");

		if(!$update_trip_end){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
else
{
	if($lr_type=='EMPTY')
	{
		$update_trip_end = Qry($conn,"UPDATE dairy.trip SET end_poi_date='$trip_end_date',to_poi='$trip_end_point',to_pincode='$end_point_pincode',pincode='$end_point_pincode',
		end_date='$timestamp',status='1',final_exp_tel='$tel_ag_amt',final_exp_ag='$tel_ag_amt',pod='1',pod_date='$timestamp',
		trip_end_user='$_SESSION[user_code]' WHERE id='$id'");

		if(!$update_trip_end){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$update_trip_end=Qry($conn,"UPDATE dairy.trip SET end_poi_date='$trip_end_date',to_poi='$trip_end_point',to_pincode='$end_point_pincode',pincode='$end_point_pincode',
		end_date='$timestamp',status='1',final_exp_tel='$tel_ag_amt',final_exp_ag='$tel_ag_amt',trip_end_user='$_SESSION[user_code]' WHERE id='$id'");

		if(!$update_trip_end){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

// if($insert_to_poi=='YES')
// {
	// $insert_to_poi_Qry = Qry($conn,"INSERT INTO address_book_consignee(consignee,to_id,pincode,_lat,_long,branch,branch_user,timestamp) 
	// VALUES ('$con2_id','$row_trip[to_id]','$trip_end_pincode','$lat_1','$long_1','$branch','$branch_sub_user','$timestamp')");
				
	// if(!$insert_to_poi_Qry){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// }
	
	// $to_loc_addr_id = getInsertID($conn);
	
	// $update_to_addr_id = Qry($conn,"UPDATE dairy.trip SET addr_book_id_consignee='$to_loc_addr_id' WHERE id='$id'");

	// if(!$update_to_addr_id){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// }
// }
// else
// {
	// $to_pois = $lat_1.",".$long_1;
	
	// if($trip_end_date!=0 AND $trip_end_date!='' AND $trip_end_point!='')
	// {
		// $update_to_addr_id = Qry($conn,"UPDATE dairy.trip SET to_poi='$to_pois' WHERE id='$id'");

		// if(!$update_to_addr_id){
			// $flag = false;
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// }
	// }
// }

$get_avg_rating = Qry($conn,"SELECT count(id) as count,SUM(ratingNumber) as total_ratings FROM dairy.driver_rating WHERE userId='$driver_code' GROUP BY userId");

if(!$get_avg_rating){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$ratingNumber = 0;
$count_d = 0;
	
if(numRows($get_avg_rating)>0)
{
	$row_rating_avg = fetchArray($get_avg_rating);
		
	$ratingNumber = $row_rating_avg['total_ratings'];
	$count_d = $row_rating_avg['count'];
}
	
$average = 0;

if($ratingNumber && $count_d) {
	$average = $ratingNumber/$count_d;
}	
	
$update_new_rating = Qry($conn,"UPDATE dairy.driver SET driver_rating='$average' WHERE code='$driver_code'");

if(!$update_new_rating){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>alert('OK: Trip marked as complete !!');window.location.href='./';</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error while processing request !!');$('#end_button_submit').attr('disabled',true);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>