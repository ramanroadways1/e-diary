<?php
require_once("../connect.php");

$timestamp = date("Y-m-d H:i:s"); 
$date=date("Y-m-d");
$tno = escapeString($conn,$_SESSION['diary']);

// echo "<script>
	// Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Hisab function is on hold !<br>Contact system-admin</font>',});
	// $('#loadicon').fadeOut('slow');
// </script>";
// exit();


echo "<script>$('#carry_forward_btn').attr('disabled',true);</script>";

include("./_hisab_func.php");

$get_cache_data = Qry($conn,"SELECT final_running,trip_no,trishul_card,driver_name,driver_code,down_driver,down_type,left_reason,standby_reason,
standby_reason_other,balance_on_hisab,da_amount,da_from,da_to,salary_amount,sal_from,sal_to,tel_amount,ag_amount,closing,asset_form_naame,
diesel_qty,sys_km,gps_km FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$get_cache_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_cache_data)==0)
{
	errorLog("HISAB NOT FOUND. in Cache. Truck No: $tno.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_cache_data)>1)
{
	errorLog("Multiple hisab found in cache. Truck No: $tno.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_get_cache = fetchArray($get_cache_data);

$trishul_card = $row_get_cache['trishul_card'];
$trip_no = $row_get_cache['trip_no'];
$driver_name = $row_get_cache['driver_name'];
$driver_code = $row_get_cache['driver_code'];
$down_driver = $row_get_cache['down_driver'];
$down_type = $row_get_cache['down_type'];
$balance_on_hisab = $row_get_cache['balance_on_hisab'];
$da_amount = $row_get_cache['da_amount'];
$da_from = $row_get_cache['da_from'];
$da_to = $row_get_cache['da_to'];
$salary_amount = $row_get_cache['salary_amount'];
$sal_from = $row_get_cache['sal_from'];
$sal_to = $row_get_cache['sal_to'];
$tel_amount = $row_get_cache['tel_amount'];
$ag_amount = $row_get_cache['ag_amount'];
$asset_form_naame = $row_get_cache['asset_form_naame'];
$closing_db = $row_get_cache['closing'];
$diesel_qty = $row_get_cache['diesel_qty'];
$sys_km = $row_get_cache['sys_km'];
$gps_km = $row_get_cache['gps_km'];
$final_running = $row_get_cache['final_running']; // final :0  or running trip : 1

if($final_running=="0")
{
	$hisab_type1="1";
	$hisab_type2="FINAL";
}
else
{
	$hisab_type1="0";
	$hisab_type2="RUNNING";
}

if($diesel_qty>0){
	$avg = sprintf("%.2f",$gps_km/$diesel_qty);
}
else{
	$avg=0;
}

$da = $da_amount;
$tel = $tel_amount;
$ag = $ag_amount;

$balance = $balance_on_hisab;
$asset_amount = $asset_form_naame;
 
$salary_from = $sal_from;
$salary_to = $sal_to;

$closing=$closing_db;

if($closing!=($balance-($da+$tel+$ag+$salary_amount)+$asset_amount+$trishul_card))
{
	errorLog("Unable to Verify Amount. Closing : $closing, DA : $da, SAL : $salary_amount, Tel & AG : $tel+$ag, Asset AmountName :$asset_amount.",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","");	
	exit();
}

if($down_driver=="1")
{
	if($down_type=='STAND_BY'){
		$down_type_db=0;
		$down_reason = escapeString($conn,($row_get_cache['standby_reason']));
		
		if($row_get_cache['standby_reason']=='OTHER'){
			$down_reason=escapeString($conn,($row_get_cache['standby_reason_other']));
		}
		else{
			$down_reason=$row_get_cache['standby_reason'];
		}
	}
	else if($down_type=='ON_LEAVE'){
		$down_type_db=1;
		$down_reason="ON_LEAVE";
	}
	else if($down_type=='LEFT_ROUTE' || $down_type=='LEFT'){
		$down_type_db=2;
		$down_reason = escapeString($conn,($row_get_cache['left_reason']));
	}
	else{
		errorLog("Driver down status not found. Driver Code : $driver_code.",$conn,$page_name,__LINE__);
		Redirect("Error while processing your request. Down Status","./");	
		exit();
	}
	
	$d_code_new="";
	$next_trip_opening = "0";
}
else
{
	$d_code_new = $driver_code;
	$next_trip_opening = $closing;
	$down_type_db="";
	$down_reason="";
}

StartCommit($conn);
$flag = true;

if($final_running=="0")
{
	$trip_copy = TripCopy($trip_no,$conn,$timestamp,$branch,$page_name);
	
	if($trip_copy==false){
		$flag = false;
	}
	
	$delete_trips = DeleteTrips($trip_no,$conn,$page_name);

	if($delete_trips==false){
		$flag = false;
	}
	
	$qry_update_closing = UpdateOpeningClosing($trip_no,$conn,$page_name,$date,$branch,$_SESSION['user_code'],$closing,$gps_km,$avg,$diesel_qty,$timestamp);

	if($qry_update_closing==false){
		$flag = false;
	}
}
else
{
	$trip_copy = RunningTripBreakup($down_driver,$conn,$page_name,$tno,$trip_no,$branch,$_SESSION['user_code'],$gps_km,$avg,$diesel_qty,$next_trip_opening,$closing,$d_code_new,$date,$timestamp);

	if($trip_copy==false){
		$flag = false;
	}
}

$balance = $balance+$trishul_card;

if($trishul_card>0)
{
	$qry_trishul_card_cr = TrishulCardCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$balance,$driver_code,$trishul_card,$timestamp);

	if($qry_trishul_card_cr==false){
		$flag = false;
	}
}

$balance = $balance+$asset_amount;

if($asset_amount>0)
{
	$qry_asset_form_cr = AssetFormCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$balance,$driver_code,$asset_amount,$timestamp);

	if($qry_asset_form_cr==false){
		$flag = false;
	}

	$asset_row_id = getInsertID($conn);
}
else
{
	$asset_row_id = "0";
}

if($salary_amount>0)
{ 
	$insert_sal = SalaryCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$timestamp);

	if($insert_sal==false){
		$flag = false;
	}
	
	$balance = $balance-$salary_amount;

	$insert_driver_book_sal = SalaryCreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$balance,$timestamp);

	if($insert_driver_book_sal==false){
		$flag = false;
	}

	$salary_row_id = getInsertID($conn);
}
else
{
	$salary_row_id = "0";
}

if($da>0)
{
	$insert_da = DAcredit($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$timestamp);

	if($insert_da==false){
		$flag = false;
	}
	
	$balance = $balance-$da;

	$insert_da_1 = DAcreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$balance,$timestamp);

	if($insert_da_1==false){
		$flag = false;
	}

	$da_row_id = getInsertID($conn);
}
else
{
	$da_row_id = "0";
}

	$balance = $balance-$tel;
	
	$insert_exp_tel = InsertTelephone($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$tel,$balance,$timestamp);

	if($insert_exp_tel==false){
		$flag = false;
	}
	
	$telephone_row_id = getInsertID($conn);
	
	$balance = $balance - $ag;

	$insert_exp_ag = InsertAirGrease($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$ag,$balance,$timestamp);

	if($insert_exp_ag==false){
		$flag = false;
	}
	
	$air_grease_row_id = getInsertID($conn);
	
	if($balance != $closing)
	{
		$flag = false;
		errorLog("Balance amount not equal to closing amount.",$conn,$page_name,__LINE__);
	}
	
	$closing_balance = $balance;
	
	$insert_carry_fwd= Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,balance,date,branch,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','HISAB-CARRY-FWD','$balance','$date','$branch','$timestamp')");

	if(!$insert_carry_fwd){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_pay_row_id = "0";
	$hisab_cfwd_row_id = getInsertID($conn);
	$hisab_credit_ho_row_id = "0";

$insert_hisab_log = InsertHisabLog($closing_balance,$conn,$page_name,$tno,$trip_no,$hisab_type1,"1",$down_driver,$down_type,0,0,0,0,$driver_code,$branch,$_SESSION['user_code'],$date,$timestamp,$asset_row_id,$salary_row_id,$da_row_id,$telephone_row_id,$air_grease_row_id,$hisab_pay_row_id,$hisab_cfwd_row_id,$hisab_credit_ho_row_id);

if($insert_hisab_log==false){
	$flag = false;
}

$driver_down_qry = DriverDownSet($hisab_type2,$conn,$page_name,$tno,$trip_no,$down_driver,$date,$down_type_db,$balance,$driver_code,$down_type,$down_reason,$branch,$timestamp);

if($driver_down_qry==false){
	$flag = false;
}

$delete_cache = Qry($conn,"DELETE FROM dairy.hisab_cache WHERE tno='$tno'");
if(!$delete_cache)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}

SessionHisabUnset();

$client_secret = md5($trip_no.$tno.date("ymd"));

echo "<form action='../hisab/hisab_view.php' id='myFormHisab' method='POST'>
		<input type='hidden' value='$trip_no' name='trip_no'>
		<input type='hidden' value='$tno' name='tno'>					
		<input type='hidden' value='$client_secret' name='secret_key'>					
	</form>";
	
echo "<script type='text/javascript'>
	alert('Hisab Success.'); 
	$('#myFormHisab')[0].submit();
</script>";
exit();
?>