<?php
require_once '../connect.php'; 

$elem = escapeString($conn,$_POST['elem']);

$hisab_loc_pincode = explode("_",$elem)[2];
 
if(strlen($hisab_loc_pincode)!=6)
{
	$destination = explode("_",$elem)[0].",".explode("_",$elem)[1];
	
	$get_pincode = getZipcode($destination);

	if(strlen($get_pincode)!=6)
	{
		echo "<script>
			$('#hisab_location_pincode_new').val('');
			$('#hisab_location_pincode_new').attr('readonly',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	else
	{
		$hisab_loc_pincode = $get_pincode;
		
		echo "<script>
			$('#hisab_location_pincode_new').val('$hisab_loc_pincode');
			$('#hisab_location_pincode_new').attr('readonly',true);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}
else
{
	echo "<script>
		$('#hisab_location_pincode_new').val('$hisab_loc_pincode');
		$('#hisab_location_pincode_new').attr('readonly',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>