<a style="display:none" id="AssetModalDown" data-toggle="modal" data-target="#AssetFormModalDown"></a>		

<div class="modal fade" id="AssetFormModalDown" style="left: 0;top: 0;background-image:url(../truck_bg.jpg); background-repeat: no-repeat;  position: fixed;width: 100%;height: 100%; 
	background-size: cover;" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
		<?php
		echo "<form action='./".basename($_SERVER['PHP_SELF'])."' id='myFormRefresh' method='POST'>
			<input type='hidden' value='".sha1($trip_no.md5($tno))."' name='KEY'>		
			<input type='hidden' value='$tno' name='tno'>		
			<input type='hidden' value='$trip_no' name='trip_no'>		
		</form>
		";
		
		echo "<script type='text/javascript'>
			function GoBackToHisab()
			{
				document.getElementById('myFormRefresh').submit();
			}
		</script>";
		?>
		
<style type="text/css">
    .form-control:focus {
        border-color: ;
        box-shadow: 0px 0px 0px rgba(0, 0, 0, 0) inset, 0px 0px 0px rgba(0, 0, 0, 0); 
    }
    .form-control{
    	 display: inline !important;
    }
</style>
		
		<button onclick="GoBackToHisab();" type="button" class="btn btn-sm btn-primary">
			<span class="glyphicon glyphicon-arrow-left"></span> GoBack</button>
		<br />
		<br />
          <h4 class="modal-title"><b>ASSET - FORM</h4></b>
			<h5> TRUCK No: <?php echo $tno; ?>, Driver Name: <?php echo $driver_name; ?></h5>
		</div>
		
<script type="text/javascript">
$(document).ready(function (e) {
$("#AssetFormDown").on('submit',(function(e) {
$("#loadicon").show();
$("#asset_submit_down").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./asset_form_save_down.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_asset_down").html(data);
	},
	error: function() 
	{} });}));});
</script>		
	
<script>	
function ItemMinus(code,qty,rate)
{
	var ext_value=$('#asset_qty'+code).val();
	
	var new_value = ext_value-1;
	
	if(new_value<0)
	{
		alert('Min value is : 0');
		
		$('#asset_qty'+code).val(qty);
		$('#asset_qty_box'+code).html(qty);
		
		$('#rate_asset'+code).val(rate);
		$('#rate_asset_box'+code).html('0');
		$('#amount_naame'+code).val('0');
	}
	else
	{
		$('#asset_qty'+code).val(new_value);
		$('#asset_qty_box'+code).html(new_value);
		
		if(rate==0)
		{
			$('#rate_asset'+code).val('0');
			$('#rate_asset_box'+code).html('0');
			$('#amount_naame'+code).val('0');
		}
		else
		{
			var new_rate = Math.round(rate/qty);
			var new_rate_set = Math.round(new_value*new_rate);
			$('#rate_asset'+code).val(new_rate_set);
			$('#rate_asset_box'+code).html(rate-new_rate_set);
			$('#amount_naame'+code).val(rate-new_rate_set);
		}
	}
}

function ItemPlus(code,qty,rate)
{
	var ext_value=$('#asset_qty'+code).val();
	
	var new_value = Number(ext_value)+1;
	
	if(new_value>qty)
	{
		alert('Max value is : '+qty);

		$('#asset_qty'+code).val(qty);
		$('#asset_qty_box'+code).html(qty);
		
		$('#rate_asset'+code).val(rate);
		$('#rate_asset_box'+code).html('0');
		$('#amount_naame'+code).val('0');
	}
	else
	{
		$('#asset_qty'+code).val(new_value);
		$('#asset_qty_box'+code).html(new_value);
	
		if(rate==0)
		{
			$('#rate_asset'+code).val('0');
			$('#rate_asset_box'+code).html('0');
			$('#amount_naame'+code).val('0');
		}
		else
		{
			var new_rate = Math.round(rate/qty);
			var new_rate_set = Math.round(new_value*new_rate);
			$('#rate_asset'+code).val(new_rate_set);
			$('#rate_asset_box'+code).html(Number(rate)-Number(new_rate_set));
			$('#amount_naame'+code).val(Number(rate)-Number(new_rate_set));
		}
	}
}
</script>	
	
<div class="modal-body">

<div id="result_asset_down"></div>

 <form id="AssetFormDown" autocomplete="off">
 
  <input type="hidden" name="page_url" value="<?php echo basename($_SERVER['PHP_SELF']); ?>">
  
	<table class="table table-bordered" style="font-size:15px;">
		<tr>
			<th>सामान </th>
			<th>मात्रा </th>
			<th>सामान जमा करते समय </th>
			<th>रकम नामे </th>
		</tr>
	
	<?php
	mysqli_set_charset($conn, 'utf8');
	
	$qry_asset_item = Qry($conn,"SELECT a.item_code,a.item_qty,a.item_rate,a.item_value_up,i.item_name,i.qty,i.rate,i.doc 
	FROM dairy.asset_form AS a 
	LEFT OUTER JOIN dairy.asset_form_items AS i ON i.code=a.item_code 
	WHERE a.tno='$tno' AND a.driver_code='$driver_code_value'");
	
	if(!$qry_asset_item){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error.","./logout.php");
		exit();
	}
	
	if(numRows($qry_asset_item)>0)
	{
		while($row_asset = fetchArray($qry_asset_item))
		{
			
		if($row_asset['item_value_up']==1)
		{	
			if($row_asset['doc']=='1')
			{
				$item_button="
				<input style='display:none' id='asset_qty$row_asset[item_code]' min='$row_asset[item_qty]' type='number' 
					value='$row_asset[item_qty]' name='item_qty[]' readonly>
				<span id='asset_qty_box$row_asset[item_code]'>$row_asset[item_qty]</span>			
				";
			}
			else
			{
				$item_button="<button id='MinusBtn$row_asset[item_code]' onclick=ItemMinus('$row_asset[item_code]','$row_asset[item_qty]','$row_asset[item_rate]') type='button' class='btn btn-xs btn-warning'> - </button>
				&nbsp; <span id='asset_qty_box$row_asset[item_code]'>$row_asset[item_qty]</span> &nbsp;
				<button id='PlusBtn$row_asset[item_code]' onclick=ItemPlus('$row_asset[item_code]','$row_asset[item_qty]','$row_asset[item_rate]') type='button' class='btn btn-xs btn-success'> + </button>
				<input id='asset_qty$row_asset[item_code]' style='display:none' min='0' type='number' 
				value='$row_asset[item_qty]' name='item_qty[]' readonly>
				";
			}
		}
		else
		{
			$item_button="
			<input style='display:none' id='asset_qty$row_asset[item_code]' min='$row_asset[item_qty]' type='number' 
				value='$row_asset[item_qty]' name='item_qty[]' readonly>
			<span id='asset_qty_box$row_asset[item_code]'>$row_asset[item_qty]</span>			
			";
		}
 
			if($row_asset['doc']=='1')
			{
					if($row_asset['item_value_up']==1)
					{
						$doc_set="OG";
						$item_box = "ORIGINAL
						<input type='hidden' value='1' name='item_sel[]' required>";
					}
					else
					{
						$doc_set="COPY";
						$item_box = "COPY
						<input type='hidden' value='0' name='item_sel[]' required>";
					}
			}
			else
			{
					if($row_asset['item_value_up']==1)
					{
						$doc_set="";
						$item_box = "<font color='green'>ASSET GIVEN</font> <b>($row_asset[item_qty])</b>
						<input type='hidden' value='1' name='item_sel[]' required />
						";
					}
					else
					{
						$doc_set="";
						$item_box = "<font color='red'>NOT GIVEN</font>
						<input type='hidden' value='0' name='item_sel[]' required />
						";
					}
			}
			
			echo "<tr>
			
				<td>
					$row_asset[item_name] ";
					if($doc_set!='')
						{ echo "($doc_set)"; };
					echo"
				</td>
				
				<td>
					$item_box
				</td>
				
				<input type='hidden' value='$row_asset[item_qty]' name='asset_qty_ext[]'>
				<input type='hidden' value='$row_asset[item_rate]' name='asset_rate_ext[]'>
				<input type='hidden' value='$row_asset[qty]' name='asset_qty_db[]'>
				
				<input type='hidden' value='$row_asset[item_code]' name='asset_code[]'>
				
				<td>
					<center>$item_button</center>
				</td>
				
				<input type='hidden' id='rate_asset$row_asset[item_code]' value='$row_asset[item_rate]' name='asset_rate[]'>
				
				<input type='hidden' id='amount_naame$row_asset[item_code]' value='0' name='rate_naame[]'>
				
				<td>
					₹ <span id='rate_asset_box$row_asset[item_code]'>0</span>
				</td>
				
				</tr>
			";
		}
	}
	?>
</table>

<input type="hidden" value="<?php echo $driver_code_value; ?>" name="driver_code">
<input type="hidden" value="<?php echo $trip_no; ?>" name="trip_no">

<p align="center">
	<button type="submit" name="asset_submit_down" class="btn btn-primary" id="asset_submit_down">Submit Asset - Form</button>
</p>

</form>
		</div>
    </div>
  </div>
</div>