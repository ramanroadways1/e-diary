<?php
require_once("../connect.php");
	
$date1 = escapeString($conn,$_POST['from']); // salary from date
$date2 = escapeString($conn,$_POST['to']); // salary to date

$tno = $_SESSION['diary'];

if($date1==0 || empty($date1)){
	errorLog("SALARY from-date not found. Vehicle_no: $tno.",$conn,$page_name,__LINE__);
	Redirect("SALARY from-date not found.","./");
	exit();
}

if($date2==0 || empty($date2)){
	errorLog("SALARY to-date not found. Vehicle_no: $tno.",$conn,$page_name,__LINE__);
	Redirect("SALARY to-date not found.","./");
	exit();
}

$page_name = escapeString($conn,$_POST['page_name']);

$driver_code = $_SESSION['hisab_driver_code'];

$get_driver_salary = Qry($conn,"SELECT salary_amount,salary_type FROM dairy.driver_up WHERE down=0 AND code='$driver_code' 
ORDER BY id DESC LIMIT 1");

if(!$get_driver_salary){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_driver_salary)==0)
{
	errorLog("Unable to fetch driver. Driver Code : $driver_code",$conn,$page_name,__LINE__);
	Redirect("Driver not found.","./");
	exit();
}

$row_salary = fetchArray($get_driver_salary);

if($row_salary['salary_type']=="1"){
	errorLog("Trip wise salary not allowed by salary dates. driver_code: $driver_code.",$conn,$page_name,__LINE__);
	Redirect("Trip wise salary not allowed by salary dates.","./");
	exit();
}

$sal_amount = $row_salary['salary_amount'];

// if($page_name=='final')
// {
	// $salary_pattern = $_POST['salary_pattern']; // for  lockdown manual sal pattern -- remove it after lockdown over
	// $sal_amount = $_POST['sal_amount']; //  Original  // keep it after lockdown over
	// $sal_amount = $salary_pattern; // for lockdown  // remove it after lockdown over
	
	// if($_POST['sal_amount'] ! =$_POST['salary_pattern'])
	// {
		// echo "<script type='text/javascript'>
			// alert('Invalid salary pattern found !');
			// window.location.href='/';
		// </script>";
		// exit();
	// }
// }
// else
// {
	// $sal_amount = $_POST['sal_amount']; //  Original  // keep it after lockdown over
// }

$array1 = date_parse_from_format("Y-m-d", $date1);
$d1 = $array1["day"];
$m1 = $array1["month"];

$array2 = date_parse_from_format("Y-m-d", $date2);
$d2 = $array2["day"];
$m2 = $array2["month"];

if(empty($m2))
{
	errorLog("Salary Month not found ! Vehicle No : $tno.",$conn,$page_name,__LINE__);
	
	echo "<script type='text/javascript'>
			alert('Salary data not found !');
			window.location.href='/';
		</script>";
	exit();
}

$dateyear1 = DateTime::createFromFormat("Y-m-d", $date1);
$y1=  $dateyear1->format("Y");
$dateyear2 = DateTime::createFromFormat("Y-m-d", $date2);
$y2 =  $dateyear2->format("Y");

$daysInMonth1 = cal_days_in_month(CAL_GREGORIAN, $m1, $y1);
$work_days1=$daysInMonth1-$d1+1;
$salary1= ($sal_amount/$daysInMonth1);
$salary = ($sal_amount/$daysInMonth1);
$salary1 = ($salary*$work_days1);

$daysInMonth2 = cal_days_in_month(CAL_GREGORIAN, $m2, $y2);
$work_days2=$d2;
$salary2= ($sal_amount/$daysInMonth2 );
$salary2 = ($work_days2*$salary2);

$ts1 = strtotime($date1);
$ts2 = strtotime($date2);

$year1 = date('Y', $ts1);
$year2 = date('Y', $ts2);

$month1 = date('m', $ts1);
$month2 = date('m', $ts2);

$diff = (($year2 - $year1) * 12) + ($month2 - $month1);

$time1  = strtotime($date1);
$time2  = strtotime($date2);
$my  = date('mY', $time2);

$months = array(date('F', $time1));

while($time1 < $time2)
{
    $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
    if(date('mY', $time1) != $my && ($time1 < $time2))
        $months[] = date('F', $time1);
}

$months[] = date('F', $time2);
    
$count = count($months);

if($diff > 2){ 

$count = count($months)-2;
$salary3 = $sal_amount*$count;
$salary = ($salary1 + $salary2 + $salary3);  

}
else if($diff==0)
{

$date1_ts = strtotime($date1);
$date2_ts = strtotime($date2);
$diff = $date2_ts - $date1_ts;
$days =  round($diff / 86400);
$salary =  (($days+1)/$daysInMonth1)*$sal_amount;
    
}
elseif($diff==1)
{
    $salary = ($salary1 + $salary2);
}
else if($diff==2)
{
	$salary = ($salary1 + $salary2)+$sal_amount;
}

if($page_name=='final')
{
	$sal_cut_off = escapeString($conn,$_POST['sal_cut_off']);

	if($sal_cut_off=='')
	{
		errorLog("Salary deduction record not found ! Vehicle No : $tno.",$conn,$page_name,__LINE__);
		
		echo "<script type='text/javascript'>
				alert('Salary deduction record not found !');
				window.location.href='/';
			</script>";
		exit();
	}
}
else
{
	$sal_cut_off = "NO";
}

if($sal_cut_off!='NO')
{
	$salary_per = round($salary*$sal_cut_off/100);
	$salary = $salary - $salary_per;
	$salary_amount = sprintf('%0.2f',round($salary));
}
else
{
	$salary_amount = sprintf('%0.2f',round($salary));
}
 
$_SESSION['hisab_salary_amount'] = $salary_amount;
$_SESSION['hisab_salary_from_date'] = date("Y-m-d",strtotime($date1));
$_SESSION['hisab_salary_to_date'] = date("Y-m-d",strtotime($date2));

$diff_days = (strtotime($date2)- strtotime($date1))/24/3600+1;

echo "<script>
	$('#salary').val('$salary_amount');
	$('#salary_amount_cache').val('$salary_amount');
	$('#salary_days_html').html('$diff_days days');
	$('#loadicon').fadeOut('slow');
	sum_balance();
</script>"; 
?>