<?php
require_once("../connect.php");

$timestamp = date("Y-m-d H:i:s"); 
$date=date("Y-m-d");
$tno=escapeString($conn,$_SESSION['diary']);

// echo "<script>
	// Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'maroon\'>Hisab function is on hold !<br>Contact system-admin</font>',});
	// $('#loadicon').fadeOut('slow');
// </script>";
// exit();

include("./_hisab_func.php");

echo "<script>$('#pay_hisab_submit_both').attr('disabled',true);</script>";

$get_cache_data = Qry($conn,"SELECT final_running,trip_no,trishul_card,driver_name,driver_code,down_driver,down_type,left_reason,standby_reason,
standby_reason_other,balance_on_hisab,da_amount,da_from,da_to,salary_amount,sal_from,sal_to,tel_amount,ag_amount,closing,asset_form_naame,
diesel_qty,sys_km,gps_km,hisab_location FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$get_cache_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_cache_data)==0)
{
	errorLog("HISAB NOT FOUND. in Cache. Truck No: $tno.",$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_get_cache = fetchArray($get_cache_data);

$hisab_location = $row_get_cache['hisab_location'];
$trishul_card = $row_get_cache['trishul_card'];
$trip_no = $row_get_cache['trip_no'];
$driver_name = $row_get_cache['driver_name'];
$driver_code = $row_get_cache['driver_code'];
$down_driver = $row_get_cache['down_driver'];
$down_type = $row_get_cache['down_type'];
$balance_on_hisab = $row_get_cache['balance_on_hisab'];
$da_amount = $row_get_cache['da_amount'];
$da_from = $row_get_cache['da_from'];
$da_to = $row_get_cache['da_to'];
$salary_amount = $row_get_cache['salary_amount'];
$sal_from = $row_get_cache['sal_from'];
$sal_to = $row_get_cache['sal_to'];
$tel_amount = $row_get_cache['tel_amount'];
$ag_amount = $row_get_cache['ag_amount'];
$asset_form_naame = $row_get_cache['asset_form_naame'];
$closing_db = $row_get_cache['closing'];
$diesel_qty = $row_get_cache['diesel_qty'];
$sys_km = $row_get_cache['sys_km'];
$gps_km = $row_get_cache['gps_km'];
$final_running = $row_get_cache['final_running']; // final :0  or running trip : 1

if($final_running!="0")
{
	echo "<script>
		alert('Error: Carry forward + Pay hisab not allowed for loaded vehicle hisab !');
		window.location.href='./';
	</script>";
	exit();
}

if($final_running=="0")
{
	$hisab_type1="1";
	$hisab_type2="FINAL";
}
else
{
	$hisab_type1="0";
	$hisab_type2="RUNNING";
}

if($diesel_qty>0)
{
	$avg=sprintf("%.2f",$gps_km/$diesel_qty);
}
else
{
	$avg=0;
}

$da = $da_amount;
$tel = $tel_amount;
$ag = $ag_amount;
// $give_salary = $_POST['give_salary'];

$balance = $balance_on_hisab;
$asset_amount = $asset_form_naame;

$salary_from = $sal_from;
$salary_to = $sal_to;

$closing=$closing_db;

if($closing>=0)
{
	errorLog("Closing must be negative value for pay hisab(cfwd+pay).",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

if($closing!=($balance-($da+$tel+$ag+$salary_amount)+$trishul_card+$asset_form_naame))
{
	errorLog("Unable to Verify Amount. Closing : $closing, DA : $da, SAL : $salary_amount, Tel & AG : $tel+$ag, Asset AmountName :$asset_amount.",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

if($down_driver==1)
{
	if($down_type=='STAND_BY'){
		$down_type_db=0;
		$down_reason = escapeString($conn,strtoupper($row_get_cache['standby_reason']));
		
		if($row_get_cache['standby_reason']=='OTHER'){
			$down_reason=escapeString($conn,strtoupper($row_get_cache['standby_reason_other']));
		}
		else{
			$down_reason=$row_get_cache['standby_reason'];
		}
	}
	else if($down_type=='ON_LEAVE'){
		$down_type_db=1;
		$down_reason="ON_LEAVE";
	}
	else if($down_type=='LEFT_ROUTE' || $down_type=='LEFT'){
		$down_type_db=2;
		$down_reason=escapeString($conn,strtoupper($row_get_cache['left_reason']));
	}
	else{
		errorLog("Driver down status not found. Driver Code : $driver_code.",$conn,$page_name,__LINE__);
		Redirect("Error while processing your request. Down Status","./");	
		exit();
	}

	$d_code_new="";
	$next_trip_opening = "0";
}
else
{
	$d_code_new = $driver_code;
	$next_trip_opening = $closing;
	$down_type_db="";
	$down_reason="";
}

$payment_by = escapeString($conn,strtoupper($_POST['payment_by_both']));
$cheque_no = escapeString($conn,strtoupper($_POST['cheque_no_both']));

if($payment_by!='CASH' AND $payment_by!='CHEQUE' AND $payment_by!='RTGS' AND $payment_by!='BOTH')
{
	errorLog("Payment type not verified. $payment_by.",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

$carry_forward = escapeString($conn,($_POST['carry_fwd_amount']));
$pay_amount = escapeString($conn,($_POST['payable_amount']));

if(abs($closing) != ($carry_forward+$pay_amount))
{
	errorLog("Carry Forward: $carry_forward and Pay amount: $pay_amount not matching with closing amount : $closing. DriverCode: $driver_code. Truck No : $tno.",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

if($carry_forward<=0)
{
	errorLog("Carry Forward: $carry_forward is not valid. Truck No : $tno.",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

if($pay_amount<=0)
{
	errorLog("Payment Amount: $pay_amount is not valid. Truck No : $tno.",$conn,$page_name,__LINE__);
	Redirect("Error while processing your request.","./");	
	exit();
}

$cash_pay = 0;
$rtgs_pay = 0;
$cheque_amount = 0;

if($payment_by=='CASH')
{
	if($_POST['cash_pay']>3000)
	{
		errorLog("CASH Limit exceed. Cash: $_POST[cash_pay], Closing: $closing.",$conn,$page_name,__LINE__);
		Redirect("Error while processing your request.","./");	
		exit();
	}
	
	$cash_pay = $pay_amount;
}
else if($payment_by=='CHEQUE')
{
	$cheque_amount = $pay_amount;
}
else if($payment_by=='RTGS')
{
	$rtgs_pay = $pay_amount;
}
else if($payment_by=='BOTH')
{
	$cash_pay = escapeString($conn,($_POST['cash_pay']));
	$rtgs_pay = escapeString($conn,($_POST['rtgs_pay']));

	if(($cash_pay+$rtgs_pay) != $pay_amount)
	{
		errorLog("Payment amount not verified. Type:Both. Cash: $cash_pay, Rtgs: $rtgs_pay. And payment amt: $pay_amount",$conn,$page_name,__LINE__);
		Redirect("Error while processing your request.","./");	
		exit();
	}
	
	if($cash_pay>3000)
	{
		errorLog("CASH Limit exceed. Cash: $cash_pay, Rtgs: $rtgs_pay. Closing: $closing.",$conn,$page_name,__LINE__);
		Redirect("Error while processing your request.","./");	
		exit();
	}
	
	if($cash_pay<=0)
	{
		errorLog("CASH Limit invalid. Cash: $cash_pay, Rtgs: $rtgs_pay. Closing: $closing.",$conn,$page_name,__LINE__);
		Redirect("Error while processing your request.","./");	
		exit();
	}
	
	if($rtgs_pay<=0)
	{
		errorLog("rtgs Limit invalid. Cash: $cash_pay, Rtgs: $rtgs_pay. Closing: $closing.",$conn,$page_name,__LINE__);
		Redirect("Error while processing your request.","./");	
		exit();
	}
}

$get_company = Qry($conn,"SELECT comp FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_company){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($get_company)>0)
{
	$row_comp = fetchArray($get_company);
	$company=$row_comp['comp'];
}
else
{
	errorLog("Company not found.",$conn,$page_name,__LINE__);
	Redirect("Company not found.","./");
	exit();
}

if($company==''){
	errorLog("Company name is EMPTY. TruckNo $tno.",$conn,$page_name,__LINE__);
	Redirect("Company not found.","./");	
	exit();
}

if($rtgs_pay>0)
{
	$chk_ac_details=Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$driver_code'");
	
	if(!$chk_ac_details){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./");
		exit();
	}

	if(numRows($chk_ac_details)>0)
	{
		$row_ac_details=fetchArray($chk_ac_details);
		
		if($row_ac_details['acname']=='' || $row_ac_details['acno']=='' || $row_ac_details['bank']=='' || $row_ac_details['ifsc']=='')
		{	
			Redirect("Driver Account details not updated !","./");	
			exit();
		}
	}
	else
	{
		errorLog("Driver not found in ac details table. Code: $driver_code.",$conn,$page_name,__LINE__);
		Redirect("Driver not found.","./");	
		exit();
	}
}

if($cash_pay>0)
{
	$check_bal=Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");

	if(!$check_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}

	if(numRows($check_bal)==0){
		Redirect("You can not pay Cash.","./");
		exit();
	}

	$row_bal=fetchArray($check_bal);

	if($company=='RRPL' && $row_bal['balance']>=$cash_pay)
	{
	}
	else if($company=='RAMAN_ROADWAYS' && $row_bal['balance2']>=$cash_pay)
	{
	}
	else
	{
		Redirect("INSUFFICIENT Balance in Company : $company.","./");
		exit();
	}
}

if($salary_amount>0)
{
	$nrr="$tno/$driver_name, DA: $da, TEL: $tel, AIR & GREASE : $ag, SALARY: $salary_amount ($salary_from to $salary_to).";
}
else
{
	$nrr="$tno/$driver_name, DA: $da, TEL: $tel, AIR & GREASE : $ag.";
}

$cash_vou_id = "0";
$cheque_vou_id = "0";	
$rtgs_vou_id = "0";

StartCommit($conn);
$flag = true;

if($cash_pay>0)
{
	$check_bal=Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");

	if(!$check_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($check_bal)==0){
		$flag = false;
		errorLog("Branch balance not found.",$conn,$page_name,__LINE__);
	}

	$row_bal=fetchArray($check_bal);

	$rrpl_cash=$row_bal['balance'];
	$rr_cash=$row_bal['balance2'];

	if($company=='RRPL' && $rrpl_cash>=$cash_pay)
	{
		$newbal_cashbook=$rrpl_cash - $cash_pay;
		$debit='debit';			
		$balance_cashbook='balance';
		
	}
	else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$cash_pay)
	{
		$newbal_cashbook=$rr_cash - $cash_pay;
		$debit='debit2';			
		$balance_cashbook='balance2';
	}
	else
	{
		$flag = false;
		errorLog("Company not found. or INSUFFICIENT Balance.",$conn,$page_name,__LINE__);
	}
	
	$get_vou_id=GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
	
	if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$tids = $get_vou_id;

	$update_cashbook=Qry($conn,"INSERT INTO cashbook(user,user_code,date,vou_date,comp,vou_no,vou_type,desct,`$debit`,`$balance_cashbook`,
	timestamp) VALUES ('$branch','$_SESSION[user_code]','$date','$date','$company','$tids','Truck_Voucher','HISAB-PAYMENT : $nrr',
	'$cash_pay','$newbal_cashbook','$timestamp')");

	if(!$update_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$query_update = Qry($conn,"update user set `$balance_cashbook`='$newbal_cashbook' where username='$branch'");

	if(!$query_update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("ZERO ROWS AFFECTED BALANCE UPDATE.",$conn,$page_name,__LINE__);
	}

	$insert_vou=Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,hisab_vou,dest,timestamp) VALUES 
	('$branch','$_SESSION[user_code]','$company','$tids','$date','$date','$tno','$driver_name','$cash_pay','CASH','1','HISAB-PAYMENT : $nrr','$timestamp')");	

	if(!$insert_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$cash_vou_id = getInsertID($conn);
}

if($cheque_amount>0)
{
	if($company=='RRPL'){
		$debit='debit';
	}
	else	{
		$debit='debit2';
	}
	
	$get_vou_id=GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
	
	if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id==""){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$tids=$get_vou_id;
		
	$update_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,chq_no,`$debit`,timestamp) 
	VALUES ('$branch','$tids','$date','$date','$company','Truck_Voucher','HISAB-PAYMENT : $nrr','$cheque_no','$cheque_amount','$timestamp')");
		
	if(!$update_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_chqBook=Qry($conn,"INSERT INTO cheque_book(vou_no,vou_type,pay_type,vou_date,amount,cheq_no,date,branch,timestamp) 
	VALUES ('$tids','Truck_Voucher','Truck_Voucher','$date','$cheque_amount','$cheque_no','$date','$branch','$timestamp')");
		
	if(!$update_chqBook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$insert_vou=Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,hisab_vou,chq_no,dest,timestamp) VALUES 
	('$branch','$_SESSION[user_code]','$company','$tids','$date','$date','$tno','$driver_name','$cheque_amount','CHEQUE','1','$cheque_no','HISAB-PAYMENT : $nrr',
	'$timestamp')");	

	if(!$insert_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$cheque_vou_id=getInsertID($conn);	
}

if($rtgs_pay>0)
{
	$fetch_acs=Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$driver_code'");
	
	if(!$fetch_acs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($fetch_acs)>0)
	{
		$row_ac1=fetchArray($fetch_acs);
		$acname=$row_ac1['acname'];
		$acno=$row_ac1['acno'];
		$bank=$row_ac1['bank'];
		$ifsc=$row_ac1['ifsc'];
	}
	else
	{
		$flag = false;
		errorLog("Driver not found in ac details table.",$conn,$page_name,__LINE__);
	}

	if($company=='RRPL'){
		$debit='debit';		
	}
	else if($company=='RAMAN_ROADWAYS'){
		$debit='debit2';
	}

	$get_vou_id=GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
	
	if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$tids=$get_vou_id;
	
	$insert_vou=Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,hisab_vou,ac_name,ac_no,bank,ifsc,pan,
	dest,timestamp) VALUES ('$branch','$_SESSION[user_code]','$company','$tids','$date','$date','$tno','$driver_name','$rtgs_pay','NEFT','1',
	'$acname','$acno','$bank','$ifsc','','HISAB-PAYMENT : $nrr','$timestamp')");	

	if(!$insert_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$rtgs_vou_id=getInsertID($conn);
		
	$update_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,`$debit`,timestamp) 
	VALUES ('$branch','$tids','$date','$date','$company','Truck_Voucher','HISAB-PAYMENT : $nrr','$rtgs_pay','$timestamp')");

	if(!$update_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL-T",$conn);
		
		if(!$get_Crn || $get_Crn=="0" || $get_Crn==""){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$crnnew=$get_Crn;		
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$get_Crn = GetCRN("RR-T",$conn);
		
		if(!$get_Crn || $get_Crn=="0" || $get_Crn==""){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$crnnew=$get_Crn;
	}

	$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pay_date,fm_date,tno,type,
	crn,timestamp) VALUES ('$tids','$branch','$company','$rtgs_pay','$rtgs_pay','$acname','$acno','$bank','$ifsc','$date',
	'$date','$tno','TRUCK_ADVANCE','$crnnew','$timestamp')");

	if(!$qry_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($cash_vou_id==""){
	$flag = false;
	errorLog("Cash vou id is empty.",$conn,$page_name,__LINE__);
}

if($cheque_vou_id==""){
	$flag = false;
	errorLog("Cheque vou id is empty.",$conn,$page_name,__LINE__);
}

if($rtgs_vou_id==""){
	$flag = false;
	errorLog("RTGS vou id is empty.",$conn,$page_name,__LINE__);
}

$trip_copy = TripCopy($trip_no,$conn,$timestamp,$branch,$page_name);

if($trip_copy==false){
	$flag = false;
}

$delete_trips = DeleteTrips($trip_no,$conn,$page_name);

if($delete_trips==false){
	$flag = false;
}

$carry_forward_amount = -$carry_forward;

$qry_update_closing = UpdateOpeningClosing($trip_no,$conn,$page_name,$date,$branch,$_SESSION['user_code'],$carry_forward_amount,$gps_km,$avg,$diesel_qty,$timestamp);

if($qry_update_closing==false){
	$flag = false;
}

$balance = $balance+$trishul_card;

if($trishul_card>0)
{
	$qry_trishul_card_cr = TrishulCardCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$balance,$driver_code,$trishul_card,$timestamp);

	if($qry_trishul_card_cr==false){
		$flag = false;
	}
}

$balance = $balance+$asset_amount;

if($asset_amount>0)
{ 
	$qry_asset_form_cr = AssetFormCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$balance,$driver_code,$asset_amount,$timestamp);

	if($qry_asset_form_cr==false){
		$flag = false;
	}

	$asset_row_id = getInsertID($conn);
}
else
{
	$asset_row_id = "0";
}

if($salary_amount>0)
{
	$insert_sal = SalaryCredit($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$timestamp);

	if($insert_sal==false){
		$flag = false;
	}
	
	$balance=$balance-$salary_amount;

	$insert_driver_book_sal = SalaryCreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$salary_from,$salary_to,$driver_code,$salary_amount,$balance,$timestamp);

	if($insert_driver_book_sal==false){
		$flag = false;
	}

	$salary_row_id = getInsertID($conn);
}
else
{
	$salary_row_id = "0";
}

if($da>0)
{
	$insert_da = DAcredit($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$timestamp);

	if($insert_da==false){
		$flag = false;
	}
	
	$balance=$balance-$da;

	$insert_da_1 = DAcreditDriverBook($trip_no,$tno,$conn,$page_name,$date,$branch,$da_from,$da_to,$driver_code,$da,$balance,$timestamp);

	if($insert_da_1==false){
		$flag = false;
	}
	
	$da_row_id = getInsertID($conn);
}
else
{
	$da_row_id = "0";
}

	$balance=$balance-$tel;
	
	$insert_exp_tel = InsertTelephone($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$tel,$balance,$timestamp);

	if($insert_exp_tel==false){
		$flag = false;
	}
	
	$telephone_row_id = getInsertID($conn);
	
	$balance = $balance - $ag;

	$insert_exp_ag = InsertAirGrease($trip_no,$tno,$conn,$page_name,$date,$branch,$driver_code,$ag,$balance,$timestamp);

	if($insert_exp_ag==false){
		$flag = false;
	}
	
	$closing_balance = $balance;
	
	$air_grease_row_id = getInsertID($conn);

	$pay_amount_hisab = $pay_amount;

	$carry_forward=($balance)+($pay_amount_hisab);
	
	if($carry_forward_amount!=$carry_forward)
	{
		$flag = false;
		errorLog("CARRY-FWD amount not verified. Previous is : $carry_forward_amount. Final is : $carry_forward.",$conn,$page_name,__LINE__);
	}

	$insert_driver_book11=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,credit,balance,date,branch,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','HISAB-PAID','$pay_amount_hisab','$carry_forward','$date','$branch','$timestamp')");

	if(!$insert_driver_book11)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_pay_row_id = getInsertID($conn);

	$insert_driver_book22=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_no,desct,balance,date,branch,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','HISAB-CARRY-FWD','$carry_forward','$date','$branch','$timestamp')");

	if(!$insert_driver_book22)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_cfwd_row_id = getInsertID($conn); 
	$hisab_credit_ho_row_id = "0";
	
$insert_hisab_log = InsertHisabLog($closing_balance,$conn,$page_name,$tno,$trip_no,$hisab_type1,"3",$down_driver,$down_type,$cash_vou_id,$cheque_vou_id,$rtgs_vou_id,0,$driver_code,$branch,$_SESSION['user_code'],$date,$timestamp,$asset_row_id,$salary_row_id,$da_row_id,$telephone_row_id,$air_grease_row_id,$hisab_pay_row_id,$hisab_cfwd_row_id,$hisab_credit_ho_row_id);

if($insert_hisab_log==false){
	$flag = false;
}

$driver_down_qry = DriverDownSet($hisab_type2,$conn,$page_name,$tno,$trip_no,$down_driver,$date,$down_type_db,$carry_forward,$driver_code,$down_type,$down_reason,$branch,$timestamp);

if($driver_down_qry==false){
	$flag = false;
}

$delete_cache = Qry($conn,"DELETE FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$delete_cache)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}

SessionHisabUnset();

$client_secret = md5($trip_no.$tno.date("ymd"));

echo "<form action='../hisab/hisab_view.php' id='myFormHisab' method='POST'>
		<input type='hidden' value='$trip_no' name='trip_no'>
		<input type='hidden' value='$tno' name='tno'>					
		<input type='hidden' value='$client_secret' name='secret_key'>					
	</form>";
	
echo "<script type='text/javascript'>
	alert('Hisab Success.'); 
	$('#myFormHisab')[0].submit();
</script>";
exit();
?>