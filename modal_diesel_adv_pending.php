<script type="text/javascript">
$(document).ready(function (e) {
$("#DieselAdvFormPending").on('submit',(function(e) {
$("#loadicon").show();
$("#diesel_adv_button_pending").attr("disabled", true); 
e.preventDefault();
	$.ajax({
	url: "./diesel_adv_save_pending.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result").html(data);
	$("#loadicon").hide();
	$("#diesel_adv_button_pending").attr("disabled", false);
	},
	error: function() 
	{} });}));});
</script>

<script>
  $(function() {  
      $("#card_no_fix").autocomplete({
			source: function(request, response) { 
			if(document.getElementById('fuel_company_fix').value!=''){
                 $.ajax({
                  url: "../b5aY6EZzK52NA8F/autofill/get_diesel_cards.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   company: document.getElementById('fuel_company_fix').value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Please select company first !');
				$("#card_no_fix").val('');
				$("#cardno2_fix").val('');
				$("#phy_card_no_fix").val('');
				$("#rilpre_pending").val('');
              }

              },
              select: function (event, ui) { 
               $('#card_no_fix').val(ui.item.value);   
               $('#cardno2_fix').val(ui.item.dbid);     
			   $('#phy_card_no_fix').val(ui.item.phy_card_no);
			    $("#rilpre_pending").val(ui.item.rilpre);
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#cardno2_fix").val('');
				$("#phy_card_no_fix").val('');
				$("#rilpre_pending").val('');
                alert('Card No. does not exist !'); 
              } 
              },
			}); 
      }); 
</script>

  <div class="modal fade" id="DieselPendingModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
		<form id="DieselAdvFormPending" autocomplete="off">   
		<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-12 bg-primary" style="color:#FFF">
				<h5>Diesel Advance :</h5>
			</div>
		
		<input type="hidden" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			
<script type="text/javascript">
$(function() {
$("#fix_diesel_qty,#rate_fix_diesel").on("keydown keyup blur change input", CalcDieselFix);
function CalcDieselFix() {
	if($("#fix_diesel_qty").val()==''){
		var qty = 0;
	}else{
		var qty = Number($("#fix_diesel_qty").val());
	}

	if($("#rate_fix_diesel").val()==''){
		var rate = 0;
	}else{
		var rate = Number($("#rate_fix_diesel").val());
	}
	
	$("#dsl_amt_fix").val(Math.round(qty * rate).toFixed(2));
}});

function AmountCallFix() 
{
	if($("#fix_diesel_qty").val()=='' && $("#rate_fix_diesel").val()==''){
		$("#fix_diesel_qty").focus()
		$("#dsl_amt_fix").val('');
	}
	else
	{
		if($("#rate_fix_diesel").val()=='' || $("#rate_fix_diesel").val()<60)
		{
			$("#rate_fix_diesel").val((Number($("#dsl_amt_fix").val()) / Number($("#fix_diesel_qty").val())).toFixed(2));
		}
		else
		{
			$("#fix_diesel_qty").val((Number($("#dsl_amt_fix").val()) / Number($("#rate_fix_diesel").val())).toFixed(2));
		}
	}	
}
</script>
			<!--
			<div class="form-group col-md-4">
				<label>Date. <sup><font color="red">*</font></sup></label>
				<input type="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" min="2019-12-03" max="" name="diesel_date" class="form-control" required />
			</div>
			-->
			<div class="form-group col-md-6">
				<label>Pump/Card. <sup><font color="red">*</font></sup></label>
				<select onchange="CardPumpFix(this.value)" id="type1_fix" name="pump_card" class="form-control" required>
					<option value="">Select option</option>
					<option value="CARD">CARD</option>
					<option value="OTP">OTP</option>
					<option value="PUMP">PUMP</option>
				</select>
			</div>
			
			   <script>
			   function ChkPumpInputFix()
			   {
				  if($('#fuel_company_fix').val()=='')
				  {
					  alert('Select Fuel Company First');
					  $('#card_no_fix').val('');
					  $('#mobile_no_fix').val('');
					  $('#phy_card_no_fix').val('');
				  }					  
			   }
			   </script>
			   
			<script> 
			function CardPumpFix(card)
			{
				$('#fuel_company_fix').val('');
				$('#card_no_fix').val("");
				$('#cardno2_fix').val("");
				$('#mobile_no_fix').val("");
				$('#phy_card_no_fix').val("");
				
				$('#rate_fix_diesel').val("");
				$('#fix_diesel_qty').val("");
				$('#dsl_amt_fix').val("");
				
				if(card=='')
				{
					$('#pump_div_fix').hide();	
					$('#pump_name_fix').attr('required',true);	
					$('#pump_company_fix').attr('required',true);
					
					$('#fuel_div_fix').hide();	
					$('#fuel_company_fix').attr('required',true);	
					
					$('#card_div_fix').hide();	
					$('#card_no_fix').attr('required',true);	
					$('#cardno2_fix').attr('required',true);	
					
					$('#mobile_div_fix').hide();	
					$('#mobile_no_fix').attr('required',true);	
					
					$('#phy_card_div_fix').hide();	
			        $('#phy_card_no_fix').attr('required',false);
					
					$('#bill_no_div_fix').hide();	
			        $('#bill_no_fix').attr('required',false);
					
				}
				else if(card=='CARD' || card=='OTP')
				{
					if(card=='CARD')
					{
						$('#mobile_div_fix').hide();
						$('#mobile_no_fix').attr("required",false);
						
						$('#card_div_fix').show();
						$('#card_no_fix').attr("required",true);
						$('#cardno2_fix').attr("required",true);
						
						$('#phy_card_div_fix').show();	
						$('#phy_card_no_fix').attr('required',true);
					}
					else
					{
						$('#mobile_div_fix').show();
						$('#mobile_no_fix').attr("required",true);
						
						$('#card_div_fix').hide();
						$('#card_no_fix').attr("required",false);
						$('#cardno2_fix').attr("required",false);
						
						$('#phy_card_div_fix').hide();	
						$('#phy_card_no_fix').attr('required',false);
					}
					
					$('#pump_div_fix').hide();	
					$('#pump_name_fix').attr('required',false);	
					$('#pump_company_fix').attr('required',false);
					
					$('#fuel_div_fix').show();	
					$('#fuel_company_fix').attr('required',true);	
					
					$('#bill_no_div_fix').hide();	
			        $('#bill_no_fix').attr('required',false);
					 
				}
				else if(card=='PUMP')
				{
					$('#pump_div_fix').show();	
					$('#pump_name_fix').attr('required',true);	
					$('#pump_company_fix').attr('required',true);
					
					$('#fuel_div_fix').hide();	
					$('#fuel_company_fix').attr('required',false);	
					
					$('#card_div_fix').hide();	
					$('#card_no_fix').attr('required',false);	
					$('#cardno2_fix').attr('required',false);	
					
					$('#mobile_div_fix').hide();	
					$('#mobile_no_fix').attr('required',false);	
					
					$('#phy_card_div_fix').hide();	
					$('#phy_card_no_fix').attr('required',false);
					
					$('#bill_no_div_fix').show();	
			        $('#bill_no_fix').attr('required',true);
				}
			}
			</script>
			
			<div style="display:none" id="pump_div_fix" class="form-group col-md-6">
				<label>Pump Name. <sup><font color="red">*</font></sup></label>
				<select onchange="SetPumpCodeFix(this.value)" id="pump_name_fix" name="pump_name" class="form-control" required>
					<option value="">Select option</option>
					<?php
					$q_pump_fix=Qry($conn,"SELECT name,code,comp,consumer_pump FROM dairy.diesel_pump_own WHERE branch='$branch' AND 
					active='1'");
					
					if(!$q_pump_fix){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						exit();
					}

					if(numRows($q_pump_fix)>0)
					{
						while($row_p_fix=fetchArray($q_pump_fix))
						{
							// if($row_p['consumer_pump']=="1")
							// {
								// echo "<option disabled='disabled' value='".$row_p['code']."-".$row_p['comp']."'>$row_p[name]</option>";	
							// }
							// else							
							// {
								echo "<option value='".$row_p_fix['code']."-".$row_p_fix['comp']."'>$row_p_fix[name]</option>";	
							// }
						}
					}
					?>
				</select>
			</div>
			
			<script>
			function SetPumpCodeFix(code)
			{
				$('#rate_fix_diesel').val("");
				$('#dsl_amt_fix').val("");
				$('#fix_diesel_qty').val("");
				
				var parts = code.split('-', 2);
				$('#pump_company_fix').val(parts[1]);
				
				$("#loadicon").show();
				$.ajax({
					url: "get_consumer_pump_data.php",
					method: "post",
					data:'code=' + parts[0] + '&pending_diesel_page=' + 'OK', 
					success: function(data){
					$("#resultChkPump_2").html(data);
				}})
			}
			</script>
			
			<input type="hidden" name="pump_company" id="pump_company_fix">
			
			<div class="form-group col-md-6">
				<label>QTY. <sup><font color="red">* <span id="fix_max_qty"></span></font></sup></label>
				<input type="number" max="0" min="1" name="qty" step="any" required id="fix_diesel_qty" class="form-control" />
			</div>
			
			<div class="form-group col-md-6">
				<label>Rate. <sup><font color="red">*</font></sup></label>
				<input type="number" min="1" max="100" name="rate" step="any" required id="rate_fix_diesel" class="form-control" />
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input min="1" max="70000" type="number" oninput="AmountCallFix();" name="amount" id="dsl_amt_fix" class="form-control" required />
			</div>
			
			
			<div id="fuel_div_fix" style="display:none" class="form-group col-md-6">
				<label>Fuel Company. <sup><font color="red">*</font></sup></label>
				<select id="fuel_company_fix" name="fuel_company" onchange="$('#card_no_fix').val('');$('#phy_card_no_fix').val('')" class="form-control" required>
					<option value="">Select option</option>
					<option disabled value="HPCL">HPCL</option>
					<option value="BPCL">BPCL</option>
					<option value="IOCL">IOCL</option>
					<option value="RELIANCE">RELIANCE</option>
					<option value="RIL">RIL</option>
				</select>
			</div>
			
			 <div style="display:none" id="card_div_fix" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Card No. <font color="red"><sup>*</sup></font></label>
					   <input type="text" oninput="ChkPumpInputFix();this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" name="card_no" id="card_no_fix" class="form-control" required />
					   <input type="hidden" id="cardno2_fix" name="cardno2">
                  </div>
               </div>
			   
			   <div style="display:none" id="mobile_div_fix" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Mobile No. <font color="red"><sup>*</sup></font></label>
					   <input type="text" maxlength="10" minlength="10" oninput="ChkPumpInputFix();this.value=this.value.replace(/[^0-9]/,'')" name="mobile_no" id="mobile_no_fix" class="form-control" required />
				  </div>
               </div>
			   
			    <div style="display:none" id="phy_card_div_fix" style="display:none" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Virtual Id. <font color="red"><sup>*</sup></font></label>
					   <input type="text" name="phy_card_no" id="phy_card_no_fix" class="form-control" readonly required />
				  </div>
               </div>
			   
			    <div style="display:none" id="bill_no_div_fix" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Bill/Slip No. <font color="red"><sup>*</sup></font></label>
					   <input type="text" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^A-Za-z 0-9,-]/,'')" name="bill_no" id="bill_no_fix" class="form-control" required />
				  </div>
               </div>
			
			<input type="hidden" name="date" value="<?php echo date("Y-m-d"); ?>" class="form-control" readonly required />
			
			<input type="hidden" name="trip_id" class="trip_id_set" />
			<input type="hidden" name="trip_no" class="trip_no_set" />
			<input type="hidden" name="driver_name" class="driver_name_set" />
			<input type="hidden" name="driver_code" value="<?php echo $driver_code_value; ?>" />
			 <input type="hidden" name="rilpre" id="rilpre_pending">
			
		</div>
        </div>
		
		<div id="resultChkPump_2"></div>
		
        <div class="modal-footer">
			<button <?php if($driver_code_value=="") {echo "disabled"; } ?> type="submit" id="diesel_adv_button_pending" class="btn btn-danger">Submit</button>
		<button type="button" class="btn btn-primary" id="hide_diesel_pending" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>
  
<!------------------ Diesel Advance Modal ENDSSSSSSSS --------------------------> 