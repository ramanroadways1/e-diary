<?php
require_once("./connect.php");

$tno = escapeString($conn,strtoupper($_SESSION['diary']));
$tno1 = escapeString($conn,strtoupper($_POST['tno']));
$timestamp = date("Y-m-d H:i:s");

if($tno!=$tno1)
{
	AlertError("Vehicle not verified..");
	errorLog("Vehicle not verified. $tno1 and $tno.",$conn,$page_name,__LINE__);
	echo "<script>$('#asset_update').attr('disabled',false);</script>";
	exit();
}

$get_driver = Qry($conn,"SELECT d.driver_code,t.trip_no 
FROM dairy.own_truck AS d 
LEFT JOIN dairy.trip AS t ON t.tno = d.tno
WHERE d.tno='$tno'");

if(!$get_driver){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#asset_update').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_driver)==0)
{
	AlertError("Error..");
	echo "<script>$('#asset_update').attr('disabled',false);</script>";
	exit();
}

$row_driver = fetchArray($get_driver);

if($row_driver['driver_code']==0 || $row_driver['driver_code']=='')
{
	AlertError("Driver not found..");
	echo "<script>$('#asset_update').attr('disabled',false);</script>";
	exit();
}

$driver_code = $row_driver['driver_code'];
$trip_no = $row_driver['trip_no'];

$chk_form_down = Qry($conn,"SELECT asset_form_down FROM dairy.driver_up WHERE code='$driver_code' AND down=0 ORDER BY id DESC LIMIT 1");

if(!$chk_form_down){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#asset_update').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_form_down)==0)
{
	AlertError("Active driver not found..");
	echo "<script>$('#asset_update').attr('disabled',false);</script>";
	exit();
}

$row_chk_down = fetchArray($chk_form_down);

if($row_chk_down['asset_form_down']!="0")
{
	AlertError("Unable to update asset-form !!");
	echo "<script>$('#asset_update').attr('disabled',false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;
$flag_updated = true;

foreach($_POST['item_code'] as $key=>$value)
{
    $item_code = escapeString($conn,strtoupper($_POST['item_code'][$key]));
    $item_sel = escapeString($conn,strtoupper($_POST['item_sel'][$item_code]));
    $item_qty = escapeString($conn,strtoupper($_POST['item_qty'][$key]));
    $asset_rate = escapeString($conn,strtoupper($_POST['asset_rate'][$key]));
	
    $old_qty = escapeString($conn,strtoupper($_POST['old_qty'][$key]));
    $old_up_value = escapeString($conn,strtoupper($_POST['old_up_value'][$key]));
	
	$update = Qry($conn,"UPDATE dairy.asset_form SET item_qty='$item_qty',item_rate='$asset_rate',item_value_up='$item_sel' 
	WHERE driver_code='$driver_code' AND item_code='$item_code'");
	
	if(!$update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)>0)
	{
		$insert_log=Qry($conn,"INSERT INTO dairy.log_asset_form_update(item_code,qty_old,qty_new,given_old,given_new,branch,tno,trip_no,
		driver_code,timestamp) VALUES ('$item_code','$old_qty','$item_qty','$old_up_value','$item_sel','$branch','$tno','$trip_no',
		'$driver_code','$timestamp')");
		
		if(!$insert_log){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$flag_updated = false;
	}
}

if($flag_updated)
{
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		Redirect("ASSET FORM SUCCESSFULLY UPDATED.","./");
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		AlertError("Error while processing request..");
		echo "<script>$('#asset_update').attr('disabled',false);</script>";
		exit();
	}
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Nothing to update..");
	echo "<script>$('#asset_update').attr('disabled',false);</script>";
	exit();
}
?>