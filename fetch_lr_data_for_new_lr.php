<?php
require('./connect.php');

$timestamp=date("Y-m-d H:i:s");

if(!isset($_SESSION['diary'])){
	Redirect("","./login.php");
	exit();
}

if($branch==''){
	Redirect("","./login.php");
	exit();
}

echo '<script>
	$("#from_stn_new").val("");
	$("#to_stn_new").val("");
	$("#actual_new").val("");
	$("#charge_new").val("");
	$("#lrno_new").val("");
	$("#lr_pending_verify_btn").attr("disabled",true);
</script>';

$tno = escapeString($conn,$_SESSION['diary']);
$id = escapeString($conn,$_POST['trip_id']);

$get_trip=Qry($conn,"SELECT tno,from_station,to_station,from_id,to_id,from_poi,from_pincode,to_poi,to_pincode,addr_book_id_consignor,
lr_pending_type FROM dairy.trip WHERE id='$id'");

if(!$get_trip){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error.","./");
	exit();
}

if(numRows($get_trip)==0)
{
	echo "<script>alert('Trip not found..');window.location.href='./';</script>";
	exit();
}

$row_trip = fetchArray($get_trip);

if($row_trip['tno']!=$tno)
{
	errorLog("Vehicle number not verified. SessionSet: $tno and Trip_Veh: $row_trip[tno]. Trip_id: $id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		window.location.href='./';
	</script>";
	exit();
}

if($row_trip['lr_pending_type']=="1")
{
	$type="OWN";
}
else if($row_trip['lr_pending_type']=="2")
{
	$type="MKT";
}
else
{
	errorLog("Invalid LR pending type: $row_trip[lr_pending_type]. Trip_id: $id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		window.location.href='./';
	</script>";
	exit();
}
	
if(empty($_POST['frno']))
{
	errorLog("Voucher not found--> $_POST[frno].",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		window.location.href='./';
	</script>";
	exit();
}
	
if($type=='OWN')
{
	$frno=escapeString($conn,strtoupper($_POST['frno']));
	
	$array_1 = explode('_', $frno);

	$frno=$array_1[0];
	$frno_id=$array_1[1];
	$con1_id=$array_1[2];
	$con2_id=$array_1[3];
	
	$vou_id=$frno_id;
	
	$fetch=Qry($conn,"SELECT fstation,tstation,from_id,to_id,con1_id FROM freight_form_lr WHERE id='$frno_id'");
	
	if(!$fetch){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		Redirect("Error.","./");
		exit();
	}
		
	$fetch2=Qry($conn,"SELECT GROUP_CONCAT(lrno SEPARATOR ',') as lrno,SUM(wt12) as act,SUM(weight) as charge FROM freight_form_lr 
	WHERE frno='$frno'");
		
	if(!$fetch2){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		Redirect("Error.","./");
		exit();
	}
	
	$row = fetchArray($fetch);
	$row2 = fetchArray($fetch2);

	if($row['con1_id']!=$con1_id)
	{
		AlertRightCornerError("Consignor not matching with LR !");
		echo "<script>
			$('#bilty_number_new').val('');
			$('#lr_pending_verify_btn').attr('disabled',true);
		</script>";
		exit();
	}	
	
	$lrnos = $row2['lrno'];
	$act = $row2['act'];
	$charge = $row2['charge'];
	$from=$row['fstation'];
	$from_id=$row['from_id'];
	$to=$row['tstation'];
	$to_id=$row['to_id'];
	$billing_type='';
	$freight=0;
	$lr_type2=$frno;
}
else if($type=='MKT')
{
	$mbno=escapeString($conn,strtoupper($_POST['frno']));
	
	$array_1 = explode('_', $mbno);

	$mbno=$array_1[0];
	$mb_id=$array_1[1];
	$con2_id=0;
	$con1_id=0;
	
	$vou_id=$mb_id;
	
	$qq=Qry($conn,"SELECT billing_type,frmstn,tostn,awt,cwt,tamt,from_id,to_id FROM mkt_bilty WHERE id='$mb_id'");
	
	if(!$qq){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		Redirect("Error.","./");
		exit();
	}
		
		$row=fetchArray($qq);
		
		$from=$row['frmstn'];
		$from_id=$row['from_id'];
		$to=$row['tostn'];
		$to_id=$row['to_id'];
		$billing_type=$row['billing_type'];
		$freight=$row['tamt'];
		$act=sprintf('%0.2f',$row['awt']);
		$charge=sprintf('%0.2f',$row['cwt']);
		$lrnos=$mbno;
		$lr_type2=$mbno;
}
else
{
	Redirect("Invalid LR Type.","./");
	exit();
}
	
if($from_id!=$row_trip['from_id'])
{
	AlertRightCornerError("Trip\'s from location is: $row_trip[from_station]<br>&<br>LR\'s from location is: $from.");
	
	echo '<script>
		// alert("********** Location Error : **********\nTrip\'s from location is : '.$row_trip['from_station'].'\nand\nLR\'s from location is : '.$from.'");
		$("#bilty_number_new").val("");
		$("#lr_pending_verify_btn").attr("disabled",true);
	</script>';
	exit();
}

if($to_id!=$row_trip['to_id'])
{
	AlertRightCornerError("Trip\'s to location is: $row_trip[to_station]<br>&<br>LR\'s to location is: $to.");
	
	echo '<script>
		// alert("********** Location Error : **********\nTrip\'s to location is : '.$row_trip['to_station'].'\nand\nLR\'s to location is : '.$to.'");
		$("#bilty_number_new").val("");
		$("#lr_pending_verify_btn").attr("disabled",true);
	</script>';
	exit();
}

$get_nxt_trip=Qry($conn,"SELECT from_station,from_id FROM dairy.trip WHERE id>'$id' AND tno='$tno' ORDER BY id ASC LIMIT 1");

if(!$get_nxt_trip){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error.","./");
	exit();
}

if(numRows($get_nxt_trip)>0)
{
	$row_nxt = fetchArray($get_nxt_trip);
	
	if($row_nxt['from_id']!=$to_id)
	{
		AlertRightCornerError("Next Trip\'s from location is: $row_nxt[from_station]<br>&<br>LR\'s to location is: $to.");
		
		echo '<script>
			// alert("********** Location Error : **********\nNext Trip\'s from location is : '.$row_nxt['from_station'].'\nand\nLR\'s to location is : '.$to.'");
			$("#bilty_number_new").val("");
			$("#lr_pending_verify_btn").attr("disabled",true);
		</script>';
		exit();
	}
}

// if($con1_id!=0)
// {
	// if($row_trip['addr_book_id_consignor']==0)
	// {
		// AlertError("Trip start point not found !");
		// echo "<script>$('#bilty_number_new').val('');$('#lr_pending_verify_btn').attr('disabled',true);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
	// }
// }
// else
// {
	if($row_trip['from_poi']=='')
	{
		AlertError("Trip start point not found.<br>Code : 2");
		echo "<script>$('#bilty_number_new').val('');$('#lr_pending_verify_btn').attr('disabled',true);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
	}
// }

echo "<script>
		$('#from_stn_new').val('$from');
		$('#to_stn_new').val('$to');
		$('#actual_new').val('$act');
		$('#charge_new').val('$charge');
		$('#lrno_new').val('$lrnos');
		
		// $('.bilty_sel_values').attr('disabled',true);
		// $('#bilty_sel_values_$vou_id').attr('disabled',false);
		
		$('#loadicon').hide();
		$('#lr_pending_verify_btn').attr('disabled',false);
	</script>";

?>