<?php
require("./connect.php");

$distance = escapeString($conn,($_POST['distance']));
$avg_value = escapeString($conn,($_POST['avg_value']));

if(!is_numeric($distance))
{
	AlertError("Invalid distance : $distance !");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
	exit();
}
	
// if($distance<=1)
// {
	// AlertError("Invalid distance : $distance !");
	// echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
	// exit();
// }

if($avg_value<=0)
{
	AlertError("Invalid Average : $avg_value !");
	echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',false);</script>";
	exit();
}

$_SESSION['create_trip_km'] = $distance;
$_SESSION['create_trip_diesel_required'] = sprintf("%.2f",($distance/$avg_value));
echo "<script>$('#create_trip_dsl_qty_required').val('".$_SESSION['create_trip_diesel_required']."')</script>";
?>