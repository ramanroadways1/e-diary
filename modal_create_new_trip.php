<style>
label{
	font-size:12px !important;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
$("#TripForm").on('submit',(function(e) {
$("#loadicon").show();
$("#trip_sub").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./create_trip.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
$(function() {
		$("#from_stn").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from_stn').val(ui.item.value);   
            $('#from_stn_id_create_trip').val(ui.item.id); 
			return false;
		},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from_stn').val("");   
			$('#from_stn_id_create_trip').val(""); 
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to_stn").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to_stn').val(ui.item.value);   
            $('#to_stn_id_create_trip').val(ui.item.id);     
			return false;
		},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_stn').val("");   
			$('#to_stn_id_create_trip').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<form style="font-size:13px" id="TripForm" autocomplete="off">  
<div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
		<div class="modal-header bg-primary">
			<span style="font-size:15px">Create new Trip :</span>
		</div>
	<div class="modal-body">
		<div class="row">
		<div class="form-group col-md-8">
	
	<div class="row">
		<input type="hidden" name="tno" value="<?php echo $tno; ?>" class="form-control" required />
			
		<div class="form-group col-md-4">
			<label>LR Type <sup><font color="red">*</font></sup></label>
			<select style="font-size:13px" name="lr_type" class="form-control" id="lr_type" onchange="LRType(this.value)" required>
				<option  style="font-size:13px" value="">Select an option</option>
				<option  style="font-size:13px" value="OWN">Own Bilty</option>
				<option  style="font-size:13px" value="MKT">Market Bilty</option>
				<option  style="font-size:13px" value="EMPTY">EMPTY</option>
				<option  style="font-size:13px" value="CON20">EMPTY-CONTAINER : 20</option>
				<option  style="font-size:13px" value="CON40">EMPTY-CONTAINER : 40</option>
			</select>
		</div>
		
<script>
function LRType(lr)
{
	editBranchReset();
	$('#trip_sub').attr('disabled',true);
				
	if(lr=='')
	{
		$('#actual').attr('readonly',true);
		$('#charge').attr('readonly',true);
		$('#lrno').attr('readonly',true);
		$('#to_stn').attr('readonly',true);
		
		$('#actual').val('');
		$('#charge').val('');
		$('#lrno').val('');
		$('#to_stn').val('');
		$('#lr_type_div').html('<label>Voucher number <sup><font color="red">*</font></sup></label><input type="text" readonly value="" class="form-control" required />');
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./fetch_lr_type.php",
			data: 'type=' + lr,
			type: "POST",
			success: function(data) {
				$("#lr_type_div").html(data);
			},
			error: function() {}
		});
	}
}
</script>
			
<script>
function FetchLRDataFromLR(frno,type)
{
	editBranchReset();
	$('#trip_sub').attr('disabled',true);
	
	if(frno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "./fetch_lr_data.php",
		data: 'frno=' + frno + '&type=' + type,
		type: "POST",
		success: function(data) {
			$("#lr_result").html(data);
		},
		error: function() {}
		});
	}
}
</script>
			
<div id="lr_result"></div>
			
<script>			
function CheckLRTypeValue(box_id)
{
	editBranchReset();
	if($('#lr_type').val()==''){
		$('#'+box_id).val('');
		$('#lr_type').focus();
	}
	else if($('#lr_type').val()=='OWN')
	{
		if($('#lr_type_own').val()==''){
			$('#'+box_id).val('');
			$('#lr_type_own').focus();
		}
	}
	else if($('#lr_type').val()=='MARKET')
	{
		if($('#lr_type_mkt').val()==''){
			$('#'+box_id).val('');
			$('#lr_type_mkt').focus();
		}
	}
}
</script>	

	<div class="form-group col-md-4" id="lr_type_div">
		<label>Voucher number <sup><font color="red">*</font></sup></label>
		<input type="text" readonly value="" class="form-control" required />
	</div>
			
	<div class="form-group col-md-2">
		<label>Act Wt. <sup><font color="red">*</font></sup></label>
		<input oninput="CheckLRTypeValue('actual')" type="number" min="0" step="any" id="actual" name="actual" class="form-control" required />
	</div>
			
	<div class="form-group col-md-2">
		<label>Chrg Wt. <sup><font color="red">*</font></sup></label>
		<input oninput="CheckLRTypeValue('charge')" type="number" min="0" step="any" id="charge" name="charge" class="form-control" required />
	</div>
			
	<?php
	if($to_stn=='')
	{
	?>
	<div class="form-group col-md-4">
		<label>From Location <sup><font color="red">*</font></sup></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'');CheckLRTypeValue('from_stn')" name="from" id="from_stn" class="form-control from_station" required />
	</div>
			
	<input type="hidden" id="from_stn_id_create_trip" class="from_loc_id_1" name="from_id" />
	<?php
	}
	else
	{
	?>
	<div class="form-group col-md-4">
		<label>From Location <sup><font color="red">*</font></sup></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" name="from" readonly value="<?php echo $to_stn; ?>" class="form-control from_station" required />
	</div>
			
	<input type="hidden" value="<?php echo $to_stn_id; ?>" class="from_loc_id_1" id="from_stn_id_create_trip" name="from_id">
	<?php
	}
	?>
			
	<div class="form-group col-md-4">
		<label>To Location <sup><font color="red">*</font></sup></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'');CheckLRTypeValue('to_stn')" name="to" id="to_stn" class="form-control" required />
	</div>
			
	<input type="hidden" id="to_stn_id_create_trip" name="to_id">
		
	<div class="form-group col-md-4">
		<label>LR Number <sup><font color="red">*</font></sup></label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9,]/,'');CheckLRTypeValue('lrno')" type="text" name="lrno" id="lrno" class="form-control" required />
	</div>
	
<script>	
function LoadingTripDate(fromdate)
{ 
	var lr_type = $('#lr_type').val();
	
	if(lr_type=='')
	{
		$('#lr_type').focus();
		$('#loading_start_date').val('');
		$('#trip_loading_point').val('').selectpicker('refresh');
	}
	else
	{
		if(lr_type=='OWN' && $('#lr_type_own').val()=='')
		{
			$('#lr_type_own').focus();
			$('#loading_start_date').val('');
			$('#trip_loading_point').val('').selectpicker('refresh');
		}
		else
		{
			if(lr_type=='OWN'){
				var vou_no_olr = $('#lr_type_own').val();
			} else {
				var vou_no_olr = "";
			}
			
			var from_id = $('.from_loc_id_1').val();
			editBranchReset();
			$("#loadicon").show();
			var nextDay = new Date(fromdate);
			nextDay.setDate(nextDay.getDate() + 0);
			var next_day=nextDay.toISOString().slice(0,10);
			$("#unloading__date").attr("min",nextDay.toISOString().slice(0,10));
			$("#unloading__date").val("");
			
			if(from_id=='')
			{
				$('#loading_start_date').val('');
				$('#trip_loading_point').val('').selectpicker('refresh');
				Swal.fire({icon: 'error',html: 'From station not found !',})
			}
			else
			{ 
				jQuery.ajax({
					url: "./_fetch_pois_loading_point.php",
					data: 'date=' + fromdate + '&from_id=' + from_id + '&lr_type=' + lr_type + '&vou_no_olr=' + vou_no_olr,
					type: "POST",
					success: function(data) {
						$("#trip_loading_point").html(data);
					},
					error: function() {}
				});
			}
		}
	}
}

function CheckFromDate(fromdate)
{
	editBranchReset();
	
	if($('#loading_start_date').val()=='')
	{
		$('#loading_start_date').focus();
		$('#unloading__date').val('');
		$('#trip_end_point').val('').selectpicker("refresh");
	}
	else
	{
		var to_id = $('#to_stn_id_create_trip').val();
		var lr_type = $('#lr_type').val();
		
		if(to_id=='')
		{
			$('#unloading__date').val('');
			$('#trip_end_point').val('').selectpicker("refresh");
			Swal.fire({icon: 'error',html: 'Destination location not found !',})
		}
		else
		{
			if(lr_type=='')
			{
				$('#lr_type').focus();
				$('#unloading__date').val('');
				$('#trip_end_point').val('').selectpicker('refresh');
			}
			else
			{
				if(lr_type=='OWN' && $('#lr_type_own').val()=='')
				{
					$('#lr_type_own').focus();
					$('#unloading__date').val('');
					$('#trip_end_point').val('').selectpicker('refresh');
				}
				else
				{
					if(lr_type=='OWN'){
						var vou_no_olr = $('#lr_type_own').val();
					} else {
						var vou_no_olr = "";
					}
			
					$("#loadicon").show();
					jQuery.ajax({ 
						url: "./_fetch_pois_unloading_point.php",
						data: 'date=' + fromdate + '&to_id=' + to_id + '&lr_type=' + lr_type + '&vou_no_olr=' + vou_no_olr,
						type: "POST",
						success: function(data) { 
							$("#trip_end_point").html(data);
						},
						error: function() {}
					});
				}	
			}	
		}	
	}
}
</script>	
			
	<div class="form-group col-md-4"> 
		<label>Trip Start Date <sup><font color="red">*</font></sup></label>
		<input onchange="LoadingTripDate(this.value);" id="loading_start_date" name="trip_start_date" type="date" min="<?php echo date("Y-m-d", strtotime("-15 day")); ?>" max="<?php echo date("Y-m-d"); ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
	</div>
			
	<div class="form-group col-md-4">
		<label>Trip Start Point <sup><font color="red">*</font></sup></label>
		<select style="font-size:12px" onchange="editBranchReset();if($('#loading_start_date').val()==''){$('#loading_start_date').focus();$('#trip_loading_point').val('').selectpicker('refresh');}" data-size="5" data-live-search="true" data-live-search-style="" name="trip_start_point" id="trip_loading_point" class="form-control selectpicker" required>
			<option style="font-size:12px !important" value="">--start/loading point--</option>
		</select>
	</div>
		
<script>			
$(document).on('click','#is_running_trip',function(){
var ckbox = $('#is_running_trip');
editBranchReset();
	
	if (ckbox.is(':checked')) {
		$('#unloading__date').val('');
		$('#trip_end_point').val('').selectpicker("refresh");
		$('#unloading__date').attr('readonly',true);
							
		$('#trip_end_point').prop('disabled', true);
		$('#trip_end_point').selectpicker('refresh');
	}else {
		$('#unloading__date').val('');
		$('#trip_end_point').val('').selectpicker("refresh");
		$('#unloading__date').attr('readonly',false);
							
		$('#trip_end_point').prop('disabled', false);
		$('#trip_end_point').selectpicker('refresh');
	}
});
</script>
			
	<div class="form-group col-md-4">
		<label>Trip End Date <sup><font color="red">*</font></sup> &nbsp; <input style="width:13px" id="is_running_trip" type="checkbox" /> 
		<span style="color:blue;font-size:11px">Running trip</span></label>
		<input onchange="CheckFromDate(this.value)" id="unloading__date" name="trip_end_date" type="date" min="<?php echo date("Y-m-d", strtotime("-15 day")); ?>" max="<?php echo date("Y-m-d"); ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
	</div>
			
	<div class="form-group col-md-4">
		<label>Trip End Point <sup><font color="red">*</font></sup></label>
		<select onchange="editBranchReset();if($('#unloading__date').val()==''){$('#unloading__date').focus();$('#trip_end_point').val('').selectpicker('refresh');}" data-size="5" data-live-search="true" data-live-search-style="" name="trip_end_point" id="trip_end_point" class="form-control selectpicker" required>
			<option style="font-size:12px !important" value="">--end/unloading point--</option>
		</select>
	</div>
			
	<div class="form-group col-md-4">
		<label>Next Edit Branch <sup><font color="red">*</font></sup></label>
		<select style="font-size:12px" data-size="8" autocomplete="off" data-live-search="true" data-live-search-style="" name="next_edit_branch" id="edit_branch" onchange="FetchKm(this.value)" class="form-control selectpicker" required>
			<option style="font-size:12px" value="">Select an option</option>
			<?php
			$branch_name_for_trip = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('DUMMY','HEAD') ORDER BY username ASC");
					
			if(!$branch_name_for_trip){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				exit();
			}
			
			if(numRows($branch_name_for_trip)>0)
			{
				echo "<option style='font-size:12px' value='0'>UNKNOWN</option>";
				while($row_branch_trip=fetchArray($branch_name_for_trip))
				{
					echo "<option style='font-size:12px' value='$row_branch_trip[username]'>$row_branch_trip[username]</option>";
				}
			}
			?>
		</select>
	</div>
	
	<div class="form-group col-md-4">
		<label>Destination Pincode <sup><font color="red">*</font></sup></label>
		<input type="text" readonly name="pincode" id="create_trip_pincode" class="form-control">
	</div>
	 
	<div class="form-group col-md-4"> 
		<label>Fuel Required <sup><font color="red">* (Qty in LTR)</font></sup></label>
		<input type="text" readonly id="create_trip_dsl_qty_required" class="form-control">
	</div>
	
	<?php
	$qry_oxygn_tno = Qry($conn,"SELECT id FROM dairy.oxygn_tno WHERE tno='$tno' AND is_active='1'");
		
	if(!$qry_oxygn_tno){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
		
	if(numRows($qry_oxygn_tno)>0)
	{
	?>
	<script>
	function CheckRouteType(elem)
	{
		var from_loc = $('.from_station').val();
		var to_loc = $('#to_stn').val();
		
		if(elem!='')
		{
			if(from_loc=='' || to_loc=='')
			{
				Swal.fire({icon: 'error',html: 'Enter from location and to location first !',})
				$('#route_type_1').val('');
			}
			else
			{
				if(from_loc=='HAPA' && to_loc=='DELHI')
				{
					
				}
				else
				{
					if(elem=='TRAIN')
					{
						Swal.fire({icon: 'error',html: 'Entered route is not Rail route !',})
						$('#route_type_1').val('');
					}
				}
			}	 
		}	 
	}
	</script>
	
		<div class="form-group col-md-4">
			<label>Route type <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px" onchange="CheckRouteType(this.value)" name="route_type_1" id="route_type_1" class="form-control" required="required">
				<option style="font-size:12px" value="">Select an option</option>
				<option style="font-size:12px" value="ROAD">By Road</option>
				<option style="font-size:12px" value="TRAIN">By Rail</option>
			</select>
		</div>
	<?php
	}
	else
	{
		echo "<input type='hidden' name='route_type_1' id='route_type_1' value=''>";
	}
	?>
		
<script>
function FetchKm()
{
	$('#trip_sub').attr('disabled',true);
			
	var lr_type = $('#lr_type').val();
	var actual_wt = $('#actual').val();
	var charge_wt = $('#charge').val();
	
	if(lr_type=='')
	{
		Swal.fire({icon: 'error',html: 'Select LR type first !!',})
		$('#trip_km').val('');
		editBranchReset();
	}
	else
	{
		if(lr_type=='MKT'){
			var vou_no = $('#lr_type_mkt').val();
			
			if(vou_no==''){
				Swal.fire({icon: 'error',html: 'Select Voucher first !!',})
				$('#trip_km').val('');
				editBranchReset();
				return;
			}
			
		}else if(lr_type=='OWN'){
			var vou_no = $('#lr_type_own').val();
			
			if(vou_no==''){
				Swal.fire({icon: 'error',html: 'Select Voucher first !!',})
				$('#trip_km').val('');
				editBranchReset();
				return;
			}
			
		}else{
			var vou_no = "";
		}
		
		var from1 = $('.from_station').val();
		var to1 = $('#to_stn').val();
		var from_id = $('.from_loc_id_1').val();
		var to_id = $('#to_stn_id_create_trip').val();
		var start_date = $('#loading_start_date').val();
		var end_date = $('#unloading__date').val();
		
		var trip_start_point = $('#trip_loading_point').val();
		var trip_end_point = $('#trip_end_point').val();
		
		var is_running_trip = $('#is_running_trip').is(':checked');
				
		if(from1=='' || to1=='' || from_id=='' || to_id=='')
		{
			Swal.fire({
				icon: 'error',
				html: 'Please enter locations first',
			})
			$('#trip_km').val('');
			editBranchReset();
		}
		else
		{
			$('.from_station').attr('readonly',true);
			$('#to_stn').attr('readonly',true);
							
			$("#loadicon").show();
			jQuery.ajax({
			url: "./validate_create_trip.php",
			data: 'from_id=' + from_id + '&to_id=' + to_id + '&lr_type=' + lr_type + '&vou_no=' + vou_no + '&running_trip=' + is_running_trip + '&start_point=' + trip_start_point + '&end_point=' + trip_end_point + '&from_loc=' + from1 + '&to_loc=' + to1 + '&end_date=' + end_date + '&start_date=' + start_date + '&actual_wt=' + actual_wt + '&charge_wt=' + charge_wt,
			type: "POST",
			success: function(data) {
				$("#map_result").html(data);
			},
			error: function() {}
			});
		}
	}
}
</script>
		
<script>
function FuncKm()
{
	if($('#edit_branch').val()=='')
	{
		$('#trip_km').val('');
		$('#edit_branch').focus();
	}
}
		
function editBranchReset()
{
	$("#edit_branch").val('').selectpicker("refresh");
}
</script>
		
		<div id="map_result"></div>
		
		
		<input type="hidden" id="set_km" name="set_km">
		<input type="hidden" id="at_loading_set" name="at_loading_set">
		
		<!--
		<input type="hidden" id="from_loc_id" name="from_loc_id" />
		<input type="hidden" id="to_loc_id" name="to_loc_id" />
		<input type="hidden" id="from_loc_of_new_trip2" value="<?php echo $to_stn; ?>" name="from_for_new_trip" />
		<input type="hidden" id="from_loc_id_new_trip2" value="<?php echo $to_stn_id; ?>" name="from_id_for_new_trip" />
		-->
		
		<input type="hidden" name="trip_km" id="trip_km">
		<input type="hidden" name="by_pass_route" id="by_pass_route">
		
		<!--<div style="display:none" class="form-group col-md-4">
			<label>Trip KM. <sup><font color="red">*</font></sup></label>
			<input readonly oninput="FuncKm()" type="number" name="trip_km" id="trip_km" class="form-control" required />
		</div>-->
	
	</div>
		
		</div>
		
		<div class="form-group col-md-4">
			<div class="row">
				<div class="col-md-12">
					<div class="map" id="map" style="border:.5px solid #666;margin-top:5px;width:100%;height:280px">
						<img src="map_def.jpg" style="width:100%;max-height:280px !important" />
					</div>
				</div>
				
				<div class="col-md-12">
					<div style="font-size:13px;padding:5px;border-bottom:.5px solid #666;border-left:.5px solid #666;border-right:.5px solid #666" class="bg-success col-md-12">
						<div class="row">
							<div class="col-md-5">
								<font color="maroon">Trip distance:</font> <span id="km_google1" style="font-size:12px">00 km</span> 
							</div>	
							<div class="col-md-7">
								<font color="maroon">Est. time:</font> <span id="duration_google1" style="font-size:12px">NA</span>
							</div>	
						</div>	
					</div>
				</div>
			</div>
		</div>
		
		</div>
        </div>
		<input type="hidden" id="create_trip_encodedPolyLine" name="polyline">
        <div class="modal-footer">
          <button type="submit" style="display:none" disabled id="trip_sub" class="btn btn-sm btn-danger">Create Trip</button>
          <button type="button" id="trip_modal_close" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
</form>