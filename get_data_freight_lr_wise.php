<?php 
require_once './connect.php';

$tno_active=escapeString($conn,$_SESSION['diary']);
$lrno=escapeString($conn,strtoupper($_POST['lrno']));
$lr_type=escapeString($conn,($_POST['lr_type']));
$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s");

$get_active_driver = Qry($conn,"SELECT driver_code FROM dairy.trip WHERE tno='$tno_active'");

if(!$get_active_driver){
	AlertError("Error !!");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
	exit();
}

if(numRows($get_active_driver)==0)
{
	AlertError("Driver not found !!");
	echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
	exit();	
}

$row_active = fetchArray($get_active_driver);
$driver_code = $row_active['driver_code'];

if($driver_code=='' || $driver_code==0)
{
	AlertError("Active driver not found !!");
	echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
	exit();	
}

if($lrno=='' || empty($lrno))
{
	AlertError("Error: LR not found !!");
	echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
	exit();	
}

if($lr_type=='')
{
	AlertError("Select LR type !!");
	echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
	exit();	
}

if($lr_type=='MARKET')
{
	$chk_lr=Qry($conn,"SELECT id,tno,frmstn as from_loc,tostn as to_loc,trip_id FROM mkt_bilty WHERE bilty_no='$lrno'");

	if(!$chk_lr){
		AlertError("Error !!");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();
	}
	
	if(numRows($chk_lr)==0)
	{
		AlertError("Error: LR not found !!");
		echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();	
	}
	
	$row_lr = fetchArray($chk_lr);
	
	$chk_trip_driver_and_veh=Qry($conn,"SELECT tno,driver_code FROM dairy.trip WHERE id='$row_lr[trip_id]'");

	if(!$chk_trip_driver_and_veh){
		AlertError("Error !!");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();
	}
	
	if(numRows($chk_trip_driver_and_veh)==0)
	{
		$chk_trip_driver_and_veh_old_trips = Qry($conn,"SELECT tno,driver_code FROM dairy.trip_final WHERE 
		trip_id='$row_lr[trip_id]'");

		if(!$chk_trip_driver_and_veh_old_trips){
			AlertError("Error !!");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
			exit();
		}
	
		if(numRows($chk_trip_driver_and_veh_old_trips)==0)
		{
			AlertError("Error: Trip not found !!");
			echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
			exit();	
		}
		
		$row_trip = fetchArray($chk_trip_driver_and_veh_old_trips);
	}
	else
	{
		$row_trip = fetchArray($chk_trip_driver_and_veh);
	}
	
	if($row_trip['tno']!=$tno_active)
	{
		AlertError("LR belongs to vehicle: $row_trip[tno] !");
		echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();	
	}
	
	if($row_trip['driver_code']!=$driver_code)
	{
		$get_driver_name = Qry($conn,"SELECT name FROM dairy.driver WHERE code='$row_trip[driver_code]'");

		if(!$get_driver_name){
			AlertError("Error !!");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
			exit();
		}
		
		$row_d_name = fetchArray($get_driver_name);
		
		AlertError("LR belongs to driver: $row_d_name[name] !");
		echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();	
	}
	
	echo "<input type='hidden' name='vou_no2' value='$lrno'>
	<input type='hidden' name='veh_no2' value='$row_lr[tno]'>
	<input type='hidden' name='from_loc2' value='$row_lr[from_loc]'>
	<input type='hidden' name='to_loc2' value='$row_lr[to_loc]'>
	<input type='hidden' name='trip_id_freight' value='$row_lr[trip_id]'>";
	
	echo "<div style='font-size:13px;margin-top:15px;padding:7px;color:maroon' id='freight_lr_sel_result' class='bg-warning form-group col-md-12'>
		>> $row_lr[from_loc] to $row_lr[to_loc] <br>
		>> Vehicle number: $row_lr[tno]
	</div>";
}
else
{
	$chk_lr=Qry($conn,"SELECT id,frno,truck_no as tno,fstation as from_loc,tstation as to_loc,trip_id 
	FROM freight_form_lr WHERE lrno='$lrno'");

	if(!$chk_lr){
		AlertError("Error..");
		echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($chk_lr)==0)
	{
		AlertError("Error: LR not found !");
		echo "<script>$('#freight_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();	
	}
	
	echo "<div class='form-group col-md-12'>
	<label>Select Voucher <font color='red'>*</font></label>
		<select style='font-size:13px' onchange='FreightLRSelection(this.value)' class='form-control' id='freight_lr_vou_type' name='vou_type' required='required'>
			<option value=''>--select lr--</option>";
				
		while($row_lr2 = fetchArray($chk_lr))		
		{
			echo "<option value='$row_lr2[frno]_$row_lr2[tno]_$row_lr2[from_loc]_$row_lr2[to_loc]_$row_lr2[trip_id]'>$row_lr2[frno]-> $row_lr2[tno] : $row_lr2[from_loc] to $row_lr2[to_loc]</option>";
		}
		
	echo "</select>
	</div>";
	
	// echo "<input type='hidden' value='$row_lr2[frno]' id='_freight_lr_frno_$row_lr2[id]'>
	// <input type='hidden' value='$row_lr2[tno]' id='_freight_lr_tno_$row_lr2[id]'>
	// <input type='hidden' value='$row_lr2[from_loc]' id='_freight_lr_from_loc_$row_lr2[id]'>
	// <input type='hidden' value='$row_lr2[to_loc]' id='_freight_lr_to_loc_$row_lr2[id]'>
	// <input type='hidden' value='$row_lr2[trip_id]' id='_freight_lr_trip_id_$row_lr2[id]'>";
	
	echo "<div style='font-size:13px;padding:7px;margin-top:15px;color:maroon' id='freight_lr_sel_result' class='bg-warning form-group col-md-12'>
		No record selected..
	</div>";
	
	echo "<input type='hidden' name='vou_no2' id='freight_vou_no2'>
	<input type='hidden' name='veh_no2' id='freight_veh_no2'>
	<input type='hidden' name='from_loc2' id='freight_from_loc2'>
	<input type='hidden' name='to_loc2' id='freight_to_loc2'>
	<input type='hidden' name='trip_id_freight' id='freight_trip_id'>";
}

echo "<script>
	$('#loadicon').hide();
	$('.freight_lr_type_sel').attr('disabled',true);
	$('.naame_sel_opt').attr('disabled',true);
	$('#naame_sel_opt_freight').attr('disabled',false);
	$('#freight_lrno1').attr('readonly',true);
	$('#freight_check_btn').attr('disabled',true);
	// $('#claim_lr_desc').show();
</script>";
	
if($lr_type=='MARKET')
{
	echo "<script>
		$('#freight_lr_type_MARKET').attr('disabled',false);
		$('#naame_button').attr('disabled',false);
	</script>";
}
else
{
	echo "<script>
		$('#freight_lr_type_OWN').attr('disabled',false);
		$('#naame_button').attr('disabled',true);
	</script>";
}

// StartCommit($conn);
// $flag = true;
	
// $query = Qry($conn,"update user set `$balance_col`='$newbal' where username='$branch'");

// if(!$query){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

// $cashbook_entry = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,`$credit_col`,`$balance_col`,timestamp) VALUES 
// ('$branch','$_SESSION[user_code]','$date','$company','CREDIT ADD BALANCE','$nrr','$amount','$newbal','$timestamp')");

// if(!$cashbook_entry){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

// if($flag)
// {
	// MySQLCommit($conn);
	// closeConnection($conn);
	// echo "<script> 	
		// alert('Amount Credited Successfully !!');
		// window.location.href='./credit.php';
	// </script>";
	// exit();
// }
// else
// {
	// MySQLRollBack($conn);
	// closeConnection($conn);
	// Redirect("Error While Processing Request.","./credit.php");
	// exit();
// }
?>