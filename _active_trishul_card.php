<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$tno = escapeString($conn,strtoupper($_SESSION['diary']));

require_once("./check_cache.php");

$chk_trishul_card = Qry($conn,"SELECT trishul_card,trishul_mapped FROM dairy.own_truck WHERE tno='$tno'");

if(!$chk_trishul_card){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_trishul_card)==0)
{
	echo "<script type='text/javascript'>
			window.location.href='./';
		</script>";
	exit();
}

$row_trishul_card = fetchArray($chk_trishul_card);

if($row_trishul_card['trishul_card']!="1")
{
	echo "<script type='text/javascript'>
		alert('Trishul Card not active on this Vehicle !!');
		window.location.href='./';
	</script>";
	exit();
}

if($row_trishul_card['trishul_mapped']=="1")
{
	echo "<script type='text/javascript'>
		alert('Trishul Card already activated on this Vehicle !!');
		window.location.href='./';
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$update_trishul_card = Qry($conn,"UPDATE dairy.own_truck SET trishul_mapped='1',trishul_mapped_from='$timestamp' 
WHERE tno='$tno'");

if(!$update_trishul_card){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$insert_log = Qry($conn,"INSERT INTO dairy._trishul_card_log(tno,status,trip_no,timestamp) VALUES 
('$tno','Assigned','','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request..");
	exit();
}

echo "<script>
		$('#loadicon').hide();
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: 'Success ! Trishul-card assigned.',
			showConfirmButton: false,
			timer: 2000
		})
		window.location.href='./';
	</script>";
exit();
?>