<?php
require('./connect.php');

if(!isset($_SESSION['diary'])){
	Redirect("","./login.php");
	exit();
}	

// if($_SESSION['user_code']!='032')
// {
	// AlertError("System under maintenance !");
	// echo "<script>$('#loadicon').fadeOut('slow');</script>";
	// exit();
// }

if($branch==''){
	Redirect("","./login.php");
	exit();
}

if(!isset($_SESSION['atloading_trip_diesel_required']))
{
	errorLog("Diesel consumption not found.",$conn,$page_name,__LINE__);
	echo "<script>alert('Diesel value not found.');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$date=date("Y-m-d");
$timestamp = date("Y-m-d H:i:s"); 

$tno = escapeString($conn,strtoupper($_SESSION['diary']));
$trip_id = escapeString($conn,$_SESSION['atloading_trip_id']);
$trip_end_date = escapeString($conn,$_SESSION['atloading_end_date']);
$trip_end_point = escapeString($conn,$_SESSION['atloading_end_point']);
$trip_km = escapeString($conn,$_SESSION['atloading_trip_km']);
$from_addr_id = escapeString($conn,$_SESSION['atloading_from_addr_id']);
$to_addr_id = escapeString($conn,$_SESSION['atloading_to_addr_id']);
$running_trip = escapeString($conn,$_SESSION['atloading_running_trip']); // true or false
$dest_pincode = escapeString($conn,$_SESSION['atloading_dest_pincode']); // destination pincode
$diesel_consumption = $_SESSION['atloading_trip_diesel_required'];
$by_pass_route = $_POST['by_pass_route'];

if($running_trip=='true')
{
	$trip_end_date="0000-00-00";
}

$vou_array = escapeString($conn,$_SESSION['atloading_vou_no']);
$array_1 = explode('_', $vou_array);

$vou_no=$array_1[0];
$vou_id=$array_1[1];
$con1_id=$array_1[2];
$con2_id=$array_1[3];

if(strpos($vou_no,'OLR') !== false){
	$vou_type="OWN";
}
else{
	$vou_type="MARKET";
}
	
$get_trip_data=Qry($conn,"SELECT from_id,to_id,tno,dsl_consumption as diesel_consumption_db FROM dairy.trip WHERE id='$trip_id'");

if(!$get_trip_data){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	AlertError("Error !");
	echo "<script>$('#lr_pending_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

if(numRows($get_trip_data)==0)
{
	errorLog("Trip not found ! id: $trip_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	AlertError("Trip not found !");
	echo "<script>$('#lr_pending_submit').attr('disabled',true);$('#loadicon').fadeOut('slow');</script>";exit();
}

$row_trip = fetchArray($get_trip_data);

$from_id = $row_trip['from_id'];
$to_id = $row_trip['to_id'];

$diesel_consumption_db = $row_trip['diesel_consumption_db'];

if($row_trip['tno']!=$tno)
{
	errorLog("Vehicle not verified with trip ! Trip_id: $trip_id. TripVeh: $row_trip[tno] and Active_Veh: $tno.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>window.location.href='./';$('#loadicon').fadeOut('slow');</script>";exit();
}

if($vou_type=='OWN')
{
	$get_lr_data=Qry($conn,"SELECT '' as billing_type,'0' as freight,truck_no as tno,done FROM freight_form_lr WHERE id='$vou_id'");
	
	if(!$get_lr_data){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error !");
		echo "<script>$('#lr_pending_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
	}
	
	if(numRows($get_lr_data)==0)
	{
		errorLog("LR not found ! id: $trip_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("LR not found !");
		echo "<script>$('#lr_pending_submit').attr('disabled',true);$('#loadicon').fadeOut('slow');</script>";exit();
	}
	
	$get_lrnos=Qry($conn,"SELECT GROUP_CONCAT(lrno SEPARATOR',') as lrno,SUM(wt12) as act,SUM(weight) as charge FROM freight_form_lr 
	WHERE frno='$vou_no'");
	
	if(!$get_lrnos){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error !");
		echo "<script>$('#lr_pending_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
	}
	
	$row_lr = fetchArray($get_lr_data);
	$row_lrs = fetchArray($get_lrnos);
	
	$lrnos = $row_lrs['lrno'];
	$act_wt = $row_lrs['act'];
	$charge_wt = $row_lrs['charge'];
	$billing_type = $row_lr['billing_type'];
	$freight = $row_lr['freight'];
	$tno_lr = $row_lr['tno'];
	$done = $row_lr['done'];
}
else
{
	$get_lr_data=Qry($conn,"SELECT bilty_no,billing_type,tno,awt as act_wt,cwt as charge_wt,done,tamt as freight 
	FROM mkt_bilty WHERE id='$vou_id'");
	
	if(!$get_lr_data){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error !");
		echo "<script>$('#lr_pending_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
	}
	
	if(numRows($get_lr_data)==0)
	{
		errorLog("LR not found ! id: $trip_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("LR not found !");
		echo "<script>$('#lr_pending_submit').attr('disabled',true);$('#loadicon').fadeOut('slow');</script>";exit();
	}
	
	$row_lr = fetchArray($get_lr_data);
	
	if($row_lr['bilty_no']!=$vou_no){
		errorLog("Voucher number not found ! Vou_id: $vou_id, Vou_no_using_id: $row_lr[bilty_no], Vou_no_client: $vou_no.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error !");
		echo "<script>$('#lr_pending_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
	}
	
	$lrnos = $row_lr['bilty_no'];
	$act_wt = $row_lr['act_wt'];
	$charge_wt = $row_lr['charge_wt'];
	$billing_type = $row_lr['billing_type'];
	$freight = $row_lr['freight'];
	$tno_lr = $row_lr['tno'];
	$done = $row_lr['done'];
}

if($done=='1')
{
	AlertError("Voucher attached with another trip !");
	echo "<script>$('#lr_pending_submit').attr('disabled',true);$('#loadicon').fadeOut('slow');</script>";exit();
}

$diff_dsl_consumption = sprintf("%.2f",($diesel_consumption - $diesel_consumption_db));

if($freight>0 and $billing_type=='TO_PAY'){
	$mb_to_pay=1;
	$mb_amount=$freight;
}
else{
	$mb_to_pay=0;
	$mb_amount=0;
}

StartCommit($conn);
$flag = true;

$qry=Qry($conn,"UPDATE dairy.trip SET km='$trip_km',dsl_consumption='$diesel_consumption',lr_pending='0',pincode='$dest_pincode',
act_wt='$act_wt',charge_wt='$charge_wt',lr_type='$vou_no',lr_type2=lr_type2+1,mb_to_pay='$mb_to_pay',mb_amount='$mb_amount',consignor_id='$con1_id',
lrno='$lrnos',consignee_id='$con2_id',addr_book_id_consignor='$from_addr_id',addr_book_id_consignee='$to_addr_id',end_poi_date='$trip_end_date' 
WHERE id='$trip_id'");
 
if(!$qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Trip not updated. TripId: $trip_id.",$conn,$page_name,__LINE__);
}

// if($by_pass_route=='YES')
// {
	// $chk_route = Qry($conn,"SELECT id FROM dairy.pending_master_lane WHERE from_id='$from_id' AND to_id='$to_id'");
	
	// if(!$chk_route){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	// }
		
	// if(numRows($chk_route)==0)
	// {
		// $insert_pending_route = Qry($conn,"INSERT INTO dairy.pending_master_lane(from_id,to_id,timestamp,tno,trip_id,branch,branch_user) VALUES 
		// ('$from_id','$to_id','$timestamp','$tno','$trip_id','$branch','$_SESSION[user_code]')");
		
		// if(!$insert_pending_route){
			// $flag = false;
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		// }
	// }
// }

$update_dsl_consumption = Qry($conn,"UPDATE dairy.own_truck SET diesel_left=(diesel_left-('$diff_dsl_consumption')) WHERE tno='$tno' 
AND diesel_trip_id!='-1'");

if(!$update_dsl_consumption){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($vou_type=='OWN')
{
	$update = Qry($conn,"UPDATE freight_form_lr SET done='1',trip_id='$trip_id' WHERE frno='$vou_no'");
}
else
{
	$update = Qry($conn,"UPDATE mkt_bilty SET done='1',trip_id='$trip_id' WHERE id='$vou_id'");
}

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Voucher not updated. vou_no: $vou_no. Type: $vou_type",$conn,$page_name,__LINE__);
}
	
if($flag)
{
	unset($_SESSION['atloading_trip_diesel_required']);
	MySQLCommit($conn);
	closeConnection($conn);
	SessionDestroyAtLoadingLRAdd();
	Redirect("LR UPDATED SUCCESSFULLY.","./");
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error..');$('#lr_pending_submit').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}
?>