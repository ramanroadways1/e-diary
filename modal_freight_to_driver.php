<script type="text/javascript">
$(document).ready(function (e) {
$("#FreightForm").on('submit',(function(e) {
$("#loadicon").show();
$("#freight_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./freight_received.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
function CheckFreightAdv(elem,type)
{
	if(type=='COLLECTED')
	{
		$('#loadicon').show();
		$('#freight_button').attr('disabled',true);
					
		jQuery.ajax({
		url: "get_lr_for_freight_advance_or_freight_rcv.php",
		data: 'bilty_no=' + elem,
		type: "POST",
		success: function(data) {
			$("#result_check_bilty").html(data);
		},
		error: function() {}
		});
	}
}
</script>

<div id="result_check_bilty"></div>

<form id="FreightForm" autocomplete="off" style="font-size:13px">   
  <div class="modal fade" id="FreightModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
		<div class="modal-header bg-primary">
			<span style="font-size:13px">Freight to Driver <span style="font-size:13px">( ड्राइवर को मिला भाड़ा ) </span> :</span>
		</div>
	<div class="modal-body">
		<div class="row">
			<div class="form-group col-md-4">
				<label>Truck No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input type="number" min="1" name="amount" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Date. <sup><font color="red">*</font></sup></label>
				<input type="text" name="date" value="<?php echo date("Y-m-d"); ?>" class="form-control" readonly required />
			</div>
			
			<script>
			function LoadFreightBilty(id)
			{
				if(id!='')
				{
					$('#loadicon').show();
					$('#freight_button').attr('disabled',true);
					
					jQuery.ajax({
					url: "get_lr_for_freight_advance_or_freight_rcv.php",
					data: 'id=' + id + '&type=' + 'ADVANCE',
					type: "POST",
					success: function(data) {
					$("#result_load_bilty").html(data);
					},
					error: function() {}
					});
				}	
			}
			</script>
			
			<input type="hidden" name="trip_id" class="trip_id_set" />
			<input type="hidden" name="trip_no" class="trip_no_set" />
			<input type="hidden" name="driver_name" class="driver_name_set" />
			<input type="hidden" name="driver_code" value="<?php echo $driver_code_value; ?>" />
			
			<div id="result_load_bilty" class="form-group col-md-6">
			</div>
			
			<div class="form-group col-md-6">
				<label>Advance/Balance. <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px" name="adv_bal" class="form-control" required="required">
					<option style="font-size:12px" value="">--Select option--</option>
					<option style="font-size:12px" value="ADVANCE">ADVANCE</option>
					<option style="font-size:12px" value="BALANCE">BALANCE</option>
				</select>
			</div>
			
			<div class="form-group col-md-12">
				<label>Narration. <sup><font color="red">*</font></sup></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,/]/,'')" class="form-control" name="narration" required></textarea>
			</div>
			
		</div>
        </div>
        <div class="modal-footer">
          <button <?php if($driver_code_value=="") {echo "disabled"; } ?> type="submit" id="freight_button" class="btn btn-sm btn-danger">Submit</button>
          <button type="button" class="btn btn-sm btn-primary" id="hide_freight" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>