<?php
require("./connect.php");

$location_string = escapeString($conn,$_POST['last_loc']);
$location_string = explode('#',$location_string);

$last_loc = $location_string[0];
$last_loc_id = $location_string[1];
$con2_id = $location_string[2];
$trip_id=escapeString($conn,$_POST['trip_id']);
$tno=escapeString($conn,$_SESSION['diary']);

// echo "<script>alert('$last_loc and $last_loc_id');</script>";

if(empty($last_loc))
{
	echo "<script>alert('Destination location not found.');$('#to_loc_for_add_more').val('');$('#loadicon').hide();</script>";
	exit();
}

if(empty($trip_id))
{
	echo "<script>alert('Trip not found.');$('#to_loc_for_add_more').val('');$('#loadicon').hide();</script>";
	exit();
}

if(empty($tno))
{
	errorLog("Unable to Fetch Vehicle Number: $tno or Trip_id: $trip_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>alert('Vehicle not found.');window.location.href='./';</script>";
	exit();
}

$get_trip_data = Qry($conn,"SELECT t.tno,t.from_station,t.from_id,t.to_station,t.to_id,t.from_poi,t.consignee_id,t.addr_book_id_consignee,
t.km,s.pincode,s._lat,s._long 
FROM dairy.trip as t 
LEFT JOIN station AS s ON s.id=t.from_id 
WHERE t.id='$trip_id'");
		
if(!$get_trip_data){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($get_trip_data)==0)
{
	errorLog("Trip not found--> $trip_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		alert('Trip not found.');
		window.location.href='./';
	</script>";
	exit();
}

$row_loc_data = fetchArray($get_trip_data); 

$from_loc_id = $row_loc_data['from_id'];
$from_loc_db = $row_loc_data['from_station'];
$current_destination = $row_loc_data['to_station'];
$trip_km = $row_loc_data['km'];

$addr_book_id = "";
$addr_book_pincode = "";
$addr_book_lat_long = "";
$insert_stoppage_point = "NO";

echo "<script>
	$('#from_loc_text').html('$from_loc_db to');
	$('#to_loc_text').html('$last_loc');
	$('#pincode_text').html('$last_loc');
</script>";

$pincode_this_loc=Qry($conn,"SELECT id,name,pincode FROM station WHERE id='$last_loc_id'");
if(!$pincode_this_loc){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($pincode_this_loc)==0){
	errorLog("Last Location id not found. Location: $last_loc and Id: $last_loc_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>alert('Location not found.');$('#to_loc_for_add_more').val('');$('#loadicon').hide();</script>";
	exit();
}

$row_pincode = fetchArray($pincode_this_loc);

if($row_pincode['name']!=$last_loc)
{
	errorLog("Last Location not verified. Location: $last_loc and Id: $last_loc_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		alert('Location is not valid..');
	</script>";
	exit();
}

$pincode = $row_pincode['pincode'];

if(empty($pincode))
{
	echo "<script>alert('Destination pincode not found.');$('#to_loc_for_add_more').val('');$('#loadicon').hide();</script>";
	exit();
}

if($last_loc != $current_destination)
{
	$trip_km = "NA";
	
	$next_trip=Qry($conn,"SELECT from_station FROM dairy.trip WHERE id>$trip_id AND tno='$tno' ORDER BY id ASC LIMIT 1");
	if(!$next_trip){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
			
	if(numRows($next_trip)>0)
	{
		$RowNextTrip=fetchArray($next_trip);
		
		if($last_loc!=$RowNextTrip['from_station'])
		{
			echo "<script>
				alert('You\'ve selected $last_loc as Last Location But next Trip\'s From Location is: $RowNextTrip[from_station] !!');
				$('#to_loc_for_add_more').val('');
				$('#to_loc_id_for_add_new').val('');
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
}

echo "<script>
	$('#to_loc_id_for_add_new').val('$last_loc_id');
	$('#pincode_for_add_new').val('$pincode');
	$('#trip_km_for_add_new').val('$trip_km');
	$('#val_btn_add_more').attr('disabled',false);
	$('#loadicon').hide();
</script>";
?>