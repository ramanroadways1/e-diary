<script type="text/javascript">
$(document).ready(function (e) {
$("#CreateBilty").on('submit',(function(e) {
$("#loadicon").show();
$("#create_bilty_btn").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./create_market_bilty.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
$(function() {
		$("#from1").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from1').val(ui.item.value);   
            $('#from_id_mkt_bilty').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from1').val("");   
			$('#from_id_mkt_bilty').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to1").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to1').val(ui.item.value);   
            $('#to_id_mkt_bilty').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to1').val("");   
			$('#to_id_mkt_bilty').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<script>
  $(function() {  
      $("#broker").autocomplete({
			source: function(request, response){ 
			$.ajax({
                  url: "autofill/broker_name.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#broker_pan').val(ui.item.pan);     
               $('#broker_id').val(ui.item.id);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#broker_pan").val('');
				$("#broker_id").val('');
				alert('Broker does not exist !'); 
              } 
              },
			}); 
      }); 
</script>

<script>
  $(function() {  
      $("#billing_party").autocomplete({
			source: function(request, response){ 
			$.ajax({
                  url: "autofill/billing_party.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#billing_party_pan').val(ui.item.pan);     
               $('#billing_party_id').val(ui.item.id);     
               $('#billing_party_addr').val(ui.item.addr);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#billing_party_pan").val('');
				$("#billing_party_id").val('');
				$("#billing_party_addr").val('');
				alert('Billing Party does not exist !'); 
              } 
              },
			}); 
      }); 
</script> 

<form style="font-size:12.5px" id="CreateBilty" autocomplete="off"> 
<div class="modal fade" id="BiltyModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
     <div class="modal-content">	
		<div class="modal-header bg-primary">
			<span style="font-size:15px">CREATE NEW MARKET BILTY :</span>
		</div>
			
		<div class="modal-body">
		<div class="row">
	
		<?php
		$sel_date_rule_mb=Qry($conn,"SELECT days FROM dairy.date_rule WHERE type='3'");
		if(!$sel_date_rule_mb){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}

		if(numRows($sel_date_rule_mb)==0)
		{
			Redirect("Date Rule not updated for LR Selection.","./");
			exit();
		}

		$row_date_rule_for_mb=fetchArray($sel_date_rule_mb);
		$min_date_mb=$row_date_rule_for_mb['days'];
		
		?>	
		<div id="result_bilty"></div>
		
		<div class="form-group col-md-4">
			<label>LR Date. <sup><font color="red">*</font></sup></label>
			<input type="date" name="lr_date" min="<?php echo date('Y-m-d', strtotime("$min_date_mb days", strtotime($date))); ?>" 
			max="<?php echo $date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" />
		</div>	
		
		<div class="form-group col-md-4">
			<label>Party LR's No. <sup><font color="red">(1234,1245)</font></sup></label>
			<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9,]/,'')" name="lr_no" class="form-control" />
		</div>	
		
		<div class="form-group col-md-4">
			<label>From. <sup><font color="red">*</font></sup></label>
			<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9.]/,'')" type="text" name="from" id="from1" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-4">
			<label>To. <sup><font color="red">*</font></sup></label>
			<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.]/,'')" name="to" id="to1" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-4">
			<label>Broker Name. <sup><font color="red">*</font></sup> 
			<a href="./new_broker.php" target="_blank">Create new</a></label>
			<input type="text" name="broker" id="broker" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-4">
			<label>Broker PAN. <sup><font color="red">*</font></sup></label> 
			<input type="text" name="broker_pan" id="broker_pan" class="form-control" readonly required />
		</div>
		
		<input type="hidden" id="broker_id" name="broker_id">
		<input type="hidden" id="billing_party_id" name="billing_party_id">
		
		<input type="hidden" id="from_id_mkt_bilty" name="from_id">
		<input type="hidden" id="to_id_mkt_bilty" name="to_id">
		
		<div class="form-group col-md-4">
			<label>Billing Party. <sup><font color="red">*</font></sup> 
			<a href="./new_party.php" target="_blank">Create new</a></label>
			<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9.]/,'')" type="text" name="billing_party" id="billing_party" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-4">
			<label>Billing Party PAN. <sup><font color="red">*</font></sup></label>
			<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.]/,'')" name="billing_party_pan" id="billing_party_pan" class="form-control" readonly />
		</div>	
		
		<script>
		function BillingType(elem)
		{
			$('#adv_amount').val('');
			$('#adv_date').attr('');
			
			if(elem=='FOC')
			{
				$('#act_weight').val('0');
				$('#charge_weight').val('0');
				$('#rate_pmt').val('0');
				$('#freight_bilty').val('0');
				
				$('#act_weight').attr('readonly',true);
				$('#charge_weight').attr('readonly',true);
				$('#rate_pmt').attr('readonly',true);
				$('#freight_bilty').attr('readonly',true);
				
				$('#adv_amount_div').hide();
				$('#adv_date_div').hide();
				$('#adv_narr_div').hide();
				$('#adv_amount').attr('required',false);
				$('#adv_date').attr('required',false);
				$('#bilty_narration').attr('required',false);
			}
			else
			{
				if(elem=='TBB')
				{
					$('#adv_amount_div').show();
					$('#adv_date_div').show();
					$('#adv_narr_div').show();
					$('#adv_amount').attr('required',true);
					$('#adv_date').attr('required',true);
					$('#bilty_narration').attr('required',true);
				}
				else
				{
					$('#adv_amount_div').hide();
					$('#adv_date_div').hide();
					$('#adv_narr_div').hide();
					$('#adv_amount').attr('required',false);
					$('#adv_date').attr('required',false);
					$('#bilty_narration').attr('required',false);
				}
				
				$('#act_weight').val('');
				$('#charge_weight').val('');
				$('#rate_pmt').val('');
				$('#freight_bilty').val('');
				
				$('#act_weight').attr('readonly',false);
				$('#charge_weight').attr('readonly',false);
				$('#rate_pmt').attr('readonly',false);
				$('#freight_bilty').attr('readonly',false);
			}
		}
		</script>
		
		<div class="form-group col-md-4">
			<label>LR BY : <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px" name="lr_by" onchange="LRBy(this.value)" class="form-control" required>
				<option style="font-size:12px" value="">Select an option</option>
				<option style="font-size:12px" value="SHIVANI">SHIVANI</option>
				<option style="font-size:12px" value="OTHERS">OTHERS</option>
			</select>
		</div>
		
		<script>
		function LRBy(elem)
		{
			$('#adv_amount').val('');
			$('#adv_date').attr('');
				
			if(elem=='SHIVANI')
			{
				$('#billing_type_others').hide();
				$('#billing_type_shivani').show();
				$('#charge_weight').attr('oninput','');
				$('#rate_pmt').attr('oninput','');
				$('#freight_bilty').attr('oninput','');
				
				$('#rate_pmt').attr('readonly',true);
				$('#billing_type').attr('required',false);
				$('#freight_bilty').attr('readonly',true);
				
				$('#rate_pmt').val('0');
				$('#freight_bilty').val('0');
				
				$('#adv_amount_div').hide();
				$('#adv_date_div').hide();
				$('#adv_narr_div').hide();
				$('#adv_amount').attr('required',false);
				$('#adv_date').attr('required',false);
				$('#bilty_narration').attr('required',false);
			}
			else if(elem=='OTHERS')
			{
				$('#billing_type_others').show();
				$('#billing_type').attr('required',true);
				$('#billing_type_shivani').hide();
				$('#charge_weight').attr('oninput','ResetRate()');
				$('#rate_pmt').attr('oninput','SumRate1()');
				$('#freight_bilty').attr('oninput','SumRate2()');
				
				$('#rate_pmt').attr('readonly',false);
				$('#freight_bilty').attr('readonly',false);
				
				$('#rate_pmt').val('');
				$('#freight_bilty').val('');
				
				$('#adv_amount_div').show();
				$('#adv_date_div').show();
				$('#adv_narr_div').show();
				$('#adv_amount').attr('required',true);
				$('#adv_date').attr('required',true);
				$('#bilty_narration').attr('required',true);
			}
			else
			{
				$('#billing_type_others').show();
				$('#billing_type_shivani').hide();
				$('#billing_type').attr('required',true);
				$('#charge_weight').attr('oninput','ResetRate()');
				$('#rate_pmt').attr('oninput','SumRate1()');
				$('#freight_bilty').attr('oninput','SumRate2()');
				
				$('#rate_pmt').attr('readonly',false);
				$('#freight_bilty').attr('readonly',false);
				
				$('#rate_pmt').val('');
				$('#freight_bilty').val('');
				
				$('#adv_amount_div').show();
				$('#adv_date_div').show();
				$('#adv_narr_div').show();
				$('#adv_amount').attr('required',true);
				$('#adv_date').attr('required',true);
				$('#bilty_narration').attr('required',true);
			}
		}
		</script>
		
		<div id="billing_type_others" class="form-group col-md-3">
			<label>Billing TYPE. <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px" name="billing_type" id="billing_type" onchange="BillingType(this.value)" class="form-control" required>
				<option style="font-size:12px" value="">Select an option</option>
				<option style="font-size:12px" value="TO_PAY">TO PAY</option>
				<option style="font-size:12px" value="TBB">TO BE BILLED</option>
				<option style="font-size:12px" value="FOC" disabled>FREE OF COST</option>
			</select>
		</div>
		
		<div id="billing_type_shivani" style="display:none" class="form-group col-md-3">
			<label>Billing TYPE. <sup><font color="red">*</font></sup></label>
			<input type="text" name="billing_type2" id="billing_type2" class="form-control" value="TBB" required readonly />
		</div>
		
		<div class="form-group col-md-3">
			<label>Actual Weight. <sup><font color="red">*</font></sup></label>
			<input type="number" min="0" step="any" max="90" name="act_weight" id="act_weight" class="form-control" required />
		</div>	
		
		<script>
		function ResetRate()
		{
			$('#rate_pmt').val('');
			$('#freight_bilty').val('');
		}
		
		function SumRate1()
		{
			var weight = $('#charge_weight').val();
			
			if(weight=='')
			{
				$('#rate_pmt').val('');
				$('#freight_bilty').val('');
				$('#charge_weight').focus();
			}
			else
			{
				$("#freight_bilty").val(Math.round(Number($("#charge_weight").val()) * Number($("#rate_pmt").val())).toFixed(2));
			}
		}
		
			function SumRate2() 
			{
				var weight = $('#charge_weight').val();
						 
				if(weight=='')
				{
					$('#rate_pmt').val('');
					$('#freight_bilty').val('');
					$('#charge_weight').focus();
				}	
				else
				{
					$("#rate_pmt").val(Math.round(Number($("#freight_bilty").val()) / Number($("#charge_weight").val())).toFixed(2));
				}	
			}
		</script>
		
		<div class="form-group col-md-3">
			<label>Charge Weight. <sup><font color="red">*</font></sup></label>
			<input type="number" min="0" max="90" oninput="ResetRate();" step="any" name="charge_weight" id="charge_weight" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-3">
			<label>Rate. <sup><font color="red">*</font></sup></label>
			<input type="number" min="0" oninput="SumRate1();" name="rate_pmt" id="rate_pmt" class="form-control" required />
		</div>
		
		<div class="form-group col-md-4">
			<label>Freight. <sup><font color="red">*</font></sup></label>
			<input type="number" min="0" oninput="SumRate2();" name="freight_bilty" id="freight_bilty" class="form-control" required />
		</div>
		
		<div class="form-group col-md-4">
			<label>Vehicle Placed By. <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px" data-size="5" data-live-search="true" data-live-search-style="" name="veh_placer" class="form-control selectpicker" required>
				<option style="font-size:12px" value="">Select an option</option>
				<?php
				$qry_veh_placer=Qry($conn,"SELECT username FROM dairy.user WHERE role='1' ORDER BY username ASC");
				if(!$qry_veh_placer){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				
				$qry_veh_placer_branch=Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('DUMMY','HEAD') 
				ORDER BY username ASC");
				
				if(!$qry_veh_placer_branch){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				
				if(numRows($qry_veh_placer)>0)
				{
					while($row_veh=fetchArray($qry_veh_placer))
					{
						echo "<option  style='font-size:12px' value='$row_veh[username]'>$row_veh[username]</option>";
					}
				}
				
				if(numRows($qry_veh_placer_branch)>0)
				{
					while($row_veh_branch=fetchArray($qry_veh_placer_branch))
					{
						echo "<option  style='font-size:12px' value='$row_veh_branch[username]'>$row_veh_branch[username]</option>";
					}
				}
				?>
			</select>
		</div>
		
		<div class="form-group col-md-4">
			<label>Send POD To. <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px" data-size="5" data-live-search="true" data-live-search-style="" name="billing_branch" class="form-control selectpicker" required>
				<option style="font-size:12px" value="">Select an option</option>
				<?php
				$get_branch_sent_pod=Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('DUMMY','HEAD') 
				ORDER BY username ASC");
				
				if(!$get_branch_sent_pod){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				
				if(numRows($get_branch_sent_pod)>0)
				{
					while($row_branch_pod_sent=fetchArray($get_branch_sent_pod))
					{
						echo "<option style='font-size:12px' value='$row_branch_pod_sent[username]'>$row_branch_pod_sent[username]</option>";
					}
				}
				?>
			</select>
		</div>
		
		<div class="form-group col-md-4" id="adv_amount_div">
			<label>Advance Amount. <sup><font color="red">(expected)*</font></sup></label>
			<input type="number" min="0" oninput="ZeroAdvBilty(this.value)" name="adv_amount" id="adv_amount" class="form-control" required />
		</div>
		
		<script>
		function ZeroAdvBilty(amount)
		{
			if(amount==0){
				$('#adv_date_div').hide();
				$('#adv_narr_div').hide();
				$('#adv_date').attr('required',false);
				$('#bilty_narration').attr('required',false);
			}
			else{
				$('#adv_date_div').show();
				$('#adv_narr_div').show();
				$('#adv_date').attr('required',true);
				$('#bilty_narration').attr('required',true);
			}
		}
		</script>
		
		<div class="form-group col-md-4" id="adv_date_div">
			<label>Advance Date. <sup><font color="red">(expected)*</font></sup></label>
			<input type="date" name="adv_date" id="adv_date" min="<?php echo $date; ?>" 
			max="<?php echo date('Y-m-d', strtotime("+7 days", strtotime($date))); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" />
		</div>

		<div class="form-group col-md-4" id="adv_narr_div">
			<label>Payment Mode. <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px" name="bilty_narration" id="bilty_narration" class="form-control" required>
				<option style="font-size:12px" value="">Select an option</option>
				<option style="font-size:12px" value="HO">RTGS to HO (हेड ऑफिस को RTGS होगा)</option>
				<option style="font-size:12px" value="BRANCH_CASH">Cash to Branch (ब्रांच को CASH मिलेगा)</option>
				<option style="font-size:12px" value="BRANCH_CHEQUE">Cheque to Branch (ब्रांच को CHEQUE मिलेगा)</option>
				<!--<option value="BRANCH_CHEQUE">Cheque to Branch (ब्रांच को CHEQUE मिलेगा)</option>-->
			</select>
		</div>		
		
		<div class="form-group col-md-12">
			<label>Billing Party Address. <sup><font color="red">*</font></sup></label>
			<textarea class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,-/]/,'')" name="billing_party_addr" id="billing_party_addr" required readonly></textarea>
		</div>
		
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="create_bilty_btn" class="pull-left btn-sm btn btn-danger">Create Bilty</button>
          <button type="button" onclick="document.getElementById('trip_button').click();LRType('MKT');" id="bilty_close_button" class="btn  btn-sm btn-primary" data-dismiss="modal">Close</button> 
          <button type="button" style="display:none" id="bilty_close_button22" data-dismiss="modal">Close2</button>
		</div>
      </div>
    </div>
  </div>
  </form>