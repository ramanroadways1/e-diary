<?php
require_once("./connect.php");

$vou_no = escapeString($conn,strtoupper($_POST['mbno']));

$get_mb = Qry($conn,"SELECT * FROM mkt_bilty WHERE bilty_no='$vou_no'");

if(!$get_mb){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>window.location.href='./'";
	exit();
}

if(numRows($get_mb)==0)
{
	AlertRightCornerError("Market bilty not found.");
	exit();
}

$row_mb = fetchArray($get_mb);
?>
<div class="modal fade" id="MktBiltyViewModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
     <div class="modal-content">	
		<div class="modal-header bg-primary">
			<span style="font-size:15px">Market Bilty : <?php echo $vou_no; ?></span>
		</div>

	<div class="modal-body">
		<div class="row">
		<div class="col-md-12 table-responsive">
			<table class="table table-bordered table-striped" style="font-family: 'Open Sans', sans-serif !important; <?php if(isMobile()){ echo "font-size:10px"; } else { echo "font-size:12px"; }?>">
				
				<tr>
					<td><b>Bilty no:</b> <?php echo $vou_no; ?></td>
					<td>LR date: <?php echo $row_mb['lrdate']; ?></td>
					<td>Vehicle no: <?php echo $row_mb['tno']; ?></td>
				</tr>

				<tr>
					<td>From <?php echo $row_mb['frmstn']; ?></td>
					<td>To <?php echo $row_mb['tostn']; ?></td>
					<td>To-PAY/TBB: <?php echo $row_mb['billing_type']; ?></td>
				</tr>	

				<tr>
					<td colspan="3">Party LR number: <?php echo $row_mb['plr']; ?></td>
				</tr>

				<tr>
					<td>Act weight: <?php echo $row_mb['awt']; ?></td>
					<td>Charge weight: <?php echo $row_mb['cwt']; ?></td>
					<td>Vehicle placer: <?php echo $row_mb['veh_placer']; ?></td>
				</tr>

				<tr>
					<td colspan="2">Broker: <?php echo $row_mb['broker']; ?></td>
					<td>Rate: <?php echo $row_mb['rate']; ?></td>
				</tr>	
				
				<tr>
					<td colspan="2">Billing party: <?php echo $row_mb['billing_party']; ?></td>
					<td>Freight: <?php echo $row_mb['tamt']; ?></td>
				</tr>

			</table>
		</div>
		</div>
   </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button> 
        </div>
      </div>
    </div>
  </div>
  
<button id="view_modal_btn_mkt_bilty" style="display:none" data-toggle="modal" data-target="#MktBiltyViewModal" type="button"></button>

<script>
	$('#view_modal_btn_mkt_bilty')[0].click();
	$('#loadicon').fadeOut('slow');
</script>