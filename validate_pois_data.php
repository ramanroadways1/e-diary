<?php
require_once("./connect.php");

$end_date = escapeString($conn,$_POST['end_date']);
$end_point = escapeString($conn,$_POST['end_point']);
$trip_id = escapeString($conn,$_POST['trip_id']);
$bilty_no = escapeString($conn,$_POST['bilty_no']);
$running_trip = escapeString($conn,$_POST['running_trip']);
$total_act_wt = escapeString($conn,$_POST['total_act_wt']);

if(empty($trip_id)){
	AlertError("Trip not found !");
	echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
}

if(empty($bilty_no)){
	AlertError("LR not found !");
	echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
}

if($running_trip!='true' AND $running_trip!='false')
{
	AlertError("Invalid data posted !");
	echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
}

if($running_trip=='false')
{
	if($end_date==0){
		AlertError("Select trip end date !");
		echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
	}
	
	if($end_point==''){
		AlertError("Select trip end point !");
		echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
	}
}

$tno = escapeString($conn,$_SESSION['diary']);

$get_trip=Qry($conn,"SELECT t.tno,t.from_station,t.to_station,t.from_id,t.to_id,t.from_poi,t.from_pincode,t.to_poi,t.dsl_consumption,
t.addr_book_id_consignor,t.addr_book_id_consignee,t.to_pincode,s2._lat,s2._long,s2.pincode 
FROM dairy.trip AS t 
LEFT JOIN station AS s2 ON s2.id=t.to_id
WHERE t.id='$trip_id'");

if(!$get_trip){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error.","./");
	exit();
}

if(numRows($get_trip)==0)
{
	echo "<script>alert('Trip not found..');window.location.href='./';</script>";
	exit();
}

$row_trip = fetchArray($get_trip);

$dsl_consumption = $row_trip['dsl_consumption'];

$get_avg = GetAvgValue($conn,$row_trip['from_id'],$row_trip['to_id'],$row_trip['from_station'],$row_trip['to_station'],$tno,"LOADING_TRIP",$total_act_wt);
		
if($get_avg[0]==false)
{
	AlertError($get_avg[1]);
	echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
	exit();
}
		
$avg_value = $get_avg[2];
$by_pass_route = $get_avg[3];	
	
if($row_trip['tno']!=$tno)
{
	errorLog("Vehicle number not verified. SessionSet: $tno and Trip_Veh: $row_trip[tno]. Trip_id: $trip_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		window.location.href='./';
	</script>";
	exit();
}

$array_1 = explode('_', $bilty_no);
$vou_id=$array_1[1];
$con1_id=$array_1[2];
$con2_id=$array_1[3];

$from_addr_id = 0;
$to_addr_id = 0;

if($con1_id!=0)
{
	$chk_from_poi = Qry($conn,"SELECT id,pincode,_lat,_long FROM address_book_consignor WHERE consignor='$con1_id' AND from_id='$row_trip[from_id]'");

	if(!$chk_from_poi){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		Redirect("Error.","./");
		exit();
	}
	
	if(numRows($chk_from_poi)==0)
	{
		AlertError("Please update loading point first !");
		echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	$row_from = fetchArray($chk_from_poi);
	
	$from_addr_id = $row_from['id'];
	$trip_start_point = $row_from['_lat'].",".$row_from['_long'];
	$trip_start_pincode = $row_from['pincode'];
}
else
{
	$trip_start_point = $row_trip['from_poi'];
	$trip_start_pincode = $row_trip['from_pincode'];
}

if(strlen($trip_start_point)<=5)
{
	AlertError("Trip start point not found !");
	echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($con2_id!=0)
{
	$chk_to_poi = Qry($conn,"SELECT id,pincode,_lat,_long FROM address_book_consignee WHERE to_id='$row_trip[to_id]' AND consignee='$con2_id'");
		
	if(!$chk_to_poi){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		Redirect("Error.","./");
		exit();
	}
		
	if(numRows($chk_to_poi)==0)
	{
		AlertError("Please update unoading point first !");
		echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	$row_to = fetchArray($chk_to_poi);
	
	$to_addr_id = $row_to['id'];
	$trip_end_point = $row_trip['_lat'].",".$row_trip['_long'];
	$trip_end_pincode = $row_trip['pincode'];
}
else
{
	if($running_trip=='false')
	{
		$trip_end_point = explode("_",$end_point)[0].",".$explode("_",$end_point)[1];
		$trip_end_pincode = explode("_",$end_point)[2];
		
		// distance b/w to location and unloading point
			
		$distance_unloading_point_and_loc = calculate_distance($row_trip['_lat'],$row_trip['_long'],explode("_",$end_point)[0],explode("_",$end_point)[1]); 
			
		if($distance_unloading_point_and_loc>10)
		{
			AlertError('Check destination OR trip end point !<br><span style=\"color:maroon\">Distance is: '.$distance_unloading_point_and_loc.' KMs</span>');
			echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";
			exit();
		}
	}
	else
	{
		$trip_end_point = $row_trip['_lat'].",".$row_trip['_long'];
		$trip_end_pincode = $row_trip['pincode'];
	}
}

if(strlen($trip_end_point)<=5)
{
	AlertError("Trip end point not found.<br>Code : 3.");
	echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
}

if(strlen($trip_end_pincode)!=6)
{
	AlertError("Trip end point pincode not found.");
	echo "<script>$('#lr_pending_verify_btn').attr('disabled',false);$('#lr_pending_submit').hide();$('#loadicon').fadeOut('slow');</script>";exit();
}

// $km_distance = calculate_distance(explode(",",$trip_start_point)[0],explode(",",$trip_start_point)[1],explode(",",$trip_end_point)[0],explode(",",$trip_end_point)[1]);

echo "<script>
	$('.bilty_sel_values').attr('disabled',true);
	$('#bilty_sel_values_$vou_id').attr('disabled',false);
	$('#is_running_add_lr_to_trip').attr('disabled',true);
	$('#pincode_ip_new').val('$trip_end_pincode');
	$('#by_pass_route_at_loading_trip').val('$by_pass_route');
</script>";
	
$_SESSION['atloading_from_addr_id']=$from_addr_id;	
$_SESSION['atloading_to_addr_id']=$to_addr_id;	
$_SESSION['atloading_vou_no']=$bilty_no; // vou_no_vou_id_con1_id_con2_id
$_SESSION['atloading_trip_id']=$trip_id;	
$_SESSION['atloading_running_trip']=$running_trip;	 // true / false
$_SESSION['atloading_end_date']=$end_date;	 // trip end date / unload date
$_SESSION['atloading_end_point']=$end_point; // lat,long 20.152456,45.45465
$_SESSION['atloading_trip_km']="0"; // trip km Google
$_SESSION['atloading_dest_pincode']= $trip_end_pincode; // trip km Google
?>
<div id="km_session_html_add_lr_to_trip"></div>

<script type="text/javascript">
    function myfunction(){
        var map;
        var start = new google.maps.LatLng(<?php echo $trip_start_point; ?>);
        var end = new google.maps.LatLng(<?php echo $trip_end_point; ?>);
        var option ={
            zoom : 15,
            center : start,
			disableDefaultUI: true,
        };
        map = new google.maps.Map(document.getElementById('map_add_lr_to_trip'),option);
        var display = new google.maps.DirectionsRenderer();
        var services = new google.maps.DirectionsService();
        display.setMap(map);
            var request ={
                origin : start,
                destination:end,
                travelMode: 'DRIVING'
            };
            services.route(request,function(result,status){
                if(status =='OK'){
                    display.setDirections(result);
					 var directionsData = result.routes[0].legs[0]; // Get data about the mapped route
					if (!directionsData) {
						Swal.fire({
							icon: 'error',
							title: 'Invalid location.',
							html: '<font size=\'2\' color=\'red\'>Error : Request failed !!</font>',
						})
						// alert('Error : Request failed !!');
						$('#loadicon').fadeOut("slow");
						$('#lr_pending_verify_btn').attr('disabled',true);
						$('#lr_pending_submit').attr('disabled',true);
						$('#lr_pending_submit').hide();
					}
					else {
					  var distance1 = directionsData.distance.text;
					  var distance_no = distance1.replace(/km/g,'');  // get only numeric value
					  var distance_no = distance_no.replace(/ /g,'');  // get only numeric value
					
					  var duration1 =  directionsData.duration.text;
					  $('#trip_km_new').val(distance_no);
					  $('#km_google_add_lr_to_trip').html(distance1);
					  $('#duration_google_add_lr_to_trip').html(duration1);
					  $('#loadicon').fadeOut("slow");
					  $('#lr_pending_submit').attr('disabled',false);
					  $('#lr_pending_submit').show();
					  $('#lr_pending_verify_btn').attr('disabled',true);
					  jQuery.ajax({url: "./_create_session_var_for_km.php",
					  data:'distance='+distance_no + '&type=' + 'add_lr_to_trip' + '&trip_id=' + '<?php echo $trip_id; ?>' + '&avg_value=' + '<?php echo $avg_value; ?>',
					  type: "POST",
					  success: function(data){
						$("#km_session_html_add_lr_to_trip").html(data);
					 },
						error: function() {
					}});
				}
                }
				else{
					alert('Error: '+status);
					$('#loadicon').fadeOut("slow");
					$('#lr_pending_submit').attr('disabled',true);
					$('#lr_pending_submit').hide();
					$('#lr_pending_verify_btn').attr('disabled',false);
				}
            });
    }
	myfunction();
</script>
