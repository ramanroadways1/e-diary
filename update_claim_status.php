<?php
require_once './connect.php'; 

$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$pod_id = escapeString($conn,($_POST['lr_id']));
$vou_no = escapeString($conn,($_POST['vou_no']));
$lrno = escapeString($conn,($_POST['lrno']));
$vou_id = escapeString($conn,($_POST['vou_id']));
$vou_type = escapeString($conn,($_POST['vou_type']));

if($vou_type=='MARKET_BILTY')
{
	$table_name="mkt_bilty";
	$where_condition="id='$vou_id'";
}
else
{
	$table_name="freight_form_lr";
	$where_condition="id='$vou_id'";
}
	
$chk_record = Qry($conn,"SELECT claim_entry FROM `$table_name` WHERE id='$vou_id'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	echo "<script>$('#claim_branch_$pod_id').val('');</script>";
	exit();
}
	
if(numRows($chk_record)==0)
{
	AlertError("Voucher not found !");
	echo "<script>$('#claim_branch_$pod_id').val('');</script>";
	exit();
}

$row_pod = fetchArray($chk_record);

if($row_pod['claim_entry']!="0")
{
	AlertRightCornerError("Claim already updated !");
	echo "<script>$('#claim_branch_$pod_id').val('');</script>";
	exit();
}
	
$update_claim_status = Qry($conn,"UPDATE `$table_name` SET claim_entry='2' WHERE $where_condition");
	
if(!$update_claim_status){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	echo "<script>$('#claim_branch_$pod_id').val('');</script>";
	exit();
}
	
AlertRightCornerSuccess("Claim status updated !");
echo "<script>$('#claim_branch_$pod_id').attr('disabled',true);</script>"; 
exit();
?>