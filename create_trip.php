<?php
require('connect.php');

$tno=escapeString($conn,strtoupper($_SESSION['diary']));
$tno_set=escapeString($conn,strtoupper($_POST['tno']));
$route_type=escapeString($conn,($_POST['route_type_1']));
$polyline=escapeString($conn,($_POST['polyline']));

// if($_SESSION['user_code']!='032')
// {
	// AlertError("System under maintenance !");
	// echo "<script>$('#loadicon').fadeOut('slow');</script>";
	// exit();
// }

$chk_lr_pending = Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$tno' AND lr_pending='1'");
	
if(!$chk_lr_pending){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_lr_pending)>0)
{
	AlertError("Clear LR pending trip first !!");
	echo "<script>$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($route_type=='ROAD')
{
	$route_type1 = "By_Road";
}
else if($route_type=='TRAIN')
{
	$route_type1 = "By_Rail";
}
else
{
	$route_type1 = "";
}

$timestamp = date("Y-m-d H:i:s"); 
$date=date("Y-m-d");

if(!isset($_SESSION['create_trip_diesel_required']))
{
	errorLog("Diesel consumption not found.",$conn,$page_name,__LINE__);
	echo "<script>alert('Diesel value not found.');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($branch=='PANIPAT')
{
	echo "<script>window.location.href='./';</script>";
	exit();
}

require_once("./check_cache.php");

if($tno!=$tno_set){
	echo "<script>window.location.href='./logout.php';</script>";
	exit();
}

$chk_fix_txns = Qry($conn,"SELECT id FROM dairy.fix_txn_pending WHERE tno='$tno'");

if(!$chk_fix_txns){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_fix_txns)>0)
{
	AlertError('Fix Txns are in process !');
	echo "<script>$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_wheeler = Qry($conn,"SELECT comp,wheeler,trishul_card,trishul_mapped,model FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_wheeler){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_wheeler = fetchArray($get_wheeler);

$wheeler = $row_wheeler['wheeler'];
$company = $row_wheeler['comp'];
$model = $row_wheeler['model'];

if($row_wheeler['trishul_card']=="1" AND $row_wheeler['trishul_mapped']!="1")
{
	AlertError('Active trishul card first !!');
	echo "<script>$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$chk_running_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE (status=0 || date=0 || end_date=0) AND tno='$tno'");

if(!$chk_running_trip){
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_running_trip)>0)
{
	AlertError('Running Trip Found !!');
	echo "<script>$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$check_trip_date = Qry($conn,"SELECT end_date FROM dairy.trip WHERE tno='$tno'");

if(!$check_trip_date){
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$trip_date = $timestamp;	

if(numRows($check_trip_date)==0)
{
	$check_trip_date2 = Qry($conn,"SELECT date(end_date) as trip_end_date FROM dairy.trip_final WHERE tno='$tno' ORDER by id DESC LIMIT 1");
	
	if(!$check_trip_date2){
		echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}

	if(numRows($check_trip_date2)>0)
	{
		$row_trip_date2 = fetchArray($check_trip_date2);
		
		if($row_trip_date2['trip_end_date']==$date)
		{
			$trip_date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
			$trip_date=$trip_date." H:i:s";
		}
	}
}
else
{
	$row_last_end_date = fetchArray($check_trip_date);
	
	if(strtotime($trip_date)<strtotime($row_last_end_date['end_date']))
	{
		$trip_date=date('Y-m-d', strtotime("+1 day", strtotime($row_last_end_date['end_date'])));
		$trip_date=$trip_date." H:i:s";
	}
}

$from = $_SESSION['create_trip_from_loc'];
$to = $_SESSION['create_trip_to_loc'];
$from_id = $_SESSION['create_trip_from_id'];
$to_id = $_SESSION['create_trip_to_id'];
$start_pincode = $_SESSION['create_trip_start_pincode']; 
$end_pincode = $_SESSION['create_trip_dest_pincode'];
$lr_type = $_SESSION['create_trip_lr_type'];
$vou_no = $_SESSION['create_trip_vou_no'];
$trip_km = $_SESSION['create_trip_km'];
$con1_id = $_SESSION['create_trip_con1_id'];
$con2_id = $_SESSION['create_trip_con2_id'];
$next_edit_branch = escapeString($conn,strtoupper($_POST['next_edit_branch']));
$diesel_consumption = $_SESSION['create_trip_diesel_required'];

$billing_type = "";
$bilty_freight = 0;
	
$chk_trip_duplicate = Qry($conn,"SELECT from_id,to_id FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1");

if(!$chk_trip_duplicate){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_trip_duplicate)>0)
{
	$row_duplicate = fetchArray($chk_trip_duplicate);
	
	if($row_duplicate['from']==$from_id AND $row_duplicate['to_id']==$to_id)
	{
		AlertError("Duplicate trip found !<br>$from to $to");
		echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
		exit();
	}
}

if($lr_type=='OWN')
{
	$vou_no_Array = explode('_', $vou_no);
	$vou_no = $vou_no_Array[0];
	$vou_id = $vou_no_Array[1];
}
else if($lr_type=='MKT')
{
	$vou_no_Array = explode('_', $vou_no);
	$vou_no = $vou_no_Array[0];
	$vou_id = $vou_no_Array[1];
	
	if($vou_no!='ATLOADING')
	{
		$chk_voucher = Qry($conn,"SELECT bilty_no,billing_type,tamt as freight FROM mkt_bilty WHERE id='$vou_id'");
			
		if(!$chk_voucher){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>alert('Error..');editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}

		if(numRows($chk_voucher)==0)
		{
			AlertError("Voucher not found..");
			echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}
		
		$row_voucher = fetchArray($chk_voucher);
		
		if($row_voucher['bilty_no']!=$vou_no)
		{
			AlertError("Voucher number not verified..");
			echo "<script>editBranchReset();$('#loadicon').fadeOut('slow');$('#trip_sub').attr('disabled',true);</script>";
			exit();
		}
		
		$billing_type = $row_voucher['billing_type'];
		$bilty_freight = $row_voucher['freight'];
	}
}

if($bilty_freight>0 and $billing_type=='TO_PAY')
{
	$mb_to_pay=1;
	$mb_amount = $bilty_freight;
}
else
{
	$mb_to_pay=0;
	$mb_amount=0;
}

if($lr_type=='EMPTY')
{
	$actual="0";
	$charge="0";
	$lrno="EMPTY";
	$lr_type2="0";
	$lr_vou_no = $lr_type;
	$lr_pending = "0";  // not zero if atloading trip
	$lr_pending_type = "0";  // atloading type--> market bilty : 2 or own bilty : 1
	$lr_vou_no_id=""; // Voucher table id
}
else
{
	$actual = escapeString($conn,($_POST['actual']));
	$charge = escapeString($conn,($_POST['charge']));
	$lrno = escapeString($conn,strtoupper($_POST['lrno']));
	
	if($lr_type=='CON20' || $lr_type=='CON40')
	{
		$lr_type2="0";
		$lr_vou_no = $lr_type;
		$lr_pending = "0";
		$lr_pending_type = "0";
		$lr_vou_no_id="";
	}
	else if($lr_type=='OWN' || $lr_type=='MKT')
	{
		$lr_vou_no = $vou_no;
		$lr_vou_no_id = $vou_id;
		$lr_type2="1";
		$lr_pending = "0";
		$lr_pending_type = "0";
		
		if($lr_vou_no=='ATLOADING')
		{
			$lr_pending = "1";
			$lrno = "ATLOADING";
			
			if($lr_type=='OWN'){
				$lr_pending_type = "1";
			}
			else{
				$lr_pending_type = "2";
			}
		}
	}
	else
	{
		errorLog("Invalid LR Type : $lr_type. Vehicle_No: $tno.",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Invalid Voucher Type.');
			window.location.href='./';
		</script>";
		exit();
	}
}

/*

************************************ Fix code starts *************************************

$search_for_fix_location = Qry($conn,"SELECT id FROM dairy.fix_lane WHERE from_id='$from_id' AND to_id='$to_id' 
AND is_active='1'");

if(!$search_for_fix_location){
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($search_for_fix_location)>0)
{
	$row_fix_lane = fetchArray($search_for_fix_location);
	$lane_id = $row_fix_lane['id'];
	
	if($lr_type=='EMPTY')
	{
		$search_for_fix = Qry($conn,"SELECT id,fix_diesel_type,diesel_value,adv_amount 
		FROM dairy.fix_lane_rules WHERE lane_id='$lane_id' AND IF(empty_loaded IN('-1','0','2'),'1','0')='1' 
		AND IF(tno='' || tno='$tno','1','0')='1' AND IF(truck_type='' || truck_type='$wheeler','1','0')='1' 
		AND is_active='1'");
		
		if(!$search_for_fix){
			echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		$search_rule_id="YES";
	}
	else if($lr_type=='OWN' AND $lr_vou_no!='ATLOADING')
	{
		$search_for_fix = Qry($conn,"SELECT id,fix_diesel_type,diesel_value,adv_amount 
		FROM dairy.fix_lane_rules WHERE lane_id='$lane_id' AND IF(empty_loaded IN('-1','1','2'),'1','0')='1' 
		AND IF(tno='' || tno='$tno','1','0')='1' AND IF(truck_type='' || truck_type='$wheeler','1','0')='1' 
		AND IF(consignor='' || consignor='$lr_con1','1','0')='1' 
		AND IF(consignee='' || consignee='$lr_con2','1','0')='1' AND is_active='1'");
		
		if(!$search_for_fix){
			echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		$search_rule_id="YES";
	}
	else if($lr_type=='MKT')
	{
		$search_for_fix = Qry($conn,"SELECT id,fix_diesel_type,diesel_value,adv_amount 
		FROM dairy.fix_lane_rules WHERE lane_id='$lane_id' AND IF(empty_loaded IN('-1','1','2'),'1','0')='1' 
		AND IF(tno='' || tno='$tno','1','0')='1' AND IF(truck_type='' || truck_type='$wheeler','1','0')='1' 
		AND is_active='1'");
		
		if(!$search_for_fix){
			echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		$search_rule_id="YES";
	}
	else
	{
		$search_rule_id="NO";
	}
}
else
{
	$search_rule_id="NO";
}

if($search_rule_id=='YES' AND numRows($search_for_fix)>0)
{
	$row_rule_id = fetchArray($search_for_fix);
	$rule_id = $row_rule_id['id'];
	
	$fix_diesel_type = $row_rule_id['fix_diesel_type'];
	$fix_diesel_value = $row_rule_id['diesel_value'];
	$fix_adv_amount = $row_rule_id['adv_amount'];
}
else
{
	$rule_id="NULL";
}

************************************ Fix code ends *************************************
*/
	
$chk_trip_no = Qry($conn,"SELECT trip_no,driver_code FROM dairy.trip WHERE tno='$tno'");

if(!$chk_trip_no){
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$new_driver = "0";

if(numRows($chk_trip_no)>0)
{
	$row_no=fetchArray($chk_trip_no);
	
	$trip_no = $row_no['trip_no'];
	$driver_code = $row_no['driver_code'];
	
	if($driver_code=='' || $driver_code==0)
	{
		$chk_driver_code = Qry($conn,"SELECT driver_code,model FROM dairy.own_truck WHERE tno='$tno'");
		
		if(!$chk_driver_code){
			echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		if(numRows($chk_driver_code)==0){
			echo "<script>alert('Error: driver not found..');window.location.href='./';</script>";
			exit();
		}
		
		$row_d=fetchArray($chk_driver_code);
		$model = $row_d['model'];
		$driver_code = $row_d['driver_code'];
	}
}
else
{	
	$new_driver = "1";

	$chk_driver_code=Qry($conn,"SELECT driver_code,model FROM dairy.own_truck WHERE tno='$tno'");
	
	if(!$chk_driver_code){
		echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($chk_driver_code)==0){
		echo "<script>alert('Error: driver not found..');window.location.href='./';</script>";
		exit();
	}
	
	$row_d = fetchArray($chk_driver_code);
	$model = $row_d['model'];
	$driver_code = $row_d['driver_code'];
		
	$trip_no_Qry = GetTripNo($conn);	
		
	if($trip_no_Qry==0 || $trip_no_Qry=='')
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error while generating trip number..');('#loadicon').fadeOut('slow');</script>";
		exit();
	}
		
	$trip_no = $trip_no_Qry;
				
	$chk_trip_no_duplicate = Qry($conn,"SELECT id FROM dairy.trip WHERE trip_no='$trip_no'");
	
	if(!$chk_trip_no_duplicate){
		echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}	
	
	if(numRows($chk_trip_no_duplicate)>0)
	{
		$trip_no_Qry = GetTripNo($conn);
		
		if($trip_no_Qry==0 || $trip_no_Qry=='')
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>alert('Error while generating trip number..');('#loadicon').fadeOut('slow');</script>";
			exit();
		}
			
		$trip_no = $trip_no_Qry;
	}
}

if($driver_code=='' || $driver_code==0)
{
	errorLog("Driver not found. Code: $driver_code.",$conn,$page_name,__LINE__);
	echo "<script>alert('Error: driver not found..');window.location.href='./';</script>";
	exit();
}

$from_addr_book_id = $_SESSION['create_trip_from_addr_id'];
$to_addr_book_id = $_SESSION['create_trip_to_addr_id'];
$is_running_trip = $_SESSION['create_trip_running_trip'];
$trip_start_date = $_SESSION['create_trip_start_date'];
$trip_end_date = $_SESSION['create_trip_end_date'];
$start_point = $_SESSION['create_trip_start_point'];
$end_point = $_SESSION['create_trip_end_point'];

$from_lat = explode(",",$start_point)[0];
$from_lng = explode(",",$start_point)[0];

$to_lat = explode(",",$end_point)[0];
$to_lng = explode(",",$end_point)[0];

$by_pass_route = $_POST['by_pass_route'];

$is_oxygen_lr="0";

StartCommit($conn);
$flag = true;

if($lr_type=='OWN' AND $lr_vou_no!='ATLOADING')
{
	$chk_oxygn_lr = Qry($conn,"SELECT oxygen_lr FROM freight_form_lr WHERE id='$lr_vou_no_id'");
	
	if(!$chk_oxygn_lr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($chk_oxygn_lr)==0){
		$flag = false;
		errorLog("Voucher not found. TruckNo: $tno. Branch: $branch. Id: $lr_vou_no_id. Vou_no: $lr_vou_no.",$conn,$page_name,__LINE__);
	}
	
	$row_oxn_chk = fetchArray($chk_oxygn_lr);
	
	if($row_oxn_chk['oxygen_lr']=="1")
	{
		$is_oxygen_lr="1";
	}
}

// if($_SESSION['create_trip_insert_from_poi']=='YES')
// {
	// $insert_from_addr = Qry($conn,"INSERT INTO address_book_consignor(consignor,from_id,pincode,_lat,_long,branch,branch_user,
	// timestamp) VALUES ('$con1_id','$from_id','$start_pincode','$from_lat','$from_lng','$branch','$_SESSION[user_code]','$timestamp')");
	
	// if(!$insert_from_addr){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	// }
		
	// $from_addr_book_id = getInsertID($conn);
// }

// if($_SESSION['create_trip_insert_to_poi']=='YES')
// {
	// $insert_to_addr = Qry($conn,"INSERT INTO address_book_consignee(consignee,to_id,pincode,_lat,_long,branch,branch_user,
	// timestamp) VALUES ('$con2_id','$to_id','$end_pincode','$to_lat','$to_lng','$branch','$_SESSION[user_code]','$timestamp')");
	
	// if(!$insert_to_addr){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	// }
		
	// $to_addr_book_id = getInsertID($conn);
// }

$chk_master_addr = Qry($conn,"SELECT id FROM master_addr_book WHERE from_id='$from_id' AND to_id='$to_id'");

if(!$chk_master_addr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_master_addr)==0)
{
	$insert_master_addr = Qry($conn,"INSERT INTO master_addr_book(from_id,to_id,from_addr_book,to_addr_book,polyline,distance,branch,branch_user,timestamp) 
	VALUES ('$from_id','$to_id','','','$polyline','$trip_km','$branch','$_SESSION[user_code]','$timestamp')");

	if(!$insert_master_addr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($con1_id=='303' AND $driver_code!=0 AND $driver_code!='')
{
	$driver_gps_naame = 3000;
}
else
{
	$driver_gps_naame = 0;
}

$qry = Qry($conn,"INSERT INTO dairy.trip(trip_no,tno,driver_code,branch,branch_user,edit_branch,from_station,to_station,from_id,to_id,
from_poi,from_pincode,to_poi,to_pincode,consignor_id,addr_book_id_consignor,consignee_id,addr_book_id_consignee,km,dsl_consumption,
lr_pending,lr_pending_type,pincode,act_wt,charge_wt,lr_type,lr_type2,oxygen_lr,mb_to_pay,mb_amount,lrno,driver_naame,driver_gps_naame,
start_poi_date,end_poi_date,date,timestamp,polyline) VALUES ('$trip_no','$tno','$driver_code','$branch','$_SESSION[user_code]','$next_edit_branch',
'$from','$to','$from_id','$to_id','$start_point','$start_pincode','$end_point','$end_pincode','$con1_id','$from_addr_book_id','$con2_id',
'$to_addr_book_id','$trip_km','$diesel_consumption','$lr_pending','$lr_pending_type','$end_pincode','$actual','$charge','$lr_vou_no',
'$lr_type2','$is_oxygen_lr','$mb_to_pay','$mb_amount','$lrno','$driver_gps_naame','$driver_gps_naame','$trip_start_date','$trip_end_date',
'$trip_date','$timestamp','$polyline')");

if(!$qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$trip_id = getInsertID($conn);

if($con1_id=='303' AND $driver_code!=0 AND $driver_code!='')
{
	$trans_id_Qry = GetTxnId_eDiary($conn,"NAAME");
	
	if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$trans_id = $trans_id_Qry;

	$insert_naame=Qry($conn,"INSERT INTO dairy.driver_naame (trip_id,trans_id,tno,driver_code,naame_type,amount,date,
	narration,branch,branch_user,timestamp) VALUES ('$trip_id','$trans_id','$tno','$driver_code','OTHER','$driver_gps_naame','$date',
	'VEDL-GPS','$branch','$_SESSION[user_code]','$timestamp')");

	if(!$insert_naame){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$select_amount=Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$select_amount){	
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($select_amount)==0)
	{
		$flag = false;
		errorLog("Error While Fetching Driver Balance. Code: $driver_code.",$conn,$page_name,__LINE__);
	}

	$row_amount = fetchArray($select_amount);
	$hold_amt = $row_amount['amount_hold']+$driver_gps_naame;
	$driver_id = $row_amount['id'];

	$insert_book=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,credit,balance,date,branch,
	timestamp) VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','DRIVER_NAAME','$driver_gps_naame','$hold_amt','$date','$branch',
	'$timestamp')");

	if(!$insert_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+'$driver_gps_naame' WHERE id='$driver_id'");

	if(!$update_hold_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_txn_id = Qry($conn,"UPDATE dairy.trip SET gps_naame_txn_id='$trans_id' WHERE id='$trip_id'");

	if(!$update_txn_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($by_pass_route=='YES')
{
	$chk_route = Qry($conn,"SELECT id FROM dairy.pending_master_lane WHERE from_id='$from_id' AND to_id='$to_id'");
	
	if(!$chk_route){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	if(numRows($chk_route)==0)
	{
		$insert_pending_route = Qry($conn,"INSERT INTO dairy.pending_master_lane(from_id,to_id,timestamp,tno,trip_id,branch,branch_user) VALUES 
		('$from_id','$to_id','$timestamp','$tno','$trip_id','$branch','$_SESSION[user_code]')");
		
		if(!$insert_pending_route){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
	}
}

$minus_dsl_left = Qry($conn,"UPDATE dairy.own_truck SET diesel_left=diesel_left-'$diesel_consumption',diesel_trip_id='$trip_id' 
WHERE tno='$tno' AND diesel_trip_id!='-1'");

if(!$minus_dsl_left){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($new_driver=="1")
{
	$update_trip_no = Qry($conn,"SELECT id,desct,trip_no FROM dairy.driver_book WHERE desct='DRIVER_UP' AND 
	driver_code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$update_trip_no){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($update_trip_no)>0)
	{
		$row_driver_up = fetchArray($update_trip_no);
		
		if($row_driver_up['trip_no']=='')
		{
			$update_trip_no_driver_book = Qry($conn,"UPDATE dairy.driver_book SET trip_no='$trip_no' WHERE id='$row_driver_up[id]'");
			
			if(!$update_trip_no_driver_book){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	
		}
	}	
	
	$driver_bal_qry = Qry($conn,"SELECT amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$driver_bal_qry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
	
	if(numRows($driver_bal_qry)>1)
	{
		$flag = false;
		errorLog("Multiple driver active. Code: $driver_code.",$conn,$page_name,__LINE__);
	}
	
	$row_bal = fetchArray($driver_bal_qry);
	$newbal = $row_bal['amount_hold'];
	
	$insert_data = Qry($conn,"INSERT INTO dairy.opening_closing(trip_no,tno,driver,opening,opening_branch,branch_user,opening_date,timestamp) 
	VALUES ('$trip_no','$tno','$driver_code','$newbal','$branch','$_SESSION[user_code]','$date','$timestamp')");
	
	if(!$insert_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
	
	$update_asset_form = Qry($conn,"UPDATE dairy.asset_form SET trip_no='$trip_no' WHERE driver_code='$driver_code'");
	
	if(!$update_asset_form){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
}
	
if($lr_type=='MKT' AND $lr_pending=="0")
{
	$update_vou_done = Qry($conn,"UPDATE mkt_bilty SET done='1',trip_id='$trip_id' WHERE id='$lr_vou_no_id'");
	
	if(!$update_vou_done){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Market Bilty Number not found in Mkt Bilty Table. VouNo : $lr_vou_no.",$conn,$page_name,__LINE__);
	}
}
else if($lr_type=='OWN' AND $lr_pending=="0")
{
	$update_vou_done=Qry($conn,"UPDATE freight_form_lr SET done='1',trip_id='$trip_id' WHERE frno='$lr_vou_no'");
	
	if(!$update_vou_done){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
		
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Vou Number not found in Freight memo LR Table. VouNo : $lr_vou_no.",$conn,$page_name,__LINE__);
	}
}

$fix_rule_id = "0";
$model_wise_fix = "0";

$chk_fix_lane = Qry($conn,"SELECT id,total_rules FROM dairy.fix_lane WHERE from_id='$from_id' AND to_id='$to_id' AND is_active='1'");
	
if(!$chk_fix_lane){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$consignee_multiple="";
	
if(numRows($chk_fix_lane)>0)
{
	$row_fix_chk = fetchArray($chk_fix_lane);
	
	if($row_fix_chk['total_rules']>0)
	{
		$lane_id = $row_fix_chk['id'];
		
		if($lr_type=='EMPTY')
		{
			$search_for_fix = Qry($conn,"SELECT id FROM dairy.fix_lane_rules WHERE lane_id='$lane_id' 
			AND IF(empty_loaded IN('-1','0','2'),'1','0')='1' AND IF(tno='$tno','1','0')='1' AND is_active='1'");
			
			if(!$search_for_fix){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($search_for_fix)>0)
			{
				$row_fix_rule = fetchArray($search_for_fix);
				$fix_rule_id = $row_fix_rule['id'];
			}
			else
			{
				$search_for_fix = Qry($conn,"SELECT id FROM dairy.fix_lane_rules WHERE lane_id='$lane_id' 
				AND IF(empty_loaded IN('-1','0','2'),'1','0')='1' AND IF(tno='' AND model='$model','1','0')='1' AND is_active='1'");
				
				if(!$search_for_fix){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(numRows($search_for_fix)>0)
				{
					$row_fix_rule = fetchArray($search_for_fix);
					$fix_rule_id = $row_fix_rule['id'];
					$model_wise_fix = "1";
				}
			}
		}
		else if($lr_type=='OWN' AND $lr_vou_no!='ATLOADING')
		{
			$get_all_con2_ids = Qry($conn,"SELECT DISTINCT con2_id FROM freight_form_lr WHERE frno='$lr_vou_no'");
			
			if(!$get_all_con2_ids){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($get_all_con2_ids) == 0)
			{
				$flag = false;
				errorLog("Voucher not found. Vou_No: $lr_vou_no.",$conn,$page_name,__LINE__);
			}
			
			$con2_id_arr = array();
			
			while($row_get_con2_id = fetchArray($get_all_con2_ids))
			{
				$con2_id_arr[] = $row_get_con2_id['con2_id'];
			}

			$comma_sep = implode(',',$con2_id_arr);
			$con2_id_arr = explode(',', $comma_sep);
			asort($con2_id_arr);
			$con2_id_arr = implode(',',$con2_id_arr);
			
			if( strpos($con2_id_arr,",") !== false )
			{
				$consignee_multiple = $con2_id_arr;
			}
			
			$search_for_fix = Qry($conn,"SELECT id FROM dairy.fix_lane_rules WHERE lane_id='$lane_id' 
			AND IF(empty_loaded IN('-1','1','2'),'1','0')='1' AND IF(tno='$tno','1','0')='1' 
			AND IF(consignor='0' || consignor='$con1_id','1','0')='1' AND IF(consignee='0' || multi_del_consignee='$con2_id_arr' || 
			consignee='$con2_id','1','0')='1' AND is_active='1'");
			
			if(!$search_for_fix){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($search_for_fix)>0)
			{
				$row_fix_rule = fetchArray($search_for_fix);
				$fix_rule_id = $row_fix_rule['id'];
			}
			else
			{
				$search_for_fix = Qry($conn,"SELECT id FROM dairy.fix_lane_rules WHERE lane_id='$lane_id' 
				AND IF(empty_loaded IN('-1','1','2'),'1','0')='1' AND IF(tno='' AND model='$model','1','0')='1' 
				AND IF(consignor='0' || consignor='$con1_id','1','0')='1' AND IF(consignee='0' || multi_del_consignee='$con2_id_arr' || 
				consignee='$con2_id','1','0')='1' AND is_active='1'");
				
				if(!$search_for_fix){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(numRows($search_for_fix)>0)
				{
					$row_fix_rule = fetchArray($search_for_fix);
					$fix_rule_id = $row_fix_rule['id'];
					$model_wise_fix = "1";
				}
				else
				{
					
				}
			}
		}
		else if($lr_type=='MKT' || $lr_type=='CON20' || $lr_type=='CON40')
		{
			$search_for_fix = Qry($conn,"SELECT id FROM dairy.fix_lane_rules WHERE lane_id='$lane_id' 
			AND IF(empty_loaded IN('-1','1','2'),'1','0')='1' AND IF(tno='$tno','1','0')='1' 
			AND consignor='0' AND consignee='0' AND is_active='1'");
			
			if(!$search_for_fix){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($search_for_fix)>0)
			{
				$row_fix_rule = fetchArray($search_for_fix);
				$fix_rule_id = $row_fix_rule['id'];
			}
			else
			{
				$search_for_fix = Qry($conn,"SELECT id FROM dairy.fix_lane_rules WHERE lane_id='$lane_id' 
				AND IF(empty_loaded IN('-1','1','2'),'1','0')='1' AND IF(tno='' AND model='$model','1','0')='1' 
				AND consignor='0' AND consignee='0' AND is_active='1'");
				
				if(!$search_for_fix){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(numRows($search_for_fix)>0)
				{
					$row_fix_rule = fetchArray($search_for_fix);
					$fix_rule_id = $row_fix_rule['id'];
					$model_wise_fix = "1";
				}
			}
		} 
	}
}

// errorLog("Truckno: $tno. model_wise_fix: $model_wise_fix. fix_rule_id: $fix_rule_id at Branch: $branch $timestamp",$conn,$page_name,__LINE__);

if($model_wise_fix=="1")
{
	$chk_model_escap = Qry($conn,"SELECT id FROM dairy.fix_lane_model_escap WHERE tno='$tno'");
	
	if(!$chk_model_escap){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($chk_model_escap) == 0)
	{
		if($fix_rule_id!='0')
		{
			$update_fix_id_in_trip = Qry($conn,"UPDATE dairy.trip SET fix_lane='$fix_rule_id',consignee_multiple='$consignee_multiple' WHERE id='$trip_id'");

			if(!$update_fix_id_in_trip){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$insert_fix_txn = Qry($conn,"INSERT INTO dairy.fix_txn_pending(tno,trip_id,lane_id,rule_id,lr_type,lr_vou_no,branch,branch_user,timestamp) 
			VALUES ('$tno','$trip_id','$lane_id','$fix_rule_id','$lr_type','$lr_vou_no','$branch','$_SESSION[user_code]','$timestamp')");
			
			if(!$insert_fix_txn){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
}
else
{
	if($fix_rule_id!='0')
	{
		$update_fix_id_in_trip = Qry($conn,"UPDATE dairy.trip SET fix_lane='$fix_rule_id',consignee_multiple='$consignee_multiple' WHERE id='$trip_id'");

		if(!$update_fix_id_in_trip){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$insert_fix_txn = Qry($conn,"INSERT INTO dairy.fix_txn_pending(tno,trip_id,lane_id,rule_id,lr_type,lr_vou_no,branch,branch_user,timestamp) 
		VALUES ('$tno','$trip_id','$lane_id','$fix_rule_id','$lr_type','$lr_vou_no','$branch','$_SESSION[user_code]','$timestamp')");
		
		if(!$insert_fix_txn){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

if($flag)
{ 
	unset($_SESSION['create_trip_diesel_required']); // dsl consumption session var.
		
	SessionDestroyCreateTrip();
	MySQLCommit($conn);
	closeConnection($conn);	
	// AlertRightCornerSuccess("Trip Created Successfully.");
	// SuccessAlertRedirect("Trip Created Successfully.","./");
	echo "<script>alert('Trip Created Successfully.');window.location.href='./';</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request..");
	echo "<script>$('#trip_sub').attr('disabled',false);</script>";
	exit();
}
?>