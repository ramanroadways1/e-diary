<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$tno = escapeString($conn,strtoupper($_POST['tno']));

if($tno!=$_SESSION['diary'])
{
	Redirect("Please log in again..","./logout.php");
	exit();
}

require_once("./check_cache.php");

$amount = escapeString($conn,$_POST['naame_amount']);
$naame_option = escapeString($conn,$_POST['naame_option']);
$date = date("Y-m-d");
$trip_id = escapeString($conn,strtoupper($_POST['trip_id']));
$narration = escapeString($conn,($_POST['naame_narration']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertError("Trip not found..");
	errorLog("Trip id not found. id: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#naame_button').attr('disabled',false);</script>";
	exit();
}

$check_trip = Qry($conn,"SELECT t.id,t.tno,t.driver_code,t.trip_no,d.name as driver_name 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
WHERE t.id='$trip_id'");

if(!$check_trip){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#naame_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertError("Trip not found..");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#naame_button').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$driver_name = $row_trip['driver_name'];
$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];

$claim_vou_no="";

if($naame_option=='CLAIM')
{
	$lr_type = escapeString($conn,($_POST['lr_type']));
	$lrno = escapeString($conn,strtoupper($_POST['lrno']));
	
	if($lr_type=="")
	{
		AlertError("LR Type is not valid !");
		echo "<script>$('#naame_button').attr('disabled',false);</script>";
		exit();	
	}	
	
	if($lrno=="")
	{
		AlertError("LR number is not valid !");
		echo "<script>$('#naame_button').attr('disabled',false);</script>";
	}
	
	if($lr_type=='MARKET')
	{
		$check_bilty_bal=Qry($conn,"SELECT balance FROM dairy.bilty_balance WHERE bilty_no='$lrno'");

		if(!$check_bilty_bal){
			AlertError("Error..");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#naame_button').attr('disabled',false);</script>";
			exit();
		}
		
		if(numRows($check_bilty_bal)==0)
		{
			AlertError("Market bilty not found !");
			echo "<script>$('#naame_button').attr('disabled',false);</script>";
			exit();	
		}
		
		$row = fetchArray($check_bilty_bal);
		$new_balance = $row['balance']-$amount;
	}
	else
	{
		$check_bilty_bal=Qry($conn,"SELECT claim_id,pod_id,vou_no,balance FROM claim_book_trans WHERE lrno='$lrno' AND vehicle_type='OWN' AND main_entry='1'");

		if(!$check_bilty_bal){
			AlertError("Error..");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#naame_button').attr('disabled',false);</script>";
			exit();
		}
		
		if(numRows($check_bilty_bal)==0)
		{
			AlertError("Claim record not found !");
			echo "<script>$('#naame_button').attr('disabled',false);</script>";
			exit();	
		}
		
		$row = fetchArray($check_bilty_bal);
		$new_balance = $row['balance']-$amount;
		$claim_vou_no = $row['vou_no'];
		$claim_claim_id = $row['claim_id'];
		$claim_pod_id = $row['pod_id'];
	}
}
else
{
	$lr_type = "";
	$lrno = "";
}

if($naame_option=='FREIGHT')
{
	$lr_type_freight=escapeString($conn,($_POST['lr_type_freight']));
	$lrno_freight = escapeString($conn,strtoupper($_POST['lrno_freight']));
	
	if($lr_type_freight=="")
	{
		AlertError("LR Type is not valid !");
		echo "<script>$('#naame_button').attr('disabled',false);</script>";
		exit();	
	}	
	
	if($lrno_freight=="")
	{
		AlertError("LR number is not valid !");
		echo "<script>$('#naame_button').attr('disabled',false);</script>";
		exit();	
	}
	
	if($lr_type_freight=='MARKET')
	{
		$check_bilty_bal=Qry($conn,"SELECT balance FROM dairy.bilty_balance WHERE bilty_no='$lrno_freight'");

		if(!$check_bilty_bal){
			AlertError("Error..");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#naame_button').attr('disabled',false);</script>";
			exit();
		}
		
		if(numRows($check_bilty_bal)==0)
		{
			AlertError("Market bilty not found !");
			echo "<script>$('#naame_button').attr('disabled',false);</script>";
			exit();	
		}
		
		$row = fetchArray($check_bilty_bal);
		$new_balance = $row['balance']-$amount;
	}
}
else
{
	$lr_type_freight = "";
	$lrno_freight = "";
}

if($naame_option=='DIESEL')
{
	$qty=escapeString($conn,strtoupper($_POST['naame_qty']));
	$rate=escapeString($conn,strtoupper($_POST['naame_rate']));
}
else
{
	$qty=0;
	$rate=0;	
}

if($amount<=0 || $amount=='')
{
	AlertError("Invalid amount !");
	echo "<script>$('#naame_button').attr('disabled',false);</script>";
	exit();	
}
	
$trans_id_Qry = GetTxnId_eDiary($conn,"NAAME");
	
if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
	AlertError("Error..");
	echo "<script>$('#naame_button').attr('disabled',false);</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$trans_id = $trans_id_Qry;

$claim_narration = "Claim naame-> Veh.no: $tno, Driver:$driver_name($driver_code)-> $narration";
$claim_naame_narration = "Claim naame-> Veh.no: $tno, Driver:$driver_name($driver_code)-> $narration";

StartCommit($conn);
$flag = true;
	
if($lr_type=='MARKET')
{
	$insert_balance = Qry($conn,"INSERT INTO dairy.bilty_book(bilty_no,trans_id,branch,branch_user,date,debit,balance,narration,timestamp) 
	VALUES ('$lrno','$trans_id','$branch','$_SESSION[user_code]','$date','$amount','$new_balance','$claim_narration','$timestamp')");		
		
	if(!$insert_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$udpate_balance = Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=rcvd+'$amount',balance='$new_balance' WHERE bilty_no='$lrno'");		
		
	if(!$udpate_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
}
else if($lr_type=='OWN')
{
	$insert_balance = Qry($conn,"INSERT INTO claim_book_trans(claim_id,pod_id,lrno,vehicle_type,vou_no,trans_id,branch,branch_user,date,
	debit,balance,narration,timestamp) VALUES ('$claim_claim_id','$claim_pod_id','$lrno','OWN','$claim_vou_no','$trans_id','$branch',
	'$_SESSION[user_code]','$date','$amount','$new_balance','$claim_naame_narration','$timestamp')");		
		
	if(!$insert_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	// $update_claim_amount = Qry($conn,"UPDATE claim_records_admin SET rcvd_amount=rcvd_amount+'$amount',
	// balance_amount=balance_amount-'$amount' WHERE lrno='$lrno'");		
		
	// if(!$update_claim_amount){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	// }
}

if($lr_type_freight=='MARKET')
{
	$insert_balance = Qry($conn,"INSERT INTO dairy.bilty_book(bilty_no,trans_id,branch,branch_user,date,debit,balance,narration,timestamp) 
	VALUES ('$lrno_freight','$trans_id','$branch','$_SESSION[user_code]','$date','$amount','$new_balance',
	'Claim naame-> Veh.no: $tno, Driver:$driver_name($driver_code)-> $narration','$timestamp')");		
		
	if(!$insert_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$udpate_balance = Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=rcvd+'$amount',balance='$new_balance' WHERE bilty_no='$lrno_freight'");		
		
	if(!$udpate_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
}

if($lrno!='')
{
	$lrno_freight_claim=$lrno;
	$claim_vou_no=$claim_vou_no;
}
else if($lrno_freight!='')
{
	$lrno_freight_claim=$lrno_freight;
	$claim_vou_no="";
}
else
{
	$lrno_freight_claim="";
	$claim_vou_no="";
}

$insert_naame=Qry($conn,"INSERT INTO dairy.driver_naame (trip_id,trans_id,tno,lrno,claim_vou_no,driver_code,naame_type,qty,rate,amount,date,narration,
branch,branch_user,timestamp) VALUES ('$trip_id','$trans_id','$tno','$lrno_freight_claim','$claim_vou_no','$driver_code','$naame_option','$qty','$rate','$amount','$date','$narration',
'$branch','$_SESSION[user_code]','$timestamp')");

if(!$insert_naame)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$select_amount=Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");
if(!$select_amount)
{	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($select_amount)==0)
{
	$flag = false;
	errorLog("Error While Fetching Driver Balance. Code: $driver_code.",$conn,$page_name,__LINE__);
}

$row_amount=fetchArray($select_amount);
$hold_amt=$row_amount['amount_hold']+$amount;
$driver_id=$row_amount['id'];

$insert_book=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,credit,balance,date,branch,
timestamp) VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','DRIVER_NAAME','$amount','$hold_amt','$date','$branch',
'$timestamp')");

if(!$insert_book)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+$amount WHERE id='$driver_id'");

if(!$update_hold_amount)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_trip=Qry($conn,"UPDATE dairy.trip SET driver_naame=driver_naame+'$amount' WHERE id='$trip_id'");
if(!$update_trip)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($naame_option=='DIESEL')
{
	$update_available_qty = Qry($conn,"UPDATE dairy.own_truck SET diesel_left = diesel_left-'$qty' WHERE tno='$tno' AND diesel_trip_id!='-1'");

	if(!$update_available_qty)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error..");
	echo "<script>$('#naame_button').attr('disabled',false);</script>";
	exit();
}

echo "<script>
		$('#naame_button').attr('disabled',false);
		$('#NaameForm')[0].reset();
		document.getElementById('hide_naame').click();
		TripAdv('#$trip_id');
		LoadDriverBalance('$driver_code');
		$('#loadicon').fadeOut('slow');	
		
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: 'Success ! Naame entry.',
			showConfirmButton: false,
			timer: 2000
		})
	</script>";
exit();
?>