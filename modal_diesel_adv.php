<?php
require_once("./connect.php");

$trip_id = escapeString($conn,($_POST['trip_id']));
?>
<script type="text/javascript">
$(document).ready(function (e) {
$("#DieselAdvForm").on('submit',(function(e) {
$("#loadicon").show();
$("#diesel_adv_button").attr("disabled", true); 
e.preventDefault();
	$.ajax({
	url: "./diesel_adv.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
  $(function() {  
      $("#card_no").autocomplete({
			source: function(request, response) { 
			if(document.getElementById('fuel_company').value!=''){
                 $.ajax({
                  url: "../b5aY6EZzK52NA8F/autofill/get_diesel_cards.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   company: document.getElementById('fuel_company').value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Please select company first !');
				$("#card_no").val('');
				$("#cardno2").val('');
				$("#phy_card_no").val('');
				$("#rilpre").val('');
              }

              },
              select: function (event, ui) { 
               $('#card_no').val(ui.item.value);   
               $('#cardno2').val(ui.item.dbid);     
			   $('#phy_card_no').val(ui.item.phy_card_no);
			    $("#rilpre").val(ui.item.rilpre);
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#cardno2").val('');
				$("#phy_card_no").val('');
				$("#rilpre").val('');
                alert('Card No. does not exist !'); 
              } 
              },
			}); 
      }); 
</script>

<?php
$get_trip_data = Qry($conn,"SELECT t.tno,t.trip_no,t.driver_code,t.fix_lane,d.name as driver_name FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code 
WHERE t.id='$trip_id'");

if(!$get_trip_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_trip_data)==0)
{
	echo "<script>
		alert('Trip not found..');
		window.location.href='./';
	</script>";
	exit();
}

$row_trip = fetchArray($get_trip_data);

$tno = $row_trip['tno'];
$trip_no = $row_trip['trip_no'];
$driver_code = $row_trip['driver_code'];
$driver_name = $row_trip['driver_name'];

$get_diesel_rate_max = Qry($conn,"SELECT qty,rate,amount FROM _diesel_max_rate WHERE market_own='OWN'");
if(!$get_diesel_rate_max){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
				
if(numRows($get_diesel_rate_max)>0)
{
	$row_dsl_max_rate = fetchArray($get_diesel_rate_max);
	$dsl_max_rate=$row_dsl_max_rate['rate'];
	$dsl_max_amount1=$row_dsl_max_rate['amount'];
	$dsl_max_qty1=$row_dsl_max_rate['qty'];
}
else
{
	$dsl_max_rate=95;
	$dsl_max_amount1=38000;
	$dsl_max_qty1=400;
}

// if($row_trip['fix_lane']!="0")
// {
	// $get_fix_qty = Qry($conn,"SELECT diesel_left_fix FROM dairy.own_truck WHERE tno='$tno'");

	// if(!$get_fix_qty){
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// exit();
	// }

	// if(numRows($get_fix_qty)==0)
	// {
		// echo "<script>
			// alert('Vehicle not found..');
			// window.location.href='./';
		// </script>";
		// exit();
	// }
	
	// $row_fix_qty = fetchArray($get_fix_qty);
	// $dsl_max_qty1 = $row_fix_qty['diesel_left_fix'];
// }

// $get_dsl_left_db = Qry($conn,"SELECT diesel_tank_cap,diesel_left,diesel_trip_id FROM dairy.own_truck WHERE tno='$tno'");
// if(!$get_dsl_left_db){
	// echo getMySQLError($conn);
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// exit();
// }
	
// if(numRows($get_dsl_left_db)==0)
// {
	// errorLog("Vehicle record not found. VehicleNo: $tno.",$conn,$page_name,__LINE__);
	// echo "<script>window.location.href='/';</script>";
	// exit();
// }

// $row_dsl_left_db = fetchArray($get_dsl_left_db);
	
// if($row_dsl_left_db['diesel_trip_id']!='-1')
// {
	// $dsl_max_qty1 = sprintf("%.2f",$row_dsl_left_db['diesel_tank_cap'] - $row_dsl_left_db['diesel_left']);
// }
?>

<form id="DieselAdvForm" autocomplete="off" style="font-size:13px">      
  <div class="modal fade" id="DieselAdvModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
	  
	  <div class="modal-header bg-primary">
			<span style="font-size:13px">Diesel Entry <span style="font-size:13px">( डीजल एंट्री )</span> :</span>
		</div>
	<div class="modal-body">
		<div class="row">
		<input type="hidden" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			
			<script type="text/javascript">
                     function sum1() 
					 {
						if($("#qty").val()=='')
						{
							$("#qty").val('0');
						}
						
						if($("#rate").val()=='')
						{
							$("#rate").val('0');
						}
							
						$("#dsl_amt").val(Math.round(Number($("#qty").val()) * Number($("#rate").val())).toFixed(2));
					}
					
					function sum2() 
					{
						if($("#qty").val()=='')
						{
							$("#qty").val('0');
						}
						
						if($("#rate").val()=='')
						{
							$("#rate").val('0');
						}
							
						$("#dsl_amt").val(Math.round(Number($("#qty").val()) * Number($("#rate").val())).toFixed(2));
					}
					 
					 function sum3() 
					 {
						if($("#qty").val()=='')
						{
							$("#qty").val('0');
						}
						
						if($("#rate").val()=='')
						{
							$("#rate").val('0');
						}
						
						if($("#dsl_amt").val()=='')
						{
							$("#dsl_amt").val('0');
						}
						
						$("#rate").val((Number($("#dsl_amt").val()) / Number($("#qty").val())).toFixed(2));
					}
					
					 function ResetAll() 
					 {
						$("#b_amt").val('');
						$("#b_rate").val('');
                     }
                </script>
			<!--
			<div class="form-group col-md-4">
				<label>Date. <sup><font color="red">*</font></sup></label>
				<input type="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" min="2019-12-03" max="" name="diesel_date" class="form-control" required />
			</div>
			-->
			<div class="form-group col-md-6">
				<label>Pump/Card. <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px" onchange="CardPump(this.value)" id="type1" name="pump_card" class="form-control" required>
					<option style="font-size:12px" value="">Select option</option>
					<option style="font-size:12px" value="CARD">CARD</option>
					<option style="font-size:12px" value="OTP">OTP</option>
					<option style="font-size:12px" value="PUMP">PUMP</option>
				</select>
			</div>
			
			   <script>
			   function ChkPumpInput()
			   {
				  if($('#fuel_company').val()=='')
				  {
					  alert('Select Fuel Company First');
					  $('#card_no').val('');
					  $('#mobile_no').val('');
					  $('#phy_card_no').val('');
				  }					  
			   }
			   </script>
			   
			<script>
			function CardPump(card)
			{
				$('#fuel_company').val('');
				$('#card_no').val("");
				$('#cardno2').val("");
				$('#mobile_no').val("");
				$('#phy_card_no').val("");
				
				$('#qty').val("");
				$('#rate').val("");
				$('#dsl_amt').val("");
				
				if(card=='')
				{
					$('#pump_div').hide();	
					$('#pump_name').attr('required',true);	
					$('#pump_company').attr('required',true);
					
					$('#fuel_div').hide();	
					$('#fuel_company').attr('required',true);	
					
					$('#card_div').hide();	
					$('#card_no').attr('required',true);	
					$('#cardno2').attr('required',true);	
					
					$('#mobile_div').hide();	
					$('#mobile_no').attr('required',true);	
					
					$('#phy_card_div').hide();	
			        $('#phy_card_no').attr('required',false);
					
					$('#bill_no_div').hide();	
			        $('#bill_no').attr('required',false);
					
				}
				else if(card=='CARD' || card=='OTP')
				{
					if(card=='CARD')
					{
						$('#mobile_div').hide();
						$('#mobile_no').attr("required",false);
						
						$('#card_div').show();
						$('#card_no').attr("required",true);
						$('#cardno2').attr("required",true);
						
						$('#phy_card_div').show();	
						$('#phy_card_no').attr('required',true);
					}
					else
					{
						$('#mobile_div').show();
						$('#mobile_no').attr("required",true);
						
						$('#card_div').hide();
						$('#card_no').attr("required",false);
						$('#cardno2').attr("required",false);
						
						$('#phy_card_div').hide();	
						$('#phy_card_no').attr('required',false);
					}
					
					$('#pump_div').hide();	
					$('#pump_name').attr('required',false);	
					$('#pump_company').attr('required',false);
					
					$('#fuel_div').show();	
					$('#fuel_company').attr('required',true);	
					
					$('#bill_no_div').hide();	
			        $('#bill_no').attr('required',false);
					 
				}
				else if(card=='PUMP')
				{
					$('#pump_div').show();	
					$('#pump_name').attr('required',true);	
					$('#pump_company').attr('required',true);
					
					$('#fuel_div').hide();	
					$('#fuel_company').attr('required',false);	
					
					$('#card_div').hide();	
					$('#card_no').attr('required',false);	
					$('#cardno2').attr('required',false);	
					
					$('#mobile_div').hide();	
					$('#mobile_no').attr('required',false);	
					
					$('#phy_card_div').hide();	
					$('#phy_card_no').attr('required',false);
					
					$('#bill_no_div').show();	
			        $('#bill_no').attr('required',false);
				}
			}
			</script>
			
			<div style="display:none" id="pump_div" class="form-group col-md-6">
				<label>Pump Name. <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px" onchange="SetPumpCode(this.value)" id="pump_name" name="pump_name" class="form-control" required>
					<option style="font-size:12px" value="">Select option</option>
					<?php
					$q_pump=Qry($conn,"SELECT name,code,comp,consumer_pump FROM dairy.diesel_pump_own WHERE branch='$branch' AND 
					active='1'");
					
					if(!$q_pump){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						exit();
					}

					if(numRows($q_pump)>0)
					{
						while($row_p=fetchArray($q_pump))
						{
							// if($row_p['consumer_pump']=="1")
							// {
								// echo "<option disabled='disabled' value='".$row_p['code']."-".$row_p['comp']."'>$row_p[name]</option>";	
							// }
							// else							
							// {
								echo "<option style='font-size:12px' value='".$row_p['code']."-".$row_p['comp']."'>$row_p[name]</option>";	
							// }
						}
					}
					?>
				</select>
			</div>
			
			<script>
			function SetPumpCode(code)
			{
				$('#qty').val("");
				$('#rate').val("");
				$('#dsl_amt').val("");
				
				var parts = code.split('-', 2);
				$('#pump_company').val(parts[1]);
				
				$("#loadicon").show();
				$.ajax({
					url: "get_consumer_pump_data.php",
					method: "post",
					data:'code=' + parts[0], 
					success: function(data){
					$("#resultChkPump").html(data);
				}})
			}
			</script>
			
			<input type="hidden" name="pump_company" id="pump_company">
			
			<div class="form-group col-md-6">
				<label>QTY. <sup><font color="red">*</font></sup></label>
				<input type="number" max="<?php echo $dsl_max_qty1; ?>" min="1" name="qty" step="any" required oninput="sum2();" id="qty" class="form-control" />
			</div>
			
			<div class="form-group col-md-6">
				<label>Rate. <sup><font color="red">*</font></sup></label>
				<input type="number" min="1" max="<?php echo $dsl_max_rate; ?>" name="rate" step="any" required oninput="sum1();" id="rate" class="form-control" />
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input min="1" max="<?php echo $dsl_max_amount1; ?>" type="number" oninput="sum3();" name="amount" id="dsl_amt" class="form-control" required />
			</div>
			
			
			<div id="fuel_div" style="display:none" class="form-group col-md-6">
				<label>Fuel Company. <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px" id="fuel_company" name="fuel_company" onchange="$('#card_no').val('');$('#phy_card_no').val('')" class="form-control" required>
					<option style="font-size:12px" value="">Select option</option>
			<?php
			$get_fuel_comp_22 = Qry($conn,"SELECT name FROM diesel_api.dsl_company WHERE status='1' ORDER by name ASC");
			if(!$get_fuel_comp_22){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while fetching diesel company.","./");
				exit();
			}

			while($r_f_c2 = fetchArray($get_fuel_comp_22))
			{
				echo "<option style='font-size:12px' value='$r_f_c2[name]'>$r_f_c2[name]</option>";
			}
			?>
				</select>
			</div>
			
			 <div style="display:none" id="card_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Card No. <font color="red"><sup>*</sup></font></label>
					   <input type="text" oninput="ChkPumpInput();this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" name="card_no" id="card_no" class="form-control" required />
					   <input type="hidden" id="cardno2" name="cardno2">
                  </div>
               </div>
			   
			   <div style="display:none" id="mobile_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Mobile No. <font color="red"><sup>*</sup></font></label>
					   <input type="text" maxlength="10" minlength="10" oninput="ChkPumpInput();this.value=this.value.replace(/[^0-9]/,'')" name="mobile_no" id="mobile_no" class="form-control" required />
				  </div>
               </div>
			   
			    <div style="display:none" id="phy_card_div" style="display:none" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Virtual Id. <font color="red"><sup>*</sup></font></label>
					   <input type="text" name="phy_card_no" id="phy_card_no" class="form-control" readonly required />
				  </div>
               </div>
			   
			    <div style="display:none" id="bill_no_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Bill/Slip No. </label>
					   <input type="text" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^A-Za-z 0-9,-]/,'')" name="bill_no" id="bill_no" class="form-control" required />
				  </div>
               </div>
			
			<input type="hidden" name="date" value="<?php echo date("Y-m-d"); ?>" class="form-control" readonly required />
			
			<input type="hidden" name="trip_id" value="<?php echo $trip_id; ?>" />
			<input type="hidden" name="trip_no" value="<?php echo $trip_no; ?>" />
			<input type="hidden" name="driver_name" value="<?php echo $driver_name; ?>" />
			<input type="hidden" name="driver_code" value="<?php echo $driver_code; ?>" />
			 <input type="hidden" name="rilpre" id="rilpre">
			
		</div>
        </div>
		
		<div id="resultChkPump"></div>
		
        <div class="modal-footer">
			<button <?php if($driver_code=="") {echo "disabled"; } ?> type="submit" id="diesel_adv_button" class="btn btn-sm btn-danger">Submit</button>
		<button type="button" class="btn btn-sm btn-primary" id="hide_diesel" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>
  
<!------------------ Diesel Advance Modal ENDSSSSSSSS --------------------------> 

<button style="display:none" data-toggle="modal" id="dsl_modal_open_btn_back" data-target="#DieselAdvModal"></button>

<script>
$('#dsl_modal_open_btn_back')[0].click();
$('#loadicon').fadeOut('slow');
</script>