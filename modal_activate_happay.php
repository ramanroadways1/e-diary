<div class="modal fade" id="HappayActivateModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
     
	 <form id="ActiveHappayForm" autocomplete="off"> 
      <div class="modal-content" style="">
	  
	  <div class="modal-header bg-primary">
			<span style="font-size:13px">Activate Happay Card : <?php echo $tno; ?></span>
		</div>
		
		<div class="modal-body">
		<div class="row">
			
		<div class="form-group col-md-12">
			<label>Enter Card No. <sup><font color="red">* (Last 8 Digits)</font></sup></label>
			<div class="row">
				<div class="form-group col-md-3">
					<input type="text" readonly value="XXXX" class="form-control" required />
				</div>
				<div class="form-group col-md-3">
					<input type="text" readonly value="XXXX" class="form-control" required />
				</div>
				<div class="form-group col-md-3">
					<input type="text" id="HappayCardno1" oninput="this.value=this.value.replace(/[^0-9]/,'');ChkInputlength()" maxlength="4" name="card_no1" class="form-control" required />
				</div>
				<div class="form-group col-md-3">
					<input type="text" id="HappayCardno2"  oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="4" name="card_no2" class="form-control" required />
				</div>
			</div>
		</div>	
		
		</div>
        </div>

<script>     
function ChkInputlength(){
  if($('#HappayCardno1').val().length==$("#HappayCardno1").attr("maxlength")){
	$('#HappayCardno2').focus();
 }
}
</script>     
	 
	  <input type="hidden" value="<?php echo $driver_code_value; ?>" name="driver_code">
	  
	  <div class="modal-footer">
          <button type="submit" id="button_happay_active" class="pull-left btn btn-sm btn-danger">Activate Happay Card</button>
          <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
	 </div>
	 
      </div>
      </form>
    </div>
  </div>
  
 <div id="result_happay_active1"></div>
  
<script type="text/javascript">
$(document).ready(function (e) {
$("#ActiveHappayForm").on('submit',(function(e) {
$("#loadicon").show();
$("#button_happay_active").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./activate_happay_card.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_happay_active1").html(data);
	},
	error: function() 
	{} });}));});
</script> 