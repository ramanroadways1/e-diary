<a style="display:none" id="AssetModalButton" data-toggle="modal" data-target="#AssetFormModal"></a>

<!--------------------------------- DRIVER ASSET FORM MODAL START ----------------------------------------->
<form id="AssetForm" autocomplete="off">

<div class="modal fade" id="AssetFormModal" role="dialog" data-backdrop="static" data-keyboard="false" 
	style="left: 0;top: 0;background-image:url(truck_bg.jpg); background-repeat: no-repeat;  position: fixed;width: 100%;height: 100%; 
	background-size: cover;">

    <div class="modal-dialog modal-lg">
      <div class="modal-content" style="">
	 
		<div class="modal-header bg-primary">
			<span style="font-size:13px">Asset Form : <?php echo $tno; ?></span>
		</div>
		
        <div class="modal-header">
			<span style="font-size:13px">Driver Name: <?php echo $driver_name; ?>, 
			Mobile: <?php echo $driver_mobile=$row_d['mobile']; ?>, 
			Lic. No: <?php echo $driver_lic_no=$row_d['lic']; ?></span>
		</div>
		
<script type="text/javascript">
$(document).ready(function (e) {
$("#AssetForm").on('submit',(function(e) {
$("#loadicon").show();
$("#asset_submit").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./asset_form_save_up.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_asset").html(data);
	},
	error: function() 
	{} });}));});
</script>		
		
<div class="modal-body">
<div id="result_asset"></div>
 <div class="row">
 <div class="form-group col-md-12 table-responsive">
	<table <?php if(isMobile()){ echo "style='font-size:11px'"; } else { echo "style='font-size:13px'"; } ?> class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>सामान </th>
			<th>सामान दिया</th>
			<th <?php if(isMobile()){ echo "style='width:30%'"; } ?>>मात्रा </th>
			<th>मूल्य </th>
		</tr>
	</thead>
	
<script>	
function ItemMinus(code,min_qty,rate)
{
	var ext_value=$('#asset_qty'+code).val();
	
	var new_value = ext_value-1;
	
	if(new_value<min_qty)
	{
		alert('Min value is : '+min_qty);
		$('#asset_qty'+code).val(ext_value);
		$('#asset_qty_box'+code).html(ext_value);
		
		var new_rate = Math.round(rate/min_qty);
		var new_rate_set = Math.round(ext_value*new_rate);
		$('#rate_asset'+code).html(new_rate_set);
		$('#rate_asset_box'+code).val(new_rate_set);
	}
	else
	{
		$('#asset_qty'+code).val(new_value);
		$('#asset_qty_box'+code).html(new_value);
		
		if(rate==0)
		{
			$('#rate_asset'+code).html('0');
			$('#rate_asset_box'+code).val('0');
		}
		else
		{
			var new_rate = Math.round(rate/min_qty);
			var new_rate_set = Math.round(new_value*new_rate);
			$('#rate_asset'+code).html(new_rate_set);
			$('#rate_asset_box'+code).val(new_rate_set);
		}
	}
}

function ItemPlus(code,min_qty,rate)
{
	var ext_value=$('#asset_qty'+code).val();
	
	var new_value = ++ext_value;
	
	$('#asset_qty'+code).val(new_value);
	$('#asset_qty_box'+code).html(new_value);
	
	if(rate==0)
	{
		$('#rate_asset'+code).html('0');
		$('#rate_asset_box'+code).val('0');
	}
	else
	{
		var new_rate = Math.round(rate/min_qty);
		var new_rate_set = Math.round(new_value*new_rate);
		$('#rate_asset'+code).html(new_rate_set);
		$('#rate_asset_box'+code).val(new_rate_set);
	}
}

function ChkQty(elem,item_code,super_user,dbQty,dbRate)
{
	if(elem==0)
	{
		if(super_user==1)
		{
			$('#MinusBtn'+item_code).hide();
			$('#PlusBtn'+item_code).hide();
		}
		
		$('#asset_qty'+item_code).val('0');
		$('#asset_qty_box'+item_code).html('0');
		$('#rate_asset'+item_code).html('0');
		$('#rate_asset_box'+item_code).val('0');
	}
	else
	{
		if(super_user==1)
		{
			$('#MinusBtn'+item_code).show();
			$('#PlusBtn'+item_code).show();
		}
		
		$('#asset_qty'+item_code).val(dbQty);
		$('#asset_qty_box'+item_code).html(dbQty);
		$('#rate_asset'+item_code).html(dbRate);
		$('#rate_asset_box'+item_code).val(dbRate);
	}
}
</script>	

	<?php
	mysqli_set_charset($conn, 'utf8');
	
	$qry_asset_item = Qry($conn,"SELECT * FROM dairy.asset_form_items ORDER BY id ASC");
	
	if(!$qry_asset_item){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}	
			
	if(numRows($qry_asset_item)>0)
	{
		while($row_asset = fetchArray($qry_asset_item))
		{
			$chk_access=Qry($conn,"SELECT id FROM dairy.asset_form_rule WHERE item_code='$row_asset[code]' AND branch='$branch'");
			
			if(!$chk_access){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				exit();
			}	
			
			if(numRows($chk_access)==0)
			{
				$super_user_branch=0;
				
				$item_button="<input style='display:none' id='asset_qty$row_asset[code]' min='$row_asset[qty]' type='number' 
				value='$row_asset[qty]' name='item_qty[]' readonly>
				<span id='asset_qty_box$row_asset[code]'>$row_asset[qty]</span>";
			}
			else
			{
				$super_user_branch=1;
				
				$item_button="<button id='MinusBtn$row_asset[code]' onclick=ItemMinus('$row_asset[code]','$row_asset[qty]','$row_asset[rate]') type='button' class='btn btn-xs btn-warning'> - </button>
				&nbsp; <span id='asset_qty_box$row_asset[code]'>$row_asset[qty]</span> &nbsp;
				<button id='PlusBtn$row_asset[code]' onclick=ItemPlus('$row_asset[code]','$row_asset[qty]','$row_asset[rate]') type='button' class='btn btn-xs btn-success'> + </button>
				<input id='asset_qty$row_asset[code]' style='display:none' min='$row_asset[qty]' type='number' 
				value='$row_asset[qty]' name='item_qty[]' readonly>";
			}
			
		if($row_asset['doc']==1)
		{
			$select_box = "
			<label class='radio-inline'><input type='radio' value='1' name='item_sel[".$row_asset['code']."]' required>Original</label>";
			
			if(isMobile()){
				$select_box.="<br><br>";
			}
			
			$select_box.="<label class='radio-inline'><input type='radio' value='0' name='item_sel[".$row_asset['code']."]' required>Xerox</label>";
				
				// $select_box = "<select style='font-size:12px;height:28px !important' id='item$row_asset[code]' class='form-control' name='item_sel[]' required>
							// <option style='font-size:12px;' value=''>--SELECT--</option>
							// <option style='font-size:12px;' value='1'>ORIGINAL</option>
							// <option style='font-size:12px;' value='0'>XEROX</option>
				// </select>";
		}
		else
		{
			$select_box = "
			<label class='radio-inline'><input type='radio' value='1' name='item_sel[".$row_asset['code']."]' 
			onchange=ChkQty(this.value,'$row_asset[code]','$super_user_branch','$row_asset[qty]','$row_asset[rate]') required>Yes</label>";
			
			if(isMobile()){
				$select_box.="<br><br>";
			}
			
			$select_box.="<label class='radio-inline'><input type='radio' value='0' name='item_sel[".$row_asset['code']."]' 
			onchange=ChkQty(this.value,'$row_asset[code]','$super_user_branch','$row_asset[qty]','$row_asset[rate]') required>No</label>";
			
			// $select_box = "<select style='font-size:12px;height:28px !important' onchange=ChkQty(this.value,'$row_asset[code]','$super_user_branch','$row_asset[qty]','$row_asset[rate]') class='form-control' name='item_sel[]' required>
				// <option style='font-size:12px;' value=''>--SELECT--</option>
				// <option style='font-size:12px;' value='1'>YES</option>
				// <option style='font-size:12px;' value='0'>NO</option>
			// </select>";
		}
			
		echo "<tr>
				<td>
					$row_asset[item_name]
				</td>
				
				<input type='hidden' value='$row_asset[code]' name='item_code[]'>
				
				<td style='padding: 10px;'>
					$select_box
				</td>
				
				<td style='width:30%'>
					<center>$item_button</center>
				</td>

				<input type='hidden' id='rate_asset_box$row_asset[code]' value='$row_asset[rate]' name='asset_rate[]'>
				<td>
					₹ <span id='rate_asset$row_asset[code]'>$row_asset[rate]</span>
				</td>
				
				</tr>
			";
		}
	}
//
?>
</table>

</div>
</div>
</div>

<input type="hidden" value="<?php echo $tno; ?>" name="tno">

<p align="center"><button type="submit" name="asset_submit" class="btn btn-sm btn-primary" id="asset_submit">Save Asset - Form</button></p>
<br />
	</div>
    </div>
  </div>
</div>
</form>