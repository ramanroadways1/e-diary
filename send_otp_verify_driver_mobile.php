<?php
require_once './connect.php';

$timestamp = date("Y-m-d H:i:s");
$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$tno = escapeString($conn,strtoupper($_SESSION['diary']));

$_SESSION['driver_verify_otp'] = "0";
$_SESSION['driver_verify_otp_by_pass'] = "NO";
$_SESSION['driver_verify_driver_id'] = "0";

echo "<script>$('#verify_otp_btn_driver').html('Verify OTP');</script>";

if(strlen($mobile)!=10)
{
	AlertError("Enter valid mobiler number !!");
	echo "<script>$('#verify_otp_btn_driver').attr('disabled',true);$('#send_otp_btn_driver').attr('disabled',false);$('#add_driver_button').attr('disabled',true);$('#otp_verify_div_driver').hide();</script>";
	exit();
}

$Check_Mobile = Qry($conn,"SELECT id,name,last_verify,active,driver_blacklist FROM dairy.driver WHERE mobile='$mobile'");

if(!$Check_Mobile){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error..");
	echo "<script>$('#verify_otp_btn_driver').attr('disabled',true);$('#send_otp_btn_driver').attr('disabled',false);$('#add_driver_button').attr('disabled',true);$('#otp_verify_div_driver').hide();</script>";
	exit();
}

if(numRows($Check_Mobile)==0)
{
	AlertError("No driver found with mobile: $mobile.");
	echo "<script>$('#verify_otp_btn_driver').attr('disabled',true);$('#send_otp_btn_driver').attr('disabled',false);$('#add_driver_button').attr('disabled',true);$('#otp_verify_div_driver').hide();</script>";
	exit();
}

$row_driver = fetchArray($Check_Mobile);
$driver_name = $row_driver['name'];
$driver_id = $row_driver['id'];

if($row_driver['driver_blacklist']=="1")
{
	AlertError("Driver: $driver_name.<br><font color=\'maroon\'>Driver blacklisted !</font>");
	echo "<script>$('#verify_otp_btn_driver').attr('disabled',true);$('#send_otp_btn_driver').attr('disabled',false);$('#add_driver_button').attr('disabled',true);$('#otp_verify_div_driver').hide();</script>";
	exit();
}

if($row_driver['active']!='0')
{
	AlertError("Driver: $driver_name.<br><font color=\'maroon\'>Active on another vehicle.</font>");
	echo "<script>$('#verify_otp_btn_driver').attr('disabled',true);$('#send_otp_btn_driver').attr('disabled',false);$('#add_driver_button').attr('disabled',true);$('#otp_verify_div_driver').hide();</script>";
	exit();
}

if($row_driver['last_verify']!=0 AND $row_driver['last_verify']!='')
{
	if(strtotime($row_driver['last_verify']) <= strtotime('-72 hours'))
	{
		// last verification is older than 72 hours..
	}
	else
	{
		$_SESSION['driver_verify_otp'] = "NA";
		$_SESSION['driver_verify_otp_by_pass'] = "YES";
		$_SESSION['driver_verify_driver_id'] = $driver_id;
		
		AlertRightCornerSuccess("Success !<br>Mobile already verified.");
		echo "<script>
			$('#send_otp_btn_driver').attr('disabled',true);
			$('#add_driver_button').attr('disabled',true);
			$('#mobile_driver').attr('readonly',true);
			$('#driver_verify_otp').attr('disabled',true);
			$('#driver_verify_otp').val('Already Verified');
			$('#verify_otp_btn_driver').attr('disabled',false);
			$('#otp_send_span').html('<font color=\'green\'>Already verified.</font>');
			$('.otp_verify_div_driver').show();
			$('#verify_otp_btn_driver').html('Proceed');
			$('#add_driver_button').hide();
		</script>";	
		exit();
	}
}

$date = date('Y-m-d');
$otp = rand(100000,999999);

$driver_name_for_sms = strtok(trim($driver_name),' ');

$msg_template="Hello $driver_name_for_sms,\nEnter OTP: $otp to verify your mobile number.\nRamanRoadways.";
$msgType="TXT";

DriverVerifyOwn($conn,$tno,$branch,$_SESSION['user_code'],$mobile,$otp,$driver_name_for_sms);
SendWAMsg($conn,$mobile,$msg_template);

$_SESSION['driver_verify_otp'] = $otp;
$_SESSION['driver_verify_driver_id'] = $driver_id;

AlertRightCornerSuccess("Success !<br>OTP Sent to $mobile.");
echo "<script>
	$('#send_otp_btn_driver').attr('disabled',true);
	$('#add_driver_button').attr('disabled',true);
	$('#mobile_driver').attr('readonly',true);
	$('#driver_verify_otp').attr('disabled',false);
	$('#verify_otp_btn_driver').attr('disabled',false);
	$('#otp_send_span').html('<font color=\'maroon\'>Sent on $mobile.</font>');
	$('.otp_verify_div_driver').show();
	$('#add_driver_button').hide();
</script>";	
exit();
?>