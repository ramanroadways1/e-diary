<?php
require_once("../connect.php");

$trip_id=escapeString($conn,strtoupper($_POST['trip_id_print']));
$driver_name=escapeString($conn,strtoupper($_POST['driver_name']));

$trip_data=Qry($conn,"SELECT trip_no,tno,from_station,to_station,charge_wt,lrno,date(date) as start_date,date(end_date) 
as end_date,freight,cash,cheque,rtgs,diesel,freight_collected,expense FROM dairy.trip WHERE id='$trip_id'");

if(!$trip_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row_trip=fetchArray($trip_data);

$fetch_comp=Qry($conn,"SELECT comp FROM dairy.own_truck WHERE tno='$row_trip[tno]'");
if(!$fetch_comp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

$row11=fetchArray($fetch_comp);
$company=$row11['comp'];

$start = date('d/m/Y', strtotime($row_trip['start_date']));
if($row_trip['end_date']==0)
{
	$end_date="<font color='green'>RUNNING<font>";
}
else
{
	$end_date=date('d/m/Y', strtotime($row_trip['end_date']));
}

$from_station = $row_trip['from_station'];
$to_station = $row_trip['to_station'];

$total_advance=$row_trip['freight']+$row_trip['cash']+$row_trip['cheque']+$row_trip['rtgs'];
?>
<!doctype html>
<html>
<head>
	<title>Trip View || E-Diary || RAMAN ROADWAYS PVT. LTD.</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="../../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
</head>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}

@media print {
  #btn{
    display: none;
  }
  body {
   zoom:85%;
 }
  
}
</style>

<a id="btn" href="../"><button style="margin:10px;" class="btn btn-sm btn-primary">Dashboard</button></a>
<button id="btn" onclick="print();" class="btn btn-sm btn-danger">Print This Page</button>
	
<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
	
<div class="container-fluid">
	<div class="row">
		<div class="form-group col-md-12">
			<table border="0" style="width:100%;font-size:7pt">
				<tr>
				<td>
				<?php
				if($company=='RRPL')
				{
					echo "<b>RAMAN ROADWAYS PRIVATE LIMITED</b>";
				}
				else
					{
					echo "<b>RAMAN ROADWAYS</b>";	
					}
				?>
				<br>
				805, Samedh complex, Nr. Associate petrol pump, C.G. Road AHMEDABAD.
				<br>
				Ph: 079-26460631.	
				</td>
				<td>
				<b>Accounting Year:</b> <?php echo GetFinYear(); ?>
				<br>
				<b>Generated On: </b><?php echo date("d-m-y h:i A"); ?>
				
			</td>
			</tr>
			</table>
		</div>
		<div class="form-group col-md-12">
		<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
			<center><h5 style="padding:5px;font-size:12px;">
			Trip No : <?php echo $row_trip['trip_no']; ?>, 
			<?php echo $from_station." to ".$to_station; ?>
			</h5></center>
		</div>
			<table class="table table-bordered" style="font-size:7pt;">
				<tr>
					<th>Truck No (ट्रक): </th><td><?php echo $row_trip['tno']; ?></td>
					<th>Trip No (ट्रिप न.): </th><td><?php echo $row_trip['trip_no']; ?></td>
					<th>DriverName (ड्राइवर): </th><td colspan="3"><?php echo $driver_name; ?></td>
				</tr>	
				
				<tr>
					<th>LR (एल आर ) : </th><td><?php echo $row_trip['lrno']; ?></td>
					<th>Wt (वजन)  : </th><td><?php echo $row_trip['charge_wt']." TN (टन में)"; ?></td>
					<th>Start Date (ट्रिप शुरू) : </th><td><?php echo $start; ?></td>
					<th>End Date (ट्रिप पूर्ण) : </th><td><?php echo $end_date; ?></td>
				</tr>	
				
			</table>
		</div>
		
		<div class="col-md-12">
		<table class="table" style="width:100%">
			<tr>
			<td>
			<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
				<center><h5 style="padding:5px;font-size:12px;">
				Advance to Driver (ड्राइवर को एडवांस) :</h5></center>
			</div>
			<table border="1" style="width:100%;font-size:7pt;">
				<?php
				echo "<tr>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Amount</th>
					<th style='padding:3px;'>Type</th>
					</tr>
					";
					
				if($row_trip['cash']>0)
				{
					$qry_adv_cash=Qry($conn,"SELECT amount,date,branch FROM dairy.cash WHERE trip_id='$trip_id'");
					
					if(!$qry_adv_cash){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error While Processing Request.","./");
						exit();
					}

					while($row_adv_cash=fetchArray($qry_adv_cash))
					{
						$cash_date=date('d/m/y', strtotime($row_adv_cash['date']));
						
						echo "<tr>
								<td style='padding:3px;'>$cash_date</td>
								<td style='padding:3px;'>$row_adv_cash[branch]</td>
								<td style='padding:3px;'>$row_adv_cash[amount]</td>
								<td style='padding:3px;'>CASH</td>
							</tr>
							";
					}
				}	

				if($row_trip['cheque']>0)
				{
					$qry_adv_cheque=Qry($conn,"SELECT amount,cheq_no,date,branch FROM dairy.cheque WHERE trip_id='$trip_id'");
					
					if(!$qry_adv_cheque){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error While Processing Request.","./");
						exit();
					}
					
					while($row_adv_cheque=fetchArray($qry_adv_cheque))
					{
						$cheque_date=date('d/m/y', strtotime($row_adv_cheque['date']));
						
						echo "<tr>
								<td style='padding:3px;'>$cheque_date</td>
								<td style='padding:3px;'>$row_adv_cheque[branch]</td>
								<td style='padding:3px;'>$row_adv_cheque[amount]</td>
								<td style='padding:3px;'>CHQ ($row_adv_cheque[cheq_no])</td>
							</tr>
							";
					}
				}	

				if($row_trip['rtgs']>0)
				{
					$qry_adv_rtgs=Qry($conn,"SELECT amount,date,branch FROM dairy.rtgs WHERE trip_id='$trip_id'");
					
					if(!$qry_adv_rtgs){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error While Processing Request.","./");
						exit();
					}
					
					while($row_adv_rtgs=fetchArray($qry_adv_rtgs))
					{
						$rtgs_date=date('d/m/y', strtotime($row_adv_rtgs['date']));
						
						echo "<tr>
								<td style='padding:3px;'>$rtgs_date</td>
								<td style='padding:3px;'>$row_adv_rtgs[branch]</td>
								<td style='padding:3px;'>$row_adv_rtgs[amount]</td>
								<td style='padding:3px;'>RTGS</td>
								
							</tr>
							";
					}
				}	

				if($row_trip['freight']>0)
				{
					$qry_adv_freight=Qry($conn,"SELECT amount,date,branch FROM dairy.freight_adv WHERE trip_id='$trip_id'");
					
					if(!$qry_adv_freight){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error While Processing Request.","./");
						exit();
					}
					
					while($row_adv_freight=fetchArray($qry_adv_freight))
					{
						$freight_date=date('d/m/y', strtotime($row_adv_freight['date']));
						
						echo "<tr>
								<td style='padding:3px;'>$freight_date</td>
								<td style='padding:3px;'>$row_adv_freight[branch]</td>
								<td style='padding:3px;'>$row_adv_freight[amount]</td>
								<td style='padding:3px;'>FT (भाड़ा)</td>
								
							</tr>
							";
					}
				}	
			
			echo "<tr>
					<td style='padding:3px;' colspan='2'><b>Total Calculation :</b></td>
					<td style='padding:3px;'><b>Rs: $total_advance /-</b></td>
					<td></td>
				</tr>
				";
			
				?>
			</table>
		</td>
		
		<!-- FREIGHT COLLECTED AT HO DIV -->
		<td>
			<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
				<center><h5 style="padding:5px;font-size:12px;">
				Freight Collected (भाड़ा जमा) :</h5></center>
			</div>
			<table border="1" style="width:100%;font-size:7pt;">
				<?php
				echo "<tr>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Amount</th>
					</tr>
					";
					
				if($row_trip['freight_collected']>0)
				{
					$qry_freight_collected=Qry($conn,"SELECT amount,date,branch FROM dairy.freight_rcvd WHERE trip_id='$trip_id'");
					
					if(!$qry_freight_collected){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error While Processing Request.","./");
						exit();
					}
					
					while($row_freight_collected=fetchArray($qry_freight_collected))
					{
					$freight_coll_date=date('d/m/y', strtotime($row_freight_collected['date']));
					
					echo "<tr>
						<td style='padding:3px;'>$freight_coll_date</td>
						<td style='padding:3px;'>$row_freight_collected[branch]</td>
						<td style='padding:3px;'>$row_freight_collected[amount]</td>
						</tr>
						";
					}
					
					echo "<tr>
					<td style='padding:3px;' colspan='2'><b>Total Calculation :</b></td>
					<td style='padding:3px;'><b>Rs: $row_trip[freight_collected] /-</b></td>
					</tr>
				";
				}
				?>
			</table>
		</td>
		</tr>
		
		<tr>
			<!-- DIESEL DIV -->
			<td>
			<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
				<center><h5 style="padding:5px;font-size:12px;">
				Diesel Summary (डीजल) :</h5></center>
			</div>
			<table border="1" style="width:100%;font-size:7pt;">
				<?php
				echo "<tr>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Qty (लीटर)</th>
					<th style='padding:3px;'>Rate (रेट)</th>
					<th style='padding:3px;'>Amt</th>
					<th style='padding:3px;'>Narr (ब्यौरा)</th>
					</tr>
					";
					
				if($row_trip['diesel']>0)
				{
					$qry_adv_diesel=Qry($conn,"SELECT rate,qty,amount,date,narration,branch FROM dairy.diesel WHERE trip_id='$trip_id'");
					
					if(!$qry_adv_diesel){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error While Processing Request.","./");
						exit();
					}
					
					$sum_diesel=Qry($conn,"SELECT sum(qty) as total_qty FROM dairy.diesel WHERE trip_id='$trip_id'");
					
					if(!$sum_diesel){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error While Processing Request.","./");
						exit();
					}
					
					$row_sum_d=fetchArray($sum_diesel);
					
					while($row_diesel=fetchArray($qry_adv_diesel))
					{
					$diesel_date=date('d/m/y', strtotime($row_diesel['date']));
					
					echo "<tr>
						<td style='padding:3px;'>$diesel_date</td>
						<td style='padding:3px;'>$row_diesel[branch]</td>
						<td style='padding:3px;'>$row_diesel[qty]</td>
						<td style='padding:3px;'>$row_diesel[rate]</td>
						<td style='padding:3px;'>$row_diesel[amount]</td>
						<td style='padding:3px;'>$row_diesel[narration]</td>
						</tr>
						";
					}
					
					echo "<tr>
					<td style='padding:3px;' colspan='2'><b>Total Calculation :</b></td>
					<td style='padding:3px;'><b>$row_sum_d[total_qty] ltr</b></td>
					<td></td>
					<td style='padding:3px;'><b>Rs: $row_trip[diesel] /-</b></td>
					<td></td>
					</tr>";
				}
				?>
			</table>
			</td>
			
			<!-- EXPENSE DIV -->
			<td>
			<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
				<center><h5 style="padding:5px;font-size:12px;">
				Trip Expenses (ट्रिप खर्चा) :</h5></center>
			</div>
			<table border="1" style="width:100%;font-size:7pt;">
				<?php
				echo "<tr>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Exp (खर्चा)</th>
					<th style='padding:3px;'>Amount</th>
					</tr>
					";
					
				if($row_trip['expense']>0)
				{
					$qry_exp=Qry($conn,"SELECT exp_name,amount,date,branch FROM dairy.trip_exp WHERE trip_id='$trip_id'");
					
					if(!$qry_exp){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error While Processing Request.","./");
						exit();
					}
					
					while($row_exp=fetchArray($qry_exp))
					{
					$exp_date=date('d/m/y', strtotime($row_exp['date']));
					
					echo "<tr>
						<td style='padding:3px;'>$exp_date</td>
						<td style='padding:3px;'>$row_exp[branch]</td>
						<td style='padding:3px;'>$row_exp[exp_name]</td>
						<td style='padding:3px;'>$row_exp[amount]</td>
						</tr>
						";
					}
					
					echo "<tr>
					<td colspan='3' style='padding:3px;'><b>Total Calculation :</b></td>
					<td style='padding:3px;'><b>Rs: $row_trip[expense] /-</b></td>
					</tr>";
				}
			closeConnection($conn);	
				?>
			</table>
			</td>
		</tr>
		</table>
		</div>
		
	</div>
</div>