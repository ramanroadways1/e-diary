<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$name = escapeString($conn,strtoupper($_POST['broker_name']));
$pan = escapeString($conn,strtoupper($_POST['broker_pan']));
$mobile = escapeString($conn,strtoupper($_POST['broker_mobile']));

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	AlertRightCornerError("Invalid mobile number !");
	echo "<script>$('#broker_submit').attr('disabled', false);</script>";
	exit();
}

if(strpos($mobile,'0')===0)
{
	AlertRightCornerError("Invalid mobile number !");
	echo "<script>$('#broker_submit').attr('disabled', false);</script>";
	exit();
}

if(strlen($mobile)!=10)
{
	AlertRightCornerError("Invalid mobile number !");
	echo "<script>$('#broker_submit').attr('disabled', false);</script>";
	exit();
}

if(!preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $pan) AND $pan!='')
{
	AlertRightCornerError("Invalid PAN number !");
	echo "<script>$('#broker_submit').attr('disabled', false);</script>";
	exit();
}

$chk_pan = Qry($conn,"SELECT name FROM dairy.broker WHERE pan='$pan' AND pan!=''");

if(!$chk_pan){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error !");
	echo "<script>$('#broker_submit').attr('disabled', false);</script>";
	exit();
}
	
if(numRows($chk_pan)>0)
{
	$row_pan = fetchArray($chk_pan);
	AlertRightCornerError("Duplicate PAN number found !<br>Reg. with: $row_pan[name].");
	echo "<script>$('#broker_submit').attr('disabled', false);</script>";
	exit();
}

$chk_name = Qry($conn,"SELECT pan FROM dairy.broker WHERE name='$name'");

if(!$chk_name){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error !");
	echo "<script>$('#broker_submit').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_name)>0)
{
	$row_name = fetchArray($chk_name);
	AlertRightCornerError("Duplicate broker name !<br>Reg. with: $row_name[pan].");
	echo "<script>$('#broker_submit').attr('disabled', false);</script>";
	exit();
}

$insert_broker = Qry($conn,"INSERT INTO dairy.broker(name,mobile,pan,branch,date,timestamp) VALUES ('$name','$mobile','$pan','$branch',
'$date','$timestamp')");
	
if(!$insert_broker){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error !");
	echo "<script>$('#broker_submit').attr('disabled', false);</script>";
	exit();
}

AlertRightCornerSuccess("Broker added successfully !");
echo "<script>
	$('#broker_submit').attr('disabled', false);
	$('#FormBroker').trigger('reset');
</script>";
exit();
?>