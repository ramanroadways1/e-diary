<?php
require("connect.php");

$edit_branch=escapeString($conn,$_REQUEST['edit_branch']);
$trip_id=escapeString($conn,$_REQUEST['trip_id']);
?>
<script>
function ChangeEditBranch()
{
	if($('#edit_branch_value').val()==''){
		alert('Please select edit branch first !');
	}
	else
	{
		$('#edit_branch_update_btn').attr('disabled',true);
		$('#edit_branch_update_btn').html('Loading...');
				
		jQuery.ajax({
		url: "next_edit_branch.php",
		data: 'edit_branch=' + $('#edit_branch_value').val() + '&trip_id=' + '<?php echo $trip_id; ?>',
		type: "POST",
		success: function(data) {
			$("#result_edit_branch").html(data);
		},
		error: function() {}
		});
	}
}
</script>
		
<div id="result_edit_branch"></div>
		
<div class="form-inline">
	<div class="form-group">
		<select style="font-size:12px;height:30px;" name="edit_branch" id="edit_branch_value" class="form-control" required>
			<option value="">Select edit branch</option>
			<?php
		$edit_branch_query=Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('HEAD','DUMMY') 
		ORDER BY username ASC");
						
		if(!$edit_branch_query){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			Redirect("Error !","./");
			exit();
		}
						
		if(numRows($edit_branch_query)>0)
		{
			while($row_edit_b=fetchArray($edit_branch_query))
			{
			?>
			<option <?php if($row_edit_b['username']==$edit_branch) {echo "selected style='background:yellow'";} ?> 
			value="<?php echo $row_edit_b['username']; ?>"><?php echo $row_edit_b['username']; ?></option>
			<?php
			}
		}
		?>
		</select>	
			<input type="hidden" id="trip_id_edit_branch" name="trip_id" value="<?php echo $trip_id; ?>">
		<button type="button" onclick="ChangeEditBranch()" id="edit_branch_update_btn" class="btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-danger"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update Edit Branch</button>
	</div>
</div>