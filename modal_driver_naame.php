<script type="text/javascript">
$(document).ready(function (e) {
$("#NaameForm").on('submit',(function(e) {
$("#loadicon").show();
$("#naame_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./driver_naame.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form id="NaameForm" autocomplete="off" style="font-size:13px">     
  <div class="modal fade" id="NaameModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
	  <div class="modal-header bg-primary">
			<span style="font-size:13px">Driver Naame <span style="font-size:13px">( ड्राइवर नामे लिखे )</span> :</span>
		</div>
	<div class="modal-body">
		<div class="row">
			<div class="form-group col-md-4">
				<label>Naame A/c. <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px" name="naame_option" onchange="NaameOpt(this.value)" class="form-control" required>
					<option style="font-size:12px" class="naame_sel_opt" value="">--select--</option>
					<option style="font-size:12px" class="naame_sel_opt" value="DIESEL">Diesel Naame</option>
					<option style="font-size:12px" class="naame_sel_opt" value="HAPPAY_CARD_PENALTY">Happay Card Penalty</option>
					<option style="font-size:12px" class="naame_sel_opt" value="TRISHUL_CARD_PENALTY">Trishul Card Penalty</option>
					<option style="font-size:12px" class="naame_sel_opt" id="naame_sel_opt_claim" value="CLAIM">Claim</option>
					<option style="font-size:12px" class="naame_sel_opt" id="naame_sel_opt_freight" value="FREIGHT">Freight Naame</option>
					<option style="font-size:12px" class="naame_sel_opt" value="OTHER">Other Naame</option>
				</select>
			</div>
			
<script>
function NaameOpt(elem)
{
	$('#naame_rate').val('');
	$('#naame_qty').val('');
	$('#naame_amount').val('');
	
	if(elem=='DIESEL')
	{
		$('#qty_div').show();
		$('#rate_div').show();
		$('#claim_credit_div').hide();
		$('#freight_naame_div').hide();
		
		$('#naame_qty').attr('required',true);
		$('#naame_rate').attr('required',true);
		$('#naame_amount').attr('oninput','sum3Naame()');
		$('#naame_qty').attr('oninput','sum2Naame()');
		$('#naame_rate').attr('oninput','sum1Naame()');
		
		$("#claim_lr_type").attr('required',false);
		$("#claim_lrno").attr('required',false);
		$("#freight_lr_type1").attr('required',false);
		$("#freight_lrno1").attr('required',false);
		$("#naame_button").attr('disabled',false);
	}
	else if(elem=='CLAIM')
	{
		$('#qty_div').hide();
		$('#rate_div').hide();
		$('#claim_credit_div').show();
		$('#freight_naame_div').hide();
		
		$("#claim_lr_type").attr('required',true);
		$("#claim_lrno").attr('required',true);
		$("#naame_button").attr('disabled',true);
		$("#freight_lr_type1").attr('required',false);
		$("#freight_lrno1").attr('required',false);
		
		$('#naame_qty').attr('required',false);
		$('#naame_rate').attr('required',false);
		$('#naame_amount').attr('oninput','');
		$('#naame_qty').attr('oninput','');
		$('#naame_rate').attr('oninput','');
	}
	else if(elem=='FREIGHT')
	{
		$('#qty_div').hide();
		$('#rate_div').hide();
		$('#claim_credit_div').hide();
		$('#freight_naame_div').show();
		
		$("#freight_lr_type1").attr('required',true);
		$("#freight_lrno1").attr('required',true);
		$("#claim_lr_type").attr('required',false);
		$("#claim_lrno").attr('required',false);
		$("#naame_button").attr('disabled',true);
		// $("#naame_button").hide();
		
		$('#naame_qty').attr('required',false);
		$('#naame_rate').attr('required',false);
		$('#naame_amount').attr('oninput','');
		$('#naame_qty').attr('oninput','');
		$('#naame_rate').attr('oninput','');
	}
	else
	{
		$('#qty_div').hide();
		$('#rate_div').hide();
		$('#claim_credit_div').hide();
		$('#freight_naame_div').hide();
		
		$("#freight_lr_type1").attr('required',false);
		$("#freight_lrno1").attr('required',false);
		$('#naame_qty').attr('required',false);
		$('#naame_rate').attr('required',false);
		$('#naame_amount').attr('oninput','');
		$('#naame_qty').attr('oninput','');
		$('#naame_rate').attr('oninput','');
		$("#claim_lr_type").attr('required',false);
		$("#claim_lrno").attr('required',false);
		$("#naame_button").attr('disabled',false);
	}
}
					
function sum1Naame() 
{
	if($("#naame_qty").val()=='')
	{
		$("#naame_qty").val('0');
	}
	
	if($("#naame_rate").val()=='')
	{
		$("#naame_rate").val('0');
	}
		
	$("#naame_amount").val(Math.round(Number($("#naame_qty").val()) * Number($("#naame_rate").val())).toFixed(2));
}

function sum2Naame() 
{
	if($("#naame_qty").val()=='')
	{
		$("#naame_qty").val('0');
	}
	
	if($("#naame_rate").val()=='')
	{
		$("#naame_rate").val('0');
	}
		
	$("#naame_amount").val(Math.round(Number($("#naame_qty").val()) * Number($("#naame_rate").val())).toFixed(2));
}
 
 function sum3Naame() 
 {
	if($("#naame_qty").val()=='')
	{
		$("#naame_qty").val('0');
	}
	
	if($("#naame_rate").val()=='')
	{
		$("#naame_rate").val('0');
	}
	
	if($("#naame_amount").val()=='')
	{
		$("#naame_amount").val('0');
	}
	
	$("#naame_rate").val((Number($("#naame_amount").val()) / Number($("#naame_qty").val())).toFixed(2));
}
</script>
			
			
			<div id="qty_div" class="form-group col-md-4">
				<label>QTY. <sup><font color="red">*</font></sup></label>
				<input type="number" step="any" max="600" min="1" id="naame_qty" oninput="sum2Naame();" name="naame_qty" class="form-control" required />
			</div>
			
			<div id="rate_div" class="form-group col-md-4">
				<label>Rate. <sup><font color="red">*</font></sup></label>
				<input type="number" step="any" max="110" min="1" id="naame_rate" oninput="sum1Naame();" name="naame_rate" class="form-control" required />
			</div>
			
<script>
			function CheckClailLR()
			{
				var claim_lrno = $('#claim_lrno').val();
				var lr_type = $('#claim_lr_type').val();
				
				if(claim_lrno=='')
				{
					alert('Enter LR number first !!');
				}
				else
				{
					if(lr_type=='')
					{
						alert('Select LR Type first !!');
					}
					else
					{
						$("#loadicon").show();	
						 jQuery.ajax({
							 url: "./get_data_claim_lr_wise.php",
							data: 'lrno=' + claim_lrno + '&lr_type=' + lr_type,
							 type: "POST",
							 success: function(data) {
								$("#result_naame_modal1").html(data);
							},
							 error: function() {}
						});
					}
				}
			}
			
			function CheckFreightLR()
			{
				var lrno = $('#freight_lrno1').val();
				var lr_type = $('#freight_lr_type1').val();
				
				if(lrno=='')
				{
					alert('Enter LR number first !!');
				}
				else
				{
					if(lr_type=='')
					{
						alert('Select LR Type first !!');
					}
					else
					{
						$("#loadicon").show();	
						 jQuery.ajax({
							 url: "./get_data_freight_lr_wise.php",
							data: 'lrno=' + lrno + '&lr_type=' + lr_type,
							 type: "POST",
							 success: function(data) {
								$("#result_freight_naame").html(data);
							},
							 error: function() {}
						});
					}
				}
			}
			</script>
		
	<div id="freight_naame_div" class="row" style="display:none">
			<div class="form-group col-md-12">
				<div class="form-group col-md-5">
					<label>LR Type <font color="red">*</font></label>
					<select style="font-size:12px" class="form-control" id="freight_lr_type1" name="lr_type_freight" required="required">
						<option style="font-size:12px" class="freight_lr_type_sel" value="">--select lr type--</option>
						<option style="font-size:12px" class="freight_lr_type_sel" id="freight_lr_type_MARKET" value="MARKET">Market LR</option>
						<option style="font-size:12px" class="freight_lr_type_sel" id="freight_lr_type_OWN" value="OWN">Own LR</option>
					</select>
				</div>
				
				<div class="form-group col-md-7 form-inline">
					<label>LR number <font color="red">*</font></label>
					<br />
					<input class="form-control" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');if($('#freight_lr_type1').val()==''){ $('#freight_lrno1').val('');$('#freight_lr_type1').focus(); }" type="text" name="lrno_freight" id="freight_lrno1" required />
					<button onclick="CheckFreightLR()" id="freight_check_btn" type="button" class="btn btn-sm btn-success">
					Check LR <span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button>
				</div>
				
				<div class="row"> 
					<div id="result_freight_naame" class="col-md-12"></div>
				</div>
				<!--
				<div style="display:none;padding:7px;font-size:13px;color:maroon" id="claim_lr_desc" class="bg-warning form-group col-md-12">
					>> Trip: Aburoad to Jalore & Vehicle no: GJ18AX7902
				</div>-->
			</div>
		</div>
		
		<script>
		function FreightLRSelection(id)
		{
			if(id=='')
			{
				$('#freight_vou_no2').val('');
				$('#freight_veh_no2').val('');
				$('#freight_from_loc2').val('');
				$('#freight_to_loc2').val('');
				$('#freight_trip_id').val('');
				
				$('#freight_lr_sel_result').html('No record selected..');
			}
			else
			{
				var res = id.split("_");
				var vou_no = res[0];
				var tno = res[1];
				var from = res[2];
				var to = res[3];
				var trip_id = res[4];
				
				$("#loadicon").show();	
					 jQuery.ajax({
						 url: "./verify_driver_for_freight_naame.php",
						data: 'trip_id=' + trip_id,
						 type: "POST",
						 success: function(data) {
						$("#result_naame_modal1").html(data);
					},
					error: function() {}
				});
						
				$('#freight_vou_no2').val(vou_no);
				$('#freight_veh_no2').val(tno);
				$('#freight_from_loc2').val(from);
				$('#freight_to_loc2').val(to);
				$('#freight_trip_id').val(trip_id);
				
				$('#freight_lr_sel_result').html('>> '+from+' to '+to+'<br>>> Vehicle number: '+tno);
			}
		}
		</script>
		
		<div id="claim_credit_div" class="row" style="display:none">
			<div class="form-group col-md-12">
				<div class="form-group col-md-5">
					<label>LR Type <font color="red">*</font></label>
					<select style="font-size:12px" class="form-control" id="claim_lr_type" name="lr_type" required="required">
						<option style="font-size:12px" class="claim_lr_type_sel" value="">--select lr type--</option>
						<option style="font-size:12px" class="claim_lr_type_sel" id="claim_lr_type_MARKET" value="MARKET">Market LR</option>
						<option style="font-size:12px" class="claim_lr_type_sel" id="claim_lr_type_OWN" value="OWN">Own LR</option>
					</select>
				</div>
				
				<div class="form-group col-md-7 form-inline">
					<label>LR number <font color="red">*</font></label>
					<br />
					<input class="form-control" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');if($('#claim_lr_type').val()==''){ $('#claim_lrno').val('');$('#claim_lr_type').focus(); }" type="text" name="lrno" id="claim_lrno" required />
					<button onclick="CheckClailLR()" id="claim_check_btn" type="button" class="btn btn-sm btn-success">
					Check LR <span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button>
				</div>
				
				<div style="display:none;padding:7px;font-size:13px;color:maroon" id="claim_lr_desc" class="bg-warning form-group col-md-12">
					>> Trip: Aburoad to Jalore & Vehicle no: GJ18AX7902
				</div>
			</div>
		</div>			
			
		<div class="row">
			<div class="form-group col-md-12">	
				<div class="form-group col-md-6">
					<label>Amount. <sup><font color="red">*</font></sup></label>
					<input type="number" min="1" oninput="sum3Naame();" id="naame_amount" name="naame_amount" class="form-control" required />
				</div>
				
				<div class="form-group col-md-6">
					<label>Narration. <sup><font color="red">*</font></sup></label>
					<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,/]/,'')" class="form-control" name="naame_narration" required></textarea>
				</div>
			</div>
		</div>
			
			<div id="result_naame_modal1"></div>	
			<input type="hidden" name="trip_id" class="trip_id_set" />
			<input type="hidden" name="trip_no" class="trip_no_set" />
			<input type="hidden" name="driver_name" class="driver_name_set" />
			<input type="hidden" name="driver_code" value="<?php echo $driver_code_value; ?>" />
			<input type="hidden" name="tno" value="<?php echo $tno; ?>" />
			
		</div>
        </div>
        <div class="modal-footer">
          <button <?php if($driver_code_value=="") {echo "disabled"; } ?> type="submit" id="naame_button" class="btn btn-sm btn-danger">Submit</button>
          <button type="button" class="btn btn-sm btn-primary" id="hide_naame" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>