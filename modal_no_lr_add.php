<?php
exit();
?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#NOLRForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_nolr1").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./add_no_lr_trip.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_my_modal").html(data);
	$("#loadicon").hide();
	$("#submit_nolr1").attr("disabled", false);
	},
	error: function() 
	{} });}));});
</script>

<script>
$(function() {
		$("#from_stn_no_lr").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from_stn_no_lr').val(ui.item.value);   
            $('#from_loc_id_no_lr').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from_stn_no_lr').val("");   
			$('#from_loc_id_no_lr').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to_stn_no_lr").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to_stn_no_lr').val(ui.item.value);   
            $('#to_loc_id_no_lr').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_stn_no_lr').val("");   
			$('#to_loc_id_no_lr').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div id="result_my_modal"></div> 

  <div class="modal fade" id="ModalNoLRAdd" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
		<form id="NOLRForm" autocomplete="off">   
		<div class="modal-body">
		<div class="row">
			
			<input type="hidden" name="tno" value="<?php echo $tno; ?>" class="form-control" required />
			<input type="hidden" name="billing_type_text" id="billing_type_text_no_lr" class="form-control" required />
			<input type="hidden" name="freight_bilty_text" id="freight_bilty_text_no_lr" class="form-control" required />
			
			<div class="form-group col-md-12 bg-primary" style="color:#FFF">
				<h4>ADD LR to TRIP :</h4>
			</div>
		
			<div class="form-group col-md-4">
				<label>LR Type. <sup><font color="red">*</font></sup></label>
				<select name="lr_type" class="form-control" id="lr_type22" onchange="LRType11(this)" required>
					<option value="">Select an option</option>
					<option value="OWN">Own Bilty</option>
					<option value="MKT">Market Bilty</option>
					<option value="EMPTY">EMPTY</option>
					<option value="CON20">EMPTY-CONTAINER : 20</option>
					<option value="CON40">EMPTY-CONTAINER : 40</option>
				</select>
			</div>
		
			<script>
			function LRType11(lr)
			{
				$('#edit_branch_no_lr').val('');
				
					$('#actual_no_lr').attr('readonly',false);
					$('#charge_no_lr').attr('readonly',false);
					$('#lrno_no_lr').attr('readonly',false);
					$('#from_stn_no_lr').attr('readonly',false);
					$('#to_stn_no_lr').attr('readonly',false);
				
					$('#actual_no_lr').val('');
					$('#charge_no_lr').val('');
					$('#lrno_no_lr').val('');
					$('#from_stn_no_lr').val('');
					$('#to_stn_no_lr').val('');
					
				if(lr.value=='')
				{
					$('#lr_type_div2').html('');
				}
				else
				{
					$("#loadicon").show();
					jQuery.ajax({
					url: "./fetch_lr_type_no_lr.php",
					data: 'type=' + lr.value,
					type: "POST",
					success: function(data) {
					$("#lr_type_div2").html(data);
					$("#loadicon").hide();
					},
					error: function() {}
					});
				}
			}
			</script>
			
			<script>
			function FetchLRDataFromLR_no_lr(frno,type)
			{
					$("#loadicon").show();
					jQuery.ajax({
					url: "./fetch_lr_data_no_lr.php",
					data: 'frno=' + frno + '&type=' + type + '&trip_id=' + $('#no_lr_trip_id').val(),
					type: "POST",
					success: function(data) {
					$("#lr_result_no_lr").html(data);
					$("#loadicon").hide();
					},
					error: function() {}
					});
			}
			</script>
			
			<div id="lr_result_no_lr"></div>
			
			<div id="lr_type_div2">
			</div>
			
			<div class="form-group col-md-2">
				<label>Act Wt. <sup><font color="red">*</font></sup></label>
				<input type="number" min="0" step="any" id="actual_no_lr" name="actual" class="form-control" required />
			</div>
			
			<div class="form-group col-md-2">
				<label>Chrg Wt. <sup><font color="red">*</font></sup></label>
				<input type="number" min="0" step="any" id="charge_no_lr" name="charge" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>From. <sup><font color="red">*</font></sup></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" name="from" id="from_stn_no_lr" class="form-control from_station_no_lr" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>To. <sup><font color="red">*</font></sup></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" name="to" id="to_stn_no_lr" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>LR No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="lrno" id="lrno_no_lr" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Unloading Loc. PinCode. <sup><font color="red">*</font></sup></label>
				<input type="text" maxlength="6" minlength="6" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="pincode" id="pincode_no_lr" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
			<label>Next Edit Branch. <sup><font color="red">*</font></sup></label>
			<select name="next_edit_branch" id="edit_branch_no_lr" onchange="FetchKm_no_lr(this.value)" class="form-control" required>
				<option value="">Select an option</option>
				<?php
				$qry_22=Qry($conn,"SELECT username FROM user WHERE role='2' ORDER BY username ASC");
				if(!$qry_22)
				{
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				if(numRows($qry_22)>0)
				{
					echo "<option value='0'>UNKNOWN</option>";
					while($row_branch22=fetchArray($qry_22))
					{
						echo "<option value='$row_branch22[username]'>$row_branch22[username]</option>";
					}
				}
				?>
			</select>
		</div>
		
		<script>
		function FetchKm_no_lr()
		{
			var from1 = $('.from_station_no_lr').val();
			var to1 = $('#to_stn_no_lr').val();
			
			if(from1=='' || to1=='')
			{
				alert('Enter From and To Location first !');
				$('#trip_km_no_lr').val('');
				$('#edit_branch_no_lr').val('');
			}
			else
			{
				$('.from_station_no_lr').attr('readonly',true);
				$('#to_stn_no_lr').attr('readonly',true);
				
					$("#loadicon").show();
					jQuery.ajax({
					url: "./fetch_master_km_no_lr.php",
					data: 'from=' + from1 + '&to=' + to1,
					type: "POST",
					success: function(data) {
					$("#km_result_no_lr").html(data);
					$("#loadicon").hide();
					},
					error: function() {}
					});
			}
			
		}
		</script>
		
		<script>
		function FuncKm_no_lr()
		{
			if($('#edit_branch_no_lr').val()=='')
			{
				$('#trip_km_no_lr').val('');
				$('#edit_branch_no_lr').focus();
			}
		}
		</script>
		
		<div id="km_result_no_lr"></div>
		
		<input type="hidden" id="from_loc_id_no_lr" name="from_loc_id" />
		<input type="hidden" id="to_loc_id_no_lr" name="to_loc_id" />
		<input type="hidden" id="set_km_no_lr" name="set_km" />
		<input type="hidden" id="no_lr_trip_id" name="trip_id" />
		<input type="hidden" id="no_lr_truck_no" name="tno" />
		
		<input type="hidden" id="at_loading_set_no_lr" name="at_loading_set" />
		
		<div class="form-group col-md-4">
			<label>Trip KM. <sup><font color="red">*</font></sup></label>
			<input oninput="FuncKm_no_lr()" type="number" name="trip_km" id="trip_km_no_lr" class="form-control" required />
		</div>
		
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="submit_nolr1" class="btn btn-danger">Create Trip</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>
 