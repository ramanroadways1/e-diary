<script>	
function AlertLogout()
{
	Swal.fire({
	  title: "Are you sure ?",
	  html: "Do you really want to Log out !",
	  icon: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes Confirm",
	  closeOnConfirm: false
	}).then((result) => {
		if(result.isConfirmed) {
			window.location.href='./logout.php';
		}
	})
}
</script>	