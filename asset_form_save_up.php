<?php
require_once("./connect.php");

$tno1 = escapeString($conn,strtoupper($_POST['tno']));
$tno = escapeString($conn,strtoupper($_SESSION['diary']));
$timestamp = date("Y-m-d H:i:s");

if($tno!=$tno1)
{
	AlertError("Vehicle not verified..");
	errorLog("Vehicle not verified. $tno1 and $tno.",$conn,$page_name,__LINE__);
	echo "<script>$('#asset_submit').attr('disabled',false);</script>";
	exit();
}

$get_driver = Qry($conn,"SELECT driver_code FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_driver){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#asset_submit').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_driver)==0)
{
	AlertError("Error..");
	echo "<script>$('#asset_submit').attr('disabled',false);</script>";
	exit();
}

$row_driver = fetchArray($get_driver);

if($row_driver['driver_code']==0 || $row_driver['driver_code']=='')
{
	AlertError("Driver not found..");
	echo "<script>$('#asset_submit').attr('disabled',false);</script>";
	exit();
}

$driver_code = $row_driver['driver_code'];

$check_loaded_hisab = Qry($conn,"SELECT trip_no FROM dairy.trip WHERE tno='$tno' AND loaded_hisab='1'");

if(!$check_loaded_hisab){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#asset_submit').attr('disabled',false);</script>";
	exit();
}

$chk_asset_form = Qry($conn,"SELECT id FROM dairy.asset_form WHERE driver_code='$driver_code' AND trip_no=''");

if(!$chk_asset_form){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#asset_submit').attr('disabled',false);</script>";
	exit();
}	
			
if(numRows($chk_asset_form)>0)
{
	AlertError("Asset-form updated already.");
	echo "<script>$('#asset_submit').attr('disabled',false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

if(numRows($check_loaded_hisab)>0)
{
	$row_trip_no = fetchArray($check_loaded_hisab);
	$trip_no1 = $row_trip_no['trip_no'];
	
	foreach($_POST['item_code'] as $key=>$value)
	{
		$item_code = escapeString($conn,strtoupper($_POST['item_code'][$key]));
		$item_sel = escapeString($conn,strtoupper($_POST['item_sel'][$item_code]));
		$item_qty = escapeString($conn,strtoupper($_POST['item_qty'][$key]));
		$asset_rate = escapeString($conn,strtoupper($_POST['asset_rate'][$key]));
		
		$insert = Qry($conn,"INSERT INTO dairy.asset_form(driver_code,item_code,item_qty,item_rate,item_value_up,branch,trip_no,tno,
		timestamp) VALUES ('$driver_code','$item_code','$item_qty','$asset_rate','$item_sel','$branch','$trip_no1','$tno','$timestamp')");
		
		if(!$insert){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
else
{
	foreach($_POST['item_code'] as $key=>$value)
	{
		$item_code = escapeString($conn,strtoupper($_POST['item_code'][$key]));
		$item_sel = escapeString($conn,strtoupper($_POST['item_sel'][$item_code]));
		$item_qty = escapeString($conn,strtoupper($_POST['item_qty'][$key]));
		$asset_rate = escapeString($conn,strtoupper($_POST['asset_rate'][$key]));
		
		$insert = Qry($conn,"INSERT INTO dairy.asset_form(driver_code,item_code,item_qty,item_rate,item_value_up,branch,tno,timestamp) 
		VALUES ('$driver_code','$item_code','$item_qty','$asset_rate','$item_sel','$branch','$tno','$timestamp')");
		
		if(!$insert){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

$update_status = Qry($conn,"UPDATE dairy.driver_up SET asset_form_up='1' WHERE down=0 AND code='$driver_code'");

if(!$update_status){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	Redirect("ASSET FORM SUCCESSFULLY UPDATED.","./");
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request..");
	echo "<script>$('#asset_submit').attr('disabled',false);</script>";
	exit();
}
?>