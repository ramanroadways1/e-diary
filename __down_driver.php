<?php
require_once('./connect.php');
exit();
if(!isset($_SESSION['diary']))
{
	echo "<script type='text/javascript'>
		window.location.href='./login.php';
		</script>";
	exit();
}	

$date=date("Y-m-d");

$tno=mysqli_real_escape_string($conn,strtoupper($_SESSION['diary']));

$code=mysqli_real_escape_string($conn,strtoupper($_POST['driver_code']));
$name=mysqli_real_escape_string($conn,strtoupper($_POST['driver_name']));
$mobile=mysqli_real_escape_string($conn,strtoupper($_POST['mobile']));

$active_d_code=mysqli_real_escape_string($conn,strtoupper($_POST['driver_code_value']));
$active_d_name=mysqli_real_escape_string($conn,strtoupper($_POST['active_driver']));

$qry_active=mysqli_query($conn,"SELECT name,mobile,active FROM driver WHERE code='$active_d_code'");
if(!$qry_active)
{
	echo mysqli_error($conn);
	exit();
}

$row_active=mysqli_fetch_array($qry_active);

if($row_active['active']==1)
	{
		echo "
	<script>
		alert('Driver already assigned to another vehicle.');
		$('#update_driver_button').attr('disabled',false);
		$('#driver_name_fetch_update').val('');
		$('#driver_code_fetch_update').val('');
		$('#new_driver_mob').val('');
	</script>
	";
	exit();
	}

if($row_active['name']!=$active_d_name)
{
	echo "
	<script>
		alert('Unable to validate active driver.');
		$('#update_driver_button').attr('disabled',false);
		$('#driver_name_fetch_update').val('');
		$('#driver_code_fetch_update').val('');
		$('#new_driver_mob').val('');
	</script>
	";
	exit();
}

$qry=mysqli_query($conn,"SELECT name,mobile FROM driver WHERE code='$code'");
if(mysqli_num_rows($qry)>0)
{
	$row=mysqli_fetch_array($qry);
	$name1=$row['name'];
	$mobile1=$row['mobile'];
	
	if($name1==$name && $mobile==$mobile1)
	{
		$update_own_truck_db=mysqli_query($conn,"UPDATE own_truck SET driver_code='$code' WHERE tno='$tno'");
		if(!$update_own_truck_db)
		{
			echo mysqli_error($conn);
			exit();
		}
		
		$update_driver_active=mysqli_query($conn,"UPDATE driver SET active='1' WHERE code='$code' AND mobile='$mobile'");
		if(!$update_driver_active)
		{
			echo mysqli_error($conn);
			exit();
		}
		
		$update_driver_inactive=mysqli_query($conn,"UPDATE driver SET active='0' WHERE code='$active_d_code'");
		if(!$update_driver_inactive)
		{
			echo mysqli_error($conn);
			exit();
		}
		
		$up_driver=mysqli_query($conn,"INSERT INTO driver_up (code,tno,up) VALUES ('$code','$tno','$date')");
		if(!$up_driver)
		{
			echo mysqli_error($conn);
			exit();
		}
		
		$down_driver=mysqli_query($conn,"INSERT INTO driver_up (code,tno,down) VALUES ('$active_d_code','$tno','$date')");
		if(!$down_driver)
		{
			echo mysqli_error($conn);
			exit();
		}
		
		echo "
	<script>
		alert('Driver updated successfully..')
		window.location.href='./';
	</script>
	";
	exit();
	}
	else
	{
	echo "
	<script>
		alert('Something went wrong. unable to validate driver.')
		$('#driver_name_fetch_update').val('');
		$('#driver_code_fetch_update').val('');
		$('#new_driver_mob').val('');
	</script>
	";
	exit();
	}
}
else
{
	echo "
	<script>
		alert('Driver not found.');
		$('#update_driver_button').attr('disabled',false);
		$('#driver_name_fetch_update').val('');
		$('#driver_code_fetch_update').val('');
		$('#new_driver_mob').val('');
	</script>
	";
	exit();
}
?>