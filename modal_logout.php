<!-- LOGOUT MODAL -->

<div class="modal fade modal2" id="LogoutModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog2 modal-md">
      <!-- Modal content-->
      <div class="modal-content" style="font-family:Verdana">
		<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<h5 style="color:red">you are attempting to logout do you really want to logout ?</h5>
			</div>
		</div>
        </div>
        <div class="modal-footer">
          <a href="./logout.php"><button type="button" class="pull-left btn btn-danger">Yes i'm sure.</button></a>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>

<!-- LOGOUT MODAL -->