<?php
require('./connect.php');

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$tno = escapeString($conn,($_SESSION['diary']));
$tank_cap = escapeString($conn,($_POST['tank_cap']));
$diesel_left = escapeString($conn,($_POST['diesel_left']));

echo "<script>$('#submit_qty_update').attr('disabled',true);</script>";

if($diesel_left>$tank_cap)
{
	AlertError("Error : Invalid Qty entered !");
	echo "<script>$('#submit_qty_update').attr('disabled',false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;
		
$update_qty = Qry($conn,"UPDATE dairy.own_truck SET diesel_tank_cap='$tank_cap',diesel_left='$diesel_left',diesel_trip_id=0,
qty_update_user='$_SESSION[user_code]',qty_update_timestamp='$timestamp' WHERE tno='$tno'");

if(!$update_qty){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
				
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	// AlertError("Updated !");	
	Redirect("Fuel details updated successfully.","./");
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");	
	exit();
}
?>