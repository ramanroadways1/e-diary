<?php
require_once './connect.php'; 

$timestamp = date("Y-m-d H:i:s");
$req_url = $_POST['req_url'];
$date = date("d-m-Y",strtotime(escapeString($conn,($_POST['date']))));
$veh_no = escapeString($conn,$_SESSION['diary']);
$start_date = $date."T00:00:01";
$end_date = $date."T23:59:59";
$Duration1= "10"; // in minutes

$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => "https://apps2.locanix.net/RuningHourReport/api/Stoppages?VehicleName=$veh_no&FromDateTime=$start_date&ToDateTime=$end_date&Duration=$Duration1",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 900,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "GET",
CURLOPT_HTTPHEADER => array(
'Content-Type: text/plain'
),
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
		
if($err)
{
	echo "<script>
		alert('Error: $err.');
		$('#val_btn_add_more').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}
		
$response2 = json_decode($response, true);
// echo $response."<br>";
// echo $response2;
echo "<option style='font-size:12px' value=''>--select--</option>";

if($response=='null' || $response=='')
{
	
}
else
{
	// $stop_array=array();
	
	foreach($response2['Stoppages'] as $Data1)
	{
		$from_time = str_replace("+05:30","",str_replace("T"," ",$Data1['From']));
		$to_time = str_replace("+05:30","",str_replace("T"," ",$Data1['To']));
		$duration = $Data1['Duration'];
		$address = $Data1['Address'];
		
		$zipcode = preg_match("/\b\d{6}\b/", $address, $matches);
			
			if(empty($matches[0])){
				$pincode = "";
			}else {
				$pincode = ": ".$matches[0];
			}
			
		$lat = $Data1['Latitude'];
		$long = $Data1['Longitude'];
		$lat_long = $lat.",".$long;
		$from_time_new = date("h:i A",strtotime($from_time));
		$to_time_new = date("h:i A",strtotime($to_time));
		// echo $from_time." to ".$to_time."<br>".$duration."<br>".$address."<br>".$lat."<br>".$long."<br>";
		echo "<option style='font-size:12px' value='$lat_long'>$address $pincode- $from_time_new to $to_time_new</option>";
	}
}

if($_POST['poi_type']=='loading')
{
	echo "<script> 
		$('#loadicon').hide();
		// $('#$start_point_id').selectpicker('refresh');
	</script>";
}
else
{
	echo "<script> 
		$('#loadicon').hide();
		// $('#$end_point_id').selectpicker('refresh');
	</script>";
}
?>