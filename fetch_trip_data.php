<?php
require("./connect.php");

$tno_session = $_SESSION['diary'];
$tno = $_SESSION['diary'];
$trip_id = escapeString($conn,strtoupper($_POST['id']));
$lock_trip_btn = escapeString($conn,($_POST['lock_trip_btn']));
$trip_days_duration = escapeString($conn,($_POST['trip_days_duration']));
$date2 = date("Y-m-d");

if($trip_id=='' || $trip_id==0)
{
	errorLog("Unable to fetch require Trip Data.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Trip not found !","logout.php");
	exit();
}

$sel_date_rule=Qry($conn,"SELECT days FROM dairy.date_rule WHERE type='2'");

if(!$sel_date_rule){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($sel_date_rule)==0)
{
	errorLog("Date Rule not updated for LR Selection.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Date Rule not updated for LR Selection.","logout.php");
	exit();
}

$row_date_rule = fetchArray($sel_date_rule);

$days_ago = date('Y-m-d', strtotime("$row_date_rule[days] days", strtotime($date2)));

$check_trip=Qry($conn,"SELECT t.*,d.name,d.dummy_driver,o.is_crain 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code 
LEFT OUTER JOIN dairy.own_truck AS o ON o.tno = t.tno 
WHERE t.id='$trip_id'");

if(!$check_trip){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($check_trip)==0)
{
	echo "<center><b><font color='red'>Trip not found..</font></b></center>
	<script>$('#trip_branch_view_div').hide();$('#trip_locs_div').hide();$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$driver_name = $row_trip['name'];
$is_dummy_driver = $row_trip['dummy_driver'];

if($driver_name=='')
{
	echo "<br><center><b><font color='red'>ERROR : </font></b>Driver not found.</center>";
	echo "<script>
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$from_stn = escapeString($conn,$row_trip['from_station']);
$trip_no = escapeString($conn,$row_trip['trip_no']);
$to_stn = escapeString($conn,$row_trip['to_station']);
$to_stn_id = escapeString($conn,$row_trip['to_id']);
$trip_date = convertDate("d-m-y","$row_trip[date]");

$trip_branch = escapeString($conn,$row_trip['branch']);
$edit_branch = escapeString($conn,$row_trip['edit_branch']); 
$driver_code = escapeString($conn,$row_trip['driver_code']);
$lr_type = $row_trip['lr_type'];
$lr_type2 = $row_trip['lr_type2'];
$pod_rcvd = $row_trip['pod'];
$trip_lrno = $row_trip['lrno'];
$lr_pending = $row_trip['lr_pending'];
$lr_pending_type = $row_trip['lr_pending_type'];
$supervisor_approval = $row_trip['supervisor_approval'];
$pincode = $row_trip['pincode'];
$fix_lane = $row_trip['fix_lane'];
$consignee_id_trip = $row_trip['consignee_id'];
$is_crain = $row_trip['is_crain'];
// $to_loc_pois = $row_trip['_lat'].",".$row_trip['_long'];

if($lr_type2>2)
{
	errorLog("More than Two LR Found. Truck_no : $tno. Trip-id: $trip_id.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<br>
	<center><b><font color='red'>ERROR : </font></b>Multiple LRs found..</center>";
	echo "<script>
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($lr_type2>0)
{
	$can_rcv_freight="YES";
}
else
{
	$can_rcv_freight="NO";
}

$mb_to_pay=escapeString($conn,$row_trip['mb_to_pay']);
$mb_amount=escapeString($conn,$row_trip['mb_amount']);
$freight_collected=escapeString($conn,$row_trip['freight_collected']);
$freight_adv=escapeString($conn,$row_trip['freight']);

if($branch==$trip_branch || $branch==$edit_branch){
	$spc_branch="1";	
}
else{
	$spc_branch="0";		
}
	
if($row_trip['status']=="1") // TRIP END
{
	$trip_status="1";
	
	if($row_trip['active_trip']=="1"){
		$trip_active="1";
	}
	else{
		$trip_active="0";
	}
	
} 
else // TRIP RUNNING
{
	$trip_status="0";
	$trip_active="1";
}
?>
<script>
		$("#station_div").html("<span style='font-size:13px'><b> <?php echo $from_stn;?> to <?php echo $to_stn." (".$trip_date.")";?></b></font>");
		$(".trip_locations_html").html("<span style='font-size:13px'><b> <?php echo $from_stn;?> to <?php echo $to_stn;?></b></font>");
		$(".trip_id_set").val('<?php echo $trip_id;?>');
		$(".trip_no_set").val('<?php echo $trip_no;?>'); 
		$(".driver_name_set").val('<?php echo $driver_name;?>');
		// $(".driver_code_set").val('<?php echo $driver_code;?>');
		$("#print_button_div").html('<button class="btn btn-xs btn-default pull-right" onclick="PrintTrip(<?php echo $trip_id; ?>)" style="color:#000"><span class="glyphicon glyphicon-print"></span> Print</button>');
		$("#edit_branch_name").html('<?php echo $edit_branch;?>');
		$("#trip_created_by_name").html('<?php echo $trip_branch;?>');
		// $("#cashAdvTrip<?php echo $trip_id; ?>").html('<?php echo $row_trip["cash"]; ?>');
</script>

<div class="form-group col-md-12" id="supervisor_approval_alert" style="display:none;font-size:11px;color:maroon">
	<div class="bg-warning" style="padding:5px;">
		Trip approved by supervisor. Advance, expenses and other entries will remain closed.
		<br>
		ट्रिप सुपरवाइजर द्वारा अप्रूव हो चुकी है. एडवांस एक्सपेंसेस और दूसरी एंट्रिया बंद रहेगी। सहायता के लिए सुपरवाइजर से संपर्क करे.
	</div>
</div>
	
<div class="form-group col-md-6" style="border-right:0px solid gray;overflow:auto">
	
	<div class="row">
		<div class="form-group col-md-12 table-responsive" style="min-height:100px !important;border:0;overflow:auto">
		
		<div class="col-md-12 bg-primary" style="color:#FFF;padding:3px;">
			<span style="font-size:12px;padding-left:5px;">Advance : <?php echo "₹ ".$row_trip['cash']; ?> &nbsp; &nbsp; 
			<button id="cash_modal_button" style="font-size:10px;margin-bottom:1px;color:black" class="btn btn-xs btn-default" data-toggle="modal" 
			data-target="#CashAdvModal"><span class="glyphicon glyphicon-edit"></span> Add</button> </span>
		</div>
				
	<?php
		$qry_cash=Qry($conn,"SELECT vou_no,trans_type,amount,date,narration FROM dairy.cash WHERE trip_id='$trip_id'");
		
		if(!$qry_cash){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		
		echo "<table border='.5' style='color:#000;border:1;width:100%;font-size:11px;'>";
		if(numRows($qry_cash)>0)
		{
			echo "
				<tr>
					<th style='padding:3px;'>VouNo</th>
					<th style='padding:3px;'>Amt</th>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Type</th>
					<th style='padding:3px;'>Narration</th>
				</tr>	
			";	
			$i_cash=1;
			while($row_cash=fetchArray($qry_cash))
			{
				if($row_cash['trans_type']=='CASH'){
					$trans_type_val = "CASH";
				}
				else{
					$trans_type_val = "ATM";
				}
				
				echo "<tr>
						<td style='padding:3px;'>".escapeString($conn,$row_cash['vou_no'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_cash['amount'])."</td>
						<td style='padding:3px;'>".date('d-m-y',strtotime($row_cash['date']))."</td>
						<td style='padding:3px;'>".$trans_type_val."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_cash['narration'])."</td>
					</tr>	
				";
			$i_cash++;	
			}
		}
		else
		{
			echo "<td><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>		
	</div>
</div>
	
	<?php
	/*
	<div class="form-group col-md-12" style="background:#CCC;color:#000">
		<span>Cheque : <?php echo "&#x20B9; ".$row_trip['cheque']; ?> &nbsp; &nbsp;
		<button id="cheque_modal_button" style="color:black" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#ChequeAdvModal" disabled>
		<span class="glyphicon glyphicon-edit"></span> Add</button></span>
	</div>
	
	<div class="form-group col-md-12 table-responsive" style="overflow:auto">
		<?php
		$qry_chq_rtgs=mysqli_query($conn,"SELECT amount,cheq_no,date,bank FROM cheque WHERE trip_id='$trip_id'");
		if(numRows($qry_chq_rtgs)>0)
		{
			echo "<table class='table table-bordered table-striped' style='font-size:11px;'>
				<tr>
					<th>Id</th>
					<th>Amt</th>
					<th>ChqNo</th>
					<th>Date</th>
					<th>AcNo</th>
				</tr>	
			";	
			$i_rtgs=1;
			while($row_rtgs=fetchArray($qry_chq_rtgs))
			{
				echo "<tr>
						<td>$i_rtgs</td>
						<td>".escapeString($conn,$row_rtgs['amount'])."</td>
						<td>".escapeString($conn,$row_rtgs['cheq_no'])."</td>
						<td>".escapeString($conn,$row_rtgs['date'])."</td>
						<td>".escapeString($conn,$row_rtgs['bank'])."</td>
					</tr>	
				";
			$i_rtgs++;	
			}
			echo "</table>";
		}
		else
		{
			echo "<font color='red'><center>No result found.</center></font>";
		}
		?>		
	</div>
	<?php
	*/
	?>
	
<div class="row">
		<div class="form-group col-md-12 table-responsive" style="min-height:100px !important;border:none;overflow:auto">
		
	<div class="col-md-12 bg-primary" style="color:#FFF;padding:3px">
		<span style="font-size:12px;padding-left:5px;">RTGS : <?php echo "₹ ".$row_trip['rtgs']; ?> &nbsp; &nbsp; 
		<button id="rtgs_modal_button" style="font-size:10px;margin-bottom:1px;color:black" class="btn btn-xs btn-default" data-toggle="modal" data-target="#RtgsAdvModal">
			<span class="glyphicon glyphicon-edit"></span> Add</button></span>
	</div>
	
	<?php
		$qry_chq_rtgs2=Qry($conn,"SELECT amount,driver_code,acname,acno,date FROM dairy.rtgs WHERE trip_id='$trip_id'");
		if(!$qry_chq_rtgs2)
		{
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		echo "<table border='.5' style='color:#000;border:1;width:100%;font-size:11px;'>";
		
		if(numRows($qry_chq_rtgs2)>0)
		{
			echo "
			<tr>
					<th style='padding:3px;'>Benf.Name</th>
					<th style='padding:3px;'>AcNo</th>
					<th style='padding:3px;'>Amt</th>
					<th style='padding:3px;'>Date</th>
				</tr>	
			";	
			$i_rtgs2=1;
			while($row_rtgs2=fetchArray($qry_chq_rtgs2))
			{
				echo "<tr>
						<td style='padding:3px;'>".escapeString($conn,$row_rtgs2['acname'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_rtgs2['acno'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_rtgs2['amount'])."</td>
						<td style='padding:3px;'>".date('d-m-y',strtotime($row_rtgs2['date']))."</td>
					</tr>	
				";
			$i_rtgs2++;	
			}
			
		}
		else
		{
			echo "<tr><td colspan='4'><font color='red'><center>No result found.</center></font></td></tr>";
		}
		echo "</table>";
		?>	
		</div>		
</div>
	
<div class="row">
		<div class="form-group col-md-12 table-responsive" style="min-height:100px !important;border:none;overflow:auto">
		
	<div class="col-md-12 bg-primary" style="color:#FFF;padding:3px">
			<span style="font-size:12px;padding-left:5px;">Freight to Driver : <?php echo "₹ ".$row_trip['freight']; ?> &nbsp; &nbsp; 
				<button onclick="LoadFreightBilty('<?php echo $trip_id; ?>')" id="freight_to_driver_modal_button" 
				class="btn btn-xs btn-default" style="font-size:10px;margin-bottom:1px;color:black" data-toggle="modal" data-target="#FreightModal">
			<span class="glyphicon glyphicon-edit"></span> Add</button></span>
		</div>
		
	<?php
		$qry_f1=Qry($conn,"SELECT amount,date,branch,narration FROM dairy.freight_adv WHERE trip_id='$trip_id'");
		
		if(!$qry_f1){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		
		echo "<table border='.5' style='color:#000;border:1;width:100%;font-size:11px;'>";
		if(numRows($qry_f1)>0)
		{
			echo "
				<tr>
					<th style='padding:3px;'>Amt</th>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Narration</th>
				</tr>	
			";	
			$i_f1=1;
			while($row_f1=fetchArray($qry_f1))
			{
				echo "<tr>
						<td style='padding:3px;'>".escapeString($conn,$row_f1['amount'])."</td>
						<td style='padding:3px;'>".date('d-m-y',strtotime($row_f1['date']))."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_f1['branch'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_f1['narration'])."</td>
					</tr>	
				";
			$i_f1++;	
			}
		}
		else
		{
			echo "<td colspan='4'><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>	
		</div>
	 				
	</div>
	
	
	<div class="row">
		<div class="form-group col-md-12 table-responsive" style="min-height:100px !important;border:none;overflow:auto">
		
	<div class="col-md-12 bg-primary" style="color:#FFF;padding:3px">
			<span style="font-size:12px;padding-left:5px;">Credit Driver <span style="font-size:11px">ड्राइवर नामें : </span>&nbsp;<?php echo "₹ ".$row_trip['driver_naame']; ?> &nbsp; &nbsp; 
			<button id="naame_modal_button" class="btn btn-xs btn-default" style="font-size:10px;margin-bottom:1px;color:black" data-toggle="modal" 
			data-target="#NaameModal"><span class="glyphicon glyphicon-edit"></span> Add</button></span>
		</div>
	 			
	<?php
		$qry_naame2=Qry($conn,"SELECT naame_type,amount,date,branch,narration FROM dairy.driver_naame WHERE trip_id='$trip_id'");
		
		if(!$qry_naame2){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		
		echo "<table border='.5' style='color:#000;border:1;width:100%;font-size:11px;'>";
		
		if(numRows($qry_naame2)>0)
		{
			echo "
				<tr>
					<th style='padding:3px;'>Amt</th>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Type</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Narration</th>
				</tr>	
			";	
			$i_naame=1;
			while($row_naame2=fetchArray($qry_naame2))
			{
				echo "<tr>
						<td style='padding:3px;'>".escapeString($conn,$row_naame2['amount'])."</td>
						<td style='padding:3px;'>".date('d-m-y',strtotime($row_naame2['date']))."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_naame2['naame_type'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_naame2['branch'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_naame2['narration'])."</td>
					</tr>	
				";
			$i_naame++;	
			}
			
		}
		else
		{
			echo "<td colspan='5'><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>		
		</div>
	</div>
			
	</div>
		
	<div class="form-group col-md-6" style="overflow:auto;">
	
	<div class="row">
		<div class="form-group col-md-12 table-responsive" style="min-height:100px !important;border:none;overflow:auto">
		
	<div class="col-md-12 bg-primary" style="color:#FFF;padding:3px">
		<span style="font-size:12px;padding-left:5px;">Diesel : <?php echo "₹ ".$row_trip['diesel']. "&nbsp; <span style='font-size:13px'><sup>($row_trip[diesel_qty] LTR)</sup></span>"; ?> &nbsp; &nbsp; 
		<button id="diesel_modal_button" style="font-size:10px;margin-bottom:1px;color:black" onclick="DieselModalOpen('<?php echo $trip_id; ?>')" 
		class="btn btn-xs btn-default"><span class="glyphicon glyphicon-edit"></span> Add</button>
		</span>
	</div>
	
	<?php
		$qry_diesel=Qry($conn,"SELECT d.rate,d.qty,d.amount,d.branch,d.date,d.narration,de.card_pump,pump.consumer_pump 
		FROM dairy.diesel as d 
		LEFT OUTER JOIN dairy.diesel_entry as de on de.id=d.id 
		LEFT OUTER JOIN dairy.diesel_pump_own as pump on pump.code=de.card AND pump.branch=d.branch
		WHERE d.trip_id='$trip_id'");
		
		if(!$qry_diesel){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		
		echo "<table border='.5' style='color:#000;border:1;width:100%;font-size:11px;'>";
		
		if(numRows($qry_diesel)>0)
		{
			$dieselQty=0;
			echo "
				<tr>
					<th style='padding:3px;'>Qty</th>
					<th style='padding:3px;'>Rate</th>
					<th style='padding:3px;'>Amt</th>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Type</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Narration</th>
				</tr>";	
			$i_diesel=1;
			while($row_diesel=fetchArray($qry_diesel))
			{
				if($row_diesel['consumer_pump']=="1"){
				echo "<tr style='background:lightblue'>";
					$dslRate = "NULL";
					$dslAmount = "NULL";
					$dslNrr1 = escapeString($conn,$row_diesel['narration']);
				}
				else{
				echo "<tr>";
					$dslRate = escapeString($conn,$row_diesel['rate']);
					$dslAmount = escapeString($conn,$row_diesel['amount']);
					$dslNrr1 = escapeString($conn,$row_diesel['narration']);
				}
				
				$dieselQty+=$row_diesel['qty'];
				echo"
						<td style='padding:3px;'>".escapeString($conn,$row_diesel['qty'])."</td>
						<td style='padding:3px;'>".$dslRate."</td>
						<td style='padding:3px;'>".$dslAmount."</td>
						<td style='padding:3px;'>".date('d-m',strtotime($row_diesel['date']))."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_diesel['card_pump'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_diesel['branch'])."</td>
						<td style='padding:3px;'>".$dslNrr1."</td>
					</tr>	
				";
			$i_diesel++;	
			}
			
		}
		else
		{
			$dieselQty=0;
			echo "<td colspan='7'><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>	
	<script>
		$("#DieselQtyTrip<?php echo $trip_id; ?>").html('<?php echo $dieselQty; ?> Ltr');		
	</script>
	
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-12 table-responsive" style="min-height:100px !important;border:none;overflow:auto">
		
	<div class="col-md-12 bg-primary" style="color:#FFF;padding:3px">
		<span style="font-size:12px;padding-left:5px;">Expenses : <?php echo "₹ ".$row_trip['expense']; ?> &nbsp; &nbsp; 
		<button id="exp_modal_button" class="btn btn-xs btn-default" style="font-size:10px;margin-bottom:1px;color:black" data-toggle="modal" data-target="#ExpenseModal">
			<span class="glyphicon glyphicon-edit"></span> Add</button></span>
		</div>
	
	<?php
		$qry_exp=Qry($conn,"SELECT exp_name,amount,date,branch,narration FROM dairy.trip_exp WHERE trip_id='$trip_id'");
		
		if(!$qry_exp){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		
		echo "<table border='.5' style='color:#000;border:1;width:100%;font-size:11px;'>";
		
		if(numRows($qry_exp)>0)
		{
			echo "
				<tr>
					<th style='padding:3px;'>Exp</th>
					<th style='padding:3px;'>Amt</th>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Narration</th>
				</tr>	
			";	
			$i_exp=1;
			while($row_exp=fetchArray($qry_exp))
			{
				echo "<tr>
						<td style='padding:3px;'>".escapeString($conn,$row_exp['exp_name'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_exp['amount'])."</td>
						<td style='padding:3px;'>".date('d-m-y',strtotime($row_exp['date']))."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_exp['branch'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_exp['narration'])."</td>
					</tr>	
				";
			$i_exp++;	
			}
			
		}
		else
		{
			echo "<td colspan='5'><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>		
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-12 table-responsive" style="min-height:100px !important;border:none;overflow:auto">
		
	<div class="col-md-12 bg-primary" style="color:#FFF;padding:3px">
			<span style="font-size:12px;padding-left:5px;">Freight collected at branch : 
			<?php echo "₹ ".$row_trip['freight_collected']; ?> &nbsp; &nbsp; 
			<button onclick="LoadFreightCollected('<?php echo $trip_id; ?>')" id="freight_collected_modal_button" 
			style="font-size:10px;margin-bottom:1px;color:black" 
			class="btn btn-xs btn-default" data-toggle="modal" data-target="#FreightCollectedModal">
			<span class="glyphicon glyphicon-edit"></span> Add</button></span>
		</div>
	
	<?php
		$qry_f2=Qry($conn,"SELECT amount,date,branch,narration FROM dairy.freight_rcvd WHERE trip_id='$trip_id'");
		
		if(!$qry_f2){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		
		echo "<table border='.5' style='color:#000;border:1;width:100%;font-size:11px;'>";
		
		if(numRows($qry_f2)>0)
		{
			echo "
				<tr>
					<th style='padding:3px;'>Amt</th>
					<th style='padding:3px;'>Date</th>
					<th style='padding:3px;'>Branch</th>
					<th style='padding:3px;'>Narration</th>
				</tr>	
			";	
			$i_f2=1;
			while($row_f2=fetchArray($qry_f2))
			{
				echo "<tr>
						<td style='padding:3px;'>".escapeString($conn,$row_f2['amount'])."</td>
						<td style='padding:3px;'>".date('d-m-y',strtotime($row_f2['date']))."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_f2['branch'])."</td>
						<td style='padding:3px;'>".escapeString($conn,$row_f2['narration'])."</td>
					</tr>	
				";
			$i_f2++;	
			}
			
		}
		else
		{
			echo "<td colspan='4'><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>		
		</div>
	</div>
		
<?php
if($spc_branch=="1" AND $branch!='PANIPAT')
{
?>
<script>
	$('#edit_branch_div').load('./edit_branch_code.php?edit_branch=<?php echo $edit_branch; ?>&trip_id=<?php echo $trip_id; ?>');
</script>
<?php
}
?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#PODForm").on('submit',(function(e) {
$("#loadicon").show();
$('#pod_button').attr('disabled', true);
e.preventDefault();
	$.ajax({
	url: "./close_trip/rcv_container_pod.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_pod").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form id="PODViewForm" target="_blank" action="./view_pod.php" method="POST">
	<input type="hidden" name="trip_id" value="<?php echo $trip_id; ?>">
</form>
	
<script>
function ViewPOD(){
	$('#PODViewForm').submit();
}
</script>

<div id="result_pod"></div>

<!--<div class="form-group col-md-12 table-responsive" style="border:.5px solid #ddd;overflow:auto;font-size:13px;">-->
	
	<div class="row">
		<div class="form-group col-md-12 table-responsive" style="border:none;overflow:auto;font-size:12px">
		
		<div class="col-md-12 bg-primary" style="color:#FFF;padding:3px">
			<span style="font-size:12px;padding-left:5px;">Rcv POD & END Trip : </span>
		</div>

<?php
if($lr_type=='ATLOADING')
{
?>
	<div class="row">
		<div style="margin-top:10px" class="col-md-12">
			<span style="font-size:13px;color:red">LR Pending : <font color="#000">Add LR to this Trip</font></span>
			&nbsp; <button data-toggle="modal" data-target="#PendingLRModal" type="button" 
			class="btn btn-xs btn-success">Add here</button>
		</div>
	</div>
	<?php
}
else
{
	if($lr_type!='EMPTY')
	{
		if($lr_type=='CON20' || $lr_type=='CON40') // Code for container POD Starts Here....
		{
			if($pod_rcvd=='1')
			{
				echo '<span style="color:blue"><b>POD Received</b></span> 
				&nbsp; <button onclick="ViewPOD('.$trip_id.')" class="btn btn-xs btn-primary">View-POD</button>';
							
				if($trip_status=="0")
				{
				?>
				<br />
					<button style="margin-top:5px;" onclick="EndTrip('<?php echo $trip_id; ?>')" type='button' id='end_trip_button' 
					class='btn btn-danger'><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp; END TRIP</button>
				<?php
				}
			}
			else
			{
				if($trip_status=="1")
				{
				?>
				<br />
				<br />
				<br />
			<div class="row">
				<form id="PODForm">
					<div class="form-group col-md-6">	
						<label>Select LR <font color="red"> <sup>*</sup></font></label>
						<select style="font-size:13px" class="form-control" name="lrnos" required="required">
							<option value="">--select LR number--</option>
							<option value="<?php echo $trip_lrno; ?>"><?php echo $trip_lrno; ?></option>
						</select>
					</div>
								
					<div class="form-group col-md-6">	
						<label>POD Copy <font color="red"> <sup>* (multiple)</sup></font></label>
						<input style="font-size:12px" accept=".jpg,.jpeg,.png,.pdf" type="file" name="pod_copy[]" id="pod_file" class="form-control" multiple required />
					</div>

					<input type="hidden" name="trip_id" value="<?php echo $trip_id; ?>">
					<input type="hidden" name="container_pod" value="1">
								
					<div class="form-group col-md-6">
						<button id="pod_button" type="submit" class="btn btn-sm btn-success btn-block"><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp; Receive POD</button>
					</div>	
				</form>
			</div>	
				<?php
				}
				else
				{
				?>
				<form id="PODForm">
					<div class="row">
								
							<script>
							$('input[type=radio][name=pod_selection]').change(function() {
								if (this.value == '1') {
									$('#end_trip_div').hide();
									$('#pod_submit_form').show();
								}
								else if (this.value == '0') {
									$('#end_trip_div').show();
									$('#pod_submit_form').hide();
								}
							});
							</script>
								
				<div class="col-md-12">&nbsp;</div>
								
				<div class="form-group col-md-6">
					<label>Receive POD <font color='red'> <sup>*</sup></font></label>
					<br>
					<input type="radio" name="pod_selection" value="1"> Yes
						&nbsp; <input type="radio" name="pod_selection" value="0"> No
				</div>
								
				<div id="pod_submit_form" style="display:none">	
					
					<div class="form-group col-md-6">	
						<label>Select LR <font color="red"> <sup>*</sup></font></label>
						<select style="font-size:13px" class="form-control" name="lrnos" required="required">
							<option value="">--select LR number--</option>
							<option value="<?php echo $trip_lrno; ?>"><?php echo $trip_lrno; ?></option>
						</select>
					</div>
								
					<div class="form-group col-md-6" id="pod_form_div">
						<label>POD Copy <font color="red"> <sup>* (multiple)</sup></font></label>
						<input style="font-size:13px" accept=".pdf,.jpg,.jpeg,.png" type="file" name="pod_copy[]" id="pod_file" class="form-control" multiple required />
					</div>	
								
					<input type="hidden" name="trip_id" value="<?php echo $trip_id; ?>">
					<input type="hidden" name="container_pod" value="1">
									
					<div class="form-group col-md-6">
						<label>&nbsp;</label>
						<button id="pod_button" type="submit" class="btn btn-sm btn-success btn-block"><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp; Receive POD</button>
					</div>
				</div>		
						
				<div class="form-group col-md-6" id="end_trip_div" style="display:none">
					<label>&nbsp;</label>
					<button onclick="EndTrip('<?php echo $trip_id; ?>')" type='button' id='end_trip_button' 
					class='btn btn-block btn-sm btn-danger'><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp; END TRIP without POD</button>
				</div>
			</div>
		</form>
		<?php
				}
			}// Code for container POD ENDS Here....
		}
		else // Code for Own and Market LR starts here
		{	
			if(count(explode(',',$trip_lrno))==$pod_rcvd AND $lr_type!='')
			{
				echo '
				<br />
				<br />
				<br />
				<span style="color:blue"><b>POD Received</b></span> 
				&nbsp; <button onclick="ViewPOD('.$trip_id.')" class="btn btn-xs btn-primary">View-POD</button>';
				
				if($row_trip['consignor_id']=='303' AND $row_trip['vedl_gps_status']=='0' AND $row_trip['gps_naame_txn_id']!='')
				{
				?>
					<button onclick="UpdateVedlGpsStatus('<?php echo $trip_id; ?>')" type='button' id='update_gps_status_btn' 
					class='btn btn-xs btn-success'><i class="fa fa-battery-half" aria-hidden="true"></i>&nbsp; VEDL GPS Status</button>
				<?php	
				}
				
				if($trip_status=="0")
				{
				?>
					<button onclick="EndTrip('<?php echo $trip_id; ?>')" type='button' id='end_trip_button' 
					class='btn btn-xs btn-danger'><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp; END TRIP</button>
				<?php
				}
			}
			else
			{
				if($trip_status=="1")
				{
				?><br><br><br>
					<div class="row">
						<div class="form-group col-md-6">
							<button id="pod_button" onclick="PodUploadModal('<?php echo $trip_id; ?>');" type="button" 
							class="btn btn-success btn-sm btn-block">
							<i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp; Receive POD</button>
						</div>
					</div>	
				<?php
				}
				else
				{
				?>
				<div class="row">
								
					<script>
						$('input[type=radio][name=pod_selection]').change(function() {
						if (this.value == '1') {
							$('#end_trip_div').hide();
							$('#pod_submit_form').show();
						}
						else if (this.value == '0') {
							$('#end_trip_div').show();
							$('#pod_submit_form').hide();
						}
						});
					</script>
								
					<div class="col-md-12">&nbsp;</div>
								
						<div class="form-group col-md-6">
							<label>Receive POD <font color='red'> <sup>*</sup></font></label>
							<br>
							<input type="radio" name="pod_selection" value="1"> Yes
							&nbsp; <input type="radio" name="pod_selection" value="0"> No
						</div>
								
						<div id="pod_submit_form" style="display:none">
							<div class="form-group col-md-6">
								<label>&nbsp;</label>
								<button id="pod_button" onclick="PodUploadModal('<?php echo $trip_id; ?>');" type="button" 
								class="btn btn-success btn-sm btn-block">
								<i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp; Receive POD</button>
							</div>
						</div>		
						
								
						<div class="form-group col-md-6" id="end_trip_div" style="display:none">
							<label>&nbsp;</label>
							<br />
							<button onclick="EndTrip('<?php echo $trip_id; ?>')" type='button' id='end_trip_button' 
							class='btn btn-sm btn-block btn-danger'><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp; END TRIP without POD</button>
						</div>
							
				</div>
				<?php
				}
			}
		}
	}
	else
	{
	?>
		<br />
		<br />
		<br />
		<button <?php if($trip_status=="1") { echo "disabled"; } ?> onclick="EndTrip('<?php echo $trip_id; ?>')" type='button' id='end_trip_button' 
		class='btn btn-sm btn-danger'><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp; END Empty TRIP</button>
		<?php
	}
}
?>
	</div>
	</div>
</div>
	
<script>
function EndTrip(id)
{
	<?php
	if($branch=='PANIPAT')
	{
	?>
	alert('You can\'t end/off trip.');
	<?php	
	}
	else
	{
	?>
	document.getElementById('billing_modal_button').click();
	<?php
	// ConfirmAlertSubmitForm("Do you really want to END this trip ?","billing_modal_button");
	}
	?>	
}

function UpdateVedlGpsStatus(id)
{
	<?php
	if($branch!='VAPI')
	{
	?>
		alert('Only Vapi Branch Can Update !');
	<?php	
	}
	else
	{
	?>
		document.getElementById('vedl_gps_rcv_modal_btn').click();
	<?php
	}
	?>	
}
</script>

<!--------------------- VEDL GPS modal starts here --------------------->

<script type="text/javascript">
$(document).ready(function (e) {
$("#VedlGpsForm").on('submit',(function(e) {
$("#loadicon").show();
$('#update_btn_vedl_gps_modal').attr('disabled', true);
e.preventDefault();
	$.ajax({
	url: "./update_vedl_gps_status.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#vedl_gps_rcv_modal_res").html(data);
	},
	error: function() 
	{} });}));});
</script>

<button id="vedl_gps_rcv_modal_btn" style="display:none" type="button" data-toggle="modal" data-target="#RcvGpsVedl"></button>

<form id="VedlGpsForm" style="font-size:13px;" autocomplete="off">
<div id="RcvGpsVedl" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Update Vedanta GPS Status : <?php echo $from_stn." to ".$to_stn;?></h5>
      </div>
	  
	<div class="modal-body">
       <div class="row">
		
			<div class="form-group col-md-12">
				<label>GPS Device Received <font color="red"><sup>*</sup></font></label>
				<select style="font-size:12px !important" name="gps_status" class="form-control" required="required">
					<option style="font-size:12px !important" value="">Select an option</option>
					<option style="font-size:12px !important" value="YES">YES</option>
					<option style="font-size:12px !important" value="NO">NO</option>
				</select>
			</div>	
		
		<input type="hidden" name="id" value="<?php echo $trip_id; ?>">
		
		<div class="form-group col-md-12" id="vedl_gps_rcv_modal_res"></div>
		
		</div>
	</div>
      <div class="modal-footer">
		<button id="update_btn_vedl_gps_modal" type="submit" class="btn btn-sm btn-danger">
		<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp; Save</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
 </div>
</div>
</form>	

<!--------------------- VEDL Gps rcv modal ends here --------------------->


<!--------------------- Trip end modal starts here --------------------->

<button id="billing_modal_button" style="display:none" type="button" data-toggle="modal" data-target="#BillingModal"></button>

<form id="BiltyEndForm" style="font-size:13px;" autocomplete="off">
<div id="BillingModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">END Trip : <?php echo $from_stn." to ".$to_stn;?></h5>
      </div>
	  
	  <script>
	  function BiltyNarration(elem)
	  {
			if(elem=='OTHER'){
				$('#other_narrtion_div').show();
				$('#other_narrtion').attr('required',true);
			} else {
				$('#other_narrtion_div').hide();
				$('#other_narrtion').attr('required',false);
			}
	  }
	  
	  $(".rateButton" ).click(function() {
		if($(this).hasClass('btn-grey')) {			
			$(this).removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
			$(this).prevAll('.rateButton').removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
			$(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');			
		} else {						
			$(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');
		}
		$("#rating").val($('.star-selected').length);		
		// $("#rate_nos_html").html($('.star-selected').length);		
	});
	  </script>
	  
<script type="text/javascript">
$(document).ready(function (e) {
$("#BiltyEndForm").on('submit',(function(e) {
$("#loadicon").show();
$('#end_button_submit').attr('disabled', true);
e.preventDefault();
	$.ajax({
	url: "./close_trip/end_trip.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_pod").html(data);
	},
	error: function() 
	{} });}));});
</script>	  
	<div class="modal-body">
       <div class="row">
<?php
if($mb_amount>0 AND $freight_adv<$mb_amount)
{
?>
			<div class="form-group col-md-7">
				<div class="alert alert-warning" role="alert">
				  This is To-PAY Bilty.<br>Freight is: ₹ <?php echo $mb_amount; ?> & Recovered ₹ <?php echo $freight_adv ?> by Branch !
				</div>
			</div>
			
			<div class="form-group col-md-5">
				<div style="font-size:15px;" class="alert alert-danger" role="alert">
				  Due Amount : ₹ <?php echo $mb_amount-$freight_adv; ?>
				</div>
			</div>
			
			<div class="form-group col-md-7">
				<label>Write Narration About Dues. <font color="red"><sup>*</sup></font></label>
				<select style="font-size:13px" onchange="BiltyNarration(this.value)" name="bilty_narration" class="form-control" required>
					<option value="">Select an option</option>
					<option value="HO">TO BE RECOVERED AT HO BY RTGS (हेड ऑफिस को RTGS प्राप्त होगा)</option>
					<option value="BRANCH_CASH">TO BE RECOVERED BY BRANCH in CASH (ब्रांच को CASH मिलेगा)</option>
					<option value="BRANCH_CHEQUE">TO BE RECOVERED BY BRANCH in CHEQUE (ब्रांच को CHEQUE मिलेगा)</option>
					<option value="OTHER">OTHER NARRATION</option>
				</select>
			</div>	
			
			<div style="display:none" class="form-group col-md-5" id="other_narrtion_div">
				<label>Write your narration here. <font color="red"><sup>*</sup></font></label>
				<textarea class="form-control" name="other_narrtion" id="other_narrtion" required="required"></textarea>
			</div>
			
			<div class="form-group col-md-6">
				<label>Recovery Amount <font color="red"><sup>(expected) *</sup></font></label>
				<input type="number" name="recovery_amount" oninput="ZeroAdvBilty(this.value)" id="recovery_amount" min="0" required class="form-control" />
			</div>
			
		<script>
		function ZeroAdvBilty(amount)
		{
			if(amount==0){
				$('#recovery_date_div').hide();
				$('#recovery_date').attr('required',false);
			}
			else{
				$('#recovery_date_div').show();
				$('#recovery_date').attr('required',true);
			}
		}
		</script>
		
		<div class="form-group col-md-6" id="recovery_date_div" style="display:none">
			<label>Recovery Date <font color="red"><sup>(expected) *</sup></font></label>
			<input type="date" name="recovery_date" id="recovery_date" min="<?php echo date("Y-m-d"); ?>" max="<?php echo date('Y-m-d', strtotime("+7 days", strtotime(date("Y-m-d")))); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" />
		</div>	
			
<?php		
}
?>
		<input type="hidden" name="id" value="<?php echo $trip_id; ?>">
			
		<div class="form-group col-md-12">
		
			<div class="row" style="color:#FFF;padding:5px;background:#666">
				Give rating and feedback to : <?php echo $driver_name ?>
			</div>
				
			<div class="col-md-12">&nbsp;</div>	
			
			<div class="form-group col-md-5">				
				<div class="row">		

				<div class="form-group col-md-12">						
					<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
					</button>
					<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
					</button>
					<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
					</button>
					<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
					</button>
					<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
					</button>
				</div>
				</div>
					<input type="hidden" class="form-control" id="rating" name="rating">					
					
					<div class="row">
					<div class="form-group col-md-12">		
						<br />
						<label style="margin-top:8px;">Write Feedback here</label>
						<textarea class="form-control" id="comment" name="comment"></textarea>
					</div>
					</div>
			</div>		
			
			<div class="form-group col-md-7">				
				<?php
				if($row_trip['end_poi_date']==0 || $row_trip['end_poi_date']=='')
				{
				?>
<script>				
function LoadUnloadPointsEndTrip(fromdate)
{
	$('#trip_end_point_end_trip').val('');
	$("#loadicon").show();
	jQuery.ajax({
	url: "./_fetch_pois.php",
	data: 'date=' + fromdate + '&poi_type=' + 'unloading' + '&req_url=' + 'end_trip_modal' + '&to_id=' + '<?php echo $to_stn_id; ?>' + '&con2_id=' + '<?php echo $consignee_id_trip; ?>' + '&lr_type=' + '<?php echo $lr_type; ?>',
	type: "POST",
	success: function(data) {
		$("#trip_end_point_end_trip").html(data);
	},
	error: function() {}
	});
}

function UnloadingPointSelectedEndTrip(elem)
{
	if($('#trip_end_date_end_trip').val()==''){
		$('#trip_end_date_end_trip').focus();
		$('#trip_end_point_end_trip').val('');
	}
}
</script>				
				<div class="row">	
					
					<div style="color:maroon;font-size:13px;" class="form-group col-md-12">
						गाड़ी जिस दिन खाली हुआ या जिस दिन गंतव्य पर पंहुचा वो वह चुने
					</div>
					
					<div class="form-group col-md-12">	
						<label>Unloading date/Trip end date <font color="red">*</font></label>
						<input onchange="LoadUnloadPointsEndTrip(this.value)" id="trip_end_date_end_trip" name="trip_end_date" type="date" min="<?php echo $row_trip['start_poi_date']; ?>" max="<?php echo date("Y-m-d"); ?>" class="form-control" required="required" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
					</div>
					
					<div style="color:maroon;font-size:13px;" class="form-group col-md-12">
						गाड़ी जिस समय खाली हुआ या गंतव्य पर पंहुचा उस समय अनुसार अनलोडिंग या अंतिम पॉइंट चुने
					</div>
					
					<div class="form-group col-md-12"> 
						<label>Unloading/End Point <sup><font color="red">*</font></sup></label>
						<select onchange="UnloadingPointSelectedEndTrip(this.value)" name="trip_end_point" id="trip_end_point_end_trip" class="form-control" required="required">
							<option style="font-size:12px" value="">Select an option</option>
						</select>
					</div>
				</div>
				<?php				
				}
				?>
			</div>		
		</div>		
		</div>
	</div>
      <div class="modal-footer">
		<button id="end_button_submit" type="submit" class="btn btn-sm btn-danger">
		<i class="fa fa-power-off" aria-hidden="true"></i>&nbsp; Submit and END TRIP</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
 </div>
</div>
</form>	

<!--------------------- Trip end modal ends here --------------------->

<script>
function PodUploadModal(trip_id)
{
	$('#pod_button').attr('disabled', true);
	$("#loadicon").show();
	jQuery.ajax({
	url: "./modal_pod_upload.php",
	data: 'trip_id=' + trip_id,
	type: "POST",
	success: function(data) {
		$("#result_pod_upload_new").html(data);
	},
	error: function() {}
	});
}

function DieselModalOpen(trip_id)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "./modal_diesel_adv.php",
	data: 'trip_id=' + trip_id,
	type: "POST",
	success: function(data) {
		$("#result_load_dsl_modal").html(data);
	},
	error: function() {}
	});
}
</script>

<div id="result_load_dsl_modal"></div>

<div id="result_pod_upload_new"></div>
<div id="result_pod_upload_result"></div>

<script type="text/javascript">
$(document).ready(function (e) {
$("#PendingBiltyForm").on('submit',(function(e) {
$("#loadicon").show();
$("#lr_pending_submit").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./add_lr_to_trip.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#lr_result22").html(data);
	},
	error: function() 
	{} });}));});
</script>

<!-------- Atloading LR add modal start here ---->

<form id="PendingBiltyForm" style="font-size:13px" autocomplete="off">
<div id="PendingLRModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
   <div class="modal-content">
    <div class="bg-primary  modal-header">
        <h5 class="modal-title">At-Loading Trip : <span class="trip_locations_html"></span></h5>
    </div>
	
	<div class="modal-body">
	  <div class="row">
	  
	    <div class="form-group col-md-8">
		
	 <div class="row">	
	 
	<?php
	if($lr_pending=="1" AND $lr_type=='ATLOADING')
	{
		?>
		<div class="form-group col-md-4">	
			<label>Select <?php if($lr_pending_type=='1'){ echo "Own "; } else if($lr_pending_type=='2'){ echo "Market "; } ?>LR <font color="red"><sup>*</sup></font></label>
			<select style="font-size:12px" id="bilty_number_new" onchange="FetchNewLRs(this.value)" name="bilty_number_new" class="form-control" required>
		<?php		
		if($lr_pending_type=="1")
		{
			$qry_pending_lr_fetch=Qry($conn,"SELECT id,frno,date,fstation,tstation,consignor,con1_id,con2_id FROM freight_form_lr 
			WHERE create_date between '$days_ago' AND '$date2' AND truck_no='$tno' AND branch='$branch' AND done=0 ORDER BY id ASC");
			
			if(!$qry_pending_lr_fetch){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while Processing Request.","./");
				exit();
			}
					
			if(numRows($qry_pending_lr_fetch)>0)
			{
				echo '<option style="font-size:12px" class="bilty_sel_values" value="">Select Bilty</option>';
						
				while($row999=fetchArray($qry_pending_lr_fetch))
				{
					echo "<option style='font-size:12px' class='bilty_sel_values' id='bilty_sel_values_$row999[id]' value='$row999[frno]_$row999[id]_$row999[con1_id]_$row999[con2_id]'>$row999[frno]: ".convertDate("d/m/y",$row999['date']).", $row999[fstation] to $row999[tstation], $row999[consignor]</option>";
				}
			}
			else
			{
				echo '<option style="font-size:12px" class="bilty_sel_values" value="">--LR not found--</option>';
			}
			
		}
		else if($lr_pending_type=="2")
		{
			$qry_pending_lr_fetch=Qry($conn,"SELECT id,lrdate,bilty_no,frmstn,tostn FROM mkt_bilty WHERE date between '$days_ago' AND 
			'$date2' AND tno='$tno' AND branch='$branch' AND done=0 ORDER BY id ASC");
			
			if(!$qry_pending_lr_fetch){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while Processing Request.","./");
				exit();
			}
			
			if(numRows($qry_pending_lr_fetch)>0)
			{
				echo '<option class="bilty_sel_values" value="">Select Bilty</option>';
						
				while($row999=fetchArray($qry_pending_lr_fetch))
				{
					echo "<option style='font-size:12px' class='bilty_sel_values' id='bilty_sel_values_$row999[id]' value='$row999[bilty_no]_$row999[id]_0_0'>$row999[bilty_no]: ".convertDate("d/m/y",$row999['lrdate']).", $row999[frmstn] to $row999[tostn]</option>";
				}
			}
			else
			{
				echo '<option style="font-size:12px" class="bilty_sel_values" value="">--LR not found--</option>';
			}
		}	
		else
		{
			echo '<option style="font-size:12px" class="bilty_sel_values" value="">--Invalid LR type--</option>';
		}			
		?>
		</select>
	</div>
		
		<script>
		function FetchNewLRs(elem)
		{
			if(elem!='')
			{
				$("#loadicon").show();
				jQuery.ajax({
				url: "./fetch_lr_data_for_new_lr.php",
				data: 'frno=' + elem + '&trip_id=' + '<?php echo $trip_id; ?>',
				type: "POST",
				success: function(data) {
					$("#lr_result22").html(data);
				},
				error: function() {}
				});
			}
			else
			{
				$('#from_stn_new').val('');
				$('#to_stn_new').val('');
				$('#actual_new').val('');
				$('#charge_new').val('');
				$('#trip_km_new').val('');
				$('#pincode_ip_new').val('');
				$('#lrno_new').val('');
			}
		}
		</script>
		
		<input type="hidden" name="trip_id" value="<?php echo $trip_id; ?>">
		<input type="hidden" name="by_pass_route" id="by_pass_route_at_loading_trip">
			
		<div class="form-group col-md-4">	
			<label>From Station <font color="red">*</font></label>
			<input type="text" readonly name="from_stn_new" id="from_stn_new" class="form-control" required>
		</div>	
		
		<div class="form-group col-md-4">	
			<label>To Station <font color="red">*</font></label>
			<input type="text" readonly name="to_stn_new" id="to_stn_new" class="form-control" required>
		</div>	
		
		<div class="form-group col-md-4">	
			<label>Act Wt <font color="red">*</font></label>
			<input type="number" step="any" readonly name="actual_new" id="actual_new" class="form-control" required>
		</div>	
		
		<div class="form-group col-md-4">	
			<label>Charge Wt <font color="red">*</font></label>
			<input type="number" step="any" readonly name="charge_new" id="charge_new" class="form-control" required>
		</div>	
		
		<div class="form-group col-md-4">	
			<label>Trip KM <font color="red">*</font></label>
			<input type="number" name="trip_km" id="trip_km_new" readonly class="form-control" required>
		</div>
		
		<div class="form-group col-md-3">	
			<label>Pincode <font color="red">*</font></label>
			<input id="pincode_ip_new" readonly oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="6" type="text" value="<?php echo $pincode; ?>" name="pincode" class="form-control" required>
		</div>
		
		<div class="form-group col-md-9">	
			<label>LR No <font color="red">*</font></label>
			<input style="font-size:12px" type="text" readonly name="lrno_new" id="lrno_new" class="form-control" required>
		</div>	
		
		<div class="form-group col-md-4">	
			<label>Trip end date <font color="red">*</font> &nbsp; <input style="width:13px" name="is_running_trip" id="is_running_add_lr_to_trip" type="checkbox" /> <span style="color:blue;font-size:12px">Running trip</span></label>
			<input onchange="LoadUnloadPointsAddLR(this.value)" id="trip_end_date_add_lr_to_trip" name="trip_end_date" type="date" min="<?php echo date("Y-m-d", strtotime("-2 day")); ?>" max="<?php echo date("Y-m-d"); ?>" class="form-control" required="required" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
		</div>
		
		<div class="form-group col-md-4">
			<label>Unloading/End Point <sup><font color="red">*</font></sup></label>
			<select required="required" onchange="UnloadingPointSelected(this.value)" name="trip_end_point" id="trip_end_point_add_lr_to_trip" class="form-control">
				<option style="font-size:12px" value="">Select an option</option>
			</select>
		</div>
		
		<div class="form-group col-md-4">
			<label>Diesel Consumption <sup><font color="red">* (Qty in LTR)</font></sup></label>
			<input type="text" readonly id="atloading_lr_dsl_qty_required" class="form-control">
		</div>
	
	</div>	
	</div>	
	
	<div class="form-group col-md-4">
			<div class="row">
				<div class="col-md-12">
					<div class="map" id="map_add_lr_to_trip" style="border:.5px solid #666;margin-top:5px;width:100%;height:300px">
						<img src="map_def.jpg" style="width:100%;max-height:300px !important" />
					</div>
				</div>
				
				<div class="col-md-12">
					<div style="font-size:13px;padding:5px;border-bottom:.5px solid #666;border-left:.5px solid #666;border-right:.5px solid #666" class="bg-success col-md-12">
						<div class="row">
							<div class="col-md-5">
								<font color="maroon">Trip distance:</font> <span id="km_google_add_lr_to_trip" style="font-size:12px">00 km</span> 
							</div>	
							<div class="col-md-7">
								<font color="maroon">Est. time:</font> <span id="duration_google_add_lr_to_trip" style="font-size:12px">NA</span>
							</div>	
						</div>	
					</div>
				</div>
			</div>
		</div>
		</div>
		
		<div id="lr_result22"></div>
 	 </div>
	 
	 <script>
	 function VerifyDataAddPendingLR()
	 {
		var bilty_no = $('#bilty_number_new').val();
		var end_point = $('#trip_end_point_add_lr_to_trip').val();
		var end_date = $('#trip_end_date_add_lr_to_trip').val();
		var total_act_wt = $('#actual_new').val();
		
		if(bilty_no==''){
			alert('Select LR first !!');
		}
		else
		{
			$("#loadicon").show();
			jQuery.ajax({
			url: "./validate_pois_data.php",
			data: 'trip_id=' + '<?php echo $trip_id; ?>' + '&bilty_no=' + bilty_no + '&running_trip=' + $('#is_running_add_lr_to_trip').is(':checked') + '&end_date=' + end_date + '&end_point=' + end_point + '&total_act_wt=' + total_act_wt,
			type: "POST",
			success: function(data) {
				$("#lr_result22").html(data);
			},
			error: function() {}
			});
		}
	 }
	 </script>
	 
	 <script>
		function UnloadingPointSelected(elem)
		{
			if($('#trip_end_date_add_lr_to_trip').val()=='')
			{
				$('#trip_end_date_add_lr_to_trip').focus();
				$('#trip_end_point_add_lr_to_trip').val('');
			}
		}
		
		function LoadUnloadPointsAddLR(fromdate)
		{
			$('#trip_end_point_add_lr_to_trip').val('');
			$("#loadicon").show();
			jQuery.ajax({
			url: "./_fetch_pois.php",
			data: 'date=' + fromdate + '&poi_type=' + 'unloading' + '&req_url=' + 'add_lr_to_trip',
			type: "POST",
			success: function(data) {
				$("#trip_end_point_add_lr_to_trip").html(data);
			},
				error: function() {}
			});
		}
		
		$(document).on('click','#is_running_add_lr_to_trip',function(){
		
		 $('#trip_end_point_add_lr_to_trip').val('');		
		 
			 if($(this).is(':checked')){
				$('#trip_end_date_add_lr_to_trip').attr('readonly',true);
				$('#trip_end_point_add_lr_to_trip').prop('disabled', true);
			 }
			 else{
				$('#trip_end_date_add_lr_to_trip').attr('readonly',false);
				$('#trip_end_point_add_lr_to_trip').prop('disabled', false);
			 }
		});
	</script>
		
     <div class="modal-footer">
		<button type="button" id="lr_pending_verify_btn" onclick="VerifyDataAddPendingLR()" disabled class="pull-left btn-sm btn btn-primary">Verify details</button>
		<input id="lr_pending_submit" style="display:none" disabled type="submit" value="Update LR" class="btn btn-sm btn-danger" />
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
     </div>
	  
	  <?php		
		}
		?>
    </div>
 </div>
</div>
</form>	

<!-------- Atloading LR add modal ends here ---->

<?php
if($trip_active!="1" AND $spc_branch!="1")
{
	HideEntryBtnsOne(); // hide all btn except exp and naame
}

if($fix_lane!="0")
{
	HideFixLaneBtns(); // cash,rtgs,diesel,exp
}

if($branch=='PANIPAT')
{
	HideEntryBtnsAll(); // hide all btns for specific branch
}

if($is_dummy_driver=='1')
{
	HideEntryBtnsOne();
}

if($lock_trip_btn=='1')
{ 
	TripLocFunc($trip_days_duration); // if hisab is 15 days older
}

if($supervisor_approval=="1")
{
	LockBtnSupervisorApproval();
}

if($is_crain=="1")
{
	HideEntryBtnsForCrain();
}

$chk_diesel_rights = Qry($conn,"SELECT login_type FROM emp_attendance WHERE code='$_SESSION[user_code]'");

if(!$chk_diesel_rights){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_diesel_rights)==0)
{
	AlertError("Employee not found..");
	exit();
}

$row_emp = fetchArray($chk_diesel_rights);

if($row_emp['login_type']=='6')
{
	echo "<script>
		$('#diesel_modal_button').attr('disabled',true);
	</script>";
}
?>	
<script>$("#loadicon").fadeOut('slow');</script>