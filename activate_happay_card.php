<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 
$tno=escapeString($conn,strtoupper($_SESSION['diary']));
$card_no1=escapeString($conn,strtoupper($_POST['card_no1']));
$card_no2=escapeString($conn,strtoupper($_POST['card_no2'])); // last four digit

$get_driver = Qry($conn,"SELECT d.driver_code,t.id 
FROM dairy.own_truck AS d 
LEFT JOIN dairy.trip AS t ON t.tno = d.tno 
WHERE d.tno='$tno'");
	
if(!$get_driver){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_driver)==0)
{
	AlertError("Driver not found..");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

$row_driver = fetchArray($get_driver);
	
$driver_code = $row_driver['driver_code'];
$trip_id = $row_driver['id'];

if($driver_code=='' || empty($driver_code))
{
	AlertError("Driver not found..");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

if($trip_id=='' || empty($trip_id))
{
	AlertError("Running trip not found..");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

$card_no = $card_no1.$card_no2;

$CheckCard=Qry($conn,"SELECT c.card_no2,c.company,c.card_status as status,b.balance,c.card_kit_id,h.tno,h.card_using,h.card_assigned 
FROM dairy.happay_card_inventory AS c 
LEFT OUTER JOIN dairy.happay_live_balance AS b ON b.veh_no=c.veh_no
LEFT OUTER JOIN dairy.happay_card AS h ON h.tno=c.veh_no
WHERE c.veh_no='$tno'");

if(!$CheckCard){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error..");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

if(numRows($CheckCard)>0)
{
	$row_card=fetchArray($CheckCard);
	
	$card_assigned=$row_card['card_assigned'];
	$balance=$row_card['balance'];
	$status=$row_card['status'];
	$company=$row_card['company'];
	$last_four_digit=$row_card['card_no2'];
	$card_kit_id=$row_card['card_kit_id'];
}
else
{
	AlertError("Invalid Card Number !");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

if($status==0)
{
	AlertError("Card is In-Active !");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

if($balance>0)
{
	AlertError("Withdraw card balance first !");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

if($card_assigned==1)
{
	AlertError("Card already assigned !");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

if($card_no2!=$last_four_digit)
{
	AlertError("Check Card Number !");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}

$date = date("Y-m-d");

StartCommit($conn);
$flag = true;
		
$insert_log=Qry($conn,"INSERT INTO dairy.happay_activate_log (tno,driver_code,card_no,branch,date,timestamp) VALUES 
('$tno','$driver_code','$card_kit_id','$branch','$date','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_card=Qry($conn,"UPDATE dairy.happay_card SET card_assigned='1' WHERE tno='$tno'");

if(!$update_card){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_own_truck_table=Qry($conn,"UPDATE dairy.own_truck SET happay_active='1' WHERE tno='$tno'");

if(!$update_own_truck_table){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
	$('#loadicon').hide();
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: 'Success ! Card assigned.',
			showConfirmButton: false,
			timer: 2000
		})
		window.location.href='./';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request..");
	echo "<script>$('#button_happay_active').attr('disabled',false);</script>";
	exit();
}
?>