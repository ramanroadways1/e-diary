<script type="text/javascript">
$(document).ready(function (e) {
$("#CashAdvForm").on('submit',(function(e) {
$("#loadicon").show();
$("#cash_adv_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./cash_adv.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form id="CashAdvForm" autocomplete="off" style="font-size:13px">   
  <div class="modal fade" id="CashAdvModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
		<div class="modal-header bg-primary">
			<span style="font-size:13px">Cash or ATM Advance <span style="font-size:13px">( एटीएम या कॅश एडवांस ) </span> :</span>
		</div>
	<div class="modal-body">
		<div class="row">
			<div class="form-group col-md-6">
				<label>Truck No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
			
			<!--
			<div class="form-group col-md-4">
				<label>Date. <sup><font color="red">*</font></sup></label>
				<input type="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" min="" max="" value="" name="cash_date" class="form-control" readonly required />
			</div>
			-->
			
			<div class="form-group col-md-6">
				<label>Advance Type. <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px" name="adv_trans_type" onchange="TransTypeAdv(this.value)" class="form-control" required="required">
					<option style="font-size:12px" value="">--transaction type--</option>
					<?php
					$getHappayActive_Status = Qry($conn,"SELECT id FROM dairy.happay_card WHERE tno='$tno' AND card_assigned='1'");
					
					if(!$getHappayActive_Status){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error while Processing Request.","./");
					}
					
					if(numRows($getHappayActive_Status)>0)
					{
						$Get_Cash_With_Happay_Branches = Qry($conn,"SELECT id FROM dairy.happay_cash_and_happay_branches WHERE 
						branch='$branch'");
						
						if(!$Get_Cash_With_Happay_Branches){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
							Redirect("Error while Processing Request.","./");
						}

						if(numRows($Get_Cash_With_Happay_Branches)>0)
						{
							echo "<option style='font-size:12px' value='CASH'>CASH ADVANCE</option>";
						}
						echo "<option style='font-size:12px' value='HAPPAY'>HAPPAY (ATM) ADVANCE</option>";
					}
					else
					{
						echo "<option style='font-size:12px' value='CASH'>CASH ADVANCE</option>";
					}
					?>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input type="number" id="advance_amount_wd" oninput="UpdateHappayBal(this.value)" max="10000" min="1" name="amount" class="form-control" required />
			</div>
			
			<script>
			function TransTypeAdv(elem)
			{
				if(elem=='HAPPAY')
				{
					$('#advance_amount_wd').attr('oninput','UpdateHappayBal(this.value)');
				}
				else
				{
					$('#advance_amount_wd').attr('oninput','');
				}
			}
			
			function UpdateHappayBal(amount)
			{
				var curr_bal = $('#happay_bal_in_cash_window').html();//Number(
				
				if(curr_bal!='NA')
				{
					// $('#happay_updated_bal_div').show();
					var new_balance = Number(curr_bal)+Number(amount);
					$('.updated_happay_bal').html(", Updated Balance: "+new_balance);
				}
				else
				{
					// $('#happay_updated_bal_div').hide();
				}
			}
			</script>
			
			<div class="form-group col-md-6">
				<label>Date. <sup><font color="red">*</font></sup></label>
				<input type="text" name="date" value="<?php echo date("Y-m-d"); ?>" class="form-control" readonly required />
			</div>
			
			<input type="hidden" name="trip_id" class="trip_id_set" />
			<input type="hidden" name="trip_no" class="trip_no_set" />
			<input type="hidden" name="driver_name" class="driver_name_set" />
			
			<input type="hidden" name="driver_code" value="<?php echo $driver_code_value; ?>" />
		
			<div class="form-group col-md-12">
				<label>Happay-Card <sup><span style="color:maroon;<?php if(isMobile()){ echo "font-size:9px"; } else { echo "font-size:12px"; } ?>">* Available Bal: 
				<span id="happay_bal_in_cash_window"></span> <span class="updated_happay_bal"></span></span></sup></label>
				<input class="form-control" name="happay_card_no" id="happay_card_no" readonly required>
			</div>
			
			<div class="form-group col-md-12">
				<label>Narration. <sup><font color="red">*</font></sup></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,/]/,'')" class="form-control" name="narration" required></textarea>
			</div>
			
			<div id="happay_updated_bal_div" style="display:none" class="form-group col-md-12">
				<label>Updated Balance is : <span class="updated_happay_bal" style="color:red"></span></label>
			</div>
			
		</div>
        </div> 
        <div class="modal-footer">
			<button <?php if($driver_code_value=="") {echo "disabled"; } ?> type="submit" id="cash_adv_button" class="btn btn-sm btn-danger">Submit</button>
			<button type="button" class="btn btn-sm btn-primary" id="hide_cash" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>