<?php
require("./connect.php");

$distance = escapeString($conn,($_POST['distance']));
$avg_value = escapeString($conn,($_POST['avg_value']));

$trip_id = escapeString($conn,($_POST['trip_id']));
$to_id = escapeString($conn,($_POST['to_id']));
$_SESSION['add_more_trip_km'] = $distance;
	
if(!is_numeric($distance))
{
	AlertError("Invalid distance : $distance !");
	echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#to_loc_for_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

// if($distance<=1)
// {
	// AlertError("Invalid distance : $distance !");
	// echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#to_loc_for_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	// exit();
// }
	
if(empty($trip_id))
{
	AlertError('Trip ID not found');
	echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#to_loc_for_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
	
$get_loc = Qry($conn,"SELECT to_id,km,dsl_consumption FROM dairy.trip WHERE id='$trip_id'");
	
if(!$get_loc){
	AlertError('Error while processing request !');
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_loc) == 0)
{
	AlertError('Trip not found');
	echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#to_loc_for_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_loc = fetchArray($get_loc);
	
if($row_loc['to_id']!=$to_id)
{
	if($distance<$row_loc['km'])
	{
		AlertError('Invalid destination selected !');
		echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#to_loc_for_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	if(empty($avg_value))
	{
		AlertError('Average data not found');
		echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#to_loc_for_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
		
	$_SESSION['add_more_lr_diesel_required'] = sprintf("%.2f",($distance/$avg_value));
}
else
{
	// if($row_loc['dsl_consumption']==0)
	// {
		// $_SESSION['add_more_lr_diesel_required'] = sprintf("%.2f",($distance/$avg_value));
	// }
	// else
	// {
	$_SESSION['add_more_lr_diesel_required'] = $row_loc['dsl_consumption'];
	// }
}
	
echo "<script>$('#add_more_lr_dsl_qty_required').val('".$_SESSION['add_more_lr_diesel_required']."')</script>";
?>