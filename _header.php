<!doctype html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<link rel="icon" type="image/png" href="<?php echo $diary_header; ?>logo_rrpl.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>E-Diary || RAMAN ROADWAYS PVT. LTD.</title>
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw" async defer></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<!-- SWEET ALERT-->
<!--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>-->
<script src="<?php echo $diary_header; ?>sweet_alert_lib/dist/sweetalert2.min.js"></script>
<link rel="stylesheet" href="<?php echo $diary_header; ?>sweet_alert_lib/dist/sweetalert2.min.css">
	<!-- END SWEET ALERT-->
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
	<!--<link rel="stylesheet" href="./rating/style.css">-->
	<meta name="google" content="notranslate">
	<link href="<?php echo $diary_header; ?>google_font.css" rel="stylesheet">
	<!-- Dynamic select box -->
	
	<script src="<?php echo $diary_header; ?>duplicate.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
	
	<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
	<link href="https://html2canvas.hertzen.com/dist/html2canvas.min.js" rel="stylesheet" />
</head>

<!-- 
.modal-body {
    overflow : auto !important;
}
-->
<style>
.modal { overflow: auto !important; } 

.selectpicker { width:auto; font-size: 12px !important;}

th {
  white-space: normal !important; 
  /* word-wrap: break-word; */
}
td {
  white-space: normal !important; 
  /* word-wrap: break-word;  */
}
table {
  /*table-layout: fixed;*/
}

.ui-autocomplete { z-index:2147483647; font-size:13px !important;}

.ui-autocomplete { z-index:2147483647;font-family: Verdana,Arial,sans-serif; font-size:11px !important;}

.filter-option {
    font-size: 12px !important;
}
 
.div1{
	border-right:5px solid red;
} 
</style>

<style>
label{
	font-size:12.5px !important;
}
input[type='date'] { font-size: 12px !important;}
input[type='text'] { font-size: 12px !important;}
select { font-size: 12px !important; }
textarea { font-size: 12px !important; }
</style>

<style>
.modal2 {
  text-align: center;
  padding: 0!important;
}

.modal2:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog2 {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>

<style>	
#salary_bg
{
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	background: url(truck_bg.jpg) no-repeat center center fixed;
	background-size: cover;
	-webkit-filter: blur(4px);    
}

@media screen and (min-width: 600px)  {
   .mobile-break { display: none; }
}
</style>

<style>
.modal-backdrop{
   backdrop-filter: blur(5px);
   background-color: #01223770;
}
.modal-backdrop.in{
   opacity: 1 !important;
}

@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}

@media (min-width: 768px) {
  .modal-xl-mini {
    width: 80%; 
   max-width:100%;
  }
}

::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
</style>

<style>
table{
	font-family:Calibri !important;
}
</style>

<?php
if(isMobile())
{
	echo "<style>label{font-size:11px;}</style>";
}
?>

<div id="loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:150px" src="<?php echo $diary_header; ?>load.gif" /></center>
</div>