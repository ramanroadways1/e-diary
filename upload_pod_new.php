<?php
require("./connect.php");

$id=escapeString($conn,$_POST['id']);
$del_date=escapeString($conn,$_POST['del_date']);
$lrno=escapeString($conn,$_POST['lrno']);
$vou_no=escapeString($conn,$_POST['vou_no']);
$lrno=escapeString($conn,$_POST['lrno']);
$truck_no=escapeString($conn,$_POST['truck_no']);
$trip_no=escapeString($conn,$_POST['trip_no']);
$trip_id=escapeString($conn,$_POST['trip_id']);
$trip_branch=escapeString($conn,$_POST['trip_branch']);
$lr_date=escapeString($conn,$_POST['lr_date']);
$con1_id=escapeString($conn,$_POST['con1_id']);
$tab_id=escapeString($conn,$_POST['tab_id']);
$vou_type=escapeString($conn,$_POST['vou_type']);
$loaded_hisab=escapeString($conn,$_POST['loaded_hisab']);
$lr_bill_party_id=escapeString($conn,$_POST['billparty_id']);
$lr_bill_branch=escapeString($conn,$_POST['billing_branch']);
$is_oxygen_lr=escapeString($conn,$_POST['is_oxygen_lr']);

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

if(strpos($vou_no,'OLRSP') !== false)
{
	AlertError("LR belongs to COAL branch !");
	echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
	exit();
}

if($is_oxygen_lr=="1")
{
	$chk_oxygeny_vou_count = Qry($conn,"SELECT lrno,count FROM oxygen_olr WHERE frno='$vou_no'");
	
	if(!$chk_oxygeny_vou_count){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$row_oxn = fetchArray($chk_oxygeny_vou_count);
	
	if($row_oxn['count']!='3')
	{
		AlertError("Oxygen POD upload allowed in last trip only !");
		echo "<script>
			$('#rcv_button$id').attr('disabled', false);
		</script>";
		exit();
	}
	
	$oxygen_lrno = $row_oxn['lrno'];
}

if($vou_type=='MARKET_BILTY')
{
	$chk_claim_entry = Qry($conn,"SELECT id  FROM mkt_bilty WHERE id='$tab_id' AND claim_entry='0'");
}
else
{
	$chk_claim_entry = Qry($conn,"SELECT id FROM freight_form_lr WHERE id='$tab_id' AND claim_entry='0'");
}

if(!$chk_claim_entry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_claim_entry)>0 AND $is_oxygen_lr!='1')
{
	AlertError("Update claim first !");
	echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
	exit();
}

if((substr($lrno,3,1)!='M') AND ($con1_id=='' || $con1_id==0))
{
	AlertError("Consignor not found !");
	echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
	exit();
}

if(substr($lrno,3,1)=='M')
{
	$get_bill_branch_mb = Qry($conn,"SELECT billing_branch FROM mkt_bilty WHERE bilty_no='$vou_no'");
	
	if(!$get_bill_branch_mb){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertError("LR belongs to COAL branch !");
		echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
		exit();
	}
	
	if(numRows($get_bill_branch_mb)==0)
	{
		AlertError("Market bilty not found !");
		echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
		exit();
	}
	
	$row_bill_branch_mb = fetchArray($get_bill_branch_mb);
	$billing_branch = $row_bill_branch_mb['billing_branch'];
	$billing_party_id="-1";
	
	if($billing_branch==$branch){
		$self_var=1;
	}
	else{
		$self_var=0;
	}
}
else
{
	$bill_func = GetBillPartyBranch($conn,$branch,$con1_id,$lr_bill_branch,$lr_bill_party_id);

	$billing_branch = $bill_func[0];
	$billing_party_id = $bill_func[1];
	$self_var = $bill_func[2];
}

if(count($_FILES['upload_file']['name'])==0)
{
	AlertError("POD not found !");
	echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
	exit();
}

if(count($_FILES['upload_file']['name'])>0)
{
	if($con1_id=='56'){
		$valid_types = array("application/pdf");
		$valid_types_text = "application/pdf";
		
	} else {
		$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");
		$valid_types_text = "image/jpg, image/jpeg, image/bmp, image/gif, image/png, application/pdf";
	}
	
	foreach($_FILES['upload_file']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			AlertError("File format not allowed !<br>Upload pdf or jpg file.");
			echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
			exit();
		}
    }
}

	for($i=0; $i<count($_FILES['upload_file']['name']);$i++) 
	{
		$fix_Name = date('dmYHis').mt_rand().$i;
		$sourcePath = $_FILES['upload_file']['tmp_name'][$i];
		$shortname = "pod/".$fix_Name.".".pathinfo($_FILES['upload_file']['name'][$i],PATHINFO_EXTENSION);
		$targetPath = "close_trip/pod/".$fix_Name.".".pathinfo($_FILES['upload_file']['name'][$i],PATHINFO_EXTENSION);
		
		if($_FILES['upload_file']['type'][$i]!='application/pdf')
		{		
			ImageUpload(800,800,$_FILES['upload_file']['tmp_name'][$i]);
		}
		
	   if(move_uploaded_file($sourcePath, $targetPath))
	   {
			$files[] = $shortname;
	   }
	   else
	   {
		   AlertError("File upload failed !");
			echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
			exit();
	    }
	}

	$file_name=implode(',',$files);

StartCommit($conn);
$flag = true;	

$insert_pod = Qry($conn,"INSERT INTO dairy.rcv_pod(trip_id,tno,vou_no,lrno,trip_no,loaded_vehicle,date,copy,branch,branch_user,consignor_id,fromstation,
tostation,lr_date,del_date,billing_party,billing_ofc,self,exdate,timestamp) VALUES ('$trip_id','$truck_no','$vou_no','$lrno','$trip_no',
'$loaded_hisab','$date','$file_name','$branch','$_SESSION[user_code]','$con1_id','$branch','$billing_branch','$lr_date','$del_date',
'$billing_party_id','$billing_branch','$self_var','$timestamp','$timestamp')");

if(!$insert_pod){
	$flag = false; 
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_pod_id = getInsertID($conn);

$chk_claim_lr = Qry($conn,"SELECT id FROM claim_records WHERE lrno='$lrno' AND vou_no='$vou_no'");

if(!$chk_claim_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_claim_lr)>0)
{
	$claim_branch="1";
	$row_claim = fetchArray($chk_claim_lr);
}
else
{
	$claim_branch="2";
}

$insert_pod_main = Qry($conn,"INSERT INTO rcv_pod(trip_id,tno,trip_no,frno,veh_type,lrno,consignor_id,fromstation,tostation,branch,
branch_user,fm_date,pod_copy,pod_date,del_date,billing_party,billing_ofc,claim_branch,timestamp,self,exdate) VALUES ('$trip_id','$truck_no',
'$trip_no','$vou_no','OWN','$lrno','$con1_id','$branch','$billing_branch','$branch','$_SESSION[user_code]','$lr_date','$file_name','$date','$del_date',
'$billing_party_id','$billing_branch','$claim_branch','$timestamp','$self_var','$date')");

if(!$insert_pod_main){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$pod_id = getInsertID($conn);

if($claim_branch=="1")
{
	$update_pod_id_claim = Qry($conn,"UPDATE claim_records_desc SET pod_id='$pod_id' WHERE claim_id='$row_claim[id]'");
	
	if(!$update_pod_id_claim){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_pod_id_claim2 = Qry($conn,"UPDATE claim_records SET pod_id='$pod_id' WHERE id='$row_claim[id]'");
	
	if(!$update_pod_id_claim2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_pod_id_claim3 = Qry($conn,"UPDATE claim_book_trans SET pod_id='$pod_id' WHERE claim_id='$row_claim[id]'");
	
	if(!$update_pod_id_claim3){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if(substr($lrno,3,1)=='M')
{
	$update_pod_date = Qry($conn,"UPDATE mkt_bilty SET pod_date='$date' WHERE id='$tab_id'");
}
else
{
	if($is_oxygen_lr=="1")
	{
		$update_pod_date = Qry($conn,"UPDATE freight_form_lr SET pod_rcv_date_own='$date' WHERE lrno='$oxygen_lrno' AND truck_no='$truck_no'");
	}
	else
	{
		$update_pod_date = Qry($conn,"UPDATE freight_form_lr SET pod_rcv_date_own='$date' WHERE id='$tab_id'");
	}
}

if(!$update_pod_date){
	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($is_oxygen_lr=="1")
{
	$update_oxygen_pod = Qry($conn,"UPDATE dairy.trip SET pod='1',pod_date='$timestamp' WHERE tno='$truck_no' AND lrno='$oxygen_lrno'");

	if(!$update_oxygen_pod){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$updatePodCount = Qry($conn,"UPDATE dairy.trip SET pod=pod+1,pod_date='$timestamp' WHERE id='$trip_id'");

	if(!$updatePodCount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Success ! POD Received.<br>LR number: $lrno.");
	
	echo "<script> 	
		$('#rcv_button$id').attr('disabled',true);	
		$('#pod_copy_ip$id').attr('disabled',true);	
		$('#del_date$id').attr('disabled',true);	
		$('.del_date_Ip').val('$del_date');
		$('#pod_copy_td$id').html('POD Received.');	
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");
	echo "<script>$('#rcv_button$id').attr('disabled', false);</script>";
	exit();
}	
?>