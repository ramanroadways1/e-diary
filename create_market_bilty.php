<?php
require("./connect.php");

echo "<script>$('#create_bilty_btn').attr('disabled',true);</script>";

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d"); 

$lr_date = escapeString($conn,$_POST['lr_date']);
$lr_no=escapeString($conn,strtoupper($_POST['lr_no']));
$lr_by=escapeString($conn,strtoupper($_POST['lr_by']));
$billing_branch=escapeString($conn,strtoupper($_POST['billing_branch']));

$from_id=escapeString($conn,strtoupper($_POST['from_id']));
$to_id=escapeString($conn,strtoupper($_POST['to_id']));

$broker_id = escapeString($conn,strtoupper($_POST['broker_id']));
$bill_party_id = escapeString($conn,strtoupper($_POST['billing_party_id']));

if(empty($from_id) || empty($to_id))
{
	AlertError("Locations not found..");
	echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

if(empty($broker_id))
{
	AlertError("Broker not found..");
	echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

if(empty($bill_party_id))
{
	AlertError("Billing party not found..");
	echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

if($lr_by=='SHIVANI')
{
	$rate_pmt=0;
	$freight_bilty=0;
	$billing_type="TBB";
	$adv_amount=0;
	$adv_date=0;
	$adv_bilty_narration="";
}
else if($lr_by=='OTHERS')
{
	$rate_pmt=escapeString($conn,strtoupper($_POST['rate_pmt']));
	$freight_bilty=escapeString($conn,strtoupper($_POST['freight_bilty']));
	$billing_type=escapeString($conn,strtoupper($_POST['billing_type']));
	
	if($billing_type=='TBB')
	{
		$adv_amount=escapeString($conn,strtoupper($_POST['adv_amount']));
		$adv_date=escapeString($conn,strtoupper($_POST['adv_date']));
		$adv_bilty_narration=escapeString($conn,strtoupper($_POST['bilty_narration']));
		
		if($adv_amount<0){
			AlertError("Enter Valid Advance Amount !");
			echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
			exit();
		}
		
		if($adv_amount>0 AND $adv_date==0){
			AlertError("Advance date is Invalid !");
			echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
			exit();
		}
		
		if($adv_amount==0)
		{
			$adv_date=0;
			$adv_bilty_narration="";
		}
	}
	else
	{
		$adv_amount=0;
		$adv_date=0;
		$adv_bilty_narration="";
	}
}
else
{
	AlertError("Invalid data posted !");
	echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

$act_weight=escapeString($conn,strtoupper($_POST['act_weight']));
$charge_weight=escapeString($conn,strtoupper($_POST['charge_weight']));
$veh_placer=escapeString($conn,strtoupper($_POST['veh_placer']));
$tno=escapeString($conn,strtoupper($_SESSION['diary']));

$get_from_loc = Qry($conn,"SELECT name FROM station WHERE id='$from_id'");

if(!$get_from_loc){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);AlertError("Error !");echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_from_loc)==0){
	AlertError("From location not found !");echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

$get_to_loc = Qry($conn,"SELECT name FROM station WHERE id='$to_id'");

if(!$get_to_loc){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);AlertError("Error !");echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_to_loc)==0){
	AlertError("To location not found !");echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

$get_bill_party = Qry($conn,"SELECT name,pan FROM dairy.billing_party WHERE id='$bill_party_id'");

if(!$get_bill_party){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);AlertError("Error !");echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_bill_party)==0){
	AlertError("Billing party not found !");echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

$get_broker = Qry($conn,"SELECT name,pan FROM dairy.broker WHERE id='$broker_id'");

if(!$get_broker){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);AlertError("Error !");echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_broker)==0){
	AlertError("Broker not found !");echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

$row_from_loc = fetchArray($get_from_loc);
$row_to_loc = fetchArray($get_to_loc);
$row_bill_party = fetchArray($get_bill_party);
$row_broker = fetchArray($get_broker);

$from = $row_from_loc['name'];
$to = $row_to_loc['name'];
$billing_party = $row_bill_party['name'];
$broker = $row_broker['name'];

$get_company = Qry($conn,"SELECT comp FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_company){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

$row_get_company = fetchArray($get_company);
$company = $row_get_company['comp'];

if(empty($company))
{
	AlertError("Company not found !");
	echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}

$get_vou_id=GetVouId("M",$conn,"mkt_bilty","bilty_no",$branch);

if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id==""){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}
	
$lrno = $get_vou_id;
	
StartCommit($conn);
$flag = true;
	
$insert_bilty = Qry($conn,"INSERT INTO mkt_bilty(company,date,lrdate,bilty_no,lr_by,billing_type,veh_placer,plr,broker,broker_id,
billing_party,bill_party_id,tno,frmstn,tostn,from_id,to_id,awt,cwt,rate,tamt,branch,branch_user,billing_branch,timestamp) 
VALUES ('$company','$date','$lr_date','$lrno','$lr_by','$billing_type','$veh_placer','$lr_no','$broker','$broker_id','$billing_party',
'$bill_party_id','$tno','$from','$to','$from_id','$to_id','$act_weight','$charge_weight','$rate_pmt','$freight_bilty','$branch',
'$_SESSION[user_code]','$billing_branch','$timestamp')");
		
if(!$insert_bilty){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
$insert_bilty_book = Qry($conn,"INSERT INTO dairy.bilty_book(bilty_no,type,branch,branch_user,date,credit,balance,timestamp) VALUES 
('$lrno','$billing_type','$branch','$_SESSION[user_code]','$date','$freight_bilty','$freight_bilty','$timestamp')");
			
if(!$insert_bilty_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$insert_bilty_balance = Qry($conn,"INSERT INTO dairy.bilty_balance(bilty_no,type,freight,balance,timestamp) VALUES ('$lrno',
'$billing_type','$freight_bilty','$freight_bilty','$timestamp')");
			
if(!$insert_bilty_balance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($billing_type=='TBB' AND $lr_by!='SHIVANI')
{
	$insert_narr = Qry($conn,"INSERT INTO dairy.bilty_narration(bilty_no,bilty_type,narration,adv_bal,branch,date,amount,recovery_date,
	timestamp) VALUES ('$lrno','TBB','$adv_bilty_narration','1','$branch','$date','$adv_amount','$adv_date','$timestamp')");
				
	if(!$insert_narr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}	
				
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		document.getElementById('bilty_close_button22').click();
		LRType('');
		$('#lr_type').val('');
		$('#CreateBilty')[0].reset()
		$('#create_bilty_btn').attr('disabled',false);
		document.getElementById('trip_button').click();
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: 'Market Bilty Created !',
			showConfirmButton: false,
			timer: 2000
		})
		$('#loadicon').fadeOut('slow');
	</script>";
	
	// echo "<script>
	// Swal.fire({
	  // html: 'Market Bilty Created !',
	  // icon: 'success',
	  // closeOnConfirm: true,
	 // }).then((result) => {
		// if(result.isConfirmed) {
			// document.getElementById('bilty_close_button').click();
			// LRType('');
			// $('#lr_type').val('');
			// $('#CreateBilty')[0].reset()
			// $('#create_bilty_btn').attr('disabled',false);
			// document.getElementById('trip_button').click();
		// }
	// })
	// </script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error !");
	echo "<script>$('#create_bilty_btn').attr('disabled',false);</script>";
	exit();
}
?>