<div class="modal fade" id="SettingModal" style="font-size:12px" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
		<div class="modal-body">
		<div class="row">
			<div class="form-group col-md-8" style="border-right:0px solid #000">
				<div style="color:#FFF;padding:4px;" class="bg-primary form-group col-md-12">
					<center><span><b>Search HISAB by Trip No or Truck No</b></span></center>
				</div>
				
				<form autocomplete="off" action="hisab/hisab_view.php" method="POST" target="_blank">
				<div class="row">	
					<div class="form-group col-md-4">
						<label>Trip No.</label>
						<input style="font-size:13px" type="text" class="form-control" name="trip_no" required />
						<input type="submit" value="View Hisab" name="submit" class="btn-xs btn btn-danger" />
					</div>
				</form>	
				
				<form autocomplete="off" action="hisab/by_tno.php" method="POST" target="_blank">
					<div class="form-group col-md-8">
					
					<div class="row">
						<div class="col-md-6">
							<label>Truck No.</label>
							<input style="font-size:13px" type="text" id="truck_no_hisab_search" class="form-control" name="truck_no" required />
						</div>
						
						<div class="col-md-6">
							<label>Hisab date range.</label>
							<select style="font-size:12px" name="duration" class="form-control" required>
								<option style="font-size:12px" value="">---SELECT---</option>
								<option style="font-size:12px" value="-0 days">Today's</option>
								<option style="font-size:12px" value="-1 days">Last 2 days</option>
								<option style="font-size:12px" value="-4 days">Last 5 days</option>
								<option style="font-size:12px" value="-6 days">Last 7 days</option>
								<option style="font-size:12px" value="-9 days">Last 10 days</option>
								<option style="font-size:12px" value="-14 days">Last 15 days</option>
								<option style="font-size:12px" value="-29 days">Last 30 days</option>
								<option style="font-size:12px" value="-59 days">Last 60 days</option>
								<option style="font-size:12px" value="-89 days">Last 90 days</option>
								<option style="font-size:12px" value="-119 days">Last 120 days</option>
								<option style="font-size:12px" value="FULL">FULL REPORT</option>
							</select>
						</div>
						
						<div class="col-md-12">
							<input type="submit" value="View Hisab" name="submit" class="btn-xs btn btn-danger" />
						</div>	
					</div>
					</form>
				</div>
			</div>
			</div>
				
			<div class="form-group col-md-4">
				<div style="color:#FFF;padding:4px;" class="bg-primary form-group col-md-12">
					<center><span><b>Take Hisab : Loaded Vehicle.</b></span></center>
				</div>
					
				<div class="row"> 
					<div class="form-group col-md-6 col-md-6 col-xs-6">
						<img class="img-responsive" style="width:80%" src="./img/loaded_truck.PNG">		
					</div>	
					<div class="form-group col-md-6 col-md-6 col-xs-6">
						<button style="font-size:12px;border-radius:12px;color:#000" onclick="LoadedHisab('<?php echo $tno; ?>','<?php echo $trip_no_db; ?>')" type='button' 
						id='hisab_button_loaded' class='btn btn-default'>
						Take Loaded<br>Vehicle Hisab</button>
					</div>
				</div>
		   </div>
				
				
		 <div class="form-group col-md-12">	
				<div style="color:#FFF;padding:4px;" class="bg-primary form-group col-md-12">
					<center><span><b>Driver Account Details .</b></span></center>
				</div>
				
				<div class="row">
					<?php
					$fetch_acno=Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$driver_code_value'");
					
					if(!$fetch_acno){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error while Processing Request.","./");
						exit();
					}

					if(numRows($fetch_acno)>0)
					{
					
					$row_acno=fetchArray($fetch_acno);
					
					if($row_acno['acno']=='')
					{
					?>
				<form id="AcUpdate" autocomplete="off">	
						<div class="form-group col-md-6">
							<label>Ac Holder : <font color="red"><sup>*</sup></font></label>
							<input oninput='this.value=this.value.replace(/[^a-z A-Z]/,"")' type="text" name="update_acname" class="form-control" required>
						</div>
						
						<div class="form-group col-md-6">
							<label>Ac No : <font color="red"><sup>*</sup></font></label>
							<input type="text" oninput='this.value=this.value.replace(/[^0-9]/,"")' name="update_acno" class="form-control" required>
						</div>
						
						<div class="form-group col-md-6">
							<label>Bank : <font color="red"><sup>*</sup></font></label>
							<input oninput='this.value=this.value.replace(/[^a-z A-Z]/,"")' type="text" name="update_bank" class="form-control" required>
						</div>
						
						<div class="form-group col-md-6">
							<label>IFSC : <font color="red"><sup>*</sup></font></label>
							<input oninput='this.value=this.value.replace(/[^a-zA-Z0-9]/,"");ValidateIFSC()' type="text" name="update_ifsc" id="update_ifsc" class="form-control" required>
						</div>
						
						<input type="hidden" value="<?php echo $driver_code_value; ?>" name="account_id" />
						
						<div class="form-group col-md-6">
							<input type="submit" value="Update Account" class="btn btn-sm btn-danger" id="ac_update_button" />
						</div>
						
			</form>
					
					<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("update_ifsc");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
                //alert("Invalid Pan No");
				document.getElementById("update_ifsc").setAttribute(
   "style","background-color:red;text-transform:uppercase;color:#fff;");
   document.getElementById("ac_update_button").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
              //alert("Correct Pan No");
			  document.getElementById("update_ifsc").setAttribute(
   "style","background-color:green;color:#fff;text-transform:uppercase;");
			document.getElementById("ac_update_button").removeAttribute("disabled");
              }
        }
		else
		{
			document.getElementById("update_ifsc").setAttribute(
   "style","background-color:white;color:#fff;text-transform:uppercase;");
	document.getElementById("ac_update_button").removeAttribute("disabled");
		//	$("#pan_div").val("1");
		}
  } 			
</script>	
					
						<?php
					}
					else
					{
						?>
					<div class="col-md-12">
						<b>Ac Holder :</b> <?php echo $row_acno['acname']; ?> <br> 
						<b>Ac No :</b> <?php echo $row_acno['acno']; ?><br> 
						<b>BANK :</b> <?php echo $row_acno['bank']; ?><br> 
						<b>IFSC Code :</b> <?php echo $row_acno['ifsc']; ?>
					</div>
						<?php
					}						
					}
					?>
				</div>
				
				</div>
				
<script type="text/javascript">
$(document).ready(function (e) {
$("#AcUpdate").on('submit',(function(e) {
$("#loadicon").show();
$("#ac_update_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./account_update.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_ac_update").html(data);
	$("#ac_update_button").attr("disabled", false);
	$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script>

<div id="result_ac_update">
</div>
				
		</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary" id="settings_modal_button" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>
  
<script> 
 // function ActiveHappay(card_no,tno,driver_code)
 // {
	 // $("#loadicon").show();
	 // if(card_no=='NA')
	 // {
		 
	 // }
	 // alert(card_no);
	 // alert(tno);
	 // alert(driver_code);
	 // $("#loadicon").hide();
 // }
</script> 

<!-- SETTING MODAL -->