<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$name = escapeString($conn,strtoupper($_POST['name']));
$pan = escapeString($conn,strtoupper($_POST['pan']));
$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$gst = escapeString($conn,strtoupper($_POST['gst']));
$addr = escapeString($conn,($_POST['addr']));

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	AlertRightCornerError("Invalid mobile number !");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}

if(strpos($mobile,'0')===0)
{
	AlertRightCornerError("Invalid mobile number !");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}

if(strlen($mobile)!=10)
{
	AlertRightCornerError("Invalid mobile number !");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $pan) AND $pan!='')
{
	AlertRightCornerError("Invalid PAN number !");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match("/^(0[1-9]|[1-2][0-9]|3[0-5])([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([a-zA-Z0-9]){1}([a-zA-Z]){1}([a-zA-Z0-9]){1}?$/",$gst) AND $gst!='')
{
    AlertRightCornerError("Invalid GST number !");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
} 

$chk_gst = Qry($conn,"SELECT name FROM dairy.billing_party WHERE gst='$gst' AND gst!=''");

if(!$chk_gst){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error !");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}
	
if(numRows($chk_gst)>0)
{
	$row_gst = fetchArray($chk_gst);
	AlertRightCornerError("Duplicate GST number found !<br>Reg. with: $row_gst[name].");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}

$chk_pan = Qry($conn,"SELECT name FROM dairy.billing_party WHERE pan='$pan' AND pan!=''");

if(!$chk_pan){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error !");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}
	
if(numRows($chk_pan)>0)
{
	$row_pan = fetchArray($chk_pan);
	AlertRightCornerError("Duplicate PAN number found !<br>Reg. with: $row_pan[name].");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}

$chk_name = Qry($conn,"SELECT pan,gst FROM dairy.billing_party WHERE name='$name'");

if(!$chk_name){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error !");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($chk_name)>0)
{
	$row_name = fetchArray($chk_name);
	AlertRightCornerError("Duplicate party name !<br>Reg. GST: $row_name[gst]<br>PAN: $row_name[pan].");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}

$insert_party = Qry($conn,"INSERT INTO dairy.billing_party(name,mobile,gst,pan,branch,date,timestamp) VALUES ('$name','$mobile','$gst',
'$pan','$branch','$date','$timestamp')");
	
if(!$insert_party){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error !");
	echo "<script>$('#submit_btn').attr('disabled', false);</script>";
	exit();
}

AlertRightCornerSuccess("Party added successfully !");
echo "<script>
	$('#submit_btn').attr('disabled', false);
	$('#FormBillParty').trigger('reset');
</script>";
exit();
?>