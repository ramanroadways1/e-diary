<?php
session_start();

date_default_timezone_set('Asia/Kolkata');

include($_SERVER['DOCUMENT_ROOT']."/_connect.php");
// include($_SERVER['DOCUMENT_ROOT']."/rrpl/_connect.php");
	
if(isset($_SESSION['user']))
{
	$branch_name = $_SESSION['user'];
}
else if(isset($_SESSION['rrpl_ship']))
{
	$branch_name = $_SESSION['rrpl_ship'];
}
else
{
	echo "<script>
		window.location.href='/';
	</script>";
	exit();
}

if(isset($_SESSION['diary']))
{
	echo "<script>
		window.location.href='./';
	</script>";
	exit();
}

function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>E-Diary || RAMAN ROADWAYS PVT. LTD.</title>
	<link href='https://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  	
	<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<style>
label{
	font-size:13px;
}

.ui-autocomplete { z-index:2147483647; font-size:12px;}

body {
  background: #999;
  padding: 40px;
}

#bg {
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: url(truck_bg.jpg) no-repeat center center fixed;
  background-size: cover;
  -webkit-filter: blur(4px);    
}
</style>

<script type="text/javascript">
$(function() {
    $( "#tno" ).autocomplete({
      source: './autofill/own_tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			alert('Truck No does not exists.');
			$("#tno").val("");
			$("#tno").focus();
		}
    }, 
    focus: function (event, ui){
        return false;
    }
    });
  });
</script>

<script>
<!--
//Disable right mouse click Script
var message="Function Disabled!";
///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}
function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}
if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}
document.oncontextmenu=new Function("return false")

function disableCtrlKeyCombination(e)
{
        //list all CTRL + key combinations you want to disable
        var forbiddenKeys = new Array('a', 'c', 'x', 'v');
        var key;
        var isCtrl;
        if(window.event)
        {
                key = window.event.keyCode;     //IE
                if(window.event.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        else
        {
                key = e.which;     //firefox
                if(e.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        //if ctrl is pressed check if other key is in forbidenKeys array
        if(isCtrl)
        {
                for(i=0; i<forbiddenKeys.length; i++)
                {
                        //case-insensitive comparation
                        if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                        {
                                alert('Key combination CTRL + ' +String.fromCharCode(key)+' has been disabled.');
                                return false;
                        }
                }
        }
        return true;
}
// --> 
</script>

<div id="bg"></div>

<body onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);" style="font-family: 'Open Sans', sans-serif !important">

<div class="container" style="">
	
	<form action="" method="POST">
	
	<div class="form-group col-md-4 col-md-offset-4 col-sm-12 col-xs-12">
		<center><h3 style="<?php if(isMobile()) { echo "font-size:16px"; } ?>">RAMAN - ROADWAYS</h3></center>
	</div>	
	
	<div class="form-group col-md-4 col-md-offset-4 col-sm-12 col-xs-12" style="border:.5px solid gray;border-radius:9px;box-shadow:0 0 5px #ddd inset">
		
		<div class="form-group col-md-12" style="background:gray;color:#FFF;<?php if(isMobile()) { echo "padding:3px;"; } else { echo "padding:5px;"; } ?>">
			<center><b>Driver e-Diary</b></center>
		</div>
		
		<script>
		function Func1()
		{
			var tno1=$('#tno1').val();
			
			if(tno1=='')
			{
				$('.back_div').show();
				$(".login_div").removeClass("col-md-12").addClass("col-md-6");
				
				$('#tno2_div').hide();
				$('#tno1_div').show();
				$('#login_button').attr('disabled',true);
			}
			else
			{
				$('#tno2_div').show();
				$('#tno1_div').hide();
				$('#login_button').attr('disabled',true);
				
				$('.back_div').hide();
				$(".login_div").removeClass("col-md-6").addClass("col-md-12");
			}
		}
		
		function Func2()
		{
			var tno1=$('#tno1').val();
			var tno2=$('#tno').val();
			
			if(tno1=='')
			{
				$('#tno2_div').hide();
				$('#tno1_div').show();
				$('#login_button').attr('disabled',true);
				
				$('.back_div').show();
				$(".login_div").removeClass("col-md-12").addClass("col-md-6");
			}
			else if(tno2=='')
			{
				$('#tno2_div').show();
				$('#tno1_div').hide();
				$('#login_button').attr('disabled',true);
				
				$('.back_div').hide();
				$(".login_div").removeClass("col-md-6").addClass("col-md-12");
			}
			else
			{
				if(tno1.toUpperCase()==tno2)
				{
					$('#tno').attr('readonly',true);
					$('#login_button').attr('disabled',false);
					
					$('.back_div').hide();
					$(".login_div").removeClass("col-md-6").addClass("col-md-12");
				}
				else
				{
					alert('Truck no not matching.');
					$('#tno1').val('');
					$('#tno').val('');
					$('#tno2_div').hide();
					$('#tno1_div').show();
					$('#login_button').attr('disabled',true);
					
					$('.back_div').show();
					$(".login_div").removeClass("col-md-12").addClass("col-md-6");
				}
			}
		}
		
		function ResetAll()
		{
					$('#tno1').val('');
					$('#tno').val('');
					$('#tno2_div').hide();
					$('#tno1_div').show();
					$('#login_button').attr('disabled',true);
		}
		</script>
		
		<div id="tno1_div" class="form-group col-md-12">
			<label style="<?php if(isMobile()) { echo "font-size:11px"; } ?>">Enter Truck No <font color="red"><sup>*</sup></font></label>
			<input style="font-size:12px" type="text" onblur="Func1();" name="tno1" id="tno1" class="form-control" style="text-transform:uppercase" required />
		</div>
		
		<div style="display:none" id="tno2_div" class="form-group col-md-12">
			<label style="<?php if(isMobile()) { echo "font-size:11px"; } ?>"><font color="red">Verify Truck No <sup>*</sup></font> <?php if(!isMobile()) {?>(enter last 4 digits) <?php } ?></label>
			<input style="font-size:12px" type="text" onblur="Func2();" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="tno" id="tno" class="form-control" style="text-transform:uppercase" required />
		</div>
		
		<div class="form-group back_div col-md-6 col-sm-6 col-xs-6">
			<a href="../" style="text-decoration:none"><button type="button" class="btn <?php if(isMobile()) { echo "btn-sm"; } ?> btn-default btn-block" style="margin-top:8px;"><i class="fa fa-backward" aria-hidden="true"></i> &nbsp; Go back</button></a>
		</div>
		
		<div class="form-group login_div col-md-6 col-sm-6 col-xs-6">
			<button type="submit" name="login" class="btn btn-primary btn-block <?php if(isMobile()) { echo "btn-sm"; } ?>" id="login_button" style="margin-top:8px;" disabled><i class="fa fa-sign-in" aria-hidden="true"></i> &nbsp; Log in</button>
		</div>	
		
	</div>
	</form>
</div>
</body>
<?php
if(isset($_POST['login']))
{
	$chk_vehicle = Qry($conn,"SELECT ediary_active FROM dairy.own_truck WHERE tno='$_POST[tno]'");
	
	if(!$chk_vehicle){
		unset($_SESSION['diary']);
		unset($_SESSION['diary_last_login']);
		unset($_SESSION['token_number']);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($chk_vehicle)==0)
	{
		unset($_SESSION['diary']);
		unset($_SESSION['diary_last_login']);
		unset($_SESSION['token_number']);
		
		echo "<script>
			window.location.href='./';
		</script>";
		exit();
	}
	
	$row_vehicle = fetchArray($chk_vehicle);
	
	if($row_vehicle['ediary_active']!='1')
	{
		unset($_SESSION['diary']);
		unset($_SESSION['diary_last_login']);
		unset($_SESSION['token_number']);
		
		echo "<script>
			alert('Vehicle in-active !');
			window.location.href='./';
		</script>";
		exit();
	}
	
	$_SESSION['diary']=$_POST['tno'];
	$_SESSION['diary_last_login']=time();
	$_SESSION['token_number'] = sha1(md5(date("Ymdhis").mt_rand()));
	
	// $insert_token = Qry($conn,"INSERT INTO dairy.token(branch,session_id,token,timestamp) VALUES ('$_SESSION[user]','$_POST[tno]',
	// '$_SESSION[token_number]','".date("Y-m-d H:i:s")."')");
	
	// if(!$insert_token){
		// unset($_SESSION['diary']);
		// unset($_SESSION['diary_last_login']);
		// unset($_SESSION['token_number']);
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// exit();
	// }
	
	echo "<script>
			window.location.href='./';
	</script>";
	exit();
}
?>