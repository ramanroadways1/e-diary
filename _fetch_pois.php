<?php
require_once './connect.php'; 

$timestamp = date("Y-m-d H:i:s");
$req_url = escapeString($conn,$_POST['req_url']);
$date = date("d-m-Y",strtotime(escapeString($conn,($_POST['date']))));
$veh_no = escapeString($conn,$_SESSION['diary']);
$start_date = $date."T00:00:01";
$end_date = $date."T23:59:59";
$Duration1= "2"; // in minutes

echo "<option style='font-size:12px' value=''>--select--</option>";

if($req_url=='create_trip')
{
	$lr_type = escapeString($conn,$_POST['lr_type']);
	$vou_no_olr = escapeString($conn,$_POST['vou_no_olr']);
	
	if(escapeString($conn,$_POST['poi_type'])=='loading')
	{
		$from_id = escapeString($conn,$_POST['from_id']);
		
		if($lr_type=='OWN' AND $vou_no_olr=='')
		{
			echo "<script>$('#loading_start_date').val('');$('#trip_loading_point').val('').selectpicker('refresh');</script>";
			AlertError("LR not found !");
			exit();
		}
		
		if($lr_type=='OWN' AND explode("_",$vou_no_olr)[0]!='ATLOADING')
		{
			$vou_id = explode("_",$vou_no_olr)[1];
			
			$get_my_loading_points = Qry($conn,"SELECT label,_lat,_long,pincode FROM address_book_consignor WHERE from_id='$from_id' 
			AND consignor=(SELECT con1_id FROM freight_form_lr WHERE id='$vou_id')");

			if(!$get_my_loading_points){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				AlertError("Error while processing request !");
				exit();
			}
			
			if(numRows($get_my_loading_points)>0)
			{
				$row_loading_p = fetchArray($get_my_loading_points);
				echo "<option style='font-size:12px' value='$row_loading_p[_lat]_$row_loading_p[_long]_$row_loading_p[pincode]'>Loading Point - $row_loading_p[label]</option>";
			}
			else
			{
				echo "<option style='font-size:12px' value=''>--Loading Point not found--</option>";
			}
			
			echo "<script> 
				$('#trip_loading_point').selectpicker('refresh');
				$('#loadicon').fadeOut('slow')
			</script>";
			// exit();
		}
	}
	else
	{
		$to_id = escapeString($conn,$_POST['to_id']);
		
		if($lr_type=='OWN' AND $vou_no_olr=='')
		{
			echo "<script>$('#unloading__date').val('');$('#trip_end_point').val('').selectpicker('refresh');</script>";
			AlertError("LR not found !");
			exit();
		}
		
		if($lr_type=='OWN' AND explode("_",$vou_no_olr)[0]!='ATLOADING')
		{
			$vou_id = explode("_",$vou_no_olr)[1];
			
			$get_voucher = Qry($conn,"SELECT f.mother_lr_id,l.con2_id,l.shipment_party_id 
			FROM freight_form_lr AS f 
			LEFT OUTER JOIN lr_sample AS l ON l.id = f.mother_lr_id 
			WHERE f.id='$vou_id')");

			if(!$get_voucher){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				AlertError("Error while processing request !");
				exit();
			}
			
			if(numRows($get_voucher)==0)
			{
				echo "<script>$('#unloading__date').val('');$('#trip_end_point').val('').selectpicker('refresh');</script>";
				AlertError("Voucher not found !");
				exit();
			}
			
			$row_vou = fetchArray($get_voucher);
			
			if($row_vou['shipment_party_id']!=0 AND $row_vou['shipment_party_id']!=$row_vou['con2_id'])
			{
				$con2_id = $row_vou['shipment_party_id'];
			}
			else
			{
				$con2_id = $row_vou['con2_id'];
			}
			
			$get_my_unloading_points = Qry($conn,"SELECT label,_lat,_long,pincode FROM address_book_consignee WHERE to_id='$to_id' AND consignee='$con2_id'");

			if(!$get_my_unloading_points){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				AlertError("Error while processing request !");
				exit();
			}
			
			if(numRows($get_my_unloading_points)>0)
			{
				$row_up = fetchArray($get_my_unloading_points);
				echo "<option style='font-size:12px' value='$row_up[_lat]_$row_up[_long]_$row_up[pincode]'>Unloading Point - $row_up[label]</option>";
			}
			else
			{
				echo "<option style='font-size:12px' value=''>--Loading Point not found--</option>";
			}
			
			echo "<script> 
				$('#trip_end_point').selectpicker('refresh');
				$('#loadicon').fadeOut('slow')
			</script>";
			exit();
		}
	}
	
	$start_point_id = "trip_loading_point";
	$end_point_id = "trip_end_point";
	
	$gps_veh_by_pass = Qry($conn,"SELECT id FROM _functions WHERE func_type='GPS_VEHICLE_BY_PASS' AND func_value='$veh_no' AND is_active='1'");

	if(!$gps_veh_by_pass){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error while processing request !");
		exit();
	}
	
	$chk_location_based_poi = Qry($conn,"SELECT id FROM _functions WHERE func_type='LOCATION_BASED_POI' AND is_active='1'");

	if(!$chk_location_based_poi){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error while processing request !");
		exit();
	}
	
	$chk_gps_active_or_not = Qry($conn,"SELECT gps_timestamp FROM dairy._gps_position WHERE tno='$veh_no' AND gps_timestamp!=0");

	if(!$chk_gps_active_or_not){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error while processing request !");
		exit();
	}
	
	$row_gps = fetchArray($chk_gps_active_or_not);
	
	$time_now = strtotime(date("Y-m-d H:i:s"));
	$time_gps = strtotime($row_gps['gps_timestamp']);
	$gps_hours = (($time_now-$time_gps) / ( 60 * 60 ));
	
	if(escapeString($conn,($_POST['poi_type']=='loading')))
		{
			$from_id = escapeString($conn,$_POST['from_id']);
			
			$get_from_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$from_id'");

			if(!$get_from_loc_poi){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				AlertError("Error while processing request !");
				exit();
			}

			if(numRows($get_from_loc_poi)==0)
			{
				AlertError("From location not found !");
				exit();
			}

			$row_from_poi = fetchArray($get_from_loc_poi);
			
			$lat_long = $row_from_poi['_lat'].",".$row_from_poi['_long'];
			echo "<option style='font-size:12px' value='$row_from_poi[_lat]_$row_from_poi[_long]_$row_from_poi[pincode]'>Location - $row_from_poi[name]</option>";
		}
		else
		{
			$to_id = escapeString($conn,$_POST['to_id']);
			
			$get_to_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$to_id'");

			if(!$get_to_loc_poi){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				AlertError("Error while processing request !");
				exit();
			}

			if(numRows($get_to_loc_poi)==0)
			{
				AlertError("Destination location not found !");
				exit();
			}

			$row_to_poi = fetchArray($get_to_loc_poi);
			
			$lat_long = $row_to_poi['_lat'].",".$row_to_poi['_long'];
			echo "<option style='font-size:12px' value='$row_to_poi[_lat]_$row_to_poi[_long]_$row_to_poi[pincode]'>Location - $row_to_poi[name]</option>";
		}
		
	if(numRows($gps_veh_by_pass)>0)
	{
		/// BY PASS GPS VEHICEL BASED ------------------
		
		// if(escapeString($conn,($_POST['poi_type']=='loading')))
		// {
			// $from_id = escapeString($conn,$_POST['from_id']);
			
			// $get_from_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$from_id'");

			// if(!$get_from_loc_poi){
				// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				// AlertError("Error while processing request !");
				// exit();
			// }

			// if(numRows($get_from_loc_poi)==0)
			// {
				// AlertError("From location not found !");
				// exit();
			// }

			// $row_from_poi = fetchArray($get_from_loc_poi);
			
			// $lat_long = $row_from_poi['_lat'].",".$row_from_poi['_long'];
			// echo "<option style='font-size:12px' value='$row_from_poi[_lat]_$row_from_poi[_long]_$row_from_poi[pincode]'>Location - $row_from_poi[name]</option>";
		// }
		// else
		// {
			// $to_id = escapeString($conn,$_POST['to_id']);
			
			// $get_to_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$to_id'");

			// if(!$get_to_loc_poi){
				// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				// AlertError("Error while processing request !");
				// exit();
			// }

			// if(numRows($get_to_loc_poi)==0)
			// {
				// AlertError("Destination location not found !");
				// exit();
			// }

			// $row_to_poi = fetchArray($get_to_loc_poi);
			
			// $lat_long = $row_to_poi['_lat'].",".$row_to_poi['_long'];
			// echo "<option style='font-size:12px' value='$row_to_poi[_lat]_$row_to_poi[_long]_$row_to_poi[pincode]'>Location - $row_to_poi[name]</option>";
		// }
	}
	else if($gps_hours>24 AND numRows($chk_location_based_poi)>0)
	{
		if(escapeString($conn,($_POST['poi_type']=='loading')))
		{
			$from_id = escapeString($conn,$_POST['from_id']);
			
			$get_from_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$from_id'");

			if(!$get_from_loc_poi){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				AlertError("Error while processing request !");
				exit();
			}

			if(numRows($get_from_loc_poi)==0)
			{
				AlertError("From location not found !");
				exit();
			}

			$row_from_poi = fetchArray($get_from_loc_poi);
			
			$lat_long = $row_from_poi['_lat'].",".$row_from_poi['_long'];
			echo "<option style='font-size:12px' value='$row_from_poi[_lat]_$row_from_poi[_long]_$row_from_poi[pincode]'>Location - $row_from_poi[name]</option>";
		}
		else
		{
			$to_id = escapeString($conn,$_POST['to_id']);
			
			$get_to_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$to_id'");

			if(!$get_to_loc_poi){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				AlertError("Error while processing request !");
				exit();
			}

			if(numRows($get_to_loc_poi)==0)
			{
				AlertError("Destination location not found !");
				exit();
			}

			$row_to_poi = fetchArray($get_to_loc_poi);
			
			$lat_long = $row_to_poi['_lat'].",".$row_to_poi['_long'];
			echo "<option style='font-size:12px' value='$row_to_poi[_lat]_$row_to_poi[_long]_$row_to_poi[pincode]'>Location - $row_to_poi[name]</option>";
		}
	}
}
else if($req_url=='add_more_lr')
{
	$start_point_id = "trip_start_point_add_more";
	$end_point_id = "trip_end_point_add_more";
}
else if($req_url=='add_lr_to_trip')
{
	$start_point_id = "trip_end_point_add_lr_to_trip";
	$end_point_id = "trip_end_point_add_lr_to_trip";
}
else if($req_url=='end_trip_modal')
{
	$end_point_id = "trip_end_point_end_trip";
	
	$to_id = escapeString($conn,$_POST['to_id']);
	$con2_id = escapeString($conn,$_POST['con2_id']);
	$lr_type = escapeString($conn,$_POST['lr_type']);
	
	if($con2_id!=0)
	{
		// $olr_vou_no = array();
		
		// foreach(explode(",",$lr_type) as $lr_type2)
		// {
			// if(substr($lr_type2,3,3)=='OLR'){
				// $olr_vou_no[]="'".$lr_type2."'";
			// }
		// }
		
		// $olr_vou_no = implode(",",$olr_vou_no)
		
		// if(!empty($olr_vou_no))
		// {
			// $get_valid_consignee = Qry($conn,"SELECT id,mother_lr_id FROM freight_form_lr WHERE frno in($olr_vou_no) AND con2_id='$con2_id'");

			// if(!$get_valid_consignee){
				// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				// AlertError("Error while processing request !");
				// exit();
			// }
			
			// if(numRows($get_valid_consignee)==0)
			// {
				// AlertError("Voucher not found !");
				// echo "<script> 
					// $('#trip_end_date_end_trip').val('');
					// $('#trip_end_point_end_trip').val('');
					// $('#trip_end_point_end_trip').selectpicker('refresh');
					// $('#loadicon').fadeOut('slow')
				// </script>";
				// exit();
			// }
			
			// $row_get_consignee = fetchArray($get_valid_consignee);
			
			// $mother
			
			// if(numRows($get_my_loading_points)>0)
			// {
				// $row_loading_p = fetchArray($get_my_loading_points);
				// echo "<option style='font-size:12px' value='$row_loading_p[_lat]_$row_loading_p[_long]_$row_loading_p[pincode]'>Loading Point - $row_loading_p[label]</option>";
			// }
			 
			$get_my_unloading_points = Qry($conn,"SELECT label,_lat,_long,pincode FROM address_book_consignee WHERE to_id='$to_id' AND consignee='$con2_id'");

			if(!$get_my_unloading_points){
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				AlertError("Error while processing request !");
				exit();
			}
			
			if(numRows($get_my_unloading_points)>0)
			{
				$row_up = fetchArray($get_my_unloading_points);
				echo "<option style='font-size:12px' value='$row_up[_lat]_$row_up[_long]_$row_up[pincode]'>Unloading Point - $row_up[label]</option>";
			}
			else
			{
				echo "<option style='font-size:12px' value=''>--Unloading Point not found--</option>";
			}
			
				// echo "<script> 
						// $('#trip_end_point_end_trip').selectpicker('refresh');
					// $('#loadicon').fadeOut('slow')
				// </script>";
				// exit();
		// }
	}
	
	$gps_veh_by_pass = Qry($conn,"SELECT id FROM _functions WHERE func_type='GPS_VEHICLE_BY_PASS' AND func_value='$veh_no' AND is_active='1'");

	if(!$gps_veh_by_pass){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error while processing request !");
		exit();
	}
	
	$chk_location_based_poi = Qry($conn,"SELECT id FROM _functions WHERE func_type='LOCATION_BASED_POI' AND is_active='1'");

	if(!$chk_location_based_poi){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error while processing request !");
		exit();
	}
	
	$chk_gps_active_or_not = Qry($conn,"SELECT gps_timestamp FROM dairy._gps_position WHERE tno='$veh_no' AND gps_timestamp!=0");

	if(!$chk_gps_active_or_not){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertError("Error while processing request !");
		exit();
	}
	
	$row_gps = fetchArray($chk_gps_active_or_not);
	
	$time_now = strtotime(date("Y-m-d H:i:s"));
	$time_gps = strtotime($row_gps['gps_timestamp']);
	$gps_hours = (($time_now-$time_gps) / ( 60 * 60 ));

	$get_to_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$to_id'");

		if(!$get_to_loc_poi){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			AlertError("Error while processing request !");
			exit();
		}

		if(numRows($get_to_loc_poi)==0)
		{
			AlertError("Destination location not found !");
			exit();
		}

		$row_to_poi = fetchArray($get_to_loc_poi);
		
		$lat_long = $row_to_poi['_lat'].",".$row_to_poi['_long'];
		echo "<option style='font-size:12px' value='$row_to_poi[_lat]_$row_to_poi[_long]_$row_to_poi[pincode]'>Location - $row_to_poi[name]</option>";
		
	if(numRows($gps_veh_by_pass)>0)
	{
		// $get_to_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$to_id'");

		// if(!$get_to_loc_poi){
			// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			// AlertError("Error while processing request !");
			// exit();
		// }

		// if(numRows($get_to_loc_poi)==0)
		// {
			// AlertError("Destination location not found !");
			// exit();
		// }

		// $row_to_poi = fetchArray($get_to_loc_poi);
		
		// $lat_long = $row_to_poi['_lat'].",".$row_to_poi['_long'];
		// echo "<option style='font-size:12px' value='$row_to_poi[_lat]_$row_to_poi[_long]_$row_to_poi[pincode]'>Location - $row_to_poi[name]</option>";
	}
	else if($gps_hours>24 AND numRows($chk_location_based_poi)>0)
	{
		$get_to_loc_poi = Qry($conn,"SELECT name,_lat,_long,pincode FROM station WHERE id='$to_id'");

		if(!$get_to_loc_poi){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			AlertError("Error while processing request !");
			exit();
		}

		if(numRows($get_to_loc_poi)==0)
		{
			AlertError("Destination location not found !");
			exit();
		}

		$row_to_poi = fetchArray($get_to_loc_poi);
		
		$lat_long = $row_to_poi['_lat'].",".$row_to_poi['_long'];
		echo "<option style='font-size:12px' value='$row_to_poi[_lat]_$row_to_poi[_long]_$row_to_poi[pincode]'>Location - $row_to_poi[name]</option>";
	}
}
else
{
	errorLog("Url not found--> $req_url.",$conn,$_SERVER['REQUEST_URI'],__LINE__);
	AlertError("POI type not defined !");
	exit();
}

$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => "https://apps2.locanix.net/RuningHourReport/api/Stoppages?VehicleName=$veh_no&FromDateTime=$start_date&ToDateTime=$end_date&Duration=$Duration1",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 900,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "GET",
CURLOPT_HTTPHEADER => array(
'Content-Type: text/plain'
),
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
		
if($err)
{
	AlertError("Connection failed with Locanix !");
	exit();
}
		
$response2 = json_decode($response, true);

if($response=='null' || $response=='')
{
}
else
{
	// $stop_array=array();
	if($response2['Stoppages']!='')
	{
		foreach($response2['Stoppages'] as $Data1)
		{
			$from_time = str_replace("+05:30","",str_replace("T"," ",$Data1['From']));
			$to_time = str_replace("+05:30","",str_replace("T"," ",$Data1['To']));
			$duration = $Data1['Duration'];
			$address = $Data1['Address']; 
			
			$zipcode = preg_match("/\b\d{6}\b/", $address, $matches);
			
			if(empty($matches[0])){
				$pincode = "";
				$pincode1 = "";
			}else {
				$pincode = ": ".$matches[0];
				$pincode1 = $matches[0];
			}
			
			$pincode1 = str_replace(" ","",$pincode1);
			$pincode1 = str_replace(".","",$pincode1);
			
			$lat = $Data1['Latitude'];
			$long = $Data1['Longitude'];
			$lat_long_pincode = $lat."_".$long."_".$pincode1; 
			$from_time_new = date("h:i A",strtotime($from_time));
			$to_time_new = date("h:i A",strtotime($to_time));
			// echo $from_time." to ".$to_time."<br>".$duration."<br>".$address."<br>".$lat."<br>".$long."<br>";
			echo "<option style='font-size:12px' value='$lat_long_pincode'>$address $pincode- $from_time_new to $to_time_new</option>";
		}
	}
}

if($req_url!='add_lr_to_trip')
{
	if($_POST['poi_type']=='loading')
	{
		echo "<script> 
			$('#$start_point_id').selectpicker('refresh');
			$('#loadicon').fadeOut('slow')
		</script>";
	}
	else
	{
		echo "<script> 
			$('#$end_point_id').selectpicker('refresh');
			$('#loadicon').fadeOut('slow')
		</script>";
	}
}

echo "<script>$('#loadicon').fadeOut('slow');</script>";
?>