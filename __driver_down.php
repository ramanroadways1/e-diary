<?php
require_once("./connect.php");
exit();
$tno = mysqli_real_escape_string($conn,strtoupper($_SESSION['diary']));
$down_type = mysqli_real_escape_string($conn,strtoupper($_POST['down_driver_type']));
$driver_code = mysqli_real_escape_string($conn,strtoupper($_POST['driver_code']));
$reason_left = mysqli_real_escape_string($conn,strtoupper($_POST['reason_left']));

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$trip_no_fetch=mysqli_query($conn,"SELECT trip_no FROM trip WHERE tno='$tno'");
if(mysqli_num_rows($trip_no_fetch)>0)
{
	echo "<script>
			alert('RUNNING TRIP FOUND FROM DRIVER. PLEASE CLEAR IT FIRST.');
			$('#down_driver_button').attr('disabled',true);
		</script>
		";
		exit();
}
		
$select_amount = mysqli_query($conn,"SELECT amount_hold FROM driver_up WHERE code='$driver_code' AND down=0 ORDER BY id DESC LIMIT 1");
if(mysqli_num_rows($select_amount)==0)
{
	echo "<script>
			alert('SOMETHING WENT WRONG WITH DRIVER DOWN. CONTACT HEAD-OFFICE.');
			$('#down_driver_button').attr('disabled',true);
		</script>
		";
		exit();
}
$row_amount = mysqli_fetch_array($select_amount);

$amount_hold = $row_amount['amount_hold'];

if($amount_hold>0)
{
	echo "<script>
			alert('Driver outstanding is $amount_hold. Please clear it first !!');
			$('#down_driver_button').attr('disabled',true);
		</script>
		";
		exit();
}

if($down_type=='STAND_BY')
{
	$driver_down_type=mysqli_query($conn,"INSERT INTO driver_standby(driver_code,branch,status,timestamp) VALUES 
	('$driver_code','$_SESSION[user]','1','$timestamp')");
	if(!$driver_down_type)
	{
		echo mysqli_error($conn);
		exit();
	}
	
	$status=0;
}
else if($down_type=='ON_LEAVE')
{
	$driver_down_type=mysqli_query($conn,"INSERT INTO driver_on_leave(driver_code,branch,status,timestamp) VALUES 
	('$driver_code','$_SESSION[user]','1','$timestamp')");
	if(!$driver_down_type)
	{
		echo mysqli_error($conn);
		exit();
	}
	
	$status=1;
}
else if($down_type=='LEFT')
{
	$driver_down_type=mysqli_query($conn,"INSERT INTO driver_left(driver_code,reason,branch,status,timestamp) VALUES 
	('$driver_code','$reason_left','$_SESSION[user]','1','$timestamp')");
	if(!$driver_down_type)
	{
		echo mysqli_error($conn);
		exit();
	}
	
	$status=2;
}
else
{
	echo "<script>
			alert('Unable to Get Details.');
			$('#down_driver_button').attr('disabled',true);
		</script>
		";
		exit();
}

$inset_driver_book=mysqli_query($conn,"INSERT INTO driver_book(driver_code,tno,desct,date,branch,timestamp) VALUES 
	('$driver_code','$tno','DRIVER_DOWN','$date','$_SESSION[user]','$timestamp')");
	if(!$inset_driver_book)
	{
		echo mysqli_error($conn);
		exit();
	}

$driver_down_qry=mysqli_query($conn,"UPDATE driver_up SET down='$date',status='$status',branch='$_SESSION[user]' WHERE code='$driver_code' 
AND down=0 ORDER BY id DESC LIMIT 1 ");
	if(!$driver_down_qry)
	{
		echo mysqli_error($conn);
		exit();
	}
	
$update_truck_active=mysqli_query($conn,"UPDATE own_truck SET driver_code='0' WHERE tno='$tno'");
if(!$update_truck_active)
	{
		echo mysqli_error($conn);
		exit();
	}
	
$update_driver_table=mysqli_query($conn,"UPDATE driver SET active='0' WHERE code='$driver_code'");
if(!$update_driver_table)
	{
		echo mysqli_error($conn);
		exit();
	}	
	
?>