<?php 
require_once './connect.php';

$lrno=escapeString($conn,strtoupper($_POST['lrno']));
$lr_type=escapeString($conn,($_POST['lr_type']));
$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s");
	
if($lrno=='' || empty($lrno))
{
	AlertError("Invalid LR number !!");
	echo "<script>$('#claim_lr_desc').hide();$('#claim_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
	exit();	
}

if($lr_type=='')
{
	AlertError("Select LR type !!");
	echo "<script>$('#claim_lr_desc').hide();$('#claim_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
	exit();	
}

if($lr_type=='MARKET')
{
	$chk_lr=Qry($conn,"SELECT tno,frmstn as from_loc,tostn as to_loc,done FROM mkt_bilty WHERE bilty_no='$lrno'");

	if(!$chk_lr){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertError("Error !!");
		echo "<script>$('#claim_lr_desc').hide();$('#claim_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();
	}
	
	if(numRows($chk_lr)==0)
	{
		AlertError("Error: LR not found !!");
		echo "<script>$('#claim_lr_desc').hide();$('#claim_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();	
	}
	
	$row_lr = fetchArray($chk_lr);
	
	$claim_main_id = "0";
}
else
{
	$chk_pod=Qry($conn,"SELECT id,balance,vou_no FROM claim_book_trans WHERE lrno='$lrno' AND vehicle_type='OWN' AND main_entry='1'");

	if(!$chk_pod){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertError("Error !!");
		echo "<script>$('#claim_lr_desc').hide();$('#claim_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();
	}
	
	if(numRows($chk_pod)==0)
	{
		AlertError("Claim not found for LR: $lrno !!");
		echo "<script>$('#claim_lr_desc').hide();$('#claim_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();	
	}
	
	$row_claim_data = fetchArray($chk_pod);
	
	$claim_balance = $row_claim_data['balance'];
	$claim_vou_no = $row_claim_data['vou_no'];
	$claim_main_id = $row_claim_data['id'];
	
	// if($row_chk_pod['claim_ho']=="0")
	// {
		// echo "<script>
			// alert('Error : POD approval pending. Contact head-office !');
			// $('#loadicon').hide();
			// $('#claim_lr_desc').hide();
			// $('#claim_check_btn').attr('disabled',false);
			// $('#naame_button').attr('disabled',true);
		// </script>";
		// exit();	
	// }

	
	// $chk_lr=Qry($conn,"SELECT id,lrno,amount,timestamp FROM claim_records_admin WHERE lrno='$lrno'");

	// if(!$chk_lr){
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// Redirect("Error while processing Request","./credit.php");
		// exit();
	// }
	
	// if(numRows($chk_lr)==0)
	// {
		// echo "<script>
			// alert('Error : No claim approved on this LR. Contact head-office !');
			// $('#loadicon').hide();
			// $('#claim_lr_desc').hide();
			// $('#claim_check_btn').attr('disabled',false);
			// $('#naame_button').attr('disabled',true);
		// </script>";
		// exit();	
	// }
	
	// $row2 = fetchArray($chk_lr);
	
	$chk_lr_loc=Qry($conn,"SELECT truck_no as tno,fstation as from_loc,tstation as to_loc FROM lr_sample WHERE lrno='$lrno'");

	if(!$chk_lr_loc){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertError("Error !!");
		echo "<script>$('#claim_lr_desc').hide();$('#claim_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();
	}
	
	if(numRows($chk_lr_loc)==0)
	{
		AlertError("Error: LR not found !!");
		echo "<script>$('#claim_lr_desc').hide();$('#claim_check_btn').attr('disabled',false);$('#naame_button').attr('disabled',true);</script>";
		exit();	
	}
	
	$row_lr = fetchArray($chk_lr_loc);
	
	// $chk_txn_entry = Qry($conn,"SELECT id FROM claim_book_trans WHERE lrno='$lrno'");

	// if(!$chk_txn_entry){
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// Redirect("Error while processing Request","./credit.php");
		// exit();
	// }
	
	// if(numRows($chk_txn_entry)==0)
	// {
		// $insert_new_txn = Qry($conn,"INSERT INTO claim_book_trans(lrno,branch,date,credit,balance,narration,timestamp) 
		// VALUES ('$row2[lrno]','HO','$row2[timestamp]','$row2[amount]','$row2[amount]','Claim on POD','$row2[timestamp]') ");

		// if(!$insert_new_txn){
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			// Redirect("Error while processing Request","./credit.php");
			// exit();
		// }
		
		// $txn_id = getInsertID($conn);
		
		// $update_txn_id = Qry($conn,"UPDATE claim_records_admin SET txn_book_id='$txn_id' WHERE id='$row2[id]'");

		// if(!$update_txn_id){
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			// Redirect("Error while processing Request","./credit.php");
			// exit();
		// }
	// }
}

$tno = $row_lr['tno'];
$from_loc = $row_lr['from_loc'];
$to_loc = $row_lr['to_loc'];

echo "<script>
	$('#loadicon').fadeOut('slow');
	// $('#claim_veh_no').html('>> Vehicle number: $tno');
	$('#claim_lr_desc').html('>> Trip: $from_loc to $to_loc & Vehicle no: $tno');
	$('.claim_lr_type_sel').attr('disabled',true);
	$('.naame_sel_opt').attr('disabled',true);
	// $('#tno_claim1').val('$tno');
	// $('#from_loc_claim1').val('$from_loc');
	// $('#to_loc_claim1').val('$to_loc');
	$('#naame_button').attr('disabled',false);
	$('#naame_sel_opt_claim').attr('disabled',false);
	$('#claim_lrno').attr('readonly',true);
	$('#claim_check_btn').attr('disabled',true);
	$('#claim_lr_desc').show();
</script>";
	
if($lr_type=='MARKET')
{
	echo "<script>
		$('#claim_lr_type_MARKET').attr('disabled',false);
	</script>";
}
else
{
	echo "<script>
		$('#claim_lr_type_OWN').attr('disabled',false);
	</script>";
}

// StartCommit($conn);
// $flag = true;
	
// $query = Qry($conn,"update user set `$balance_col`='$newbal' where username='$branch'");

// if(!$query){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

// $cashbook_entry = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,`$credit_col`,`$balance_col`,timestamp) VALUES 
// ('$branch','$_SESSION[user_code]','$date','$company','CREDIT ADD BALANCE','$nrr','$amount','$newbal','$timestamp')");

// if(!$cashbook_entry){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

// if($flag)
// {
	// MySQLCommit($conn);
	// closeConnection($conn);
	// echo "<script> 	
		// alert('Amount Credited Successfully !!');
		// window.location.href='./credit.php';
	// </script>";
	// exit();
// }
// else
// {
	// MySQLRollBack($conn);
	// closeConnection($conn);
	// Redirect("Error While Processing Request.","./credit.php");
	// exit();
// }
?>