<?php
require('./connect.php');
$date2 = date("Y-m-d"); 
?>

<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.btn * { visibility: hidden; }
.container-fluid * { visibility: visible}
}
</style>
<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>
</head>
<?php
$lrno=strtoupper(escapeString($conn,$_POST['bilty_no']));

$qry=Qry($conn,"SELECT company,date,lrdate,bilty_no,billing_type,veh_placer,plr,broker,billing_party,tno,frmstn,tostn,awt,cwt,rate,
tamt,branch FROM mkt_bilty WHERE bilty_no='$lrno'");

if($qry)
{
	if(numRows($qry)==0)
	{
		Redirect("No result found.","./");
		exit();
	}
	
	$row = fetchArray($qry);
	$lrdate = date('d/m/y', strtotime($row['lrdate']));
	$mbdate = date('d/m/y', strtotime($row['date']));
	$plr = $row['plr'];
	$tno = $row['tno'];
	$frm = $row['frmstn'];
	$to = $row['tostn'];
	$cwt = $row['cwt'];
	$rate = $row['rate'];
	$tamt = $row['tamt'];
	$company = $row['company'];
	$branch = $row['branch'];
	
	if($company=="RRPL")
	{
		$path1 = "../b5aY6EZzK52NA8F/rrpl.png";
	}
	else
	{
		$path1 = "../b5aY6EZzK52NA8F/rr.png";
	}

	echo "
	
<br />	
<div class='container-fluid' style='font-family:Verdana;font-size:13px;'>
<center><h3>Market - Bilty</h3></center>
<br />
<br />
<div class='col-md-8 col-md-offset-2'>
<table class='table table-bordered' style='font-size:13px;'>
<tr>
	<td colspan='2'>
	<img style='width:400px;height:90px' src='$path1' />
	<td>
	<b>Company :</b> $company
	<br>
	<br>
	<b>LR Date :</b> $lrdate
	</td>
	</tr>

	<tr>

		<td><b>Bilty No :</b> $lrno</td>

		<td><b>Sys Date :</b> $mbdate</td>

		<td><b>Truck No :</b> $tno</td>

	</tr>	

	
	<tr>

		<td><b>From Station :</b> $frm</td>

		<td><b>To Station :</b> $to</td>

		<td><b>Party LR No :</b> $plr</td>

	</tr>	

	<tr>
		<td><b>Actual Weight :</b> $row[awt]</td>	
		<td><b>Charge Weight :</b> $cwt</td>	
		<td><b>Rate :</b> $rate</td>
	</tr>	
	
	<tr>
		<td><b>Total Amount :</b> $tamt</td>	
		<td><b>Billing Type :</b> $row[billing_type]</td>
		<td><b>Vehicle Placer :</b> $row[veh_placer]</td>
	</tr>
	
	<tr>
		<td colspan='3'><b>Broker Name :</b> $row[broker]</td>
	</tr>
	
	<tr>
		<td colspan='3'><b>Billing PARTY :</b> $row[billing_party]</td>
	</tr>	

	</table>
</div>
</div>

<center>
	<button class='btn btn-danger' style='font-family:Verdana;letter-spacing:1px;color:#FFF;' onclick='print();'>Print Bilty</button>
	&nbsp; <button class='btn btn-primary' style='font-family:Verdana;letter-spacing:1px;color:#FFF;' onclick='window.close();'>Close</button>
</center>
<br />
<br />";	

}
?>