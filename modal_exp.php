<script type="text/javascript">
$(document).ready(function (e) {
$("#ExpForm").on('submit',(function(e) {
$("#loadicon").show();
$("#exp_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./exp_submit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form id="ExpForm" autocomplete="off" style="font-size:13px">       
<div class="modal fade" id="ExpenseModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
		
		 <div class="modal-header bg-primary">
			<span style="font-size:13px">Expense Entry <span style="font-size:13px">( गाड़ी / ड्राइवर खर्चा )</span> :</span>
		</div>
	<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-4">
				<label>Truck No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-4"> 
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input type="number" max="<?php if($branch=='CGROAD') { echo '20000'; } else { echo '9999'; } ?>" min="1" name="amount" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Date. <sup><font color="red">*</font></sup></label>
				<input name="exp_date" readonly type="date" value="<?php echo date("Y-m-d"); ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<input type="hidden" name="trip_id" class="trip_id_set" />
			<input type="hidden" name="trip_no" class="trip_no_set" />
			<input type="hidden" name="driver_name" class="driver_name_set" />
			<input type="hidden" name="exp_code" id="exp_code_function" />
			<input type="hidden" name="exp_name" id="exp_name_function" />
			<input type="hidden" name="driver_code" value="<?php echo $driver_code_value; ?>" />
			
			<script>
			function ExpenseSel(elem)
			{
				var code = elem.split("_")[0];
				var name = elem.split("_")[1];
				
				$('#exp_code_function').val(code);
				$('#exp_name_function').val(name);
				
				if(code=='TR00013')
				{
					$('#rto_upload').attr('required',true);
					$('#rto_upload_div').show();
				}
				else
				{
					$('#rto_upload').attr('required',false);
					$('#rto_upload_div').hide();
				}
			}
			</script>
			
			<div class="form-group col-md-12">
				<label>Select Expense. <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px" data-size="8" name="exp" onchange="ExpenseSel(this.value)" class="form-control selectpicker" data-live-search="true" required>
				<option style="font-size:12px" data-tokens="" value="">select an option</option>
		<?php
		if($is_dummy_driver=="1")
		{
			echo "<option style='font-size:12px' data-tokens='TR00012_REPAIRING' value='TR00012_REPAIRING'>TRUCK REPAIRING & MAINT.</option>";
			
		}
		else
		{
			// $inc_exp=Qry($conn,"SELECT id FROM dairy.inc_veh WHERE tno='$tno'");
			
			// if(!$inc_exp){
				// echo getMySQLError($conn);
				// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				// exit();
			// }			

				// if(numRows($inc_exp)>0)
				// {
					// echo "<option style='font-size:12px' data-tokens='TR00543_INCENTIVE' value='TR00543_INCENTIVE'>INCENTIVE</option>";
				// }
				
				// if($branch=='CGROAD')
				// {
					// $exp_master = Qry($conn,"SELECT exp_code,name FROM dairy.exp_head WHERE role='4' ORDER BY name ASC");
					
					// if(!$exp_master){
						// echo getMySQLError($conn);
						// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						// exit();
					// }
			
					// if(numRows($exp_master)>0)
					// {
						// while($row_exp_master=fetchArray($exp_master))
						// {
							// echo "<option style='font-size:12px' data-tokens='$row_exp_master[exp_code]_$row_exp_master[name]' value='$row_exp_master[exp_code]_$row_exp_master[name]'>$row_exp_master[name]</option>";
						// }	
					// }
				// }
				
				$fetch_exp=Qry($conn,"SELECT exp_code,name FROM dairy.exp_head WHERE visible_to_branch='1' ORDER BY name ASC");
				
				if(!$fetch_exp){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						exit();
					}
					
				if(numRows($fetch_exp)>0)
				{
					while($row_exp=fetchArray($fetch_exp))
					{
						?>
						<option style="font-size:12px" <?php if($trishul_card=="1" AND $row_exp['exp_code']=="TR00877"){ echo "disabled"; } ?> data-tokens='<?php echo $row_exp["exp_code"]."_".$row_exp["name"]; ?>' 
						value='<?php echo $row_exp["exp_code"]."_".$row_exp["name"]; ?>'><?php echo $row_exp["name"]; ?></option>
						<?php
					}					
				}
		}
				?>
				</select>
			</div>
			
			<div id="rto_upload_div" style="display:none" class="form-group col-md-12">
				<label>Upload RTO Receipt. <sup><font color="red">* (multiple upload support)</font></sup></label>
				<input type="file" name="rto_upload[]" accept=".pdf,.jpg,.jpeg,.png" multiple="multiple" required style="padding-bottom:28px;" id="rto_upload" class="form-control" />
			</div>
			
			<div class="form-group col-md-12">
				<label>Narration. <sup><font color="red">*</font></sup></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,/]/,'')" class="form-control" name="narration" required></textarea>
			</div>
		</div>
        </div>
        <div class="modal-footer">
          <button <?php if($driver_code_value=="") {echo "disabled"; } ?> type="submit" id="exp_button" class="btn btn-sm btn-danger">Submit</button>
          <button type="button" class="btn btn-sm btn-primary" id="hide_exp" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>