<?php
require("./connect.php");

$distance = escapeString($conn,($_POST['distance']));
$avg_value = escapeString($conn,($_POST['avg_value']));

if(isset($_POST['type']))
{ 
	if($_POST['type']=='create_new_trip')
	{
		$_SESSION['create_trip_km'] = $distance;
		$_SESSION['create_trip_diesel_required'] = sprintf("%.2f",($distance/$avg_value));
		echo "<script>$('#create_trip_dsl_qty_required').val('".$_SESSION['create_trip_diesel_required']."')</script>";
	}
	else // add lr to at-loading trip
	{
		$_SESSION['atloading_trip_km'] = $distance;
		$_SESSION['atloading_trip_diesel_required'] = sprintf("%.2f",($distance/$avg_value));
		echo "<script>$('#atloading_lr_dsl_qty_required').val('".$_SESSION['atloading_trip_diesel_required']."')</script>";
	}
}
else
{
	$trip_id = escapeString($conn,($_POST['trip_id']));
	$to_id = escapeString($conn,($_POST['to_id']));
	$_SESSION['add_more_trip_km'] = $distance;
	
	$get_loc = Qry($conn,"SELECT to_id,km,dsl_consumption FROM dairy.trip WHERE id='$trip_id'");
	
	if(!$get_loc){
		AlertError('Error..');
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script type='text/javascript'>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}

	$row_loc = fetchArray($get_loc);
	
	if($row_loc['to_id']!=$to_id)
	{
		if($distance<$row_loc['km'])
		{
			AlertError('Invalid destination selected !<br>');
			echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#to_loc_for_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
			exit();
		}
		
		$_SESSION['add_more_lr_diesel_required'] = sprintf("%.2f",($distance/$avg_value));
	}
	else
	{
		if($row_loc['dsl_consumption']==0)
		{
			$_SESSION['add_more_lr_diesel_required'] = sprintf("%.2f",($distance/$avg_value));
		}
		else
		{
			$_SESSION['add_more_lr_diesel_required'] = $row_loc['dsl_consumption'];
		}
	}
	
	echo "<script>$('#add_more_lr_dsl_qty_required').val('".$_SESSION['add_more_lr_diesel_required']."')</script>";
}
?>