<?php
require('./connect.php');

$tno=escapeString($conn,strtoupper($_SESSION['diary']));

$type=escapeString($conn,strtoupper($_POST['type']));
$trip_id=escapeString($conn,strtoupper($_POST['trip_id']));

if($trip_id=='')
{
	echo "<script>
			alert('Trip not found. !');
			window.location.href='./';
		</script>";
	exit();
} 

$frno=escapeString($conn,strtoupper($_POST['frno']));

$chk1 = Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE tno='$tno' AND lr_type LIKE '%$frno%'");
		if(numRows($chk1)>0)
		{
			$rowChk1 = fetchArray($chk1);
			
			echo "<script>
				alert('This LR already attached with Trip : $rowChk1[from_station] to $rowChk1[to_station] !');
				window.location.href='./';
			</script>";
			exit();
		}
		
$fetch_old_trip_data = Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE tno='$tno' AND id<'$trip_id' ORDER BY id DESC LIMIT 1");
if(!$fetch_old_trip_data)	
{
	Redirect("Error while processing Request.","./");
	exit();
}

$fetch_next_trip_data = Qry($conn,"SELECT from_station FROM dairy.trip WHERE tno='$tno' AND id>'$trip_id' ORDER BY id ASC LIMIT 1");
if(!$fetch_next_trip_data)	
{
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($fetch_next_trip_data)>0)
{
	
$row_next_trip = fetchArray($fetch_next_trip_data);

if($row_next_trip['from_station']!='' AND $row_next_trip['from_station']!='NA')
{
	$from_set = "1";
	$from_set_loc = $row_next_trip['from_station'];
}
else
{
	$from_set = "0";
	$from_set_loc = "";
}
}
else
{
	$from_set = "0";
	$from_set_loc = "";
}

if(numRows($fetch_old_trip_data)>0)
{
$row_old_trip = fetchArray($fetch_old_trip_data);

if($row_old_trip['from_station']=='' || $row_old_trip['from_station']=='NA' || $row_old_trip['to_station']=='' || $row_old_trip['to_station']=='NA')
{
	Redirect("Warning : ADD LR in Previous Trip First !","./");
	exit();
}

	$to_set = "1";
	$to_set_loc = $row_old_trip['to_station'];
}
else
{
	$to_set = "0";
	$to_set_loc = "";
}

$fetch_trip_data = 	Qry($conn,"SELECT from_station,to_station FROM dairy.trip WHERE id='$trip_id'");	
if(!$fetch_trip_data)	
{
	Redirect("Error while processing Request.","./");
	exit();
}

$row_this_trip = fetchArray($fetch_trip_data);
		
if($type=='OWN')
{
	$frno=escapeString($conn,strtoupper($_POST['frno']));
	
	if($frno=="ATLOADING")
	{
		$at_loading="1";
	}
	else
	{
		$fetch=Qry($conn,"SELECT fstation,tstation,lrno FROM freight_form_lr WHERE frno='$frno'");
		$fetch2=Qry($conn,"SELECT SUM(wt12) as act,SUM(weight) as charge FROM freight_form_lr WHERE frno='$frno'");
		
		$lrnos = array();
		while($row=fetchArray($fetch))
		{
			$lrnos[] = $row['lrno'];
			$from=$row['fstation'];
			$to=$row['tstation'];
		}
		
		$row2=fetchArray($fetch2);
		
		$lrnos = implode(',',$lrnos);
		$act=sprintf('%0.2f',$row2['act']);
		$charge=sprintf('%0.2f',$row2['charge']);
		$from=@$from;
		$to=@$to;
		$billing_type='';
		$freight=0;
		
		$at_loading="0";
	}
}
else if($type=='MKT')
{
	$mbno=escapeString($conn,strtoupper($_POST['frno']));
	
	if($mbno=="ATLOADING")
	{
		$at_loading="2";
	}
	else
	{
		$qq=Qry($conn,"SELECT billing_type,frmstn,tostn,cwt,tamt FROM mkt_bilty WHERE bilty_no='$mbno'");
		$row=fetchArray($qq);
		
		$from=$row['frmstn'];
		$to=$row['tostn'];
		$billing_type=$row['billing_type'];
		$freight=$row['tamt'];
		$act=sprintf('%0.2f',$row['cwt']);
		$charge=sprintf('%0.2f',$row['cwt']);
		$lrnos=$mbno;
		
		$at_loading="0";
	}
}

if($row_this_trip['from_station']!='NA' AND $row_this_trip['from_station']!='')
{
	if($row_this_trip['from_station']!=$from)
	{
		echo "<script>
			alert('Warning : Location not matching. Current trip\'s from location is : $row_this_trip[from_station] and LR\'s From Location is : $from.');
			window.location.href='./';
		</script>";
		exit();
	}
}

if($to_set=='1')
{
	if($to_set_loc!=$from)
	{
		echo "<script>
			alert('Warning : Location not matching. Last trip\'s to location is : $to_set_loc and LR\'s From Location is : $from.');
			window.location.href='./';
		</script>";
		exit();
	}
}

if($from_set=='1')
{
	if($from_set_loc!=$to)
	{
		echo "<script>
			alert('Warning : Location not matching. Next trip\'s From location is : $from_set_loc and LR\'s To Location is : $to.');
			window.location.href='./';
		</script>";
		exit();
	}
}

if($at_loading>0)
{
	$fetch_last_trip=Qry($conn,"SELECT to_station FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1");
	if(numRows($fetch_last_trip)==0)
	{
		$fetch_last_trip_from_hisab=Qry($conn,"SELECT to_station FROM dairy.trip_final WHERE tno='$tno' ORDER BY id DESC LIMIT 1");
		if(numRows($fetch_last_trip_from_hisab)==0)
		{
			$last_station="";
			echo "
			<script>
				$('#from_stn').val('');
				$('#from_stn').attr('readonly',false);
			</script>";	
		}
		else
		{
			$row_final_trip=fetchArray($fetch_last_trip_from_hisab);
			$last_station=$row_final_trip['to_station'];
			echo "
			<script>
				$('#from_stn').val('$last_station');
				$('#from_stn').attr('readonly',true);
			</script>";	
		}
	}
	else
	{
		$row_trip=fetchArray($fetch_last_trip);
		$last_station=$row_trip['to_station'];
			
			echo "
			<script>
				$('#from_stn').val('$last_station');
				$('#from_stn').attr('readonly',true);
			</script>";	
	}
	
	echo "
	<script>
		$('#to_stn').val('');
		$('#actual').val('0');
		$('#charge').val('0');
		$('#lrno').val('ATLOADING');
		$('#billing_type_text').val('');
		$('#freight_bilty_text').val('0');
		
		$('#to_stn').attr('readonly',false);
		$('#actual').attr('readonly',true);
		$('#charge').attr('readonly',true);
		$('#lrno').attr('readonly',true);
		
		$('#at_loading_set').val('$at_loading');
		
	</script>
	";
}
else
{
	echo "
	<script>
		$('#from_stn_no_lr').val('$from');
		$('#to_stn_no_lr').val('$to');
		$('#actual_no_lr').val('$act');
		$('#charge_no_lr').val('$charge');
		$('#lrno_no_lr').val('$lrnos');
		$('#billing_type_text_no_lr').val('$billing_type');
		$('#freight_bilty_text_no_lr').val('$freight');
		
		$('#from_stn_no_lr').attr('readonly',true);
		$('#to_stn_no_lr').attr('readonly',true);
		$('#actual_no_lr').attr('readonly',true);
		$('#charge_no_lr').attr('readonly',true);
		$('#lrno_no_lr').attr('readonly',true);
		
		$('#at_loading_set_no_lr').val('0');
	</script>
	";
}	
?>