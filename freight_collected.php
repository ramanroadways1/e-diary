<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 
$date = date("Y-m-d");

$tno = escapeString($conn,strtoupper($_POST['tno']));

if($tno!=$_SESSION['diary'])
{
	Redirect("Please log in again..","./logout.php");
	exit();
}

require_once("./check_cache.php");

$get_company = Qry($conn,"SELECT comp FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_company){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_company)>0)
{
	$row_comp = fetchArray($get_company);
	$company=$row_comp['comp'];
}
else
{
	AlertError("Company not found..");
	errorLog("Company not found.",$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

if($company!='RRPL' AND $company!='RAMAN_ROADWAYS')
{
	AlertError("Company not found..");
	errorLog("Company not found2.",$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

$trip_id = escapeString($conn,strtoupper($_POST['trip_id']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertError("Trip not found..");
	errorLog("Trip id not found. id: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

$check_trip = Qry($conn,"SELECT t.id,t.tno,t.driver_code,t.trip_no,d.name as driver_name 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
WHERE t.id='$trip_id'");

if(!$check_trip){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertError("Trip not found..");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$driver_name = $row_trip['driver_name'];
$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];
$amount = escapeString($conn,$_POST['amount']);
$narration = escapeString($conn,($_POST['narration']));
$lrno = escapeString($conn,strtoupper($_POST['lr_no']));

if(substr($lrno,3,1)=='M')
{
	$chk_balance = Qry($conn,"SELECT id FROM dairy.bilty_balance WHERE bilty_no='$lrno'");
	
	if(!$chk_balance){
		AlertError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#freight2_button').attr('disabled',false);</script>";
		exit();
	}

	if(numRows($chk_balance)==0)
	{
		AlertError("LR not found..");
		errorLog("Market Bilty: $lrno not found.",$conn,$page_name,__LINE__);
		echo "<script>$('#freight2_button').attr('disabled',false);</script>";
		exit();
	}
	
	$row_bilty_bal = fetchArray($chk_balance);
	$bilty_balance_id = $row_bilty_bal['id'];
	$bilty_type="MARKET";
	
	$fetch_lr_date = Qry($conn,"SELECT lrdate as lrdate,frmstn as from_loc,tostn as to_loc FROM mkt_bilty WHERE 
	bilty_no='$lrno'");
	
}
else if(substr($lrno,3,3)=='OLR')
{
	$bilty_type="OWN";
	
	$fetch_lr_date=Qry($conn,"SELECT date as lrdate,fstation as from_loc,tstation as to_loc FROM freight_form_lr WHERE 
	frno='$lrno'");
}
else
{
	AlertError("LR not found..");
	errorLog("Invalid bilty: $lrno found.",$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

if(!$fetch_lr_date){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($fetch_lr_date)==0)
{
	AlertError("LR not found..");
	errorLog("LR: $lrno not found..",$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

$row_lr = fetchArray($fetch_lr_date);

$lr_date = $row_lr['lrdate'];
$from_loc = $row_lr['from_loc'];
$to_loc = $row_lr['to_loc'];

if($company=='RRPL')
{
	$credit='credit';			
	$balance='balance';
	
}
else if($company=='RAMAN_ROADWAYS')
{
	$credit='credit2';			
	$balance='balance2';
}

$nrr = $tno."/".$driver_name." : ".$narration;

$trans_id_Qry = GetTxnId_eDiary($conn,"FRTCOL");
	
if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
{
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}
	
$trans_id = $trans_id_Qry;
	
$cash_balance = GetBranchCash($company,$conn,$branch);
	
if(!$cash_balance || $cash_balance=="")
{
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}
	
$newbal = $cash_balance + $amount;
	
$cashbook_narr = "Truck No: $tno, Date: $date, TripNo-$trip_no, Bilty No: $lrno";
	
StartCommit($conn);
$flag = true;
	
$insert_cash = Qry($conn,"INSERT INTO cashbook(user,user_code,date,vou_date,comp,vou_no,vou_type,desct,`$credit`,`$balance`,timestamp) 
VALUES ('$branch','$_SESSION[user_code]','$date','$date','$company','$trans_id','CREDIT-FREIGHT','$cashbook_narr','$amount','$newbal',
'$timestamp')");

if(!$insert_cash){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$last_id_cashbook = getInsertID($conn);

$cash_balance_update = Qry($conn,"update user set `$balance`='$newbal' where username='$branch'");

if(!$cash_balance_update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$insert_freight_diary = Qry($conn,"INSERT INTO dairy.freight_rcvd(trip_id,trans_id,vou_id,tno,amount,date,narration,branch,branch_user,
timestamp) VALUES ('$trip_id','$trans_id','$lrno','$tno','$amount','$date','$lrno : $narration','$branch','$_SESSION[user_code]',
'$timestamp')");

if(!$insert_freight_diary){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_trip = Qry($conn,"UPDATE dairy.trip SET freight_collected=freight_collected+'$amount' WHERE id='$trip_id'");

if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$select_amount = Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($select_amount)==0){
	$flag = false;
	errorLog("Error While Fetching Driver Balance. Code: $driver_code.",$conn,$page_name,__LINE__);
}

$row_amount=fetchArray($select_amount);
$hold_amt=$row_amount['amount_hold']-$amount;
$driver_id=$row_amount['id'];

$insert_book = Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,debit,balance,date,branch,timestamp) 
VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','FREIGHT_RECEIVED','$amount','$hold_amt','$date','$branch','$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$last_id_driver_book = getInsertID($conn);

$qry_cr = Qry($conn,"INSERT INTO credit(credit_by,cash_id,diary_id,section,tno,lr_date,bilty_type,ediary_credit,trans_id,
from_stn,to_stn,branch,branch_user,company,bilty_no,amount,narr,date,timestamp) VALUES 
('CASH','$last_id_cashbook','$last_id_driver_book','FREIGHT','$tno','$lr_date','$bilty_type','1','$trans_id','$from_loc','$to_loc',
'$branch','$_SESSION[user_code]','$company','$lrno','$amount','$nrr','$date','$timestamp')");		

if(!$qry_cr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-$amount WHERE id='$driver_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request..");
	echo "<script>$('#freight2_button').attr('disabled',false);</script>";
	exit();
}

echo "<script>
		$('#freight2_button').attr('disabled',false);
		$('#Freight2Form')[0].reset();
		document.getElementById('hide_freight2').click();
		TripAdv('#$trip_id');
		LoadDriverBalance('$driver_code');
		$('#loadicon').fadeOut('slow');	
		
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: 'Success ! Freight credited.',
			showConfirmButton: false,
			timer: 2000
		})
	</script>";
exit();
?>