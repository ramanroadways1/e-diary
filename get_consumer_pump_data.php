<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$code=escapeString($conn,strtoupper($_POST['code']));

$getConsumerPump = Qry($conn,"SELECT consumer_pump FROM dairy.diesel_pump_own WHERE code='$code'");
if(!$getConsumerPump){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		window.location.href='./';
	</script>";
	exit();
}

$rowComp = fetchArray($getConsumerPump);

if(isset($_POST['pending_diesel_page']))
{
	echo "<script>
			$('#rate_fix_diesel').val('');
			$('#dsl_amt_fix').val('');
			$('#fix_diesel_qty').val('');
		</script>";
		
	if($rowComp['consumer_pump']=="1")
	{
		echo "<script>
			$('#dsl_amt_fix').attr('oninput','');
			$('#rate_fix_diesel').attr('readonly',true);
			$('#dsl_amt_fix').attr('readonly',true);
			$('#loadicon').hide();
		</script>";
	}
	else{
		echo "<script>
			$('#dsl_amt_fix').attr('oninput','AmountCallFix()');
			$('#rate_fix_diesel').attr('readonly',false);
			$('#dsl_amt_fix').attr('readonly',false);
			$('#loadicon').hide();
		</script>";
	}
} 
else
{
	if($rowComp['consumer_pump']=="1")
	{
		echo "<script>
			$('#rate').attr('oninput','');
			$('#dsl_amt').attr('oninput','');
			$('#rate').attr('readonly',true);
			$('#dsl_amt').attr('readonly',true);
			$('#loadicon').hide();
		</script>";
	}
	else{
		echo "<script>
			$('#rate').attr('oninput','sum1()');
			$('#dsl_amt').attr('oninput','sum3()');
			$('#rate').attr('readonly',false);
			$('#dsl_amt').attr('readonly',false);
			$('#loadicon').hide();
		</script>";
	}
}
?>