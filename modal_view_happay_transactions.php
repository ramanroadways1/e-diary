<div id="happay_trans_modal" class="modal fade" role="dialog" style="background:#299C9B" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Happay Transactions : <?php echo $tno; ?>
      </div>

<script>	
// function WithdrawAmount(amount)
// {
	// var amount = $('#amount_wdl').val();
	
	// if(amount!=0 && amount!='' && amount>0)
	// {
		// $('#WithdrawAmountButton').attr('disabled',true);
		// $("#loadicon").show();
			// jQuery.ajax({
				// url: "happay/withdraw_wallet.php",
				// data: 'tno=' + '<?php echo $tno; ?>' + '&amount=' + amount,
				// type: "POST",
				// success: function(data) {
				// $("#result_wdl_amount").html(data);
				// },
			// error: function() {}
		// });
	// }
	// else
	// {
		// alert('Please enter valid amount value !');
	// }
// }
</script>
	<div class="modal-body">
	  <div class="row">
			<!--
			<div class="form-group col-md-12">
				<label>Withdraw Wallet Amount. <font color="red">Current Balance</font> : <span id="view_current_bal"></span> </label>
				<div class="form-inline">
					<input type="number" class="form-control" style="font-size:12px;height:30px;" id="amount_wdl">
					<button disabled type="button" id="WithdrawAmountButton" onclick="WithdrawAmount()" 
					class="btn btn-primary btn-md"><span style="font-size:px;" class="glyphicon glyphicon-share-alt"></span></button>
				</div>
			</div>
			-->
			<div class="form-group col-md-12 table-responsive">
			
<?php
$get_Card_using = Qry($conn,"SELECT card_using FROM dairy.happay_card WHERE tno='$tno'");
if(!$get_Card_using){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

$row_card_using = fetchArray($get_Card_using);

$card_kit_no = $row_card_using['card_using'];

$happay_Trans_View = Qry($conn,"SELECT merchant,trans_date,trans_type,credit,debit FROM dairy.happay_webhook_live 
WHERE date(trans_date) BETWEEN '$tripStart_date1' AND '".date("Y-m-d")."' AND card_no='$card_kit_no'");
				
if(!$happay_Trans_View){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

	if(numRows($happay_Trans_View)>0)
	{
		if(isMobile()){
			echo "<table class='table table-bordered table-striped' style='color:#000;font-size:9px;'>
			<tr style='font-size:10px;'>
			";
		}else{
			echo "<table class='table table-bordered table-striped' style='color:#000;font-size:12px;'>
			<tr style='font-size:13px;'>
			";
		}
				
		echo "
							<th>#</th>
							<th>Merchant</th>
							<th>Txn date</th>
							<th>Trans type</th>
							<th>C/D</th>
							<th>Amount</th>
						</tr>";
				$srno_Happay=1;
				while($row_TransHappay = fetchArray($happay_Trans_View))
				{
					if($row_TransHappay['credit']>0)
					{
						$indicator="<font color='green'>C</font>";
						$amount_trans_Happay=$row_TransHappay['credit'];
					}
					else
					{
						$indicator="<font color='red'>D</font>";
						$amount_trans_Happay=$row_TransHappay['debit'];
					}
					echo "<tr>
						<td>$srno_Happay</td>
						<td>$row_TransHappay[merchant]</td>
						<td>".date("d-m-y H:i A",strtotime($row_TransHappay["trans_date"]))."</td>
						<td>$row_TransHappay[trans_type]</td>
						<td>$indicator</td>
						<td>$amount_trans_Happay</td>
					</tr>";
				$srno_Happay++;
				}	
				echo "</table>";	
				}
				else
				{
					echo "<center><br><b><font color='red'>No Transactions found.</b></font>";
				}
				?>
			</div>
      </div>
      </div>
	  
	  <div id="result_wdl_amount"></div>
	  
	 <div class="modal-footer">
        <button type="button" style="color:#FFF;" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>