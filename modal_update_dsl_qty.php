<script type="text/javascript">
$(document).ready(function (e) {
$("#QtyUpdateForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_qty_update").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_update_qty.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_modal_qty_update").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form style="font-size:13px" id="QtyUpdateForm" autocomplete="off">  
<div class="modal fade" id="QtyUpdateModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
		<div class="modal-header bg-primary">
			<span style="font-size:15px">Update fuel details :</span>
		</div>
	<div class="modal-body">
		<div class="row">
		
			<div style="color:red" class="form-group col-md-12">
				Note: दर्ज की गयी मात्रा सटीक होनी चाहिए !
				<br />
				किसी प्रकार की सहायता के लिए गाड़ी के सुपरवाइजर या 9879200311 पर संपर्क करे !
			</div>
			
			<div class="form-group col-md-12">
				<label>Fuel Tank Capacity <span style="font-size:11px">फ्यूल टैंक क्षमता</span><sup><font color="red"> * (in LTR)</font></sup></label>
				<input style="font-size:13px" type="number" min="100" name="tank_cap" max="500" class="form-control" required />
			</div>
			
			<div class="form-group col-md-12">
				<label>Qty available in tank <span style="font-size:11px">टैंक में उपलब्ध मात्रा दर्ज करे </span><font color="red"> <sup>* (in LTR)</sup></font></label>
				<input style="font-size:13px" class="form-control" min="10" type="number" name="diesel_left" required />
			</div>
			
			<div id="result_modal_qty_update"></div>
			
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="submit_qty_update" class="btn btn-sm btn-danger">Update</button>
          <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
</form>