<?php
require_once("./connect.php");
require_once("./check_cache.php");

$timestamp = date("Y-m-d H:i:s"); 
$tno=escapeString($conn,strtoupper($_POST['tno']));
$diesel_key = $tno."_".mt_rand().date("His");

if($tno!=$_SESSION['diary'])
{
	Redirect("Invalid Session.","./logout.php");
	exit();
}

$amount=escapeString($conn,$_POST['amount']);
$rate=escapeString($conn,$_POST['rate']);
$qty=escapeString($conn,$_POST['qty']);

if($amount!=round($qty*$rate))
{
	echo "<script type='text/javascript'>
		alert('Error : Check Qty/Rate/Amount.');
		$('#diesel_adv_button_pending').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($qty<=0)
{
	echo "<script type='text/javascript'>
			alert('Error : Invalid Qty.');
			$('#diesel_adv_button_pending').attr('disabled', false);
			$('#loadicon').hide();
	</script>";
	exit();
}

$date=date("Y-m-d");
// $date=escapeString($conn,strtoupper($_POST['diesel_date']));
$trip_id=escapeString($conn,strtoupper($_POST['trip_id']));
$bill_no=escapeString($conn,strtoupper($_POST['bill_no']));

$driver_code=escapeString($conn,strtoupper($_POST['driver_code']));
$trip_no=escapeString($conn,strtoupper($_POST['trip_no']));
$driver_name=escapeString($conn,strtoupper($_POST['driver_name']));

$random_key=date("ymdhis").mt_rand();

if($trip_id=='' || $amount=='' || $trip_no=='' || $driver_name=='' || $driver_code=='')
{
	errorLog("Fields are empty.",$conn,$page_name,__LINE__);
	Redirect("Empty inputs found.","./");
	exit();
}

$check_trip_lr = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$check_trip_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Trip not Found.","./");
	exit();
}

if(numRows($check_trip_lr)==0)
{
	echo "<script>
		alert('Trip not found..');
		$('#diesel_adv_button_pending').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$check_fix_diesel = Qry($conn,"SELECT qty FROM dairy._pending_fix_diesel WHERE trip_id='$trip_id'");

if(!$check_fix_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Trip not Found.","./");
	exit();
}

if(numRows($check_fix_diesel)>0)
{
	$row_fix_qty = fetchArray($check_fix_diesel);
	
	if($qty>$row_fix_qty['qty'])
	{
		echo "<script>
			alert('Max diesel QTY allowed is: $row_fix_qty[qty] !!');
			$('#diesel_adv_button_pending').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

$GetDriverMobile = Qry($conn,"SELECT mobile,mobile2 FROM dairy.driver WHERE code='$driver_code'");
if(!$GetDriverMobile){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}

	if(numRows($GetDriverMobile)==0){
		errorLog("Driver not found. Code: $driver_code.",$conn,$page_name,__LINE__);
		Redirect("Driver not found.","./");
		exit();
	}
	
$row_Driver = fetchArray($GetDriverMobile);

$rilpre = escapeString($conn,strtoupper($_POST['rilpre']));
$pump_card=escapeString($conn,strtoupper($_POST['pump_card']));

if($pump_card=='CARD')
{
	$card=escapeString($conn,strtoupper($_POST['card_no']));
	$type='CARD';
	$phy_card_no=escapeString($conn,strtoupper($_POST['phy_card_no']));

	if($card=='')
	{
		errorLog("Card is Empty.",$conn,$page_name,__LINE__);
		Redirect("Card is Empty.","./");
		exit();
	}

	$fuel_company=escapeString($conn,strtoupper($_POST['fuel_company']));
	$mobile_no="";

	$msg_text="कार्ड";
	$consumer_pump = "0";
	$total_qty = $qty;
	$total_amount = $amount;
}
else if($pump_card=='OTP')
{
	$card_id="0";
	$card=escapeString($conn,strtoupper($_POST['mobile_no']));
	$mobile_no=escapeString($conn,strtoupper($_POST['mobile_no']));
	$phy_card_no=$mobile_no;
	$type="OTP";
	$fuel_company=escapeString($conn,strtoupper($_POST['fuel_company']));
	$msg_text="OTP";
	$consumer_pump = "0";
	$total_qty = $qty;
	$total_amount = $amount;
}
else
{
	$consumer_pump = "0";	
	$mobile_no="";	
	$card_id="0";
	$card=escapeString($conn,strtoupper($_POST['pump_name']));	
	$card=strtok($card,'-');
	$phy_card_no=$card;
	$type='PUMP';
	$fuel_company=escapeString($conn,strtoupper($_POST['pump_company']));

	$total_qty = $qty;
	$total_amount = $amount;

$check_consumer = Qry($conn,"SELECT id FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no'");
if(!$check_consumer){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_consumer)>0)
{
	$check_stock = Qry($conn,"SELECT SUM(balance) as balance FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no'");
	if(!$check_stock){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$row_balance = fetchArray($check_stock);
	
	if($row_balance['balance']<$qty)
	{
		echo "<script type='text/javascript'>
				alert('Error : Diesel not in Stock.');
				$('#insert_diesel_button').attr('disabled', false);
				$('#loadicon').hide();
		</script>";
		exit();
	}
}

$get_qty = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no' AND balance>0");
if(!$get_qty){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

$numRowsQty = numRows($get_qty);

// if($numRowsQty==0)
// {
	// echo "<script type='text/javascript'>
			// alert('Error : Diesel not in stock.');
			// $('#diesel_adv_button_pending').attr('disabled', false);
			// $('#loadicon').hide();
		// </script>";
	// exit();
// }
// else
if($numRowsQty==1)
{
	$row_Qty = fetchArray($get_qty);
	if($row_Qty['balance']<$qty)
	{
		echo "<script type='text/javascript'>
			alert('Diesel not in Stock. Stock : 01');
			$('#diesel_adv_button_pending').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$remainQty = sprintf("%.2f",$row_Qty['balance']-$qty);
	$StockId = $row_Qty['id'];
	$PurchaseId = $row_Qty['purchaseid'];
	
	$rate = $row_Qty['rate'];
	$amount = round($qty*$rate);
	$consumer_pump = "1";
	
	$total_qty = $qty;
	$total_amount = $amount;
}
else if($numRowsQty==2)
{
	$consumer_pump = "1";
	
	$get_first_pump = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no' AND balance>0 
	ORDER BY id ASC LIMIT 1");
	
	if(!$get_first_pump){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}

	$get_second_pump = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$phy_card_no' AND balance>0 
	ORDER BY id DESC LIMIT 1");
	
	if(!$get_second_pump){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

	$rowFirstPump = fetchArray($get_first_pump);
	$rowSecondPump = fetchArray($get_second_pump);
	
	if(($rowFirstPump['balance']+$rowSecondPump['balance'])<$qty)
	{
		echo "<script type='text/javascript'>
			alert('Diesel not in Stock. Stock : 02');
			$('#diesel_adv_button_pending').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	if($rowFirstPump['balance']>=$qty)
	{
		$split_entry="0";
		$rate = $rowFirstPump['rate'];
		$amount = round($qty*$rate);
		$remainQty = sprintf("%.2f",$rowFirstPump['balance']-$qty);
		$StockId = $rowFirstPump['id'];
		$PurchaseId = $rowFirstPump['purchaseid'];
		
		$total_qty = $qty;
		$total_amount = $amount;
	}
	else
	{
		$split_entry="1";
		
		$rate = $rowFirstPump['rate'];
		$rate2 = $rowSecondPump['rate'];
		$qty1 = $rowFirstPump['balance'];
		$qty2 = sprintf("%.2f",$qty-$qty1);
		$amount = round($rate * $qty1);
		$amount2 = round($rate2 * $qty2);
		$StockId = $rowFirstPump['id'];
		$StockId2 = $rowSecondPump['id'];
		$remainQty = 0;
		$remainQty2 = sprintf("%.2f",$rowSecondPump['balance']-$qty2);
		$PurchaseId = $rowFirstPump['purchaseid'];
		$PurchaseId2 = $rowSecondPump['purchaseid'];
		
		$total_qty = $qty1+$qty2;
		$total_amount = $amount+$amount2;
	}
	
}
else if($numRowsQty>2)
{
	echo "<script>
		alert('Multiple Records found of fuel station.');
		window.location.href='./';
	</script>";
	exit();
}

	$GetPumpName = Qry($conn,"SELECT name FROM dairy.diesel_pump_own WHERE code='$phy_card_no'");
	if(!$GetPumpName){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}

	if(numRows($GetPumpName)==0){
		errorLog("PUMP not found. Code: $phy_card_no.",$conn,$page_name,__LINE__);
		Redirect("PUMP not found.","./");
		exit();
	}
	
	$rowPumpName = fetchArray($GetPumpName);

$msg_text=$rowPumpName['name'];	
}

if($rilpre=="1" AND $pump_card=='CARD')
{
	$check_stock = Qry($conn,"SELECT stockid,balance FROM diesel_api.dsl_ril_stock WHERE cardno='$phy_card_no' ORDER BY id DESC LIMIT 1");
	
	if(!$check_stock){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($check_stock)==0){
		errorLog("Ril Stock not found. Card_no : $phy_card_no",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Stock not found !');
			window.location.href='./';
		</script>";
		exit();
	}
	
	$row_ril_stock = fetchArray($check_stock);
	
	$ril_stockid = $row_ril_stock['stockid'];
	
	if($row_ril_stock['balance']<$amount)
	{
		echo "<script>
			alert('Card not in stock. Available Balance is : $row_ril_stock[balance].');
			$('#card_no').val('');
			$('#card_no2').val('');
			$('#phy_card_no').val('');
			$('#rilpre').val('');
			$('#insert_diesel_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}		
}
else
{
	$ril_stockid="";
}

	$trans_id_Qry=GetTransId("DSL",$conn,"diesel");
	
	if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Fetching Transaction Id.","./");
		exit();
	}
	
	$trans_id=$trans_id_Qry;
	
StartCommit($conn);
$flag = true;
	
if($consumer_pump=="1")
{
	if($numRowsQty==1)
	{
		$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel (unq_id,trip_id,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
		timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trans_id','$tno','$rate','$qty','$amount','$date',
		'$fuel_company-$phy_card_no','$branch','$_SESSION[user_code]','$timestamp','$PurchaseId')");

		if(!$insert_diesel_diary){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}	

		$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
		narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','$bill_no','$tno','$amount','$type','$card','$phy_card_no',
		'$fuel_company','$fuel_company-$phy_card_no','$branch','$date','$timestamp','$mobile_no')");

		if(!$insert_diesel_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='$remainQty' WHERE id='$StockId'");

		if(!$updateStock1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		if($split_entry=="0")
		{
			$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
			timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trans_id','$tno','$rate','$qty','$amount','$date','$fuel_company-$phy_card_no',
			'$branch','$_SESSION[user_code]','$timestamp','$PurchaseId')");

			if(!$insert_diesel_diary)
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
			narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','$bill_no','$tno','$amount','$type','$card','$phy_card_no',
			'$fuel_company','$fuel_company-$phy_card_no','$branch','$date','$timestamp','$mobile_no')");

			if(!$insert_diesel_entry){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='$remainQty' WHERE id='$StockId'");

			if(!$updateStock1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	
		}
		else
		{
			$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
			timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trans_id','$tno','$rate','$qty1','$amount','$date','$fuel_company-$phy_card_no',
			'$branch','$_SESSION[user_code]','$timestamp','$PurchaseId')");

			if(!$insert_diesel_diary)
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry(unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
			narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','$bill_no','$tno','$amount','$type','$card','$phy_card_no',
			'$fuel_company','$fuel_company-$phy_card_no','$branch','$date','$timestamp','$mobile_no')");

			if(!$insert_diesel_entry){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='0' WHERE id='$StockId'");

			if(!$updateStock1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_diary2=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
			timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trans_id','$tno','$rate2','$qty2','$amount2','$date',
			'$fuel_company-$phy_card_no','$branch','$_SESSION[user_code]','$timestamp','$PurchaseId2')");

			if(!$insert_diesel_diary2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_entry2=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
			narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','$bill_no','$tno','$amount2','$type','$card','$phy_card_no',
			'$fuel_company','$fuel_company-$phy_card_no','$branch','$date','$timestamp','$mobile_no')");

			if(!$insert_diesel_entry2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$updateStock2 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='$remainQty2' WHERE id='$StockId2'");

			if(!$updateStock2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}			
		}
	}	
}
else
{	
	$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel (trip_id,trans_id,unq_id,tno,rate,qty,amount,date,narration,branch,branch_user,
	timestamp,ril_card,stockid) VALUES ('$trip_id','$trans_id','$diesel_key','$tno','$rate','$qty','$amount','$date','$fuel_company-$phy_card_no',
	'$branch','$_SESSION[user_code]','$timestamp','$rilpre','$ril_stockid')");

	if(!$insert_diesel_diary)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,narration,branch,
	date,dsl_mobileno) VALUES ('$diesel_key','$bill_no','$tno','$amount','$type','$card','$phy_card_no','$fuel_company','$fuel_company-$phy_card_no','$branch','$date','$mobile_no')");

	if(!$insert_diesel_entry)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$insert_id=getInsertID($conn);

	if($type=='CARD')
	{
		$card_live_bal=Qry($conn,"select balance from diesel_api.dsl_cards where cardno='$card'");
		if(!$card_live_bal){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($card_live_bal)==0){
			$flag = false;
			errorLog("Card not found.",$conn,$page_name,__LINE__);
		}
		
		$row_card_bal=fetchArray($card_live_bal); 
		$live_bal = $row_card_bal['balance']; 

		$qry_log=Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,
		dslcomp,dsltype,cardno,vehno,cardbal,amount,mobileno,reqid) VALUES 
		('$branch','SUCCESS','New Request Added','$timestamp','OWN','$tno','$fuel_company',
		'CARD','$card','$phy_card_no','$live_bal','$amount','$mobile_no','$insert_id')");
		
		if(!$qry_log){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
	}
}
				
// $fetch_diesel_qty = Qry($conn,"SELECT diesel_qty FROM trip WHERE id='$trip_id'");
// if(!$fetch_diesel_qty)
// {
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

// $row_qty = fetchArray($fetch_diesel_qty);

// $new_qty = sprintf("%.2f",$row_qty['diesel_qty']+$total_qty);

$dateMsg = date("d/m/y",strtotime($date));

if($rilpre=="1" AND $pump_card=='CARD')
{
	$update_ril_stock = Qry($conn,"UPDATE diesel_api.dsl_ril_stock SET balance=balance-'$total_amount' WHERE 
	cardno='$phy_card_no' ORDER BY id DESC LIMIT 1");
			
	if(!$update_ril_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_trip=Qry($conn,"UPDATE dairy.trip SET diesel=diesel+'$total_amount',diesel_qty=ROUND(diesel_qty+'$total_qty',2) WHERE id='$trip_id'");
if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dlt_pending_diesel = Qry($conn,"DELETE FROM dairy._pending_fix_diesel WHERE trip_id='$trip_id'");
if(!$dlt_pending_diesel){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	$msg_text = "आपको $msg_text से  $branch ब्रांच ने दिनांक  $dateMsg  को  $total_qty लीटर डीजल दिया";
	
	if($row_Driver['mobile2']!='')
	{
		SendTransSms($row_Driver['mobile2'],$tno.": ".$msg_text);
	}
	else
	{
		SendTransSms($row_Driver['mobile'],$tno.": ".$msg_text);
	}	
	
	MySQLCommit($conn);
}
else
{
	MySQLRollBack($conn);
	errorLog("COMMIT NOT COMPLETED. Timestamp : $timestamp. Truck No: $tno, TripNo: $trip_no.",$conn,$page_name,__LINE__);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}

echo "<script>
		swal({
			title: 'Updated Successfully !',
			type: 'success',
			closeOnConfirm: true
			});
		$('#diesel_adv_button_pending').attr('disabled',false);
		$('#DieselAdvFormPending')[0].reset();
		document.getElementById('hide_diesel_pending').click();
		TripAdv('#$trip_id');
		$('#loadicon').hide();
	</script>";
	closeConnection($conn);
	exit();
?>