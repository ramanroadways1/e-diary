<form id="AssetFormUpdate" autocomplete="off">

<div class="modal fade" id="AssetFormViewModal" role="dialog" data-backdrop="static" data-keyboard="false" 
	style="left: 0;top: 0;background-image:url(truck_bg.jpg); background-repeat: no-repeat;  position: fixed;width: 100%;height: 100%; 
	background-size: cover;">

    <div class="modal-dialog modal-lg">
      <div class="modal-content" style="">
        
		<div class="modal-header bg-primary">
			<span style="font-size:13px">Asset Form : <?php echo $tno; ?></span>
			<button type="button" data-dismiss="modal" class="btn btn-default pull-right btn-sm"><span class="fa fa-window-close"></span>&nbsp; Close</button>
		</div>
		
		<div class="modal-header">
			<span style="font-size:13px">
			Driver Name: <?php echo $driver_name; ?>, 
			Mobile: <?php echo $driver_mobile=$row_d['mobile']; ?>, Lic No: <?php echo $driver_lic_no=$row_d['lic']; ?>
			</span>
		</div>
		
<script type="text/javascript">
$(document).ready(function (e) {
$("#AssetFormUpdate").on('submit',(function(e) {
$("#loadicon").show();
$("#asset_update").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./asset_form_update.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_asset_view").html(data);
	},
	error: function() 
	{} });}));});
</script>		
		
<div class="modal-body">
<div id="result_asset_view"></div>

<div class="row">
	<div class="form-group col-md-12">
	
<div class="table-responsive">	
	
<table <?php if(isMobile()){ echo "style='font-size:11px'"; } else { echo "style='font-size:13px'"; } ?> class="table table-striped table-bordered">
	
	<thead>
		<tr>
			<th>सामान </th>
			<th>सामान दिया</th>
			<th <?php if(isMobile()){ echo "style='width:30%'"; } ?>>मात्रा </th>
			<th>मूल्य </th>
		</tr>
	</thead>
	
<script>	
function ItemMinusView(code,min_qty,rate)
{
	var ext_value=$('#i_qty'+code).val();
	
	var new_value = ext_value-1;
	
	if(new_value<min_qty)
	{
		alert('Min value is : '+min_qty);
		$('#i_qty'+code).val(ext_value);
		$('#i_qty_box'+code).html(ext_value);
		
		var new_rate = Math.round(rate/min_qty);
		var new_rate_set = Math.round(ext_value*new_rate);
		$('#rate_asset_v'+code).html(new_rate_set);
		$('#rate_asset_box_v'+code).val(new_rate_set);
	}
	else
	{
		$('#i_qty'+code).val(new_value);
		$('#i_qty_box'+code).html(new_value);
		
		if(rate==0)
		{
			$('#rate_asset_v'+code).html('0');
			$('#rate_asset_box_v'+code).val('0');
		}
		else
		{
			var new_rate = Math.round(rate/min_qty);
			var new_rate_set = Math.round(new_value*new_rate);
			$('#rate_asset_v'+code).html(new_rate_set);
			$('#rate_asset_box_v'+code).val(new_rate_set);
		}
	}
}

function ItemPlusView(code,min_qty,rate)
{
	var ext_value=$('#i_qty'+code).val();
	
	var new_value = ++ext_value;
	
	$('#i_qty'+code).val(new_value);
	$('#i_qty_box'+code).html(new_value);
	
	if(rate==0)
	{
		$('#rate_asset_v'+code).html('0');
		$('#rate_asset_box_v'+code).val('0');
	}
	else
	{
		var new_rate = Math.round(rate/min_qty);
		var new_rate_set = Math.round(new_value*new_rate);
		$('#rate_asset_v'+code).html(new_rate_set);
		$('#rate_asset_box_v'+code).val(new_rate_set);
	}
}

function ChkQtyView(elem,item_code,dbQty,dbRate)
{
	if(elem==0)
	{
		$('#i_qty'+item_code).val('0');
		$('#i_qty_box'+item_code).html('0');
		$('#rate_asset_v'+item_code).html('0');
		$('#rate_asset_box_v'+item_code).val('0');
		
		$('#MinusBtnView'+item_code).hide();
		$('#PlusBtnView'+item_code).hide();
	}
	else
	{
		$('#i_qty'+item_code).val(dbQty);
		$('#i_qty_box'+item_code).html(dbQty);
		$('#rate_asset_v'+item_code).html(dbRate);
		$('#rate_asset_box_v'+item_code).val(dbRate);
		
		$('#MinusBtnView'+item_code).show();
		$('#PlusBtnView'+item_code).show();
	}
}
</script>	

<?php
mysqli_set_charset($conn, 'utf8');
	
$qry_asset_item_view = Qry($conn,"SELECT a.item_code,a.item_qty,a.item_rate,a.item_value_up,i.item_name,i.qty,i.rate,i.doc FROM 
dairy.asset_form as a
LEFT OUTER JOIN dairy.asset_form_items as i ON i.code=a.item_code 
WHERE a.driver_code='$driver_code_value' ORDER BY a.id ASC");
	
if(!$qry_asset_item_view){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}	
			
if(numRows($qry_asset_item_view)>0)
{
	while($row_asset_view = fetchArray($qry_asset_item_view))
	{
		if($row_asset_view['item_value_up']=="1"){
			$selected_yes = "checked";
			$selected_no = "";
			$no_disabled = "disabled";
		}else{
			$selected_no = "checked";
			$selected_yes = "";
			$no_disabled = "";
		}
		
		if($row_asset_view['doc']=='1')
		{
			$select_box_v = "
			<label class='radio-inline'><input type='radio' $selected_yes value='1' name='item_sel[".$row_asset_view['item_code']."]' required>Original</label>";
			
			if(isMobile()){
				$select_box_v.="<br><br>";
			}
			
			
			$select_box_v.="<label class='radio-inline'><input type='radio' $selected_no value='0' name='item_sel[".$row_asset_view['item_code']."]' required>Xerox</label>";
			
			// <select id='i_view$row_asset_view[item_code]' value='$row_asset_view[item_value_up]' class='form-control' name='item_sel[]' required>
				// <option value=''>--SELECT--</option>
				// <option value='1'>ORIGINAL</option>
				// <option value='0'>XEROX</option>
			// </select>";
		}
		else
		{
			$select_box_v = "
			<label class='radio-inline'><input $selected_yes type='radio' value='1' name='item_sel[".$row_asset_view['item_code']."]' 
			onchange=ChkQtyView(this.value,'$row_asset_view[item_code]','$row_asset_view[item_qty]','$row_asset_view[item_rate]') required>Yes</label>
			";
			
			if(isMobile()){
				$select_box_v.="<br><br>";
			}
			
			$select_box_v.="
			<label class='radio-inline'><input $selected_no type='radio' $no_disabled value='0' name='item_sel[".$row_asset_view['item_code']."]' 
			onchange=ChkQtyView(this.value,'$row_asset_view[item_code]','$row_asset_view[qty]','$row_asset_view[rate]') required>No</label>";
			
			// if($row_asset_view['item_value_up']=='1')
			// {
				// $select_box_v = "
				// <select onchange=ChkQtyView(this.value,'$row_asset_view[item_code]','$row_asset_view[item_qty]','$row_asset_view[item_rate]') class='form-control' name='item_sel[]' required>
					// <option value=''>--SELECT--</option>
					// <option value='1'>YES</option>
				// </select>";
			// }
			// else
			// {
				// $select_box_v = "
				// <select onchange=ChkQtyView(this.value,'$row_asset_view[item_code]','$row_asset_view[qty]','$row_asset_view[rate]') class='form-control' name='item_sel[]' required>
					// <option value=''>--SELECT--</option>
					// <option value='1'>YES</option>
					// <option value='0'>NO</option>
				// </select>";
			// }
		}
			
	$chk_access_view = Qry($conn,"SELECT id FROM dairy.asset_form_rule WHERE item_code='$row_asset_view[item_code]' AND branch='$branch'");
			
	if(!$chk_access_view){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
			
	if($row_asset_view['item_value_up']=='1')
	{
		$minQtyofAsset=$row_asset_view['item_qty'];
		$RateofAsset=$row_asset_view['item_rate'];
	}
	else
	{
		$minQtyofAsset=$row_asset_view['qty'];
		$RateofAsset=$row_asset_view['rate'];
	}
			
		if(numRows($chk_access_view)==0)
		{
			$item_button_v="<input style='display:none' id='i_qty$row_asset_view[item_code]' min='$minQtyofAsset' type='number' 
			value='$minQtyofAsset' name='item_qty[]' readonly>
				
			<span id='i_qty_box$row_asset_view[item_code]'>$minQtyofAsset</span>";
		}
		else
		{
				$item_button_v="<button id='MinusBtnView$row_asset_view[item_code]' 
				onclick=ItemMinusView('$row_asset_view[item_code]','$minQtyofAsset','$RateofAsset') type='button' 
				class='btn btn-xs btn-warning'> - </button>
				
				&nbsp; <span id='i_qty_box$row_asset_view[item_code]'>$minQtyofAsset</span> &nbsp;
				
				<button id='PlusBtnView$row_asset_view[item_code]' 
				onclick=ItemPlusView('$row_asset_view[item_code]','$minQtyofAsset','$RateofAsset') type='button' 
				class='btn btn-xs btn-success'> + </button>
				
				<input id='i_qty$row_asset_view[item_code]' style='display:none' min='$minQtyofAsset' type='number' 
				value='$minQtyofAsset' name='item_qty[]' readonly>
				";
		}
			
		echo "<tr>
				<td>
					$row_asset_view[item_name]
				</td>
				
				<input type='hidden' value='$row_asset_view[item_code]' name='item_code[]'>
				
				<td style='padding: 10px;'>
					$select_box_v
				</td>
				
				<td style='width:30%'>
					<center>$item_button_v</center>
				</td>
				
				<input type='hidden' value='$row_asset_view[item_qty]' name='old_qty[]'>
				<input type='hidden' value='$row_asset_view[item_value_up]' name='old_up_value[]'>
				
				<input type='hidden' id='rate_asset_box_v$row_asset_view[item_code]' value='$RateofAsset' name='asset_rate[]'>
				<td>
					₹ <span id='rate_asset_v$row_asset_view[item_code]'>$RateofAsset</span>
				</td>
				
				</tr>
			";
		}
	}
//
?>
</table>

</div>
</div>
</div>

<input type="hidden" value="<?php echo $tno; ?>" name="tno">
<?php
if(numRows($qry_asset_item_view)>0)
{
?>
<p align="center"><button type="submit" name="asset_submit" class="btn btn-sm btn-primary" id="asset_update">Update Asset - Form</button></p>
<?php
}
else
{
	echo "<p>&nbsp;</p>";
}
?>
		</div>
    </div>
  </div>
</div>
</form>