<?php
require("./connect.php");

$from = escapeString($conn,strtoupper($_POST['from']));
$to = escapeString($conn,strtoupper($_POST['to']));
$tno = escapeString($conn,strtoupper($_SESSION['diary']));

	$from_id=Qry($conn,"SELECT id FROM station WHERE name='$from'");
	if(!$from_id)
	{
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}	
	
	$row_from=fetchArray($from_id);
	$from_id_loc=$row_from['id'];


$to_id=Qry($conn,"SELECT id FROM station WHERE name='$to'");
if(!$to_id)
{
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_to=fetchArray($to_id);
$to_id_loc=$row_to['id'];

if($from_id_loc=='' || $to_id_loc=='')
{
	errorLog("Unable to fetch Location Id.",$conn,$page_name,__LINE__);
	Redirect("Unable to fetch Location Id.","./");
	exit();
}

$ext_trip=Qry($conn,"SELECT from_id,to_id FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1");
if(numRows($ext_trip)>0)
{
	$row_ext_trip=fetchArray($ext_trip);
	
	if($row_ext_trip['from_id']==$from_id_loc AND $row_ext_trip['to_id']==$to_id_loc)
	{
		Redirect("Duplicate Trip Found.","./");
		exit();
	}
}

$sel_master=Qry($conn,"SELECT km FROM dairy.master_km WHERE from_loc_id='$from_id_loc' AND to_loc_id='$to_id_loc'");
if(!$sel_master)
{
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($sel_master)>0)
{
	$row_km=fetchArray($sel_master);
	
	echo "<script>
			$('#trip_km_no_lr').val('$row_km[km]');
			$('#trip_km_no_lr').attr('readonly',true);
			$('#set_km_no_lr').val('0');
		</script>";
}
else
{
	echo "<script>
			$('#trip_km_no_lr').val('');
			$('#trip_km_no_lr').attr('readonly',false);
			$('#set_km_no_lr').val('1');
		</script>";
}

echo "<script>
			$('#from_loc_id_no_lr').val('$from_id_loc');
			$('#to_loc_id_no_lr').val('$to_id_loc');
	</script>";
?>