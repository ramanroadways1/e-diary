<script type="text/javascript">
$(document).ready(function (e) {
$("#ChequeForm").on('submit',(function(e) {
$("#loadicon").show();
$("#cheque_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./cheque_adv.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
  $(function() {  
      $("#cheq_acno").autocomplete({
			source: function(request, response){ 
			$.ajax({
                  url: "autofill/bank_acno.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#bank_branch').val(ui.item.bank_branch);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#bank_branch").val('');
				alert('Broker does not exist !'); 
              } 
              },
			}); 
      }); 
</script>

  <div class="modal fade" id="ChequeAdvModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content" style="font-family:Verdana">
		<form id="ChequeForm" autocomplete="off">   
		<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-12" style="background:#5989EE;color:#FFF">
				<h5>Cheque to driver :</h5>
			</div>
		
			<div class="form-group col-md-4">
				<label style="font-size:12px;">Truck No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-4">
				<label style="font-size:12px;">Amount. <sup><font color="red">*</font></sup></label>
				<input type="number" min="1" name="amount" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label style="font-size:12px;">Cheq No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="cheq_no" class="form-control" required />
			</div>
			
			<div id="chq_div"></div>
			
			<script>
			function FetchCheqBank(acno)
			{
					$("#loadicon").show();
					jQuery.ajax({
					url: "./fetch_cheque_bank.php",
					data: 'acno=' + acno,
					type: "POST",
					success: function(data) {
					$("#chq_div").html(data);
					$("#loadicon").hide();
					},
					error: function() {}
					});
			}
			</script>
			
			<div class="form-group col-md-6">
				<label style="font-size:12px;">Ac No. <sup><font color="red">*</font></sup></label>
				<input name="cheq_acno" type="text" id="cheq_acno" class="form-control" required />
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:12px;">Bank/Branch. <sup><font color="red">*</font></sup></label>
				<input name="bank_branch" type="text" id="bank_branch" class="form-control" readonly required />
			</div>
			
			<input type="hidden" name="date" value="<?php echo date("Y-m-d"); ?>" class="form-control" readonly required />
			
			<input type="text" name="trip_id" class="trip_id_set" />
			<input type="text" name="trip_no" class="trip_no_set" />
			<input type="text" name="driver_name" class="driver_name_set" />
			<input type="hidden" name="driver_code" value="<?php echo $driver_code_value; ?>" />
			
			<div class="form-group col-md-12">
				<label style="font-size:12px;">Narration. <sup><font color="red">*</font></sup></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,/]/,'')" class="form-control" name="narration" required></textarea>
			</div>
		</div>
        </div>
        <div class="modal-footer">
          <button <?php if($driver_code_value=="") {echo "disabled"; } ?> type="submit" id="cheque_button" class="btn btn-danger">Submit</button>
          <button type="button" class="btn btn-primary" id="hide_cheque" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>
  