<?php
require_once("./connect.php");

$driver_code_value=escapeString($conn,strtoupper($_POST['code']));
$tno=escapeString($conn,strtoupper($_SESSION['diary']));

$timestamp=date("Y-m-d H:i:s");

$fetch_bal_qry=Qry($conn,"SELECT amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code_value'");
if(!$fetch_bal_qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$fetch_happay_bal=Qry($conn,"SELECT card_no2 FROM dairy.happay_card_inventory WHERE veh_no=(SELECT card_using FROM dairy.happay_card 
WHERE tno='$tno' AND card_assigned='1')");	
	
if(!$fetch_happay_bal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !!');
		window.location.href='./logout.php';
	</script>";
	exit();
}

if(numRows($fetch_happay_bal)>0)
{
	$getLiveBal = Qry($conn,"SELECT balance FROM dairy.happay_live_balance WHERE veh_no=(SELECT card_using FROM dairy.happay_card WHERE 
	tno='$tno')");
	
	if(!$getLiveBal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>
			alert('Error !!');
			window.location.href='./logout.php';
		</script>";
		exit();
	}

	$rowLive_Bal = fetchArray($getLiveBal);
	
	$row_Happay=fetchArray($fetch_happay_bal);
	$happay_bal=$rowLive_Bal['balance'];
	$happay_card_no="XXXX-XXXX-XXXX-".$row_Happay['card_no2'];
}
else
{
	$happay_bal = "NA";
	$happay_card_no = "NA";
}

if(numRows($fetch_bal_qry)>1)
{
	errorLog("DRIVER NOT DOWN PROPERLY ACTIVE ON OTHER TRUCK",$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while fetching driver balance.');
		window.location.href='./logout.php';
	</script>";
	exit();
}

if(numRows($fetch_bal_qry)>0)
{
	$row_balance=fetchArray($fetch_bal_qry);
	$balance=$row_balance['amount_hold'];
}
else
{
	$balance="0";
}

echo "<script>
		$('#bal_show').html('$balance');
		$('#happay_bal').html('$happay_bal');
		$('#happay_bal_in_cash_window').html('$happay_bal');
		$('#view_current_bal').html('$happay_bal');
		$('#happay_card_no').val('$happay_card_no');
		CheckHappayStatus('$happay_card_no');
	</script>";
?>