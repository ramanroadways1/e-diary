<?php
require("./connect.php");

$trip_id = escapeString($conn,$_POST['trip_id']);

if($trip_id=='')
{
	echo "<script>alert('Trip not found.');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_trip_info = Qry($conn,"SELECT trip_no,tno,branch,from_station,to_station,loaded_hisab,lrno,driver_gps_naame,gps_naame_txn_id,pod,lr_type,
oxygen_lr FROM dairy.trip WHERE id='$trip_id'");

if(!$get_trip_info){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#pod_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_trip = fetchArray($get_trip_info);

if($row_trip['lrno']=='EMPTY' || $row_trip['lrno']=='ATLOADING' || $row_trip['lrno']=='')
{
	echo "<script>
		alert('LR number is not valid !');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$lrno_pod = $row_trip['lrno']; 
$from_loc = $row_trip['from_station']; 
$to_loc = $row_trip['to_station']; 
$trip_no = $row_trip['trip_no']; 
$tno = $row_trip['tno']; 
$trip_branch = $row_trip['branch']; 
$loaded_hisab = $row_trip['loaded_hisab']; 
$driver_gps_naame = $row_trip['driver_gps_naame']; 
$gps_naame_txn_id = $row_trip['gps_naame_txn_id']; 

?>
<button id="PodModal_New_btn" style="display:none" type="button" data-toggle="modal" data-target="#PodModal_New"></button>

<div class="modal fade" style="background:#DDD" id="PodModal_New" role="dialog" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
		<div class="modal-header bg-primary">
			Upload POD : <?php echo "<font color=''>".$from_loc." to ".$to_loc."</font>"; ?>
		</div>
	  <div class="modal-body">
		<div class="row">
			
		<div class="form-group col-md-12 table-responsive" style="overflow:auto">
		
			<table style="width:100%;font-size:13px;" class="table table-bordered">
				<tr>
					<th style="width:30px">#</th>
					<th style="width:120px">LR_No & Date</th>
					<th>POD_Copy</th>
					<th style="width:220px">Claim</th>
					<?php
					/*
					if($driver_gps_naame>0)
					{
					?>
				<th style="width:220px">VEDL-GPS</th>
					<?php
					}
					*/
					?>
				</tr>
<?php
	$sn=1;
	foreach(explode(",",$lrno_pod) as $lrno_1)
	{
			if(substr($lrno_1,3,1)=='M')
			{
				$chk_pod = Qry($conn,"SELECT id,bilty_no as vou_no,'0' as con1_id,lrdate,crossing,pod_date,claim_entry,
				'0' as billparty_id,'' as billing_branch,'0' as oxygen_count FROM mkt_bilty WHERE bilty_no='$lrno_1'");
				
				if(!$chk_pod){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>
						alert('Error !');
						$('#loadicon').fadeOut('slow');
					</script>";
					exit();
				}
				
				$claim_update="NO";
				$vou_type_claim="MARKET_BILTY";
			}
			else
			{
				if($row_trip['oxygen_lr']=="1")
				{
					$chk_pod = Qry($conn,"SELECT f.id,f.frno as vou_no,f.date as lrdate,f.con1_id,f.crossing,f.pod_rcv_date_own AS pod_date,
					f.claim_entry,l.billingbranch as billing_branch,l.billparty as billparty_id,oxygen.count as oxygen_count 
					FROM freight_form_lr AS f 
					LEFT JOIN lr_sample AS l ON l.lrno=f.lrno 
					LEFT JOIN oxygen_olr AS oxygen ON oxygen.frno=f.frno 
					WHERE f.frno='$row_trip[lr_type]'");
				}
				else
				{
					$chk_pod = Qry($conn,"SELECT f.id,f.frno as vou_no,f.date as lrdate,f.con1_id,f.crossing,f.pod_rcv_date_own AS pod_date,
					f.claim_entry,l.billingbranch as billing_branch,l.billparty as billparty_id,'0' as oxygen_count 
					FROM freight_form_lr AS f 
					LEFT JOIN lr_sample AS l ON l.lrno=f.lrno 
					LEFT JOIN oxygen_olr AS oxygen ON oxygen.frno=f.frno 
					WHERE f.lrno='$lrno_1' AND f.truck_no='$tno'");
				}
				
				if(!$chk_pod){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					echo "<script>
						alert('Error !');
						$('#loadicon').fadeOut('slow');
					</script>";
					exit();
				}
				
				$vou_type_claim="OWN_BILTY";
				
				// $chk_claim = Qry($conn,"SELECT id FROM lr_sample WHERE lrno='$lrno_1' AND claim='0'");
				
				// if(!$chk_claim){
					// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					// echo "<script>
						// alert('Error !');
						// $('#loadicon').fadeOut('slow');
					// </script>";
					// exit();
				// }
				
				// if(numRows($chk_claim)>0)
				// {
					// $claim_update="YES";
				// }
				// else
				// {
					// $claim_update="NO";
				// }
			}
			
		$row_1 = fetchArray($chk_pod);
		
		if($row_1['crossing']=='YES'){
			$is_crossing="1";
		}
		else{
			$is_crossing="0";
		}
		
	if($row_1['pod_date']==0)
	{
		if($row_trip['oxygen_lr']=="1" AND $row_1['oxygen_count']!="3")
		{
			$pod_status="<font color='maroon'>Oxygen POD allowed in last voucher only.</font>";
			$pod_status_html="1";
		}
		else
		{
			echo '<script type="text/javascript">
			$(document).ready(function (e) {
				$("#PodFormUpload'.$sn.'").on("submit",(function(e) {
				e.preventDefault();
				$("#loadicon").show();
				$("#rcv_button'.$sn.'").attr("disabled", true);
				$.ajax({
						url: "./upload_pod_new.php",
						type: "POST",
						data:  new FormData(this),
						contentType: false,
						cache: false,
						processData:false,
						success: function(data){
							$("#result_pod_upload_result").html(data);
						},
						error: function() 
						{
						} 	        
				   });
				}));
			});
		</script>';
		
			$pod_status="
			<form id='PodFormUpload$sn'>
			<div class='form-inline'>
			<label style='font-size:13px;'>Delivery Date <font color='red'><sup>*</sup></font></label>
			<br />
			<input max='".date("Y-m-d")."' class='del_date_Ip form-control' id='del_date$sn' required='required' 
			pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' type='date' style='width:140px;font-size:11px;height:28px;' name='del_date'>";
			
			if($row_1['con1_id']=='56')
			{
				$pod_status.="&nbsp; <input id='pod_copy_ip$sn' required='required' type='file' class='form-control' 
				accept='application/pdf' style='width:140px;font-size:10px;height:28px;' name='upload_file[]'>";
			}
			else
			{
				$pod_status.="&nbsp; <input id='pod_copy_ip$sn' required='required' type='file' multiple='multiple' class='form-control' 
				accept='image/jpeg,image/gif,image/png,application/pdf,image/x-eps' style='width:140px;font-size:9px;height:28px;' name='upload_file[]'>";
			}
			
			$pod_status.="&nbsp; <button type='submit' style='padding:3px' id='rcv_button$sn' class='btn btn-xs btn-success'>
				<b>Upload &nbsp;<span style='' class='fa fa-floppy-o'></b></span>
			</button>
				<input type='hidden' name='id' value='$sn'>			
				<input type='hidden' name='vou_no' value='$row_1[vou_no]'>			
				<input type='hidden' name='lrno' value='$lrno_1'>			
				<input type='hidden' name='truck_no' value='$tno'>			
				<input type='hidden' name='trip_no' value='$trip_no'>			
				<input type='hidden' name='trip_id' value='$trip_id'>			
				<input type='hidden' name='trip_branch' value='$trip_branch'>			
				<input type='hidden' name='lr_date' value='$row_1[lrdate]'>			
				<input type='hidden' name='con1_id' value='$row_1[con1_id]'>			
				<input type='hidden' name='tab_id' value='$row_1[id]'>	
				<input type='hidden' name='billing_branch' value='$row_1[billing_branch]'>			
				<input type='hidden' name='billparty_id' value='$row_1[billparty_id]'>	
				<input type='hidden' name='is_oxygen_lr' value='$row_trip[oxygen_lr]'>				
				<input type='hidden' name='loaded_hisab' value='$loaded_hisab'>			
				<input type='hidden' name='vou_type' value='$vou_type_claim'>			
			</form></div>";
			
			$pod_status_html="0";
		}
	}
	else
	{
		$pod_status="Uploaded";
		$pod_status_html="1";
	}
		?>
		
		<tr>
			<td style="width:30px"><?php echo $sn; ?></td>
			<td style="width:120px"><?php echo $lrno_1."<br>".date("d/m/y",strtotime($row_1['lrdate'])); ?></td>
			<td id="pod_copy_td<?php echo $sn; ?>"><?php echo $pod_status; ?></td>
			<td style="width:220px" id="claim_td<?php echo $sn; ?>">
			<?php 
			if($row_1['claim_entry']=='0')
			{
				if($row_trip['oxygen_lr']=="1")
				{
					echo "No Claim";
				}
				else
				{
					echo " 
					<input type='hidden' id='claim_branch_lrno_$sn' value='$lrno_1'>
					<input type='hidden' id='claim_branch_vou_no_$sn' value='$row_1[vou_no]'>
					<input type='hidden' value='$pod_status_html' id='is_pod_rcvd_$sn'>
					<input type='hidden' value='$row_1[id]' id='vou_id_claim$sn'>
					<input type='hidden' value='$vou_type_claim' id='vou_type_claim$sn'>
					
					<div class='form-inline'>
						<label>Claim in this LR? <font color='red'><sup>*</sup></font></label>
						<br />
						<select style='width:130px !important;font-size:12px;height:30px;' onchange=ClaimSelection(this.value,'$sn') class='form-control' id='claim_branch_$sn'>
							<option value=''>--select--</option>
							<option value='YES'>YES</option>
							<option value='NO'>NO</option>
						</select>
					</div>";
				}
			}
			else
			{
				if($row_1['claim_entry']=="1"){
					$claim_branch="YES";
				}else{
					$claim_branch="<b>No Claim</b>";
				}
				echo "$claim_branch";
			}
			?>
			</td>
			<?php
			/*
			if($driver_gps_naame>0)
			{
			?>
			<td>
				<div class='form-inline'>
				<label>GPS Received ? <font color='red'><sup>*</sup></font></label>
				<br />
				<select style="width:130px !important;font-size:12px;height:30px;" id="vedl_gps_sel" class="form-control">
					<option class="gps_sel_opt1" value="">--select--</option>
					<option class="gps_sel_opt1" value="YES">YES</option>
					<option class="gps_sel_opt1" value="NO">NO</option>
				</select>
				<button type="button" id="btn_gps_sel1" onclick="VEDLGps('<?php echo $trip_id; ?>')" class="btn btn-sm btn-success">Update</button>
				</div>
			</td>
			<?php
			}
			*/
			?>
		</tr>	
<?php
$sn++;
	}
?>	
		</table>
		</div>
			
		</div>
        </div>
		
<div id="gps_sel_res1"></div>		
		
<style>
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
</style>		
	
<script>	
// function VEDLGps(trip_id)
// {
	// var elem = $('#vedl_gps_sel').val();
	// var txn_id = '<?php echo $gps_naame_txn_id; ?>';
	
	// if(elem!='')
	// {
		// $("#loadicon").show();	
		// jQuery.ajax({
			// url: "./update_gps_status_vedl.php",
			// data: 'trip_id=' + trip_id + '&elem=' + elem + '&txn_id=' + txn_id,
			// type: "POST",
			// success: function(data) {
					// $("#gps_sel_res1").html(data);
			// },
			 // error: function() {}
		// });
	// }
	// else
	// {
		// alert('Select yes or not first !');
	// }
// }

function ClaimSelection(elem,lr_id)
{
	var lrno = $('#claim_branch_lrno_'+lr_id).val();
	var vou_no = $('#claim_branch_vou_no_'+lr_id).val();
	var is_pod_rcvd = $('#is_pod_rcvd_'+lr_id).val();
	var vou_id = $('#vou_id_claim'+lr_id).val();
	var vou_type = $('#vou_type_claim'+lr_id).val();
	
	// if(is_pod_rcvd=='0')
	// {
		// alert('Please receive pod first !!');
		// $('#claim_branch_'+lr_id).val('');
	// }
	// else
	// {
		if(elem=='YES')
		{
			$("#loadicon").show();	
				jQuery.ajax({
				url: "./load_claim_modal.php",
				data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&lr_id=' + lr_id + '&vou_id=' + vou_id + '&vou_type=' + vou_type,
				type: "POST",
				success: function(data) {
				$("#modal_claim_result1").html(data);
			},
			 error: function() {}
			});
		}
		else if(elem=='NO')
		{
			$("#loadicon").show();	
				jQuery.ajax({
				url: "./update_claim_status.php",
				data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&lr_id=' + lr_id + '&vou_id=' + vou_id + '&vou_type=' + vou_type,
				type: "POST",
				success: function(data) {
				$("#modal_claim_result1").html(data);
			},
			 error: function() {}
			});
		}
	// }
}
</script>
	<div class="modal-footer">
			<button type="button" onclick="window.location.href='./'" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
			<button style="display:none" type="button" id="pod_upload_modal_close_btn" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
 
<script> 
$('#PodModal_New_btn')[0].click();
$('#loadicon').fadeOut('slow');
</script> 

<div id="modal_claim_result1"></div>