<?php
require("./connect.php");
require("./_header.php");
$date=date("Y-m-d");
include("code_disabled_right_click.php");
?>
<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<br/>
<br/>

<style>
label{font-size:13px;}
.form-control{
	text-transform:uppercase;
}
#bg {
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: url(truck_bg.jpg) no-repeat center center fixed;
  background-size: cover;
  -webkit-filter: blur(4px);    
}
</style>

<div id="bg"></div>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormBillParty").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#submit_btn").attr("disabled", true);
	$.ajax({
        	url: "./save_billing_party.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_div").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<form action="" id="FormBillParty" autocomplete="off" method="POST">
<div class="container-fluid">
	<div class="row">
		<<div class="col-md-5 col-md-offset-1" style="border:0px solid gray;border-radius:px;box-shadow:0 0 25px #DDD inset">
			
			<div class="row bg-primary" style="padding:3px;">
				<center><span style="font-size:16px;color:#FFF;">ADD BILLING PARTY</span></center>
			</div>	
			
			<div class="form-group col-md-12"></div>
			
			<div class="form-group col-md-12">
				<label>Billing Party Name <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z.,]/,'')" name="name" class="form-control" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>PAN number <font color="red"><sup>(optional)</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePAN2()" id="pan_no" minlength="10" maxlength="10" name="pan" class="form-control" />
			</div>
			
			<div class="form-group col-md-6">
				<label>GST number <font color="red"><sup>(optional)</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateGst()" id="gst_no" name="gst" class="form-control" />
			</div>
			
			<div class="form-group col-md-6">
				<label>Mobile number <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" minlength="10" maxlength="10" name="mobile" class="form-control" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Address <font color="red"><sup>*</sup></font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,]/,'')" name="addr" class="form-control" required></textarea>
			</div>
			
			<div class="form-group col-md-12">
				<input type="submit" id="submit_btn" name="submit_btn" class="btn btn-danger btn-sm" value="Save Billing Party" />
			</div>
			
			<br />
		</div>
	</div>
</div>
</form>

<script>
function ValidatePAN2() { 
  var Obj = document.getElementById("pan_no");
  if(Obj.value!=""){
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                document.getElementById("pan_no").setAttribute("style","background-color:#ff8080;color:#FFF");
				$("#submit_btn").attr("disabled", true);
                Obj.focus();
                return false;
            }
			else
            {
				document.getElementById("pan_no").setAttribute("style","background-color:#66ff66;color:#000");
				$("#submit_btn").attr("disabled", false);
            }
        }
		else
		{
			document.getElementById("pan_no").setAttribute("style","background-color:white;");
			$("#submit_btn").attr("disabled", false);
		}
  }

$('#loadicon').fadeOut('slow');
</script>

<script>
function ValidateGst()
{
	var gst = $('#gst_no').val();
	
	if(gst!='')
	{
		var gstinformat = new RegExp('^([0-9]{2})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})([1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})$');
	   
	   if(gstinformat.test(gst))
	   {
			$("#submit_btn").attr("disabled", false);
			document.getElementById("gst_no").setAttribute("style","background-color:#66ff66;color:#000");
	   }
	   else
	   {
			$("#submit_btn").attr("disabled", true);
			 document.getElementById("gst_no").setAttribute("style","background-color:#ff8080;color:#FFF");
		}
	}
	else
	{
		document.getElementById("gst_no").setAttribute("style","background-color:white;");
		$("#submit_btn").attr("disabled", false);
	}
}
</script>

<div id="result_div"></div>