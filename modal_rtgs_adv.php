<script type="text/javascript">
$(document).ready(function (e) {
$("#RtgsForm").on('submit',(function(e) {
$("#loadicon").show();
$("#rtgs_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./rtgs_adv.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form id="RtgsForm" autocomplete="off" style="font-size:13px">   
  <div class="modal fade" id="RtgsAdvModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
	  
		<div class="modal-header bg-primary">
			<span style="font-size:13px">RTGS Advance :</span>
		</div>
	
	<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-4">
				<label>Truck No. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="number" min="1" name="amount" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Date. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="text" name="date" value="<?php echo date("Y-m-d"); ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Ac Holder. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="text" name="ac_holder" id="ac_holder" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Ac No. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="text" name="acno" id="driver_acno" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-6"> 
				<label>Bank Name. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="text" name="bank" id="driver_bank_name" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-6">
				<label>IFSC Code. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="text" name="ifsc" id="driver_ifsc" class="form-control" readonly required />
			</div>
			
			<input type="hidden" name="trip_id" class="trip_id_set" />
			<input type="hidden" name="trip_no" class="trip_no_set" />
			<input type="hidden" name="driver_name" class="driver_name_set" />
			<input type="hidden" name="driver_code" value="<?php echo $driver_code_value; ?>" />
			
			<div class="form-group col-md-12">
				<label>Narration. <sup><font color="red">*</font></sup></label>
				<textarea style="font-size:12px" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,/]/,'')" class="form-control" name="narration" required></textarea>
			</div>
		</div>
        </div>
        <div class="modal-footer">
          <button <?php if($driver_code_value=="") {echo "disabled"; } ?> type="submit" id="rtgs_button" class="btn btn-sm btn-danger">Submit</button>
          <button type="button" class="btn btn-sm btn-primary" id="hide_rtgs" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
</form> 