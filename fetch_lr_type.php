<?php
require("./connect.php");

$sel_date_rule = Qry($conn,"SELECT days FROM dairy.date_rule WHERE type='2'");

if(!$sel_date_rule){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error.","./");
	exit();
}

if(numRows($sel_date_rule) == 0)
{
	echo "<script>alert('Error..');$('#lr_type').val('');$('#loadicon').fadeOut('slow');</script>";
	echo '<label>Voucher number <sup><font color="red">*</font></sup></label><input type="text" readonly value="" class="form-control" required />';
	exit();
}

$row_date_rule = fetchArray($sel_date_rule);

$date2=date("Y-m-d");
$days_ago = date('Y-m-d', strtotime("$row_date_rule[days] days", strtotime($date2)));

$tno=escapeString($conn,strtoupper($_SESSION['diary']));
$type=escapeString($conn,strtoupper($_POST['type']));

// $chk_lr_pending = Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$tno' AND lr_pending='1'");
	
// if(!$chk_lr_pending){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// echo "<script>alert('Error..');$('#lr_type').val('');$('#loadicon').fadeOut('slow');</script>";
	// echo '<label>Voucher number <sup><font color="red">*</font></sup></label><input type="text" readonly value="" class="form-control" required />';
	// exit();
// }

// if(numRows($chk_lr_pending)>0)
// {
	// AlertError("Clear LR pending trip first !!");
	// echo "<script>$('#lr_type').val('');$('#loadicon').fadeOut('slow');</script>";
	// echo '<label>Voucher number <sup><font color="red">*</font></sup></label><input type="text" readonly value="" class="form-control" required />';
	// exit();
// }
	
if($type=='CON20' || $type=='CON40' || $type=='EMPTY')
{
	if($type=='CON20'){
		$weight = 2;
		$lrno_1 = "";
	}
	else if($type=='CON40'){
		$weight = 4;
		$lrno_1 = "";
	}
	else if($type=='EMPTY'){
		$weight = 0;
		$lrno_1 = "EMPTY";
	}
	else{
		echo "<script>alert('Error..');$('#lr_type').val('');$('#loadicon').fadeOut('slow');</script>";
		echo '<label>Voucher number <sup><font color="red">*</font></sup></label><input type="text" readonly value="" class="form-control" required />';
		exit();
	}
	
	echo "<script>
		$('#actual').val('$weight');
		$('#charge').val('$weight');
		$('#lrno').val('$lrno_1');
		$('#billing_type_text').val('0');
		$('#freight_bilty_text').val('0');
		$('#at_loading_set').val('0');
	</script>";
	
	if($type=='CON20' || $type=='CON40')
	{
		echo "<script>$('#actual').attr('readonly',false);$('#charge').attr('readonly',false);$('#lrno').attr('readonly',false);</script>";
	}
	else
	{
		echo "<script>$('#actual').attr('readonly',true);$('#charge').attr('readonly',true);$('#lrno').attr('readonly',true);</script>";
	}
	
	echo '<label>Voucher number <sup><font color="red">*</font></sup></label><input type="text" readonly value="'.$type.'" class="form-control" required />';
}
else if($type=='OWN')
{
	$fetch_own=Qry($conn,"SELECT id,frno,date,fstation,tstation,consignor,consignee FROM freight_form_lr WHERE create_date 
	between '$days_ago' AND '$date2' AND truck_no='$tno' AND branch='$branch' AND done=0 ORDER BY id ASC");
	
	if(!$fetch_own){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error..');$('#lr_type').val('');$('#loadicon').fadeOut('slow');</script>";
		exit();
	}

	echo "<label>OLR Voucher no. <sup><font color='red'>*</font></sup></label>
		<select style='font-size:12px !important' name='lr_frno' id='lr_type_own' class='form-control' required onchange=FetchLRDataFromLR(this.value,'OWN')>";
	
	if(numRows($fetch_own)>0)
	{
		echo "<option style='font-size:12px !important' value=''>--SELECT--</option>";
		
		while($row_own=fetchArray($fetch_own))
		{
			echo "<option style='font-size:12px !important' value='$row_own[frno]_$row_own[id]'>$row_own[frno]: ".convertDate("d/m/y",$row_own['date'])." $row_own[fstation] to $row_own[tstation]. Consignor: $row_own[consignor], Consignee: $row_own[consignee]</option>";
		}
	}
	else
	{
		echo "<option style='font-size:12px !important' value=''>--SELECT--</option>";
		echo "<option style='font-size:12px !important' value='ATLOADING_0'>VEHICLE AT LOADING</option>";
	}
	echo "</select>";
}
else if($type=='MKT')
{
	$fetch_mkt=Qry($conn,"SELECT id,lrdate,bilty_no,frmstn,tostn FROM mkt_bilty WHERE date between '$days_ago' AND '$date2' AND 
	tno='$tno' AND branch='$branch' AND done=0 ORDER BY id ASC");
	
	if(!$fetch_mkt){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error..');$('#lr_type').val('');$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	echo "<label>Market Bilty no. <sup><font color='red'>*</font></sup> <a onclick=document.getElementById('trip_modal_close').click();
	data-toggle='modal' data-target='#BiltyModal'>Create new</a></label>
	<select style='font-size:12px !important' name='lr_frno' id='lr_type_mkt' class='form-control' required onchange=FetchLRDataFromLR(this.value,'MKT')>";
	
	if(numRows($fetch_mkt)>0)
	{
		echo "<option style='font-size:12px !important' value=''>--SELECT--</option>";
		
		while($row_mkt=fetchArray($fetch_mkt))
		{
			echo "<option style='font-size:12px !important' value='$row_mkt[bilty_no]_$row_mkt[id]'>$row_mkt[bilty_no]: ".convertDate("d/m/y",$row_mkt['lrdate']).", $row_mkt[frmstn] to $row_mkt[tostn]</option>";
		}
	}
	else
	{
		echo "<option style='font-size:12px !important' value=''>--SELECT--</option>";
		echo "<option style='font-size:12px !important' value='ATLOADING_0'>VEHICLE AT LOADING</option>";
	}
	echo "</select>";
}

echo "<script>
		$('#trip_sub').attr('disabled', true);
		$('#loadicon').fadeOut('slow');
	</script>";
?>