<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$tno=escapeString($conn,strtoupper($_POST['tno']));

$trip_id=escapeString($conn,strtoupper($_POST['trip_id']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertError("Trip not found..");
	errorLog("Trip id not found. id: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#rtgs_button').attr('disabled',false);</script>";
	exit();
}

require_once("./_check_fix_lane.php");
require_once("./check_cache.php");

if($tno!=$_SESSION['diary'])
{
	errorLog("Invalid Session.",$conn,$page_name,__LINE__);
	Redirect("Invalid Session.","./");
	exit();
}

$comp=Qry($conn,"SELECT comp,happay_active FROM dairy.own_truck WHERE tno='$tno'");

if(!$comp){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#rtgs_button').attr('disabled',false);</script>";
	exit();
}
	
if(numRows($comp)>0)
{
	$row_comp=fetchArray($comp);
	$company=$row_comp['comp'];
}
else
{
	AlertError("Company not found..");
	echo "<script>$('#rtgs_button').attr('disabled',false);</script>";
	exit();
}

$amount=escapeString($conn,$_POST['amount']);
$date=date("Y-m-d");
$narration=escapeString($conn,strtoupper($_POST['narration']));
$pan='';

$check_trip = Qry($conn,"SELECT t.id,t.tno,t.branch,t.cash,t.driver_code,t.trip_no,d.name as driver_name,d.mobile,d.mobile2,
d_ac.acname,d_ac.acno,d_ac.bank,d_ac.ifsc 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
LEFT OUTER JOIN dairy.driver_ac AS d_ac ON d_ac.code=t.driver_code 
WHERE t.id='$trip_id'");

if(!$check_trip){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#rtgs_button').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertError("Trip not found..");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#rtgs_button').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$driver_name = $row_trip['driver_name'];
$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];
$mobile1 = $row_trip['mobile'];
$mobile2 = $row_trip['mobile2'];

$ac_holder = $row_trip['acname'];
$acno = $row_trip['acno'];
$bank = $row_trip['bank'];
$ifsc = $row_trip['ifsc'];

$get_vou_id=GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
	
if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Fetching Vou Id.","./");
	exit();
}
	
$tids = $get_vou_id;

$nrr=$tno."/".$driver_name." : ".$narration;

if($company=='RRPL')
{
	$debit='debit';		
	
	$get_Crn = GetCRN("RRPL-T",$conn);
	if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Fetching Vou Id.","./");
		exit();
	}
	
	$crnnew=$get_Crn;
}
else if($company=='RAMAN_ROADWAYS')
{
	$debit='debit2';
	
	$get_Crn = GetCRN("RR-T",$conn);
	if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Fetching Vou Id.","./");
		exit();
	}
	
	$crnnew=$get_Crn;
}

StartCommit($conn);
$flag = true;

$insert_vou=Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,ac_name,ac_no,bank,ifsc,
pan,dest,timestamp) VALUES ('$branch','$_SESSION[user_code]','$company','$tids','$date','$date','$tno','$driver_name','$amount','NEFT','$ac_holder',
'$acno','$bank','$ifsc','$pan','$narration','$timestamp')");	

if(!$insert_vou){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,`$debit`,timestamp) 
VALUES ('$branch','$tids','$date','$date','$company','Truck_Voucher','$nrr','$amount','$timestamp')");

if(!$update_passbook){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	
	
$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,
tno,type,email,crn,timestamp) VALUES ('$tids','$branch','$company','$amount','$amount','$ac_holder','$acno','$bank','$ifsc',
'$pan','$date','$date','$tno','TRUCK_ADVANCE','','$crnnew','$timestamp')");

if(!$qry_rtgs){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

	
$trans_id_Qry = GetTxnId_eDiary($conn,"RTGS");

if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$trans_id = $trans_id_Qry;
	
$insert_rtgs_diary=Qry($conn,"INSERT INTO dairy.rtgs (trip_id,trans_id,tno,vou_no,amount,driver_code,acname,acno,bank,ifsc,date,
narration,branch,branch_user,timestamp) VALUES ('$trip_id','$trans_id','$tno','$tids','$amount','$driver_code','$ac_holder','$acno','$bank',
'$ifsc','$date','$narration','$branch','$_SESSION[user_code]','$timestamp')");

if(!$insert_rtgs_diary){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_trip=Qry($conn,"UPDATE dairy.trip SET rtgs=rtgs+$amount WHERE id='$trip_id'");

if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$select_amount=Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($select_amount)==0){
	$flag = false;
	errorLog("Driver not found. Code : $driver_code.",$conn,$page_name,__LINE__);
}

$row_amount=fetchArray($select_amount);

$hold_amt=$row_amount['amount_hold']+$amount;
$driver_id=$row_amount['id'];

$insert_book=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,credit,balance,date,branch,
timestamp) VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','RTGS_ADV','$amount','$hold_amt','$date','$branch',
'$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+$amount WHERE id='$driver_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	$dateMsg = date("d/m/y");
	
	if(strlen($mobile2)==10)
	{
		$adv_msg_mobile = $mobile2;
	}
	else if(strlen($mobile1)==10)
	{
		$adv_msg_mobile = $mobile1;
	}
	
	if(strlen($adv_msg_mobile)==10)
	{
		MsgRtgsAdvDiary($adv_msg_mobile,$tno,$branch,$amount,$dateMsg);
	}
	
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error..");
	echo "<script>$('#rtgs_button').attr('disabled',false);</script>";
	exit();
}

echo "<script>
		$('#rtgs_button').attr('disabled',false);
		$('#RtgsForm')[0].reset();
		document.getElementById('hide_rtgs').click();
		TripAdv('#$trip_id');
		LoadDriverBalance('$driver_code');
		$('#loadicon').fadeOut('slow');	
		
		Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: 'Success ! RTGS entry.',
			showConfirmButton: false,
			timer: 2000
		})
	</script>";
exit();
?>