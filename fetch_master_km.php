<?php
require("./connect.php");

$from_id = escapeString($conn,strtoupper($_POST['from_id']));
$to_id = escapeString($conn,strtoupper($_POST['to_id']));
$tno = escapeString($conn,strtoupper($_SESSION['diary']));

// $to_location_set = escapeString($conn,($_POST['to_location_set']));
// $to_stn_id = escapeString($conn,($_POST['to_stn_id']));

// if($to_location_set==1)
// {
	// $from_id_loc=$to_stn_id;
// }
// else
// {
	// $from_id=Qry($conn,"SELECT id FROM station WHERE name='$from'");
	// if(!$from_id)
	// {
		// echo getMySQLError($conn);
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// exit();
	// }	
	
	// $row_from=fetchArray($from_id);
	// $from_id_loc=$row_from['id'];
// }

// $to_id=Qry($conn,"SELECT id FROM station WHERE name='$to'");
// if(!$to_id)
// {
	// echo getMySQLError($conn);
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// exit();
// }

// $row_to=fetchArray($to_id);
// $to_id_loc=$row_to['id'];

if($from_id=='' || $to_id=='')
{
	errorLog("Unable to fetch Location Id.",$conn,$page_name,__LINE__);
	Redirect("Unable to fetch Location Id.","./");
	exit();
}

$ext_trip=Qry($conn,"SELECT from_id,to_id FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1");
if(!$ext_trip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error.","./");
		exit();
	}
	
if(numRows($ext_trip)>0)
{
	$row_ext_trip=fetchArray($ext_trip);
	
	if($row_ext_trip['from_id']==$from_id AND $row_ext_trip['to_id']==$to_id)
	{
		Redirect("Duplicate Trip Found.","./");
		exit();
	}
}

$sel_master=Qry($conn,"SELECT km FROM dairy.master_km WHERE from_loc_id='$from_id' AND to_loc_id='$to_id'");
if(!$sel_master){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error.","./");
		exit();
	}

if(numRows($sel_master)>0)
{
	$row_km=fetchArray($sel_master);
	
	echo "<script>
		$('#trip_km').val('$row_km[km]');
		$('#trip_km').attr('readonly',true);
		$('#set_km').val('0');
		$('#loadicon').hide();
		$('#trip_sub').attr('disabled',false);
	</script>";
}
else
{
	echo "<script>
		$('#trip_km').val('');
		$('#trip_km').attr('readonly',false);
		$('#set_km').val('1');
		$('#loadicon').hide();
		$('#trip_sub').attr('disabled',false);
	</script>";
}

// echo "<script>
		// $('#from_loc_id').val('$from_id_loc');
		// $('#to_loc_id').val('$to_id_loc');
// </script>";
?>