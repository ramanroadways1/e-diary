<?php
require("./connect.php");

echo "<script>$('#add_more_lr_submit').attr('disabled',true);$('#add_more_lr_submit').hide();</script>";
echo "<script>$('#val_btn_add_more').attr('disabled',true);</script>";

$tno=escapeString($conn,strtoupper($_SESSION['diary']));

$error1_flag=false;

if(empty($_POST['bilty_type'])){
	$error1_flag=true;
	AlertError('Bilty type not found !');
}

if(empty($_POST['vou_no'])){
	$error1_flag=true;
	AlertError('LR not found !');
}

if(empty($_POST['act_wt'])){
	$error1_flag=true;
	AlertError('Actual weight not found !');
}

if(empty($_POST['chrg_wt'])){
	$error1_flag=true;
	AlertError('Charge weight not found !');
}

if(empty($_POST['to_loc_id'])){
	$error1_flag=true;
	AlertError('Destination not found !');
}

if(empty($_POST['to_loc'])){
	$error1_flag=true;
	AlertError('Destination location not found !');
}

if(empty($_POST['trip_id'])){
	$error1_flag=true;
	AlertError('Trip not found !');
}

if($error1_flag)
{
	echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');;</script>";
	exit();
}

$vou_no=escapeString($conn,explode('_', $_POST['vou_no'])[0]);
$vou_id=escapeString($conn,explode('_', $_POST['vou_no'])[1]);
$con1_id=escapeString($conn,explode('_', $_POST['vou_no'])[2]);
$con2_id=escapeString($conn,explode('_', $_POST['vou_no'])[3]);

$location_string = escapeString($conn,$_POST['to_loc']);
$location_string = explode('#',$location_string);

$to_location = $location_string[0];
$to_id = $location_string[1];

if($to_id!=$_POST['to_loc_id']){
	AlertError('Destination not verified !');
	echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

$trip_id = escapeString($conn,($_POST['trip_id']));

$check_trip = Qry($conn,"SELECT t.from_station,t.from_id,t.from_poi,t.from_pincode,t.consignor_id,t.addr_book_id_consignor,t.to_id,
t.consignee_id,t.dsl_consumption,t.addr_book_id_consignee,s._lat,s._long,s.pincode 
FROM dairy.trip AS t 
LEFT JOIN station AS s ON s.id=t.from_id 
WHERE t.id='$trip_id'");
	
if(!$check_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error.","./");
	exit();
}

if(numRows($check_trip)==0)
{
	AlertError('Trip not found !');
	echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

$row_trip = fetchArray($check_trip);

$from_poi = $row_trip['from_poi'];
$from_pincode = $row_trip['from_pincode'];

if(strlen($from_poi)<4)
{
	AlertError('Trip start POI not found !');
	echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

if(strlen($from_pincode)!=6)
{
	AlertError('Invalid FROM pincode !');
	echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

$from_location = $row_trip['from_station'];
$from_id = $row_trip['from_id'];
$consignor_id = $row_trip['consignor_id'];
$addr_book_id_consignor = $row_trip['addr_book_id_consignor'];
$consignee_id = $row_trip['consignee_id'];
$addr_book_id_consignee = $row_trip['addr_book_id_consignee'];

$to_id_current = $row_trip['to_id'];
$dsl_consumption = $row_trip['dsl_consumption'];

$to_loc_lat_lng = Qry($conn,"SELECT _lat,_long,pincode FROM station WHERE id='$to_id'");
	
if(!$to_loc_lat_lng){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error.","./");
	exit();
}

$row_to_loc = fetchArray($to_loc_lat_lng);

$to_poi = $row_to_loc['_lat'].",".$row_to_loc['_long'];
$to_pincode = $row_to_loc['pincode'];

$total_act_wt = escapeString($conn,($_POST['total_act_wt']));  // total actual weight after add more LR
$bilty_type = escapeString($conn,($_POST['bilty_type']));  // market or own
$act_wt = escapeString($conn,($_POST['act_wt']));  // act weight
$chrg_wt = escapeString($conn,($_POST['chrg_wt']));  // chrg weight

if(empty($total_act_wt)){
	AlertError("Weight not found !");	
	echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";exit();
}

$avg_value="NA";
$by_pass_route = "NO";

if($to_id_current==$to_id)
{
	if($dsl_consumption==0)
	{
		AlertError("Fuel Consumption Value not found !");
		echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
			
		// $get_avg = GetAvgValue($conn,$from_id,$to_id,$from_location,$to_location,$tno,"OWN",$total_act_wt);
		
		// if($get_avg[0]==false)
		// {
			// AlertError($get_avg[1]);
			// echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
			// exit();
		// }
		
		// $avg_value = $get_avg[2];
		// $by_pass_route = $get_avg[3];
	}
	// else
	// {
		// $avg_value = 0;
	// }
	
	$avg_value = 0;
}
else
{
	$get_avg = GetAvgValue($conn,$from_id,$to_id,$from_location,$to_location,$tno,"OWN",$total_act_wt);
		
	if($get_avg[0]==false)
	{
		AlertError($get_avg[1]);
		echo "<script>$('#val_btn_add_more').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
		
	$avg_value = $get_avg[2];
	$by_pass_route = $get_avg[3];
}

if($to_id!=$to_id_current)
{
	if(strpos($vou_no,'OLR') !== false)
	{
		$chk_addr_book_to = Qry($conn,"SELECT id,pincode,_lat,_long FROM address_book_consignee WHERE consignee='$con2_id' AND to_id='$to_id'");
				
		if(!$chk_addr_book_to){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			AlertError("Error while processing request !");
			exit();
		}
				
		if(numRows($chk_addr_book_to)==0)
		{
			AlertError("Unloading point not found ! Please add unloading point.");
			exit();
		}
		
		$row_addr_book_to = fetchArray($chk_addr_book_to);
		
		$to_pincode = $row_addr_book_to['pincode'];
		$to_poi = $row_addr_book_to['_lat'].",".$row_addr_book_to['_long'];
	}
}

echo "<script>
		if($('#bilty_type_add_more_lrs').val()=='MARKET'){
			$('#market_div_input').attr('disabled',true);
		}
		else{
			$('#own_div_input').attr('disabled',true);
		}
	
		$('#bilty_type_add_more_lrs').attr('disabled',true);
		$('#to_loc_for_add_more').attr('disabled',true);
		$('#pincode_for_add_new').val('$to_pincode');
	</script>";
	
$_SESSION['add_more_bilty_type']=$bilty_type;	
$_SESSION['add_more_vou_no']=$vou_no;	
$_SESSION['add_more_vou_id']=$vou_id;	
$_SESSION['add_more_destination_id']=$to_id;	
$_SESSION['add_more_trip_id']=$trip_id;	
$_SESSION['add_more_end_point']=$to_poi;	
$_SESSION['add_more_lr_to_pincode']=$to_pincode;	
$_SESSION['add_more_trip_km']="0";	
?>
<div id="km_session_html"></div>

<script type="text/javascript">
    function myfunction(){
        var map;
        var start = new google.maps.LatLng(<?php echo $from_poi; ?>);
        var end = new google.maps.LatLng(<?php echo $to_poi; ?>);
        var option ={
            zoom : 15,
            center : start,
			disableDefaultUI: true,
        };
        map = new google.maps.Map(document.getElementById('map_add_more_lrs'),option);
        var display = new google.maps.DirectionsRenderer();
        var services = new google.maps.DirectionsService();
        display.setMap(map);
            var request ={
                origin : start,
                destination:end,
                travelMode: 'DRIVING'
            };
            services.route(request,function(result,status){
                if(status =='OK'){
                    display.setDirections(result);
					 var directionsData = result.routes[0].legs[0]; // Get data about the mapped route
					if (!directionsData) {
						// Swal.fire({
							// icon: 'error',
							// title: 'Invalid location.',
							// html: '<font size=\'2\' color=\'red\'>Error : Request failed !!</font>',
						// })
						alert('Error : Request failed !!');
						$('#loadicon').fadeOut("slow");
						$('#add_more_lr_submit').attr('disabled',true);
						$('#add_more_lr_submit').hide();
						$('#val_btn_add_more').attr('disabled',false);
					}
					else {
					  var distance1 = directionsData.distance.text;
					 var distance_no = distance1.replace(',','');	
					var distance_no = parseFloat(distance_no.match(/[\d\.]+/));
					  var duration1 =  directionsData.duration.text;
					  var encodedPolyLine = result.routes[0].overview_polyline;
					  
					  $('#trip_km_for_add_new').val(distance_no);
					  $('#add_more_lr_trip_encodedPolyLine').val(encodedPolyLine);
					  $('#km_google_add_more').html(distance_no+" KM");
					  $('#duration_google_add_more').html(duration1);
					  $('#loadicon').fadeOut("slow");
					  $('#add_more_lr_submit').attr('disabled',false);
					  $('#add_more_lr_submit').show();
					  $('#val_btn_add_more').attr('disabled',true);
						 jQuery.ajax({url: "./_create_session_var_for_km_add_more_lr.php",
						 data:'distance=' + distance_no + '&to_id=' + '<?php echo $to_id; ?>' + '&trip_id=' + '<?php echo $trip_id; ?>' + '&avg_value=' + '<?php echo $avg_value; ?>',
						 type: "POST",
						 success: function(data){
							$("#km_session_html").html(data);
						},
						error: function() {}
						});
				}
                }
				else{
					alert('Error: '+status);
					$('#loadicon').fadeOut("slow");
					$('#add_more_lr_submit').attr('disabled',true);
					$('#add_more_lr_submit').hide();
					$('#val_btn_add_more').attr('disabled',false);
				}
            });
    }
	myfunction();
</script>

<?php
echo "<script>
	$('#by_pass_route_add_more_lr').val('$by_pass_route');
</script>";
?>
