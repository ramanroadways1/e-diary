<?php
require("./connect.php");

$tno = $_SESSION['diary'];
$otp = escapeString($conn,$_POST['otp']);
$mobile = escapeString($conn,$_POST['mobile']);
$timestamp=date('Y-m-d H:i:s');

echo "<script>
	$('#driver_verify_otp').attr('readonly',true);
	$('#verify_otp_btn_driver').attr('disabled',true);
</script>";

if(strlen($mobile)!=10){
	AlertError("Invalid mobile number entered !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if(strlen($otp)!=6 AND $_SESSION['driver_verify_otp']!='NA')
{
	AlertError("Please check OTP !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#driver_verify_otp').attr('readonly',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if(empty($tno)){
	AlertError("Vehicle not found !!");
	echo "<script>window.location.href='./logout.php';</script>";
	exit();
}

if(!isset($_SESSION['driver_verify_otp'])){
	AlertError("OTP not found !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if(!isset($_SESSION['driver_verify_otp_by_pass'])){
	AlertError("Verification status not found !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if(!isset($_SESSION['driver_verify_driver_id'])){
	AlertError("Driver not found !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}
if(empty($_SESSION['driver_verify_driver_id']) || $_SESSION['driver_verify_driver_id']==0)
{
	AlertError("Driver-id not found !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if($_SESSION['driver_verify_otp']!='NA' AND $_SESSION['driver_verify_otp']!=$otp)
{
	AlertError("OTP verification failed !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#driver_verify_otp').attr('readonly',false);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

$driver_id = escapeString($conn,$_SESSION['driver_verify_driver_id']);

$fetch_driver_data = Qry($conn,"SELECT d.name,d.active,d.mobile,d.code
FROM dairy.driver AS d 
WHERE d.id='$driver_id'");

if(!$fetch_driver_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if(numRows($fetch_driver_data)==0)
{
	AlertError("Driver details not found !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

$update_verification_timestamp = Qry($conn,"UPDATE dairy.driver SET last_verify='$timestamp' WHERE id='$driver_id'");

if(!$update_verification_timestamp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

$get_model = Qry($conn,"SELECT o.model,o.driver_code,o.diesel_tank_cap,d.name as driver_name 
FROM dairy.own_truck AS o 
LEFT OUTER JOIN dairy.driver AS d ON d.code = o.driver_code 
WHERE o.tno='$tno'");

if(!$get_model){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if(numRows($get_model)==0)
{
	AlertError("Vehicle not found !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

$row_model = fetchArray($get_model);

$tank_cap = $row_model['diesel_tank_cap'];

// if($tank_cap==0)
// {
	// AlertError("Diesel tank capacity not updated !<br>Contact truck-division dpt.");
	// echo "<script>
		// $('#verify_otp_btn_driver').attr('disabled',false);
		// $('#add_driver_button').attr('disabled',true);
		// $('#add_driver_button').hide();
	// </script>";
	// exit();
// }

$row_driver = fetchArray($fetch_driver_data);

if($row_model['driver_code']!="0" AND $row_model['driver_code']!='')
{
	AlertError("<span style=\'font-size:10px;color:maroon\'>$row_model[driver_name].</span><br>already active on this vehicle.");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if($row_driver['active']!="0")
{
	AlertError("Driver active on another vehicle !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if($row_driver['mobile']!=$mobile)
{
	AlertError("Mobile number not verified !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

if($row_model['model']=="0" || empty($row_model['model']))
{
	AlertError("$tno: Model not found !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}

$model = escapeString($conn,$row_model['model']);
$driver_code = escapeString($conn,$row_driver['code']);
$driver_name = escapeString($conn,$row_driver['name']);

// if($_SESSION['driver_verify_otp_by_pass']!='YES')
// {
	// if($row_driver['mobile']!=$mobile)
	// {
		// AlertError("Mobile number not verified !");
		// echo "<script>
			// $('#verify_otp_btn_driver').attr('disabled',false);
			// $('#add_driver_button').attr('disabled',true);
			// $('#add_driver_button').hide();
		// </script>";
		// exit();
	// }
// }

$date_now=date("Y-m-d");
// $date_prev=date('Y-m-d', strtotime("$row_date_rule[days] days", strtotime($date_now)));
$sal_last_sel=date('Y-m-d', strtotime("-7 days", strtotime($date_now)));
$on_duty_prev=date('Y-m-d', strtotime("-7 days", strtotime($date_now)));
$on_duty_prev2=date('Y-m-d', strtotime("-1 days", strtotime($date_now)));

$down_check=Qry($conn,"SELECT tno,down,status,amount_hold FROM dairy.driver_up WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$down_check){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error !");
	echo "<script>
		$('#verify_otp_btn_driver').attr('disabled',false);
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
	</script>";
	exit();
}
			
if(numRows($down_check)>0)
{
	$row_down = fetchArray($down_check);
		
	if($row_down['down']==0)
	{
		errorLog("DRIVER NOT DOWN PROPERLY. Code : $driver_code. Active On : $row_down[tno].",$conn,$page_name,__LINE__);
		AlertError("Driver not down completely !");
		echo "<script>
			$('#verify_otp_btn_driver').attr('disabled',false);
			$('#add_driver_button').attr('disabled',true);
			$('#add_driver_button').hide();
		</script>";
		exit();
	}
	
	$down_type = $row_down['status'];
		
	if($down_type=='2')
	{
		$qry_left_rsn=Qry($conn,"SELECT reason,branch,timestamp FROM dairy.driver_left WHERE driver_code='$driver_code' ORDER BY id 
		DESC LIMIT 1");
		
		if(!$qry_left_rsn){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			AlertError("Error !");
			echo "<script>
				$('#verify_otp_btn_driver').attr('disabled',false);
				$('#add_driver_button').attr('disabled',true);
				$('#add_driver_button').hide();
			</script>";
			exit();
		}
	
		if(numRows($qry_left_rsn)>0)
		{
			$row_left_rsn=fetchArray($qry_left_rsn);
			$left_reason=$row_left_rsn['branch'].", TimeStamp : ".$row_left_rsn['timestamp'].", Reason : ".$row_left_rsn['Reason'];
		}
		else
		{
			$left_reason="";
		}
	}
	else
	{
		$left_reason="";
	}
		
	$opening_balance = $row_down['amount_hold'];
	$set_box="readonly";
	$salary_date_box="readonly";
}
else
{
	$down_type="NEW";
	$opening_balance=0;
	$set_box="readonly";
	$left_reason="";
	$salary_date_box="";
}
	
echo "<script>
function DriverOnDuty(elem)
{
	if(elem=='BACK')
	{
		$('#driver_on_duty_date').val('');
		$('#driver_on_duty_date').attr('readonly',false);
		$('#driver_on_duty_date').attr('max','$on_duty_prev2');
		
		$('#da_diff_div').show();
		$('#da_diff_sel').val('');
		$('#da_diff_sel').attr('required',true);
		$('#diff_da_from').attr('required',true);
		$('#diff_da_to').attr('required',true);
		
		$('#salary_diff_div').show();
		$('#salary_diff_sel').val('');
		$('#salary_diff_sel').attr('required',true);
		$('#diff_sal_from').attr('required',true);
		$('#diff_sal_to').attr('required',true);
	}
	else
	{
		$('#driver_on_duty_date').val('".date("Y-m-d")."');
		$('#driver_on_duty_date').attr('readonly',true);
		$('#driver_on_duty_date').attr('max','".date("Y-m-d")."');
		
		$('#da_diff_div').hide();
		$('#da_diff_sel').val('');
		$('#da_diff_sel').attr('required',false);
		$('#diff_da_from').attr('required',false);
		$('#diff_da_to').attr('required',false);
		
		$('#salary_diff_div').hide();
		$('#salary_diff_sel').val('');
		$('#salary_diff_sel').attr('required',false);
		$('#diff_sal_from').attr('required',false);
		$('#diff_sal_to').attr('required',false);
	}
}
</script>

<input type='hidden' value='$left_reason' name='left_reason'>
	
<div class='row'>	

	<div class='form-group col-md-5'>
		<label><font color='green'>Driver name</font></label>
		<input style='font-size:12px' class='form-control' value='$driver_name' readonly />
	</div>
	
	<div class='form-group col-md-4'>
		<label>Opening Bal<font color='red'> <sup>*</sup></font></label>
		<input class='form-control' type='number' id='opening_balance' $set_box name='opening_balance' value='$opening_balance' required />
	</div>
	
	<div class='form-group col-md-3'>
		<label>Select Salary<font color='red'> <sup>*</sup></font></label>";
	
	$qry_sal=Qry($conn,"SELECT salary,type FROM dairy.salary_master WHERE model='$model'");
	
	if(!$qry_sal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertError("Error !");
		echo "<script>
			$('#verify_otp_btn_driver').attr('disabled',false);
			$('#add_driver_button').attr('disabled',true);
			$('#add_driver_button').hide();
		</script>";
		exit();
	}
			
	if(numRows($qry_sal)==0)
	{
		echo "<script>
			alert('Model not found in salary-database..');
			window.location.href='./';
		</script>";
		exit();
	}
	
	echo "<select style='font-size:12px' class='form-control' name='salary_master' id='salary_master' required>
		<option style='font-size:12px' value=''>--select salary--</option>";
			
	while($row_sal=fetchArray($qry_sal))
	{
		if($row_sal['type']=='1')
		{
			echo "<option style='font-size:12px' value='$row_sal[salary]_1'>$row_sal[salary] / trip</option>";	
		}
		else
		{
			echo "<option style='font-size:12px' value='$row_sal[salary]_0'>$row_sal[salary] / month</option>";	
		}
	}
	echo "</select>";	
	
	echo "</div>
	
	<div class='form-group col-md-4'>
		<label>Salary Pending From <font color='red'> <sup>*</sup></font></label>
		<input class='form-control' $salary_date_box min='$sal_last_sel' max='$date_now' type='date' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' 
		id='salary_date_pending' name='salary_date_pending' required />
	</div>
	
	<div class='form-group col-md-4'>
		<label>On Duty on <font color='red'> <sup>*</sup></font></label>
		<select style='font-size:12px' name='on_duty_on' onchange='DriverOnDuty(this.value)' required class='form-control'>
			<option style='font-size:12px' value=''>--SELECT--</option>
			<option style='font-size:12px' value='TODAY'>TODAY</option>
			<option style='font-size:12px' disabled value='BACK'>BACK DATED</option>
		</select>
	</div>
	
	<div class='form-group col-md-4'>
		<label><b>On Duty on </b><font color='red'> <sup>*</sup></font></label>
		<input class='form-control' min='$on_duty_prev' max='".date("Y-m-d")."' type='date' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' 
		id='driver_on_duty_date' readonly name='driver_on_duty_date' required />
	</div>
</div>

	<script>
		// hide both btns send otp and verify otp
		$('#verify_otp_btn_driver').attr('disabled',true);
		$('#send_otp_btn_driver').attr('disabled',true);
		$('#verify_otp_btn_driver').hide();
		$('#send_otp_btn_driver').hide();
		
		$('.verified_sign_driver').show(); // show verified sign
		
		// unlock assign btn
		$('#add_driver_button').attr('disabled',false);
		$('#add_driver_button').show();
	</script>";
	
AlertRightCornerSuccess("Successfully verified !");	
exit();

// <div class='form-group col-md-12'>
		// <label style='color:maroon'>Diesel Qty available in tank <span style='font-size:12px'>टैंक में उपलब्ध डीजल मात्रा दर्ज करे </span><font color='red'> <sup>*</sup></font></label>
	// </div>
	
	// <div class='form-group col-md-4'>
		// <input class='form-control' placeholder='Diesel Qty Available' min='10' max='$tank_cap' type='number' name='diesel_left' required />
	// </div>

/*
<div id='da_diff_div' class='col-md-12' style='display:none'>
	
	<div class='row'>
	
	<div class='form-group col-md-4'>
		<label><b>Pay DA Diff. </b><font color='red'> <sup>*</sup></font></label>
		<select name='da_diff' id='da_diff_sel' onchange='PayDaDiff(this.value)' required class='form-control'>
			<option value=''>--SELECT--</option>
			<option value='0'>NO</option>
			<option disabled value='1'>YES</option>
		</select>
	</div>
	
	<div id='diff_da_dates'>
		<div class='form-group col-md-4'>
			<label><b>DA From Date </b><font color='red'> <sup>*</sup></font></label>
			<input class='form-control' min='$on_duty_prev' max='$on_duty_prev2' type='date' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' 
			id='diff_da_from' readonly name='diff_da_from' required />
		</div>
		
		<div class='form-group col-md-4'>
			<label><b>DA To Date </b><font color='red'> <sup>*</sup></font></label>
			<input class='form-control' min='$on_duty_prev' max='$on_duty_prev2' type='date' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' 
			id='diff_da_to' readonly name='diff_da_to' required />
		</div>
	
	</div> 
	
	</div>
	
	</div>
	
	<div class='col-md-12' id='salary_diff_div' style='display:none'>

	<div class='row'>
	
	<div class='form-group col-md-4'>
		<label><b>Pay SALARY Diff. </b><font color='red'> <sup>*</sup></font></label>
		<select name='salary_diff' id='salary_diff_sel' onchange='PaySalaryDiff(this.value)' required class='form-control'>
			<option value=''>--SELECT--</option>
			<option value='0'>NO</option>
			<option disabled value='1'>YES</option>
		</select>
	</div>
	
	<div id='diff_sal_dates'>
		<div class='form-group col-md-4'>
			<label><b>SALARY From Date </b><font color='red'> <sup>*</sup></font></label>
			<input class='form-control' min='$on_duty_prev' max='$on_duty_prev2' type='date' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' 
			id='diff_sal_from' readonly name='diff_sal_from' required />
		</div>
		
		<div class='form-group col-md-4'>
			<label><b>SALARY To Date </b><font color='red'> <sup>*</sup></font></label>
			<input class='form-control' min='$on_duty_prev' max='$on_duty_prev2' type='date' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' 
			id='diff_sal_to' readonly name='diff_sal_to' required />
		</div>
	</div>
	
	</div>
	
	</div>
	
<script>
		function PayDaDiff(elem)
		{
			if(elem=='1')
			{
				$('#diff_da_dates').show();
				$('#diff_da_from').attr('required',true);
				$('#diff_da_to').attr('required',true);
				
				$('#diff_da_from').attr('readonly',false);
				$('#diff_da_to').attr('readonly',false);
			}
			else
			{
				$('#diff_da_dates').hide();
				$('#diff_da_from').attr('required',false);
				$('#diff_da_to').attr('required',false);
				
				$('#diff_da_from').attr('readonly',true);
				$('#diff_da_to').attr('readonly',true);
			}
		}
		
		function PaySalaryDiff(elem)
		{
			if(elem=='1')
			{
				$('#diff_sal_dates').show();
				$('#diff_sal_from').attr('required',true);
				$('#diff_sal_to').attr('required',true);
				
				$('#diff_sal_from').attr('readonly',false);
				$('#diff_sal_to').attr('readonly',false);
			}
			else
			{
				$('#diff_sal_dates').hide();
				$('#diff_sal_from').attr('required',false);
				$('#diff_sal_to').attr('required',false);
				
				$('#diff_sal_from').attr('readonly',true);
				$('#diff_sal_to').attr('readonly',true);
			}
		}
	</script>
*/	
?>