<?php
require_once './connect.php';

$tno_active=escapeString($conn,$_SESSION['diary']);
$trip_id=escapeString($conn,($_POST['trip_id']));

if($trip_id=='' || $trip_id==0)
{
	AlertError("Trip not found !!");
	echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
	exit();	
}

$get_active_driver = Qry($conn,"SELECT driver_code FROM dairy.trip WHERE tno='$tno_active'");

if(!$get_active_driver){
	AlertError("Error..");
	echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_active_driver)==0)
{
	AlertError("Invalid request !!");
	echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
	exit();	
}

$row_active = fetchArray($get_active_driver);
$driver_code = $row_active['driver_code'];

if($driver_code=='' || $driver_code==0)
{
	AlertError("Active driver not found !!");
	echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
	exit();	
}

$chk_trip_driver_and_veh=Qry($conn,"SELECT tno,driver_code FROM dairy.trip WHERE id='$trip_id'");

	if(!$chk_trip_driver_and_veh){
		AlertError("Error..");
		echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($chk_trip_driver_and_veh)==0)
	{
		$chk_trip_driver_and_veh_old_trips = Qry($conn,"SELECT tno,driver_code FROM dairy.trip_final WHERE 
		trip_id='$trip_id'");

		if(!$chk_trip_driver_and_veh_old_trips){
			AlertError("Error..");
			echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		if(numRows($chk_trip_driver_and_veh_old_trips)==0)
		{
			AlertError("Trip not found !!");
			echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
			exit();	
		}
		
		$row_trip = fetchArray($chk_trip_driver_and_veh_old_trips);
	}
	else
	{
		$row_trip = fetchArray($chk_trip_driver_and_veh);
	}
	
	if($row_trip['tno']!=$tno_active)
	{
		AlertError("LR belongs to vehicle: $row_trip[tno] !");
		echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
		exit();	
	}
	
	if($row_trip['driver_code']!=$driver_code)
	{
		$get_driver_name = Qry($conn,"SELECT name FROM dairy.driver WHERE code='$row_trip[driver_code]'");

		if(!$get_driver_name){
			AlertError("Error..");
			echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		$row_d_name = fetchArray($get_driver_name);
		
		AlertError("LR belongs to driver: $row_d_name[name] !");
		echo "<script>$('#freight_lr_vou_type').val('');FreightLRSelection('');$('#naame_button').attr('disabled',true);</script>";
		exit();	
	}
	
echo "<script>$('#loadicon').fadeOut('slow');$('#naame_button').attr('disabled',false);</script>";
exit();	
?>