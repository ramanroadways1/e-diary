<?php
require("./connect.php");

if(!isset($_SESSION['diary']))
{
	echo "<script type='text/javascript'>
			window.location.href='./login.php';
		</script>";
	exit();
}

$tno = escapeString($conn,strtoupper($_SESSION['diary']));

$qry = Qry($conn,"SELECT t.tno,t.trishul_card,t.trishul_mapped,t.model,t.diesel_left_fix,t.driver_code,t.sal_per_day,t.superv_id,
t.supervisor_qty_approval_timestamp,t.force_update_diesel_qty,t.diesel_tank_cap,t.diesel_left,u.title as supervisor_name 
FROM dairy.own_truck AS t 
LEFT OUTER JOIN dairy.user AS u ON u.id=t.superv_id 
WHERE t.tno='$tno'");

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
		
if(numRows($qry)==0)
{
	if(isset($_SESSION['diary']))
	{
		unset($_SESSION['diary']);
	}
	
	Redirect("Invalid vehicle number entered.","login.php");
	exit();
}
else if(numRows($qry)>1)
{
	if(isset($_SESSION['diary']))
	{
		unset($_SESSION['diary']);
	}

	errorLog("More than one Truck No Found. Truck No: $tno.",$conn,$page_name,__LINE__);
	Redirect("Something went wrong !","login.php");
	exit();
}


$row_truck=fetchArray($qry);
  
$supervisor_qty_approval_timestamp = $row_truck['supervisor_qty_approval_timestamp'];
$diesel_tank_cap = $row_truck['diesel_tank_cap'];
$force_update_diesel_qty = $row_truck['force_update_diesel_qty'];
$model=$row_truck['model'];
$driver_code=$row_truck['driver_code'];
$sal_per_day=$row_truck['sal_per_day'];
$superv_id=$row_truck['superv_id'];
$supervisor_name=$row_truck['supervisor_name'];
$trishul_card=$row_truck['trishul_card'];
$trishul_mapped=$row_truck['trishul_mapped'];
 
if($model=='' || $model=='0')
{
	echo "<center><br /><br /><h4 style='color:maroon;font-family:Verdana'>$tno's Model not updated. Contact truck division department.</h4>
	<a href='../'><button type='button' class='btn btn-danger'>Go back</button></a>
	</center>";
	exit();
}

$driver_code_value='0';	
$driver_lic_no='';
$driver_name='';
$driver_mobile='';
$is_dummy_driver = "0";
$hide_trip_and_hisab_btns = "0";
$lock_trip_btn = "0";
$rating_average = 0;
	
if($driver_code==0 || $driver_code=='')
{
	$hide_trip_and_hisab_btns="1";
}
else
{
	$qry_d = Qry($conn,"SELECT name,mobile,lic,dummy_driver,driver_blacklist,driver_rating FROM dairy.driver WHERE code='$driver_code'");
	
	if(!$qry_d){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
		
	if(numRows($qry_d)>0)
	{
		$row_d = fetchArray($qry_d);
		
		if($row_d['driver_blacklist']=="1")
		{
			echo "<br><br><center><h4 style='font-family:Verdana;color:maroon'>Driver: $row_d[name] is blacklised !</h4></center>";
			exit();
		}
		
		$rating_average = $row_d['driver_rating'];
	}
	else
	{
		echo "<br><br><center><h4 style='font-family:Verdana;color:maroon'>Error : Unable to fetch driver !</h4></center>";
		exit();
	}
		
	$driver_name = $row_d['name'];
	$driver_code_value = $driver_code;
	$driver_mobile = $row_d['mobile'];
	$driver_lic_no = $row_d['lic'];
		
	if($row_d['dummy_driver']=="1")
	{
		$is_dummy_driver = "1";
	}
}

$trip_start_date = Qry($conn,"SELECT trip_no,date(date) as start_date FROM dairy.trip WHERE tno='$tno' ORDER BY id ASC LIMIT 1");

if(!$trip_start_date){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
		
if(numRows($trip_start_date)>0)
{
	$row_start = fetchArray($trip_start_date);
	$datediff = strtotime(date("Y-m-d")) - strtotime($row_start['start_date']);
	$trip_days = round($datediff / (60 * 60 * 24));
	$trip_no_db=$row_start['trip_no'];
	$tripStart_date1 = $row_start['start_date'];
	$is_trip_created="1";
}
else
{
	$trip_days=0;
	$trip_no_db="0";
	$tripStart_date1 = date("Y-m-d");
	$is_trip_created="0";
}

$get_trip_lock_days = Qry($conn,"SELECT no_of_days FROM dairy._trip_lock_func ORDER BY id DESC LIMIT 1");
	
if(!$get_trip_lock_days){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_trip_lock_days = fetchArray($get_trip_lock_days);

$trip_days_duration = $row_trip_lock_days['no_of_days'];

if($trip_days>$trip_days_duration)
{
	$fetch_hisab_branches = Qry($conn,"SELECT branch FROM dairy.hisab_branches WHERE branch='$branch' AND lock_trip='1'");
	
	if(!$fetch_hisab_branches){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}

	if(numRows($fetch_hisab_branches)>0)
	{
		$fetch_lock = Qry($conn,"SELECT timestamp FROM dairy.hisab_branches_lock_allow WHERE trip_no='$trip_no_db' ORDER BY id DESC LIMIT 1");
		if(!$fetch_lock){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		$row_lock = fetchArray($fetch_lock);
		
		if(numRows($fetch_lock)==0){
			$lock_trip_btn="1";
		}
		else{
			
			$hisab_hours =  round((strtotime(date("Y-m-d H:i:s")) - strtotime($row_lock['timestamp']))/3600, 1);
			
			if($hisab_hours>=24){
				$lock_trip_btn="1";
			}
		}
	}
}

$date = date("Y-m-d");

// $rateResult = Qry($conn,"SELECT ratingNumber FROM dairy.driver_rating WHERE userId='$driver_code_value'");

// if(!$rateResult){
	// getMySQLError($conn);
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// exit();
// }
	
	// $ratingNumber = 0;
	// $count_d = 0;
	// $fiveStarRating = 0;
	// $fourStarRating = 0;
	// $threeStarRating = 0;
	// $twoStarRating = 0;
	// $oneStarRating = 0;
	
	// while($rate = fetchArray($rateResult)) {
		// $ratingNumber+= $rate['ratingNumber'];
		// $count_d += 1;
		// if($rate['ratingNumber'] == 5) {
			// $fiveStarRating +=1;
		// } else if($rate['ratingNumber'] == 4) {
			// $fourStarRating +=1;
		// } else if($rate['ratingNumber'] == 3) {
			// $threeStarRating +=1;
		// } else if($rate['ratingNumber'] == 2) {
			// $twoStarRating +=1;
		// } else if($rate['ratingNumber'] == 1) {
			// $oneStarRating +=1;
		// }
	// }
	
// $average = 0;

// if($ratingNumber && $count_d) {
	// $average = $ratingNumber/$count_d;
// }	
	
$sel_date_rule=Qry($conn,"SELECT days FROM dairy.date_rule WHERE type='2'"); // Bilty fetch days ago

if(!$sel_date_rule){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($sel_date_rule)==0)
{
	Redirect("Date Rule not updated for LR Selection.","../");
	exit();
}

$row_date_rule=fetchArray($sel_date_rule);
$bilty_days_ago = date('Y-m-d', strtotime("$row_date_rule[days] days", strtotime($date)));	

include("_header.php"); 
include("code_disabled_right_click.php");

?>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div id="result"></div>

<script>
$(function() {
		$("#truck_no_hisab_search").autocomplete({
		source: './autofill/own_tno.php',
		// appendTo: '#truck_no_hisab_search',
		select: function (event, ui) { 
            $('#truck_no_hisab_search').val(ui.item.value);   
            return false;
		},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<?php
$trip_chk_basic = Qry($conn,"SELECT t.id,t.to_station,t.to_id,s.pincode,s._lat,s._long 
FROM dairy.trip AS t 
LEFT JOIN station AS s ON s.id=t.to_id
WHERE t.tno='$tno' ORDER by t.id desc LIMIT 1");

if(!$trip_chk_basic){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($trip_chk_basic)>0)
{
	$row_trip_basic=fetchArray($trip_chk_basic);
	$trip_basic_id=$row_trip_basic['id'];
	
	$to_stn=$row_trip_basic['to_station'];
	$to_location_set=1;
	$to_stn_id=$row_trip_basic['to_id'];
	$to_stn_poi = $row_trip_basic['_lat'].",".$row_trip_basic['_long'];
	$to_stn_pincode1 = $row_trip_basic['pincode'];
	
	if($to_stn_id=='' || $to_stn=='')
	{
		errorLog("Unable to fetch Last Location : Running Trip. TruckNo : $tno.",$conn,$page_name,__LINE__);
		echo "<br><br><center><h3 style='font-family:Verdana'>Unable to fetch Last Location : Running Trip.</h3></center>";
		exit();
	}
}
else
{
	$trip_basic_id="0";
	
	$chk_old_trip_location=Qry($conn,"SELECT t.to_station,t.to_id,s.pincode,s._lat,s._long 
	FROM dairy.trip_final as t 
	LEFT JOIN station AS s ON s.id=t.to_id
	WHERE t.tno='$tno' ORDER by t.id desc LIMIT 1");
	
	if(!$chk_old_trip_location){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($chk_old_trip_location)==0)
	{
		$to_stn='';
		$to_location_set='';
		$to_stn_id='';
		$to_stn_poi='';
		$to_stn_pincode1='';
	}
	else
	{
		$row_old_trip=fetchArray($chk_old_trip_location);
		
		$to_stn = $row_old_trip['to_station'];
		$to_stn_id = $row_old_trip['to_id'];
		$to_stn_poi = $row_old_trip['_lat'].",".$row_old_trip['_long'];
		$to_stn_pincode1 = $row_old_trip['pincode'];
		$to_location_set=1;
		
		if($to_stn_id=='' || $to_stn=='')
		{
			errorLog("Unable to fetch Last Location : Old Trip. TruckNo : $tno.",$conn,$page_name,__LINE__);
			echo "<br><br><center><h3 style='font-family:Verdana'>Unable to fetch Last Location : Old Trip.</h3></center>";
			exit();
		}
	}		
}
?>

<script>
function TripAdv(id)
{
	$('#loadicon').show();
	<?php
	if(!isMobile())
	{
	?>
		$('.main').attr('class','form-group col-md-12 main');
		$(id).attr('class','form-group col-md-12 div1 main');
	<?php
	}
	else
	{
	?>
		$('.main').attr('class','col-md-12 main');
		$(id).attr('class','col-md-12 div1 main');
	<?php	
	}
	?>
	
	 jQuery.ajax({
		url: "fetch_trip_data.php",
		data: 'id=' + id.substr(1) + '&lock_trip_btn=' + '<?php echo $lock_trip_btn ?>' + '&trip_days_duration=' + '<?php echo $trip_days ?>',
		type: "POST",
		success: function(data) {
		$("#trip_data_div").html(data);
		},
		error: function() {}
	});
}
</script>

<?php
// include("modal_logout.php"); 
include("modal_settings.php");
?>

<!-- ASSIGN Driver MODAL -->

<script type="text/javascript">
$(document).ready(function (e) {
$("#AssignDriver").on('submit',(function(e) {
$("#loadicon").show();
$("#add_driver_button").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./driver_assign.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#FormADDLRMORE").on('submit',(function(e) {
$("#loadicon").show();
$("#add_more_lr_submit").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./add_more_lr_to_trip.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#lr_result_add_more").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form id="AssignDriver" autocomplete="off" style="font-size:13px"> 
<div class="modal fade" id="DriverChangeModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
     <div class="modal-content" style="">
		<div class="modal-header bg-primary">
			<span style="font-size:15px">Assign driver : <?php echo $tno; ?></span>
		</div>
		
<div class="modal-body">
	<div class="row">
			
<script>	
function VerifyDriverOTP()
{
	var otp = $('#driver_verify_otp').val();
	var mobile = $('#mobile_driver').val();
	$('#verify_otp_btn_driver').attr('disabled',true);
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "./fetch_driver_data.php",
	data: 'otp=' + otp + '&mobile=' + mobile,
	type: "POST",
	success: function(data) {
		$("#driver_div").html(data);
	},
	error: function() {}
	});
}

function SendDriverOTP()
{
	var mobile_no = $('#mobile_driver').val();
	
	if(mobile_no!='')
	{
		$('#send_otp_btn_driver').attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
		url: "./send_otp_verify_driver_mobile.php",
		data: 'mobile=' + mobile_no,
		type: "POST",
		success: function(data) {
			$("#driver_div").html(data);
		},
		error: function() {}
		});
	}
}
</script>	
			
		<div class="form-group col-md-4">
			<label>Enter Mobile No. <sup><font color="red">*</font></sup></label>
			<input maxlength="10" type="number" oninput="this.value=this.value.replace(/[^0-9]/,'')" oninput="$('#add_driver_button').attr('disabled',true)" name="mobile" id="mobile_driver" class="form-control" required />
		</div>

		<div class="form-group col-md-2">
			<?php
			if(!isMobile()){
			echo "<br />";
			}
			?>
			<img style="display:none;<?php if(!isMobile()){ echo "margin-top:5px"; } else { echo "margin-top:-10px"; } ?>;width:30px" class="verified_sign_driver img-responsive" src="../b5aY6EZzK52NA8F/verified.png" />
			<button type="button" id="send_otp_btn_driver" style="margin-top:5px;" class="btn btn-sm btn-success" onclick="SendDriverOTP();">Send OTP</button>
		</div>
		
		<div style="display:none" class="otp_verify_div_driver form-group col-md-4">
			<label>Enter OTP <sup><font color="red">*</font> <span id="otp_send_span"></span></sup></label>
			<input disabled maxlength="6" type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="otp" id="driver_verify_otp" class="form-control" required />
		</div>			
		
		<div style="display:none" class="otp_verify_div_driver form-group col-md-2">
			<?php
			if(!isMobile()){
			echo "<br />";
			}
			?>
			<img style="display:none;<?php if(!isMobile()){ echo "margin-top:5px"; } else { echo "margin-top:-10px"; } ?>;width:30px" class="verified_sign_driver img-responsive" src="../b5aY6EZzK52NA8F/verified.png" />
			<button type="button" id="verify_otp_btn_driver" disabled style="margin-top:6px;" class="btn btn-sm btn-success" onclick="VerifyDriverOTP();">Verify OTP</button>
		</div>	
		
		<div id="driver_div" class="form-group col-md-12"></div>	

	</div>
   </div>
      
	<div class="modal-footer">
          <button type="submit" id="add_driver_button" style="display:none" class="pull-left btn-sm btn btn-danger" disabled>Assign Driver</button>
          <button type="button" class="btn btn-sm btn-primary" id="driver_modal_close" data-dismiss="modal">Close</button>
	 </div>
	 
      </div>
    </div>
  </div>
</form>

<!-- Assign Driver MODAL ENDS -->

<div id="bilty_result"></div>

<?php include("modal_create_market_bilty.php"); ?>

<?php include("modal_create_new_trip.php"); ?>

<?php include("modal_update_dsl_qty.php"); ?>

<?php //include("modal_no_lr_add.php"); ?>

<?php include("modal_cash_adv.php"); ?>

<?php include("modal_freight_to_driver.php"); ?>

<?php include("modal_diesel_adv_pending.php"); ?>

<?php include("modal_cheque_adv.php"); ?>

<?php include("modal_rtgs_adv.php"); ?>

<?php include("modal_freight_collected.php"); ?>

<?php include("modal_exp.php"); ?>

<?php include("modal_asset_form_up.php"); ?>	

<?php include("modal_asset_form_view.php"); ?>	

<?php include("modal_driver_naame.php"); ?>	

<?php include("modal_activate_happay.php"); ?>	
	
<?php
$fetch_balance=Qry($conn,"SELECT asset_form_up,asset_form_down,salary_approval,amount_hold FROM dairy.driver_up WHERE down=0 AND 
code='$driver_code_value' ORDER BY id DESC LIMIT 1");

if(!$fetch_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request.","./");
	exit();
}

if(numRows($fetch_balance)==0)
{
	$hide_trip_and_hisab_btns="1";
	$asset_form_fill=0;	
}
else
{
	$row_balance=fetchArray($fetch_balance);

	if($row_balance['asset_form_down']==1){
		$asset_form_end=1;	
	}
	else{
		$asset_form_end=0;	
	}

	if($row_balance['salary_approval']==0)
	{
		echo "<div id='salary_bg'></div>
		<div class='col-md-5 col-md-offset-3'>
		<br />
		<br />
		<center>
			<h4>SALARY APPROVAL PENDING !</h4>
			<font color='maroon'>Supervisor : $supervisor_name (VISALPUR)</font>
			<br>
			<br>
			<button class='btn btn-sm btn-primary' onclick='location.reload()'>Refresh</button>
		</center>
		</div>
		<script>$('#loadicon').fadeOut('slow');</script>";
		exit();
	}

	if($row_balance['asset_form_up']==0 AND $driver_lic_no!='')
	{
		$asset_form_fill=0;	
		
		echo "<script>
		function AssetFunc()
		{
			// alert('ASSET FORM IS PENDING PLEASE FILL IT FIRST CAREFULLY.');
			document.getElementById('AssetModalButton').click(); 
			$('#loadicon').fadeOut('slow');
		}	
		AssetFunc();
		</script>";
		exit();
	}
	else
	{
		$asset_form_fill=1;
	}
}
?>

<nav class="navbar navbar-light bg-primary" style="font-family:Calibri !important;">
  <div class="container-fluid">
    <div class="navbar-header">
      <span class="navbar-brand" href="#">
		<b><span class="fa fa-truck" style="font-size:15px"></span> <span style="font-size:15px"><?php echo $tno; ?></b></span>
	 </span>
	</div>

<?php
if(isMobile()){
	echo '<ul class="nav navbar-nav" style="margin-top:-10px;">';
}
else{
	echo '<ul class="nav navbar-nav">';
}

if($driver_code!=0)
{
	$rating_value = sprintf('%.1f', $rating_average);	
			
	// if($rating_value>=4)
	// {
		// $emoji = "<img src='emoji/good1.PNG' style='width:25px;' />";
	// }
	// else if($rating_value>=3 AND $rating_value<4)
	// {
		// $emoji = "<img src='emoji/avg.PNG' style='width:25px;' />";
	// }
	// else if($rating_value>=2 AND $rating_value<3)
	// {
		// $emoji = "<img src='emoji/low.PNG' style='width:25px;' />";
	// }
	// else
	// {
		// $emoji = "<img src='emoji/bad.PNG' style='width:25px;' />";
	// }
?>
	<li>
	
<?php
if(isMobile())
{
?>
	<a href="#" style="color:#FFF;pointer-events:none;">
		<span class="fa fa-user-circle-o font_small_mobile" style="font-size:12px"></span>
		<span class="font_small_mobile" style="background:yellow;padding:1px;font-size:12px;color:#000;border-radius:5px">
		&nbsp;<b><?php echo $rating_value; ?> <font color="maroon">*</font></b>&nbsp;</span>
		<span class="font_small_mobile" style="font-size:11px"><?php echo $driver_name; ?></span>
		<br />
		<span style="font-size:12px;">		
		Balance: ₹ <span class="font_small_mobile" style="font-size:12px;" id="bal_show"></span>,
		Fuel Available: <span style="font-size:12px;"><?php echo $row_truck['diesel_left']; ?> Ltr</span>,
		Fuel Bal. (fix): <span style="font-size:12px;"><?php echo $row_truck['diesel_left_fix']; ?> Ltr</span>
		</span>		
	</a> 
<?php
}
else
{
?>
	<a href="#" style="color:#FFF;pointer-events:none;">
		<span class="fa fa-user-circle-o font_small_mobile" style="font-size:14px"></span>
		<span class="font_small_mobile" style="margin-left:3px;background:yellow;padding:1px;font-size:13px;color:#000;border-radius:5px">
		&nbsp;<b><?php echo $rating_value; ?> <font color="maroon">*</font></b>&nbsp;</span>
		<span style="font-size:15px"><?php echo $driver_name; ?></span>
		&nbsp &nbsp; Driver Balance : <span style="font-size:14px;">₹</span> <span class="font_small_mobile" style="font-size:15px;" id="bal_show"></span>, 
		&nbsp;Fuel Available: <span style="font-size:15px;"><?php echo $row_truck['diesel_left']; ?> Ltr</span>,
		&nbsp;Fuel Balance (fix): <span style="font-size:15px;"><?php echo $row_truck['diesel_left_fix']; ?> Ltr</span>
	</a> 
<?php
}
?>
		
</li>			
	<?php
	}
	else
	{
	?>
	<li>
		<a href="#" id="link2" style="color:#FFF;">
		<button data-toggle="modal" id="driver_up_button" data-target="#DriverChangeModal" class="btn btn-xs btn-default" type="button">
		<span class="fa fa-user-circle-o"></span> &nbsp;Assign Driver </button>
		</a>
	</li>		
	<?php
	}
	?>
</ul>
	
	<style>
	#link2:hover
	{
		background:inherit;
		color:inherit;
	}
	</style>

<?php
if(isMobile()){
	echo '<ul class="nav navbar-nav navbar-right" style="margin-top:-15px;">
	<style>
	table{
		font-size:11px;
	}
	</style>';
}
else{
	echo '<ul class="nav navbar-nav navbar-right">';
}
?>
	<li>
	 <?php
	 if($branch!='PANIPAT')
	 {
		 if($trishul_card=="1" AND $trishul_mapped!="1")
		 {
		?>
		<script>
		function ActiveTrishulCard()
		{
			if(confirm("Are you sure ??")==true)
			{
				jQuery.ajax({
				url: "./_active_trishul_card.php",
				data: 'tno=' + 'OK',
				type: "POST",
				success: function(data) {
					$("#result").html(data);
				},
				error: function() {}
				});
			}
		}
		</script>
			&nbsp; 	
			<button onclick="ActiveTrishulCard()" style="margin-top:7px;" class="btn btn-sm btn-default" type="button">
			<span class="glyphicon glyphicon-credit-card"></span> Assign Trishul Card</button>
		<?php			
		 }
		?>
		&nbsp; 	
		<button <?php if($branch!='HEAD'){echo "disabled";} ?> data-toggle="modal" data-target="#HappayActivateModal" id="happay_activate_button" style="<?php if(isMobile()){ echo "font-size:11px"; } else { echo "font-size:14px;margin-top:7px;"; } ?>" class="btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-default" type="button">
			<span class="glyphicon glyphicon-credit-card"></span> Assign Happay</button>

<button data-toggle="modal" data-target="#happay_trans_modal" id="happay_trans_button" style="<?php if(isMobile()){ echo "font-size:11px"; } else { echo "font-size:14px;margin-top:7px;"; } ?>" class="btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-default" type="button">
<span class="glyphicon glyphicon-credit-card" style="font-size:11px"></span> &nbsp;₹ <span id="happay_bal"></span></button>
			
		<?php
			if($asset_form_fill==1)
			{ 
				if($asset_form_end==0)
				{
				?>
				 &nbsp;
	<button id="AssetFormViewButton" data-toggle="modal" style="<?php if(isMobile()){ echo "font-size:11px"; } else { echo "font-size:14px;margin-top:7px;"; } ?>" data-target="#AssetFormViewModal" class="btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-default" type="button">
			<span class="glyphicon glyphicon-book" style="<?php if(isMobile()){ echo "font-size:10px"; } else { echo "font-size:12px;"; } ?>"></span> Asset Form </button>	
				<?php
				}
			}
		?>
	&nbsp;
		<button data-toggle="modal" data-target="#SettingModal" style="<?php if(isMobile()){ echo "font-size:11px"; } else { echo "font-size:12px;margin-top:7px;"; } ?>" class="btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-default" type="button">
			<span class="glyphicon glyphicon-cog"></span> <!--Settings --></button>
		&nbsp; 
			<?php
			}
			?>
		<button onclick="AlertLogout()" style="<?php if(isMobile()){ echo "font-size:11px"; } else { echo "font-size:12px;margin-top:7px;"; } ?>" class="btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-default" type="button">
			<span class="glyphicon glyphicon-log-in"></span> <!--Logout --></button>
		&nbsp; &nbsp;		 
	</li>
	
	</ul>
	
  </div>
</nav>

<div class="container-fluid" style="font-family: 'Open Sans', sans-serif !important">

<script>
function FinalHisab(tno,trip_no)
{
	<?php
	if($branch=='PANIPAT')
	{
	?>
		alert('You can\'t end/off trip.');
	<?php	
	}
	else if($trishul_card=="1" AND $trishul_mapped=="0")
	{
	?>
		alert('Trishul Card not found. Please active trishul card first !!');
	<?php		
	}
	else
	{
	?>
	if(trip_no!=0)
	{
		$("body").css("cursor", "not-allowed");
		$('#hisab_button').html('Please wait..');
		$('#hisab_button').attr('disabled',true);
		jQuery.ajax({
		url: "./close_trip/final_hisab.php",
		data: 'tno=' + tno + '&trip_no=' + trip_no + '&type=' + 'FINAL',
		type: "POST",
		success: function(data) {
			$("#result").html(data);
		},
		error: function() {}
		});
	}
	else
	{
		alert('Trip not found');
		window.location.href='./';
	}
	<?php
	}
	?>	
}
					
function LoadedHisab(tno,trip_no)
{
	<?php
	if($branch=='PANIPAT')
	{
	?>
		alert('You can\'t end/off trip.');
	<?php	
	}
	else if($trishul_card=="1" AND $trishul_mapped=="0")
	{
	?>
		alert('Trishul Card not found. Please active trishul card first !!');
	<?php		
	}
	else
	{
	?>
	if(trip_no!=0)
	{
		$("body").css("cursor", "not-allowed");
		$('#hisab_button_loaded').html('Please wait..');
		$('#hisab_button_loaded').attr('disabled',true);
		jQuery.ajax({
		url: "./close_trip/final_hisab.php",
		data: 'tno=' + tno + '&trip_no=' + trip_no + '&type=' + 'LOADED',
		type: "POST",
		success: function(data) {
			$("#result").html(data);
		},
		error: function() {}
		});
	}
	else
	{
		alert('Trip not found');
		window.location.href='./';
	}
	<?php
	}
	?>		
}
</script>
		
<div id="view_mb_div"></div>
		
<script>
function ViewMB(mbno)
{
	$('#loadicon').show();
	jQuery.ajax({
		url: "./load_market_bilty_for_view.php",
		data: 'mbno=' + mbno,
		type: "POST",
		success: function(data) {
			$("#view_mb_div").html(data);
		},
		error: function() {}
		});
	// $('#market_bilty_number').val(mbno);
	// $('#ViewMBForm').submit();
}
</script>
			
<div class="row">	
	<div class="col-md-4" style="overflow:auto">

<?php
$trip_chk=Qry($conn,"SELECT id,from_station,to_station,from_id,to_id,fix_lane,fix_dsl_value,km,pincode,act_wt,charge_wt,lr_type,lr_type2,lr_pending,
lr_pending_type,date(date) as trip_date,lrno,status,active_trip,cash,diesel_qty,dsl_consumption,narration FROM dairy.trip WHERE tno='$tno' ORDER by id desc");
			
if(!$trip_chk){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($trip_chk)>0)
{
	if($force_update_diesel_qty=="1" AND ($diesel_tank_cap==0 OR $supervisor_qty_approval_timestamp=='' OR $supervisor_qty_approval_timestamp==0))
	{
		if($diesel_tank_cap==0)
		{
		?>
		<button id='qty_update_btn' data-toggle='modal' data-target='#QtyUpdateModal' type='button' class='btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-success'>
		<span class='fa fa-pencil-square-o'></span> Update fuel details !</button>
		<?php	
		}	
		else if($supervisor_qty_approval_timestamp=='' || $supervisor_qty_approval_timestamp==0)
		{
			echo "<h5 style='color:maroon'>Supervisor approval pending for fuel details !</h5>
			<span style='color:blue'>Supervisor name: &nbsp; <font color=''>$supervisor_name</font></span>";	
		}
	}
	else
	{	
	echo "<div class='row'>
		<div class='col-md-12'>";
?>
	<button <?php if($trishul_card=="1" AND $trishul_mapped=="0") { echo "disabled"; } ?> id='trip_button' data-toggle='modal' data-target='#myModal' type='button' style='color:#FFF' class='btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-success'><span class='fa fa-pencil-square-o'></span> 
	<?php if($trishul_card=="1" AND $trishul_mapped=="0") { echo "Assign Trishul Card"; } else { echo "Create New Trip"; } ?></button>
	
	<button onclick="FinalHisab('<?php echo $tno; ?>','<?php echo $trip_no_db; ?>')" type='button' id='hisab_button' class='pull-right <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn btn-danger'>
	<span class='fa fa-id-card-o'></span> &nbsp; FINAL HISAB</button>
		</div>
	</div>

<?php				
	while($row_trip=fetchArray($trip_chk))
	{
		echo "<input type='hidden' id='FromLoc$row_trip[id]' value='$row_trip[from_station]'>";
		echo "<input type='hidden' id='ToLoc$row_trip[id]' value='$row_trip[to_station]'>";
		echo "<input type='hidden' id='ActWtTotal$row_trip[id]' value='$row_trip[act_wt]'>";
		echo "<input type='hidden' id='ChrgWtTotal$row_trip[id]' value='$row_trip[charge_wt]'>";
					
		if($row_trip['lr_type']=='' || empty($row_trip['lr_type']))
		{
			Redirect("Error while processing request.. $row_trip[lr_type]","./logout.php");
			exit();
		}
		else
		{
			if($row_trip['lr_type']=='ATLOADING' || $row_trip['lr_type']=='EMPTY')
			{
				$btn_func="";
				$add_more_button="disabled";
				
				if($row_trip['lr_type']=='ATLOADING'){
					$btn_text_value="LR pending";
				} else{
					$btn_text_value="Empty Trip";
				}
			}
			else
			{
				if($row_trip['status']!='0')
				{
					$btn_func="";
					$add_more_button="disabled";
					$btn_text_value="LR Attached";
				}
				else
				{
					$add_more_button="";
					$btn_func="AddMoreLR('$row_trip[id]','$row_trip[from_id]','$row_trip[to_id]','$row_trip[km]','$row_trip[pincode]')";
					$btn_text_value="+ Add more LR";
				}
			}
		}
					
		if($row_trip['lr_type2']>=2)
		{
			$btn_func="";
			$add_more_button="disabled";
			$btn_text_value="LR Attached";
		}
					
		$trip_lrnos=array();
		
		foreach(explode(",",$row_trip['lr_type']) as $trip_lr_list)
		{
			if(substr($trip_lr_list,3,1)=='M'){
				
				if(isMobile()){ 
					$trip_lrnos[]="<a href='#' style='color:maroon;font-size:11px;letter-spacing:.5px' onclick=ViewMB('$trip_lr_list')>$trip_lr_list</a>";
				}else{
					$trip_lrnos[]="<a href='#' style='color:maroon;font-size:14px;letter-spacing:.5px' onclick=ViewMB('$trip_lr_list')>$trip_lr_list</a>";
				}
			}
			else{
				$trip_lrnos[]=$trip_lr_list;
			}
		}
		
		if($row_trip['narration']=="ROAD"){
			$trip_type_icon="<label style='margin-top:8px;color:maroon'><span class='glyphicon glyphicon-road'></span> By-Road</label>";
		}
		else if($row_trip['narration']=="TRAIN"){
			$trip_type_icon="<label style='margin-top:8px;color:maroon'><span class='fa fa-train'></span> By-Rail</label>";
		}else {
			$trip_type_icon="";
		}
					
		if($row_trip['fix_lane']!="0"){
			$is_fix_lane="&nbsp; <label style='margin-top:8px;color:maroon'><span class='glyphicon glyphicon-road'></span> Fix Lane</label>";
		}
		else{
			$is_fix_lane="";
		}

		echo "<br>";
					
		if($row_trip['status']==0)
		{
			$trip_bckgnd="#66ff66";
		}
		else
		{
			if($row_trip['active_trip']==1){
				$trip_bckgnd="#cce6ff";
			}
			else{
				$trip_bckgnd="#FFFF33";
			}
		}
		
	echo "<div id='$row_trip[id]' style='overflow:auto;cursor:pointer;border-radius:10px;background:$trip_bckgnd;color:#000;font-size:12px;padding:10px;' 
			onclick=TripAdv('#$row_trip[id]') class='form-group main col-md-12 table-responsive'>
		";
	if(isMobile()){
		echo "<table border='0' style='width:100%;font-size:11px;'>";
	}else{
		echo "<table border='0' style='width:100%;font-size:12.5px;'>";
	}
	echo "<tr>
				<td>From: $row_trip[from_station]</td>
				<td></td>
				<td>To: $row_trip[to_station]</td>
			</tr>
						
			<tr>
				<td colspan='2'>Act Wt: $row_trip[act_wt], Chrg Wt: $row_trip[charge_wt]</td>
				<td>Date: ".convertDate("d-m-y",$row_trip['trip_date'])."</td>
			</tr>
						
			<tr>
				<td colspan='3'>LR Type: ".implode(", ",$trip_lrnos)."</td>
			</tr>
						
			<tr>
				<td colspan='3' style='padding-right:5px !important'>LR No: $row_trip[lrno]</td>
			</tr>
						
			<tr>
				<td>Advance : <span id=''>$row_trip[cash]</span></td>
				<td></td>
				<td>Diesel_Given : <span id=''>$row_trip[diesel_qty] Ltr</span></td>
			</tr>
			
			<tr>
				<td style='maroon' colspan='2'>
					Diesel Consumption : <span>$row_trip[dsl_consumption] Ltr</span>
				</td>
				<td>Fix Diesel : <span>$row_trip[fix_dsl_value] Ltr</span></td>
			</tr>
						
			<tr>
				<td colspan='2'><button $add_more_button style='margin-top:4px;' class='btn btn-xs btn-default' type='button' 
					onclick=$btn_func>$btn_text_value</button>
				</td>
				<td>$trip_type_icon $is_fix_lane</td>
			</tr>
		</table>
	</div>";
	}
	}
}
else
{
echo "<div class='row'>
	<div class='col-md-12'>";
	if($diesel_tank_cap==0)
	{
?>
<button id='qty_update_btn' data-toggle='modal' data-target='#QtyUpdateModal' type='button' class='btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-success'>
<span class='fa fa-pencil-square-o'></span> Update fuel details !</button>
<?php	
	}	
	else if($supervisor_qty_approval_timestamp=='' || $supervisor_qty_approval_timestamp==0)
	{
		echo "<h5 style='color:maroon'>Supervisor approval pending for fuel details !</h5>
		<span style='color:blue'>Supervisor name: &nbsp; <font color=''>$supervisor_name</font></span>";	
	}
	else
	{	
echo "<span style='color:red'>No running trip found.</span><br>";	
	?>
	<button id='trip_button' data-toggle='modal' data-target='#myModal' type='button' class='btn <?php if(isMobile()){ echo "btn-xs"; } else { echo "btn-sm"; } ?> btn-success'
		<?php if($trishul_card=="1" AND $trishul_mapped=="0"){ echo "disabled"; } ?> style='margin-top:5px'>
		<span class='fa fa-pencil-square-o'></span> 
		<?php if($trishul_card=="1" AND $trishul_mapped=="0") { echo "Assign Trishul Card"; } else { echo "Create New Trip"; } ?></button>
	<?php
	}
	?>
	</div>
</div>
<?php
}
?>
	</div>

<?php
if(isMobile()){
	echo '<div class="col-md-8" style="font-family:Calibri !important;">';
}
else{
	echo '<div class="col-md-8">';
}
?>
		<div class="row" id="trip_branch_view_div" style="color:#000;padding-top:6px;padding-bottom:6px">
			<div class="form-group col-md-6">
				<span style="font-size:13px">Edit branch : <span id="edit_branch_name"></span> , 
				TripCreated By : <span id="trip_created_by_name"></span>
			</div>
			<div id="edit_branch_div" class="col-md-6"></div>
		</div>
		
		<div class="form-group col-md-12" id="trip_locs_div" style="background:#EEE;color:#000;border:1px solid #ddd;padding:2px;">
			<center><span> 
			Trip : <span id="station_div"></span>
			&nbsp; <span id="print_button_div"></span></span></center>
		</div>
		
		<div class="row" id="trip_data_div">	
		</div>
	</div>
	
</div>
</div>

<?php
include("alerts.php");
?>

</body>
</html>

<script>
function LoadDriverBalance(code)
{
	jQuery.ajax({
	url: "./fetch_driver_balance.php",
	data: 'code=' + code,
	type: "POST",
	success: function(data) {
		$("#result_bal").html(data);
	},
	error: function() {}
	});
}
LoadDriverBalance('<?php echo $driver_code_value; ?>');			
</script>

	
<script>	
function CheckHappayStatus(card_no)
{
	if(card_no=='NA')
	{
		$('#happay_activate_button').show();
		$('#happay_trans_button').hide();
		$('#happay_activate_button').attr('disabled',false);
		$('#happay_trans_button').attr('disabled',true);
	}
	else
	{
		$('#happay_activate_button').hide();
		$('#happay_trans_button').show();
		$('#happay_activate_button').attr('disabled',true);
		$('#happay_trans_button').attr('disabled',false);
	}
}
</script>

<form action='./print/single_trip.php' target="_blank" id='PrintForm1' method='POST'>
	<input type='hidden' id="trip_id_print" name='trip_id_print'>
	<input type='hidden' value="<?php echo $driver_name; ?>" name='driver_name'>
</form>

<form action='./market_bilty_view.php' method='POST' id="ViewMBForm" target='_blank'>
	<input type='hidden' name='bilty_no' id="market_bilty_number" />
</form>

<div id="result_bal"></div>

<style>
    .pac-container {
        z-index: 10000 !important;
    }
</style>

<script>
function TripAdvBasic()
{
	$('#loadicon').show();
	
	<?php
	if(!isMobile())
	{
	?>
		$('.main').attr('class','form-group col-md-12 main');
		$('#<?php echo $trip_basic_id; ?>').attr('class','form-group col-md-12 div1 main');
	<?php
	}
	else
	{
	?>
		$('.main').attr('class','col-md-12 main');
		$('#<?php echo $trip_basic_id; ?>').attr('class','col-md-12 div1 main');
	<?php	
	}
	?>
	
	jQuery.ajax({
		url: "fetch_trip_data.php",
		data: 'id=' + '<?php echo $trip_basic_id; ?>' + '&lock_trip_btn=' + '<?php echo $lock_trip_btn ?>' + '&trip_days_duration=' + '<?php echo $trip_days ?>',
		type: "POST",
		success: function(data) {
			$("#trip_data_div").html(data);
		},
		error: function() {}
	});
}
</script>
 
<button id="no_lr_add_modal_button11" style="display:none" type="button" data-toggle="modal" data-target="#ModalNoLRAdd"></button>

<script>
function NoLRCall(id,tno)
{
	$('#no_lr_trip_id').val(id);
	$('#no_lr_truck_no').val(tno);
	$('#no_lr_add_modal_button11').click();
}

function AddMoreLR(id,from_loc_id,to_loc_id,km,pincode)
{
	$('#trip_id_for_add_more').val(id);
	$('#from_loc_db_txt_for_add_more').val($('#FromLoc'+id).val());
	$('#total_awt_add_more').html($('#ActWtTotal'+id).val());
	$('#act_wt_db_add_more').val($('#ActWtTotal'+id).val());
	$('#total_cwt_add_more').html($('#ChrgWtTotal'+id).val());
	$('#chrg_wt_db_add_more').val($('#ChrgWtTotal'+id).val());
	
	$('#add_more_modal_button').click();
}
</script>

<?php include("modal_add_more_lr.php"); ?>
<?php include("modal_view_happay_transactions.php"); ?>

<?php	
$fetch_ac=Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$driver_code_value'");
if(!$fetch_ac){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($fetch_ac)>0)
{
	$row_ac=fetchArray($fetch_ac);
	
	echo "<script>
	function RTGS()
	{
		$('#ac_holder').val('$row_ac[acname]');
		$('#driver_acno').val('$row_ac[acno]');
		$('#driver_bank_name').val('$row_ac[bank]');
		$('#driver_ifsc').val('$row_ac[ifsc]');
		$('#ac_holder').attr('readonly',true);
		$('#driver_acno').attr('readonly',true);
		$('#driver_bank_name').attr('readonly',true);
		$('#driver_ifsc').attr('readonly',true);
	}
	RTGS();
	</script>";
}
else
{
	echo "
	<script>
	function RTGS()
	{
		$('#ac_holder').attr('readonly',true);
		$('#driver_acno').attr('readonly',true);
		$('#driver_bank_name').attr('readonly',true);
		$('#driver_ifsc').attr('readonly',true);
	}
	RTGS();
	</script>";
}
?>

<script>	
function PrintTrip(id)
{
	$("#trip_id_print").val(id);
	document.getElementById('PrintForm1').submit();
}
</script>

<?php
if(numRows($trip_chk)>0)
{
	echo "<script>TripAdvBasic();</script>";	
}
else
{
	echo "<script>$('#trip_branch_view_div').hide();$('#trip_locs_div').hide();$('#loadicon').fadeOut('slow');</script>";	
}

if($is_trip_created=="0")
{
	HideLockHisabBtn(); // trip not created hide trip and hisab btns
}
else if($hide_trip_and_hisab_btns=='1')
{
	HideTripHisabBtn(); // if driver is not active
}

include("./footer.php");
?>